/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ch.post.it.evoting.controlcomponent.domain.MixnetInitialCiphertextsEntity;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
public interface MixnetInitialCiphertextsRepository extends CrudRepository<MixnetInitialCiphertextsEntity, Long> {

	Optional<MixnetInitialCiphertextsEntity> findByElectionEventIdAndBallotBoxId(final String electionEventId, final String ballotBoxId);
}
