/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.service;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;

import ch.post.it.evoting.controlcomponent.domain.EncryptedVerifiableVoteEntity;
import ch.post.it.evoting.controlcomponent.domain.VerificationCardEntity;
import ch.post.it.evoting.controlcomponent.domain.VerificationCardSetEntity;
import ch.post.it.evoting.controlcomponent.repository.EncryptedVerifiableVoteRepository;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ExponentiationProof;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.PlaintextEqualityProof;
import ch.post.it.evoting.evotinglibraries.domain.common.ContextIds;
import ch.post.it.evoting.evotinglibraries.domain.common.EncryptedVerifiableVote;

@Service
public class EncryptedVerifiableVoteService {

	private static final String GROUP = "group";

	private final ObjectMapper objectMapper;
	private final ElectionEventService electionEventService;
	private final VerificationCardService verificationCardService;
	private final EncryptedVerifiableVoteRepository encryptedVerifiableVoteRepository;

	public EncryptedVerifiableVoteService(
			final ObjectMapper objectMapper,
			final ElectionEventService electionEventService,
			final VerificationCardService verificationCardService,
			final EncryptedVerifiableVoteRepository encryptedVerifiableVoteRepository) {
		this.objectMapper = objectMapper;
		this.electionEventService = electionEventService;
		this.verificationCardService = verificationCardService;
		this.encryptedVerifiableVoteRepository = encryptedVerifiableVoteRepository;
	}

	@Transactional
	public void save(final EncryptedVerifiableVote encryptedVerifiableVote) {
		checkNotNull(encryptedVerifiableVote);

		final ContextIds contextIds = encryptedVerifiableVote.contextIds();

		final byte[] serializedEncodeEncryptedVote;
		final byte[] serializedExponentiatedEncryptedVote;
		final byte[] serializedEncryptedPartialChoiceReturnCodes;
		final byte[] serializedExponentiationProof;
		final byte[] serializedPlaintextEqualityProof;
		final byte[] serializedContextIds;
		try {
			serializedContextIds = objectMapper.writeValueAsBytes(contextIds);
			serializedEncodeEncryptedVote = objectMapper.writeValueAsBytes(encryptedVerifiableVote.encryptedVote());
			serializedExponentiatedEncryptedVote = objectMapper.writeValueAsBytes(encryptedVerifiableVote.exponentiatedEncryptedVote());
			serializedEncryptedPartialChoiceReturnCodes = objectMapper.writeValueAsBytes(
					encryptedVerifiableVote.encryptedPartialChoiceReturnCodes());
			serializedExponentiationProof = objectMapper.writeValueAsBytes(encryptedVerifiableVote.exponentiationProof());
			serializedPlaintextEqualityProof = objectMapper.writeValueAsBytes(encryptedVerifiableVote.plaintextEqualityProof());

		} catch (final JsonProcessingException e) {
			throw new UncheckedIOException("Failed to serialize encrypted verifiable vote.", e);
		}

		final VerificationCardEntity verificationCardEntity = verificationCardService.getVerificationCardEntity(contextIds.verificationCardId());

		final EncryptedVerifiableVoteEntity encryptedVerifiableVoteEntity = new EncryptedVerifiableVoteEntity.Builder()
				.setContextIds(serializedContextIds)
				.setEncryptedVote(serializedEncodeEncryptedVote)
				.setExponentiatedEncryptedVote(serializedExponentiatedEncryptedVote)
				.setEncryptedPartialChoiceReturnCodes(serializedEncryptedPartialChoiceReturnCodes)
				.setExponentiationProof(serializedExponentiationProof)
				.setPlaintextEqualityProof(serializedPlaintextEqualityProof)
				.setVerificationCardEntity(verificationCardEntity)
				.build();
		encryptedVerifiableVoteRepository.save(encryptedVerifiableVoteEntity);
	}

	@Transactional
	public EncryptedVerifiableVote getEncryptedVerifiableVote(final String verificationCardId) {
		validateUUID(verificationCardId);

		final EncryptedVerifiableVoteEntity encryptedVerifiableVoteEntity = encryptedVerifiableVoteRepository.findByVerificationCardId(
						verificationCardId)
				.orElseThrow(() -> new IllegalStateException(
						String.format("Encrypted verifiable vote not found. [verificationCardId: %s]", verificationCardId)));

		final VerificationCardSetEntity verificationCardSetEntity = encryptedVerifiableVoteEntity.getVerificationCardEntity()
				.getVerificationCardSetEntity();
		final String electionEventId = verificationCardSetEntity.getElectionEventEntity().getElectionEventId();

		final GqGroup encryptionGroup = electionEventService.getEncryptionGroup(electionEventId);

		return deserializeEncryptedVerifiableVote(encryptedVerifiableVoteEntity, encryptionGroup);
	}

	@Transactional
	public List<EncryptedVerifiableVote> getConfirmedVotes(final String verificationCardSetId) {
		validateUUID(verificationCardSetId);

		final List<EncryptedVerifiableVoteEntity> confirmedVoteEntities = encryptedVerifiableVoteRepository.findAllConfirmedByVerificationCardSetId(
				verificationCardSetId);

		if (confirmedVoteEntities.isEmpty()) {
			return List.of();
		}

		final VerificationCardSetEntity verificationCardSetEntity = confirmedVoteEntities.get(0).getVerificationCardEntity()
				.getVerificationCardSetEntity();
		final String electionEventId = verificationCardSetEntity.getElectionEventEntity().getElectionEventId();

		final GqGroup encryptionGroup = electionEventService.getEncryptionGroup(electionEventId);

		return confirmedVoteEntities.stream()
				.map(encryptedVerifiableVoteEntity -> deserializeEncryptedVerifiableVote(encryptedVerifiableVoteEntity, encryptionGroup))
				.toList();
	}

	private EncryptedVerifiableVote deserializeEncryptedVerifiableVote(final EncryptedVerifiableVoteEntity encryptedVerifiableVoteEntity,
			final GqGroup encryptionGroup) {
		final ObjectReader reader = objectMapper.reader().withAttribute(GROUP, encryptionGroup);

		final ContextIds contextIds;
		final ElGamalMultiRecipientCiphertext encryptedVote;
		final ElGamalMultiRecipientCiphertext exponentiatedEncryptedVote;
		final ElGamalMultiRecipientCiphertext encryptedPartialChoiceReturnCodes;
		final ExponentiationProof exponentiationProof;
		final PlaintextEqualityProof plaintextEqualityProof;
		try {
			contextIds = reader.readValue(encryptedVerifiableVoteEntity.getContextIds(), ContextIds.class);
			encryptedVote = reader.readValue(encryptedVerifiableVoteEntity.getEncryptedVote(), ElGamalMultiRecipientCiphertext.class);
			exponentiatedEncryptedVote = reader.readValue(encryptedVerifiableVoteEntity.getExponentiatedEncryptedVote(),
					ElGamalMultiRecipientCiphertext.class);
			encryptedPartialChoiceReturnCodes = reader.readValue(encryptedVerifiableVoteEntity.getEncryptedPartialChoiceReturnCodes(),
					ElGamalMultiRecipientCiphertext.class);
			exponentiationProof = reader.readValue(encryptedVerifiableVoteEntity.getExponentiationProof(), ExponentiationProof.class);
			plaintextEqualityProof = reader.readValue(encryptedVerifiableVoteEntity.getPlaintextEqualityProof(), PlaintextEqualityProof.class);
		} catch (final IOException e) {
			throw new UncheckedIOException("Failed to deserialize encrypted verifiable vote.", e);
		}

		return new EncryptedVerifiableVote(contextIds, encryptedVote, exponentiatedEncryptedVote, encryptedPartialChoiceReturnCodes,
				exponentiationProof, plaintextEqualityProof);
	}

}
