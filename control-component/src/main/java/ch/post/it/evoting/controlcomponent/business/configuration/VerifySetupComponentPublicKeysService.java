/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.business.configuration;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.common.collect.MoreCollectors;

import ch.post.it.evoting.controlcomponent.service.CcmjElectionKeysService;
import ch.post.it.evoting.controlcomponent.service.CcrjReturnCodesKeysService;
import ch.post.it.evoting.controlcomponent.service.ElectionContextService;
import ch.post.it.evoting.controlcomponent.service.ElectionEventService;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.evotinglibraries.domain.election.ControlComponentPublicKeys;
import ch.post.it.evoting.evotinglibraries.domain.election.ElectionEventContext;
import ch.post.it.evoting.evotinglibraries.domain.election.SetupComponentPublicKeys;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.preliminaries.proofofcorrectkeygeneration.VerifyKeyGenerationSchnorrProofsAlgorithm;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.preliminaries.proofofcorrectkeygeneration.VerifyKeyGenerationSchnorrProofsInput;

@Service
public class VerifySetupComponentPublicKeysService {

	public static final Logger LOGGER = LoggerFactory.getLogger(VerifySetupComponentPublicKeysService.class);

	private final ElectionEventService electionEventService;
	private final ElectionContextService electionContextService;
	private final CcmjElectionKeysService ccmjElectionKeysService;
	private final CcrjReturnCodesKeysService ccrjReturnCodesKeysService;
	private final VerifyKeyGenerationSchnorrProofsAlgorithm verifyKeyGenerationSchnorrProofsAlgorithm;

	@Value("${nodeID}")
	private int nodeId;

	public VerifySetupComponentPublicKeysService(
			final ElectionEventService electionEventService,
			final ElectionContextService electionContextService,
			final CcmjElectionKeysService ccmjElectionKeysService,
			final CcrjReturnCodesKeysService ccrjReturnCodesKeysService,
			final VerifyKeyGenerationSchnorrProofsAlgorithm verifyKeyGenerationSchnorrProofsAlgorithm) {
		this.electionEventService = electionEventService;
		this.electionContextService = electionContextService;
		this.ccmjElectionKeysService = ccmjElectionKeysService;
		this.ccrjReturnCodesKeysService = ccrjReturnCodesKeysService;
		this.verifyKeyGenerationSchnorrProofsAlgorithm = verifyKeyGenerationSchnorrProofsAlgorithm;
	}

	/**
	 * Verifies the {@link SetupComponentPublicKeys} by verifying:
	 * <ul>
	 *     <li>the consistency of the encryption group</li>
	 *     <li>the election event is not over</li>
	 *     <li>the received CCRj and CCMj public keys match the saved CCRj and CCMj public keys</li>
	 *     <li>the key generation Schnorr proofs of knowledge with the {@link VerifyKeyGenerationSchnorrProofsAlgorithm}</li>
	 * </ul>
	 *
	 * @param electionEventId          the election event id. Must be non-null and a valid UUID.
	 * @param setupComponentPublicKeys the {@link SetupComponentPublicKeys}. Must be non-null.
	 * @throws IllegalStateException if any consistency or key generation Schnorr proofs validation fails.
	 */
	public void verifySetupComponentPublicKeys(final String electionEventId, final SetupComponentPublicKeys setupComponentPublicKeys) {
		validateUUID(electionEventId);
		checkNotNull(setupComponentPublicKeys);

		// Verify group consistency.
		final GqGroup encryptionGroup = electionEventService.getEncryptionGroup(electionEventId);
		checkState(encryptionGroup.equals(setupComponentPublicKeys.electionPublicKey().getGroup()),
				"The Setup Component encryption group does not match the saved encryption group. [electionEventId: %s, nodeId: %s]", electionEventId,
				nodeId);

		// Verify election event is not over.
		final LocalDateTime electionEventFinishTime = electionContextService.getElectionEventFinishTime(electionEventId);
		checkState(LocalDateTime.now().isBefore(electionEventFinishTime), "The election event is over. [electionEventId: %s, nodeId: %s]",
				electionEventId, nodeId);

		// Verify public keys consistency.
		final ControlComponentPublicKeys receivedControlComponentPublicKeys = setupComponentPublicKeys.combinedControlComponentPublicKeys().stream()
				.filter(controlComponentPublicKeys -> controlComponentPublicKeys.nodeId() == nodeId)
				.collect(MoreCollectors.onlyElement());

		final ElGamalMultiRecipientPublicKey ccrjReturnCodesEncryptionPublicKey = ccrjReturnCodesKeysService.getCcrjReturnCodesKeys(electionEventId)
				.ccrjChoiceReturnCodesEncryptionKeyPair().getPublicKey();
		checkState(ccrjReturnCodesEncryptionPublicKey.equals(receivedControlComponentPublicKeys.ccrjChoiceReturnCodesEncryptionPublicKey()),
				"The Setup Component CCRj Return Codes encryption public key does not match the saved CCRj Return Codes encryption public key. [electionEventId: %s, nodeId: %s]",
				electionEventId, nodeId);

		final ElGamalMultiRecipientPublicKey ccmjElectionPublicKey = ccmjElectionKeysService.getCcmjElectionKeyPair(electionEventId).getPublicKey();
		checkState(ccmjElectionPublicKey.equals(receivedControlComponentPublicKeys.ccmjElectionPublicKey()),
				"The Setup Component CCMj election public key does not match the saved CCMj election public key. [electionEventId: %s, nodeId: %s]",
				electionEventId, nodeId);

		// Verify public keys Schnorr proofs.
		final ElectionEventContext electionEventContext = electionContextService.getElectionEventContext(electionEventId);
		final VerifyKeyGenerationSchnorrProofsInput verifyKeyGenerationSchnorrProofsInput = new VerifyKeyGenerationSchnorrProofsInput(
				setupComponentPublicKeys);
		checkState(verifyKeyGenerationSchnorrProofsAlgorithm.verifyKeyGenerationSchnorrProofs(electionEventContext,
				verifyKeyGenerationSchnorrProofsInput), "The Schnorr proofs are invalid. [electionEventId: %s]");
	}
}
