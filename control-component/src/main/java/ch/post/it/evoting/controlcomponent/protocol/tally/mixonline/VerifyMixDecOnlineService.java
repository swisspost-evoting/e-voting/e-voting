/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.protocol.tally.mixonline;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.common.base.Preconditions;

import ch.post.it.evoting.controlcomponent.service.SetupComponentPublicKeysService;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.mixnet.VerifiableShuffle;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.VerifiableDecryptions;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.ControlComponentShufflePayload;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.tally.mixoffline.VerifyMixDecInput;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.tally.mixonline.GetMixnetInitialCiphertextsOutput;

@Service
public class VerifyMixDecOnlineService {

	private static final Logger LOGGER = LoggerFactory.getLogger(VerifyMixDecOnlineService.class);

	private final VerifyMixDecOnlineAlgorithm verifyMixDecOnlineAlgorithm;
	private final SetupComponentPublicKeysService setupComponentPublicKeysService;

	@Value("${nodeID}")
	private int nodeId;

	public VerifyMixDecOnlineService(final VerifyMixDecOnlineAlgorithm verifyMixDecOnlineAlgorithm,
			final SetupComponentPublicKeysService setupComponentPublicKeysService) {
		this.verifyMixDecOnlineAlgorithm = verifyMixDecOnlineAlgorithm;
		this.setupComponentPublicKeysService = setupComponentPublicKeysService;
	}

	/**
	 * Invokes the VerifyMixDecOnline algorithm.
	 *
	 * @param controlComponentShufflePayloads   the control component shuffle payloads. Must be non-null.
	 * @param encryptionGroup                   the encryption group. Must be non-null.
	 * @param numberOfWriteInsPlusOne           the number of write-ins plus one. Must be in range [1, delta<sub>sup</sub>].
	 * @param ccmElectionPublicKeys             the CCM election public keys. Must be non-null.
	 * @param electoralBoardPublicKey           the electoral board public key. Must be non-null.
	 * @param getMixnetInitialCiphertextsOutput the output of the algorithm GetMixnetInitialCiphertexts. Must be non-null.
	 * @throws NullPointerException if any parameter is null.
	 */
	public boolean verifyMixDecOnline(final List<ControlComponentShufflePayload> controlComponentShufflePayloads,
			final GqGroup encryptionGroup,
			final int numberOfWriteInsPlusOne,
			final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> ccmElectionPublicKeys,
			final ElGamalMultiRecipientPublicKey electoralBoardPublicKey,
			final GetMixnetInitialCiphertextsOutput getMixnetInitialCiphertextsOutput) {
		checkNotNull(encryptionGroup);
		checkNotNull(ccmElectionPublicKeys);
		checkNotNull(electoralBoardPublicKey);
		checkNotNull(getMixnetInitialCiphertextsOutput);
		checkNotNull(controlComponentShufflePayloads);

		final List<ControlComponentShufflePayload> controlComponentShufflePayloadsCopy = checkNotNull(controlComponentShufflePayloads).stream()
				.map(Preconditions::checkNotNull)
				.toList();
		final String electionEventId = controlComponentShufflePayloadsCopy.get(0).getElectionEventId();
		final String ballotBoxId = controlComponentShufflePayloadsCopy.get(0).getBallotBoxId();

		final ElGamalMultiRecipientPublicKey electionPublicKey = setupComponentPublicKeysService.getElectionPublicKey(electionEventId);

		final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> initialCiphertexts = getMixnetInitialCiphertextsOutput.mixnetInitialCiphertexts();

		final List<VerifiableShuffle> precedingVerifiableShuffledVotes = controlComponentShufflePayloadsCopy.stream()
				.map(ControlComponentShufflePayload::getVerifiableShuffle)
				.toList();
		final List<VerifiableDecryptions> precedingVerifiableDecryptions = controlComponentShufflePayloadsCopy.stream()
				.map(ControlComponentShufflePayload::getVerifiableDecryptions)
				.toList();

		final VerifyMixDecOnlineContext verifyMixDecOnlineContext = new VerifyMixDecOnlineContext(encryptionGroup, nodeId, electionEventId,
				ballotBoxId, numberOfWriteInsPlusOne, electionPublicKey, ccmElectionPublicKeys, electoralBoardPublicKey);

		final VerifyMixDecInput verifyMixDecInput = new VerifyMixDecInput(initialCiphertexts, precedingVerifiableShuffledVotes,
				precedingVerifiableDecryptions);

		LOGGER.debug("Performing VerifyMixDecOnline algorithm... [electionEventId: {}, ballotBoxId: {}, nodeId: {}]", electionEventId, ballotBoxId,
				nodeId);

		return verifyMixDecOnlineAlgorithm.verifyMixDecOnline(verifyMixDecOnlineContext, verifyMixDecInput);
	}
}
