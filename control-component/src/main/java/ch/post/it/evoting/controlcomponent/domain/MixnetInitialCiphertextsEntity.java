/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.domain;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.BASE64_ENCODED_HASH_OUTPUT_LENGTH;
import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateBase64Encoded;
import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Arrays;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import jakarta.persistence.Version;

@Entity
@Table(name = "MIXNET_INITIAL_CIPHERTEXTS")
public class MixnetInitialCiphertextsEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MIXNET_INITIAL_CIPHERTEXTS_SEQ_GENERATOR")
	@SequenceGenerator(sequenceName = "MIXNET_INITIAL_CIPHERTEXTS_SEQ", allocationSize = 1, name = "MIXNET_INITIAL_CIPHERTEXTS_SEQ_GENERATOR")
	private Long id;

	private String electionEventId;

	private String ballotBoxId;

	private String encryptedConfirmedVotesHash;

	private byte[] mixnetInitialCiphertexts;

	@Version
	private Integer changeControlId;

	public MixnetInitialCiphertextsEntity() {
		// Intentionally left blank.
	}

	public MixnetInitialCiphertextsEntity(final String electionEventId, final String ballotBoxId,
			final String encryptedConfirmedVotesHash, final byte[] mixnetInitialCiphertexts) {
		this.electionEventId = validateUUID(electionEventId);
		this.ballotBoxId = validateUUID(ballotBoxId);
		this.encryptedConfirmedVotesHash = checkNotNull(encryptedConfirmedVotesHash);
		checkArgument(this.encryptedConfirmedVotesHash.length() == BASE64_ENCODED_HASH_OUTPUT_LENGTH,
				"The hash of the encrypted, confirmed votes must be of size %s.", BASE64_ENCODED_HASH_OUTPUT_LENGTH);
		validateBase64Encoded(this.encryptedConfirmedVotesHash);
		this.mixnetInitialCiphertexts = Arrays.copyOf(checkNotNull(mixnetInitialCiphertexts), mixnetInitialCiphertexts.length);
	}

	public String getEncryptedConfirmedVotesHash() {
		return encryptedConfirmedVotesHash;
	}

	public byte[] getMixnetInitialCiphertexts() {
		return Arrays.copyOf(mixnetInitialCiphertexts, mixnetInitialCiphertexts.length);
	}
}
