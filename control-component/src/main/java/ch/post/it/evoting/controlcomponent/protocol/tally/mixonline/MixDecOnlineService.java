/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.protocol.tally.mixonline;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.common.base.Preconditions;

import ch.post.it.evoting.controlcomponent.service.CcmjElectionKeysService;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPrivateKey;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.VerifiableDecryptions;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.ControlComponentVotesHashPayload;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.tally.mixonline.GetMixnetInitialCiphertextsOutput;

@Service
public class MixDecOnlineService {
	private static final Logger LOGGER = LoggerFactory.getLogger(MixDecOnlineService.class);

	private final MixDecOnlineAlgorithm mixDecOnlineAlgorithm;
	private final CcmjElectionKeysService ccmjElectionKeysService;

	@Value("${nodeID}")
	private int nodeId;

	public MixDecOnlineService(final MixDecOnlineAlgorithm mixDecOnlineAlgorithm,
			final CcmjElectionKeysService ccmjElectionKeysService) {
		this.mixDecOnlineAlgorithm = mixDecOnlineAlgorithm;
		this.ccmjElectionKeysService = ccmjElectionKeysService;
	}

	/**
	 * Invokes the MixDecOnline algorithm.
	 *
	 * @param controlComponentVotesHashPayloads the control component votes hash payloads. Must be non-null.
	 * @param encryptionGroup                   the encryption group. Must be non-null.
	 * @param numberOfWriteInsPlusOne           the number of write-ins plus one. Must be in range [1, delta<sub>sup</sub>].
	 * @param ccmElectionPublicKeys             the CCM election public keys. Must be non-null.
	 * @param electoralBoardPublicKey           the electoral board public key. Must be non-null.
	 * @param getMixnetInitialCiphertextsOutput the output of the algorithm GetMixnetInitialCiphertexts. Must be non-null.
	 * @param precedingVerifiableDecryptions    the preceding verifiable decryptions. Must be non-null.
	 * @throws NullPointerException     if any parameter is null.
	 * @throws IllegalArgumentException if the control component votes hash payloads are not valid.
	 */
	public MixDecOnlineOutput mixDecOnline(final List<ControlComponentVotesHashPayload> controlComponentVotesHashPayloads,
			final GqGroup encryptionGroup,
			final int numberOfWriteInsPlusOne,
			final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> ccmElectionPublicKeys,
			final ElGamalMultiRecipientPublicKey electoralBoardPublicKey,
			final GetMixnetInitialCiphertextsOutput getMixnetInitialCiphertextsOutput,
			final List<VerifiableDecryptions> precedingVerifiableDecryptions) {
		checkNotNull(encryptionGroup);
		checkNotNull(ccmElectionPublicKeys);
		checkNotNull(electoralBoardPublicKey);
		checkNotNull(getMixnetInitialCiphertextsOutput);
		checkNotNull(precedingVerifiableDecryptions);

		final List<ControlComponentVotesHashPayload> controlComponentVotesHashPayloadsCopy = checkNotNull(controlComponentVotesHashPayloads).stream()
				.map(Preconditions::checkNotNull)
				.toList();
		final String electionEventId = controlComponentVotesHashPayloadsCopy.get(0).getElectionEventId();
		final String ballotBoxId = controlComponentVotesHashPayloadsCopy.get(0).getBallotBoxId();

		final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> partiallyDecryptedVotes = nodeId == 1
				? getMixnetInitialCiphertextsOutput.mixnetInitialCiphertexts()
				: precedingVerifiableDecryptions.get(precedingVerifiableDecryptions.size() - 1).getCiphertexts();

		final ElGamalMultiRecipientPrivateKey ccmjElectionSecretKey = ccmjElectionKeysService.getCcmjElectionKeyPair(electionEventId)
				.getPrivateKey();

		final String encryptedConfirmedVotesHash = getMixnetInitialCiphertextsOutput.encryptedConfirmedVotesHash();

		final List<String> encryptedConfirmedVotesHashes = controlComponentVotesHashPayloadsCopy.stream()
				.map(ControlComponentVotesHashPayload::getEncryptedConfirmedVotesHash)
				.toList();

		final MixDecOnlineContext mixDecOnlineContext = new MixDecOnlineContext.Builder()
				.setEncryptionGroup(encryptionGroup)
				.setNodeId(nodeId)
				.setElectionEventId(electionEventId)
				.setBallotBoxId(ballotBoxId)
				.setNumberOfAllowedWriteInsPlusOne(numberOfWriteInsPlusOne)
				.setCcmElectionPublicKeys(ccmElectionPublicKeys)
				.setElectoralBoardPublicKey(electoralBoardPublicKey)
				.build();

		final MixDecOnlineInput mixDecOnlineInput = new MixDecOnlineInput.Builder()
				.setPartiallyDecryptedVotes(partiallyDecryptedVotes)
				.setCcmjElectionSecretKey(ccmjElectionSecretKey)
				.setEncryptedConfirmedVotesHash(encryptedConfirmedVotesHash)
				.setEncryptedConfirmedVotesHashes(encryptedConfirmedVotesHashes)
				.build();

		LOGGER.debug("Performing MixDecOnline algorithm... [electionEventId: {}, nodeId: {}]", electionEventId, nodeId);

		return mixDecOnlineAlgorithm.mixDecOnline(mixDecOnlineContext, mixDecOnlineInput);
	}
}
