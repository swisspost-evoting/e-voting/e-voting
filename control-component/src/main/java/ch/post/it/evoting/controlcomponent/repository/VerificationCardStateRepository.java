/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ch.post.it.evoting.controlcomponent.domain.VerificationCardStateEntity;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
public interface VerificationCardStateRepository extends CrudRepository<VerificationCardStateEntity, Long> {

	@Query("select e from VerificationCardStateEntity e where e.verificationCardEntity.verificationCardId = ?1")
	Optional<VerificationCardStateEntity> findByVerificationCardId(final String verificationCardId);

}
