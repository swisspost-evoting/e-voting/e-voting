/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.business.configuration;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ch.post.it.evoting.controlcomponent.domain.CcrjReturnCodesKeys;
import ch.post.it.evoting.controlcomponent.protocol.configuration.setuptally.SetupTallyCCMOutput;
import ch.post.it.evoting.controlcomponent.protocol.configuration.setuptally.SetupTallyCCMService;
import ch.post.it.evoting.controlcomponent.protocol.configuration.setupvoting.GenKeysCCROutput;
import ch.post.it.evoting.controlcomponent.protocol.configuration.setupvoting.GenKeysCCRService;
import ch.post.it.evoting.controlcomponent.service.CcmjElectionKeysService;
import ch.post.it.evoting.controlcomponent.service.CcrjReturnCodesKeysService;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientKeyPair;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.ZqElement;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.SchnorrProof;
import ch.post.it.evoting.evotinglibraries.domain.election.ControlComponentPublicKeys;
import ch.post.it.evoting.evotinglibraries.domain.election.ElectionEventContext;

@Service
class KeyGenerationService {

	private static final Logger LOGGER = LoggerFactory.getLogger(KeyGenerationService.class);

	private final GenKeysCCRService genKeysCCRService;
	private final SetupTallyCCMService setupTallyCCMService;
	private final CcmjElectionKeysService ccmjElectionKeysService;
	private final CcrjReturnCodesKeysService ccrjReturnCodesKeysService;

	@Value("${nodeID}")
	private int nodeId;

	KeyGenerationService(
			final GenKeysCCRService genKeysCCRService,
			final SetupTallyCCMService setupTallyCCMService,
			final CcmjElectionKeysService ccmjElectionKeysService,
			final CcrjReturnCodesKeysService ccrjReturnCodesKeysService) {
		this.genKeysCCRService = genKeysCCRService;
		this.setupTallyCCMService = setupTallyCCMService;
		this.ccmjElectionKeysService = ccmjElectionKeysService;
		this.ccrjReturnCodesKeysService = ccrjReturnCodesKeysService;
	}

	@Transactional
	public ControlComponentPublicKeys generateCCKeys(final GqGroup encryptionGroup, final String electionEventId,
			final ElectionEventContext electionEventContext) {
		validateUUID(electionEventId);
		checkNotNull(encryptionGroup);
		checkNotNull(electionEventContext);

		// Generate ccrj keys and save them.
		final GenKeysCCROutput genKeysCCROutput = genKeysCCRService.genKeysCCR(encryptionGroup, electionEventId, electionEventContext);
		LOGGER.info("Gen Keys CCR algorithm successfully performed. [electionEventId: {}]", electionEventId);

		final ZqElement ccrjReturnCodesGenerationSecretKey = genKeysCCROutput.ccrjReturnCodesGenerationSecretKey();
		final ElGamalMultiRecipientKeyPair ccrjChoiceReturnCodesEncryptionKeyPair = genKeysCCROutput.ccrjChoiceReturnCodesEncryptionKeyPair();

		final CcrjReturnCodesKeys newCcrjReturnCodesKeys = new CcrjReturnCodesKeys(electionEventId, ccrjReturnCodesGenerationSecretKey,
				ccrjChoiceReturnCodesEncryptionKeyPair);
		ccrjReturnCodesKeysService.save(newCcrjReturnCodesKeys);

		// Generate ccm election key pair and save it.
		final SetupTallyCCMOutput setupTallyCCMOutput = setupTallyCCMService.setupTallyCCM(encryptionGroup, electionEventContext);
		LOGGER.info("Setup Tally CCM algorithm successfully performed. [electionEventId: {}]", electionEventId);

		ccmjElectionKeysService.save(electionEventId, setupTallyCCMOutput.getCcmjElectionKeyPair());

		final GroupVector<SchnorrProof, ZqGroup> ccrjSchnorrProofs = genKeysCCROutput.ccrjSchnorrProofs();

		return new ControlComponentPublicKeys(nodeId, ccrjChoiceReturnCodesEncryptionKeyPair.getPublicKey(), ccrjSchnorrProofs,
				setupTallyCCMOutput.getCcmjElectionKeyPair().getPublicKey(), setupTallyCCMOutput.getSchnorrProofs());
	}

}
