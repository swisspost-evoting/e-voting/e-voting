/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.domain;

import static com.google.common.base.Preconditions.checkNotNull;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.MapsId;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Version;

@Entity
@Table(name = "CCM_ELECTION_KEY")
public class CcmjElectionKeysEntity {

	@Id
	private Long id;

	@OneToOne(fetch = FetchType.LAZY)
	@MapsId
	@JoinColumn(name = "ELECTION_EVENT_FK_ID")
	private ElectionEventEntity electionEventEntity;

	@Version
	private Integer changeControlId;

	private byte[] ccmjElectionKeyPair;

	public CcmjElectionKeysEntity() {
		// Needed by the repository.
	}

	public CcmjElectionKeysEntity(final byte[] ccmjElectionKeyPair, final ElectionEventEntity electionEventEntity) {
		this.ccmjElectionKeyPair = checkNotNull(ccmjElectionKeyPair);
		this.electionEventEntity = checkNotNull(electionEventEntity);
	}

	public byte[] getCcmjElectionKeyPair() {
		return ccmjElectionKeyPair;
	}
}
