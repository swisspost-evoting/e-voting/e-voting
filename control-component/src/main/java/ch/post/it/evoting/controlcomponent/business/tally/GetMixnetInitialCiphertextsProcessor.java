/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.business.tally;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.security.SignatureException;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.controlcomponent.domain.BallotBoxEntity;
import ch.post.it.evoting.controlcomponent.protocol.tally.mixonline.GetMixnetInitialCiphertextsService;
import ch.post.it.evoting.controlcomponent.service.BallotBoxService;
import ch.post.it.evoting.controlcomponent.service.ElectionEventService;
import ch.post.it.evoting.controlcomponent.service.EncryptedVerifiableVoteService;
import ch.post.it.evoting.controlcomponent.service.MixnetInitialCiphertextsService;
import ch.post.it.evoting.controlcomponent.service.SetupComponentPublicKeysService;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.domain.tally.GetMixnetInitialCiphertextsRequestPayload;
import ch.post.it.evoting.evotinglibraries.domain.common.ChannelSecurityContextData;
import ch.post.it.evoting.evotinglibraries.domain.common.EncryptedVerifiableVote;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.ControlComponentVotesHashPayload;
import ch.post.it.evoting.evotinglibraries.domain.signature.Alias;
import ch.post.it.evoting.evotinglibraries.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.tally.mixonline.GetMixnetInitialCiphertextsOutput;

@Service
public class GetMixnetInitialCiphertextsProcessor {
	private static final Logger LOGGER = LoggerFactory.getLogger(GetMixnetInitialCiphertextsProcessor.class);

	private final ObjectMapper objectMapper;
	private final BallotBoxService ballotBoxService;
	private final ElectionEventService electionEventService;
	private final SignatureKeystore<Alias> signatureKeystoreService;
	private final EncryptedVerifiableVoteService encryptedVerifiableVoteService;
	private final SetupComponentPublicKeysService setupComponentPublicKeysService;
	private final MixnetInitialCiphertextsService mixnetInitialCiphertextsService;
	private final GetMixnetInitialCiphertextsService getMixnetInitialCiphertextsService;

	@Value("${nodeID}")
	private int nodeId;

	GetMixnetInitialCiphertextsProcessor(
			final ObjectMapper objectMapper,
			final BallotBoxService ballotBoxService,
			final ElectionEventService electionEventService,
			final SignatureKeystore<Alias> signatureKeystoreService,
			final EncryptedVerifiableVoteService encryptedVerifiableVoteService,
			final SetupComponentPublicKeysService setupComponentPublicKeysService,
			final MixnetInitialCiphertextsService mixnetInitialCiphertextsService,
			final GetMixnetInitialCiphertextsService getMixnetInitialCiphertextsService) {
		this.objectMapper = objectMapper;
		this.ballotBoxService = ballotBoxService;
		this.electionEventService = electionEventService;
		this.signatureKeystoreService = signatureKeystoreService;
		this.encryptedVerifiableVoteService = encryptedVerifiableVoteService;
		this.setupComponentPublicKeysService = setupComponentPublicKeysService;
		this.mixnetInitialCiphertextsService = mixnetInitialCiphertextsService;
		this.getMixnetInitialCiphertextsService = getMixnetInitialCiphertextsService;
	}

	public ControlComponentVotesHashPayload onRequest(final GetMixnetInitialCiphertextsRequestPayload getMixnetInitialCiphertextsRequestPayload) {
		checkNotNull(getMixnetInitialCiphertextsRequestPayload);

		final String electionEventId = getMixnetInitialCiphertextsRequestPayload.electionEventId();
		final String ballotBoxId = getMixnetInitialCiphertextsRequestPayload.ballotBoxId();

		checkArgument(ballotBoxService.existsForElectionEventId(ballotBoxId, electionEventId),
				"The given election event id and ballot box id are unrelated. [electionEventId: %s, ballotBoxId: %s]", electionEventId, ballotBoxId);

		final GqGroup encryptionGroup = electionEventService.getEncryptionGroup(electionEventId);
		final BallotBoxEntity ballotBoxEntity = ballotBoxService.getBallotBox(ballotBoxId);
		final String verificationCardSetId = ballotBoxEntity.getVerificationCardSetEntity().getVerificationCardSetId();
		final List<EncryptedVerifiableVote> confirmedVotes = encryptedVerifiableVoteService.getConfirmedVotes(verificationCardSetId);
		final ElGamalMultiRecipientPublicKey electionPublicKey = setupComponentPublicKeysService.getElectionPublicKey(electionEventId);

		final GetMixnetInitialCiphertextsOutput getMixnetInitialCiphertextsOutput = getMixnetInitialCiphertextsService.getMixnetInitialCiphertexts(
				encryptionGroup, electionEventId, ballotBoxEntity, electionPublicKey, confirmedVotes);

		LOGGER.info("Mixnet initial ciphertexts retrieved. [electionEventId: {}, ballotBoxId: {}]", electionEventId, ballotBoxId);

		mixnetInitialCiphertextsService.save(electionEventId, ballotBoxId, getMixnetInitialCiphertextsOutput);

		LOGGER.info("Mixnet initial ciphertexts saved. [electionEventId: {}, ballotBoxId: {}]", electionEventId, ballotBoxId);

		final String encryptedConfirmedVotesHash = getMixnetInitialCiphertextsOutput.encryptedConfirmedVotesHash();
		final ControlComponentVotesHashPayload controlComponentVotesHashPayload = new ControlComponentVotesHashPayload(
				electionEventId, ballotBoxId, nodeId, encryptedConfirmedVotesHash);

		controlComponentVotesHashPayload.setSignature(generateSignature(controlComponentVotesHashPayload));

		return controlComponentVotesHashPayload;
	}

	public GetMixnetInitialCiphertextsRequestPayload deserializeRequest(final byte[] messageBytes) {
		checkNotNull(messageBytes);

		final byte[] messageBytesCopy = Arrays.copyOf(messageBytes, messageBytes.length);

		try {
			return objectMapper.readValue(messageBytesCopy, GetMixnetInitialCiphertextsRequestPayload.class);
		} catch (final IOException e) {
			throw new UncheckedIOException("Unable to deserialize GetMixnetInitialCiphertexts Request Payload", e);
		}
	}

	public byte[] serializeResponse(final ControlComponentVotesHashPayload controlComponentVotesHashPayload) {
		checkNotNull(controlComponentVotesHashPayload);

		try {
			return objectMapper.writeValueAsBytes(controlComponentVotesHashPayload);
		} catch (final IOException e) {
			throw new UncheckedIOException("Unable to serialize Control Component Votes Hash Payload", e);
		}

	}

	public boolean verifyRequestSignature(final GetMixnetInitialCiphertextsRequestPayload getMixnetInitialCiphertextsRequestPayload) {
		checkNotNull(getMixnetInitialCiphertextsRequestPayload);

		// The GetMixnetInitialCiphertextsRequestPayload is not signed.
		return true;
	}

	private CryptoPrimitivesSignature generateSignature(final ControlComponentVotesHashPayload controlComponentVotesHashPayload) {

		final String electionEventId = controlComponentVotesHashPayload.getElectionEventId();
		final String ballotBoxId = controlComponentVotesHashPayload.getBallotBoxId();

		final Hashable additionalContextData = ChannelSecurityContextData.controlComponentVotesHash(nodeId, electionEventId, ballotBoxId);
		try {
			final byte[] signature = signatureKeystoreService.generateSignature(controlComponentVotesHashPayload, additionalContextData);
			return new CryptoPrimitivesSignature(signature);
		} catch (final SignatureException e) {
			throw new IllegalStateException(String.format("Failed to generate Control Component Votes Hash Payload signature [%s, %s]",
					controlComponentVotesHashPayload.getClass().getSimpleName(), additionalContextData), e);
		}
	}
}
