/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.business.configuration;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.security.SignatureException;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.controlcomponent.domain.BallotBoxEntity;
import ch.post.it.evoting.controlcomponent.domain.ElectionEventState;
import ch.post.it.evoting.controlcomponent.service.BallotBoxService;
import ch.post.it.evoting.controlcomponent.service.ElectionEventStateService;
import ch.post.it.evoting.controlcomponent.service.SetupComponentPublicKeysService;
import ch.post.it.evoting.controlcomponent.service.VerificationCardService;
import ch.post.it.evoting.controlcomponent.service.VerificationCardSetService;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.domain.configuration.SetupComponentPublicKeysResponsePayload;
import ch.post.it.evoting.evotinglibraries.domain.common.ChannelSecurityContextData;
import ch.post.it.evoting.evotinglibraries.domain.election.SetupComponentPublicKeys;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.SetupComponentPublicKeysPayload;
import ch.post.it.evoting.evotinglibraries.domain.signature.Alias;
import ch.post.it.evoting.evotinglibraries.domain.signature.CryptoPrimitivesSignature;

/**
 * Consumes the messages for saving the Setup Component public keys.
 */
@Service
public class SetupComponentPublicKeysProcessor {

	public static final Logger LOGGER = LoggerFactory.getLogger(SetupComponentPublicKeysProcessor.class);

	private final BallotBoxService ballotBoxService;
	private final VerificationCardService verificationCardService;
	private final VerificationCardSetService verificationCardSetService;
	private final SignatureKeystore<Alias> signatureKeystoreService;
	private final ElectionEventStateService electionEventStateService;
	private final SetupComponentPublicKeysService setupComponentPublicKeysService;
	private final VerifySetupComponentPublicKeysService verifySetupComponentPublicKeysService;
	private final ObjectMapper objectMapper;

	@Value("${nodeID}")
	private int nodeId;

	public SetupComponentPublicKeysProcessor(
			final BallotBoxService ballotBoxService,
			final VerificationCardService verificationCardService,
			final VerificationCardSetService verificationCardSetService,
			final SignatureKeystore<Alias> signatureKeystoreService,
			final ElectionEventStateService electionEventStateService,
			final SetupComponentPublicKeysService setupComponentPublicKeysService,
			final VerifySetupComponentPublicKeysService verifySetupComponentPublicKeysService,
			final ObjectMapper objectMapper) {
		this.ballotBoxService = ballotBoxService;
		this.verificationCardService = verificationCardService;
		this.verificationCardSetService = verificationCardSetService;
		this.signatureKeystoreService = signatureKeystoreService;
		this.electionEventStateService = electionEventStateService;
		this.setupComponentPublicKeysService = setupComponentPublicKeysService;
		this.verifySetupComponentPublicKeysService = verifySetupComponentPublicKeysService;
		this.objectMapper = objectMapper;
	}

	public SetupComponentPublicKeysResponsePayload onRequest(final SetupComponentPublicKeysPayload setupComponentPublicKeysPayload) {
		checkNotNull(setupComponentPublicKeysPayload);

		verifyPayloadSignature(setupComponentPublicKeysPayload);

		// Verify number of saved verification cards.
		final String electionEventId = setupComponentPublicKeysPayload.getElectionEventId();
		verificationCardSetService.findAllByElectionEventId(electionEventId).stream()
				.parallel()
				.forEach(verificationCardSetEntity -> {
					final String verificationCardSetId = verificationCardSetEntity.getVerificationCardSetId();
					final BallotBoxEntity ballotBoxEntity = ballotBoxService.getBallotBox(verificationCardSetEntity);
					final int numberOfVerificationCards = ballotBoxEntity.getNumberOfVotingCards();
					final int savedNumberOfVerificationCards = verificationCardService.countNumberOfVotingCards(electionEventId,
							verificationCardSetId);

					checkArgument(numberOfVerificationCards == savedNumberOfVerificationCards,
							"The number of verification cards should be equal to the one saved for the given election event and verification card set id. "
									+ "[electionEventId: %s, nodeId: %s, verificationCardSetId: %s, numberOfVerificationCards: %s, savedNumberOfVerificationCards: %s]",
							electionEventId, nodeId, verificationCardSetId, numberOfVerificationCards, savedNumberOfVerificationCards);
				});

		final SetupComponentPublicKeys setupComponentPublicKeys = setupComponentPublicKeysPayload.getSetupComponentPublicKeys();

		return createSetupComponentPublicKeysResponse(electionEventId, setupComponentPublicKeys);
	}

	public boolean verifyPayloadSignature(final SetupComponentPublicKeysPayload setupComponentPublicKeysPayload) {
		final String electionEventId = setupComponentPublicKeysPayload.getElectionEventId();

		final CryptoPrimitivesSignature signature = setupComponentPublicKeysPayload.getSignature();

		checkState(signature != null, "The signature of the setup component public keys payload is null. [electionEventId: %s, nodeId: %s]",
				electionEventId, nodeId);

		final Hashable additionalContextData = ChannelSecurityContextData.setupComponentPublicKeys(electionEventId);

		final boolean isSignatureValid;
		try {
			isSignatureValid = signatureKeystoreService.verifySignature(Alias.SDM_CONFIG, setupComponentPublicKeysPayload,
					additionalContextData, signature.signatureContents());
		} catch (final SignatureException e) {
			throw new IllegalStateException(
					String.format("Could not verify the signature of the setup component public keys payload. [electionEventId: %s, nodeId: %s]",
							electionEventId, nodeId));
		}

		return isSignatureValid;
	}

	private SetupComponentPublicKeysResponsePayload createSetupComponentPublicKeysResponse(final String electionEventId,
			final SetupComponentPublicKeys setupComponentPublicKeys) {
		// Validate election event state. Implicitly checks election event existence.
		final ElectionEventState expectedState = ElectionEventState.INITIAL;
		final ElectionEventState electionEventState = electionEventStateService.getElectionEventState(electionEventId);
		checkState(expectedState.equals(electionEventState),
				"The election event is not in the expected state. [electionEventId: %s, nodeId: %s, expected: %s, actual: %s]", electionEventId,
				nodeId, expectedState, electionEventState);

		// Validate and save the Setup Component public keys.
		verifySetupComponentPublicKeysService.verifySetupComponentPublicKeys(electionEventId, setupComponentPublicKeys);
		setupComponentPublicKeysService.save(electionEventId, setupComponentPublicKeys);
		LOGGER.info("Saved setup component public keys. [electionEventId: {}, nodeId: {}]", electionEventId, nodeId);

		// Update election state to CONFIGURED.
		final ElectionEventState configuredState = ElectionEventState.CONFIGURED;
		electionEventStateService.updateElectionEventState(electionEventId, configuredState);
		LOGGER.info("Updated election event state. [electionEventId: {}, nodeId: {}, state: {}]", electionEventId, nodeId, configuredState);

		return new SetupComponentPublicKeysResponsePayload(nodeId, electionEventId);
	}

	public SetupComponentPublicKeysPayload deserializeRequest(final byte[] bytes) {
		checkNotNull(bytes);
		final byte[] bytesCopy = Arrays.copyOf(bytes, bytes.length);
		try {
			return objectMapper.readValue(bytesCopy, SetupComponentPublicKeysPayload.class);
		} catch (final IOException e) {
			throw new UncheckedIOException(e);
		}
	}

	public byte[] serializeResponse(final SetupComponentPublicKeysResponsePayload setupComponentPublicKeysResponsePayload) {
		checkNotNull(setupComponentPublicKeysResponsePayload);
		try {
			return objectMapper.writeValueAsBytes(setupComponentPublicKeysResponsePayload);
		} catch (final JsonProcessingException e) {
			throw new UncheckedIOException(e);
		}
	}
}
