/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.business.voting;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.security.SignatureException;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.controlcomponent.domain.ElectionEventState;
import ch.post.it.evoting.controlcomponent.service.ElectionEventStateService;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.domain.InvalidPayloadSignatureException;
import ch.post.it.evoting.domain.voting.sendvote.CombinedControlComponentPartialDecryptPayload;
import ch.post.it.evoting.domain.voting.sendvote.ControlComponentLCCSharePayload;
import ch.post.it.evoting.domain.voting.sendvote.ControlComponentPartialDecryptPayload;
import ch.post.it.evoting.domain.voting.sendvote.LongChoiceReturnCodesShare;
import ch.post.it.evoting.domain.voting.sendvote.PartiallyDecryptedEncryptedPCC;
import ch.post.it.evoting.evotinglibraries.domain.common.ChannelSecurityContextData;
import ch.post.it.evoting.evotinglibraries.domain.common.ContextIds;
import ch.post.it.evoting.evotinglibraries.domain.signature.Alias;
import ch.post.it.evoting.evotinglibraries.domain.signature.CryptoPrimitivesSignature;

/**
 * Consumes the messages asking for the Long Choice Return Codes Share.
 */
@Service
public class LongChoiceReturnCodesShareProcessor {

	private static final Logger LOGGER = LoggerFactory.getLogger(LongChoiceReturnCodesShareProcessor.class);

	private final ObjectMapper objectMapper;
	private final LongChoiceReturnCodesShareService longChoiceReturnCodesShareService;
	private final SignatureKeystore<Alias> signatureKeystoreService;
	private final ElectionEventStateService electionEventStateService;
	private final PartiallyDecryptedPCCService partiallyDecryptedPCCService;

	@Value("${nodeID}")
	private int nodeId;

	public LongChoiceReturnCodesShareProcessor(
			final ObjectMapper objectMapper,
			final LongChoiceReturnCodesShareService longChoiceReturnCodesShareService,
			final SignatureKeystore<Alias> signatureKeystoreService,
			final ElectionEventStateService electionEventStateService,
			final PartiallyDecryptedPCCService partiallyDecryptedPCCService) {
		this.objectMapper = objectMapper;
		this.longChoiceReturnCodesShareService = longChoiceReturnCodesShareService;
		this.electionEventStateService = electionEventStateService;
		this.signatureKeystoreService = signatureKeystoreService;
		this.partiallyDecryptedPCCService = partiallyDecryptedPCCService;
	}

	public boolean verifyPayload(final CombinedControlComponentPartialDecryptPayload combinedControlComponentPartialDecryptPayload) {
		final ContextIds contextIds = combinedControlComponentPartialDecryptPayload.controlComponentPartialDecryptPayloads().get(0)
				.getPartiallyDecryptedEncryptedPCC().contextIds();
		final String verificationCardId = contextIds.verificationCardId();

		// Verify signature of the received ControlComponentPartialDecryptPayloads.
		combinedControlComponentPartialDecryptPayload.controlComponentPartialDecryptPayloads().forEach(this::verifySignature);

		// Retrieve the partially decrypted encrypted PCC previously computed.
		final PartiallyDecryptedEncryptedPCC previouslyComputedPCCPayload = partiallyDecryptedPCCService.get(verificationCardId);

		// Get the partially decrypted encrypted PCC corresponding to this node id.
		final ControlComponentPartialDecryptPayload receivedPCCPayload = combinedControlComponentPartialDecryptPayload.controlComponentPartialDecryptPayloads()
				.stream().filter(payload -> payload.getPartiallyDecryptedEncryptedPCC().nodeId() == nodeId)
				.findAny() // Uniqueness ensured by the combined payload.
				.orElseThrow(() -> new IllegalStateException("The combined payload does not contain payload for this node id."));

		// Check that they are equal.
		if (!previouslyComputedPCCPayload.equals(receivedPCCPayload.getPartiallyDecryptedEncryptedPCC())) {
			throw new IllegalStateException("The received partially decrypted encrypted PCC is not equal to the previously computed one.");
		}

		return true;
	}

	private void verifySignature(final ControlComponentPartialDecryptPayload payload) {
		final PartiallyDecryptedEncryptedPCC partiallyDecryptedEncryptedPCC = payload.getPartiallyDecryptedEncryptedPCC();
		final ContextIds contextIds = partiallyDecryptedEncryptedPCC.contextIds();
		final int otherNodeId = partiallyDecryptedEncryptedPCC.nodeId();
		final CryptoPrimitivesSignature signature = payload.getSignature();

		checkState(signature != null, "The signature of the control component partial decrypt payload is null. [contextIds: %s]", contextIds);

		final Hashable additionalContextData = ChannelSecurityContextData.controlComponentPartialDecrypt(otherNodeId,
				contextIds.electionEventId(), contextIds.verificationCardSetId(), contextIds.verificationCardId());

		final boolean isSignatureValid;
		try {
			isSignatureValid = signatureKeystoreService.verifySignature(Alias.getControlComponentByNodeId(otherNodeId), payload,
					additionalContextData, signature.signatureContents());
		} catch (final SignatureException e) {
			throw new IllegalStateException(
					String.format("Could not verify the signature of the control component partial decrypt payload. [contextIds: %s]", contextIds));
		}

		if (!isSignatureValid) {
			throw new InvalidPayloadSignatureException(ControlComponentPartialDecryptPayload.class, String.format("[contextIds: %s]", contextIds));
		}
	}

	public ControlComponentLCCSharePayload generateControlComponentLCCSharePayload(
			final CombinedControlComponentPartialDecryptPayload combinedControlComponentPartialDecryptPayload) {
		final List<ControlComponentPartialDecryptPayload> controlComponentPartialDecryptPayloads = combinedControlComponentPartialDecryptPayload.controlComponentPartialDecryptPayloads();
		final ContextIds contextIds = controlComponentPartialDecryptPayloads.get(0).getPartiallyDecryptedEncryptedPCC().contextIds();

		// Validate election event state.
		final ElectionEventState expectedState = ElectionEventState.CONFIGURED;
		final String electionEventId = contextIds.electionEventId();
		final ElectionEventState electionEventState = electionEventStateService.getElectionEventState(electionEventId);
		checkState(expectedState.equals(electionEventState),
				"The election event is not in the expected state. [electionEventId: %s, nodeId: %s, expected: %s, actual: %s]", electionEventId,
				nodeId, expectedState, electionEventState);

		// Perform LCC share computation.
		final LongChoiceReturnCodesShare longReturnCodesShare = longChoiceReturnCodesShareService.computeLCCShares(
				controlComponentPartialDecryptPayloads);
		LOGGER.info("Successfully generated the Long Choice Return Codes Share. [contextIds: {}]", contextIds);

		// Create and sign response payload.
		final GqGroup gqGroup = controlComponentPartialDecryptPayloads.get(0).getEncryptionGroup();
		final ControlComponentLCCSharePayload controlComponentLCCSharePayload = new ControlComponentLCCSharePayload(gqGroup, longReturnCodesShare);

		controlComponentLCCSharePayload.setSignature(getPayloadSignature(controlComponentLCCSharePayload));
		LOGGER.info("Successfully signed Long Return Codes Share payload. [contextIds: {}]", contextIds);

		return controlComponentLCCSharePayload;
	}

	private CryptoPrimitivesSignature getPayloadSignature(final ControlComponentLCCSharePayload payload) {
		final LongChoiceReturnCodesShare longChoiceReturnCodesShare = payload.getLongChoiceReturnCodesShare();
		final String electionEventId = longChoiceReturnCodesShare.electionEventId();
		final String verificationCardSetId = longChoiceReturnCodesShare.verificationCardSetId();
		final String verificationCardId = longChoiceReturnCodesShare.verificationCardId();

		final Hashable additionalContextData = ChannelSecurityContextData.controlComponentLCCShare(nodeId, electionEventId, verificationCardSetId,
				verificationCardId);

		try {
			final byte[] signature = signatureKeystoreService.generateSignature(payload, additionalContextData);
			return new CryptoPrimitivesSignature(signature);
		} catch (final SignatureException e) {
			throw new IllegalStateException(
					String.format("Could not generate payload signature. [electionEventId: %s, verificationCardSetId: %s, verificationCardId: %s]",
							electionEventId,
							verificationCardSetId, verificationCardId));
		}
	}

	public CombinedControlComponentPartialDecryptPayload deserializeRequest(final byte[] bytes) {
		checkNotNull(bytes);
		final byte[] bytesCopy = Arrays.copyOf(bytes, bytes.length);
		try {
			return objectMapper.readValue(bytesCopy, CombinedControlComponentPartialDecryptPayload.class);
		} catch (final IOException e) {
			throw new UncheckedIOException(e);
		}
	}

	public byte[] serializeResponse(final ControlComponentLCCSharePayload controlComponentLCCSharePayload) {
		checkNotNull(controlComponentLCCSharePayload);
		try {
			return objectMapper.writeValueAsBytes(controlComponentLCCSharePayload);
		} catch (final JsonProcessingException e) {
			throw new UncheckedIOException(e);
		}
	}
}
