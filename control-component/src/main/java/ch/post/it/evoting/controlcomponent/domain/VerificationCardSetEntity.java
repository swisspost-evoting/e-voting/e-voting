/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.domain;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateNonBlankUCS;
import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateXsToken;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Objects;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import jakarta.persistence.Version;

@Entity
@Table(name = "VERIFICATION_CARD_SET")
public class VerificationCardSetEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "VERIFICATION_CARD_SET_SEQ_GENERATOR")
	@SequenceGenerator(sequenceName = "VERIFICATION_CARD_SET_SEQ", allocationSize = 1, name = "VERIFICATION_CARD_SET_SEQ_GENERATOR")
	private Long id;

	private String verificationCardSetId;
	private String verificationCardSetAlias;
	private String verificationCardSetDescription;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ELECTION_EVENT_FK_ID", referencedColumnName = "ID")
	private ElectionEventEntity electionEventEntity;

	@Version
	private Integer changeControlId;

	public VerificationCardSetEntity() {
	}

	private VerificationCardSetEntity(final String verificationCardSetId, final String verificationCardSetAlias,
			final String verificationCardSetDescription, final ElectionEventEntity electionEventEntity) {

		this.verificationCardSetId = validateUUID(verificationCardSetId);
		this.verificationCardSetAlias = validateXsToken(verificationCardSetAlias);
		this.verificationCardSetDescription = validateNonBlankUCS(verificationCardSetDescription);
		this.electionEventEntity = checkNotNull(electionEventEntity);
	}

	public String getVerificationCardSetId() {
		return verificationCardSetId;
	}

	public String getVerificationCardSetAlias() {
		return verificationCardSetAlias;
	}

	public String getVerificationCardSetDescription() {
		return verificationCardSetDescription;
	}

	public ElectionEventEntity getElectionEventEntity() {
		return electionEventEntity;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		final VerificationCardSetEntity that = (VerificationCardSetEntity) o;
		return Objects.equals(id, that.id) && Objects.equals(verificationCardSetId, that.verificationCardSetId)
				&& Objects.equals(verificationCardSetAlias, that.verificationCardSetAlias) && Objects.equals(
				verificationCardSetDescription, that.verificationCardSetDescription) && Objects.equals(electionEventEntity,
				that.electionEventEntity) && Objects.equals(changeControlId, that.changeControlId);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, verificationCardSetId, verificationCardSetAlias, verificationCardSetDescription, electionEventEntity,
				changeControlId);
	}

	public static class Builder {

		private String verificationCardSetId;
		private String verificationCardSetAlias;
		private String verificationCardSetDescription;
		private ElectionEventEntity electionEventEntity;

		public Builder() {
			// Do nothing
		}

		public Builder setVerificationCardSetId(final String verificationCardSetId) {
			this.verificationCardSetId = verificationCardSetId;
			return this;
		}

		public Builder setVerificationCardSetAlias(final String verificationCardSetAlias) {
			this.verificationCardSetAlias = verificationCardSetAlias;
			return this;
		}

		public Builder setVerificationCardSetDescription(final String verificationCardSetDescription) {
			this.verificationCardSetDescription = verificationCardSetDescription;
			return this;
		}

		public Builder setElectionEventEntity(final ElectionEventEntity electionEventEntity) {
			this.electionEventEntity = electionEventEntity;
			return this;
		}

		public VerificationCardSetEntity build() {
			return new VerificationCardSetEntity(verificationCardSetId, verificationCardSetAlias, verificationCardSetDescription,
					electionEventEntity);
		}
	}
}
