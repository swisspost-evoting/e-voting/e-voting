/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.service;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ch.post.it.evoting.controlcomponent.domain.BallotBoxEntity;
import ch.post.it.evoting.controlcomponent.domain.VerificationCardSetEntity;
import ch.post.it.evoting.controlcomponent.repository.BallotBoxRepository;
import ch.post.it.evoting.evotinglibraries.domain.election.PrimesMappingTable;
import ch.post.it.evoting.evotinglibraries.domain.election.VerificationCardSetContext;
import ch.post.it.evoting.evotinglibraries.domain.validations.GracePeriodValidation;

@Service
public class BallotBoxService {

	private final BallotBoxRepository ballotBoxRepository;
	private final VerificationCardSetService verificationCardSetService;

	public BallotBoxService(final BallotBoxRepository ballotBoxRepository,
			final VerificationCardSetService verificationCardSetService) {
		this.ballotBoxRepository = ballotBoxRepository;
		this.verificationCardSetService = verificationCardSetService;
	}

	@Transactional
	public BallotBoxEntity save(final String ballotBoxId, final String verificationCardSetId, final LocalDateTime ballotBoxStartTime,
			final LocalDateTime ballotBoxFinishTime, final boolean testBallotBox, final int numberOfVotingCards, final int gracePeriod,
			final PrimesMappingTable primesMappingTable) {
		validateUUID(ballotBoxId);
		validateUUID(verificationCardSetId);
		checkNotNull(ballotBoxStartTime);
		checkNotNull(ballotBoxFinishTime);
		checkArgument(ballotBoxStartTime.isBefore(ballotBoxFinishTime) || ballotBoxStartTime.equals(ballotBoxFinishTime),
				"Start time must be before finish time.");
		checkArgument(numberOfVotingCards > 0);
		GracePeriodValidation.validate(gracePeriod);
		checkNotNull(primesMappingTable);

		final VerificationCardSetEntity verificationCardSetEntity = verificationCardSetService.getVerificationCardSet(verificationCardSetId);
		final BallotBoxEntity ballotBoxEntity = new BallotBoxEntity.Builder()
				.setBallotBoxId(ballotBoxId)
				.setVerificationCardSetEntity(verificationCardSetEntity)
				.setBallotBoxStartTime(ballotBoxStartTime)
				.setBallotBoxFinishTime(ballotBoxFinishTime)
				.setTestBallotBox(testBallotBox)
				.setNumberOfVotingCards(numberOfVotingCards)
				.setGracePeriod(gracePeriod)
				.setPrimesMappingTable(primesMappingTable)
				.build();

		return ballotBoxRepository.save(ballotBoxEntity);
	}

	@Transactional
	public void saveFromContexts(final List<VerificationCardSetContext> verificationCardSetContexts) {
		checkNotNull(verificationCardSetContexts);

		final List<BallotBoxEntity> ballotBoxEntities = verificationCardSetContexts.stream()
				.map(verificationCardSetContext -> {
					final String verificationCardSetId = verificationCardSetContext.getVerificationCardSetId();
					final VerificationCardSetEntity verificationCardSetEntity = verificationCardSetService.getVerificationCardSet(
							verificationCardSetId);

					return new BallotBoxEntity.Builder()
							.setBallotBoxId(verificationCardSetContext.getBallotBoxId())
							.setVerificationCardSetEntity(verificationCardSetEntity)
							.setBallotBoxStartTime(verificationCardSetContext.getBallotBoxStartTime())
							.setBallotBoxFinishTime(verificationCardSetContext.getBallotBoxFinishTime())
							.setTestBallotBox(verificationCardSetContext.isTestBallotBox())
							.setNumberOfVotingCards(verificationCardSetContext.getNumberOfVotingCards())
							.setGracePeriod(verificationCardSetContext.getGracePeriod())
							.setPrimesMappingTable(verificationCardSetContext.getPrimesMappingTable())
							.build();
				}).toList();

		ballotBoxRepository.saveAll(ballotBoxEntities);
	}

	@Transactional
	public boolean existsForElectionEventId(final String ballotBoxId, final String electionEventId) {
		validateUUID(ballotBoxId);
		validateUUID(electionEventId);

		if (!ballotBoxRepository.existsByBallotBoxId(ballotBoxId)) {
			return false;
		}
		final String ballotBoxElectionEventId = getBallotBox(ballotBoxId).getVerificationCardSetEntity().getElectionEventEntity()
				.getElectionEventId();
		return electionEventId.equals(ballotBoxElectionEventId);
	}

	@Transactional
	public BallotBoxEntity getBallotBox(final String ballotBoxId) {
		validateUUID(ballotBoxId);

		return ballotBoxRepository.findByBallotBoxId(ballotBoxId)
				.orElseThrow(() -> new IllegalStateException(String.format("Ballot box not found. [ballotBoxId: %s]", ballotBoxId)));
	}

	@Transactional
	public BallotBoxEntity getBallotBox(final VerificationCardSetEntity verificationCardSetEntity) {
		checkNotNull(verificationCardSetEntity);

		return ballotBoxRepository.findByVerificationCardSetEntity(verificationCardSetEntity)
				.orElseThrow(() -> new IllegalStateException(
						String.format("Ballot box not found. [verificationCardSetId: %s]", verificationCardSetEntity.getVerificationCardSetId())));
	}

	@Transactional
	public boolean isMixed(final String ballotBoxId) {
		validateUUID(ballotBoxId);

		final BallotBoxEntity ballotBoxEntity = getBallotBox(ballotBoxId);
		return ballotBoxEntity.isMixed();
	}

	@Transactional
	public BallotBoxEntity setMixed(final String ballotBoxId) {
		validateUUID(ballotBoxId);

		final BallotBoxEntity ballotBoxEntity = getBallotBox(ballotBoxId);
		ballotBoxEntity.setMixed(true);

		return ballotBoxRepository.save(ballotBoxEntity);
	}

	@Transactional
	public PrimesMappingTable getBallotBoxPrimesMappingTable(final VerificationCardSetEntity verificationCardSetEntity) {
		checkNotNull(verificationCardSetEntity);
		final BallotBoxEntity ballotBoxEntity = getBallotBox(verificationCardSetEntity);
		return ballotBoxEntity.getPrimesMappingTable();
	}

}
