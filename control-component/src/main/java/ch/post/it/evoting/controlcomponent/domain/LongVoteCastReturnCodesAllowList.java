/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.domain;

public interface LongVoteCastReturnCodesAllowList {

	boolean exists(final String longVoteCastReturnCode);
}
