/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.service;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.controlcomponent.domain.MixnetInitialCiphertextsEntity;
import ch.post.it.evoting.controlcomponent.repository.MixnetInitialCiphertextsRepository;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.tally.mixonline.GetMixnetInitialCiphertextsOutput;

@Service
public class MixnetInitialCiphertextsService {
	private static final Logger LOGGER = LoggerFactory.getLogger(MixnetInitialCiphertextsService.class);

	private static final String GROUP = "group";

	private final ObjectMapper objectMapper;
	private final ElectionEventService electionEventService;
	private final MixnetInitialCiphertextsRepository mixnetInitialCiphertextsRepository;

	public MixnetInitialCiphertextsService(
			final ObjectMapper objectMapper,
			final ElectionEventService electionEventService,
			final MixnetInitialCiphertextsRepository mixnetInitialCiphertextsRepository) {
		this.objectMapper = objectMapper;
		this.electionEventService = electionEventService;
		this.mixnetInitialCiphertextsRepository = mixnetInitialCiphertextsRepository;
	}

	@Transactional
	public void save(final String electionEventId, final String ballotBoxId,
			final GetMixnetInitialCiphertextsOutput getMixnetInitialCiphertextsOutput) {
		validateUUID(electionEventId);
		validateUUID(ballotBoxId);
		checkNotNull(getMixnetInitialCiphertextsOutput);

		final byte[] serializedMixnetInitialCiphertexts;
		try {
			serializedMixnetInitialCiphertexts = objectMapper.writeValueAsBytes(getMixnetInitialCiphertextsOutput.mixnetInitialCiphertexts());
		} catch (final JsonProcessingException e) {
			throw new UncheckedIOException(
					String.format("Failed to serialize mixnet initial ciphertexts. [electionEventId: %s, ballotBoxId: %s]", electionEventId,
							ballotBoxId), e);
		}

		final MixnetInitialCiphertextsEntity mixnetInitialCiphertextsEntity = new MixnetInitialCiphertextsEntity(electionEventId, ballotBoxId,
				getMixnetInitialCiphertextsOutput.encryptedConfirmedVotesHash(), serializedMixnetInitialCiphertexts);

		mixnetInitialCiphertextsRepository.save(mixnetInitialCiphertextsEntity);

		LOGGER.debug("Mixnet initial ciphertexts entity saved. [electionEventId: {}, ballotBoxId: {}]", electionEventId, ballotBoxId);
	}

	@Transactional
	public GetMixnetInitialCiphertextsOutput getMixnetInitialCiphertextsOutput(final String electionEventId, final String ballotBoxId) {
		validateUUID(electionEventId);
		validateUUID(ballotBoxId);

		final MixnetInitialCiphertextsEntity mixnetInitialCiphertextsEntity = mixnetInitialCiphertextsRepository.findByElectionEventIdAndBallotBoxId(
						electionEventId, ballotBoxId)
				.orElseThrow(() -> new IllegalStateException(
						String.format("Mixnet initial ciphertexts entity not found. [electionEventId: %s, ballotBoxId: %s]", electionEventId,
								ballotBoxId)));

		LOGGER.debug("Mixnet initial ciphertexts entity retrieved. [electionEventId: {}, ballotBoxId: {}]", electionEventId, ballotBoxId);

		final GqGroup encryptionGroup = electionEventService.getEncryptionGroup(electionEventId);
		final ElGamalMultiRecipientCiphertext[] mixnetInitialCiphertexts;
		try {
			mixnetInitialCiphertexts = objectMapper.reader()
					.withAttribute(GROUP, encryptionGroup)
					.readValue(mixnetInitialCiphertextsEntity.getMixnetInitialCiphertexts(), ElGamalMultiRecipientCiphertext[].class);
		} catch (final IOException e) {
			throw new UncheckedIOException(
					String.format("Failed to deserialize mixnet initial ciphertexts. [electionEventId: %s, ballotBoxId: %s]", electionEventId,
							ballotBoxId), e);
		}

		LOGGER.debug("Mixnet initial ciphertexts entity deserialized. [electionEventId: {}, ballotBoxId: {}]", electionEventId, ballotBoxId);

		return new GetMixnetInitialCiphertextsOutput(mixnetInitialCiphertextsEntity.getEncryptedConfirmedVotesHash(),
				GroupVector.from(Arrays.asList(mixnetInitialCiphertexts)));
	}

}
