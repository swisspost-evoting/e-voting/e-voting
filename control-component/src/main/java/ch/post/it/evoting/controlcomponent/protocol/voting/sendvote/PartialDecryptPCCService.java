/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.protocol.voting.sendvote;

import static ch.post.it.evoting.cryptoprimitives.utils.Validations.allEqual;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.function.Function;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.controlcomponent.service.CcrjReturnCodesKeysService;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientKeyPair;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.evotinglibraries.domain.common.ContextIds;
import ch.post.it.evoting.evotinglibraries.domain.common.EncryptedVerifiableVote;
import ch.post.it.evoting.evotinglibraries.domain.election.PrimesMappingTable;

@Service
public class PartialDecryptPCCService {

	private static final Logger LOGGER = LoggerFactory.getLogger(PartialDecryptPCCService.class);

	private final PartialDecryptPCCAlgorithm partialDecryptPCCAlgorithm;
	private final CcrjReturnCodesKeysService ccrjReturnCodesKeysService;

	@Value("${nodeID}")
	private int nodeId;

	public PartialDecryptPCCService(final PartialDecryptPCCAlgorithm partialDecryptPCCAlgorithm,
			final CcrjReturnCodesKeysService ccrjReturnCodesKeysService) {
		this.partialDecryptPCCAlgorithm = partialDecryptPCCAlgorithm;
		this.ccrjReturnCodesKeysService = ccrjReturnCodesKeysService;
	}

	/**
	 * Invokes the PartialDecryptPCC algorithm.
	 *
	 * @param encryptionGroup                      the encryption group. Must be non-null.
	 * @param primesMappingTable                   the primes mapping table. Must be non-null.
	 * @param electionPublicKey                    the election public key. Must be non-null.
	 * @param choiceReturnCodesEncryptionPublicKey the choice return codes encryption public key. Must be non-null.
	 * @param encryptedVerifiableVote              the encrypted vote. Must be non-null.
	 * @throws NullPointerException     if any parameter is null.
	 * @throws IllegalArgumentException if the inputs have different encryption groups.
	 */
	public PartialDecryptPCCOutput partialDecryptPCC(final GqGroup encryptionGroup, final PrimesMappingTable primesMappingTable,
			final ElGamalMultiRecipientPublicKey electionPublicKey, final ElGamalMultiRecipientPublicKey choiceReturnCodesEncryptionPublicKey,
			final EncryptedVerifiableVote encryptedVerifiableVote) {
		checkNotNull(encryptionGroup);
		checkNotNull(primesMappingTable);
		checkNotNull(electionPublicKey);
		checkNotNull(choiceReturnCodesEncryptionPublicKey);
		checkNotNull(encryptedVerifiableVote);
		checkArgument(allEqual(
				Stream.of(encryptionGroup, primesMappingTable.getEncryptionGroup(), electionPublicKey.getGroup(),
						choiceReturnCodesEncryptionPublicKey.getGroup(), encryptedVerifiableVote.encryptedVote().getGroup()),
				Function.identity()));

		final ContextIds contextIds = encryptedVerifiableVote.contextIds();
		final String electionEventId = contextIds.electionEventId();
		final String verificationCardSetId = contextIds.verificationCardSetId();
		final String verificationCardId = contextIds.verificationCardId();

		final ElGamalMultiRecipientKeyPair ccrjChoiceReturnCodesEncryptionKeyPair = ccrjReturnCodesKeysService.getCcrjReturnCodesKeys(electionEventId)
				.ccrjChoiceReturnCodesEncryptionKeyPair();

		// Perform partial decryption of the encrypted partial Choice Return codes.
		final DecryptPCCContext decryptPCCContext = new DecryptPCCContext.Builder()
				.setEncryptionGroup(encryptionGroup)
				.setNodeId(nodeId)
				.setElectionEventId(electionEventId)
				.setVerificationCardSetId(verificationCardSetId)
				.setVerificationCardId(verificationCardId)
				.setPrimesMappingTable(primesMappingTable)
				.setElectionPublicKey(electionPublicKey)
				.setChoiceReturnCodesEncryptionPublicKey(choiceReturnCodesEncryptionPublicKey)
				.build();

		final PartialDecryptPCCInput partialDecryptPCCInput = new PartialDecryptPCCInput.Builder()
				.setEncryptedVote(encryptedVerifiableVote.encryptedVote())
				.setExponentiatedEncryptedVote(encryptedVerifiableVote.exponentiatedEncryptedVote())
				.setEncryptedPartialChoiceReturnCodes(encryptedVerifiableVote.encryptedPartialChoiceReturnCodes())
				.setCcrjChoiceReturnCodesEncryptionPublicKey(ccrjChoiceReturnCodesEncryptionKeyPair.getPublicKey())
				.setCcrjChoiceReturnCodesEncryptionSecretKey(ccrjChoiceReturnCodesEncryptionKeyPair.getPrivateKey())
				.build();

		LOGGER.debug("Performing Partial Decrypt PCC algorithm... [contextIds: {}, nodeId: {}]", contextIds, nodeId);

		return partialDecryptPCCAlgorithm.partialDecryptPCC(decryptPCCContext, partialDecryptPCCInput);
	}
}
