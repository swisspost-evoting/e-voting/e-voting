/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.domain;

import static com.google.common.base.Preconditions.checkNotNull;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.MapsId;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Version;

@Entity
@Table(name = "ELECTION_EVENT_STATE")
public class ElectionEventStateEntity {

	@Id
	private Long id;

	@OneToOne(fetch = FetchType.LAZY)
	@MapsId
	@JoinColumn(name = "ELECTION_EVENT_FK_ID")
	private ElectionEventEntity electionEventEntity;

	private ElectionEventState state = ElectionEventState.INITIAL;

	@Version
	private Integer changeControlId;

	public ElectionEventStateEntity() {
	}

	public ElectionEventStateEntity(final ElectionEventEntity electionEventEntity) {
		this.electionEventEntity = checkNotNull(electionEventEntity);
	}

	public ElectionEventState getState() {
		return state;
	}

	public void setState(final ElectionEventState state) {
		this.state = checkNotNull(state);
	}

}
