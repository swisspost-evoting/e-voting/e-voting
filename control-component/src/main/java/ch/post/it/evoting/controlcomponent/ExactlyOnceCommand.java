/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.concurrent.Callable;
import java.util.function.Function;

public class ExactlyOnceCommand<U> {

	private final String correlationId;
	private final String contextId;
	private final String context;
	private final Callable<U> task;
	private final Function<U, byte[]> serializer;
	private final byte[] requestContent;

	private ExactlyOnceCommand(final String correlationId, final String contextId, final String context, final Callable<U> task,
			final Function<U, byte[]> serializer, final byte[] requestContent) {
		this.correlationId = correlationId;
		this.contextId = contextId;
		this.context = context;
		this.task = task;
		this.serializer = serializer;
		this.requestContent = requestContent;
	}

	public String getCorrelationId() {
		return correlationId;
	}

	public String getContextId() {
		return contextId;
	}

	public String getContext() {
		return context;
	}

	public Callable<U> getTask() {
		return task;
	}

	public byte[] getRequestContent() {
		return requestContent;
	}

	public Function<U, byte[]> getSerializer() {
		return serializer;
	}

	public static class Builder<U> {
		private String correlationId;
		private String contextId;
		private String context;
		private Callable<U> task;

		private Function<U, byte[]> serializer;
		private byte[] requestContent;

		/**
		 * Sets the correlation id which is identical for all subtask of a business operation execution.
		 * <p>
		 * The combination of correlation id, context id and context uniquely identify this task.
		 * </p>
		 *
		 * @param correlationId The correlation id to be used for building the ExactlyOnceCommand.
		 * @return this Builder with the correlationId set.
		 */
		public Builder<U> setCorrelationId(final String correlationId) {
			this.correlationId = correlationId;
			return this;
		}

		/**
		 * Sets the context id which uniquely identifies the resource being operated on.
		 * <p>
		 * The combination of correlation id, context id and context uniquely identify this task.
		 * </p>
		 *
		 * @param contextId The context id to be used for building the ExactlyOnceCommand.
		 * @return this Builder with the contextId set.
		 */
		public Builder<U> setContextId(final String contextId) {
			this.contextId = contextId;
			return this;
		}

		/**
		 * Sets the context which identifies the business operation being executed.
		 * <p>
		 * The combination of correlation id, context id and context uniquely identify this task.
		 * </p>
		 *
		 * @param context The context to be used for building the ExactlyOnceCommand.
		 * @return this Builder with the context set.
		 */
		public Builder<U> setContext(final String context) {
			this.context = context;
			return this;
		}

		/**
		 * Sets the task that is guaranteed to be successfully processed exactly once. If the task fails, it is guaranteed not to be persisted.
		 *
		 * @param task The task to be executed in the ExactlyOnceCommand.
		 * @return the Builder with the task set.
		 */
		public Builder<U> setTask(final Callable<U> task) {
			this.task = task;
			return this;
		}

		/**
		 * Sets the request content which is the input to the process.
		 *
		 * @param requestContent The message to be processed in the ExactlyOnceCommand.
		 * @return the Builder with the message set.
		 */
		public Builder<U> setRequestContent(final byte[] requestContent) {
			this.requestContent = requestContent;
			return this;
		}

		public Builder<U> setSerializer(final Function<U, byte[]> serializer) {
			this.serializer = serializer;
			return this;
		}

		/**
		 * Instantiates an ExactlyOnceCommand with the fields set according to the Builder's fields.
		 *
		 * @return an ExactlyOnceCommand.
		 * @throws NullPointerException if any of the Builder's fields is null
		 */
		public ExactlyOnceCommand<U> build() {
			checkNotNull(correlationId);
			checkNotNull(contextId);
			checkNotNull(context);
			checkNotNull(task);
			checkNotNull(requestContent);
			checkNotNull(serializer);

			return new ExactlyOnceCommand<>(correlationId, contextId, context, task, serializer, requestContent);
		}
	}
}
