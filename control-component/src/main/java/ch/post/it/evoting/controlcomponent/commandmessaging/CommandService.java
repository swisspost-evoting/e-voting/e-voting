/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.commandmessaging;

import static com.google.common.base.Preconditions.checkNotNull;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class CommandService {

	private final CommandRepository commandRepository;

	public CommandService(final CommandRepository commandRepository) {
		this.commandRepository = commandRepository;
	}

	public CommandEntity save(final CommandId commandId, final byte[] requestPayload, final LocalDateTime requestDateTime,
			final byte[] responsePayload,
			final LocalDateTime responseDateTime) {
		checkNotNull(commandId);
		checkNotNull(requestPayload);
		checkNotNull(responsePayload);

		final CommandEntity commandEntity = CommandEntity.builder()
				.commandId(commandId)
				.requestPayload(requestPayload)
				.requestDateTime(requestDateTime)
				.responsePayload(responsePayload)
				.responseDateTime(responseDateTime)
				.build();
		return commandRepository.save(commandEntity);
	}

	public List<CommandEntity> findAllCommandsWithCorrelationId(final String correlationId) {
		checkNotNull(correlationId);

		return commandRepository.findAllByCorrelationId(correlationId);
	}

	public Optional<CommandEntity> findIdenticalCommand(final CommandId commandId) {
		checkNotNull(commandId);

		return commandRepository.findById(commandId);
	}

	public List<CommandEntity> findSemanticallyIdenticalCommand(final CommandId commandId) {
		checkNotNull(commandId);

		final String contextId = commandId.getContextId();
		final String context = commandId.getContext();
		final Integer nodeId = commandId.getNodeId();

		return commandRepository.findAllByContextIdAndContextAndNodeId(contextId, context, nodeId);
	}
}
