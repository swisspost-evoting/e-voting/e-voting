/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.protocol.voting.sendvote;

import static ch.post.it.evoting.evotinglibraries.domain.ControlComponentConstants.NODE_IDS;
import static ch.post.it.evoting.evotinglibraries.domain.VotingOptionsConstants.MAXIMUM_SUPPORTED_NUMBER_OF_SELECTIONS;
import static ch.post.it.evoting.evotinglibraries.domain.VotingOptionsConstants.MAXIMUM_SUPPORTED_NUMBER_OF_WRITE_INS;
import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;
import java.util.Optional;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.evotinglibraries.domain.election.PrimesMappingTable;

/**
 * Regroups the context values needed by the DecryptPCC and PartialDecryptPCC algorithms.
 *
 * <ul>
 *     <li>(p, q, g), the {@code GqGroup} with modulus p, cardinality q and generator g. Not null.</li>
 *     <li>j, the CCR's index. In range [1, 4].</li>
 *     <li>j&#770;, the other CCR's indeces. Not null and not empty.</li>
 *     <li>ee, the election event id. Not null and a valid UUID.</li>
 *     <li>vcs, the verification card set id. Not null and a valid UUID.</li>
 *     <li>vc<sub>id</sub>, the verification card id. Not null and a valid UUID.</li>
 *     <li>pTable, the primes mapping table of size n. Not null.</li>
 *     <li>EL<sub>pk</sub>, the election public key of size &delta;<sub>max</sub>. Not null.</li>
 *     <li>pk<sub>CCR</sub>, the choice return codes encryption public key of size &psi;<sub>max</sub>. Not null.</li>
 *     <li>(pk<sub>CCR_j&#770;_1</sub>, pk<sub>CCR_j&#770;_2</sub>, pk<sub>CCR_j&#770;_3</sub>), the other CCR's Choice Return Codes encryption keys. Not null.</li>
 * </ul>
 */
public class DecryptPCCContext {

	private final GqGroup encryptionGroup;
	private final int nodeId;
	private final List<Integer> otherNodeIds;
	private final String electionEventId;
	private final String verificationCardSetId;
	private final String verificationCardId;
	private final PrimesMappingTable primesMappingTable;
	private final ElGamalMultiRecipientPublicKey electionPublicKey;
	private final ElGamalMultiRecipientPublicKey choiceReturnCodesEncryptionPublicKey;
	private final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> otherCcrChoiceReturnCodesEncryptionKeys;

	private DecryptPCCContext(final GqGroup encryptionGroup, final int nodeId, final List<Integer> otherNodeIds, final String electionEventId,
			final String verificationCardSetId, final String verificationCardId, final PrimesMappingTable primesMappingTable,
			final ElGamalMultiRecipientPublicKey electionPublicKey, final ElGamalMultiRecipientPublicKey choiceReturnCodesEncryptionPublicKey,
			final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> otherCcrChoiceReturnCodesEncryptionKeys) {
		this.encryptionGroup = encryptionGroup;
		this.nodeId = nodeId;
		this.otherNodeIds = otherNodeIds;
		this.electionEventId = electionEventId;
		this.verificationCardSetId = verificationCardSetId;
		this.verificationCardId = verificationCardId;
		this.primesMappingTable = primesMappingTable;
		this.electionPublicKey = electionPublicKey;
		this.choiceReturnCodesEncryptionPublicKey = choiceReturnCodesEncryptionPublicKey;
		this.otherCcrChoiceReturnCodesEncryptionKeys = otherCcrChoiceReturnCodesEncryptionKeys;
	}

	public GqGroup getEncryptionGroup() {
		return encryptionGroup;
	}

	public int getNodeId() {
		return nodeId;
	}

	public List<Integer> getOtherNodeIds() {
		return List.copyOf(otherNodeIds);
	}

	public String getElectionEventId() {
		return electionEventId;
	}

	public String getVerificationCardSetId() {
		return verificationCardSetId;
	}

	public String getVerificationCardId() {
		return verificationCardId;
	}

	public PrimesMappingTable getPrimesMappingTable() {
		return primesMappingTable;
	}

	public ElGamalMultiRecipientPublicKey getElectionPublicKey() {
		return electionPublicKey;
	}

	public ElGamalMultiRecipientPublicKey getChoiceReturnCodesEncryptionPublicKey() {
		return choiceReturnCodesEncryptionPublicKey;
	}

	public Optional<GroupVector<ElGamalMultiRecipientPublicKey, GqGroup>> getOtherCcrChoiceReturnCodesEncryptionKeys() {
		return Optional.ofNullable(otherCcrChoiceReturnCodesEncryptionKeys);
	}

	/**
	 * Builder performing input validations and cross-validations before constructing a {@link DecryptPCCContext}.
	 */
	public static class Builder {

		private GqGroup encryptionGroup;
		private int nodeId;
		private String electionEventId;
		private String verificationCardSetId;
		private String verificationCardId;
		private PrimesMappingTable primesMappingTable;
		private ElGamalMultiRecipientPublicKey electionPublicKey;
		private ElGamalMultiRecipientPublicKey choiceReturnCodesEncryptionPublicKey;
		private GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> otherCcrChoiceReturnCodesEncryptionKeys;

		public Builder setEncryptionGroup(final GqGroup encryptionGroup) {
			this.encryptionGroup = encryptionGroup;
			return this;
		}

		public Builder setNodeId(final int nodeId) {
			this.nodeId = nodeId;
			return this;
		}

		public Builder setElectionEventId(final String electionEventId) {
			this.electionEventId = electionEventId;
			return this;
		}

		public Builder setVerificationCardSetId(final String verificationCardSetId) {
			this.verificationCardSetId = verificationCardSetId;
			return this;
		}

		public Builder setVerificationCardId(final String verificationCardId) {
			this.verificationCardId = verificationCardId;
			return this;
		}

		public Builder setPrimesMappingTable(final PrimesMappingTable primesMappingTable) {
			this.primesMappingTable = primesMappingTable;
			return this;
		}

		public Builder setElectionPublicKey(final ElGamalMultiRecipientPublicKey electionPublicKey) {
			this.electionPublicKey = electionPublicKey;
			return this;
		}

		public Builder setChoiceReturnCodesEncryptionPublicKey(final ElGamalMultiRecipientPublicKey choiceReturnCodesEncryptionPublicKey) {
			this.choiceReturnCodesEncryptionPublicKey = choiceReturnCodesEncryptionPublicKey;
			return this;
		}

		public Builder setOtherCcrChoiceReturnCodesEncryptionKeys(
				final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> otherCcrChoiceReturnCodesEncryptionKeys) {
			this.otherCcrChoiceReturnCodesEncryptionKeys = otherCcrChoiceReturnCodesEncryptionKeys;
			return this;
		}

		public DecryptPCCContext build() {
			checkNotNull(encryptionGroup);
			checkArgument(NODE_IDS.contains(nodeId), "The node id must be part of the known node ids. [nodeId: %s]", nodeId);
			validateUUID(electionEventId);
			validateUUID(verificationCardSetId);
			validateUUID(verificationCardId);
			checkNotNull(electionPublicKey);
			checkNotNull(choiceReturnCodesEncryptionPublicKey);

			checkArgument(primesMappingTable.getEncryptionGroup().equals(encryptionGroup),
					"The primes mapping table's group must be equal to the encryption group.");

			final int delta_sup = MAXIMUM_SUPPORTED_NUMBER_OF_WRITE_INS + 1;
			checkArgument(electionPublicKey.size() <= delta_sup,
					"The election public key must not have more elements than the maximum supported number of write-ins + 1. [delta_max: %s, delta_sup: %s]",
					electionPublicKey.size(), delta_sup);

			checkArgument(choiceReturnCodesEncryptionPublicKey.size() <= MAXIMUM_SUPPORTED_NUMBER_OF_SELECTIONS,
					"The choice return codes encryption public key size must be smaller than or equal to the maximum supported number of selections. [psi_max: %s, psi_sup: %s]",
					choiceReturnCodesEncryptionPublicKey.size(), MAXIMUM_SUPPORTED_NUMBER_OF_SELECTIONS);

			final List<Integer> otherNodeIds = NODE_IDS.stream().filter(j -> !j.equals(nodeId)).toList();
			checkArgument(otherNodeIds.size() == NODE_IDS.size() - 1,
					"The size of the other node ids must be equal to the number of known node ids - 1.");

			checkArgument(otherCcrChoiceReturnCodesEncryptionKeys == null || otherCcrChoiceReturnCodesEncryptionKeys.size() == 3,
					"There must be exactly 3 vectors of other CCR's Choice Return Codes encryption keys.");
			checkArgument(otherCcrChoiceReturnCodesEncryptionKeys == null || otherCcrChoiceReturnCodesEncryptionKeys.allEqual(
							ElGamalMultiRecipientPublicKey::size),
					"All other CCR's Choice Return Codes encryption keys must have the same size.");

			return new DecryptPCCContext(encryptionGroup, nodeId, otherNodeIds, electionEventId, verificationCardSetId, verificationCardId,
					primesMappingTable, electionPublicKey, choiceReturnCodesEncryptionPublicKey, otherCcrChoiceReturnCodesEncryptionKeys);
		}
	}
}