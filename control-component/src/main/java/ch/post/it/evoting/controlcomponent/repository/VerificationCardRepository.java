/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ch.post.it.evoting.controlcomponent.domain.VerificationCardEntity;

@Transactional(propagation = Propagation.MANDATORY)
public interface VerificationCardRepository extends CrudRepository<VerificationCardEntity, Long> {

	Optional<VerificationCardEntity> findByVerificationCardId(final String verificationCardId);

	boolean existsByVerificationCardId(final String verificationCardId);

	@SuppressWarnings("java:S100")
	int countAllByVerificationCardSetEntity_VerificationCardSetIdAndVerificationCardSetEntity_ElectionEventEntity_ElectionEventId(
			final String verificationCardSetId, final String electionEventId);
}
