/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent;

import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import ch.post.it.evoting.controlcomponent.business.configuration.GenEncLongCodeSharesProcessor;
import ch.post.it.evoting.controlcomponent.business.configuration.KeyGenerationProcessor;
import ch.post.it.evoting.controlcomponent.business.configuration.LongVoteCastReturnCodesAllowListProcessor;
import ch.post.it.evoting.controlcomponent.business.configuration.SetupComponentPublicKeysProcessor;
import ch.post.it.evoting.controlcomponent.business.tally.GetMixnetInitialCiphertextsProcessor;
import ch.post.it.evoting.controlcomponent.business.tally.MixDecryptMessagingService;
import ch.post.it.evoting.controlcomponent.business.voting.LongChoiceReturnCodesShareProcessor;
import ch.post.it.evoting.controlcomponent.business.voting.LongVoteCastReturnCodesShareHashProcessor;
import ch.post.it.evoting.controlcomponent.business.voting.LongVoteCastReturnCodesShareVerifyProcessor;
import ch.post.it.evoting.controlcomponent.business.voting.PartialDecryptProcessor;
import ch.post.it.evoting.controlcomponent.commandmessaging.Context;
import ch.post.it.evoting.domain.ContextIdExtractor;
import ch.post.it.evoting.domain.configuration.SetupComponentPublicKeysResponsePayload;
import ch.post.it.evoting.domain.configuration.setupvoting.LongVoteCastReturnCodesAllowListResponsePayload;
import ch.post.it.evoting.domain.configuration.setupvoting.SetupComponentLVCCAllowListPayload;
import ch.post.it.evoting.domain.tally.GetMixnetInitialCiphertextsRequestPayload;
import ch.post.it.evoting.domain.tally.MixDecryptOnlineRequestPayload;
import ch.post.it.evoting.domain.tally.MixDecryptOnlineResponsePayload;
import ch.post.it.evoting.domain.voting.confirmvote.ControlComponenthlVCCPayload;
import ch.post.it.evoting.domain.voting.confirmvote.ControlComponenthlVCCRequestPayload;
import ch.post.it.evoting.domain.voting.confirmvote.ControlComponentlVCCSharePayload;
import ch.post.it.evoting.domain.voting.confirmvote.VotingServerConfirmPayload;
import ch.post.it.evoting.domain.voting.sendvote.CombinedControlComponentPartialDecryptPayload;
import ch.post.it.evoting.domain.voting.sendvote.ControlComponentLCCSharePayload;
import ch.post.it.evoting.domain.voting.sendvote.ControlComponentPartialDecryptPayload;
import ch.post.it.evoting.domain.voting.sendvote.VotingServerEncryptedVotePayload;
import ch.post.it.evoting.evotinglibraries.domain.configuration.ControlComponentPublicKeysPayload;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.ControlComponentVotesHashPayload;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.ElectionEventContextPayload;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.SetupComponentPublicKeysPayload;
import ch.post.it.evoting.evotinglibraries.domain.returncodes.ControlComponentCodeSharesPayload;
import ch.post.it.evoting.evotinglibraries.domain.returncodes.SetupComponentVerificationDataPayload;

@Configuration
public class MessageHandlerConfiguration {

	private final LongChoiceReturnCodesShareProcessor longChoiceReturnCodesShareProcessor;
	private final KeyGenerationProcessor keyGenerationProcessor;
	private final PartialDecryptProcessor partialDecryptProcessor;
	private final MixDecryptMessagingService mixDecryptMessagingService;
	private final GenEncLongCodeSharesProcessor genEncLongCodeSharesProcessor;
	private final SetupComponentPublicKeysProcessor setupComponentPublicKeysProcessor;
	private final GetMixnetInitialCiphertextsProcessor getMixnetInitialCiphertextsProcessor;
	private final LongVoteCastReturnCodesAllowListProcessor longVoteCastReturnCodesAllowListProcessor;
	private final LongVoteCastReturnCodesShareHashProcessor longVoteCastReturnCodesShareHashProcessor;
	private final LongVoteCastReturnCodesShareVerifyProcessor longVoteCastReturnCodesShareVerifyProcessor;

	public MessageHandlerConfiguration(
			final LongChoiceReturnCodesShareProcessor longChoiceReturnCodesShareProcessor,
			final KeyGenerationProcessor keyGenerationProcessor,
			final PartialDecryptProcessor partialDecryptProcessor,
			final MixDecryptMessagingService mixDecryptMessagingService,
			final GenEncLongCodeSharesProcessor genEncLongCodeSharesProcessor,
			final SetupComponentPublicKeysProcessor setupComponentPublicKeysProcessor,
			final GetMixnetInitialCiphertextsProcessor getMixnetInitialCiphertextsProcessor,
			final LongVoteCastReturnCodesAllowListProcessor longVoteCastReturnCodesAllowListProcessor,
			final LongVoteCastReturnCodesShareHashProcessor longVoteCastReturnCodesShareHashProcessor,
			final LongVoteCastReturnCodesShareVerifyProcessor longVoteCastReturnCodesShareVerifyProcessor) {
		this.longChoiceReturnCodesShareProcessor = longChoiceReturnCodesShareProcessor;
		this.keyGenerationProcessor = keyGenerationProcessor;
		this.partialDecryptProcessor = partialDecryptProcessor;
		this.mixDecryptMessagingService = mixDecryptMessagingService;
		this.genEncLongCodeSharesProcessor = genEncLongCodeSharesProcessor;
		this.setupComponentPublicKeysProcessor = setupComponentPublicKeysProcessor;
		this.getMixnetInitialCiphertextsProcessor = getMixnetInitialCiphertextsProcessor;
		this.longVoteCastReturnCodesAllowListProcessor = longVoteCastReturnCodesAllowListProcessor;
		this.longVoteCastReturnCodesShareHashProcessor = longVoteCastReturnCodesShareHashProcessor;
		this.longVoteCastReturnCodesShareVerifyProcessor = longVoteCastReturnCodesShareVerifyProcessor;
	}

	@Bean
	public List<MessageHandler.Configuration<?, ?>> messageConsumerConfigurations() {
		return List.of(
				mixDecOnlineConfiguration(),
				keyGenerationConfiguration(),
				genEncLongCodeSharesConfiguration(),
				setupComponentPublicKeysConfiguration(),
				votingServerEncryptedVoteConfiguration(),
				longVoteCastReturnCodesAllowListConfiguration(),
				combinedControlComponentPartialDecryptConfiguration(),
				votingServerConfirmPayloadConfiguration(),
				controlComponenthlVCCRequestPayloadConfiguration(),
				getMixnetInitialCiphertextsConfiguration()
		);
	}

	private MessageHandler.Configuration<ControlComponenthlVCCRequestPayload, ControlComponentlVCCSharePayload> controlComponenthlVCCRequestPayloadConfiguration() {
		return new MessageHandler.Configuration<>(
				ControlComponenthlVCCRequestPayload.class,
				longVoteCastReturnCodesShareVerifyProcessor::verifyPayloadSignature,
				Context.VOTING_RETURN_CODES_VERIFY_LVCC_SHARE_HASH,
				longVoteCastReturnCodesShareVerifyProcessor::onRequest,
				ContextIdExtractor::extract,
				longVoteCastReturnCodesShareVerifyProcessor::deserializeRequest,
				ControlComponentlVCCSharePayload.class,
				longVoteCastReturnCodesShareVerifyProcessor::serializeResponse);
	}

	private MessageHandler.Configuration<VotingServerConfirmPayload, ControlComponenthlVCCPayload> votingServerConfirmPayloadConfiguration() {
		return new MessageHandler.Configuration<>(
				VotingServerConfirmPayload.class,
				longVoteCastReturnCodesShareHashProcessor::verifyPayloadSignature,
				Context.VOTING_RETURN_CODES_CREATE_LVCC_SHARE_HASH,
				longVoteCastReturnCodesShareHashProcessor::onRequest,
				ContextIdExtractor::extract,
				longVoteCastReturnCodesShareHashProcessor::deserializeRequest,
				ControlComponenthlVCCPayload.class,
				longVoteCastReturnCodesShareHashProcessor::serializeResponse);
	}

	private MessageHandler.Configuration<SetupComponentPublicKeysPayload, SetupComponentPublicKeysResponsePayload> setupComponentPublicKeysConfiguration() {
		return new MessageHandler.Configuration<>(
				SetupComponentPublicKeysPayload.class,
				setupComponentPublicKeysProcessor::verifyPayloadSignature,
				Context.CONFIGURATION_SETUP_COMPONENT_PUBLIC_KEYS,
				setupComponentPublicKeysProcessor::onRequest,
				ContextIdExtractor::extract,
				setupComponentPublicKeysProcessor::deserializeRequest,
				SetupComponentPublicKeysResponsePayload.class,
				setupComponentPublicKeysProcessor::serializeResponse);
	}

	private MessageHandler.Configuration<CombinedControlComponentPartialDecryptPayload, ControlComponentLCCSharePayload> combinedControlComponentPartialDecryptConfiguration() {
		return new MessageHandler.Configuration<>(
				CombinedControlComponentPartialDecryptPayload.class,
				longChoiceReturnCodesShareProcessor::verifyPayload,
				Context.VOTING_RETURN_CODES_CREATE_LCC_SHARE,
				longChoiceReturnCodesShareProcessor::generateControlComponentLCCSharePayload,
				ContextIdExtractor::extract,
				longChoiceReturnCodesShareProcessor::deserializeRequest,
				ControlComponentLCCSharePayload.class,
				longChoiceReturnCodesShareProcessor::serializeResponse);
	}

	private MessageHandler.Configuration<VotingServerEncryptedVotePayload, ControlComponentPartialDecryptPayload> votingServerEncryptedVoteConfiguration() {
		return new MessageHandler.Configuration<>(
				VotingServerEncryptedVotePayload.class,
				partialDecryptProcessor::verifyPayload,
				Context.VOTING_RETURN_CODES_PARTIAL_DECRYPT_PCC,
				partialDecryptProcessor::generatePartiallyDecryptedEncryptedPayload,
				ContextIdExtractor::extract,
				partialDecryptProcessor::deserializeRequest,
				ControlComponentPartialDecryptPayload.class,
				partialDecryptProcessor::serializeResponse);
	}

	private MessageHandler.Configuration<MixDecryptOnlineRequestPayload, MixDecryptOnlineResponsePayload> mixDecOnlineConfiguration() {
		return new MessageHandler.Configuration<>(
				MixDecryptOnlineRequestPayload.class,
				mixDecryptMessagingService::verifySignature,
				Context.MIXING_TALLY_MIX_DEC_ONLINE,
				mixDecryptMessagingService::generateMixDecryptOnlineResponsePayload,
				ContextIdExtractor::extract,
				mixDecryptMessagingService::deserializeRequest,
				MixDecryptOnlineResponsePayload.class,
				mixDecryptMessagingService::serializeResponse);
	}

	private MessageHandler.Configuration<ElectionEventContextPayload, ControlComponentPublicKeysPayload> keyGenerationConfiguration() {
		return new MessageHandler.Configuration<>(
				ElectionEventContextPayload.class,
				keyGenerationProcessor::verifyPayloadSignature,
				Context.CONFIGURATION_RETURN_CODES_GEN_KEYS_CCR,
				keyGenerationProcessor::onRequest,
				ContextIdExtractor::extract,
				keyGenerationProcessor::deserializeRequest,
				ControlComponentPublicKeysPayload.class,
				keyGenerationProcessor::serializeResponse);
	}

	private MessageHandler.Configuration<SetupComponentVerificationDataPayload, ControlComponentCodeSharesPayload> genEncLongCodeSharesConfiguration() {
		return new MessageHandler.Configuration<>(
				SetupComponentVerificationDataPayload.class,
				genEncLongCodeSharesProcessor::validateSignature,
				Context.CONFIGURATION_RETURN_CODES_GEN_ENC_LONG_CODE_SHARES,
				genEncLongCodeSharesProcessor::onRequest,
				ContextIdExtractor::extract,
				genEncLongCodeSharesProcessor::deserializeRequest,
				ControlComponentCodeSharesPayload.class,
				genEncLongCodeSharesProcessor::serializeResponse);
	}

	private MessageHandler.Configuration<SetupComponentLVCCAllowListPayload, LongVoteCastReturnCodesAllowListResponsePayload> longVoteCastReturnCodesAllowListConfiguration() {
		return new MessageHandler.Configuration<>(
				SetupComponentLVCCAllowListPayload.class,
				longVoteCastReturnCodesAllowListProcessor::verifyPayloadSignature,
				Context.CONFIGURATION_SETUP_VOTING_LONG_VOTE_CAST_RETURN_CODES_ALLOW_LIST,
				longVoteCastReturnCodesAllowListProcessor::onRequest,
				ContextIdExtractor::extract,
				longVoteCastReturnCodesAllowListProcessor::deserializeRequest,
				LongVoteCastReturnCodesAllowListResponsePayload.class,
				longVoteCastReturnCodesAllowListProcessor::serializeResponse);
	}

	private MessageHandler.Configuration<GetMixnetInitialCiphertextsRequestPayload, ControlComponentVotesHashPayload> getMixnetInitialCiphertextsConfiguration() {
		return new MessageHandler.Configuration<>(
				GetMixnetInitialCiphertextsRequestPayload.class,
				getMixnetInitialCiphertextsProcessor::verifyRequestSignature,
				Context.MIXING_TALLY_GET_MIXNET_INITIAL_CIPHERTEXTS,
				getMixnetInitialCiphertextsProcessor::onRequest,
				ContextIdExtractor::extract,
				getMixnetInitialCiphertextsProcessor::deserializeRequest,
				ControlComponentVotesHashPayload.class,
				getMixnetInitialCiphertextsProcessor::serializeResponse);
	}

}
