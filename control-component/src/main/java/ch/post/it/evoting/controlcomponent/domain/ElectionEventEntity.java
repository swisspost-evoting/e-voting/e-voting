/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.domain;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import jakarta.persistence.Convert;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import jakarta.persistence.Version;

import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.domain.converters.EncryptionGroupConverter;

@Entity
@Table(name = "ELECTION_EVENT")
public class ElectionEventEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ELECTION_EVENT_SEQ_GENERATOR")
	@SequenceGenerator(sequenceName = "ELECTION_EVENT_SEQ", allocationSize = 1, name = "ELECTION_EVENT_SEQ_GENERATOR")
	private Long id;

	private String electionEventId;

	@Convert(converter = EncryptionGroupConverter.class)
	private GqGroup encryptionGroup;

	@Version
	private Integer changeControlId;

	public ElectionEventEntity() {
	}

	public ElectionEventEntity(final String electionEventId, final GqGroup encryptionGroup) {
		this.electionEventId = validateUUID(electionEventId);
		this.encryptionGroup = checkNotNull(encryptionGroup);
	}

	public String getElectionEventId() {
		return electionEventId;
	}

	public GqGroup getEncryptionGroup() {
		return encryptionGroup;
	}
}
