/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.business.configuration;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.BASE64_ENCODED_HASH_OUTPUT_LENGTH;
import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateBase64Encoded;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.security.SignatureException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.controlcomponent.domain.BallotBoxEntity;
import ch.post.it.evoting.controlcomponent.domain.CcrjReturnCodesKeys;
import ch.post.it.evoting.controlcomponent.domain.ElectionEventState;
import ch.post.it.evoting.controlcomponent.domain.PCCAllowListEntryEntity;
import ch.post.it.evoting.controlcomponent.domain.VerificationCardSetEntity;
import ch.post.it.evoting.controlcomponent.protocol.configuration.setupvoting.GenEncLongCodeSharesOutput;
import ch.post.it.evoting.controlcomponent.protocol.configuration.setupvoting.GenEncLongCodeSharesService;
import ch.post.it.evoting.controlcomponent.service.BallotBoxService;
import ch.post.it.evoting.controlcomponent.service.CcrjReturnCodesKeysService;
import ch.post.it.evoting.controlcomponent.service.ElectionEventService;
import ch.post.it.evoting.controlcomponent.service.ElectionEventStateService;
import ch.post.it.evoting.controlcomponent.service.IdentifierValidationService;
import ch.post.it.evoting.controlcomponent.service.PCCAllowListEntryService;
import ch.post.it.evoting.controlcomponent.service.VerificationCardService;
import ch.post.it.evoting.controlcomponent.service.VerificationCardSetService;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ExponentiationProof;
import ch.post.it.evoting.domain.InvalidPayloadSignatureException;
import ch.post.it.evoting.evotinglibraries.domain.common.ChannelSecurityContextData;
import ch.post.it.evoting.evotinglibraries.domain.returncodes.ControlComponentCodeShare;
import ch.post.it.evoting.evotinglibraries.domain.returncodes.ControlComponentCodeSharesPayload;
import ch.post.it.evoting.evotinglibraries.domain.returncodes.SetupComponentVerificationData;
import ch.post.it.evoting.evotinglibraries.domain.returncodes.SetupComponentVerificationDataPayload;
import ch.post.it.evoting.evotinglibraries.domain.signature.Alias;
import ch.post.it.evoting.evotinglibraries.domain.signature.CryptoPrimitivesSignature;

@Service
public class GenEncLongCodeSharesProcessor {

	private static final Logger LOGGER = LoggerFactory.getLogger(GenEncLongCodeSharesProcessor.class);

	private final ObjectMapper objectMapper;
	private final BallotBoxService ballotBoxService;
	private final ElectionEventService electionEventService;
	private final VerificationCardService verificationCardService;
	private final PCCAllowListEntryService pccAllowListEntryService;
	private final SignatureKeystore<Alias> signatureKeystoreService;
	private final ElectionEventStateService electionEventStateService;
	private final CcrjReturnCodesKeysService ccrjReturnCodesKeysService;
	private final VerificationCardSetService verificationCardSetService;
	private final IdentifierValidationService identifierValidationService;
	private final GenEncLongCodeSharesService genEncLongCodeSharesService;

	@Value("${nodeID}")
	private int nodeId;

	public GenEncLongCodeSharesProcessor(
			final ObjectMapper objectMapper,
			final BallotBoxService ballotBoxService,
			final ElectionEventService electionEventService,
			final VerificationCardService verificationCardService,
			final PCCAllowListEntryService pccAllowListEntryService,
			final SignatureKeystore<Alias> signatureKeystoreService,
			final ElectionEventStateService electionEventStateService,
			final CcrjReturnCodesKeysService ccrjReturnCodesKeysService,
			final VerificationCardSetService verificationCardSetService,
			final IdentifierValidationService identifierValidationService,
			final GenEncLongCodeSharesService genEncLongCodeSharesService) {
		this.objectMapper = objectMapper;
		this.ballotBoxService = ballotBoxService;
		this.electionEventService = electionEventService;
		this.verificationCardService = verificationCardService;
		this.pccAllowListEntryService = pccAllowListEntryService;
		this.signatureKeystoreService = signatureKeystoreService;
		this.electionEventStateService = electionEventStateService;
		this.ccrjReturnCodesKeysService = ccrjReturnCodesKeysService;
		this.verificationCardSetService = verificationCardSetService;
		this.identifierValidationService = identifierValidationService;
		this.genEncLongCodeSharesService = genEncLongCodeSharesService;
	}

	public ControlComponentCodeSharesPayload onRequest(final SetupComponentVerificationDataPayload setupComponentVerificationDataPayload) {
		checkNotNull(setupComponentVerificationDataPayload);

		final String electionEventId = setupComponentVerificationDataPayload.getElectionEventId();
		final String verificationCardSetId = setupComponentVerificationDataPayload.getVerificationCardSetId();
		final int chunkId = setupComponentVerificationDataPayload.getChunkId();

		// Validate encryption group.
		final GqGroup encryptionGroup = electionEventService.getEncryptionGroup(electionEventId);
		checkState(setupComponentVerificationDataPayload.getEncryptionGroup().equals(encryptionGroup),
				"The group of te setup component verification data payload must be equal to the encryption group.");
		identifierValidationService.validateIds(electionEventId, verificationCardSetId);

		// Validate election event state.
		final ElectionEventState expectedState = ElectionEventState.INITIAL;
		final ElectionEventState electionEventState = electionEventStateService.getElectionEventState(electionEventId);
		checkState(expectedState.equals(electionEventState),
				"The election event is not in the expected state. [electionEventId: %s, nodeId: %s, expected: %s, actual: %s]", electionEventId,
				nodeId, expectedState, electionEventState);

		// Sanity check the partial Choice Return Codes allow list before saving.
		final List<String> payloadValidatedAllowList = validateAllowList(setupComponentVerificationDataPayload);

		// Save allow list chunk.
		final VerificationCardSetEntity verificationCardSetEntity = verificationCardSetService.getVerificationCardSet(verificationCardSetId);
		final List<PCCAllowListEntryEntity> partialChoiceReturnCodeAllowList = payloadValidatedAllowList.stream()
				.map(partialChoiceCode -> new PCCAllowListEntryEntity(verificationCardSetEntity, partialChoiceCode, chunkId)).toList();
		pccAllowListEntryService.saveAll(partialChoiceReturnCodeAllowList);
		LOGGER.info("AllowList saved chunk. [electionEventId: {}, verificationCardSetId: {}, nodeId: {}, chunkId: {}]", electionEventId,
				verificationCardSetId, nodeId, chunkId);

		final BallotBoxEntity ballotBoxEntity = ballotBoxService.getBallotBox(verificationCardSetEntity);
		final int numberOfVerificationCards = ballotBoxEntity.getNumberOfVotingCards();

		final int savedNumberOfVerificationCards = verificationCardService.countNumberOfVotingCards(electionEventId, verificationCardSetId);
		final List<String> verificationCardIdsToSave = getVerificationCardIds(setupComponentVerificationDataPayload);
		checkArgument(numberOfVerificationCards >= savedNumberOfVerificationCards + verificationCardIdsToSave.size(),
				"The number of verification cards saved + the number of verification cards to save must not exceed the number of verification cards for the given election event and verification card set id. "
						+ "[electionEventId: %s, nodeId: %s, verificationCardSetId: %s, numberOfVerificationCards: %s, savedNumberOfVerificationCards: %s, numberOfVerificationCardIdsToSave: %s]",
				electionEventId, nodeId, verificationCardSetId, numberOfVerificationCards, savedNumberOfVerificationCards,
				verificationCardIdsToSave.size());

		final CcrjReturnCodesKeys ccrjReturnCodesKeys = ccrjReturnCodesKeysService.getCcrjReturnCodesKeys(electionEventId);

		final GenEncLongCodeSharesOutput genEncLongCodeSharesOutput = genEncLongCodeSharesService.genEncLongCodeShares(
				setupComponentVerificationDataPayload, ccrjReturnCodesKeys, ballotBoxEntity.getPrimesMappingTable().getNumberOfVotingOptions());
		LOGGER.info("GenEnLongCodeShares algorithm successfully performed. [electionEventId: {}, verificationCardSetId: {}, nodeId: {}, chunkId: {}]",
				electionEventId, verificationCardSetId, nodeId, chunkId);

		return toControlComponentCodeSharesPayload(electionEventId, verificationCardSetId, encryptionGroup, setupComponentVerificationDataPayload,
				genEncLongCodeSharesOutput);
	}

	public boolean validateSignature(final SetupComponentVerificationDataPayload payload) {
		checkNotNull(payload);

		final String electionEventId = payload.getElectionEventId();
		final String verificationCardSetId = payload.getVerificationCardSetId();
		final String payloadId = String.format("[electionEventId: %s, verificationCardSetId: %s, nodeId: %s, chunkId: %s]", electionEventId,
				verificationCardSetId, nodeId, payload.getChunkId());

		final CryptoPrimitivesSignature signature = payload.getSignature();
		checkState(signature != null, "The signature of the setup component verification data payload is null. %s", payloadId);

		final Hashable additionalContextData = ChannelSecurityContextData.setupComponentVerificationData(electionEventId, verificationCardSetId);

		LOGGER.debug("Checking the signature of payload... {}", payloadId);

		final boolean isSignatureValid;
		try {
			isSignatureValid = signatureKeystoreService.verifySignature(Alias.SDM_CONFIG, payload, additionalContextData,
					signature.signatureContents());
		} catch (final SignatureException e) {
			throw new IllegalStateException(String.format("Unable to verify the setup component verification data payload signature. %s", payloadId),
					e);
		}

		if (!isSignatureValid) {
			throw new InvalidPayloadSignatureException(SetupComponentVerificationDataPayload.class, payloadId);
		}

		LOGGER.info("Successfully verified the signature of the setup component verification data payload. {}", payloadId);

		return isSignatureValid;
	}

	public SetupComponentVerificationDataPayload deserializeRequest(final byte[] messageBytes) {
		checkNotNull(messageBytes);
		final byte[] messageBytesCopy = Arrays.copyOf(messageBytes, messageBytes.length);

		try {
			return objectMapper.readValue(messageBytesCopy, SetupComponentVerificationDataPayload.class);
		} catch (final IOException e) {
			throw new UncheckedIOException("Unable to deserialize Setup Component Verification Data Payload", e);
		}
	}

	public byte[] serializeResponse(final ControlComponentCodeSharesPayload controlComponentCodeSharesPayload) {
		checkNotNull(controlComponentCodeSharesPayload);

		try {
			return objectMapper.writeValueAsBytes(controlComponentCodeSharesPayload);
		} catch (final IOException e) {
			throw new UncheckedIOException("Unable to serialize Control Component Code Shares Payload", e);
		}
	}

	private ControlComponentCodeSharesPayload toControlComponentCodeSharesPayload(final String electionEventId, final String verificationCardSetId,
			final GqGroup gqGroup, final SetupComponentVerificationDataPayload payload, final GenEncLongCodeSharesOutput output) {

		final List<String> verificationCardIds = getVerificationCardIds(payload);

		final int numberOfEligibleVoters = output.getExponentiatedEncryptedHashedPartialChoiceReturnCodes().size();
		checkArgument(verificationCardIds.size() == numberOfEligibleVoters);

		final List<ControlComponentCodeShare> returnCodeGenerationOutputs = IntStream.range(0, numberOfEligibleVoters)
				.parallel()
				.mapToObj(i -> {
					final String verificationCardId = verificationCardIds.get(i);
					final GqElement voterChoiceReturnCodeGenerationPublicKeys = output.getVoterChoiceReturnCodeGenerationPublicKeys().get(i);
					final GqElement voterVoteCastReturnCodeGenerationPublicKeys = output.getVoterVoteCastReturnCodeGenerationPublicKeys().get(i);
					final ElGamalMultiRecipientCiphertext exponentiatedEncryptedHashedPartialChoiceReturnCodes = output.getExponentiatedEncryptedHashedPartialChoiceReturnCodes()
							.get(i);
					final ExponentiationProof proofsCorrectExponentiationPartialChoiceReturnCodes = output.getProofsCorrectExponentiationPartialChoiceReturnCodes()
							.get(i);
					final ElGamalMultiRecipientCiphertext exponentiatedEncryptedHashedConfirmationKeys = output.getExponentiatedEncryptedHashedConfirmationKeys()
							.get(i);
					final ExponentiationProof proofsCorrectExponentiationConfirmationKeys = output.getProofsCorrectExponentiationConfirmationKeys()
							.get(i);

					return new ControlComponentCodeShare(verificationCardId,
							new ElGamalMultiRecipientPublicKey(GroupVector.of(voterChoiceReturnCodeGenerationPublicKeys)),
							new ElGamalMultiRecipientPublicKey(GroupVector.of(voterVoteCastReturnCodeGenerationPublicKeys)),
							exponentiatedEncryptedHashedPartialChoiceReturnCodes, exponentiatedEncryptedHashedConfirmationKeys,
							proofsCorrectExponentiationPartialChoiceReturnCodes, proofsCorrectExponentiationConfirmationKeys);
				}).toList();

		final ControlComponentCodeSharesPayload controlComponentCodeSharesPayload = new ControlComponentCodeSharesPayload(electionEventId,
				verificationCardSetId, payload.getChunkId(), gqGroup, returnCodeGenerationOutputs, nodeId);

		final CryptoPrimitivesSignature controlComponentCodeSharesPayloadSignature = getPayloadSignature(controlComponentCodeSharesPayload);
		controlComponentCodeSharesPayload.setSignature(controlComponentCodeSharesPayloadSignature);
		LOGGER.info(
				"Successfully signed control component code shares payload [electionEventId: {}, verificationCardSetId: {}, nodeId: {}, chunkId: {}]",
				electionEventId, verificationCardSetId, nodeId, payload.getChunkId());

		return controlComponentCodeSharesPayload;
	}

	private List<String> getVerificationCardIds(final SetupComponentVerificationDataPayload payload) {
		final List<SetupComponentVerificationData> returnCodeGenerationInputs = payload.getSetupComponentVerificationData();

		return returnCodeGenerationInputs.stream().parallel()
				.map(SetupComponentVerificationData::verificationCardId)
				.toList();
	}

	private List<String> validateAllowList(final SetupComponentVerificationDataPayload payload) {
		final List<String> payloadAllowList = payload.getPartialChoiceReturnCodesAllowList();
		payloadAllowList.stream().parallel()
				.forEach(element -> checkArgument(validateBase64Encoded(element).length() == BASE64_ENCODED_HASH_OUTPUT_LENGTH, String.format(
						"At least one element in the partial Choice Return Codes allow list has incorrect length. [element: %s, allowed length: %s]",
						element, BASE64_ENCODED_HASH_OUTPUT_LENGTH)));
		final ArrayList<String> payloadAllowListCopy = new ArrayList<>(payloadAllowList);
		Collections.sort(payloadAllowListCopy);
		checkArgument(payloadAllowList.equals(payloadAllowListCopy), "The allow list is not lexicographically sorted.");

		return payloadAllowList;
	}

	private CryptoPrimitivesSignature getPayloadSignature(final ControlComponentCodeSharesPayload payload) {
		final String electionEventId = payload.getElectionEventId();
		final String verificationCardSetId = payload.getVerificationCardSetId();

		final Hashable additionalContextData = ChannelSecurityContextData.controlComponentCodeShares(nodeId, electionEventId, verificationCardSetId);

		try {
			final byte[] signature = signatureKeystoreService.generateSignature(payload, additionalContextData);

			return new CryptoPrimitivesSignature(signature);
		} catch (final SignatureException se) {
			final String message = String.format(
					"Failed to generate the control component code shares payload signature [electionEventId: %s, verificationCardSetId: %s, nodeId: %s, chunkId: %s]",
					electionEventId, verificationCardSetId, nodeId, payload.getChunkId());
			throw new IllegalStateException(message, se);
		}
	}
}
