/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ch.post.it.evoting.controlcomponent.domain.PartiallyDecryptedPCCEntity;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
public interface PartiallyDecryptedPCCRepository extends CrudRepository<PartiallyDecryptedPCCEntity, Long> {

	@Query("select e from PartiallyDecryptedPCCEntity e where e.verificationCardEntity.verificationCardId = ?1")
	Optional<PartiallyDecryptedPCCEntity> findByVerificationCardId(final String verificationCardId);

}