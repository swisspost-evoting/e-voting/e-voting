/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.business.voting;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.security.SignatureException;
import java.time.LocalDateTime;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.controlcomponent.domain.ElectionEventState;
import ch.post.it.evoting.controlcomponent.protocol.voting.confirmvote.CreateLVCCShareOutput;
import ch.post.it.evoting.controlcomponent.protocol.voting.confirmvote.CreateLVCCShareService;
import ch.post.it.evoting.controlcomponent.service.ElectionEventService;
import ch.post.it.evoting.controlcomponent.service.ElectionEventStateService;
import ch.post.it.evoting.controlcomponent.service.IdentifierValidationService;
import ch.post.it.evoting.controlcomponent.service.VerificationCardStateService;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.domain.voting.confirmvote.ConfirmationKey;
import ch.post.it.evoting.domain.voting.confirmvote.ControlComponenthlVCCPayload;
import ch.post.it.evoting.domain.voting.confirmvote.VotingServerConfirmPayload;
import ch.post.it.evoting.evotinglibraries.domain.common.ChannelSecurityContextData;
import ch.post.it.evoting.evotinglibraries.domain.common.ContextIds;
import ch.post.it.evoting.evotinglibraries.domain.signature.Alias;
import ch.post.it.evoting.evotinglibraries.domain.signature.CryptoPrimitivesSignature;

@Service
public class LongVoteCastReturnCodesShareHashProcessor {

	private static final Logger LOGGER = LoggerFactory.getLogger(LongVoteCastReturnCodesShareHashProcessor.class);

	private final ObjectMapper objectMapper;
	private final ElectionEventService electionEventService;
	private final SignatureKeystore<Alias> signatureKeystoreService;
	private final CreateLVCCShareService createLVCCShareService;
	private final ElectionEventStateService electionEventStateService;
	private final IdentifierValidationService identifierValidationService;
	private final VerificationCardStateService verificationCardStateService;
	private final LongVoteCastReturnCodesShareService longVoteCastReturnCodesShareService;

	@Value("${nodeID}")
	private int nodeId;

	LongVoteCastReturnCodesShareHashProcessor(
			final ObjectMapper objectMapper,
			final ElectionEventService electionEventService,
			final SignatureKeystore<Alias> signatureKeystoreService,
			final CreateLVCCShareService createLVCCShareService,
			final ElectionEventStateService electionEventStateService,
			final IdentifierValidationService identifierValidationService,
			final VerificationCardStateService verificationCardStateService,
			final LongVoteCastReturnCodesShareService longVoteCastReturnCodesShareService) {
		this.objectMapper = objectMapper;
		this.electionEventService = electionEventService;
		this.signatureKeystoreService = signatureKeystoreService;
		this.createLVCCShareService = createLVCCShareService;
		this.electionEventStateService = electionEventStateService;
		this.identifierValidationService = identifierValidationService;
		this.verificationCardStateService = verificationCardStateService;
		this.longVoteCastReturnCodesShareService = longVoteCastReturnCodesShareService;
	}

	public ControlComponenthlVCCPayload onRequest(final VotingServerConfirmPayload votingServerConfirmPayload) {
		checkNotNull(votingServerConfirmPayload);

		verifyPayloadSignature(votingServerConfirmPayload);

		final ContextIds contextIds = votingServerConfirmPayload.getConfirmationKey().contextIds();

		identifierValidationService.validateContextIds(contextIds);
		final String electionEventId = contextIds.electionEventId();
		final String verificationCardSetId = contextIds.verificationCardSetId();
		final String verificationCardId = contextIds.verificationCardId();

		longVoteCastReturnCodesShareService.validateConfirmationIsAllowed(electionEventId, verificationCardId, LocalDateTime::now);

		// The variable confirmationAttempts is initialized to 0 and incremented for each confirmation attempt.
		final int contextConfirmationAttempts = verificationCardStateService.getConfirmationAttempts(verificationCardId);

		checkState(contextConfirmationAttempts == votingServerConfirmPayload.getUnsuccessfulConfirmationAttemptCount(),
				"The unsuccessfulConfirmationAttempts count does not match between payload file and database. [payload: {}, database: {}]",
				votingServerConfirmPayload.getUnsuccessfulConfirmationAttemptCount(), contextConfirmationAttempts);

		final String contextId = String.join("-", electionEventId, verificationCardSetId, verificationCardId,
				String.valueOf(contextConfirmationAttempts));
		LOGGER.info("Received create LVCC share hash request. [contextId: {}]", contextId);

		return generateControlComponenthlVCCPayload(votingServerConfirmPayload);
	}

	public boolean verifyPayloadSignature(final VotingServerConfirmPayload votingServerConfirmPayload) {
		final CryptoPrimitivesSignature signature = votingServerConfirmPayload.getSignature();
		final ContextIds contextIds = votingServerConfirmPayload.getConfirmationKey().contextIds();

		checkState(signature != null, "The signature of voting server confirm payload is null. [contextIds: %s]",
				contextIds);

		final Hashable additionalContextData = ChannelSecurityContextData.votingServerConfirm(contextIds.electionEventId(),
				contextIds.verificationCardSetId(), contextIds.verificationCardId());
		final boolean isSignatureValid;
		try {
			isSignatureValid = signatureKeystoreService.verifySignature(Alias.VOTING_SERVER, votingServerConfirmPayload,
					additionalContextData, signature.signatureContents());
		} catch (final SignatureException e) {
			throw new IllegalStateException(String.format("Unable to verify signature [contextIds: %s]", contextIds), e);
		}

		return isSignatureValid;
	}

	private ControlComponenthlVCCPayload generateControlComponenthlVCCPayload(final VotingServerConfirmPayload unsignedVotingServerConfirmPayload) {
		final ConfirmationKey confirmationKey = unsignedVotingServerConfirmPayload.getConfirmationKey();
		final int unsuccessfulConfirmationAttemptCount = unsignedVotingServerConfirmPayload.getUnsuccessfulConfirmationAttemptCount();
		final ContextIds contextIds = confirmationKey.contextIds();
		final String electionEventId = contextIds.electionEventId();
		final GqGroup encryptionGroup = electionEventService.getEncryptionGroup(electionEventId);

		// Validate election event state.
		final ElectionEventState expectedState = ElectionEventState.CONFIGURED;
		final ElectionEventState electionEventState = electionEventStateService.getElectionEventState(contextIds.electionEventId());
		checkState(expectedState.equals(electionEventState),
				"The election event is not in the expected state. [electionEventId: %s, nodeId: %s, expected: %s, actual: %s]", electionEventId,
				nodeId, expectedState, electionEventState);

		final CreateLVCCShareOutput createLVCCShareOutput = createLVCCShareService.createLVCCShare(encryptionGroup, confirmationKey);
		LOGGER.info(
				"CreateLVCCShare algorithm successfully performed. Successfully generated the Long Vote Cast Return Codes Share. [contextIds: {}]",
				contextIds);

		longVoteCastReturnCodesShareService.saveLVCCShare(confirmationKey, createLVCCShareOutput);

		final ControlComponenthlVCCPayload payload = new ControlComponenthlVCCPayload(encryptionGroup, nodeId,
				createLVCCShareOutput.hashedLongVoteCastReturnCodeShare(), confirmationKey, unsuccessfulConfirmationAttemptCount);

		payload.setSignature(getPayloadSignature(payload));
		LOGGER.info("Successfully signed Control Component hlVCC payload. [contextIds: {}]", contextIds);
		return payload;
	}

	private CryptoPrimitivesSignature getPayloadSignature(final ControlComponenthlVCCPayload payload) {
		final ContextIds contextIds = payload.getConfirmationKey().contextIds();
		final Hashable additionalContextData = ChannelSecurityContextData.controlComponenthlVCC(nodeId, contextIds.electionEventId(),
				contextIds.verificationCardSetId(), contextIds.verificationCardId());

		final byte[] signature;
		try {
			signature = signatureKeystoreService.generateSignature(payload, additionalContextData);
		} catch (final SignatureException e) {
			throw new IllegalStateException(String.format("Failed to generate payload signature [contextIds: %s, nodeId: %s]", contextIds, nodeId),
					e);
		}
		return new CryptoPrimitivesSignature(signature);
	}

	public VotingServerConfirmPayload deserializeRequest(final byte[] bytes) {
		checkNotNull(bytes);
		final byte[] bytesCopy = Arrays.copyOf(bytes, bytes.length);
		try {
			return objectMapper.readValue(bytesCopy, VotingServerConfirmPayload.class);
		} catch (final IOException e) {
			throw new UncheckedIOException(e);
		}
	}

	public byte[] serializeResponse(final ControlComponenthlVCCPayload controlComponenthlVCCPayload) {
		checkNotNull(controlComponenthlVCCPayload);
		try {
			return objectMapper.writeValueAsBytes(controlComponenthlVCCPayload);
		} catch (final JsonProcessingException e) {
			throw new UncheckedIOException(e);
		}
	}
}
