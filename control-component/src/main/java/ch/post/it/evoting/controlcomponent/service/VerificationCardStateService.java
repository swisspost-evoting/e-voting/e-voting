/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.service;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ch.post.it.evoting.controlcomponent.domain.VerificationCardStateEntity;
import ch.post.it.evoting.controlcomponent.repository.VerificationCardStateRepository;

@Service
public class VerificationCardStateService {

	private static final Logger LOGGER = LoggerFactory.getLogger(VerificationCardStateService.class);

	private final VerificationCardStateRepository verificationCardStateRepository;

	public VerificationCardStateService(
			final VerificationCardStateRepository verificationCardStateRepository) {
		this.verificationCardStateRepository = verificationCardStateRepository;
	}

	@Transactional
	public boolean isPartiallyDecrypted(final String verificationCardId) {
		validateUUID(verificationCardId);

		final VerificationCardStateEntity verificationCardStateEntity = getVerificationCardState(verificationCardId);

		return verificationCardStateEntity.isPartiallyDecrypted();
	}

	@Transactional
	public boolean isNotPartiallyDecrypted(final String verificationCardId) {
		validateUUID(verificationCardId);

		final VerificationCardStateEntity verificationCardStateEntity = getVerificationCardState(verificationCardId);

		return !verificationCardStateEntity.isPartiallyDecrypted();
	}

	@Transactional
	public void setPartiallyDecrypted(final String verificationCardId) {
		validateUUID(verificationCardId);

		final VerificationCardStateEntity verificationCardStateEntity = getVerificationCardState(verificationCardId);
		verificationCardStateEntity.setPartiallyDecrypted(true);

		verificationCardStateRepository.save(verificationCardStateEntity);
		LOGGER.info("Set verification card state to partially decrypted [verificationCardId: {}]", verificationCardId);
	}

	@Transactional
	public boolean isSentVote(final String verificationCardId) {
		validateUUID(verificationCardId);

		final VerificationCardStateEntity verificationCardStateEntity = getVerificationCardState(verificationCardId);

		return verificationCardStateEntity.isLccShareCreated();
	}

	@Transactional
	public boolean isNotSentVote(final String verificationCardId) {
		validateUUID(verificationCardId);

		return !isSentVote(verificationCardId);
	}

	@Transactional
	public void setSentVote(final String verificationCardId) {
		validateUUID(verificationCardId);

		final VerificationCardStateEntity verificationCardStateEntity = getVerificationCardState(verificationCardId);
		verificationCardStateEntity.setLccShareCreated(true);

		verificationCardStateRepository.save(verificationCardStateEntity);
		LOGGER.info("Set verification card state to LCC created [verificationCardId: {}]", verificationCardId);
	}

	@Transactional
	public boolean isNotConfirmedVote(final String verificationCardId) {
		validateUUID(verificationCardId);

		final VerificationCardStateEntity verificationCardStateEntity = getVerificationCardState(verificationCardId);

		return !verificationCardStateEntity.isConfirmed();
	}

	@Transactional
	public void setConfirmedVote(final String verificationCardId) {
		validateUUID(verificationCardId);

		final VerificationCardStateEntity verificationCardStateEntity = getVerificationCardState(verificationCardId);
		verificationCardStateEntity.setConfirmed(true);

		verificationCardStateRepository.save(verificationCardStateEntity);
		LOGGER.info("Set verification card state to confirmed [verificationCardId: {}]", verificationCardId);
	}

	@Transactional
	public int getConfirmationAttempts(final String verificationCardId) {
		validateUUID(verificationCardId);

		final VerificationCardStateEntity verificationCardStateEntity = getVerificationCardState(verificationCardId);

		return verificationCardStateEntity.getConfirmationAttempts();
	}

	@Transactional
	public void incrementConfirmationAttempts(final String verificationCardId) {
		validateUUID(verificationCardId);

		final VerificationCardStateEntity verificationCardStateEntity = getVerificationCardState(verificationCardId);

		int confirmationAttempts = verificationCardStateEntity.getConfirmationAttempts();
		verificationCardStateEntity.setConfirmationAttempts(++confirmationAttempts);

		verificationCardStateRepository.save(verificationCardStateEntity);
	}

	private VerificationCardStateEntity getVerificationCardState(final String verificationCardId) {
		validateUUID(verificationCardId);

		return verificationCardStateRepository.findByVerificationCardId(verificationCardId)
				.orElseThrow(() -> new IllegalStateException(
						String.format("No verification card state found. [verificationCardId: %s]", verificationCardId)));
	}
}
