/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.domain;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Arrays;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.MapsId;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Version;

@Entity
@Table(name = "PARTIALLY_DECRYPTED_PCC")
public class PartiallyDecryptedPCCEntity {

	@Id
	private Long id;

	@OneToOne(fetch = FetchType.LAZY)
	@MapsId
	@JoinColumn(name = "VERIFICATION_CARD_FK_ID")
	private VerificationCardEntity verificationCardEntity;

	@Version
	private Integer changeControlId;

	@Column(name = "PARTIALLY_DECRYPTED_ENCRYPTED_PCC")
	private byte[] partiallyDecryptedEncryptedPCC;

	public PartiallyDecryptedPCCEntity() {
	}

	public PartiallyDecryptedPCCEntity(final VerificationCardEntity verificationCardEntity, final byte[] partiallyDecryptedEncryptedPCC) {
		this.verificationCardEntity = checkNotNull(verificationCardEntity);
		this.partiallyDecryptedEncryptedPCC = checkNotNull(partiallyDecryptedEncryptedPCC);
	}

	public byte[] getPartiallyDecryptedEncryptedPCC() {
		return Arrays.copyOf(partiallyDecryptedEncryptedPCC, partiallyDecryptedEncryptedPCC.length);
	}

	public VerificationCardEntity getVerificationCardEntity() {
		return verificationCardEntity;
	}

}
