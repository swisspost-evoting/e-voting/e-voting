/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent;

import static ch.post.it.evoting.domain.SharedQueue.CONTROL_COMPONENT_QUEUE;
import static ch.post.it.evoting.domain.SharedQueue.MESSAGE_HEADER_MESSAGE_TYPE;
import static ch.post.it.evoting.domain.SharedQueue.VOTING_SERVER_ADDRESS;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.util.List;
import java.util.function.Function;

import jakarta.jms.JMSException;
import jakarta.jms.Message;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import com.google.common.collect.MoreCollectors;

import ch.post.it.evoting.controlcomponent.commandmessaging.Context;

@Component
public class MessageHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(MessageHandler.class);

	private final List<Configuration<?, ?>> configurations;
	private final ExactlyOnceCommandExecutor exactlyOnceCommandExecutor;
	private final JmsTemplate jmsTemplate;

	@Value("${nodeID}")
	private int nodeId;

	MessageHandler(
			final List<Configuration<?, ?>> configurations,
			final ExactlyOnceCommandExecutor exactlyOnceCommandExecutor,
			final JmsTemplate jmsTemplate) {
		this.configurations = configurations;
		this.exactlyOnceCommandExecutor = exactlyOnceCommandExecutor;
		this.jmsTemplate = jmsTemplate;
	}

	@JmsListener(
			destination = CONTROL_COMPONENT_QUEUE + "${nodeID}",
			concurrency = "${jms.listener.concurrency}",
			containerFactory = "customFactory"
	)
	public <T, U> void onMessage(final Message message) throws JMSException {
		checkNotNull(message);
		final String requestMessageType = checkNotNull(message.getStringProperty(MESSAGE_HEADER_MESSAGE_TYPE));
		final String correlationId = checkNotNull(message.getJMSCorrelationID());

		LOGGER.info("Received new request. [requestMessageType: {}, correlationId: {}, nodeId: {}]", requestMessageType, correlationId, nodeId);

		final byte[] messageBody = message.getBody(byte[].class);

		@SuppressWarnings("unchecked")
		final Configuration<T, U> configuration = (Configuration<T, U>) getConfiguration(requestMessageType);

		final T payload = configuration.requestDeserializer().apply(messageBody);

		checkState(configuration.signatureValidator().apply(payload),
				"The signature is not valid. [requestMessageType: %s, correlationId: %s, nodeId: %s]", requestMessageType, correlationId, nodeId);

		final String contextId = configuration.contextIdExtractor().apply(payload);
		final Context context = configuration.context();
		final Function<T, U> exactlyOnceTask = configuration.exactlyOnceTask();
		final Function<U, byte[]> responseSerializer = configuration.responseSerializer();

		// Exactly once process the payload.
		final ExactlyOnceCommand<U> exactlyOnceCommand = new ExactlyOnceCommand.Builder<U>()
				.setCorrelationId(correlationId)
				.setContextId(contextId)
				.setContext(context.toString())
				.setTask(() -> exactlyOnceTask.apply(payload))
				.setSerializer(responseSerializer)
				.setRequestContent(messageBody)
				.build();

		final byte[] responseAsBytes = exactlyOnceCommandExecutor.process(exactlyOnceCommand);
		final String responseMessageTypeName = configuration.responseType.getName();

		jmsTemplate.convertAndSend(VOTING_SERVER_ADDRESS, responseAsBytes, jmsMessage -> {
			jmsMessage.setJMSCorrelationID(correlationId);
			jmsMessage.setStringProperty(MESSAGE_HEADER_MESSAGE_TYPE, responseMessageTypeName);
			return jmsMessage;
		});

		LOGGER.info("Response sent. [requestMessageType: {}, responseMessageType: {}, correlationId: {}, nodeId: {}]", requestMessageType,
				responseMessageTypeName, correlationId, nodeId);
	}

	private Configuration<?, ?> getConfiguration(final String requestMessageType) {
		final Class<?> messageClass;
		try {
			messageClass = Class.forName(requestMessageType);
		} catch (final ClassNotFoundException e) {
			throw new IllegalArgumentException(
					String.format("The given request message type is unknown. [requestMessageType: %s]", requestMessageType), e);
		}

		return configurations.stream()
				.filter(c -> messageClass.equals(c.requestType()))
				.collect(MoreCollectors.onlyElement());
	}

	public record Configuration<T, U>(
			Class<T> requestType,
			Function<T, Boolean> signatureValidator,
			Context context,
			Function<T, U> exactlyOnceTask,
			Function<T, String> contextIdExtractor,
			Function<byte[], T> requestDeserializer,
			Class<U> responseType,
			Function<U, byte[]> responseSerializer) {
	}

}


