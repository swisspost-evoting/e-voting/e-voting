/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.domain;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.time.LocalDateTime;

import jakarta.persistence.Convert;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.MapsId;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Version;

import ch.post.it.evoting.domain.converters.BooleanConverter;
import ch.post.it.evoting.domain.converters.PrimesMappingTableConverter;
import ch.post.it.evoting.evotinglibraries.domain.election.PrimesMappingTable;
import ch.post.it.evoting.evotinglibraries.domain.validations.GracePeriodValidation;

@Entity
@Table(name = "BALLOT_BOX")
public class BallotBoxEntity {

	@Id
	private Long id;

	private String ballotBoxId;

	@OneToOne(fetch = FetchType.LAZY)
	@MapsId
	@JoinColumn(name = "VERIFICATION_CARD_SET_FK_ID")
	private VerificationCardSetEntity verificationCardSetEntity;

	private LocalDateTime ballotBoxStartTime;
	private LocalDateTime ballotBoxFinishTime;

	@Convert(converter = BooleanConverter.class)
	private boolean mixed = false;

	@Convert(converter = BooleanConverter.class)
	private boolean testBallotBox = false;

	private int numberOfVotingCards;

	// The grace period in seconds.
	private int gracePeriod;

	@Convert(converter = PrimesMappingTableConverter.class)
	private PrimesMappingTable primesMappingTable;

	@Version
	private Integer changeControlId;

	public BallotBoxEntity() {
	}

	private BallotBoxEntity(final String ballotBoxId, final VerificationCardSetEntity verificationCardSetEntity,
			final LocalDateTime ballotBoxStartTime, final LocalDateTime ballotBoxFinishTime, final boolean testBallotBox,
			final int numberOfVotingCards, final int gracePeriod, final PrimesMappingTable primesMappingTable) {
		this.ballotBoxId = validateUUID(ballotBoxId);
		this.verificationCardSetEntity = checkNotNull(verificationCardSetEntity);

		this.ballotBoxStartTime = checkNotNull(ballotBoxStartTime);
		this.ballotBoxFinishTime = checkNotNull(ballotBoxFinishTime);
		checkArgument(ballotBoxStartTime.isBefore(ballotBoxFinishTime) || ballotBoxStartTime.equals(ballotBoxFinishTime),
				"The ballot box start time must not be after the ballot box finish time.");

		this.testBallotBox = testBallotBox;

		checkArgument(numberOfVotingCards > 0);
		this.numberOfVotingCards = numberOfVotingCards;

		this.gracePeriod = GracePeriodValidation.validate(gracePeriod);

		this.primesMappingTable = checkNotNull(primesMappingTable);
	}

	public Long getId() {
		return id;
	}

	public String getBallotBoxId() {
		return ballotBoxId;
	}

	public VerificationCardSetEntity getVerificationCardSetEntity() {
		return verificationCardSetEntity;
	}

	public Integer getChangeControlId() {
		return changeControlId;
	}

	public LocalDateTime getBallotBoxStartTime() {
		return ballotBoxStartTime;
	}

	public LocalDateTime getBallotBoxFinishTime() {
		return ballotBoxFinishTime;
	}

	public boolean isMixed() {
		return mixed;
	}

	public void setMixed(final boolean mixed) {
		this.mixed = mixed;
	}

	public boolean isTestBallotBox() {
		return testBallotBox;
	}

	public int getNumberOfVotingCards() {
		return numberOfVotingCards;
	}

	public int getGracePeriod() {
		return gracePeriod;
	}

	public PrimesMappingTable getPrimesMappingTable() {
		return primesMappingTable;
	}

	public static class Builder {

		private String ballotBoxId;
		private VerificationCardSetEntity verificationCardSetEntity;
		private LocalDateTime ballotBoxStartTime;
		private LocalDateTime ballotBoxFinishTime;
		private boolean testBallotBox;
		private int numberOfVotingCards;
		private int gracePeriod;
		private PrimesMappingTable primesMappingTable;

		public Builder setBallotBoxId(final String ballotBoxId) {
			this.ballotBoxId = ballotBoxId;
			return this;
		}

		public Builder setVerificationCardSetEntity(final VerificationCardSetEntity verificationCardSetEntity) {
			this.verificationCardSetEntity = verificationCardSetEntity;
			return this;
		}

		public Builder setBallotBoxStartTime(final LocalDateTime ballotBoxStartTime) {
			this.ballotBoxStartTime = ballotBoxStartTime;
			return this;
		}

		public Builder setBallotBoxFinishTime(final LocalDateTime ballotBoxFinishTime) {
			this.ballotBoxFinishTime = ballotBoxFinishTime;
			return this;
		}

		public Builder setTestBallotBox(final boolean testBallotBox) {
			this.testBallotBox = testBallotBox;
			return this;
		}

		public Builder setNumberOfVotingCards(final int numberOfVotingCards) {
			this.numberOfVotingCards = numberOfVotingCards;
			return this;
		}

		public Builder setGracePeriod(final int gracePeriod) {
			this.gracePeriod = gracePeriod;
			return this;
		}

		public Builder setPrimesMappingTable(final PrimesMappingTable primesMappingTable) {
			this.primesMappingTable = primesMappingTable;
			return this;
		}

		public BallotBoxEntity build() {
			return new BallotBoxEntity(ballotBoxId, verificationCardSetEntity, ballotBoxStartTime, ballotBoxFinishTime, testBallotBox,
					numberOfVotingCards, gracePeriod, primesMappingTable);
		}

	}
}
