/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.domain;

import static com.google.common.base.Preconditions.checkArgument;

import jakarta.persistence.Convert;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.MapsId;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Version;

import ch.post.it.evoting.domain.converters.BooleanConverter;

@Entity
@Table(name = "VERIFICATION_CARD_STATE")
public class VerificationCardStateEntity {

	@Id
	private Long id;

	@OneToOne(fetch = FetchType.LAZY)
	@MapsId
	@JoinColumn(name = "VERIFICATION_CARD_FK_ID")
	private VerificationCardEntity verificationCardEntity;

	@Convert(converter = BooleanConverter.class)
	private boolean partiallyDecrypted = false;

	@Convert(converter = BooleanConverter.class)
	private boolean lccShareCreated = false;

	private int confirmationAttempts = 0;

	@Convert(converter = BooleanConverter.class)
	private boolean confirmed = false;

	@Version
	private Integer changeControlId;

	public VerificationCardStateEntity() {
		// Needed by the repository.
	}

	public void setVerificationCardEntity(final VerificationCardEntity verificationCardEntity) {
		this.verificationCardEntity = verificationCardEntity;
	}

	public boolean isPartiallyDecrypted() {
		return partiallyDecrypted;
	}

	public boolean isLccShareCreated() {
		return lccShareCreated;
	}

	public int getConfirmationAttempts() {
		return confirmationAttempts;
	}

	public boolean isConfirmed() {
		return confirmed;
	}

	public void setPartiallyDecrypted(boolean partiallyDecrypted) {
		this.partiallyDecrypted = partiallyDecrypted;
	}

	public void setLccShareCreated(boolean lccShareCreated) {
		this.lccShareCreated = lccShareCreated;
	}

	public void setConfirmed(final boolean confirmed) {
		this.confirmed = confirmed;
	}

	public void setConfirmationAttempts(int confirmationAttempts) {
		checkArgument(confirmationAttempts >= 0 && confirmationAttempts <= 5);
		this.confirmationAttempts = confirmationAttempts;
	}

}
