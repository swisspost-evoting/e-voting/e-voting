/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ch.post.it.evoting.controlcomponent.domain.CcrjReturnCodesKeysEntity;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
public interface CcrjReturnCodesKeysRepository extends CrudRepository<CcrjReturnCodesKeysEntity, Long> {

	@Query("select e from CcrjReturnCodesKeysEntity e where e.electionEventEntity.electionEventId = ?1")
	Optional<CcrjReturnCodesKeysEntity> findByElectionEventId(final String electionEventId);

}
