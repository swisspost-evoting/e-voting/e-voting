/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.business.tally;

import static ch.post.it.evoting.controlcomponent.business.tally.MixDecryptService.MixDecryptServiceOutput;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.security.SignatureException;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.domain.InvalidPayloadSignatureException;
import ch.post.it.evoting.domain.tally.MixDecryptOnlineRequestPayload;
import ch.post.it.evoting.domain.tally.MixDecryptOnlineResponsePayload;
import ch.post.it.evoting.evotinglibraries.domain.common.ChannelSecurityContextData;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.ControlComponentShufflePayload;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.ControlComponentVotesHashPayload;
import ch.post.it.evoting.evotinglibraries.domain.signature.Alias;
import ch.post.it.evoting.evotinglibraries.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.evotinglibraries.domain.signature.SignedPayload;
import ch.post.it.evoting.evotinglibraries.domain.tally.ControlComponentBallotBoxPayload;

@Service
public class MixDecryptMessagingService {
	private final ObjectMapper objectMapper;
	private final MixDecryptService mixDecryptService;
	private final SignatureKeystore<Alias> signatureKeystoreService;

	@Value("${nodeID}")
	private int nodeId;

	MixDecryptMessagingService(
			final ObjectMapper objectMapper,
			final MixDecryptService mixDecryptService,
			final SignatureKeystore<Alias> signatureKeystoreService) {
		this.objectMapper = objectMapper;
		this.mixDecryptService = mixDecryptService;
		this.signatureKeystoreService = signatureKeystoreService;
	}

	public MixDecryptOnlineResponsePayload generateMixDecryptOnlineResponsePayload(
			final MixDecryptOnlineRequestPayload mixDecryptOnlineRequestPayload) {
		checkNotNull(mixDecryptOnlineRequestPayload);

		checkState(mixDecryptOnlineRequestPayload.nodeId() == nodeId,
				"The payload is not intended for the current control component. [nodeId: {}, payloadNodeId: {}]", nodeId,
				mixDecryptOnlineRequestPayload.nodeId());

		final String electionEventId = mixDecryptOnlineRequestPayload.electionEventId();
		final String ballotBoxId = mixDecryptOnlineRequestPayload.ballotBoxId();

		final List<ControlComponentVotesHashPayload> controlComponentVotesHashPayloads = mixDecryptOnlineRequestPayload.controlComponentVotesHashPayloads();
		final List<ControlComponentShufflePayload> controlComponentShufflePayloads = mixDecryptOnlineRequestPayload.controlComponentShufflePayloads();

		final MixDecryptServiceOutput mixDecryptServiceOutput = mixDecryptService.performMixDecrypt(electionEventId, ballotBoxId,
				controlComponentVotesHashPayloads, controlComponentShufflePayloads);

		final ControlComponentBallotBoxPayload controlComponentBallotBoxPayload = mixDecryptServiceOutput.controlComponentBallotBoxPayload();
		final ControlComponentShufflePayload shufflePayload = mixDecryptServiceOutput.controlComponentShufflePayload();

		controlComponentBallotBoxPayload.setSignature(getPayloadSignature(controlComponentBallotBoxPayload,
				ChannelSecurityContextData.controlComponentBallotBox(nodeId, electionEventId, ballotBoxId)));

		shufflePayload.setSignature(
				getPayloadSignature(shufflePayload,
						ChannelSecurityContextData.controlComponentShuffle(nodeId, electionEventId, ballotBoxId)));

		return new MixDecryptOnlineResponsePayload(controlComponentBallotBoxPayload, shufflePayload);
	}

	public MixDecryptOnlineRequestPayload deserializeRequest(final byte[] messageBytes) {
		checkNotNull(messageBytes);
		final byte[] messageBytesCopy = Arrays.copyOf(messageBytes, messageBytes.length);

		try {
			return objectMapper.readValue(messageBytesCopy, MixDecryptOnlineRequestPayload.class);
		} catch (final IOException e) {
			throw new UncheckedIOException("Unable to deserialize Mix Decrypt Online Request Payload", e);
		}
	}

	public byte[] serializeResponse(final MixDecryptOnlineResponsePayload mixDecryptOnlineResponsePayload) {
		checkNotNull(mixDecryptOnlineResponsePayload);
		try {
			return objectMapper.writeValueAsBytes(mixDecryptOnlineResponsePayload);
		} catch (final IOException e) {
			throw new UncheckedIOException("Unable to serialize Mix Decrypt Online Response Payload", e);
		}

	}

	public boolean verifySignature(final MixDecryptOnlineRequestPayload mixDecryptOnlineRequestPayload) {
		checkNotNull(mixDecryptOnlineRequestPayload);

		mixDecryptOnlineRequestPayload.controlComponentShufflePayloads().forEach(controlComponentShufflePayload -> {
			final int payloadNodeId = controlComponentShufflePayload.getNodeId();
			final String electionEventId = controlComponentShufflePayload.getElectionEventId();
			final String ballotBoxId = controlComponentShufflePayload.getBallotBoxId();
			final String contextMessage = String.format("[nodeId: %s, electionEventId: %s, ballotBoxId: %s]", payloadNodeId, electionEventId,
					ballotBoxId);

			final CryptoPrimitivesSignature signature = controlComponentShufflePayload.getSignature();

			checkState(signature != null, "The signature of the control component shuffle payload is null. %s", contextMessage);

			final Hashable additionalContextData = ChannelSecurityContextData.controlComponentShuffle(payloadNodeId, electionEventId, ballotBoxId);

			final boolean isSignatureValid;
			try {
				isSignatureValid = signatureKeystoreService.verifySignature(Alias.getControlComponentByNodeId(payloadNodeId),
						controlComponentShufflePayload, additionalContextData, signature.signatureContents());
			} catch (final SignatureException e) {
				throw new IllegalStateException(
						String.format("Could not verify the signature of %s. %s", controlComponentShufflePayload.getClass().getSimpleName(),
								contextMessage));
			}

			if (!isSignatureValid) {
				throw new InvalidPayloadSignatureException(ControlComponentShufflePayload.class, contextMessage);
			}
		});

		mixDecryptOnlineRequestPayload.controlComponentVotesHashPayloads().forEach(controlComponentVotesHashPayload -> {
			final String electionEventId = controlComponentVotesHashPayload.getElectionEventId();
			final String ballotBoxId = controlComponentVotesHashPayload.getBallotBoxId();
			final int payloadNodeId = controlComponentVotesHashPayload.getNodeId();
			final String contextMessage = String.format("[nodeId: %s, electionEventId: %s, ballotBoxId: %s]", payloadNodeId, electionEventId,
					ballotBoxId);

			final CryptoPrimitivesSignature signature = controlComponentVotesHashPayload.getSignature();

			checkState(signature != null, "The signature of the control component votes hash payload is null. %s", contextMessage);

			final Hashable additionalContextData = ChannelSecurityContextData.controlComponentVotesHash(payloadNodeId, electionEventId, ballotBoxId);

			final boolean isSignatureValid;
			try {
				isSignatureValid = signatureKeystoreService.verifySignature(Alias.getControlComponentByNodeId(payloadNodeId),
						controlComponentVotesHashPayload, additionalContextData, signature.signatureContents());
			} catch (final SignatureException e) {
				throw new IllegalStateException(
						String.format("Could not verify the signature of %s. %s", controlComponentVotesHashPayload.getClass().getSimpleName(),
								contextMessage));
			}

			if (!isSignatureValid) {
				throw new InvalidPayloadSignatureException(ControlComponentShufflePayload.class, contextMessage);
			}
		});

		return true;
	}

	private CryptoPrimitivesSignature getPayloadSignature(final SignedPayload payload, final Hashable additionalContextData) {

		try {
			final byte[] signature = signatureKeystoreService.generateSignature(payload, additionalContextData);

			return new CryptoPrimitivesSignature(signature);

		} catch (final SignatureException se) {
			throw new IllegalStateException(
					String.format("Failed to generate payload signature [%s, %s]", payload.getClass().getSimpleName(), additionalContextData), se);
		}
	}
}
