/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
-- For column type NUMBER where the value is set by the database, use a type such that DATABASE.TYPE.MAXVALUE <= APPLICATION.TYPE.MAXVALUE.
-- For column type NUMBER where the value is set by the application, use a type such that DATABASE.TYPE.MAXVALUE >= APPLICATION.TYPE.MAXVALUE.

CREATE TABLE ELECTION_EVENT
(
    ID                NUMERIC(18)  NOT NULL,
    ELECTION_EVENT_ID VARCHAR(100) NOT NULL UNIQUE,
    ENCRYPTION_GROUP  BYTEA        NOT NULL,
    CHANGE_CONTROL_ID NUMERIC(10)  NOT NULL,
    CONSTRAINT ELECTION_EVENT_PK PRIMARY KEY (ID),
    CONSTRAINT ELECTION_EVENT_IMMUTABILITY_CK CHECK (CHANGE_CONTROL_ID = 0)
);

CREATE SEQUENCE ELECTION_EVENT_SEQ
    MAXVALUE 9223372000000000000
    NO CYCLE;

CREATE TABLE ELECTION_EVENT_STATE
(
    ELECTION_EVENT_FK_ID NUMERIC(18) NOT NULL,
    STATE                VARCHAR(10) NOT NULL,
    CHANGE_CONTROL_ID    NUMERIC(1)  NOT NULL,
    CONSTRAINT ELECTION_EVENT_STATE_PK PRIMARY KEY (ELECTION_EVENT_FK_ID),
    FOREIGN KEY (ELECTION_EVENT_FK_ID) REFERENCES ELECTION_EVENT (ID),
    CONSTRAINT STATE_CHANGE_CK CHECK (CHANGE_CONTROL_ID <= 1)
);

CREATE TABLE CCR_RETURN_CODES_KEYS
(
    ELECTION_EVENT_FK_ID                         NUMERIC(18) NOT NULL,
    CCRJ_RETURN_CODES_GENERATION_SECRET_KEY      BYTEA       NOT NULL,
    CCRJ_CHOICE_RETURN_CODES_ENCRYPTION_KEY_PAIR BYTEA       NOT NULL,
    CHANGE_CONTROL_ID                            NUMERIC(10) NOT NULL,
    CONSTRAINT CCR_RETURN_CODES_KEYS_PK PRIMARY KEY (ELECTION_EVENT_FK_ID),
    FOREIGN KEY (ELECTION_EVENT_FK_ID) REFERENCES ELECTION_EVENT (ID),
    CONSTRAINT CCR_RETURN_CODES_KEYS_IMMUTABILITY_CK CHECK (CHANGE_CONTROL_ID = 0)
);

CREATE TABLE CCM_ELECTION_KEY
(
    ELECTION_EVENT_FK_ID   NUMERIC(18) NOT NULL,
    CCMJ_ELECTION_KEY_PAIR BYTEA       NOT NULL,
    CHANGE_CONTROL_ID      NUMERIC(10) NOT NULL,
    CONSTRAINT CCM_ELECTION_KEY_PK PRIMARY KEY (ELECTION_EVENT_FK_ID),
    FOREIGN KEY (ELECTION_EVENT_FK_ID) REFERENCES ELECTION_EVENT (ID),
    CONSTRAINT CCM_ELECTION_KEY_IMMUTABILITY_CK CHECK (CHANGE_CONTROL_ID = 0)
);

CREATE TABLE ELECTION_EVENT_CONTEXT
(
    ELECTION_EVENT_FK_ID             NUMERIC(18)              NOT NULL,
    ELECTION_EVENT_ALIAS             VARCHAR(50)              NOT NULL,
    ELECTION_EVENT_DESCRIPTION       VARCHAR(255)             NOT NULL,
    START_TIME                       TIMESTAMP WITH TIME ZONE NOT NULL,
    FINISH_TIME                      TIMESTAMP WITH TIME ZONE NOT NULL,
    MAX_NUMBER_OF_VOTING_OPTIONS     NUMERIC(10)              NOT NULL,
    MAX_NUMBER_OF_SELECTIONS         NUMERIC(10)              NOT NULL,
    MAX_NUMBER_OF_WRITE_INS_PLUS_ONE NUMERIC(10)              NOT NULL,
    CHANGE_CONTROL_ID                NUMERIC(10)              NOT NULL,
    CONSTRAINT ELECTION_EVENT_CONTEXT_PK PRIMARY KEY (ELECTION_EVENT_FK_ID),
    FOREIGN KEY (ELECTION_EVENT_FK_ID) REFERENCES ELECTION_EVENT (ID),
    CONSTRAINT ELECTION_EVENT_CONTEXT_IMMUTABILITY_CK CHECK (CHANGE_CONTROL_ID = 0)
);

CREATE TABLE VERIFICATION_CARD_SET
(
    ID                                NUMERIC(18)  NOT NULL,
    VERIFICATION_CARD_SET_ID          VARCHAR(100) NOT NULL UNIQUE,
    VERIFICATION_CARD_SET_ALIAS       VARCHAR(50)  NOT NULL,
    VERIFICATION_CARD_SET_DESCRIPTION VARCHAR(255) NOT NULL,
    ELECTION_EVENT_FK_ID              NUMERIC(18)  NOT NULL,
    CHANGE_CONTROL_ID                 NUMERIC(10)  NOT NULL,
    CONSTRAINT VERIFICATION_CARD_SET_PK PRIMARY KEY (ID),
    FOREIGN KEY (ELECTION_EVENT_FK_ID) REFERENCES ELECTION_EVENT (ID),
    CONSTRAINT VERIFICATION_CARD_SET_IMMUTABILITY_CK CHECK ( CHANGE_CONTROL_ID = 0 )
);

CREATE SEQUENCE VERIFICATION_CARD_SET_SEQ
    MAXVALUE 9223372000000000000
    NO CYCLE;

CREATE TABLE LVCC_ALLOW_LIST_ENTRY
(
    VERIFICATION_CARD_SET_FK_ID NUMERIC(18) NOT NULL,
    LONG_VOTE_CAST_RETURN_CODE  VARCHAR(48) NOT NULL,
    CHANGE_CONTROL_ID           NUMERIC(1)  NOT NULL,
    CONSTRAINT LONG_VOTE_CAST_RETURN_CODE_PK PRIMARY KEY (LONG_VOTE_CAST_RETURN_CODE),
    FOREIGN KEY (VERIFICATION_CARD_SET_FK_ID) REFERENCES VERIFICATION_CARD_SET (ID),
    CONSTRAINT LONG_VOTE_CAST_RETURN_CODE_ENTITY_CK CHECK (CHANGE_CONTROL_ID = 0)
);

CREATE TABLE PCC_ALLOW_LIST_ENTRY
(
    VERIFICATION_CARD_SET_FK_ID NUMERIC(18) NOT NULL,
    PARTIAL_CHOICE_RETURN_CODE  VARCHAR(44) NOT NULL,
    CHUNK_ID                    NUMERIC(10) NOT NULL,
    CHANGE_CONTROL_ID           NUMERIC(1)  NOT NULL,
    CONSTRAINT PARTIAL_CHOICE_RETURN_CODE_ENTITY_PK PRIMARY KEY (PARTIAL_CHOICE_RETURN_CODE),
    FOREIGN KEY (VERIFICATION_CARD_SET_FK_ID) REFERENCES VERIFICATION_CARD_SET (ID),
    CONSTRAINT CHUNK_POSITIVITY_CK CHECK (CHUNK_ID >= 0),
    CONSTRAINT PARTIAL_CHOICE_RETURN_CODE_ENTITY_CK CHECK (CHANGE_CONTROL_ID = 0)
);

CREATE TABLE VERIFICATION_CARD
(
    ID                           NUMERIC(18)  NOT NULL,
    VERIFICATION_CARD_ID         VARCHAR(100) NOT NULL UNIQUE,
    VERIFICATION_CARD_SET_FK_ID  NUMERIC(18)  NOT NULL,
    VERIFICATION_CARD_PUBLIC_KEY BYTEA        NOT NULL,
    CHANGE_CONTROL_ID            NUMERIC(10)  NOT NULL,
    CONSTRAINT VERIFICATION_CARD_PK PRIMARY KEY (ID),
    FOREIGN KEY (VERIFICATION_CARD_SET_FK_ID) REFERENCES VERIFICATION_CARD_SET (ID),
    CONSTRAINT VERIFICATION_CARD_IMMUTABILITY_CK CHECK (CHANGE_CONTROL_ID = 0)
);

CREATE SEQUENCE VERIFICATION_CARD_SEQ
    MAXVALUE 9223372000000000000
    NO CYCLE;

CREATE TABLE VERIFICATION_CARD_STATE
(
    VERIFICATION_CARD_FK_ID NUMERIC(18)            NOT NULL,
    PARTIALLY_DECRYPTED     CHAR(1)    DEFAULT 'N' NOT NULL,
    LCC_SHARE_CREATED       CHAR(1)    DEFAULT 'N' NOT NULL,
    CONFIRMATION_ATTEMPTS   NUMERIC(1) DEFAULT 0   NOT NULL,
    CONFIRMED               CHAR(1)    DEFAULT 'N' NOT NULL,
    CHANGE_CONTROL_ID       NUMERIC(10)            NOT NULL,
    CONSTRAINT VERIFICATION_CARD_STATE_PK PRIMARY KEY (VERIFICATION_CARD_FK_ID),
    FOREIGN KEY (VERIFICATION_CARD_FK_ID) REFERENCES VERIFICATION_CARD (ID)
);

CREATE TABLE SETUP_COMPONENT_PUBLIC_KEYS
(
    ELECTION_EVENT_FK_ID                      NUMERIC(18) NOT NULL,
    COMBINED_CONTROL_COMPONENT_PUBLIC_KEYS    BYTEA       NOT NULL,
    ELECTORAL_BOARD_PUBLIC_KEY                BYTEA       NOT NULL,
    ELECTORAL_BOARD_SCHNORR_PROOFS            BYTEA       NOT NULL,
    ELECTION_PUBLIC_KEY                       BYTEA       NOT NULL,
    CHOICE_RETURN_CODES_ENCRYPTION_PUBLIC_KEY BYTEA       NOT NULL,
    CHANGE_CONTROL_ID                         NUMERIC(10) NOT NULL,
    CONSTRAINT SETUP_COMPONENT_PUBLIC_KEYS_PK PRIMARY KEY (ELECTION_EVENT_FK_ID),
    FOREIGN KEY (ELECTION_EVENT_FK_ID) REFERENCES ELECTION_EVENT (ID),
    CONSTRAINT SETUP_COMPONENT_PUBLIC_KEYS_IMMUTABILITY_CK CHECK (CHANGE_CONTROL_ID = 0)
);

CREATE TABLE BALLOT_BOX
(
    VERIFICATION_CARD_SET_FK_ID NUMERIC(18),
    BALLOT_BOX_ID               VARCHAR(100)             NOT NULL UNIQUE,
    BALLOT_BOX_START_TIME       TIMESTAMP WITH TIME ZONE NOT NULL,
    BALLOT_BOX_FINISH_TIME      TIMESTAMP WITH TIME ZONE NOT NULL,
    MIXED                       CHAR(1) DEFAULT 'N'      NOT NULL,
    TEST_BALLOT_BOX             CHAR(1) DEFAULT 'N'      NOT NULL,
    NUMBER_OF_VOTING_CARDS      NUMERIC(10),
    GRACE_PERIOD                NUMERIC(10),
    PRIMES_MAPPING_TABLE        BYTEA                    NOT NULL,
    CHANGE_CONTROL_ID           NUMERIC(10)              NOT NULL,
    CONSTRAINT BALLOT_BOX_PK PRIMARY KEY (VERIFICATION_CARD_SET_FK_ID),
    FOREIGN KEY (VERIFICATION_CARD_SET_FK_ID) REFERENCES VERIFICATION_CARD_SET (ID)
);

CREATE TABLE PARTIALLY_DECRYPTED_PCC
(
    VERIFICATION_CARD_FK_ID           NUMERIC(18) NOT NULL,
    PARTIALLY_DECRYPTED_ENCRYPTED_PCC BYTEA       NOT NULL,
    CHANGE_CONTROL_ID                 NUMERIC(10) NOT NULL,
    CONSTRAINT PARTIALLY_DECRYPTED_PCC_PK PRIMARY KEY (VERIFICATION_CARD_FK_ID),
    FOREIGN KEY (VERIFICATION_CARD_FK_ID) REFERENCES VERIFICATION_CARD (ID),
    CONSTRAINT PARTIALLY_DECRYPTED_PCC_IMMUTABILITY_CK CHECK (CHANGE_CONTROL_ID = 0)
);

CREATE TABLE ENCRYPTED_VERIFIABLE_VOTE
(
    VERIFICATION_CARD_FK_ID               NUMERIC(18) NOT NULL,
    CONTEXT_IDS                           BYTEA       NOT NULL,
    ENCRYPTED_VOTE                        BYTEA       NOT NULL,
    EXPONENTIATED_ENCRYPTED_VOTE          BYTEA       NOT NULL,
    ENCRYPTED_PARTIAL_CHOICE_RETURN_CODES BYTEA       NOT NULL,
    EXPONENTIATION_PROOF                  BYTEA       NOT NULL,
    PLAINTEXT_EQUALITY_PROOF              BYTEA       NOT NULL,
    CHANGE_CONTROL_ID                     NUMERIC(10) NOT NULL,
    CONSTRAINT ENCRYPTED_VERIFIABLE_VOTE_PK PRIMARY KEY (VERIFICATION_CARD_FK_ID),
    FOREIGN KEY (VERIFICATION_CARD_FK_ID) REFERENCES VERIFICATION_CARD (ID),
    CONSTRAINT ENCRYPTED_VERIFIABLE_VOTE_IMMUTABILITY_CK CHECK (CHANGE_CONTROL_ID = 0)
);

CREATE TABLE LONG_VOTE_CAST_RETURN_CODES_SHARE
(
    ID                               NUMERIC(18)   NOT NULL,
    CONFIRMATION_KEY                 VARCHAR(1000) NOT NULL,
    VERIFICATION_CARD_FK_ID          NUMERIC(18)   NOT NULL,
    LONG_VOTE_CAST_RETURN_CODE_SHARE BYTEA         NOT NULL,
    CONFIRMATION_ATTEMPTS            NUMERIC(1)    NOT NULL,
    CHANGE_CONTROL_ID                NUMERIC(10)   NOT NULL,
    CONSTRAINT LONG_VOTE_CAST_RETURN_CODES_SHARE_PK PRIMARY KEY (ID),
    FOREIGN KEY (VERIFICATION_CARD_FK_ID) REFERENCES VERIFICATION_CARD (ID),
    CONSTRAINT LONG_VOTE_CAST_RETURN_CODES_SHARE_IMMUTABILITY_CK CHECK (CHANGE_CONTROL_ID = 0),
    CONSTRAINT LONG_VOTE_CAST_RETURN_CODES_SHARE_UK UNIQUE (VERIFICATION_CARD_FK_ID, CONFIRMATION_ATTEMPTS),
    CONSTRAINT LONG_VOTE_CAST_RETURN_CODES_SHARE_ATTEMPTS_CK CHECK (CONFIRMATION_ATTEMPTS >= 1 and CONFIRMATION_ATTEMPTS <= 5)
);

CREATE SEQUENCE LONG_VOTE_CAST_RETURN_CODES_SHARE_SEQ
    MAXVALUE 9223372000000000000
    NO CYCLE;

CREATE TABLE COMMAND
(
    CONTEXT_ID         VARCHAR(255) NOT NULL,
    CONTEXT            VARCHAR(255) NOT NULL,
    CORRELATION_ID     VARCHAR(255) NOT NULL,
    NODE_ID            NUMERIC(10)  NOT NULL,
    REQUEST_PAYLOAD    BYTEA        NOT NULL,
    REQUEST_DATE_TIME  TIMESTAMP(6) NOT NULL,
    RESPONSE_PAYLOAD   BYTEA        NOT NULL,
    RESPONSE_DATE_TIME TIMESTAMP(6) NOT NULL,
    CHANGE_CONTROL_ID  NUMERIC(10)  NOT NULL,
    PRIMARY KEY (CONTEXT_ID, CONTEXT, CORRELATION_ID, NODE_ID),
    CONSTRAINT COMMAND_UK UNIQUE (CONTEXT_ID, CONTEXT, NODE_ID),
    CONSTRAINT COMMAND_IMMUTABILITY_CK CHECK (CHANGE_CONTROL_ID = 0)
);

CREATE TABLE MIXNET_INITIAL_CIPHERTEXTS
(
    ID                             NUMERIC(18)   NOT NULL,
    ELECTION_EVENT_ID              VARCHAR(100)  NOT NULL,
    BALLOT_BOX_ID                  VARCHAR(100)  NOT NULL UNIQUE,
    ENCRYPTED_CONFIRMED_VOTES_HASH VARCHAR(1000) NOT NULL,
    MIXNET_INITIAL_CIPHERTEXTS     BYTEA         NOT NULL,
    CHANGE_CONTROL_ID              NUMERIC(10)   NOT NULL,
    CONSTRAINT MIXNET_INITIAL_CIPHERTEXTS_PK PRIMARY KEY (ID),
    CONSTRAINT MIXNET_INITIAL_CIPHERTEXTS_UK UNIQUE (ELECTION_EVENT_ID, BALLOT_BOX_ID),
    CONSTRAINT MIXNET_INITIAL_CIPHERTEXTS_IMMUTABILITY_CK CHECK (CHANGE_CONTROL_ID = 0)
);

CREATE SEQUENCE MIXNET_INITIAL_CIPHERTEXTS_SEQ
    MAXVALUE 9223372000000000000
    NO CYCLE;
