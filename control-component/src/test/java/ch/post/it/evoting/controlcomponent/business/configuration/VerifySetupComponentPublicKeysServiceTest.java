/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.business.configuration;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.google.common.collect.Streams;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamal;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalFactory;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.math.ZqElement;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.test.tools.TestGroupSetup;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.SchnorrProof;
import ch.post.it.evoting.evotinglibraries.domain.election.ControlComponentPublicKeys;
import ch.post.it.evoting.evotinglibraries.domain.election.SetupComponentPublicKeys;

@DisplayName("VerifySetupComponentPublicKeysService calling")
class VerifySetupComponentPublicKeysServiceTest extends TestGroupSetup {

	private static final Random RANDOM = RandomFactory.createRandom();
	private static final ElGamal elGamal = ElGamalFactory.createElGamal();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();

	private static VerifySetupComponentPublicKeysService verifySetupComponentPublicKeysService;

	private String electionEventId;
	private SetupComponentPublicKeys setupComponentPublicKeys;

	@BeforeAll
	static void setupAll() {
		verifySetupComponentPublicKeysService = new VerifySetupComponentPublicKeysService(null, null, null, null, null);
	}

	@BeforeEach
	void setup() {
		electionEventId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		setupComponentPublicKeys = createSetupComponentPublicKeys(5);
	}

	@Test
	@DisplayName("verifySetupComponentPublicKeys with null arguments throws a NullPointerException")
	void verifySetupComponentPublicKeysWithNullArgumentsThrows() {
		assertThrows(NullPointerException.class,
				() -> verifySetupComponentPublicKeysService.verifySetupComponentPublicKeys(null, setupComponentPublicKeys));
		assertThrows(NullPointerException.class, () -> verifySetupComponentPublicKeysService.verifySetupComponentPublicKeys(electionEventId, null));
	}

	private SetupComponentPublicKeys createSetupComponentPublicKeys(final int l) {
		final List<ControlComponentPublicKeys> combinedControlComponentPublicKeys = new ArrayList<>();

		IntStream.rangeClosed(1, 4)
				.forEach(nodeId -> combinedControlComponentPublicKeys.add(generateCombinedControlComponentPublicKeys(nodeId, l)));

		final ElGamalMultiRecipientPublicKey electoralBoardPublicKey = elGamalGenerator.genRandomPublicKey(l);

		final GroupVector<SchnorrProof, ZqGroup> schnorrProofs = generateSchnorrProofs(l);

		final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> ccrChoiceReturnCodePublicKeys = combinedControlComponentPublicKeys.stream()
				.map(ControlComponentPublicKeys::ccrjChoiceReturnCodesEncryptionPublicKey).collect(GroupVector.toGroupVector());

		final ElGamalMultiRecipientPublicKey choiceReturnCodesPublicKey = elGamal.combinePublicKeys(ccrChoiceReturnCodePublicKeys);

		final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> ccmElectionPublicKeys = Streams.concat(
				combinedControlComponentPublicKeys.stream()
						.map(ControlComponentPublicKeys::ccmjElectionPublicKey),
				Stream.of(electoralBoardPublicKey)).collect(GroupVector.toGroupVector());

		final ElGamalMultiRecipientPublicKey electionPublicKey = elGamal.combinePublicKeys(ccmElectionPublicKeys);

		return new SetupComponentPublicKeys(combinedControlComponentPublicKeys, electoralBoardPublicKey, schnorrProofs, electionPublicKey,
				choiceReturnCodesPublicKey);
	}

	private ControlComponentPublicKeys generateCombinedControlComponentPublicKeys(final int nodeId, final int l) {
		final ElGamalMultiRecipientPublicKey ccrChoiceReturnCodesEncryptionPublicKey = elGamalGenerator.genRandomPublicKey(l);
		final ElGamalMultiRecipientPublicKey ccmElectionPublicKey = elGamalGenerator.genRandomPublicKey(l);

		final GroupVector<SchnorrProof, ZqGroup> schnorrProofs = generateSchnorrProofs(l);

		return new ControlComponentPublicKeys(nodeId, ccrChoiceReturnCodesEncryptionPublicKey, schnorrProofs, ccmElectionPublicKey, schnorrProofs);
	}

	private GroupVector<SchnorrProof, ZqGroup> generateSchnorrProofs(final int l) {
		final ZqGroup zqGroup = ZqGroup.sameOrderAs(gqGroup);
		final ZqElement e = ZqElement.create(2, zqGroup);
		final ZqElement z = ZqElement.create(2, zqGroup);
		final SchnorrProof schnorrProof = new SchnorrProof(e, z);
		return Collections.nCopies(l, schnorrProof).stream().collect(GroupVector.toGroupVector());
	}
}
