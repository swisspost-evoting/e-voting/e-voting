/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.domain;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDateTime;

import org.junit.jupiter.api.Test;

import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.evotinglibraries.domain.election.PrimesMappingTable;
import ch.post.it.evoting.evotinglibraries.domain.election.generators.PrimesMappingTableGenerator;

class BallotBoxEntityTest {

	private static final Random random = RandomFactory.createRandom();

	@Test
	void constructWithoutParametersDoesNotThrow() {
		final BallotBoxEntity ballotBoxEntity = new BallotBoxEntity();
		assertFalse(ballotBoxEntity.isMixed());
		assertFalse(ballotBoxEntity.isTestBallotBox());
	}

	@Test
	void constructWithValidParametersDoesNotThrow() {
		final Alphabet base16Alphabet = Base16Alphabet.getInstance();
		final String ballotBoxId = random.genRandomString(ID_LENGTH, base16Alphabet);
		final VerificationCardSetEntity verificationCardSetEntity = new VerificationCardSetEntity();

		final PrimesMappingTable primesMappingTable = new PrimesMappingTableGenerator().generate(2);

		final BallotBoxEntity ballotBoxEntity = new BallotBoxEntity.Builder()
				.setBallotBoxId(ballotBoxId)
				.setVerificationCardSetEntity(verificationCardSetEntity)
				.setBallotBoxStartTime(LocalDateTime.now().minusDays(1))
				.setBallotBoxFinishTime(LocalDateTime.now().plusDays(5))
				.setTestBallotBox(true)
				.setNumberOfVotingCards(10).setGracePeriod(900)
				.setPrimesMappingTable(primesMappingTable)
				.build();
		assertEquals(ballotBoxId, ballotBoxEntity.getBallotBoxId());
		assertEquals(verificationCardSetEntity, ballotBoxEntity.getVerificationCardSetEntity());
		assertFalse(ballotBoxEntity.isMixed());
		assertTrue(ballotBoxEntity.isTestBallotBox());
	}
}
