/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.service;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import ch.post.it.evoting.controlcomponent.TestKeyStoreInitializer;
import ch.post.it.evoting.controlcomponent.domain.CcrjReturnCodesKeys;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientKeyPair;
import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.math.ZqElement;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ZqGroupGenerator;

@SpringBootTest
@ContextConfiguration(initializers = TestKeyStoreInitializer.class)
@ActiveProfiles("test")
@DisplayName("CcrjReturnCodesKeysService")
class CcrjReturnCodesKeysServiceIT {

	private static final Random random = RandomFactory.createRandom();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();

	private static String electionEventId;
	private static GqGroup encryptionGroup;

	@Autowired
	private CcrjReturnCodesKeysService ccrjReturnCodesKeysService;

	@BeforeAll
	static void setUpAll(
			@Autowired
			final ElectionEventService electionEventService) {

		electionEventId = random.genRandomString(ID_LENGTH, base16Alphabet);
		encryptionGroup = GroupTestData.getGqGroup();
		electionEventService.save(electionEventId, encryptionGroup);
	}

	@Test
	@DisplayName("return codes keys saves to database")
	void save() {
		final ZqGroupGenerator zqGroupGenerator = new ZqGroupGenerator(ZqGroup.sameOrderAs(encryptionGroup));
		final ZqElement ccrjReturnCodesGenerationSecretKey = zqGroupGenerator.genRandomZqElementMember();
		final ElGamalMultiRecipientKeyPair ccrjChoiceReturnCodesEncryptionKeyPair = ElGamalMultiRecipientKeyPair.genKeyPair(encryptionGroup, 1,
				RandomFactory.createRandom());
		final CcrjReturnCodesKeys ccrjReturnCodesKeys = new CcrjReturnCodesKeys(electionEventId, ccrjReturnCodesGenerationSecretKey,
				ccrjChoiceReturnCodesEncryptionKeyPair);

		assertDoesNotThrow(() -> ccrjReturnCodesKeysService.save(ccrjReturnCodesKeys));
	}
}