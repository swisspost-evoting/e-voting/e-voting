/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.domain;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.google.common.base.Throwables;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientKeyPair;
import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.math.ZqElement;
import ch.post.it.evoting.cryptoprimitives.test.tools.TestGroupSetup;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;

@DisplayName("CcrjReturnCodesKeys calling")
class CcrjReturnCodesKeysTest extends TestGroupSetup {

	private static final Random RANDOM = RandomFactory.createRandom();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();

	private String electionEventId;
	private ZqElement ccrjReturnCodesGenerationSecretKey;
	private ElGamalMultiRecipientKeyPair ccrjChoiceReturnCodesEncryptionKeyPair;

	@BeforeEach
	void setup() {
		electionEventId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		ccrjReturnCodesGenerationSecretKey = zqGroupGenerator.genRandomZqElementMember();
		ccrjChoiceReturnCodesEncryptionKeyPair = elGamalGenerator.genRandomKeyPair(5);
	}

	@Test
	@DisplayName("constructor with null arguments throws a NullPointerException")
	void constructWithNullArgumentsThrows() {
		assertThrows(NullPointerException.class,
				() -> new CcrjReturnCodesKeys(null, ccrjReturnCodesGenerationSecretKey, ccrjChoiceReturnCodesEncryptionKeyPair));
		assertThrows(NullPointerException.class, () -> new CcrjReturnCodesKeys(electionEventId, null, ccrjChoiceReturnCodesEncryptionKeyPair));
		assertThrows(NullPointerException.class, () -> new CcrjReturnCodesKeys(electionEventId, ccrjReturnCodesGenerationSecretKey, null));
	}

	@Test
	@DisplayName("constructor with invalid UUID throws a FailedValidationException")
	void constructWithInvalidUuidThrows() {
		final String badElectionEventId = "NOT_A_UUID";
		assertThrows(FailedValidationException.class,
				() -> new CcrjReturnCodesKeys(badElectionEventId, ccrjReturnCodesGenerationSecretKey, ccrjChoiceReturnCodesEncryptionKeyPair));
	}

	@Test
	@DisplayName("constructor with generation key and encryption key pair having different group order throws an IllegalArgumentException")
	void constructWithDifferentGroupOrdersThrows() {
		final ZqElement otherCcrjReturnCodesGenerationSecretKey = otherZqGroupGenerator.genRandomZqElementMember();
		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new CcrjReturnCodesKeys(electionEventId, otherCcrjReturnCodesGenerationSecretKey, ccrjChoiceReturnCodesEncryptionKeyPair));
		assertEquals("The generation secret key must have the same order as the encryption key pair.",
				Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("constructor with valid arguments does not throw")
	void constructWithValidArgumentsDoesNotThrow() {
		final CcrjReturnCodesKeys ccrjReturnCodesKeys = assertDoesNotThrow(
				() -> new CcrjReturnCodesKeys(electionEventId, ccrjReturnCodesGenerationSecretKey, ccrjChoiceReturnCodesEncryptionKeyPair));
		assertEquals(electionEventId, ccrjReturnCodesKeys.electionEventId());
		assertEquals(ccrjReturnCodesGenerationSecretKey, ccrjReturnCodesKeys.ccrjReturnCodesGenerationSecretKey());
		assertEquals(ccrjChoiceReturnCodesEncryptionKeyPair, ccrjReturnCodesKeys.ccrjChoiceReturnCodesEncryptionKeyPair());
	}
}
