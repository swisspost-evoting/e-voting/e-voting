/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Function;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Throwables;

import ch.post.it.evoting.controlcomponent.commandmessaging.CommandId;
import ch.post.it.evoting.controlcomponent.commandmessaging.CommandService;
import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base32Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base64Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;

@SpringBootTest
@ContextConfiguration(initializers = TestKeyStoreInitializer.class)
@ActiveProfiles("test")
@DisplayName("ExactlyOnceCommandExecutor calling")
class ExactlyOnceCommandExecutorIT {

	private static final String BAD_INPUT_ID = "Bad input";
	private static final Random random = RandomFactory.createRandom();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();
	private static final Alphabet base32Alphabet = Base32Alphabet.getInstance();
	private static final Alphabet base64Alphabet = Base64Alphabet.getInstance();

	@Value("${nodeID}")
	private int nodeId;

	@SpyBean
	private CommandService commandService;

	@SpyBean
	private ObjectMapper objectMapper;

	@SpyBean
	private ExactlyOnceCommandExecutor processor;

	private String correlationId;
	private String contextId;
	private String context;

	@BeforeEach
	void setup() {
		correlationId = random.genRandomString(ID_LENGTH, base64Alphabet);

		contextId = random.genRandomString(ID_LENGTH, base32Alphabet);
		context = random.genRandomString(ID_LENGTH, base16Alphabet);
	}

	@Test
	@DisplayName("processExactlyOnce with null argument does not save")
	void testProcessExactlyOnceWithNullArgumentsThrows() {
		assertThrows(NullPointerException.class, () -> processor.process(null));
	}

	@Test
	@DisplayName("processExactlyOnce with process throwing an exception does roll back correctly")
	void testProcessExactlyOnceWithProcessThrowingExceptionRollsBackCorrectly() throws JsonProcessingException {

		final CommandId commandId = CommandId.builder()
				.contextId(contextId)
				.context(context)
				.correlationId(correlationId)
				.nodeId(nodeId)
				.build();

		assertFalse(commandService.findIdenticalCommand(commandId).isPresent());

		final TestPayload payload = new TestPayload(BAD_INPUT_ID);
		final byte[] payloadBytes = objectMapper.writeValueAsBytes(payload);
		final Callable<byte[]> callable = () -> getTestPayloadBytes(payload);
		final ExactlyOnceCommand<byte[]> processingInput = new ExactlyOnceCommand.Builder<byte[]>()
				.setCorrelationId(correlationId)
				.setContextId(contextId)
				.setContext(context)
				.setTask(callable)
				.setRequestContent(payloadBytes)
				.setSerializer(Function.identity())
				.build();

		final IllegalStateException exception = assertThrows(IllegalStateException.class,
				() -> processor.process(processingInput));
		assertEquals("Failed to execute exactly once command", exception.getMessage());
		assertEquals(BAD_INPUT_ID, Throwables.getRootCause(exception).getMessage());

		assertFalse(commandService.findIdenticalCommand(commandId).isPresent());
	}

	@Test
	@DisplayName("processExactlyOnce with processing function returning a payload saves request and response")
	void testProcessExactlyOnceWithCallableReturningPayloadSavesResponseCorrectly() throws IOException {
		final TestPayload testPayload = new TestPayload("PayloadToBeSaved1");
		final byte[] testPayloadBytes = objectMapper.writeValueAsBytes(testPayload);
		final Callable<byte[]> callable = () -> getTestPayloadBytes(testPayload);
		final ExactlyOnceCommand<byte[]> processingInput = new ExactlyOnceCommand.Builder<byte[]>()
				.setCorrelationId(correlationId)
				.setContextId(contextId)
				.setContext(context)
				.setTask(callable)
				.setRequestContent(testPayloadBytes)
				.setSerializer(Function.identity())
				.build();

		final CommandId commandId = CommandId.builder()
				.contextId(contextId)
				.context(context)
				.correlationId(correlationId)
				.nodeId(nodeId)
				.build();

		assertFalse(commandService.findIdenticalCommand(commandId).isPresent());

		final byte[] payloadBytes = assertDoesNotThrow(() -> processor.process(processingInput));

		assertTrue(commandService.findIdenticalCommand(commandId).isPresent());
		assertEquals(objectMapper.writeValueAsString(testPayload), new String(payloadBytes));
	}

	@Test
	@DisplayName("processExactlyOnce twice with exactly the same message, saves request and response only once")
	void testProcessExactlyOnceWithMessageAlreadySavedDoesNotCallAgain() throws JsonProcessingException {
		final TestPayload testPayload = new TestPayload("PayloadToBeSaved2");
		final byte[] testPayloadBytes = objectMapper.writeValueAsBytes(testPayload);
		final Callable<byte[]> callable = () -> getTestPayloadBytes(testPayload);

		final CommandId commandId = CommandId.builder()
				.contextId(contextId)
				.context(context)
				.correlationId(correlationId)
				.nodeId(nodeId)
				.build();

		assertFalse(commandService.findIdenticalCommand(commandId).isPresent());

		// 1. Call
		final ExactlyOnceCommand<byte[]> task = new ExactlyOnceCommand.Builder<byte[]>()
				.setCorrelationId(correlationId)
				.setContextId(contextId)
				.setContext(context)
				.setTask(callable)
				.setRequestContent(testPayloadBytes)
				.setSerializer(Function.identity())
				.build();
		final byte[] responseBytes1 = assertDoesNotThrow(() -> processor.process(task));

		verify(commandService, times(1)).save(any(), any(), any(), any(), any());
		assertTrue(commandService.findIdenticalCommand(commandId).isPresent());
		assertEquals(objectMapper.writeValueAsString(testPayload), new String(responseBytes1));

		// 2.call
		final ExactlyOnceCommand<byte[]> throwingTask = new ExactlyOnceCommand.Builder<byte[]>()
				.setCorrelationId(correlationId)
				.setContextId(contextId)
				.setContext(context)
				.setTask(() -> {
					throw new NullPointerException("This should not be thrown.");
				})
				.setRequestContent(testPayloadBytes)
				.setSerializer(Function.identity())
				.build();

		final byte[] responseBytes2 = assertDoesNotThrow(() -> processor.process(throwingTask));

		assertTrue(commandService.findIdenticalCommand(commandId).isPresent());
		assertEquals(objectMapper.writeValueAsString(testPayload), new String(responseBytes2));

	}

	@Test
	@DisplayName("processExactlyOnce twice with the same message but different correlation id throws for one")
	void twiceWithSameMessageButDifferentCorrelationIds() throws JsonProcessingException {
		final TestPayload testPayload = new TestPayload("PayloadToBeSavedDifferentCorrelationId");
		final byte[] testPayloadBytes = objectMapper.writeValueAsBytes(testPayload);

		final CommandId commandId = CommandId.builder()
				.contextId(contextId)
				.context(context)
				.correlationId(correlationId)
				.nodeId(nodeId)
				.build();

		final String differentCorrelationId = random.genRandomString(ID_LENGTH, base64Alphabet);
		final CommandId differentCommandId = CommandId.builder()
				.contextId(contextId)
				.context(context)
				.correlationId(differentCorrelationId)
				.nodeId(nodeId)
				.build();

		assertFalse(commandService.findIdenticalCommand(commandId).isPresent());
		assertFalse(commandService.findIdenticalCommand(differentCommandId).isPresent());

		// Use a latch to synchronize threads, ensuring both are concurrently trying to save their command.
		final CountDownLatch countDownLatch = new CountDownLatch(2);

		// 1. Call
		final Callable<byte[]> firstTask = () -> {
			countDownLatch.countDown();
			countDownLatch.await();
			return getTestPayloadBytes(testPayload);
		};
		final ExactlyOnceCommand<byte[]> firstExactlyOnceCommand = new ExactlyOnceCommand.Builder<byte[]>()
				.setCorrelationId(correlationId)
				.setContextId(contextId)
				.setContext(context)
				.setTask(firstTask)
				.setRequestContent(testPayloadBytes)
				.setSerializer(Function.identity())
				.build();
		final Runnable firstProcess = () -> processor.process(firstExactlyOnceCommand);

		// 2. Call
		final Callable<byte[]> secondTask = () -> {
			countDownLatch.countDown();
			countDownLatch.await();
			return getTestPayloadBytes(testPayload);
		};
		final ExactlyOnceCommand<byte[]> secondExactlyOnceCommand = new ExactlyOnceCommand.Builder<byte[]>()
				.setCorrelationId(differentCorrelationId)
				.setContextId(contextId)
				.setContext(context)
				.setTask(secondTask)
				.setRequestContent(testPayloadBytes)
				.setSerializer(Function.identity())
				.build();
		final Runnable secondProcess = () -> processor.process(secondExactlyOnceCommand);

		// Execute the two process asynchronously.
		final ExecutorService executorService = Executors.newFixedThreadPool(2);
		final CompletableFuture<Void> firstCompletableFuture = CompletableFuture.runAsync(firstProcess, executorService);
		final CompletableFuture<Void> secondCompletableFuture = CompletableFuture.runAsync(secondProcess, executorService);

		// Block until both are completed. One should fail and complete exceptionally.
		final CompletableFuture<Void> allCompletableFuture = CompletableFuture.allOf(firstCompletableFuture, secondCompletableFuture);
		final CompletionException completionException = assertThrows(CompletionException.class, allCompletableFuture::join);

		// Assert the constraint violation happens because they have same (CONTEXT_ID, CONTEXT, NODE_ID).
		assertTrue(completionException.getCause() instanceof DataIntegrityViolationException);
		assertTrue(Throwables.getRootCause(completionException).getMessage()
				.contains("ERROR: duplicate key value violates unique constraint \"command_uk\""));

		// Verify save was indeed called for both commands.
		verify(commandService).save(eq(commandId), any(), any(), any(), any());
		verify(commandService).save(eq(differentCommandId), any(), any(), any(), any());

		// Exactly one command must be present.
		final boolean commandPresent = commandService.findIdenticalCommand(commandId).isPresent();
		final boolean differentCommandPresent = commandService.findIdenticalCommand(differentCommandId).isPresent();

		assertTrue(commandPresent || differentCommandPresent);
		assertFalse(commandPresent && differentCommandPresent);
	}

	@Test
	@DisplayName("processExactlyOnce with same message twice but with different message content throws an exception")
	void testProcessExactlyOnceWithMessageAlreadySavedButContentDifferentThrows() throws JsonProcessingException {
		final TestPayload payload = new TestPayload("PayloadToBeSaved3");
		final byte[] payloadBytes = objectMapper.writeValueAsBytes(payload);
		final Callable<byte[]> callable = () -> getTestPayloadBytes(payload);
		final ExactlyOnceCommand<byte[]> processingInput = new ExactlyOnceCommand.Builder<byte[]>()
				.setCorrelationId(correlationId)
				.setContextId(contextId)
				.setContext(context)
				.setTask(callable)
				.setRequestContent(payloadBytes)
				.setSerializer(Function.identity())
				.build();

		final CommandId commandId = CommandId.builder()
				.contextId(contextId)
				.context(context)
				.correlationId(correlationId)
				.nodeId(nodeId)
				.build();

		assertFalse(commandService.findIdenticalCommand(commandId).isPresent());

		// 1. Call
		assertDoesNotThrow(() -> processor.process(processingInput));
		assertTrue(commandService.findIdenticalCommand(commandId).isPresent());

		// 2.call
		final byte[] differentMessageBytes = new byte[] { 0b0000101 };
		final Callable<byte[]> differentCallable = () -> differentMessageBytes;
		final ExactlyOnceCommand<byte[]> differentProcessingInput = new ExactlyOnceCommand.Builder<byte[]>()
				.setCorrelationId(correlationId)
				.setContextId(contextId)
				.setContext(context)
				.setTask(differentCallable)
				.setRequestContent(differentMessageBytes)
				.setSerializer(Function.identity())
				.build();

		final IllegalStateException exception = assertThrows(IllegalStateException.class, () -> processor.process(differentProcessingInput));
		final String expectedErrorMessage = String.format(
				"Similar request previously treated but for different request payload. [correlationId: %s, contextId: %s, context: %s, nodeId: %s]",
				correlationId, contextId, context, nodeId);
		assertEquals(expectedErrorMessage, Throwables.getRootCause(exception).getMessage());
		assertTrue(commandService.findIdenticalCommand(commandId).isPresent());

	}

	private byte[] getTestPayloadBytes(final TestPayload testPayload) throws JsonProcessingException {
		if (testPayload.id().equals(BAD_INPUT_ID)) {
			throw new IllegalStateException(BAD_INPUT_ID);
		}
		return objectMapper.writeValueAsBytes(testPayload);
	}

	record TestPayload(@JsonProperty String id) {

		@JsonCreator
		TestPayload(
				@JsonProperty("id")
				final String id) {
			this.id = id;
		}

		@Override
		public String id() {
			return this.id;
		}
	}
}
