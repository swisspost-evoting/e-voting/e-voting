/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.business.voting;

import static ch.post.it.evoting.controlcomponent.TestKeyStoreInitializer.KEYSTORE_FILENAME_PATH;
import static ch.post.it.evoting.controlcomponent.TestKeyStoreInitializer.KEYSTORE_PASSWORD_FILENAME_PATH;
import static ch.post.it.evoting.domain.SharedQueue.CONTROL_COMPONENTS_ADDRESS;
import static ch.post.it.evoting.domain.SharedQueue.MESSAGE_HEADER_MESSAGE_TYPE;
import static ch.post.it.evoting.domain.SharedQueue.MESSAGE_HEADER_NODE_ID;
import static ch.post.it.evoting.domain.SharedQueue.VOTING_SERVER_ADDRESS;
import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.BASE64_ENCODED_HASH_OUTPUT_LENGTH;
import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.security.SignatureException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

import jakarta.jms.JMSException;
import jakarta.jms.Message;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.controlcomponent.ArtemisSupport;
import ch.post.it.evoting.controlcomponent.TestSigner;
import ch.post.it.evoting.controlcomponent.domain.ElectionEventState;
import ch.post.it.evoting.controlcomponent.domain.VerificationCard;
import ch.post.it.evoting.controlcomponent.protocol.voting.confirmvote.CreateLVCCShareOutput;
import ch.post.it.evoting.controlcomponent.service.ElectionContextService;
import ch.post.it.evoting.controlcomponent.service.ElectionEventService;
import ch.post.it.evoting.controlcomponent.service.ElectionEventStateService;
import ch.post.it.evoting.controlcomponent.service.VerificationCardService;
import ch.post.it.evoting.controlcomponent.service.VerificationCardSetService;
import ch.post.it.evoting.controlcomponent.service.VerificationCardStateService;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableList;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableString;
import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base64Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ElGamalGenerator;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.GqGroupGenerator;
import ch.post.it.evoting.domain.voting.confirmvote.ConfirmationKey;
import ch.post.it.evoting.domain.voting.confirmvote.ControlComponenthlVCCPayload;
import ch.post.it.evoting.domain.voting.confirmvote.ControlComponenthlVCCRequestPayload;
import ch.post.it.evoting.domain.voting.confirmvote.ControlComponentlVCCSharePayload;
import ch.post.it.evoting.domain.voting.confirmvote.LongVoteCastReturnCodesShare;
import ch.post.it.evoting.evotinglibraries.domain.common.ChannelSecurityContextData;
import ch.post.it.evoting.evotinglibraries.domain.common.ContextIds;
import ch.post.it.evoting.evotinglibraries.domain.election.ElectionEventContext;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.generators.ElectionEventContextPayloadGenerator;
import ch.post.it.evoting.evotinglibraries.domain.signature.Alias;

class LongVoteCastReturnCodeShareVerifyProcessorIT extends ArtemisSupport {

	private static final Random random = RandomFactory.createRandom();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();

	private static ControlComponenthlVCCRequestPayload controlComponenthlVCCRequestPayload;
	private static ContextIds contextIds;
	private static GqGroup gqGroup;

	@Autowired
	private ObjectMapper objectMapper;

	@BeforeAll
	static void setUpAll(
			@Autowired
			final ElectionEventService electionEventService,
			@Autowired
			final VerificationCardSetService verificationCardSetService,
			@Autowired
			final Hash hash,
			@Autowired
			final ElectionContextService electionContextService,
			@Autowired
			final ElectionEventStateService electionEventStateService,
			@Autowired
			final VerificationCardService verificationCardService,
			@Autowired
			final VerificationCardStateService verificationCardStateService,
			@Autowired
			final LongVoteCastReturnCodesShareService longVoteCastReturnCodesShareService) throws IOException, SignatureException {

		final ElectionEventContextPayloadGenerator electionEventContextPayloadGenerator = new ElectionEventContextPayloadGenerator();
		final ElectionEventContext electionEventContext = electionEventContextPayloadGenerator.generate().getElectionEventContext();
		final String electionEventId = electionEventContext.electionEventId();
		gqGroup = electionEventContext.encryptionGroup();

		// Save election event. The election must be in the CONFIGURED state for this processor.
		electionEventService.save(electionEventId, gqGroup);
		electionEventStateService.updateElectionEventState(electionEventId, ElectionEventState.CONFIGURED);

		// Save election event context.
		electionContextService.save(electionEventContext);

		// Create verification card.
		final String verificationCardSetId = electionEventContext.verificationCardSetContexts().getFirst().getVerificationCardSetId();
		final ElGamalGenerator elGamalGenerator = new ElGamalGenerator(gqGroup);
		final String verificationCardId = random.genRandomString(ID_LENGTH, base16Alphabet);
		final ElGamalMultiRecipientPublicKey publicKey = elGamalGenerator.genRandomPublicKey(1);
		verificationCardService.save(new VerificationCard(verificationCardId, verificationCardSetId, publicKey));

		contextIds = new ContextIds(electionEventId, verificationCardSetId, verificationCardId);

		// Save allow list.
		final Alphabet base64Alphabet = Base64Alphabet.getInstance();
		final String hashedLongVoteCastReturnCode = random.genRandomString(BASE64_ENCODED_HASH_OUTPUT_LENGTH, base64Alphabet);
		final List<String> otherCCRsHashedLongVoteCastReturnCodes = List.of(
				random.genRandomString(BASE64_ENCODED_HASH_OUTPUT_LENGTH, base64Alphabet),
				random.genRandomString(BASE64_ENCODED_HASH_OUTPUT_LENGTH, base64Alphabet),
				random.genRandomString(BASE64_ENCODED_HASH_OUTPUT_LENGTH, base64Alphabet));

		final List<String> hlVCC = new ArrayList<>();
		hlVCC.add(hashedLongVoteCastReturnCode);
		hlVCC.addAll(otherCCRsHashedLongVoteCastReturnCodes);

		final List<HashableString> i_aux_list = Stream.of("VerifyLVCCHash", electionEventId, verificationCardSetId, verificationCardId)
				.map(HashableString::from)
				.toList();
		final HashableList i_aux = HashableList.from(i_aux_list);
		final HashableString hlVCC_id_1 = HashableString.from(hlVCC.get(0));
		final HashableString hlVCC_id_2 = HashableString.from(hlVCC.get(1));
		final HashableString hlVCC_id_3 = HashableString.from(hlVCC.get(2));
		final HashableString hlVCC_id_4 = HashableString.from(hlVCC.get(3));
		final String hhlVCC_id = Base64.getEncoder().encodeToString(hash.recursiveHash(i_aux, hlVCC_id_1, hlVCC_id_2, hlVCC_id_3, hlVCC_id_4));

		final List<String> L_lVCC = new ArrayList<>();
		L_lVCC.add(hhlVCC_id);

		verificationCardSetService.setLongVoteCastReturnCodesAllowList(verificationCardSetId, L_lVCC);

		//Mark card as LCC share created
		verificationCardStateService.setSentVote(verificationCardId);

		final GqGroupGenerator gqGroupGenerator = new GqGroupGenerator(gqGroup);

		final ConfirmationKey confirmationKey = new ConfirmationKey(contextIds, new GqGroupGenerator(gqGroup).genMember());

		// Simulate call to CreateLVCCShare algorithm
		verificationCardStateService.incrementConfirmationAttempts(verificationCardId);
		final GqElement longVoteCastReturnCodeShare = gqGroupGenerator.genMember();
		final CreateLVCCShareOutput createLVCCShareOutput = new CreateLVCCShareOutput(longVoteCastReturnCodeShare, "hash", 1);
		longVoteCastReturnCodesShareService.saveLVCCShare(confirmationKey, createLVCCShareOutput);

		final int unsuccessfulConfirmationAttemptCount = 0;

		controlComponenthlVCCRequestPayload = new ControlComponenthlVCCRequestPayload(List.of(
				getSignedControlComponenthlVCCPayload(confirmationKey, unsuccessfulConfirmationAttemptCount, 1, hashedLongVoteCastReturnCode),
				getSignedControlComponenthlVCCPayload(confirmationKey, unsuccessfulConfirmationAttemptCount, 2,
						otherCCRsHashedLongVoteCastReturnCodes.get(0)),
				getSignedControlComponenthlVCCPayload(confirmationKey, unsuccessfulConfirmationAttemptCount, 3,
						otherCCRsHashedLongVoteCastReturnCodes.get(1)),
				getSignedControlComponenthlVCCPayload(confirmationKey, unsuccessfulConfirmationAttemptCount, 4,
						otherCCRsHashedLongVoteCastReturnCodes.get(2))
		));
	}

	private static ControlComponenthlVCCPayload getSignedControlComponenthlVCCPayload(final ConfirmationKey confirmationKey,
			final Integer unsuccessfulConfirmationAttemptCount, final int nodeId, final String hashLongVoteCastCodeShare)
			throws IOException, SignatureException {

		final ControlComponenthlVCCPayload controlComponenthlVCCPayload =
				new ControlComponenthlVCCPayload(gqGroup, nodeId, hashLongVoteCastCodeShare, confirmationKey, unsuccessfulConfirmationAttemptCount);

		final TestSigner controlComponentByNodeIdSigner = new TestSigner(KEYSTORE_FILENAME_PATH, KEYSTORE_PASSWORD_FILENAME_PATH,
				Alias.getControlComponentByNodeId(nodeId));
		controlComponentByNodeIdSigner.sign(controlComponenthlVCCPayload,
				ChannelSecurityContextData.controlComponenthlVCC(nodeId, contextIds.electionEventId(), contextIds.verificationCardSetId(),
						contextIds.verificationCardId()));

		return controlComponenthlVCCPayload;
	}

	@Test
	void happyPath() throws IOException, JMSException {
		final String correlationId = UUID.randomUUID().toString();
		multicastJmsTemplate.convertAndSend(CONTROL_COMPONENTS_ADDRESS, objectMapper.writeValueAsBytes(controlComponenthlVCCRequestPayload),
				jmsMessage -> {
					jmsMessage.setJMSCorrelationID(correlationId);
					jmsMessage.setStringProperty(MESSAGE_HEADER_MESSAGE_TYPE, controlComponenthlVCCRequestPayload.getClass().getName());
					jmsMessage.setStringProperty(MESSAGE_HEADER_NODE_ID, "1");
					return jmsMessage;
				});

		final Message responseMessage = jmsTemplate.receive(VOTING_SERVER_ADDRESS);
		assertNotNull(responseMessage);

		final ControlComponentlVCCSharePayload responsePayload =
				objectMapper.readValue(responseMessage.getBody(byte[].class), ControlComponentlVCCSharePayload.class);

		assertTrue(responsePayload.isVerified());
		assertEquals(gqGroup, responsePayload.getEncryptionGroup());
		assertNotNull(responsePayload.getSignature());

		final LongVoteCastReturnCodesShare longVoteCastReturnCodesShare = responsePayload.getLongVoteCastReturnCodesShare()
				.orElseThrow(() -> new IllegalStateException("We checked is verified, shouldn't reach here"));
		assertNotNull(longVoteCastReturnCodesShare);

		assertEquals(1, longVoteCastReturnCodesShare.nodeId());
		assertEquals(contextIds.electionEventId(), longVoteCastReturnCodesShare.electionEventId());
		assertEquals(contextIds.verificationCardSetId(), longVoteCastReturnCodesShare.verificationCardSetId());
		assertEquals(contextIds.verificationCardId(), longVoteCastReturnCodesShare.verificationCardId());
	}
}

