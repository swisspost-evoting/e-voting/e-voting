/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.controlcomponent.commandmessaging.CommandRepository;
import ch.post.it.evoting.controlcomponent.repository.BallotBoxRepository;
import ch.post.it.evoting.controlcomponent.repository.CcmjElectionKeysRepository;
import ch.post.it.evoting.controlcomponent.repository.CcrjReturnCodesKeysRepository;
import ch.post.it.evoting.controlcomponent.repository.ElectionContextRepository;
import ch.post.it.evoting.controlcomponent.repository.ElectionEventRepository;
import ch.post.it.evoting.controlcomponent.repository.ElectionEventStateRepository;
import ch.post.it.evoting.controlcomponent.repository.EncryptedVerifiableVoteRepository;
import ch.post.it.evoting.controlcomponent.repository.LVCCAllowListEntryRepository;
import ch.post.it.evoting.controlcomponent.repository.LongVoteCastReturnCodesShareRepository;
import ch.post.it.evoting.controlcomponent.repository.PCCAllowListEntryRepository;
import ch.post.it.evoting.controlcomponent.repository.PartiallyDecryptedPCCRepository;
import ch.post.it.evoting.controlcomponent.repository.SetupComponentPublicKeysRepository;
import ch.post.it.evoting.controlcomponent.repository.VerificationCardRepository;
import ch.post.it.evoting.controlcomponent.repository.VerificationCardSetRepository;

@Service
public class TestDatabaseCleanUpService {

	@Autowired
	private ElectionContextRepository electionContextRepository;

	@Autowired
	private SetupComponentPublicKeysRepository setupComponentPublicKeysRepository;

	@Autowired
	private CcrjReturnCodesKeysRepository ccrjReturnCodesKeysRepository;

	@Autowired
	private CcmjElectionKeysRepository ccmjElectionKeysRepository;

	@Autowired
	private VerificationCardRepository verificationCardRepository;

	@Autowired
	private BallotBoxRepository ballotBoxRepository;

	@Autowired
	private VerificationCardSetRepository verificationCardSetRepository;

	@Autowired
	private PCCAllowListEntryRepository pccAllowListEntryRepository;

	@Autowired
	private EncryptedVerifiableVoteRepository encryptedVerifiableVoteRepository;

	@Autowired
	private ElectionEventRepository electionEventRepository;

	@Autowired
	private ElectionEventStateRepository electionEventStateRepository;

	@Autowired
	private LongVoteCastReturnCodesShareRepository longVoteCastReturnCodesShareRepository;

	@Autowired
	private LVCCAllowListEntryRepository lvccAllowListEntryRepository;

	@Autowired
	private PartiallyDecryptedPCCRepository partiallyDecryptedPCCRepository;

	@Autowired
	private CommandRepository commandRepository;

	public void cleanUp() {
		longVoteCastReturnCodesShareRepository.deleteAll();
		encryptedVerifiableVoteRepository.deleteAll();
		partiallyDecryptedPCCRepository.deleteAll();
		verificationCardRepository.deleteAll();
		ballotBoxRepository.deleteAll();
		pccAllowListEntryRepository.deleteAll();
		lvccAllowListEntryRepository.deleteAll();
		verificationCardSetRepository.deleteAll();
		ccrjReturnCodesKeysRepository.deleteAll();
		ccmjElectionKeysRepository.deleteAll();
		setupComponentPublicKeysRepository.deleteAll();
		electionContextRepository.deleteAll();
		electionEventStateRepository.deleteAll();
		electionEventRepository.deleteAll();
		commandRepository.deleteAll();
	}

}
