/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.protocol.configuration.setupvoting;

import static ch.post.it.evoting.evotinglibraries.domain.VotingOptionsConstants.MAXIMUM_SUPPORTED_NUMBER_OF_SELECTIONS;
import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.google.common.base.Throwables;

import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;

@DisplayName("A GenKeysCCRContext with")
class GenKeysCCRContextTest {

	private static final int psiSup = MAXIMUM_SUPPORTED_NUMBER_OF_SELECTIONS;
	private static final int psiMax = RandomFactory.createRandom().genRandomInteger(psiSup - 1) + 1;
	private static final int nodeId = 1;
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();
	private static final String electionEventId = RandomFactory.createRandom().genRandomString(ID_LENGTH, base16Alphabet);

	private static final GqGroup encryptionGroup = GroupTestData.getLargeGqGroup();

	@Test
	@DisplayName("null parameters throws NullPointerException")
	void nullParametersThrows() {
		assertThrows(NullPointerException.class, () -> new GenKeysCCRContext(null, nodeId, electionEventId, psiMax));
		assertThrows(NullPointerException.class, () -> new GenKeysCCRContext(encryptionGroup, nodeId, null, psiMax));
	}

	@Test
	@DisplayName("invalid election event id throws FailedValidationException")
	void invalidElectionEventId() {
		assertThrows(FailedValidationException.class, () -> new GenKeysCCRContext(encryptionGroup, nodeId, "123", psiMax));
	}

	@Test
	@DisplayName("zero maximum number of selections throws IllegalArgumentException")
	void nullMaximumNumberOfSelectionsThrows() {
		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new GenKeysCCRContext(encryptionGroup, nodeId, electionEventId, 0));
		final String message = String.format("The maximum number of selections must be greater or equal to 1. [psi_max: %s]", 0);
		assertEquals(message, Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("negative maximum number of selections throws IllegalArgumentException")
	void negativeMaximumNumberOfSelectionsThrows() {
		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new GenKeysCCRContext(encryptionGroup, nodeId, electionEventId, -2));
		final String message = String.format("The maximum number of selections must be greater or equal to 1. [psi_max: %s]", -2);
		assertEquals(message, Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("too big maximum number of selections throws IllegalArgumentException")
	void tooBigMaximumNumberOfSelectionsThrows() {
		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new GenKeysCCRContext(encryptionGroup, nodeId, electionEventId, psiSup + 1));
		final String message = String.format(
				"The maximum number of selections must be smaller or equal to the maximum supported number of selections. [psi_max: %s, psi_sup: %s]",
				psiSup + 1, psiSup);
		assertEquals(message, Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("invalid node id throws IllegalArgumentException")
	void invalidNodeIdThrows() {
		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new GenKeysCCRContext(encryptionGroup, 5, electionEventId, psiMax));
		final String message = String.format(
				"The node id must be part of the known node ids. [nodeId: %s]", 5);
		assertEquals(message, Throwables.getRootCause(exception).getMessage());
	}
}