/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.persistence;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertInstanceOf;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDateTime;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.RepeatedTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.scheduling.concurrent.CustomizableThreadFactory;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import com.google.common.base.Strings;

import ch.post.it.evoting.controlcomponent.TestDatabaseCleanUpService;
import ch.post.it.evoting.controlcomponent.TestKeyStoreInitializer;
import ch.post.it.evoting.controlcomponent.domain.BallotBoxEntity;
import ch.post.it.evoting.controlcomponent.domain.ElectionEventEntity;
import ch.post.it.evoting.controlcomponent.domain.VerificationCardSetEntity;
import ch.post.it.evoting.controlcomponent.repository.BallotBoxRepository;
import ch.post.it.evoting.controlcomponent.service.BallotBoxService;
import ch.post.it.evoting.controlcomponent.service.ElectionEventService;
import ch.post.it.evoting.controlcomponent.service.VerificationCardSetService;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.evotinglibraries.domain.election.PrimesMappingTable;
import ch.post.it.evoting.evotinglibraries.domain.election.generators.PrimesMappingTableGenerator;

/**
 * These test are here to simulate multiple JVMs accessing the same shared resources on the database. This is the case when the control components are
 * replicated over multiple machines with the same service logic. The first tests whether two simultaneous insert leads to the second one overwriting
 * the first. Without optimistic locking JPA save becomes an insert or update so the second transaction to go through overwrites the first. We saw
 * this bug in testing. The second test is to make sure that two simultaneous updates to the same record are atomic, ie doesn't lead to an
 * inconsistent state. Optimistic locking put in place guarantees that.
 */
@SpringBootTest
@ContextConfiguration(initializers = TestKeyStoreInitializer.class)
@ActiveProfiles("test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
class ConcurrentDatabaseAccessIT {

	private static final String electionEventId = Strings.padEnd("", 32, '0');
	private static final String verificationCardSetId = Strings.padEnd("", 32, '0');
	private static final String ballotBoxId = Strings.padEnd("", 32, '0');
	private static final GqGroup encryptionGroup = GroupTestData.getLargeGqGroup();

	@Autowired
	private ElectionEventService electionEventService;

	@Autowired
	private VerificationCardSetService verificationCardSetService;

	@Autowired
	private BallotBoxRepository ballotBoxRepository;

	@Autowired
	private BallotBoxService ballotBoxService;

	@Autowired
	private TestDatabaseCleanUpService testDatabaseCleanUpService;

	@AfterEach
	void cleanUp() {
		testDatabaseCleanUpService.cleanUp();
	}

	@RepeatedTest(5)
	void testPrimaryKeyViolation() throws InterruptedException {
		final int numThreads = 2;
		final CountDownLatch checkLatch = new CountDownLatch(numThreads);
		final CountDownLatch saveLatch = new CountDownLatch(1);

		final ExecutorService executorService = Executors.newFixedThreadPool(numThreads, new CustomizableThreadFactory("lock-"));

		final CompletableFuture<?> saveFirst = CompletableFuture.runAsync(checkTogetherThenSaveFirst(checkLatch, saveLatch), executorService);
		final CompletableFuture<?> saveSecond = CompletableFuture.runAsync(checkTogetherThenSaveSecond(checkLatch, saveLatch), executorService);

		executorService.shutdown();
		final boolean normalTermination = executorService.awaitTermination(1, TimeUnit.SECONDS);
		assertTrue(normalTermination);

		assertFalse(saveFirst.isCompletedExceptionally());
		assertTrue(saveSecond.isCompletedExceptionally());

		final ElectionEventEntity electionEventEntity = assertDoesNotThrow(() -> electionEventService.getElectionEventEntity(electionEventId));
		assertEquals(encryptionGroup, electionEventEntity.getEncryptionGroup());

		final Throwable cause = getCause(saveSecond);
		assertInstanceOf(DataIntegrityViolationException.class, cause);
	}

	Runnable checkTogetherThenSaveFirst(final CountDownLatch checkLatch, final CountDownLatch saveLatch) {
		return () -> {
			try {
				electionEventService.getElectionEventEntity(electionEventId);
			} catch (final IllegalStateException ignored) {
				checkLatch.countDown();

				awaitOneSecondWithRuntimeException(checkLatch);

				electionEventService.save(electionEventId, encryptionGroup);
				saveLatch.countDown();
			}
		};
	}

	Runnable checkTogetherThenSaveSecond(final CountDownLatch checkLatch, final CountDownLatch saveLatch) {
		return () -> {
			try {
				electionEventService.getElectionEventEntity(electionEventId);
			} catch (final IllegalStateException ignored) {
				checkLatch.countDown();

				awaitOneSecondWithRuntimeException(checkLatch);
				awaitOneSecondWithRuntimeException(saveLatch);

				electionEventService.save(electionEventId, encryptionGroup);
				saveLatch.countDown();
			}
		};
	}

	@RepeatedTest(5)
	void testOptimisticLockingException() throws InterruptedException {
		setUpElection();

		final int numThreads = 2;
		final CountDownLatch getLatch = new CountDownLatch(numThreads);
		final CountDownLatch saveLatch = new CountDownLatch(1);

		final ExecutorService executorService = Executors.newFixedThreadPool(numThreads, new CustomizableThreadFactory("lock-"));

		final CompletableFuture<?> updateFirst = CompletableFuture.runAsync(getTogetherThenUpdateFirst(getLatch, saveLatch), executorService);
		final CompletableFuture<?> updateSecond = CompletableFuture.runAsync(getTogetherThenUpdateSecond(getLatch, saveLatch), executorService);

		executorService.shutdown();
		final boolean normalTermination = executorService.awaitTermination(1, TimeUnit.SECONDS);
		assertTrue(normalTermination);

		assertFalse(updateFirst.isCompletedExceptionally());
		assertTrue(updateSecond.isCompletedExceptionally());

		final BallotBoxEntity ballotBoxEntity = assertDoesNotThrow(
				() -> ballotBoxService.getBallotBox(ballotBoxId));
		assertTrue(ballotBoxEntity.isMixed());

		final Throwable cause = getCause(updateSecond);
		assertInstanceOf(OptimisticLockingFailureException.class, cause);
	}

	Runnable getTogetherThenUpdateFirst(final CountDownLatch checkLatch, final CountDownLatch saveLatch) {

		return () -> {
			try {
				final BallotBoxEntity ballotBox = ballotBoxService.getBallotBox(ballotBoxId);
				checkLatch.countDown();

				awaitOneSecondWithRuntimeException(checkLatch);

				ballotBox.setMixed(true);
				ballotBoxRepository.save(ballotBox);
				saveLatch.countDown();
			} catch (final IllegalStateException ignored) {
			}
		};
	}

	Runnable getTogetherThenUpdateSecond(final CountDownLatch checkLatch, final CountDownLatch saveLatch) {
		return () -> {
			try {
				final BallotBoxEntity ballotBox = ballotBoxService.getBallotBox(ballotBoxId);
				checkLatch.countDown();

				awaitOneSecondWithRuntimeException(checkLatch);
				awaitOneSecondWithRuntimeException(saveLatch);

				ballotBox.setMixed(false);
				ballotBoxRepository.save(ballotBox);
				saveLatch.countDown();
			} catch (final IllegalStateException ignored) {
			}
		};
	}

	private void setUpElection() {

		// Save election event.
		final ElectionEventEntity savedElectionEventEntity = electionEventService.save(electionEventId, encryptionGroup);

		// Save verification card set.
		final VerificationCardSetEntity verificationCardSetEntity = new VerificationCardSetEntity.Builder()
				.setVerificationCardSetId(verificationCardSetId)
				.setVerificationCardSetAlias("alias-" + verificationCardSetId)
				.setVerificationCardSetDescription("Description " + verificationCardSetId)
				.setElectionEventEntity(savedElectionEventEntity)
				.build();
		verificationCardSetService.save(verificationCardSetEntity);

		final PrimesMappingTable primesMappingTable = new PrimesMappingTableGenerator(encryptionGroup).generate(2);

		ballotBoxService.save(ballotBoxId, verificationCardSetId, LocalDateTime.now().minusDays(1), LocalDateTime.now().plusDays(3), true, 1,
				900, primesMappingTable);
	}

	private Throwable getCause(final CompletableFuture<?> future) throws InterruptedException {
		assert (future.isCompletedExceptionally());
		try {
			future.get();
		} catch (final ExecutionException e) {
			return e.getCause();
		}
		throw new IllegalStateException("Shouldn't reach this state.");
	}

	private void awaitOneSecondWithRuntimeException(final CountDownLatch countDownLatch) {
		awaitWithRuntimeException(countDownLatch, 1);
	}

	private void awaitWithRuntimeException(final CountDownLatch countDownLatch, final int timeout) {
		final boolean awaited;
		try {
			awaited = countDownLatch.await(timeout, TimeUnit.SECONDS);
		} catch (final InterruptedException e) {
			throw new IllegalStateException("We should not reach this state.");
		}
		if (!awaited) {
			throw new IllegalStateException("Timeout for countDownLatch.");
		}
	}

}
