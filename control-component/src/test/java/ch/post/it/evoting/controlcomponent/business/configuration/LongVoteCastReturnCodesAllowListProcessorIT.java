/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.business.configuration;

import static ch.post.it.evoting.domain.SharedQueue.CONTROL_COMPONENTS_ADDRESS;
import static ch.post.it.evoting.domain.SharedQueue.MESSAGE_HEADER_MESSAGE_TYPE;
import static ch.post.it.evoting.domain.SharedQueue.MESSAGE_HEADER_NODE_ID;
import static ch.post.it.evoting.domain.SharedQueue.VOTING_SERVER_ADDRESS;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.after;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.security.SignatureException;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.UUID;

import jakarta.jms.JMSException;
import jakarta.jms.Message;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.SpyBean;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.controlcomponent.ArtemisSupport;
import ch.post.it.evoting.controlcomponent.repository.KeystoreRepository;
import ch.post.it.evoting.controlcomponent.service.ElectionContextService;
import ch.post.it.evoting.controlcomponent.service.ElectionEventService;
import ch.post.it.evoting.controlcomponent.service.VerificationCardSetService;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystoreFactory;
import ch.post.it.evoting.domain.configuration.setupvoting.LongVoteCastReturnCodesAllowListResponsePayload;
import ch.post.it.evoting.domain.configuration.setupvoting.SetupComponentLVCCAllowListPayload;
import ch.post.it.evoting.evotinglibraries.domain.common.ChannelSecurityContextData;
import ch.post.it.evoting.evotinglibraries.domain.election.ElectionEventContext;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.generators.ElectionEventContextPayloadGenerator;
import ch.post.it.evoting.evotinglibraries.domain.signature.Alias;
import ch.post.it.evoting.evotinglibraries.domain.signature.CryptoPrimitivesSignature;

@DisplayName("A LongVoteCastReturnCodesAllowListProcessor consuming")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class LongVoteCastReturnCodesAllowListProcessorIT extends ArtemisSupport {

	private static final String lVCC1 = Base64.getEncoder().encodeToString(new byte[] { 5 });
	private static final String lVCC2 = Base64.getEncoder().encodeToString(new byte[] { 17 });
	private static final List<String> LONG_VOTE_CAST_RETURN_CODES_ALLOW_LIST = Arrays.asList(lVCC1, lVCC2);

	private static ElectionEventContext electionEventContext;
	private static SetupComponentLVCCAllowListPayload signedSetupComponentLVCCAllowListPayload;

	@Autowired
	private ObjectMapper objectMapper;

	@SpyBean
	private LongVoteCastReturnCodesAllowListProcessor longVoteCastReturnCodesAllowListProcessor;

	@SpyBean
	private VerificationCardSetService verificationCardSetService;

	@BeforeAll
	static void setUpAll(
			@Autowired
			final ElectionEventService electionEventService,
			@Autowired
			final KeystoreRepository repository,
			@Autowired
			final ElectionContextService electionContextService) throws SignatureException, IOException {

		final ElectionEventContextPayloadGenerator electionEventContextPayloadGenerator = new ElectionEventContextPayloadGenerator();
		electionEventContext = electionEventContextPayloadGenerator.generate().getElectionEventContext();
		final GqGroup gqGroup = electionEventContext.encryptionGroup();
		final String electionEventId = electionEventContext.electionEventId();

		// Save election event.
		electionEventService.save(electionEventId, gqGroup);

		// Save election event context.
		electionContextService.save(electionEventContext);

		// Request payload.
		final String verificationCardSetId = electionEventContext.verificationCardSetContexts().getFirst().getVerificationCardSetId();
		signedSetupComponentLVCCAllowListPayload =
				new SetupComponentLVCCAllowListPayload(electionEventId, verificationCardSetId, LONG_VOTE_CAST_RETURN_CODES_ALLOW_LIST);
		final SignatureKeystore<Alias> sdmSignatureKeystoreService = SignatureKeystoreFactory.createSignatureKeystore(repository.getKeyStore(),
				"PKCS12", repository.getKeystorePassword(), keystore -> true, Alias.SDM_CONFIG);
		final byte[] signature = sdmSignatureKeystoreService.generateSignature(signedSetupComponentLVCCAllowListPayload,
				ChannelSecurityContextData.setupComponentLVCCAllowList(electionEventId, verificationCardSetId));
		signedSetupComponentLVCCAllowListPayload.setSignature(new CryptoPrimitivesSignature(signature));
	}

	@Test
	@DisplayName("a longVoteCastReturnCodesAllowListPayload saves the lists in the database and returns a response.")
	void request() throws IOException, JMSException {
		final byte[] longVoteCastReturnCodesAllowListPayloadBytes = objectMapper.writeValueAsBytes(signedSetupComponentLVCCAllowListPayload);

		// Sends a request to the processor.
		final String correlationId = UUID.randomUUID().toString();
		multicastJmsTemplate.convertAndSend(CONTROL_COMPONENTS_ADDRESS, longVoteCastReturnCodesAllowListPayloadBytes, jmsMessage -> {
			jmsMessage.setJMSCorrelationID(correlationId);
			jmsMessage.setStringProperty(MESSAGE_HEADER_MESSAGE_TYPE, SetupComponentLVCCAllowListPayload.class.getName());
			jmsMessage.setStringProperty(MESSAGE_HEADER_NODE_ID, "1");
			return jmsMessage;
		});

		// Collects the response of the processor.
		final Message responseMessage = jmsTemplate.receive(VOTING_SERVER_ADDRESS);
		assertNotNull(responseMessage);
		assertEquals(correlationId, responseMessage.getJMSCorrelationID());

		verify(longVoteCastReturnCodesAllowListProcessor, after(5000).times(1)).onRequest(any());

		final String verificationCardSetId = electionEventContext.verificationCardSetContexts().get(0).getVerificationCardSetId();
		assertEquals(LONG_VOTE_CAST_RETURN_CODES_ALLOW_LIST, verificationCardSetService.getLongVoteCastReturnCodesAllowList(verificationCardSetId));

		final LongVoteCastReturnCodesAllowListResponsePayload longVoteCastReturnCodesAllowListResponsePayload =
				objectMapper.readValue(responseMessage.getBody(byte[].class), LongVoteCastReturnCodesAllowListResponsePayload.class);

		assertEquals(1, longVoteCastReturnCodesAllowListResponsePayload.nodeId());

		final String electionEventId = electionEventContext.electionEventId();
		assertEquals(electionEventId, longVoteCastReturnCodesAllowListResponsePayload.electionEventId());
	}

}
