/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.business.voting;

import static ch.post.it.evoting.controlcomponent.TestKeyStoreInitializer.KEYSTORE_FILENAME_PATH;
import static ch.post.it.evoting.controlcomponent.TestKeyStoreInitializer.KEYSTORE_PASSWORD_FILENAME_PATH;
import static ch.post.it.evoting.cryptoprimitives.math.PrimeGqElement.PrimeGqElementFactory.getSmallPrimeGroupMembers;
import static ch.post.it.evoting.cryptoprimitives.utils.Conversions.integerToString;
import static ch.post.it.evoting.domain.SharedQueue.CONTROL_COMPONENTS_ADDRESS;
import static ch.post.it.evoting.domain.SharedQueue.MESSAGE_HEADER_MESSAGE_TYPE;
import static ch.post.it.evoting.domain.SharedQueue.MESSAGE_HEADER_NODE_ID;
import static ch.post.it.evoting.domain.SharedQueue.VOTING_SERVER_ADDRESS;
import static ch.post.it.evoting.evotinglibraries.domain.ControlComponentConstants.NODE_IDS;
import static ch.post.it.evoting.evotinglibraries.domain.VotingOptionsConstants.MAXIMUM_SUPPORTED_NUMBER_OF_SELECTIONS;
import static ch.post.it.evoting.evotinglibraries.domain.VotingOptionsConstants.MAXIMUM_SUPPORTED_NUMBER_OF_WRITE_INS;
import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.security.SignatureException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import jakarta.jms.JMSException;
import jakarta.jms.Message;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.SpyBean;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Streams;

import ch.post.it.evoting.controlcomponent.ArtemisSupport;
import ch.post.it.evoting.controlcomponent.TestDatabaseCleanUpService;
import ch.post.it.evoting.controlcomponent.TestSigner;
import ch.post.it.evoting.controlcomponent.domain.CcrjReturnCodesKeys;
import ch.post.it.evoting.controlcomponent.domain.ElectionEventState;
import ch.post.it.evoting.controlcomponent.domain.PCCAllowListEntryEntity;
import ch.post.it.evoting.controlcomponent.domain.VerificationCard;
import ch.post.it.evoting.controlcomponent.protocol.voting.sendvote.DecryptPCCContext;
import ch.post.it.evoting.controlcomponent.protocol.voting.sendvote.DecryptPCCService;
import ch.post.it.evoting.controlcomponent.protocol.voting.sendvote.PartialDecryptPCCAlgorithm;
import ch.post.it.evoting.controlcomponent.protocol.voting.sendvote.PartialDecryptPCCInput;
import ch.post.it.evoting.controlcomponent.protocol.voting.sendvote.PartialDecryptPCCOutput;
import ch.post.it.evoting.controlcomponent.service.CcrjReturnCodesKeysService;
import ch.post.it.evoting.controlcomponent.service.ElectionContextService;
import ch.post.it.evoting.controlcomponent.service.ElectionEventService;
import ch.post.it.evoting.controlcomponent.service.ElectionEventStateService;
import ch.post.it.evoting.controlcomponent.service.EncryptedVerifiableVoteService;
import ch.post.it.evoting.controlcomponent.service.PCCAllowListEntryService;
import ch.post.it.evoting.controlcomponent.service.SetupComponentPublicKeysService;
import ch.post.it.evoting.controlcomponent.service.VerificationCardService;
import ch.post.it.evoting.controlcomponent.service.VerificationCardSetService;
import ch.post.it.evoting.controlcomponent.service.VerificationCardStateService;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamal;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalFactory;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientKeyPair;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientMessage;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPrivateKey;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.hashing.HashFactory;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableString;
import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base64;
import ch.post.it.evoting.cryptoprimitives.math.BaseEncodingFactory;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupElement;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.PrimeGqElement;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.math.ZqElement;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ElGamalGenerator;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.GqGroupGenerator;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ZqGroupGenerator;
import ch.post.it.evoting.cryptoprimitives.utils.Conversions;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ExponentiationProof;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.PlaintextEqualityProof;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ZeroKnowledgeProof;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ZeroKnowledgeProofFactory;
import ch.post.it.evoting.domain.voting.sendvote.CombinedControlComponentPartialDecryptPayload;
import ch.post.it.evoting.domain.voting.sendvote.ControlComponentPartialDecryptPayload;
import ch.post.it.evoting.domain.voting.sendvote.PartiallyDecryptedEncryptedPCC;
import ch.post.it.evoting.evotinglibraries.domain.common.ChannelSecurityContextData;
import ch.post.it.evoting.evotinglibraries.domain.common.ContextIds;
import ch.post.it.evoting.evotinglibraries.domain.common.EncryptedVerifiableVote;
import ch.post.it.evoting.evotinglibraries.domain.election.ElectionEventContext;
import ch.post.it.evoting.evotinglibraries.domain.election.PrimesMappingTable;
import ch.post.it.evoting.evotinglibraries.domain.election.PrimesMappingTableEntry;
import ch.post.it.evoting.evotinglibraries.domain.election.SetupComponentPublicKeys;
import ch.post.it.evoting.evotinglibraries.domain.election.generators.PrimesMappingTableGenerator;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.generators.ElectionEventContextPayloadGenerator;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.generators.SetupComponentPublicKeysPayloadGenerator;
import ch.post.it.evoting.evotinglibraries.domain.signature.Alias;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.preliminaries.votingoptions.GetHashContextAlgorithm;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.preliminaries.votingoptions.PrimesMappingTableAlgorithms;

/**
 * This test evaluates the functionality of the LCCShareProcessor by examining both valid and invalid scenarios. Initially, the test focuses on the
 * ideal case where a voting client submits the correct quantity of pCC along with a valid combination of voting options. Subsequently, the test
 * scrutinizes various error conditions: submitting either fewer or more pCC than required and supplying a valid pCC count but with an improper voting
 * option combination.
 */
@DisplayName("LCCShareProcessor consuming")
class LongChoiceReturnCodesShareProcessorIT extends ArtemisSupport {

	private static final Random random = RandomFactory.createRandom();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();
	private static final ElGamal elGamal = ElGamalFactory.createElGamal();
	private static final int numberOfVotingOptions = 10;
	private static final int numberOfVoters = 5;
	private static final int delta_sup = MAXIMUM_SUPPORTED_NUMBER_OF_WRITE_INS + 1;
	private static final int psi_sup = MAXIMUM_SUPPORTED_NUMBER_OF_SELECTIONS;
	private final GqGroup gqGroup = GroupTestData.getLargeGqGroup();
	private final GqGroupGenerator gqGroupGenerator = new GqGroupGenerator(gqGroup);
	private final ZqGroupGenerator zqGroupGenerator = new ZqGroupGenerator(ZqGroup.sameOrderAs(gqGroup));
	private final ElGamalGenerator elGamalGenerator = new ElGamalGenerator(gqGroup);

	private VoteData voteData;

	@Autowired
	private ObjectMapper objectMapper;
	@Autowired
	private ElectionEventStateService electionEventStateService;
	@Autowired
	private PartiallyDecryptedPCCService partiallyDecryptedPCCService;
	@SpyBean
	private DecryptPCCService decryptPCCService;
	@SpyBean
	private SignatureKeystore<Alias> signatureKeystoreService;
	@Autowired
	private ElectionEventService electionEventService;
	@Autowired
	private VerificationCardSetService verificationCardSetService;
	@Autowired
	private VerificationCardService verificationCardService;
	@Autowired
	private VerificationCardStateService verificationCardStateService;
	@Autowired
	private ElectionContextService electionContextService;
	@Autowired
	private EncryptedVerifiableVoteService encryptedVerifiableVoteService;
	@Autowired
	private SetupComponentPublicKeysService setupComponentPublicKeysService;
	@Autowired
	private CcrjReturnCodesKeysService ccrjReturnCodesKeysService;
	@Autowired
	private PCCAllowListEntryService pccAllowListEntryService;
	@Autowired
	private TestDatabaseCleanUpService testDatabaseCleanUpService;
	@Autowired
	private PrimesMappingTableAlgorithms primesMappingTableAlgorithms;
	@Autowired
	private GetHashContextAlgorithm getHashContextAlgorithm;

	@BeforeEach
	void setUp() {
		reset(signatureKeystoreService, decryptPCCService);
		voteData = prepareVote(numberOfVoters);
	}

	@AfterEach
	void cleanUp() {
		testDatabaseCleanUpService.cleanUp();
	}

	@Test
	@DisplayName("a request with a valid number and combination of voting options executes correctly")
	void firstTimeCommand() throws JsonProcessingException, JMSException {
		// Selected voting options: Q1_YES, Q2_NO, C
		final byte[] combinedCCPartialDecryptPayloadBytes = prepareRequestPayload(voteData, 0, 4, 8);

		// Send to request queue the CombinedControlComponentPartialDecryptPayload.
		final String correlationId = UUID.randomUUID().toString();
		multicastJmsTemplate.convertAndSend(CONTROL_COMPONENTS_ADDRESS, combinedCCPartialDecryptPayloadBytes, jmsMessage -> {
			jmsMessage.setJMSCorrelationID(correlationId);
			jmsMessage.setStringProperty(MESSAGE_HEADER_MESSAGE_TYPE, CombinedControlComponentPartialDecryptPayload.class.getName());
			jmsMessage.setStringProperty(MESSAGE_HEADER_NODE_ID, "1");
			return jmsMessage;
		});

		// Verifications.
		final Message responseMessage = jmsTemplate.receive(VOTING_SERVER_ADDRESS);

		assertNotNull(responseMessage);
		assertEquals(correlationId, responseMessage.getJMSCorrelationID());
	}

	@Test
	@DisplayName("a request with duplicated partial choice return codes writes to dead letter queue")
	void duplicatePartialChoiceReturnCodes() throws IOException, JMSException {
		final GroupVector<GqElement, GqGroup> partialChoiceReturnCodes = gqGroupGenerator.genRandomGqElementVector(
				numberOfVotingOptions - 1);
		final GroupVector<GqElement, GqGroup> pCC = partialChoiceReturnCodes.append(partialChoiceReturnCodes.get(0));
		doReturn(pCC).when(decryptPCCService).decryptPCC(any(), any(), any());
		// Selected voting options: Q1_NO, Q2_YES, B
		final byte[] combinedCCPartialDecryptPayloadBytes = prepareRequestPayload(voteData, 1, 3, 7);

		// Send to request queue the CombinedControlComponentPartialDecryptPayload.
		final String correlationId = UUID.randomUUID().toString();
		multicastJmsTemplate.convertAndSend(CONTROL_COMPONENTS_ADDRESS, combinedCCPartialDecryptPayloadBytes, jmsMessage -> {
			jmsMessage.setJMSCorrelationID(correlationId);
			jmsMessage.setStringProperty(MESSAGE_HEADER_MESSAGE_TYPE, CombinedControlComponentPartialDecryptPayload.class.getName());
			jmsMessage.setStringProperty(MESSAGE_HEADER_NODE_ID, "1");
			return jmsMessage;
		});

		final Message responseMessage = jmsTemplate.receive(VOTING_SERVER_ADDRESS);
		final Message dlqResponseMessage = dlqListenerJmsTemplate.receive(DEAD_LETTER_QUEUE);

		// Verifications.
		assertNull(responseMessage);
		assertNotNull(dlqResponseMessage);
		assertEquals(correlationId, dlqResponseMessage.getJMSCorrelationID());

		assertExceptionMessage("All pCC must be distinct.");
	}

	@Test
	@DisplayName("a request with too few partial choice return codes writes to dead letter queue")
	void tooFewPartialChoiceReturnCodes() throws IOException, JMSException {
		final GroupVector<GqElement, GqGroup> pCC = gqGroupGenerator.genRandomGqElementVector(
				numberOfVotingOptions - 1);
		doReturn(pCC).when(decryptPCCService).decryptPCC(any(), any(), any());
		// Selected voting options: Q1_EMPTY, Q2_YES, A
		final byte[] combinedCCPartialDecryptPayloadBytes = prepareRequestPayload(voteData, 2, 3, 6);

		// Send to request queue the CombinedControlComponentPartialDecryptPayload.
		final String correlationId = UUID.randomUUID().toString();
		multicastJmsTemplate.convertAndSend(CONTROL_COMPONENTS_ADDRESS, combinedCCPartialDecryptPayloadBytes, jmsMessage -> {
			jmsMessage.setJMSCorrelationID(correlationId);
			jmsMessage.setStringProperty(MESSAGE_HEADER_MESSAGE_TYPE, CombinedControlComponentPartialDecryptPayload.class.getName());
			jmsMessage.setStringProperty(MESSAGE_HEADER_NODE_ID, "1");
			return jmsMessage;
		});
		final Message responseMessage = jmsTemplate.receive(VOTING_SERVER_ADDRESS);
		final Message dlqResponseMessage = dlqListenerJmsTemplate.receive(DEAD_LETTER_QUEUE);

		// Verifications.
		assertNull(responseMessage);
		assertNotNull(dlqResponseMessage);
		assertEquals(correlationId, dlqResponseMessage.getJMSCorrelationID());

		final int psi = primesMappingTableAlgorithms.getBlankCorrectnessInformation(voteData.primesMappingTable()).size();
		assertExceptionMessage(String.format("The number of partial choice return codes must be equal to psi. [psi: %s]", psi));
	}

	@Test
	@DisplayName("a request with too many partial choice return codes writes to dead letter queue")
	void tooManyPartialChoiceReturnCodes() throws IOException, JMSException {
		final GroupVector<GqElement, GqGroup> pCC = gqGroupGenerator.genRandomGqElementVector(
				numberOfVotingOptions + 1);
		doReturn(pCC).when(decryptPCCService).decryptPCC(any(), any(), any());
		// Selected voting options: Q1_YES, Q2_EMPTY, A
		final byte[] combinedCCPartialDecryptPayloadBytes = prepareRequestPayload(voteData, 0, 5, 6);

		// Send to request queue the CombinedControlComponentPartialDecryptPayload.
		final String correlationId = UUID.randomUUID().toString();
		multicastJmsTemplate.convertAndSend(CONTROL_COMPONENTS_ADDRESS, combinedCCPartialDecryptPayloadBytes, jmsMessage -> {
			jmsMessage.setJMSCorrelationID(correlationId);
			jmsMessage.setStringProperty(MESSAGE_HEADER_MESSAGE_TYPE, CombinedControlComponentPartialDecryptPayload.class.getName());
			jmsMessage.setStringProperty(MESSAGE_HEADER_NODE_ID, "1");
			return jmsMessage;
		});
		final Message responseMessage = jmsTemplate.receive(VOTING_SERVER_ADDRESS);
		final Message dlqResponseMessage = dlqListenerJmsTemplate.receive(DEAD_LETTER_QUEUE);

		// Verifications.
		assertNull(responseMessage);
		assertNotNull(dlqResponseMessage);
		assertEquals(correlationId, dlqResponseMessage.getJMSCorrelationID());

		final int psi = primesMappingTableAlgorithms.getBlankCorrectnessInformation(voteData.primesMappingTable()).size();
		assertExceptionMessage(String.format("The number of partial choice return codes must be equal to psi. [psi: %s]", psi));
	}

	@Test
	@DisplayName("a request with an invalid combination of voting options (two voting options from the same question) writes to the dead letter queue")
	void twoPccForSameQuestion() throws IOException, JMSException {
		// Selected voting options: Q1_YES, Q1_NO, D
		final byte[] combinedControlComponentPartialDecryptPayloadBytes = prepareRequestPayload(voteData, 0, 1, 9);

		// Send to request queue the CombinedControlComponentPartialDecryptPayload.
		final String correlationId = UUID.randomUUID().toString();
		multicastJmsTemplate.convertAndSend(CONTROL_COMPONENTS_ADDRESS, combinedControlComponentPartialDecryptPayloadBytes, jmsMessage -> {
			jmsMessage.setJMSCorrelationID(correlationId);
			jmsMessage.setStringProperty(MESSAGE_HEADER_MESSAGE_TYPE, CombinedControlComponentPartialDecryptPayload.class.getName());
			jmsMessage.setStringProperty(MESSAGE_HEADER_NODE_ID, "1");
			return jmsMessage;
		});
		final Message responseMessage = jmsTemplate.receive(VOTING_SERVER_ADDRESS);
		final Message dlqResponseMessage = dlqListenerJmsTemplate.receive(DEAD_LETTER_QUEUE);

		// Verifications.
		assertNull(responseMessage);
		assertNotNull(dlqResponseMessage);
		assertEquals(correlationId, dlqResponseMessage.getJMSCorrelationID());

		assertExceptionMessage("The partial Choice Return Codes allow list does not contain the partial Choice Return Code.");
	}

	/**
	 * Generates and saves to the database all objects that are needed to be able to vote.
	 * <p>
	 * Algorithms needed to prepare the vote:
	 *     <ol>
	 *         <li>GenKeysCCR</li>
	 *         <li>GenVerDat</li>
	 *         <li>GenVerCardSetKeys</li>
	 *     </ol>
	 * </p>
	 *
	 * @param numberOfVoters the number of voters for which to prepare the vote
	 * @return the vote data, containing context ids, primes mapping table, encoded voting options, ccr encryption key pairs, and verification data
	 */
	private VoteData prepareVote(final int numberOfVoters) {
		// GenKeysCCR
		final List<ElGamalMultiRecipientKeyPair> ccrEncryptionKeyPairs = Stream.generate(() -> elGamalGenerator.genRandomKeyPair(psi_sup))
				.limit(NODE_IDS.size())
				.toList();

		// GenVerDat
		final String question1 = "question1";
		final String question2 = "question2";
		final String election1 = "election1";
		final List<String> correctnessInformation = List.of(question1, question1, question1, question2, question2, question2,
				election1, election1, election1, election1);
		final List<String> actualVotingOptions = List.of("Q1_YES", "Q1_NO", "Q1_EMPTY", "Q2_YES", "Q2_NO", "Q2_EMPTY", "A", "B", "C", "D");
		final List<String> semanticInformation = List.of("NON_BLANK|Q1|YES", "NON_BLANK|Q1|NO", "BLANK|Q1|EMPTY", "NON_BLANK|Q2|YES",
				"NON_BLANK|Q2|NO", "BLANK|Q2|EMPTY", "NON_BLANK|A", "NON_BLANK|B", "NON_BLANK|C", "BLANK|D");
		final GroupVector<PrimeGqElement, GqGroup> encodedVotingOptions = getSmallPrimeGroupMembers(gqGroup, correctnessInformation.size());
		final PrimesMappingTable primesMappingTable = new PrimesMappingTableGenerator(gqGroup).generate(actualVotingOptions, encodedVotingOptions,
				semanticInformation, correctnessInformation);

		final ElectionEventContext electionEventContext = new ElectionEventContextPayloadGenerator(gqGroup).generate(primesMappingTable,
				numberOfVoters).getElectionEventContext();
		final String electionEventId = electionEventContext.electionEventId();

		// Save election event. The election must be in the CONFIGURED state for this processor.
		electionEventService.save(electionEventId, gqGroup);
		electionEventStateService.updateElectionEventState(electionEventId, ElectionEventState.CONFIGURED);

		// Save election event context.
		electionContextService.save(electionEventContext);

		final String verificationCardSetId = electionEventContext.verificationCardSetContexts().getFirst().getVerificationCardSetId();
		final VerificationData verificationData = genPcc(random, numberOfVoters, encodedVotingOptions, primesMappingTable, electionEventId);

		// Save pcc allow list, which is normally done in the GenEncLongCodeSharesProcessor
		final List<PCCAllowListEntryEntity> partialChoiceReturnCodeAllowList = verificationData.pccAllowList.stream()
				.map(partialChoiceCode -> new PCCAllowListEntryEntity(verificationCardSetService.getVerificationCardSet(verificationCardSetId),
						partialChoiceCode, 0)).toList();
		pccAllowListEntryService.saveAll(partialChoiceReturnCodeAllowList);

		final String verificationCardId = verificationData.verificationCardIds().getFirst();
		final ContextIds contextIds = new ContextIds(electionEventId, verificationCardSetId, verificationCardId);
		final ElGamalMultiRecipientPrivateKey verificationCardSecretKey = new ElGamalMultiRecipientPrivateKey(
				GroupVector.of(verificationData.verificationCardSecretKeys().getFirst()));
		final ElGamalMultiRecipientPublicKey verificationCardPublicKey = ElGamalMultiRecipientKeyPair.from(verificationCardSecretKey,
						gqGroup.getGenerator())
				.getPublicKey();
		verificationCardService.save(new VerificationCard(verificationCardId, verificationCardSetId, verificationCardPublicKey));

		return new VoteData(contextIds, primesMappingTable, encodedVotingOptions, ccrEncryptionKeyPairs, verificationData);
	}

	/**
	 * Prepares the request to the LCCShareProcessor.
	 * <p>
	 * The algorithms that need to be executed are:
	 *     <ol>
	 *         <li>CreateVote</li>
	 *         <li>PartialDecryptPCC</li>
	 *     </ol>
	 * </p>
	 *
	 * @param voteData   the vote data to be processed.
	 * @param selections the indexes of the voting options to be selected.
	 * @return the request message as a byte array
	 * @throws JsonProcessingException if the request message cannot be written as bytes
	 */
	private byte[] prepareRequestPayload(final VoteData voteData, final int... selections) throws JsonProcessingException {
		final ContextIds contextIds = voteData.contextIds;

		// CreateVote
		final PrimesMappingTable primesMappingTable = voteData.primesMappingTable;
		final GroupVector<PrimeGqElement, GqGroup> encodedVotingOptions = voteData.encodedVotingOptions;
		final List<PrimeGqElement> selectedEncodedVotingOptionsList = new ArrayList<>();
		for (final int i : selections) {
			selectedEncodedVotingOptionsList.add(encodedVotingOptions.get(i));
		}
		final GroupVector<PrimeGqElement, GqGroup> selectedEncodedVotingOptions = GroupVector.from(selectedEncodedVotingOptionsList);
		final List<ElGamalMultiRecipientKeyPair> ccrEncryptionKeyPairs = voteData.ccrEncryptionKeyPairs;
		final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> ccrEncryptionPublicKeys = ccrEncryptionKeyPairs.stream()
				.map(ElGamalMultiRecipientKeyPair::getPublicKey)
				.collect(GroupVector.toGroupVector());
		final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> ccmElectionPublicKeys = Stream.generate(
						() -> elGamalGenerator.genRandomKeyPair(delta_sup))
				.limit(NODE_IDS.size())
				.map(ElGamalMultiRecipientKeyPair::getPublicKey)
				.collect(GroupVector.toGroupVector());
		final SetupComponentPublicKeys setupComponentPublicKeys = new SetupComponentPublicKeysPayloadGenerator(gqGroup).generate(
						ccrEncryptionPublicKeys, ccmElectionPublicKeys)
				.getSetupComponentPublicKeys();
		final ElGamalMultiRecipientPublicKey electionPublicKey = setupComponentPublicKeys.electionPublicKey();
		final VerificationData verificationData = voteData.verificationData;
		final EncryptedVerifiableVote vote = createVote(contextIds, selectedEncodedVotingOptions, electionPublicKey, ccrEncryptionKeyPairs,
				verificationData.verificationCardSecretKeys.getFirst(), primesMappingTable);
		encryptedVerifiableVoteService.save(vote);

		// PartialDecryptPCC
		final List<ControlComponentPartialDecryptPayload> controlComponentPartialDecryptPayloads = genControlComponentPartialDecryptPayloads(
				contextIds, primesMappingTable, electionPublicKey, ccrEncryptionKeyPairs, vote);

		final PartiallyDecryptedEncryptedPCC partiallyDecryptedEncryptedPCC1 = controlComponentPartialDecryptPayloads.getFirst()
				.getPartiallyDecryptedEncryptedPCC();
		partiallyDecryptedPCCService.save(partiallyDecryptedEncryptedPCC1);

		// Combine partial decrypt payloads to prepare request
		final CombinedControlComponentPartialDecryptPayload combinedControlComponentPartialDecryptPayload = new CombinedControlComponentPartialDecryptPayload(
				controlComponentPartialDecryptPayloads);

		final String electionEventId = contextIds.electionEventId();
		setupComponentPublicKeysService.save(electionEventId, setupComponentPublicKeys);
		final CcrjReturnCodesKeys ccrjReturnCodesKeys = new CcrjReturnCodesKeys(electionEventId, zqGroupGenerator.genRandomZqElementMember(),
				ccrEncryptionKeyPairs.getFirst());
		ccrjReturnCodesKeysService.save(ccrjReturnCodesKeys);

		return objectMapper.writeValueAsBytes(combinedControlComponentPartialDecryptPayload);
	}

	/**
	 * Generates the partial choice return codes.
	 * <p>
	 * This is a reduced implementation of the GenVerDat algorithm which omits the parts not needed in this context.
	 * </p>
	 *
	 * @param random             the {@link Random} instance to be used.
	 * @param N_E                the number of voters.
	 * @param p_tilde            the encoded voting options.
	 * @param primesMappingTable the primes mapping table.
	 * @param ee                 the election event id.
	 * @return the verification data, containing verification card ids, partial choice return codes, pcc allow list, and verification card secret keys
	 */
	private VerificationData genPcc(final Random random, final int N_E, final GroupVector<PrimeGqElement, GqGroup> p_tilde,
			final PrimesMappingTable primesMappingTable, final String ee) {
		final Hash hash = HashFactory.createHash();
		final Base64 base64 = BaseEncodingFactory.createBase64();
		final GqGroup gqGroup = p_tilde.getGroup();

		final int n = p_tilde.size();

		final List<String> tau = primesMappingTableAlgorithms.getCorrectnessInformation(primesMappingTable, List.of());

		// Algorithm.
		final List<VerificationData> verificationData = IntStream.range(0, N_E).parallel()
				.mapToObj(id -> {
					final String vc_id = random.genRandomString(ID_LENGTH, base16Alphabet);
					final ElGamalMultiRecipientKeyPair K_id_k_id = ElGamalMultiRecipientKeyPair.genKeyPair(gqGroup, 1, random);

					// Compute hpCC_id.
					final List<GqElement> pCC_id_elements = new ArrayList<>();
					final ZqElement k_id = K_id_k_id.getPrivateKey().get(0);
					final List<String> L_pCC_id = new ArrayList<>();
					for (int k = 0; k < n; k++) {
						final PrimeGqElement p_k_tilde = p_tilde.get(k);
						final GqElement pCC_id_k = p_k_tilde.exponentiate(k_id);
						final GqElement hpCC_id_k = hash.hashAndSquare(pCC_id_k.getValue(), gqGroup);
						final String ci = tau.get(k);
						final byte[] lpCC_id_k = hash.recursiveHash(hpCC_id_k, HashableString.from(vc_id), HashableString.from(ee),
								HashableString.from(ci));
						L_pCC_id.add(base64.base64Encode(lpCC_id_k));

						pCC_id_elements.add(pCC_id_k);
					}

					return new VerificationData(List.of(vc_id), GroupVector.from(pCC_id_elements), L_pCC_id, GroupVector.of(k_id));
				})
				.toList();

		final List<String> vc_ids = verificationData.stream().flatMap(veDa -> veDa.verificationCardIds.stream()).toList();
		final GroupVector<GqElement, GqGroup> pCC_elements = verificationData.stream().flatMap(veDa -> veDa.partialChoiceReturnCodes.stream())
				.collect(GroupVector.toGroupVector());
		final List<String> L_pCC = verificationData.stream().flatMap(v -> v.pccAllowList.stream()).toList();
		final ArrayList<String> L_pCC_sorted = new ArrayList<>(L_pCC);
		Collections.sort(L_pCC_sorted);
		final GroupVector<ZqElement, ZqGroup> k = verificationData.stream().flatMap(veDa -> veDa.verificationCardSecretKeys.stream())
				.collect(GroupVector.toGroupVector());

		return new VerificationData(vc_ids, pCC_elements, L_pCC_sorted, k);
	}

	/**
	 * Generates the encrypted verifiable vote from the selected voting options.
	 *
	 * @param contextIds                   the context ids (ee, vcs, vc_id).
	 * @param selectedVotingOptions        (p_hat_0, ..., p_hat_psi_minus_one), the selected voting options.
	 * @param ccrEncryptionKeyPairs        (pk_CCR_i, sk_CCR_i), the choice return codes encryption key pairs.
	 * @param verificationCardSetSecretKey the verification card set secret key.
	 * @param primesMappingTable           pTable, the primes mapping table.
	 * @return the {@link EncryptedVerifiableVote}
	 */
	private EncryptedVerifiableVote createVote(final ContextIds contextIds,
			final GroupVector<PrimeGqElement, GqGroup> selectedVotingOptions, final ElGamalMultiRecipientPublicKey electionPublicKey,
			final List<ElGamalMultiRecipientKeyPair> ccrEncryptionKeyPairs, final ZqElement verificationCardSetSecretKey,
			final PrimesMappingTable primesMappingTable) {
		final ZeroKnowledgeProof zkp = ZeroKnowledgeProofFactory.createZeroKnowledgeProof();
		final ZqGroup zqGroup = ZqGroup.sameOrderAs(gqGroup);
		final String electionEventId = contextIds.electionEventId();
		final String verificationCardSetId = contextIds.verificationCardSetId();
		final String verificationCardId = contextIds.verificationCardId();
		final GqElement rho = selectedVotingOptions.stream().map(p -> GqElement.GqElementFactory.fromValue(p.getValue(), gqGroup))
				.reduce(gqGroup.getIdentity(),
						GqElement::multiply);
		final ZqElement r = ZqElement.create(random.genRandomInteger(gqGroup.getQ()), zqGroup);
		final ElGamalMultiRecipientMessage m = new ElGamalMultiRecipientMessage(GroupVector.of(rho));

		final ElGamalMultiRecipientCiphertext e1 = elGamal.getCiphertext(m, r, electionPublicKey);
		final GroupVector<GqElement, GqGroup> pccId = selectedVotingOptions.stream().map(p -> p.exponentiate(verificationCardSetSecretKey))
				.collect(GroupVector.toGroupVector());
		final ZqElement rPrime = ZqElement.create(random.genRandomInteger(gqGroup.getQ()), zqGroup);
		final ElGamalMultiRecipientPublicKey ccrEncryptionPublicKey = elGamal.combinePublicKeys(
				ccrEncryptionKeyPairs.stream().map(ElGamalMultiRecipientKeyPair::getPublicKey).collect(GroupVector.toGroupVector()));
		final ElGamalMultiRecipientCiphertext e2 = elGamal.getCiphertext(new ElGamalMultiRecipientMessage(pccId), rPrime,
				ccrEncryptionPublicKey);

		final ElGamalMultiRecipientCiphertext e1Tilde = ElGamalMultiRecipientCiphertext.create(e1.getGamma(), List.of(e1.getPhis().getFirst()))
				.getCiphertextExponentiation(verificationCardSetSecretKey);
		final GqElement e2TildePhi = e2.getPhis().stream().reduce(gqGroup.getIdentity(), GqElement::multiply);
		final ElGamalMultiRecipientCiphertext e2Tilde = ElGamalMultiRecipientCiphertext.create(e2.getGamma(), List.of(e2TildePhi));

		final GqElement publicKId = gqGroup.getGenerator().exponentiate(verificationCardSetSecretKey);

		final GroupVector<PrimesMappingTableEntry, GqGroup> pTable = primesMappingTable.getPTable();
		final List<String> sigma = pTable.stream()
				.map(PrimesMappingTableEntry::semanticInformation)
				.toList();
		final List<String> v_tilde = pTable.stream()
				.map(PrimesMappingTableEntry::actualVotingOption)
				.toList();
		final GroupVector<PrimeGqElement, GqGroup> p_tilde = pTable.stream()
				.map(PrimesMappingTableEntry::encodedVotingOption)
				.collect(GroupVector.toGroupVector());

		final List<String> iAux = Streams.concat(
				Stream.of("CreateVote"),
				Stream.of(electionEventId),
				Stream.of(verificationCardId),
				electionPublicKey.stream().map(GroupElement::getValue).map(Conversions::integerToString),
				e1.getPhis().stream().map(GroupElement::getValue).map(Conversions::integerToString),
				Stream.of("EncodedVotingOptions"),
				p_tilde.stream().map(p_tilde_i -> integerToString(p_tilde_i.getValue())),
				Stream.of("ActualVotingOptions"),
				v_tilde.stream(),
				Stream.of("SemanticInformation"),
				sigma.stream()
		).toList();

		final ExponentiationProof piExp = zkp.genExponentiationProof(
				GroupVector.from(List.of(gqGroup.getGenerator(), e1.getGamma(), e1.getPhis().getFirst())),
				verificationCardSetSecretKey,
				GroupVector.from(List.of(publicKId, e1Tilde.getGamma(), e1Tilde.getPhis().getFirst())),
				iAux);

		final GqElement pkCCRTilde = ccrEncryptionPublicKey.stream().limit(selectedVotingOptions.size())
				.reduce(gqGroup.getIdentity(), GqElement::multiply);

		final PlaintextEqualityProof piEqEnc = zkp.genPlaintextEqualityProof(e1Tilde, e2Tilde, electionPublicKey.get(0), pkCCRTilde,
				GroupVector.from(List.of(r.multiply(verificationCardSetSecretKey), rPrime)), iAux);

		return new EncryptedVerifiableVote(new ContextIds(electionEventId, verificationCardSetId, verificationCardId), e1, e1Tilde, e2, piExp,
				piEqEnc);
	}

	/**
	 * Generates the list of partial decrypt payloads.
	 * <p>
	 * The payload containing all four partial decrypt payloads represents the message sent to the LCCShareProcessor, which will call the
	 * {@code decryptPCC} and {@code createLCCShare} algorithms.
	 * </p>
	 *
	 * @param contextIds                              the context ids (ee, vcs, vc_id)
	 * @param primesMappingTable                      pTable, the primes mapping table.
	 * @param ccrjChoiceReturnCodesEncryptionKeyPairs (pk_CCR, sk_CCR), the choice return codes encryption key pairs of the four CCs.
	 * @param vote                                    an {@link EncryptedVerifiableVote}
	 * @return the list of {@link ControlComponentPartialDecryptPayload}s
	 */
	private List<ControlComponentPartialDecryptPayload> genControlComponentPartialDecryptPayloads(final ContextIds contextIds,
			final PrimesMappingTable primesMappingTable, final ElGamalMultiRecipientPublicKey electionPublicKey,
			final List<ElGamalMultiRecipientKeyPair> ccrjChoiceReturnCodesEncryptionKeyPairs, final EncryptedVerifiableVote vote) {
		final String electionEventId = contextIds.electionEventId();
		final String verificationCardSetId = contextIds.verificationCardSetId();
		final String verificationCardId = contextIds.verificationCardId();
		final ElGamalMultiRecipientCiphertext encryptedVote = vote.encryptedVote();
		final ElGamalMultiRecipientCiphertext exponentiatedEncryptedVote = vote.exponentiatedEncryptedVote();
		final ElGamalMultiRecipientCiphertext encryptedPartialChoiceReturnCodes = vote.encryptedPartialChoiceReturnCodes();
		verificationCardStateService.setPartiallyDecrypted(verificationCardId);

		final ElGamalMultiRecipientPublicKey choiceReturnCodesEncryptionPublicKey = elGamal.combinePublicKeys(
				ccrjChoiceReturnCodesEncryptionKeyPairs.stream().map(ElGamalMultiRecipientKeyPair::getPublicKey)
						.collect(GroupVector.toGroupVector()));
		return IntStream.range(1, 5)
				.mapToObj(nodeId -> {
					final VerificationCardStateService verificationCardStateServiceMock = mock(VerificationCardStateService.class);
					when(verificationCardStateServiceMock.isNotPartiallyDecrypted(verificationCardId)).thenReturn(true);
					final PartialDecryptPCCAlgorithm partialDecryptPCCAlgorithm = new PartialDecryptPCCAlgorithm(
							ZeroKnowledgeProofFactory.createZeroKnowledgeProof(), verificationCardStateServiceMock, primesMappingTableAlgorithms,
							getHashContextAlgorithm);

					final DecryptPCCContext partialDecryptPccContext = new DecryptPCCContext.Builder()
							.setElectionEventId(electionEventId)
							.setVerificationCardSetId(verificationCardSetId)
							.setVerificationCardId(verificationCardId)
							.setEncryptionGroup(gqGroup)
							.setNodeId(nodeId)
							.setPrimesMappingTable(primesMappingTable)
							.setElectionPublicKey(electionPublicKey)
							.setChoiceReturnCodesEncryptionPublicKey(choiceReturnCodesEncryptionPublicKey)
							.build();

					final ElGamalMultiRecipientKeyPair ccrjChoiceReturnCodesEncryptionKeyPair = ccrjChoiceReturnCodesEncryptionKeyPairs.get(
							nodeId - 1);
					final ElGamalMultiRecipientPublicKey ccrjChoiceReturnCodesEncryptionPublicKey = ccrjChoiceReturnCodesEncryptionKeyPair.getPublicKey();
					final ElGamalMultiRecipientPrivateKey ccrjChoiceReturnCodesEncryptionPrivateKey = ccrjChoiceReturnCodesEncryptionKeyPair.getPrivateKey();

					final PartialDecryptPCCInput partialDecryptPCCInput = new PartialDecryptPCCInput.Builder()
							.setEncryptedVote(encryptedVote)
							.setExponentiatedEncryptedVote(exponentiatedEncryptedVote)
							.setEncryptedPartialChoiceReturnCodes(encryptedPartialChoiceReturnCodes)
							.setCcrjChoiceReturnCodesEncryptionPublicKey(ccrjChoiceReturnCodesEncryptionPublicKey)
							.setCcrjChoiceReturnCodesEncryptionSecretKey(ccrjChoiceReturnCodesEncryptionPrivateKey)
							.build();

					final PartialDecryptPCCOutput partialDecryptPCCOutput = partialDecryptPCCAlgorithm.partialDecryptPCC(partialDecryptPccContext,
							partialDecryptPCCInput);
					return new PartiallyDecryptedEncryptedPCC(contextIds, nodeId, partialDecryptPCCOutput.exponentiatedGammas(),
							partialDecryptPCCOutput.exponentiationProofs());
				})
				.map(partiallyDecryptedEncryptedPCC -> {
					final ControlComponentPartialDecryptPayload partialDecryptPayload = new ControlComponentPartialDecryptPayload(gqGroup,
							partiallyDecryptedEncryptedPCC);
					try {
						final int nodeId = partialDecryptPayload.getPartiallyDecryptedEncryptedPCC().nodeId();
						final TestSigner ccSigner = new TestSigner(KEYSTORE_FILENAME_PATH, KEYSTORE_PASSWORD_FILENAME_PATH,
								Alias.getControlComponentByNodeId(nodeId));
						ccSigner.sign(partialDecryptPayload,
								ChannelSecurityContextData.controlComponentPartialDecrypt(nodeId, electionEventId, verificationCardSetId,
										verificationCardId));
						return partialDecryptPayload;
					} catch (final IOException | SignatureException e) {
						throw new RuntimeException(e);
					}
				})
				.toList();
	}

	record VoteData(ContextIds contextIds,
					PrimesMappingTable primesMappingTable,
					GroupVector<PrimeGqElement, GqGroup> encodedVotingOptions,
					List<ElGamalMultiRecipientKeyPair> ccrEncryptionKeyPairs,
					VerificationData verificationData) {

	}

	record VerificationData(List<String> verificationCardIds, GroupVector<GqElement, GqGroup> partialChoiceReturnCodes,
							List<String> pccAllowList,
							GroupVector<ZqElement, ZqGroup> verificationCardSecretKeys) {

	}

}
