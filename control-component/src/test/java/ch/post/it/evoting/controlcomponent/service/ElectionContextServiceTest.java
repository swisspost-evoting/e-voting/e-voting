/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.google.common.base.Throwables;

import ch.post.it.evoting.controlcomponent.domain.BallotBoxEntity;
import ch.post.it.evoting.controlcomponent.domain.ElectionContextEntity;
import ch.post.it.evoting.controlcomponent.domain.ElectionEventEntity;
import ch.post.it.evoting.controlcomponent.domain.VerificationCardSetEntity;
import ch.post.it.evoting.controlcomponent.repository.ElectionContextRepository;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.evotinglibraries.domain.election.ElectionEventContext;
import ch.post.it.evoting.evotinglibraries.domain.election.VerificationCardSetContext;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.generators.ElectionEventContextPayloadGenerator;

@DisplayName("ElectionContextServiceTest")
class ElectionContextServiceTest {

	private static final ElectionContextRepository electionContextRepository = mock(ElectionContextRepository.class);

	private static ElectionContextService electionContextService;
	private static ElectionEventContext electionEventContext;

	@BeforeAll
	static void setUpAll() {
		final ElectionEventContextPayloadGenerator electionEventContextPayloadGenerator = new ElectionEventContextPayloadGenerator();
		electionEventContext = electionEventContextPayloadGenerator.generate().getElectionEventContext();
		final String electionEventId = electionEventContext.electionEventId();

		final GqGroup encryptionGroup = GroupTestData.getGqGroup();
		final ElectionEventEntity electionEventEntity = new ElectionEventEntity(electionEventId, encryptionGroup);

		final ElectionContextEntity electionContextEntity = new ElectionContextEntity.Builder()
				.setElectionEventEntity(electionEventEntity)
				.setElectionEventAlias(electionEventContext.electionEventAlias())
				.setElectionEventDescription(electionEventContext.electionEventDescription())
				.setStartTime(electionEventContext.startTime())
				.setFinishTime(electionEventContext.finishTime())
				.setMaxNumberOfVotingOptions(electionEventContext.maximumNumberOfVotingOptions())
				.setMaxNumberOfSelections(electionEventContext.maximumNumberOfSelections())
				.setMaxNumberOfWriteInsPlusOne(electionEventContext.maximumNumberOfWriteInsPlusOne())
				.build();

		final Map<VerificationCardSetContext, VerificationCardSetEntity> verificationCardSets = electionEventContext.verificationCardSetContexts()
				.stream()
				.collect(Collectors.toMap(
						Function.identity(),
						verificationCardSetContext -> new VerificationCardSetEntity.Builder()
								.setVerificationCardSetId(verificationCardSetContext.getVerificationCardSetId())
								.setVerificationCardSetAlias(verificationCardSetContext.getVerificationCardSetAlias())
								.setVerificationCardSetDescription(verificationCardSetContext.getVerificationCardSetDescription())
								.setElectionEventEntity(electionEventEntity)
								.build())
				);
		final VerificationCardSetService verificationCardSetService = mock(VerificationCardSetService.class);
		doNothing().when(verificationCardSetService).saveFromContext(any());
		when(verificationCardSetService.findAllByElectionEventId(electionEventId)).thenReturn(verificationCardSets.values().stream().toList());

		final BallotBoxService ballotBoxService = mock(BallotBoxService.class);
		doNothing().when(ballotBoxService).saveFromContexts(any());
		verificationCardSets.forEach((key, value) -> {
			final BallotBoxEntity ballotBoxEntity = new BallotBoxEntity.Builder()
					.setBallotBoxId(key.getBallotBoxId())
					.setVerificationCardSetEntity(value)
					.setBallotBoxStartTime(key.getBallotBoxStartTime())
					.setBallotBoxFinishTime(key.getBallotBoxFinishTime())
					.setTestBallotBox(key.isTestBallotBox())
					.setNumberOfVotingCards(key.getNumberOfVotingCards())
					.setGracePeriod(key.getGracePeriod())
					.setPrimesMappingTable(key.getPrimesMappingTable())
					.build();
			when(ballotBoxService.getBallotBox(value)).thenReturn(ballotBoxEntity);
		});

		when(electionContextRepository.save(any())).thenReturn(electionContextEntity);
		when(electionContextRepository.findByElectionEventId(electionEventContext.electionEventId()))
				.thenReturn(Optional.ofNullable(electionContextEntity));

		final ElectionEventService electionEventService = mock(ElectionEventService.class);
		when(electionEventService.getElectionEventEntity(electionEventId)).thenReturn(electionEventEntity);
		when(electionEventService.getEncryptionGroup(any())).thenReturn(encryptionGroup);

		electionContextService = new ElectionContextService(ballotBoxService, electionEventService, electionContextRepository,
				verificationCardSetService);
	}

	@Test
	@DisplayName("saving with valid ElectionEventContext does not throw")
	void savingValidElectionEventContextDoesNotThrow() {
		electionContextService.save(electionEventContext);
		verify(electionContextRepository, times(1)).save(any());

		final ElectionEventContext electionEventContextSaved = electionContextService.getElectionEventContext(electionEventContext.electionEventId());
		assertEquals(electionEventContext.electionEventAlias(), electionEventContextSaved.electionEventAlias());
	}

	@Test
	@DisplayName("loading non existing ElectionEventContext throws IllegalStateException")
	void loadNonExistingThrows() {
		final String nonExistingElectionId = "e77dbe3c70874ea584c490a0c6ac0ca4";
		final IllegalStateException exceptionCcm = assertThrows(IllegalStateException.class,
				() -> electionContextService.getElectionEventStartTime(nonExistingElectionId));
		assertEquals(String.format("Election context entity not found. [electionEventId: %s]", nonExistingElectionId),
				Throwables.getRootCause(exceptionCcm).getMessage());
	}
}
