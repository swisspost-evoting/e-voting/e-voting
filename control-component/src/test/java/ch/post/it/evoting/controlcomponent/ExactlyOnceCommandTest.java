/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.concurrent.Callable;
import java.util.function.Function;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base32Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base64Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;

class ExactlyOnceCommandTest {
	private static final Random random = RandomFactory.createRandom();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();
	private static final Alphabet base32Alphabet = Base32Alphabet.getInstance();
	private static final Alphabet base64Alphabet = Base64Alphabet.getInstance();

	private static Stream<Arguments> nullArgumentProvider() {
		final String correlationId = random.genRandomString(ID_LENGTH, base64Alphabet);

		final String contextId = random.genRandomString(ID_LENGTH, base32Alphabet);
		final String context = random.genRandomString(ID_LENGTH, base16Alphabet);
		final Callable<byte[]> callable = () -> new byte[] { 0b0000001 };
		final byte[] requestContent = new byte[] { 0b0110111 };

		return Stream.of(
				Arguments.of(null, contextId, context, callable, requestContent),
				Arguments.of(correlationId, null, context, callable, requestContent),
				Arguments.of(correlationId, contextId, null, callable, requestContent),
				Arguments.of(correlationId, contextId, context, null, requestContent),
				Arguments.of(correlationId, contextId, context, callable, null)
		);
	}

	@ParameterizedTest
	@MethodSource("nullArgumentProvider")
	void testBuildWithNullParametersThrowsNullPointerException(final String correlationId, final String contextId, final String context,
			final Callable<byte[]> callable, final byte[] requestContent) {
		final ExactlyOnceCommand.Builder<byte[]> builder = new ExactlyOnceCommand.Builder<byte[]>()
				.setCorrelationId(correlationId)
				.setContextId(contextId)
				.setContext(context)
				.setTask(callable)
				.setRequestContent(requestContent);

		assertThrows(NullPointerException.class, builder::build);
	}

	@Test
	void testBuildWithValidParametersDoesNotThrow() {
		final String correlationId = random.genRandomString(ID_LENGTH, base64Alphabet);
		final String contextId = random.genRandomString(ID_LENGTH, base32Alphabet);
		final String context = random.genRandomString(ID_LENGTH, base16Alphabet);
		final Callable<byte[]> callable = () -> new byte[] { 0b0000001 };
		final byte[] requestContent = new byte[] { 0b0110111 };

		final ExactlyOnceCommand.Builder<byte[]> builder = new ExactlyOnceCommand.Builder<byte[]>()
				.setCorrelationId(correlationId)
				.setContextId(contextId)
				.setContext(context)
				.setTask(callable)
				.setRequestContent(requestContent)
				.setSerializer(Function.identity());
		final ExactlyOnceCommand<byte[]> exactlyOnceCommand = assertDoesNotThrow(builder::build);
		assertEquals(correlationId, exactlyOnceCommand.getCorrelationId());
		assertEquals(contextId, exactlyOnceCommand.getContextId());
		assertEquals(context, exactlyOnceCommand.getContext());
		assertEquals(callable, exactlyOnceCommand.getTask());
		assertArrayEquals(requestContent, exactlyOnceCommand.getRequestContent());
	}
}
