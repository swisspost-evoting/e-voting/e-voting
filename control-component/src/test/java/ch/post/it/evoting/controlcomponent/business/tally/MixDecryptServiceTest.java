/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.business.tally;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

import com.google.common.base.Throwables;

import ch.post.it.evoting.controlcomponent.domain.BallotBoxEntity;
import ch.post.it.evoting.controlcomponent.service.BallotBoxService;
import ch.post.it.evoting.controlcomponent.service.ElectionContextService;
import ch.post.it.evoting.controlcomponent.service.SetupComponentPublicKeysService;
import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.mixnet.VerifiableShuffle;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.VerifiableShuffleGenerator;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.VerifiableDecryptionGenerator;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.VerifiableDecryptions;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.ControlComponentShufflePayload;

@ExtendWith(MockitoExtension.class)
class MixDecryptServiceTest {

	@Mock
	private BallotBoxService ballotBoxService;
	@Mock(answer = Answers.RETURNS_DEEP_STUBS)
	private ElectionContextService electionContextService;
	@Mock(answer = Answers.RETURNS_DEEP_STUBS)
	private SetupComponentPublicKeysService setupComponentPublicKeysService;
	@InjectMocks
	private MixDecryptService mixDecryptService;

	@Nested
	class ValidateMixIsAllowed {
		private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();
		private static final String ANY_ID = RandomFactory.createRandom().genRandomString(ID_LENGTH, base16Alphabet);
		private static final int GRACE_PERIOD = 3600;
		private BallotBoxEntity ballotBoxEntity;

		private LocalDateTime electionEndTime;
		private LocalDateTime currentTime;

		@BeforeEach
		void setUp() {
			currentTime = LocalDateTime.now();
			electionEndTime = currentTime.plusSeconds(3600);
			ballotBoxEntity = mock(BallotBoxEntity.class);

			when(ballotBoxEntity.getGracePeriod()).thenReturn(GRACE_PERIOD);
			when(ballotBoxService.getBallotBox(anyString())).thenReturn(ballotBoxEntity);
			when(electionContextService.getElectionEventFinishTime(anyString())).thenReturn(electionEndTime);
		}

		@Test
		@DisplayName("Test ballot box can be mixed at any time.")
		void testBallotBox() {
			when(ballotBoxEntity.isTestBallotBox()).thenReturn(true);

			assertDoesNotThrow(() -> mixDecryptService.validateMixIsAllowed(ANY_ID, ANY_ID, () -> currentTime));
		}

		@Test
		@DisplayName("Possible to mix after the grace period has expired.")
		void prodBallotBoxWithElectionFinished() {
			when(ballotBoxEntity.isTestBallotBox()).thenReturn(false);

			assertDoesNotThrow(() -> mixDecryptService.validateMixIsAllowed(ANY_ID, ANY_ID,
					() -> electionEndTime.plusSeconds(GRACE_PERIOD).plusSeconds(1)));
		}

		@Test
		@DisplayName("Not possible to mix before the election finish, including grace period has expired")
		void prodBallotBoxWithElectionNotFinished() {
			when(ballotBoxEntity.isTestBallotBox()).thenReturn(false);

			final IllegalStateException illegalStateException = assertThrows(IllegalStateException.class,
					() -> mixDecryptService.validateMixIsAllowed(ANY_ID, ANY_ID, () -> electionEndTime.plusSeconds(GRACE_PERIOD)));

			final String errorMessage = String.format(
					"The ballot box can not be mixed. [isTestBallotBox: %s, finishTime: %s, electionEventId: %s, ballotBoxId: %s]",
					ballotBoxEntity.isTestBallotBox(), electionEndTime, ANY_ID, ANY_ID);
			assertEquals(errorMessage, Throwables.getRootCause(illegalStateException).getMessage());
		}
	}

	@Nested
	class ValidateShufflePayload {

		private static final String ELECTION_EVENT_ID = "00000000000000000000000000000000";
		private static final String BALLOT_BOX_ID = "11111111111111111111111111111111";
		private static final GqGroup GQ_GROUP = GroupTestData.getGqGroup();

		@BeforeEach
		void setUp() {
			when(setupComponentPublicKeysService.getElectionPublicKey(anyString()).getGroup()).thenReturn(GQ_GROUP);
		}

		@ParameterizedTest
		@DisplayName("Valid payload pass validation")
		@ValueSource(ints = { 1, 2, 3, 4 })
		void validPayloadPassValidation(final int nodeId) {
			ReflectionTestUtils.setField(mixDecryptService, "nodeId", nodeId);
			final List<ControlComponentShufflePayload> shufflePayloads = createPayloads(GQ_GROUP, ELECTION_EVENT_ID, BALLOT_BOX_ID, nodeId);

			assertDoesNotThrow(() -> mixDecryptService.validateShufflePayload(GQ_GROUP, ELECTION_EVENT_ID, BALLOT_BOX_ID, shufflePayloads));
		}

		@ParameterizedTest
		@DisplayName("Incorrect payload count is given according node ID")
		@MethodSource("incorrectPayloadCountAccordingNodeIdProvider")
		void incorrectPayloadCountAccordingNodeId(final int nodeId, final int payloadCount) {
			ReflectionTestUtils.setField(mixDecryptService, "nodeId", nodeId);
			final List<ControlComponentShufflePayload> shufflePayloads = new ArrayList<>();
			IntStream.range(0, payloadCount)
					.forEach(value -> shufflePayloads.add(createPayload(GQ_GROUP, ELECTION_EVENT_ID, BALLOT_BOX_ID, nodeId)));

			final IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class,
					() -> mixDecryptService.validateShufflePayload(GQ_GROUP, ELECTION_EVENT_ID, BALLOT_BOX_ID, shufflePayloads));

			final String errorMessage = String.format(
					"There must be exactly the expected number of shuffle payloads. [expected: %s, actual: %s]", nodeId - 1, shufflePayloads.size());
			assertEquals(errorMessage, Throwables.getRootCause(illegalArgumentException).getMessage());
		}

		static Stream<Arguments> incorrectPayloadCountAccordingNodeIdProvider() {
			return Stream.of(
					Arguments.of(1, 1),
					Arguments.of(2, 0),
					Arguments.of(2, 2),
					Arguments.of(3, 1),
					Arguments.of(3, 3),
					Arguments.of(4, 2),
					Arguments.of(4, 5)
			);
		}

		@ParameterizedTest
		@DisplayName("Payload with incorrect election event id failed validation (except node 1 which has no payload)")
		@ValueSource(ints = { 2, 3, 4 })
		void payloadWithIncorrectElectionEventIdFailedValidation(final int nodeId) {
			ReflectionTestUtils.setField(mixDecryptService, "nodeId", nodeId);
			final String wrongElectionEventId = "22222222222222222222222222222222";
			final List<ControlComponentShufflePayload> shufflePayloads = createPayloads(GQ_GROUP, wrongElectionEventId, BALLOT_BOX_ID, nodeId);

			final IllegalStateException illegalStateException = assertThrows(IllegalStateException.class,
					() -> mixDecryptService.validateShufflePayload(GQ_GROUP, ELECTION_EVENT_ID, BALLOT_BOX_ID, shufflePayloads));

			final String errorMessage = String.format("Election event ID must be identical in shuffle payload. [expected: %s, actual: %s]",
					ELECTION_EVENT_ID, wrongElectionEventId);
			assertEquals(errorMessage, Throwables.getRootCause(illegalStateException).getMessage());
		}

		@ParameterizedTest
		@DisplayName("Payload with incorrect election event id failed validation (except node 1 which has no payload)")
		@ValueSource(ints = { 2, 3, 4 })
		void payloadWithIncorrectBallotBoxIdFailedValidation(final int nodeId) {
			ReflectionTestUtils.setField(mixDecryptService, "nodeId", nodeId);
			final String wrongBallotBoxId = "22222222222222222222222222222222";
			final List<ControlComponentShufflePayload> shufflePayloads = createPayloads(GQ_GROUP, ELECTION_EVENT_ID, wrongBallotBoxId, nodeId);

			final IllegalStateException illegalStateException = assertThrows(IllegalStateException.class,
					() -> mixDecryptService.validateShufflePayload(GQ_GROUP, ELECTION_EVENT_ID, BALLOT_BOX_ID, shufflePayloads));

			final String errorMessage = String.format("Ballot box ID must be identical in shuffle payload. [expected: %s, actual: %s]", BALLOT_BOX_ID,
					wrongBallotBoxId);
			assertEquals(errorMessage, Throwables.getRootCause(illegalStateException).getMessage());
		}

		@ParameterizedTest
		@DisplayName("Payload with incorrect encryption group failed validation (except node 1 which has no payload)")
		@ValueSource(ints = { 2, 3, 4 })
		void payloadWithIncorrectGqGroupFailedValidation(final int nodeId) {
			ReflectionTestUtils.setField(mixDecryptService, "nodeId", nodeId);
			final GqGroup wrongGqGroup = GroupTestData.getLargeGqGroup();
			final List<ControlComponentShufflePayload> shufflePayloads = createPayloads(wrongGqGroup, ELECTION_EVENT_ID, BALLOT_BOX_ID, nodeId);

			final IllegalStateException illegalStateException = assertThrows(IllegalStateException.class,
					() -> mixDecryptService.validateShufflePayload(GQ_GROUP, ELECTION_EVENT_ID, BALLOT_BOX_ID, shufflePayloads));

			final String errorMessage = String.format("Gq groups must be identical in shuffle payload. [expected: %s, actual: %s]", GQ_GROUP,
					wrongGqGroup);
			assertEquals(errorMessage, Throwables.getRootCause(illegalStateException).getMessage());
		}

		@ParameterizedTest
		@DisplayName("Payload with incorrect node id failed validation (except node 1 which has no payload)")
		@ValueSource(ints = { 2, 3, 4 })
		void payloadWithIncorrectNodeIdFailedValidation(final int nodeId) {
			ReflectionTestUtils.setField(mixDecryptService, "nodeId", nodeId);
			final List<ControlComponentShufflePayload> shufflePayloads = Collections.nCopies(nodeId - 1,
					createPayload(GQ_GROUP, ELECTION_EVENT_ID, BALLOT_BOX_ID, nodeId));
			final int[] expectedNodeIds = IntStream.range(1, nodeId).toArray();
			final int[] actualNodeIds = new int[] { nodeId };

			final IllegalStateException illegalStateException = assertThrows(IllegalStateException.class,
					() -> mixDecryptService.validateShufflePayload(GQ_GROUP, ELECTION_EVENT_ID, BALLOT_BOX_ID, shufflePayloads));

			final String errorMessage = String.format("Payloads must come from expected nodes. [expected: %s, actual: %s]",
					Arrays.toString(expectedNodeIds),
					Arrays.toString(actualNodeIds));
			assertEquals(errorMessage, Throwables.getRootCause(illegalStateException).getMessage());
		}

		private List<ControlComponentShufflePayload> createPayloads(final GqGroup gqGroup, final String electionEventId, final String ballotBoxId,
				final int nodeId) {

			if (nodeId == 1) {
				return Collections.emptyList();
			}

			final List<ControlComponentShufflePayload> shufflePayloads = new ArrayList<>();
			IntStream.range(1, nodeId)
					.forEach(n -> shufflePayloads.add(createPayload(gqGroup, electionEventId, ballotBoxId, n)));
			return shufflePayloads;
		}

		private ControlComponentShufflePayload createPayload(final GqGroup gqGroup, final String electionEventId, final String ballotBoxId,
				final int nodeId) {
			final VerifiableDecryptions verifiableDecryptions = new VerifiableDecryptionGenerator(gqGroup).genVerifiableDecryption(5, 5);
			final VerifiableShuffle verifiableShuffle = new VerifiableShuffleGenerator(gqGroup).genVerifiableShuffle(5, 5);
			return new ControlComponentShufflePayload(gqGroup, electionEventId, ballotBoxId, nodeId, verifiableShuffle, verifiableDecryptions);
		}
	}
}
