/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.service;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import com.google.common.base.Throwables;

import ch.post.it.evoting.controlcomponent.TestKeyStoreInitializer;
import ch.post.it.evoting.controlcomponent.repository.ElectionEventRepository;
import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;

@SpringBootTest
@ContextConfiguration(initializers = TestKeyStoreInitializer.class)
@ActiveProfiles("test")
@DisplayName("ElectionEventService")
class ElectionEventServiceIT {

	private static final Random random = RandomFactory.createRandom();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();

	private String electionEventId;
	private GqGroup encryptionGroup;

	@Autowired
	private ElectionEventService electionEventService;

	@SpyBean
	private ElectionEventRepository electionEventRepository;

	@BeforeEach
	void setUp() {
		electionEventId = random.genRandomString(ID_LENGTH, base16Alphabet);
		encryptionGroup = GroupTestData.getGqGroup();

		reset(electionEventRepository);
	}

	@Nested
	@DisplayName("saving")
	class SaveTest {

		@Test
		@DisplayName("with any null parameter throws NullPointerException")
		void saveNullParams() {
			assertThrows(NullPointerException.class, () -> electionEventService.save(null, encryptionGroup));
			assertThrows(NullPointerException.class, () -> electionEventService.save(electionEventId, null));
		}

		@Test
		@DisplayName("new encryption parameters saves to database")
		void save() {
			electionEventService.save(electionEventId, encryptionGroup);
			verify(electionEventRepository).save(any());
		}

	}

	@Nested
	@DisplayName("loading")
	class LoadTest {

		@Test
		@DisplayName("with null parameter throws NullPointerException")
		void loadNullParam() {
			assertThrows(NullPointerException.class, () -> electionEventService.getEncryptionGroup(null));
		}

		@Test
		@DisplayName("for the first time calls database")
		void firstTimeLoad() {
			electionEventService.save(electionEventId, encryptionGroup);

			final GqGroup loadedGroup = electionEventService.getEncryptionGroup(electionEventId);
			assertEquals(encryptionGroup, loadedGroup);

			verify(electionEventRepository).findByElectionEventId(electionEventId);
		}

		@Test
		@DisplayName("for the second time uses cache")
		void secondLoadUsesCache() {
			electionEventService.save(electionEventId, encryptionGroup);

			electionEventService.getEncryptionGroup(electionEventId);
			electionEventService.getEncryptionGroup(electionEventId);

			verify(electionEventRepository, times(1)).findByElectionEventId(electionEventId);
		}

		@Test
		@DisplayName("non existent election throws IllegalStateException")
		void nonExistentElection() {
			final String nonExistentId = random.genRandomString(ID_LENGTH, base16Alphabet);

			final IllegalStateException exception = assertThrows(IllegalStateException.class,
					() -> electionEventService.getEncryptionGroup(nonExistentId));

			final String expectedMessage = String.format("Election event not found. [electionEventId: %s]", nonExistentId);
			assertEquals(expectedMessage, Throwables.getRootCause(exception).getMessage());
		}

	}

}