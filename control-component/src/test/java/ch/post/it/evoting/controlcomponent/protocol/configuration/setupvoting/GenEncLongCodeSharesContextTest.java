/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.protocol.configuration.setupvoting;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.security.SecureRandom;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.google.common.base.Throwables;

import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;

@DisplayName("a GenEncLongCodeSharesContext built with")
class GenEncLongCodeSharesContextTest {

	private static final Random random = RandomFactory.createRandom();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();
	private static final GqGroup GQ_GROUP = GroupTestData.getGqGroup();
	private static final String ELECTION_EVENT_ID = random.genRandomString(ID_LENGTH, base16Alphabet);
	private static final String VERIFICATION_CARD_SET_ID = random.genRandomString(ID_LENGTH, base16Alphabet);
	private static final List<String> VERIFICATION_CARD_IDS = List.of(
			random.genRandomString(ID_LENGTH, base16Alphabet),
			random.genRandomString(ID_LENGTH, base16Alphabet),
			random.genRandomString(ID_LENGTH, base16Alphabet));

	private static final int NODE_ID = 1;

	private static int numberOfVotingOptions;

	@BeforeAll
	static void setupAll() {
		numberOfVotingOptions = new SecureRandom().nextInt(5) + 1;
	}

	@Test
	@DisplayName("null encryption group throws NullPointerException")
	void nullEncryptionGroup() {
		final GenEncLongCodeSharesContext.Builder builder = new GenEncLongCodeSharesContext.Builder()
				.setEncryptionGroup(null)
				.setNodeId(NODE_ID)
				.setElectionEventId(ELECTION_EVENT_ID)
				.setVerificationCardSetId(VERIFICATION_CARD_SET_ID)
				.setVerificationCardIds(VERIFICATION_CARD_IDS)
				.setNumberOfVotingOptions(numberOfVotingOptions);

		assertThrows(NullPointerException.class, builder::build);
	}

	@Test
	@DisplayName("invalid node id throws IllegalArgumentException")
	void invalidNodeId() {
		final int invalidNodeId = 0;
		final GenEncLongCodeSharesContext.Builder builder = new GenEncLongCodeSharesContext.Builder()
				.setEncryptionGroup(GQ_GROUP)
				.setNodeId(invalidNodeId)
				.setElectionEventId(ELECTION_EVENT_ID)
				.setVerificationCardSetId(VERIFICATION_CARD_SET_ID)
				.setVerificationCardIds(VERIFICATION_CARD_IDS)
				.setNumberOfVotingOptions(numberOfVotingOptions);

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, builder::build);

		final String errorMessage = String.format("The node id must be part of the known node ids. [nodeId: %s]", invalidNodeId);
		assertEquals(errorMessage, Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("null election event id throws NullPointerException")
	void nullElectionEventId() {
		final GenEncLongCodeSharesContext.Builder builder = new GenEncLongCodeSharesContext.Builder()
				.setEncryptionGroup(GQ_GROUP)
				.setNodeId(NODE_ID)
				.setElectionEventId(null)
				.setVerificationCardSetId(VERIFICATION_CARD_SET_ID)
				.setVerificationCardIds(VERIFICATION_CARD_IDS)
				.setNumberOfVotingOptions(numberOfVotingOptions);

		assertThrows(NullPointerException.class, builder::build);
	}

	@Test
	@DisplayName("invalid election event id throws FailedValidationException")
	void invalidElectionEventId() {
		GenEncLongCodeSharesContext.Builder builder = new GenEncLongCodeSharesContext.Builder()
				.setEncryptionGroup(GQ_GROUP)
				.setNodeId(NODE_ID)
				.setElectionEventId("")
				.setVerificationCardSetId(VERIFICATION_CARD_SET_ID)
				.setVerificationCardIds(VERIFICATION_CARD_IDS)
				.setNumberOfVotingOptions(numberOfVotingOptions);

		assertThrows(FailedValidationException.class, builder::build);

		builder = builder.setElectionEventId("0b88257ec32142b");
		assertThrows(FailedValidationException.class, builder::build);
	}

	@Test
	@DisplayName("null verification card set id throws NullPointerException")
	void nullVerificationCardSetId() {
		final GenEncLongCodeSharesContext.Builder builder = new GenEncLongCodeSharesContext.Builder()
				.setEncryptionGroup(GQ_GROUP)
				.setNodeId(NODE_ID)
				.setElectionEventId(ELECTION_EVENT_ID)
				.setVerificationCardSetId(null)
				.setVerificationCardIds(VERIFICATION_CARD_IDS)
				.setNumberOfVotingOptions(numberOfVotingOptions);

		assertThrows(NullPointerException.class, builder::build);
	}

	@Test
	@DisplayName("invalid verification card set id throws FailedValidationException")
	void invalidVerificationCardSetId() {
		GenEncLongCodeSharesContext.Builder builder = new GenEncLongCodeSharesContext.Builder()
				.setEncryptionGroup(GQ_GROUP)
				.setNodeId(NODE_ID)
				.setElectionEventId(ELECTION_EVENT_ID)
				.setVerificationCardSetId("")
				.setVerificationCardIds(VERIFICATION_CARD_IDS)
				.setNumberOfVotingOptions(numberOfVotingOptions);

		assertThrows(FailedValidationException.class, builder::build);

		builder = builder.setVerificationCardSetId("f1bd7b195d6f38");
		assertThrows(FailedValidationException.class, builder::build);
	}

	@Test
	@DisplayName("null verification card ids throws NullPointerException")
	void nullVERIFICATION_CARD_IDS() {
		final GenEncLongCodeSharesContext.Builder builder = new GenEncLongCodeSharesContext.Builder()
				.setEncryptionGroup(GQ_GROUP)
				.setNodeId(NODE_ID)
				.setElectionEventId(ELECTION_EVENT_ID)
				.setVerificationCardSetId(VERIFICATION_CARD_SET_ID)
				.setVerificationCardIds(null)
				.setNumberOfVotingOptions(numberOfVotingOptions);

		assertThrows(NullPointerException.class, builder::build);
	}

	@Test
	@DisplayName("non unique verification card ids throws IllegalArgumentException")
	void nonUniqueVERIFICATION_CARD_IDS() {
		final List<String> VERIFICATION_CARD_IDSWithDuplicates = List.of(VERIFICATION_CARD_IDS.get(0), VERIFICATION_CARD_IDS.get(1),
				VERIFICATION_CARD_IDS.get(0));

		final GenEncLongCodeSharesContext.Builder builder = new GenEncLongCodeSharesContext.Builder()
				.setEncryptionGroup(GQ_GROUP)
				.setNodeId(NODE_ID)
				.setElectionEventId(ELECTION_EVENT_ID)
				.setVerificationCardSetId(VERIFICATION_CARD_SET_ID)
				.setVerificationCardIds(VERIFICATION_CARD_IDSWithDuplicates)
				.setNumberOfVotingOptions(numberOfVotingOptions);

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, builder::build);
		assertEquals("The list of verification card ids contains duplicated values.", Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("invalid number of voting options throws IllegalArgumentException")
	void invalidNumberOfVotingOptions() {
		final GenEncLongCodeSharesContext.Builder builder = new GenEncLongCodeSharesContext.Builder()
				.setEncryptionGroup(GQ_GROUP)
				.setNodeId(NODE_ID)
				.setElectionEventId(ELECTION_EVENT_ID)
				.setVerificationCardSetId(VERIFICATION_CARD_SET_ID)
				.setVerificationCardIds(VERIFICATION_CARD_IDS)
				.setNumberOfVotingOptions(0);

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, builder::build);
		assertEquals("The number of voting options must be strictly positive.", Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("valid parameters gives expected context")
	void validParameters() {
		final GenEncLongCodeSharesContext.Builder builder = new GenEncLongCodeSharesContext.Builder()
				.setEncryptionGroup(GQ_GROUP)
				.setNodeId(NODE_ID)
				.setElectionEventId(ELECTION_EVENT_ID)
				.setVerificationCardSetId(VERIFICATION_CARD_SET_ID)
				.setVerificationCardIds(VERIFICATION_CARD_IDS)
				.setNumberOfVotingOptions(numberOfVotingOptions);

		assertDoesNotThrow(builder::build);
	}
}
