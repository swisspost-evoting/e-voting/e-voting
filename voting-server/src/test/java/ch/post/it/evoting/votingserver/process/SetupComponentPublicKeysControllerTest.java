/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.process;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.URL;
import java.security.SignatureException;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.domain.InvalidPayloadSignatureException;
import ch.post.it.evoting.evotinglibraries.domain.election.SetupComponentPublicKeys;
import ch.post.it.evoting.evotinglibraries.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.SetupComponentPublicKeysPayload;
import ch.post.it.evoting.evotinglibraries.domain.signature.Alias;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;
import ch.post.it.evoting.votingserver.idempotence.IdempotenceContext;
import ch.post.it.evoting.votingserver.idempotence.IdempotenceService;
import ch.post.it.evoting.votingserver.idempotence.IdempotentExecutionRepository;

@DisplayName("SetupComponentPublicKeysController")
class SetupComponentPublicKeysControllerTest {

	private static final SetupComponentPublicKeysService SETUP_COMPONENT_PUBLIC_KEYS_SERVICE = mock(SetupComponentPublicKeysService.class);
	private static final SignatureKeystore<Alias> SIGNATURE_KEYSTORE = mock(SignatureKeystore.class);
	private static final IdempotentExecutionRepository IDEMPOTENT_EXECUTION_REPOSITORY = mock(IdempotentExecutionRepository.class);
	private static final IdempotenceService<IdempotenceContext> IDEMPOTENCE_SERVICE = new IdempotenceService<>(IDEMPOTENT_EXECUTION_REPOSITORY);
	private static SetupComponentPublicKeysPayload setupComponentPublicKeysPayload;
	private static String tenantId;
	private static String electionEventId;
	private static SetupComponentPublicKeysController setupComponentPublicKeysController;

	@BeforeAll
	static void setUpAll() throws IOException {
		final ObjectMapper mapper = DomainObjectMapper.getNewInstance();
		final URL setupComponentPublicKeysPayloadUrl = SetupComponentPublicKeysControllerTest.class.getResource(
				"/process/setupComponentPublicKeysServiceTest/setup-component-public-keys-payload.json");
		setupComponentPublicKeysPayload = mapper.readValue(setupComponentPublicKeysPayloadUrl, SetupComponentPublicKeysPayload.class);

		setupComponentPublicKeysController = new SetupComponentPublicKeysController(SIGNATURE_KEYSTORE, SETUP_COMPONENT_PUBLIC_KEYS_SERVICE,
				IDEMPOTENCE_SERVICE);

		electionEventId = setupComponentPublicKeysPayload.getElectionEventId();
		tenantId = electionEventId;
	}

	@Test
	@DisplayName("save setup component public keys with valid parameters")
	void saveSetupComponentPublicKeysHappyPath() throws SignatureException {
		when(SIGNATURE_KEYSTORE.verifySignature(any(), eq(setupComponentPublicKeysPayload), any(), any())).thenReturn(true);
		when(IDEMPOTENT_EXECUTION_REPOSITORY.existsById(any())).thenReturn(false);
		when(IDEMPOTENT_EXECUTION_REPOSITORY.save(any())).thenReturn(null);

		setupComponentPublicKeysController.saveSetupComponentPublicKeys(electionEventId, setupComponentPublicKeysPayload);

		verify(SETUP_COMPONENT_PUBLIC_KEYS_SERVICE, times(1)).saveSetupComponentPublicKeys(any());
	}

	@Test
	@DisplayName("error verifying payload signature while saving throws IllegalStateException")
	void savingErrorVerifyingSignatureThrows() throws SignatureException {
		when(SIGNATURE_KEYSTORE.verifySignature(any(), eq(setupComponentPublicKeysPayload), any(), any())).thenThrow(SignatureException.class);

		final IllegalStateException exception = assertThrows(IllegalStateException.class,
				() -> setupComponentPublicKeysController.saveSetupComponentPublicKeys(electionEventId, setupComponentPublicKeysPayload));

		assertEquals(String.format("Could not verify the signature of the setup component public keys. [electionEventId: %s]", electionEventId),
				exception.getMessage());
	}

	@Test
	@DisplayName("save payload with invalid signature throws InvalidPayloadSignatureException")
	void savingInvalidSignatureThrows() throws SignatureException {
		when(SIGNATURE_KEYSTORE.verifySignature(any(), eq(setupComponentPublicKeysPayload), any(), any())).thenReturn(false);

		final InvalidPayloadSignatureException exception = assertThrows(InvalidPayloadSignatureException.class,
				() -> setupComponentPublicKeysController.saveSetupComponentPublicKeys(electionEventId, setupComponentPublicKeysPayload));

		assertEquals(String.format("Signature of payload %s is invalid. [electionEventId: %s]", SetupComponentPublicKeysPayload.class.getSimpleName(),
				electionEventId), exception.getMessage());
	}

	@Test
	@DisplayName("retrieve voting client public keys with valid parameters")
	void retrieveVotingClientPublicKeysHappyPath() {
		final SetupComponentPublicKeys setupComponentPublicKeys = setupComponentPublicKeysPayload.getSetupComponentPublicKeys();
		final VotingClientPublicKeys votingClientPublicKeys = new VotingClientPublicKeys(setupComponentPublicKeysPayload.getEncryptionGroup(),
				setupComponentPublicKeys.electionPublicKey(), setupComponentPublicKeys.choiceReturnCodesEncryptionPublicKey());

		when(SETUP_COMPONENT_PUBLIC_KEYS_SERVICE.getVotingClientPublicKeys(electionEventId)).thenReturn(votingClientPublicKeys);

		final VotingClientPublicKeys response = setupComponentPublicKeysController.getVotingClientPublicKeys(tenantId, electionEventId);

		verify(SETUP_COMPONENT_PUBLIC_KEYS_SERVICE, times(1)).getVotingClientPublicKeys(anyString());

		assertEquals(votingClientPublicKeys, response);
	}

	@Test
	@DisplayName("retrieve voting client public keys with invalid parameters throws")
	void retrieveVotingClientPublicKeysInvalidParametersThrows() {
		assertAll(
				() -> assertThrows(NullPointerException.class,
						() -> setupComponentPublicKeysController.getVotingClientPublicKeys(null, electionEventId)),
				() -> assertThrows(NullPointerException.class,
						() -> setupComponentPublicKeysController.getVotingClientPublicKeys(tenantId, null)),
				() -> assertThrows(FailedValidationException.class,
						() -> setupComponentPublicKeysController.getVotingClientPublicKeys(tenantId, "invalid electionEventId"))
		);
	}
}
