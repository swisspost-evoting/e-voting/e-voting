/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.process;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDateTime;

import org.junit.jupiter.api.Test;

class VerificationCardStateEntityTest {

	@Test
	void updateStateDoesNotThrow() throws InterruptedException {
		final VerificationCardStateEntity verificationCardStateEntity = new VerificationCardStateEntity();
		final LocalDateTime initialStateDate = verificationCardStateEntity.getStateDate();

		final VerificationCardState sent = VerificationCardState.SENT;

		assertNotEquals(sent, verificationCardStateEntity.getState());

		Thread.sleep(100); //force waiting a bit to ensure the LocalDateTime.now will give another value

		verificationCardStateEntity.updateState(sent);
		assertEquals(sent, verificationCardStateEntity.getState());
		assertTrue(initialStateDate.isBefore(verificationCardStateEntity.getStateDate()));
	}

	@Test
	void updateStateThrowsOnNullInput() {
		final VerificationCardStateEntity verificationCardStateEntity = new VerificationCardStateEntity();

		assertThrows(NullPointerException.class, () -> verificationCardStateEntity.updateState(null));
	}
}