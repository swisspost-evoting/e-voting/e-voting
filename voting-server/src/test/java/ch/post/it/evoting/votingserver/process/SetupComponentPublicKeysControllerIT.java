/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.process;

import static ch.post.it.evoting.evotinglibraries.domain.ControlComponentConstants.NODE_IDS;
import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

import java.security.SignatureException;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Streams;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamal;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalFactory;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.math.ZqElement;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.cryptoprimitives.test.tools.TestGroupSetup;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ElGamalGenerator;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.SchnorrProof;
import ch.post.it.evoting.evotinglibraries.domain.election.ControlComponentPublicKeys;
import ch.post.it.evoting.evotinglibraries.domain.election.SetupComponentPublicKeys;
import ch.post.it.evoting.evotinglibraries.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.SetupComponentPublicKeysPayload;
import ch.post.it.evoting.evotinglibraries.domain.signature.Alias;
import ch.post.it.evoting.evotinglibraries.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.votingserver.idempotence.IdempotenceContext;
import ch.post.it.evoting.votingserver.idempotence.IdempotenceService;

@WebFluxTest(SetupComponentPublicKeysController.class)
class SetupComponentPublicKeysControllerIT extends TestGroupSetup {

	private static final Random random = RandomFactory.createRandom();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();
	private static final ObjectMapper objectMapper = DomainObjectMapper.getNewInstance();

	private String electionEventId;
	private SetupComponentPublicKeysPayload setupComponentPublicKeysPayload;

	@Autowired
	private WebTestClient webTestClient;

	@MockBean
	private SetupComponentPublicKeysService setupComponentPublicKeysService;

	@MockBean
	private SignatureKeystore<Alias> signatureKeystore;

	@MockBean
	private IdempotenceService<IdempotenceContext> idempotenceService;

	@BeforeEach
	void setup() {
		reset(setupComponentPublicKeysService, signatureKeystore, idempotenceService);

		electionEventId = random.genRandomString(ID_LENGTH, base16Alphabet);
		setupComponentPublicKeysPayload = createSetupComponentPublicKeysPayload();
	}

	@Test
	@DisplayName("save returns 200 with correct response body")
	void happyPathSave() throws Exception {
		doNothing().when(idempotenceService).execute(any(), any(), any());

		doNothing().when(setupComponentPublicKeysService).saveSetupComponentPublicKeys(any());

		when(signatureKeystore.verifySignature(any(), any(), any(), any())).thenReturn(true);

		final String targetUrl = String.format("/api/v1/processor/configuration/setupkeys/electionevent/%s", electionEventId);

		// Request payload.
		final byte[] setupComponentPublicKeysPayloadBytes = objectMapper.writeValueAsBytes(setupComponentPublicKeysPayload);

		webTestClient.post().uri(targetUrl)
				.contentType(MediaType.APPLICATION_JSON)
				.bodyValue(setupComponentPublicKeysPayloadBytes)
				.exchange()
				.expectStatus().isOk();
	}

	@Test
	@DisplayName("save returns 412 when invalid signature")
	void saveWithInvalidSignature() throws Exception {
		doNothing().when(idempotenceService).execute(any(), any(), any());

		doNothing().when(setupComponentPublicKeysService).saveSetupComponentPublicKeys(any());

		doThrow(new SignatureException()).when(signatureKeystore).verifySignature(any(), any(), any(), any());

		final String targetUrl = String.format("/api/v1/processor/configuration/setupkeys/electionevent/%s", electionEventId);

		// Request payload.
		final byte[] setupComponentPublicKeysPayloadBytes = objectMapper.writeValueAsBytes(setupComponentPublicKeysPayload);

		webTestClient.post().uri(targetUrl)
				.contentType(MediaType.APPLICATION_JSON)
				.bodyValue(setupComponentPublicKeysPayloadBytes)
				.exchange()
				.expectStatus().isEqualTo(HttpStatus.PRECONDITION_FAILED.value());
	}

	@Test
	@DisplayName("save returns 200 with correct response body")
	void happyPathGet() throws Exception {
		final String targetUrl = String.format("/api/v1/processor/configuration/setupkeys/tenant/%s/electionevent/%s", "tenantId", electionEventId);

		// Response payload.
		final VotingClientPublicKeys votingClientPublicKeys = new VotingClientPublicKeys(gqGroup,
				setupComponentPublicKeysPayload.getSetupComponentPublicKeys()
						.electionPublicKey(), setupComponentPublicKeysPayload.getSetupComponentPublicKeys().choiceReturnCodesEncryptionPublicKey());
		when(setupComponentPublicKeysService.getVotingClientPublicKeys(any())).thenReturn(votingClientPublicKeys);

		webTestClient.get().uri(targetUrl)
				.exchange()
				.expectStatus().isOk()
				.expectBody().json(objectMapper.writeValueAsString(votingClientPublicKeys));
	}

	private SetupComponentPublicKeysPayload createSetupComponentPublicKeysPayload() {
		final int keySize = 3;
		final ElGamalGenerator elGamalGenerator = new ElGamalGenerator(gqGroup);
		final List<ControlComponentPublicKeys> combinedControlComponentPublicKeys = NODE_IDS.stream()
				.map(nodeId -> new ControlComponentPublicKeys(nodeId, elGamalGenerator.genRandomPublicKey(keySize), getSchnorrProofs(),
						elGamalGenerator.genRandomPublicKey(keySize), getSchnorrProofs()))
				.toList();
		final ElGamal elGamal = ElGamalFactory.createElGamal();
		final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> ccrChoiceReturnCodePublicKeys = combinedControlComponentPublicKeys.stream()
				.map(ControlComponentPublicKeys::ccrjChoiceReturnCodesEncryptionPublicKey)
				.collect(GroupVector.toGroupVector());
		final ElGamalMultiRecipientPublicKey electoralBoardPublicKey = elGamalGenerator.genRandomPublicKey(keySize);
		final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> publicKeys = Streams.concat(
						combinedControlComponentPublicKeys.stream()
								.map(ControlComponentPublicKeys::ccmjElectionPublicKey)
								.map(ccmElectionPublicKey ->
										new ElGamalMultiRecipientPublicKey(
												GroupVector.from(ccmElectionPublicKey.getKeyElements()))),
						Stream.of(electoralBoardPublicKey))
				.collect(GroupVector.toGroupVector());
		final SetupComponentPublicKeys setupComponentPublicKeys = new SetupComponentPublicKeys(combinedControlComponentPublicKeys,
				electoralBoardPublicKey, getSchnorrProofs(), elGamal.combinePublicKeys(publicKeys),
				elGamal.combinePublicKeys(ccrChoiceReturnCodePublicKeys));

		return new SetupComponentPublicKeysPayload(gqGroup, electionEventId, setupComponentPublicKeys,
				new CryptoPrimitivesSignature(new byte[] { 0, 1, 2 }));
	}

	private GroupVector<SchnorrProof, ZqGroup> getSchnorrProofs() {
		return IntStream.range(0, 3)
				.mapToObj(i -> new SchnorrProof(ZqElement.create(2, zqGroup), ZqElement.create(2, zqGroup)))
				.collect(GroupVector.toGroupVector());
	}

	@TestConfiguration
	static class Configuration {

		@Bean
		ObjectMapper setupComponentPublicKeysControllerITObjectMapper() {
			return DomainObjectMapper.getNewInstance();
		}
	}

}