/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.process;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.UncheckedIOException;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.domain.converters.ListConverter;
import ch.post.it.evoting.evotinglibraries.domain.mapper.DomainObjectMapper;

class ListConverterTest {

	private static final Random RANDOM = RandomFactory.createRandom();

	private static ListConverter converter;

	private List<String> list;

	@BeforeAll
	static void setupAll() {
		converter = new ListConverter(DomainObjectMapper.getNewInstance());
	}

	@BeforeEach
	void setup() {
		list = RANDOM.genUniqueDecimalStrings(4, 10);
	}

	@Test
	void convertToDatabaseColumnWithNullArgumentThrows() {
		assertThrows(NullPointerException.class, () -> converter.convertToDatabaseColumn(null));
	}

	@Test
	void convertToEntityAttributeWithNullArgumentThrows() {
		assertThrows(NullPointerException.class, () -> converter.convertToEntityAttribute(null));
	}

	@Test
	void convertToEntityAttributeWhenByteArrayCannotBeConvertedThenThrows() {
		assertThrows(UncheckedIOException.class, () -> converter.convertToEntityAttribute(new byte[] { 0b0 }));
	}

	@RepeatedTest(100)
	void cycle() {
		final byte[] bytes = assertDoesNotThrow(() -> converter.convertToDatabaseColumn(list));
		final List<String> convertedList = assertDoesNotThrow(() -> converter.convertToEntityAttribute(bytes));
		assertEquals(list, convertedList);
	}
}
