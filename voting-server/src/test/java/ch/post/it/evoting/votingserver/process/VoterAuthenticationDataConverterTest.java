/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.process;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base64Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.domain.configuration.SetupComponentVoterAuthenticationData;
import ch.post.it.evoting.evotinglibraries.domain.mapper.DomainObjectMapper;

@DisplayName("VoterAuthenticationDataConverter calling")
class VoterAuthenticationDataConverterTest {

	private static final Random RANDOM = RandomFactory.createRandom();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();
	private static final Alphabet base64Alphabet = Base64Alphabet.getInstance();
	private static final VoterAuthenticationDataConverter converter = new VoterAuthenticationDataConverter(DomainObjectMapper.getNewInstance());

	private SetupComponentVoterAuthenticationData voterAuthenticationData;

	@BeforeEach
	void setup() {
		final String electionEventId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		final String verificationCardSetId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		final String votingCardSetId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		final String ballotBoxId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		final String ballotId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		final String verificationCardId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		final String votingCardId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		final String credentialId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		final String baseAuthenticationChallenge = RANDOM.genRandomString(44, base64Alphabet);
		voterAuthenticationData = new SetupComponentVoterAuthenticationData(electionEventId, verificationCardSetId, votingCardSetId, ballotBoxId,
				ballotId, verificationCardId, votingCardId, credentialId, baseAuthenticationChallenge);
	}

	@Test
	@DisplayName("convert with null argument throws")
	void convertWithNullArgumentThrows() {
		assertThrows(NullPointerException.class, () -> converter.convertToDatabaseColumn(null));
		assertThrows(NullPointerException.class, () -> converter.convertToEntityAttribute(null));
	}

	@RepeatedTest(10)
	@DisplayName("convertToDatabaseColumn then convertToEntityAttribute gives original data")
	void cycle() {
		final byte[] databaseColumnBytes = converter.convertToDatabaseColumn(voterAuthenticationData);
		final SetupComponentVoterAuthenticationData cycledVoterAuthenticationData = converter.convertToEntityAttribute(databaseColumnBytes);
		assertEquals(voterAuthenticationData, cycledVoterAuthenticationData);
	}
}
