/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.process;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;

class ElectionEventDTOTest {

	private static final Random RANDOM = RandomFactory.createRandom();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();

	private String electionEventId;
	private String electionEventAlias;
	private String electionEventDescription;

	@BeforeEach
	void setup() {
		electionEventId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		electionEventAlias = electionEventId + "_alias";
		electionEventDescription = electionEventId + "_description";
	}

	@Test
	void constructWithNullArgumentsThrows() {
		assertThrows(NullPointerException.class, () -> new ElectionEventDTO(null, electionEventAlias, electionEventDescription));
		assertThrows(NullPointerException.class, () -> new ElectionEventDTO(electionEventId, null, electionEventDescription));
		assertThrows(NullPointerException.class, () -> new ElectionEventDTO(electionEventId, electionEventAlias, null));
	}

	@Test
	void constructWithNonUuidElectionEventIdThrows() {
		assertThrows(FailedValidationException.class, () -> new ElectionEventDTO("xyz", electionEventAlias, electionEventDescription));
	}

	@Test
	void constructWithAliasOrDescriptionNotValidUcsThrows() {
		assertThrows(FailedValidationException.class, () -> new ElectionEventDTO(electionEventId, "\uDFFF", electionEventDescription));
		assertThrows(FailedValidationException.class, () -> new ElectionEventDTO(electionEventId, electionEventAlias, "\uDFFF"));
	}

	@Test
	void constructWithValidArgumentsDoesNotThrow() {
		final ElectionEventDTO electionEventDTO = assertDoesNotThrow(
				() -> new ElectionEventDTO(electionEventId, electionEventAlias, electionEventDescription));
		assertEquals(electionEventId, electionEventDTO.electionEventId());
		assertEquals(electionEventAlias, electionEventDTO.electionEventAlias());
		assertEquals(electionEventDescription, electionEventDTO.electionEventDescription());
	}
}
