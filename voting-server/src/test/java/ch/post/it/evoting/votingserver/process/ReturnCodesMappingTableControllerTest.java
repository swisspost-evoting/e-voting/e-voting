/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.process;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.URL;
import java.security.SignatureException;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.domain.InvalidPayloadSignatureException;
import ch.post.it.evoting.domain.configuration.SetupComponentCMTablePayload;
import ch.post.it.evoting.evotinglibraries.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.evotinglibraries.domain.signature.Alias;
import ch.post.it.evoting.votingserver.idempotence.IdempotenceContext;
import ch.post.it.evoting.votingserver.idempotence.IdempotenceService;
import ch.post.it.evoting.votingserver.idempotence.IdempotentExecutionRepository;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@DisplayName("ReturnCodesMappingTableController")
class ReturnCodesMappingTableControllerTest {

	private static SetupComponentCMTablePayload setupComponentCMTablePayload;
	private static String electionEventId;
	private static String verificationCardSetId;
	private static int chunkId;
	private static SignatureKeystore<Alias> signatureKeystoreService;
	private static IdempotentExecutionRepository idempotentExecutionRepository;
	private static ReturnCodesMappingTableService returnCodesMappingTableService;
	private static ReturnCodesMappingTableController returnCodesMappingTableController;

	@BeforeAll
	static void setUpAll() throws IOException {
		signatureKeystoreService = mock(SignatureKeystore.class);
		idempotentExecutionRepository = mock(IdempotentExecutionRepository.class);
		final IdempotenceService<IdempotenceContext> idempotenceService = new IdempotenceService<>(idempotentExecutionRepository);
		returnCodesMappingTableService = mock(ReturnCodesMappingTableService.class);
		returnCodesMappingTableController = new ReturnCodesMappingTableController(
				signatureKeystoreService, returnCodesMappingTableService, idempotenceService);

		final ObjectMapper mapper = DomainObjectMapper.getNewInstance();
		final URL returnCodesMappingTablePayloadUrl = ReturnCodesMappingTableControllerTest.class.getResource(
				"/process/returnCodesMappingTableResourceTest/setupComponentCMTablePayload.0.json");
		setupComponentCMTablePayload = mapper.readValue(returnCodesMappingTablePayloadUrl, SetupComponentCMTablePayload.class);
		electionEventId = setupComponentCMTablePayload.getElectionEventId();
		verificationCardSetId = setupComponentCMTablePayload.getVerificationCardSetId();
		chunkId = setupComponentCMTablePayload.getChunkId();
	}

	@Test
	@DisplayName("calling save with valid parameters")
	void saveReturnCodesMappingTableHappyPath() throws SignatureException {
		when(signatureKeystoreService.verifySignature(any(), any(), any(), any())).thenReturn(true);
		when(idempotentExecutionRepository.existsById(any())).thenReturn(false);
		returnCodesMappingTableController.saveReturnCodesMappingTable(electionEventId, verificationCardSetId,
				Flux.just(setupComponentCMTablePayload)).block();

		verify(returnCodesMappingTableService, times(1)).saveReturnCodesMappingTable(any());
	}

	@Test
	@DisplayName("calling save with valid parameters but idempotent service say it was already executed")
	void saveReturnCodesMappingTableIdempotentAlreadyExist() throws SignatureException {
		when(signatureKeystoreService.verifySignature(any(), any(), any(), any())).thenReturn(true);
		when(idempotentExecutionRepository.existsById(any())).thenReturn(true);
		returnCodesMappingTableController.saveReturnCodesMappingTable(electionEventId, verificationCardSetId,
				Flux.just(setupComponentCMTablePayload)).block();

		verify(returnCodesMappingTableService, times(0)).saveReturnCodesMappingTable(any());
		verify(idempotentExecutionRepository, times(1)).existsById(any());
	}

	@Test
	@DisplayName("calling save with invalid signature")
	void saveReturnCodesMappingTableWithInvalidSignature() throws SignatureException {
		when(signatureKeystoreService.verifySignature(any(), any(), any(), any())).thenReturn(false);

		final Mono<Void> saveReturnCodesMappingTableMono = returnCodesMappingTableController.saveReturnCodesMappingTable(electionEventId,
				verificationCardSetId,
				Flux.just(setupComponentCMTablePayload));
		final InvalidPayloadSignatureException exception = assertThrows(InvalidPayloadSignatureException.class,
				saveReturnCodesMappingTableMono::block);

		final String errorMessage = String.format("Signature of payload %s is invalid. [electionEventId: %s, verificationCardSetId: %s]",
				SetupComponentCMTablePayload.class.getSimpleName(), setupComponentCMTablePayload.getElectionEventId(),
				setupComponentCMTablePayload.getVerificationCardSetId());

		assertEquals(errorMessage, exception.getMessage());
	}

	@Test
	@DisplayName("calling save with invalid signature's content")
	void saveReturnCodesMappingTableWithInvalidSignatureContent() throws SignatureException {
		when(signatureKeystoreService.verifySignature(any(), any(), any(), any())).thenThrow(SignatureException.class);

		final Mono<Void> saveReturnCodesMappingTableMono = returnCodesMappingTableController.saveReturnCodesMappingTable(electionEventId,
				verificationCardSetId, Flux.just(setupComponentCMTablePayload));
		final IllegalStateException exception = assertThrows(IllegalStateException.class,
				saveReturnCodesMappingTableMono::block);

		final String errorMessage = String.format(
				"Couldn't verify the signature of the setup component CMTable payload. [electionEventId: %s, verificationCardSetId: %s]",
				electionEventId, verificationCardSetId);

		assertEquals(errorMessage, exception.getMessage());
	}
}
