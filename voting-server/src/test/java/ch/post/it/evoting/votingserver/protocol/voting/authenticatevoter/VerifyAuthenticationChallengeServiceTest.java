/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.protocol.voting.authenticatevoter;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static ch.post.it.evoting.votingserver.process.Constants.TWO_POW_256;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base64Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.domain.configuration.SetupComponentVoterAuthenticationData;
import ch.post.it.evoting.evotinglibraries.domain.election.ElectionEventContext;
import ch.post.it.evoting.evotinglibraries.domain.election.VerificationCardSetContext;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.generators.ElectionEventContextPayloadGenerator;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;
import ch.post.it.evoting.votingserver.process.ElectionEventEntity;
import ch.post.it.evoting.votingserver.process.VerificationCardService;
import ch.post.it.evoting.votingserver.process.VerificationCardSetEntity;
import ch.post.it.evoting.votingserver.process.VerificationCardState;
import ch.post.it.evoting.votingserver.process.configuration.setupvoting.VoterAuthenticationDataService;
import ch.post.it.evoting.votingserver.process.voting.AuthenticationChallenge;
import ch.post.it.evoting.votingserver.process.voting.AuthenticationStep;
import ch.post.it.evoting.votingserver.process.voting.VerifyAuthenticationChallengeException;

@DisplayName("verifyAuthenticationChallenge called with")
class VerifyAuthenticationChallengeServiceTest {

	private static final Random random = RandomFactory.createRandom();
	private static final VerificationCardService verificationCardService = mock(VerificationCardService.class);
	private static final VerifyAuthenticationChallengeAlgorithm verifyAuthenticationChallengeAlgorithm = mock(
			VerifyAuthenticationChallengeAlgorithm.class);

	private static VerifyAuthenticationChallengeService verifyAuthenticationChallengeService;
	private static String electionEventId;
	private static AuthenticationStep authenticationStep;
	private static AuthenticationChallenge authenticationChallenge;
	private static VerificationCardSetEntity.Builder verificationCardSetEntityBuilder;
	private static VerificationCardSetContext verificationCardSetContext;

	@BeforeAll
	static void setUpAll() {
		final VoterAuthenticationDataService voterAuthenticationDataService = mock(VoterAuthenticationDataService.class);
		verifyAuthenticationChallengeService = new VerifyAuthenticationChallengeService(verificationCardService, voterAuthenticationDataService,
				verifyAuthenticationChallengeAlgorithm);

		final ElectionEventContextPayloadGenerator electionEventContextPayloadGenerator = new ElectionEventContextPayloadGenerator();
		final ElectionEventContext electionEventContext = electionEventContextPayloadGenerator.generate().getElectionEventContext();
		electionEventId = electionEventContext.electionEventId();
		final Map<AuthenticationStep, List<VerificationCardState>> validVerificationCardStatesMap = Map.of(
				AuthenticationStep.AUTHENTICATE_VOTER, List.of(VerificationCardState.INITIAL, VerificationCardState.SENT,
						VerificationCardState.CONFIRMING, VerificationCardState.CONFIRMED),
				AuthenticationStep.SEND_VOTE, List.of(VerificationCardState.INITIAL),
				AuthenticationStep.CONFIRM_VOTE, List.of(VerificationCardState.SENT, VerificationCardState.CONFIRMING)
		);

		authenticationStep = AuthenticationStep.values()[random.genRandomInteger(AuthenticationStep.values().length)];
		final String credentialId = random.genRandomString(ID_LENGTH, Base16Alphabet.getInstance());
		final String baseAuthenticationChallenge = random.genRandomString(44, Base64Alphabet.getInstance());
		final BigInteger nonce = random.genRandomInteger(TWO_POW_256);
		authenticationChallenge = new AuthenticationChallenge(credentialId, baseAuthenticationChallenge, nonce);

		final List<VerificationCardState> validVerificationCardStates = validVerificationCardStatesMap.get(authenticationStep);
		final VerificationCardState verificationCardState = validVerificationCardStates.get(
				random.genRandomInteger(validVerificationCardStates.size()));
		when(verificationCardService.getVerificationCardState(credentialId)).thenReturn(verificationCardState);

		verificationCardSetContext = electionEventContext.verificationCardSetContexts().getFirst();

		final SetupComponentVoterAuthenticationData setupComponentVoterAuthenticationData = new SetupComponentVoterAuthenticationData(
				electionEventId,
				verificationCardSetContext.getVerificationCardSetId(),
				random.genRandomString(ID_LENGTH, Base16Alphabet.getInstance()),
				verificationCardSetContext.getBallotBoxId(),
				random.genRandomString(ID_LENGTH, Base16Alphabet.getInstance()),
				random.genRandomString(ID_LENGTH, Base16Alphabet.getInstance()),
				random.genRandomString(ID_LENGTH, Base16Alphabet.getInstance()),
				credentialId, baseAuthenticationChallenge);
		when(voterAuthenticationDataService.getVoterAuthenticationData(electionEventId, credentialId))
				.thenReturn(setupComponentVoterAuthenticationData);
	}

	@BeforeEach
	void setUp() {
		when(verifyAuthenticationChallengeAlgorithm.verifyAuthenticationChallenge(any(), any()))
				.thenReturn(VerifyAuthenticationChallengeOutput.success());

		verificationCardSetEntityBuilder = new VerificationCardSetEntity.Builder()
				.setVerificationCardSetId(verificationCardSetContext.getVerificationCardSetId())
				.setElectionEventEntity(new ElectionEventEntity())
				.setBallotBoxId(verificationCardSetContext.getBallotBoxId())
				.setGracePeriod(verificationCardSetContext.getGracePeriod())
				.setPrimesMappingTable(verificationCardSetContext.getPrimesMappingTable())
				.setBallotBoxStartTime(verificationCardSetContext.getBallotBoxStartTime())
				.setBallotBoxFinishTime(verificationCardSetContext.getBallotBoxFinishTime())
				.setTestBallotBox(verificationCardSetContext.isTestBallotBox());
		final VerificationCardSetEntity verificationCardSetEntity = verificationCardSetEntityBuilder.build();
		when(verificationCardService.getVerificationCardSetEntity(authenticationChallenge.derivedVoterIdentifier()))
				.thenReturn(verificationCardSetEntity);
	}

	private static Stream<Arguments> provideNullParameters() {
		return Stream.of(
				Arguments.of(null, authenticationStep, authenticationChallenge),
				Arguments.of(electionEventId, null, authenticationChallenge),
				Arguments.of(electionEventId, authenticationStep, null)
		);
	}

	@ParameterizedTest
	@MethodSource("provideNullParameters")
	@DisplayName("null parameters throws NullPointerException")
	void verifyAuthenticationChallengeWithNullParametersThrows(final String electionEventId, final AuthenticationStep authenticationStep,
			final AuthenticationChallenge authenticationChallenge) {
		assertThrows(NullPointerException.class,
				() -> verifyAuthenticationChallengeService.verifyAuthenticationChallenge(electionEventId, authenticationStep,
						authenticationChallenge));
	}

	@Test
	@DisplayName("invalid election event id throws FailedValidationException")
	void verifyAuthenticationChallengeWithInvalidElectionEventIdThrows() {
		assertThrows(FailedValidationException.class,
				() -> verifyAuthenticationChallengeService.verifyAuthenticationChallenge("InvalidElectionEventId", authenticationStep,
						authenticationChallenge));
	}

	@Test
	@DisplayName("ballot box not open yet throws VerifyAuthenticationChallengeException")
	void verifyAuthenticationChallengeWithBallotBoxNotOpenYetThrows() {
		final VerificationCardSetEntity verificationCardSetEntity = verificationCardSetEntityBuilder.build();
		final VerificationCardSetEntity ballotBoxNotOpenYetEntity = verificationCardSetEntityBuilder
				.setBallotBoxStartTime(verificationCardSetEntity.getBallotBoxStartTime().plusSeconds(2))
				.build();
		when(verificationCardService.getVerificationCardSetEntity(authenticationChallenge.derivedVoterIdentifier()))
				.thenReturn(ballotBoxNotOpenYetEntity);

		final VerifyAuthenticationChallengeException exception = assertThrows(VerifyAuthenticationChallengeException.class,
				() -> verifyAuthenticationChallengeService.verifyAuthenticationChallenge(electionEventId, authenticationStep,
						authenticationChallenge));

		final String expected = String.format("The ballot box is not open yet. [step: %s, credentialId: %s, ballotBoxId: %s]", authenticationStep,
				authenticationChallenge.derivedVoterIdentifier(), verificationCardSetEntity.getBallotBoxId());
		assertEquals(expected, exception.getErrorMessage());
		assertEquals(VerifyAuthenticationChallengeOutput.VerifyAuthenticationChallengeStatus.BALLOT_BOX_NOT_STARTED, exception.getErrorStatus());
	}

	@Test
	@DisplayName("ballot box closed throws VerifyAuthenticationChallengeException")
	void verifyAuthenticationChallengeWithBallotBoxClosedThrows() {
		final VerificationCardSetEntity verificationCardSetEntity = verificationCardSetEntityBuilder.build();
		final VerificationCardSetEntity ballotBoxClosedEntity = verificationCardSetEntityBuilder
				.setBallotBoxStartTime(LocalDateTime.now().minusDays(2))
				// subtract grace period in case we are in the step SEND_VOTE or CONFIRM_VOTE.
				.setBallotBoxFinishTime(verificationCardSetEntity.getBallotBoxStartTime().minusSeconds(verificationCardSetContext.getGracePeriod()))
				.build();
		when(verificationCardService.getVerificationCardSetEntity(authenticationChallenge.derivedVoterIdentifier()))
				.thenReturn(ballotBoxClosedEntity);

		final VerifyAuthenticationChallengeException exception = assertThrows(VerifyAuthenticationChallengeException.class,
				() -> verifyAuthenticationChallengeService.verifyAuthenticationChallenge(electionEventId, authenticationStep,
						authenticationChallenge));

		final String expected = String.format("The ballot box is closed. [step: %s, credentialId: %s, ballotBoxId: %s]", authenticationStep,
				authenticationChallenge.derivedVoterIdentifier(), verificationCardSetEntity.getBallotBoxId());
		assertEquals(expected, exception.getErrorMessage());
		assertEquals(VerifyAuthenticationChallengeOutput.VerifyAuthenticationChallengeStatus.BALLOT_BOX_ENDED, exception.getErrorStatus());
	}

	@Test
	@DisplayName("authentication failed throws VerifyAuthenticationChallengeException")
	void verifyAuthenticationChallengeWithAuthenticationFailedThrows() {
		final String errorMessage = "error message.";
		final List<VerifyAuthenticationChallengeOutput> errorOutputs = List.of(
				VerifyAuthenticationChallengeOutput.authenticationAttemptsExceeded(errorMessage),
				VerifyAuthenticationChallengeOutput.authenticationChallengeError(errorMessage),
				VerifyAuthenticationChallengeOutput.invalidExtendedFactor(errorMessage, 0)
		);
		final VerifyAuthenticationChallengeOutput chosenErrorOutput = errorOutputs.get(random.genRandomInteger(errorOutputs.size()));
		when(verifyAuthenticationChallengeAlgorithm.verifyAuthenticationChallenge(any(), any())).thenReturn(chosenErrorOutput);

		final VerifyAuthenticationChallengeException exception = assertThrows(VerifyAuthenticationChallengeException.class,
				() -> verifyAuthenticationChallengeService.verifyAuthenticationChallenge(electionEventId, authenticationStep,
						authenticationChallenge));

		final String expected = String.format("Verification of authentication challenge failed. [electionEventId: %s, credentialId: %s, reason: %s]",
				electionEventId, authenticationChallenge.derivedVoterIdentifier(), errorMessage);
		assertEquals(expected, exception.getErrorMessage());
		assertEquals(chosenErrorOutput.getStatus(), exception.getErrorStatus());
	}

	@Test
	@DisplayName("valid parameters does not throw")
	void verifyAuthenticationChallengeWithValidParametersDoesNotThrow() {
		assertDoesNotThrow(() -> verifyAuthenticationChallengeService.verifyAuthenticationChallenge(electionEventId, authenticationStep,
				authenticationChallenge));
	}

}
