/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.process;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base64Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.evotinglibraries.domain.election.Ballot;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;

class BallotDataServiceTest {

	private static final Random RANDOM = RandomFactory.createRandom();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();
	private static final Alphabet base64Alphabet = Base64Alphabet.getInstance();

	private static BallotDataService ballotDataService;

	private String electionEventId;
	private String ballotId;
	private Ballot ballot;
	private String ballotTextJson;

	@BeforeAll
	static void beforeAll() {
		final ElectionEventService electionEventService = mock(ElectionEventService.class);
		final BallotDataRepository ballotDataRepository = mock(BallotDataRepository.class);
		ballotDataService = new BallotDataService(electionEventService, ballotDataRepository);
	}

	@BeforeEach
	void setup() {
		electionEventId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		ballotId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		ballot = mock(Ballot.class);
		ballotTextJson = RANDOM.genRandomString(100, base64Alphabet);
	}

	@Test
	void saveBallotDataWithNullArgumentsThrows() {
		assertThrows(NullPointerException.class, () -> ballotDataService.saveBallotData(null, ballotId, ballot, ballotTextJson));
		assertThrows(NullPointerException.class, () -> ballotDataService.saveBallotData(electionEventId, null, ballot, ballotTextJson));
		assertThrows(NullPointerException.class, () -> ballotDataService.saveBallotData(electionEventId, ballotId, null, ballotTextJson));
		assertThrows(NullPointerException.class, () -> ballotDataService.saveBallotData(electionEventId, ballotId, ballot, null));
	}

	@Test
	void saveBallotDataWithNonUuidsThrows() {
		assertThrows(FailedValidationException.class, () -> ballotDataService.saveBallotData("nonUUID", ballotId, ballot, ballotTextJson));
		assertThrows(FailedValidationException.class, () -> ballotDataService.saveBallotData(electionEventId, "nonUUID", ballot, ballotTextJson));
	}

	@Test
	void getBallotDataWithNullArgumentsThrows() {
		assertThrows(NullPointerException.class, () -> ballotDataService.getBallotData(null, ballotId));
		assertThrows(NullPointerException.class, () -> ballotDataService.getBallotData(electionEventId, null));
	}

	@Test
	void getBallotDataWithNonUuidsThrows() {
		assertThrows(FailedValidationException.class, () -> ballotDataService.getBallotData("nonUUID", ballotId));
		assertThrows(FailedValidationException.class, () -> ballotDataService.getBallotData(electionEventId, "nonUUID"));
	}
}
