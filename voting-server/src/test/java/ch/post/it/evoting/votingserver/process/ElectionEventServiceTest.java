/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.process;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;

class ElectionEventServiceTest {

	private static final Random RANDOM = RandomFactory.createRandom();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();

	private static ElectionEventService electionEventService;

	private String electionEventId;
	private GqGroup encryptionGroup;

	@BeforeAll
	static void beforeAll() {
		electionEventService = new ElectionEventService(mock(ElectionEventRepository.class));
	}

	@BeforeEach
	void setup() {
		electionEventId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		encryptionGroup = GroupTestData.getGqGroup();
	}

	@Test
	void saveWithNullArgumentsThrows() {
		assertThrows(NullPointerException.class, () -> electionEventService.save(null, encryptionGroup));
		assertThrows(NullPointerException.class, () -> electionEventService.save(electionEventId, null));
	}
}
