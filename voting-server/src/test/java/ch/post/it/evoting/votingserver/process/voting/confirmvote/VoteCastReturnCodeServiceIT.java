/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.process.voting.confirmvote;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.SignatureException;
import java.util.Base64;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.scheduling.concurrent.CustomizableThreadFactory;
import org.springframework.test.annotation.DirtiesContext;

import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableBigInteger;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.GqGroupGenerator;
import ch.post.it.evoting.domain.voting.confirmvote.ConfirmationKey;
import ch.post.it.evoting.domain.voting.confirmvote.ControlComponenthlVCCPayload;
import ch.post.it.evoting.domain.voting.confirmvote.ControlComponentlVCCSharePayload;
import ch.post.it.evoting.domain.voting.confirmvote.LongVoteCastReturnCodesShare;
import ch.post.it.evoting.domain.voting.confirmvote.VotingServerConfirmPayload;
import ch.post.it.evoting.evotinglibraries.domain.common.ContextIds;
import ch.post.it.evoting.evotinglibraries.domain.signature.Alias;
import ch.post.it.evoting.evotinglibraries.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.votingserver.ArtemisSupport;
import ch.post.it.evoting.votingserver.BroadcastIntegrationTestService;
import ch.post.it.evoting.votingserver.messaging.MessageHandler;
import ch.post.it.evoting.votingserver.messaging.ResponseCompletionService;
import ch.post.it.evoting.votingserver.messaging.Serializer;
import ch.post.it.evoting.votingserver.process.ElectionEventService;
import ch.post.it.evoting.votingserver.process.VerificationCardService;
import ch.post.it.evoting.votingserver.process.VerificationCardStateService;
import ch.post.it.evoting.votingserver.process.voting.ConfirmationKeyInvalidException;
import ch.post.it.evoting.votingserver.protocol.voting.confirmvote.ExtractVCCOutput;
import ch.post.it.evoting.votingserver.protocol.voting.confirmvote.ExtractVCCService;
import ch.post.it.evoting.votingserver.shelf.WorkflowShelfService;

class VoteCastReturnCodeServiceIT extends ArtemisSupport {

	private static final Random RANDOM = RandomFactory.createRandom();

	private static GqGroup encryptionGroup;
	private static ConfirmationKey confirmationKey;
	private static String electionEventId;
	private static String verificationCardSetId;
	private static String verificationCardId;
	private static ContextIds contextIds;

	@SpyBean
	SignatureKeystore<Alias> signatureKeystore;
	@SpyBean
	ExtractVCCService extractVCCService;
	@SpyBean
	VerificationCardService verificationCardService;
	@SpyBean
	VerificationCardStateService verificationCardStateService;
	private VoteCastReturnCodeService voteCastReturnCodeService;
	@Autowired
	private BroadcastIntegrationTestService broadcastIntegrationTestService;
	@Autowired
	private MessageHandler messageHandler;
	@Autowired
	private Serializer serializer;
	@Autowired
	private WorkflowShelfService workflowShelfService;
	@Autowired
	private ResponseCompletionService responseCompletionService;
	@Autowired
	private Hash hash;

	@BeforeAll
	static void setUpAll() {
		encryptionGroup = GroupTestData.getGqGroup();
		final GqElement randomGqElement = new GqGroupGenerator(encryptionGroup).genMember();
		electionEventId = RANDOM.genRandomString(ID_LENGTH, Base16Alphabet.getInstance());
		verificationCardSetId = RANDOM.genRandomString(ID_LENGTH, Base16Alphabet.getInstance());
		verificationCardId = RANDOM.genRandomString(ID_LENGTH, Base16Alphabet.getInstance());
		contextIds = new ContextIds(electionEventId, verificationCardSetId, verificationCardId);
		confirmationKey = new ConfirmationKey(contextIds, randomGqElement);
	}

	@BeforeEach
	void setup() throws SignatureException {
		final ElectionEventService electionEventService = mock(ElectionEventService.class);

		when(electionEventService.getEncryptionGroup(electionEventId)).thenReturn(encryptionGroup);

		doReturn(true).when(signatureKeystore).verifySignature(any(), any(), any(), any());
		doReturn(new byte[] { 1, 2, 3 }).when(signatureKeystore).generateSignature(any(), any());
		doReturn(new ExtractVCCOutput("12345678")).when(extractVCCService).extractVCC(any(), any());
		doNothing().when(verificationCardService).saveConfirmingState(any());
		doNothing().when(verificationCardService).saveConfirmedState(any(), any());
		doReturn(5).when(verificationCardService).incrementConfirmationAttempts(any());
		doReturn(0).when(verificationCardStateService).getUnsuccessfulConfirmationAttemptCount(anyString());

		voteCastReturnCodeService = new VoteCastReturnCodeService(serializer, messageHandler, extractVCCService, electionEventService,
				workflowShelfService, verificationCardService, signatureKeystore, responseCompletionService);
	}

	@Test
	@DisplayName("Process create LVCC Share Contributions")
	@DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
	void happyPath() throws InterruptedException {

		final CountDownLatch serviceCountDownLatch = new CountDownLatch(1);

		final VotingServerConfirmPayload votingServerConfirmPayload = genRequestPayload();

		final BigInteger confirmationKeyValue = votingServerConfirmPayload.getConfirmationKey().element().getValue();
		final String confirmationKeyAsString = Base64.getEncoder().encodeToString(hash.recursiveHash(HashableBigInteger.from(confirmationKeyValue)));

		final ExecutorService executorService = Executors.newFixedThreadPool(1, new CustomizableThreadFactory("service-pool-"));

		// Call the service in a separate thread and wait for the results.
		executorService.execute(() -> {
			voteCastReturnCodeService.retrieveShortVoteCastCode(contextIds, votingServerConfirmPayload.getConfirmationKey().element()).get();
			serviceCountDownLatch.countDown();
		});
		broadcastIntegrationTestService.awaitBroadcastRequestsSaved(30, SECONDS);

		broadcastIntegrationTestService.respondWith(this::getLongVoteCastReturnCodesShareHashResponsePayload);

		broadcastIntegrationTestService.awaitBroadcastRequestsSaved(30, SECONDS);

		broadcastIntegrationTestService.respondWith(this::generateControlComponentlVCCSharePayload);

		assertTrue(serviceCountDownLatch.await(30, SECONDS));
	}

	@Test
	@DisplayName("Process create LVCC Share Contributions with wrong verification")
	@DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
	void verifylVCCShareWithControlComponentlVCCSharePayloadNotVerifiedReturnsBadRequest() throws InterruptedException {
		final CountDownLatch serviceCountDownLatch = new CountDownLatch(1);

		final VotingServerConfirmPayload votingServerConfirmPayload = genRequestPayload();

		final GqElement confirmationKey = votingServerConfirmPayload.getConfirmationKey().element();

		final ExecutorService executorService = Executors.newFixedThreadPool(1, new CustomizableThreadFactory("http-pool-"));

		//Send the HTTP request in a separate thread and wait for the results.
		executorService.execute(() -> {
			assertThrows(ConfirmationKeyInvalidException.class,
					() -> voteCastReturnCodeService.retrieveShortVoteCastCode(contextIds, confirmationKey).get());
			serviceCountDownLatch.countDown();
		});

		broadcastIntegrationTestService.awaitBroadcastRequestsSaved(30, SECONDS);

		broadcastIntegrationTestService.respondWith(this::getLongVoteCastReturnCodesShareHashResponsePayload);

		broadcastIntegrationTestService.awaitBroadcastRequestsSaved(30, SECONDS);

		broadcastIntegrationTestService.respondWith(this::generateControlComponentlVCCSharePayloadNotVerified);

		assertTrue(serviceCountDownLatch.await(30, SECONDS));
	}

	private ControlComponenthlVCCPayload getLongVoteCastReturnCodesShareHashResponsePayload(final Integer nodeId) {
		final int unsuccessfulConfirmationAttemptCount = 0;
		final CryptoPrimitivesSignature signature = new CryptoPrimitivesSignature("randomSignatureContents".getBytes(StandardCharsets.UTF_8));
		return new ControlComponenthlVCCPayload(encryptionGroup, nodeId, "E1A6F972679C8BD8A7C25C400A842EE7", confirmationKey,
				unsuccessfulConfirmationAttemptCount,
				signature);
	}

	private VotingServerConfirmPayload genRequestPayload() {
		final int unsuccessfulConfirmationAttemptCount = 0;
		final CryptoPrimitivesSignature signature = new CryptoPrimitivesSignature("".getBytes(StandardCharsets.UTF_8));
		return new VotingServerConfirmPayload(encryptionGroup, confirmationKey, unsuccessfulConfirmationAttemptCount, signature);
	}

	private ControlComponentlVCCSharePayload generateControlComponentlVCCSharePayload(final Integer nodeId) {
		final GqElement randomGqElement = new GqGroupGenerator(encryptionGroup).genMember();
		final LongVoteCastReturnCodesShare longVoteCastReturnCodesShare = new LongVoteCastReturnCodesShare(electionEventId,
				verificationCardSetId, verificationCardId, nodeId, randomGqElement);
		final ControlComponentlVCCSharePayload longReturnCodesSharePayload = new ControlComponentlVCCSharePayload(electionEventId,
				verificationCardSetId, verificationCardId, nodeId, encryptionGroup, longVoteCastReturnCodesShare, true, confirmationKey);
		final CryptoPrimitivesSignature signature = new CryptoPrimitivesSignature(new byte[] {});
		longReturnCodesSharePayload.setSignature(signature);
		return longReturnCodesSharePayload;
	}

	private ControlComponentlVCCSharePayload generateControlComponentlVCCSharePayloadNotVerified(final Integer nodeId) {
		final ControlComponentlVCCSharePayload longReturnCodesSharePayload = new ControlComponentlVCCSharePayload(encryptionGroup, electionEventId,
				verificationCardSetId, verificationCardId, nodeId, confirmationKey, false);
		final CryptoPrimitivesSignature signature = new CryptoPrimitivesSignature(new byte[] {});
		longReturnCodesSharePayload.setSignature(signature);
		return longReturnCodesSharePayload;
	}
}
