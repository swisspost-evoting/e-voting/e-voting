/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.process;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.google.common.base.Throwables;

import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base64Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.domain.configuration.SetupComponentVoterAuthenticationData;
import ch.post.it.evoting.evotinglibraries.domain.common.ContextIds;
import ch.post.it.evoting.evotinglibraries.domain.election.PrimesMappingTable;
import ch.post.it.evoting.evotinglibraries.domain.election.generators.PrimesMappingTableGenerator;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;
import ch.post.it.evoting.votingserver.process.voting.CredentialIdNotFoundException;

@DisplayName("IdentifierValidationService calling")
class IdentifierValidationServiceTest {

	private static final Random RANDOM = RandomFactory.createRandom();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();
	private static final Alphabet base64Alphabet = Base64Alphabet.getInstance();
	private static IdentifierValidationService identifierValidationService;
	private static VerificationCardRepository verificationCardRepository;
	private String electionEventId;
	private String verificationCardSetId;
	private String verificationCardId;
	private String credentialId;

	@BeforeAll
	static void setupAll() {
		verificationCardRepository = mock(VerificationCardRepository.class);
		final VerificationCardStateRepository verificationCardStateRepository = mock(VerificationCardStateRepository.class);
		final VerificationCardStateService verificationCardStateService = new VerificationCardStateService(verificationCardStateRepository);
		final VerificationCardService verificationCardService = new VerificationCardService(verificationCardRepository, verificationCardStateService,
				5);
		identifierValidationService = new IdentifierValidationService(verificationCardService);
	}

	@BeforeEach
	void setup() {
		reset(verificationCardRepository);
		electionEventId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		credentialId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		verificationCardSetId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		verificationCardId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
	}

	@Test
	@DisplayName("validateCredentialId with null arguments throws a NullPointerException")
	void validateCredentialIdWithNullArgumentsThrows() {
		assertThrows(NullPointerException.class, () -> identifierValidationService.validateCredentialId(null, credentialId));
		assertThrows(NullPointerException.class, () -> identifierValidationService.validateCredentialId(electionEventId, null));
	}

	@Test
	@DisplayName("validateCredentialId with non UUID arguments throws a FailedValidationException")
	void validateCredentialIdWithNonUuidArgumentsThrows() {
		final String nonUuid = "This is not a UUID";
		assertThrows(FailedValidationException.class, () -> identifierValidationService.validateCredentialId(nonUuid, credentialId));
		assertThrows(FailedValidationException.class, () -> identifierValidationService.validateCredentialId(electionEventId, nonUuid));
	}

	@Test
	@DisplayName("validateCredentialId when verification card not found for credential ID then throws a CredentialIdNotFoundException")
	void validateCredentialIdWhenVerificationCardNotFoundThrows() {
		when(verificationCardRepository.findByCredentialId(credentialId)).thenReturn(Optional.empty());
		final CredentialIdNotFoundException exception = assertThrows(CredentialIdNotFoundException.class,
				() -> identifierValidationService.validateCredentialId(electionEventId, credentialId));
		final String expectedMessage = String.format("No verification card found for given credentialId. [electionEventId: %s, credentialId: %s]",
				electionEventId,
				credentialId);
		assertEquals(expectedMessage, exception.getMessage());
	}

	@Test
	@DisplayName("validateCredentialId when verification card with different election event ID found for credential ID then throws an IllegalArgumentException")
	void validateCredentialIdWhenVerificationCardWithDifferentElectionEventIdFoundThrows() {
		final VerificationCardEntity verificationCardEntity = genVerificationCardEntity(credentialId);
		when(verificationCardRepository.findByCredentialId(credentialId)).thenReturn(Optional.of(verificationCardEntity));

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> identifierValidationService.validateCredentialId(electionEventId, credentialId));
		final String expectedMessage = String.format("Election event and credential id are not consistent. [electionEventId: %s, credentialId: %s",
				electionEventId, credentialId);
		assertEquals(expectedMessage, exception.getMessage());
	}

	@Test
	@DisplayName("validateCredentialId when election event ID and credential ID are consistent then does not throw")
	void validateCredentialIdWhenElectionEventIdAndCredentialIdConsistentDoesNotThrow() {
		final VerificationCardEntity verificationCardEntity = genVerificationCardEntity(electionEventId, verificationCardSetId, credentialId);
		when(verificationCardRepository.findByCredentialId(credentialId)).thenReturn(Optional.of(verificationCardEntity));

		assertDoesNotThrow(() -> identifierValidationService.validateCredentialId(electionEventId, credentialId));
	}

	@Test
	@DisplayName("validateContextIdsAndCredentialId with null arguments throws a NullPointerException")
	void validateContextIdsAndCredentialIdWithNullArgumentsThrows() {
		assertThrows(NullPointerException.class, () -> identifierValidationService.validateContextIdsAndCredentialId(null, credentialId));
		final ContextIds contextIds = new ContextIds(electionEventId, verificationCardSetId, verificationCardId);
		assertThrows(NullPointerException.class, () -> identifierValidationService.validateContextIdsAndCredentialId(contextIds, null));
	}

	@Test
	@DisplayName("validateContextIdsAndCredentialId with non UUID arguments throws a FailedValidationException")
	void validateContextIdsAndCredentialIdWithNonUuidArgumentsThrows() {
		final String nonUuid = "This is not a UUID";
		final ContextIds contextIds = new ContextIds(electionEventId, verificationCardSetId, verificationCardId);
		assertThrows(FailedValidationException.class, () -> identifierValidationService.validateContextIdsAndCredentialId(contextIds, nonUuid));
	}

	@Test
	@DisplayName("validateContextIdsAndCredentialId with inconsistent election event id and verification card set id throws an IllegalArgumentException")
	void validateContextIdsAndCredentialIdWithInconsistentElectionEventIdAndVerificationCardSetIdThrows() {
		final String differentElectionEventId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		final VerificationCardEntity verificationCardEntity = genVerificationCardEntity(differentElectionEventId, verificationCardSetId,
				credentialId);
		doReturn(Optional.of(verificationCardEntity)).when(verificationCardRepository).findById(verificationCardId);
		final ContextIds contextIds = new ContextIds(electionEventId, verificationCardSetId, verificationCardId);
		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> identifierValidationService.validateContextIdsAndCredentialId(contextIds, credentialId));
		final String expectedErrorMessage = String.format(
				"Verification card set and election event are not consistent. [verificationCardSetId: %s, electionEventId: %s]",
				verificationCardSetId, electionEventId);
		assertEquals(expectedErrorMessage, Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("validateContextIdsAndCredentialId with inconsistent verification card set id and verification card id throws an IllegalArgumentException")
	void validateContextIdsAndCredentialIdWithInconsistentVerificationCardSetIdAndVerificationCardIdThrows() {
		final String differentVerificationCardSetId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		final VerificationCardEntity verificationCardEntity = genVerificationCardEntity(electionEventId, differentVerificationCardSetId,
				credentialId);
		doReturn(Optional.of(verificationCardEntity)).when(verificationCardRepository).findById(verificationCardId);
		final ContextIds contextIds = new ContextIds(electionEventId, verificationCardSetId, verificationCardId);
		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> identifierValidationService.validateContextIdsAndCredentialId(contextIds, credentialId));
		final String expectedErrorMessage = String.format(
				"Verification card and verification card set are not consistent. [verificationCardId: %s, verificationCardSetId: %s]",
				verificationCardId, verificationCardSetId);
		assertEquals(expectedErrorMessage, Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("validateContextIdsAndCredentialId with inconsistent credential id throws an IllegalArgumentException")
	void validateContextIdsAndCredentialIdWithInconsistentCredentialIdThrows() {
		final String differentCredentialId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		final VerificationCardEntity verificationCardEntity = genVerificationCardEntity(electionEventId, verificationCardSetId,
				differentCredentialId);
		doReturn(Optional.of(verificationCardEntity)).when(verificationCardRepository).findById(verificationCardId);
		final ContextIds contextIds = new ContextIds(electionEventId, verificationCardSetId, verificationCardId);
		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> identifierValidationService.validateContextIdsAndCredentialId(contextIds, credentialId));
		final String expectedErrorMessage = String.format(
				"Verification card id and credential id are not consistent. [verificationCardId: %s, credentialId: %s]", verificationCardId,
				credentialId);
		assertEquals(expectedErrorMessage, Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("validateContextIds with null argument throws a NullPointerException")
	void validateContextIdsWithNullArgumentThrows() {
		assertThrows(NullPointerException.class, () -> identifierValidationService.validateContextIds(null));
	}

	@Test
	@DisplayName("validateContextIds with inconsistent election event id and verification card set id throws an IllegalArgumentException")
	void validateContextIdsWithInconsistentElectionEventIdAndVerificationCardSetIdThrows() {
		final String differentElectionEventId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		final VerificationCardEntity verificationCardEntity = genVerificationCardEntity(differentElectionEventId, verificationCardSetId,
				credentialId);
		doReturn(Optional.of(verificationCardEntity)).when(verificationCardRepository).findById(verificationCardId);
		final ContextIds contextIds = new ContextIds(electionEventId, verificationCardSetId, verificationCardId);
		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> identifierValidationService.validateContextIds(contextIds));
		final String expectedErrorMessage = String.format(
				"Verification card set and election event are not consistent. [verificationCardSetId: %s, electionEventId: %s]",
				verificationCardSetId, electionEventId);
		assertEquals(expectedErrorMessage, Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("validateContextIds with inconsistent verification card set id and verification card id throws an IllegalArgumentException")
	void validateContextIdsWithInconsistentVerificationCardSetIdAndVerificationCardIdThrows() {
		final String differentVerificationCardSetId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		final VerificationCardEntity verificationCardEntity = genVerificationCardEntity(electionEventId, differentVerificationCardSetId,
				credentialId);
		doReturn(Optional.of(verificationCardEntity)).when(verificationCardRepository).findById(verificationCardId);
		final ContextIds contextIds = new ContextIds(electionEventId, verificationCardSetId, verificationCardId);
		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> identifierValidationService.validateContextIds(contextIds));
		final String expectedErrorMessage = String.format(
				"Verification card and verification card set are not consistent. [verificationCardId: %s, verificationCardSetId: %s]",
				verificationCardId, verificationCardSetId);
		assertEquals(expectedErrorMessage, Throwables.getRootCause(exception).getMessage());
	}

	private VerificationCardEntity genVerificationCardEntity(final String electionEventId, final String verificationCardSetId,
			final String credentialId) {
		final String verificationCardId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		final String votingCardSetId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		final String ballotBoxId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		final String ballotId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		final String votingCardId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		final PrimesMappingTableGenerator primesMappingTableGenerator = new PrimesMappingTableGenerator();
		final PrimesMappingTable primesMappingTable = primesMappingTableGenerator.generate(2);
		final GqGroup encryptionGroup = primesMappingTable.getEncryptionGroup();
		final ElectionEventEntity electionEventEntity = new ElectionEventEntity(electionEventId, encryptionGroup);
		final LocalDateTime ballotBoxStartTime = LocalDateTime.now().minusDays(1);
		final LocalDateTime ballotBoxFinishTime = LocalDateTime.now().plusDays(1);

		final VerificationCardSetEntity verificationCardSetEntity = new VerificationCardSetEntity.Builder()
				.setVerificationCardSetId(verificationCardSetId)
				.setElectionEventEntity(electionEventEntity)
				.setBallotBoxId(ballotBoxId)
				.setGracePeriod(900)
				.setPrimesMappingTable(primesMappingTable)
				.setBallotBoxStartTime(ballotBoxStartTime)
				.setBallotBoxFinishTime(ballotBoxFinishTime)
				.setTestBallotBox(true)
				.build();

		final SetupComponentVoterAuthenticationData voterAuthenticationData = new SetupComponentVoterAuthenticationData(electionEventId,
				verificationCardSetId, votingCardSetId, ballotBoxId, ballotId, verificationCardId, votingCardId, credentialId,
				RANDOM.genRandomString(44, base64Alphabet));
		final VerificationCardStateEntity verificationCardStateEntity = new VerificationCardStateEntity(verificationCardId);
		return new VerificationCardEntity(verificationCardId, verificationCardSetEntity, credentialId,
				votingCardId, voterAuthenticationData, verificationCardStateEntity);
	}

	private VerificationCardEntity genVerificationCardEntity(final String credentialId) {
		final String electionEventId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		final String verificationCardSetId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		return genVerificationCardEntity(electionEventId, verificationCardSetId, credentialId);
	}
}
