/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.process.configuration.setupvoting;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Base64;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.scheduling.concurrent.CustomizableThreadFactory;
import org.springframework.test.web.reactive.server.WebTestClient;

import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.domain.configuration.setupvoting.LongVoteCastReturnCodesAllowListResponsePayload;
import ch.post.it.evoting.domain.configuration.setupvoting.SetupComponentLVCCAllowListPayload;
import ch.post.it.evoting.evotinglibraries.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.votingserver.ArtemisSupport;
import ch.post.it.evoting.votingserver.BroadcastIntegrationTestService;

@DisplayName("A LongVoteCastReturnCodesAllowListController")
class LongVoteCastReturnCodesAllowListControllerIT extends ArtemisSupport {

	private static final Random RANDOM = RandomFactory.createRandom();

	@Autowired
	private BroadcastIntegrationTestService broadcastIntegrationTestService;

	@Autowired
	private WebTestClient webTestClient;

	private String electionEventId;
	private String verificationCardSetId;

	@BeforeEach
	void setUp() {
		electionEventId = RANDOM.genRandomString(ID_LENGTH, Base16Alphabet.getInstance());
		verificationCardSetId = RANDOM.genRandomString(ID_LENGTH, Base16Alphabet.getInstance());
	}

	@Test
	@DisplayName("processing a request behaves as expected.")
	void process() throws InterruptedException {
		final CountDownLatch webClientCountDownLatch = new CountDownLatch(1);
		final SetupComponentLVCCAllowListPayload payload = getLongVoteCastReturnCodesAllowListPayload();
		final ExecutorService executorService = Executors.newFixedThreadPool(1, new CustomizableThreadFactory("http-pool-"));

		//send request to controller
		executorService.execute(() -> {
			webTestClient
					.post()
					.uri(uriBuilder -> uriBuilder
							.path("/api/v1/configuration/setupvoting/longvotecastreturncodesallowlist/electionevent/")
							.pathSegment(electionEventId, "verificationcardset", verificationCardSetId)
							.build(1L))
					.accept(MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML)
					.bodyValue(payload)
					.exchange()
					.expectStatus().isOk();
			webClientCountDownLatch.countDown();
		});

		broadcastIntegrationTestService.awaitBroadcastRequestsSaved(30, SECONDS);

		broadcastIntegrationTestService.respondWith(
				nodeId -> new LongVoteCastReturnCodesAllowListResponsePayload(nodeId, electionEventId, verificationCardSetId));

		assertTrue(webClientCountDownLatch.await(30, SECONDS));
	}

	private SetupComponentLVCCAllowListPayload getLongVoteCastReturnCodesAllowListPayload() {
		final String lVCC1 = Base64.getEncoder().encodeToString(new byte[] { 35 });
		final String lVCC2 = Base64.getEncoder().encodeToString(new byte[] { 45 });
		return new SetupComponentLVCCAllowListPayload(
				electionEventId,
				verificationCardSetId,
				Arrays.asList(lVCC1, lVCC2),
				new CryptoPrimitivesSignature("".getBytes(StandardCharsets.UTF_8)));
	}
}
