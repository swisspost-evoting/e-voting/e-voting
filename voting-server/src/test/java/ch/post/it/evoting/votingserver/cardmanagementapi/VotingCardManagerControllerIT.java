/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.cardmanagementapi;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.reactive.server.WebTestClient;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.votingserver.process.VerificationCardService;
import ch.post.it.evoting.votingserver.process.VerificationCardState;
import ch.post.it.evoting.votingserver.process.VotingCardDto;

@WebFluxTest(controllers = VotingCardManagerController.class)
class VotingCardManagerControllerIT {

	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();
	private static ObjectMapper objectMapper;

	@Autowired
	private WebTestClient webTestClient;

	@MockBean
	private VerificationCardService verificationCardService;

	@BeforeAll
	static void setUp() {
		objectMapper = new ObjectMapper().registerModule(new JavaTimeModule());
	}

	@Test
	void searchHappyPathRest() throws Exception {
		// given
		final String votingCardId = RandomFactory.createRandom().genRandomString(ID_LENGTH, base16Alphabet);
		final String votingCardId2 = RandomFactory.createRandom().genRandomString(ID_LENGTH, base16Alphabet);
		final String votingCardId3 = RandomFactory.createRandom().genRandomString(ID_LENGTH, base16Alphabet);
		final String votingCardId4 = RandomFactory.createRandom().genRandomString(ID_LENGTH, base16Alphabet);
		final String votingCardId5 = RandomFactory.createRandom().genRandomString(ID_LENGTH, base16Alphabet);
		final String votingCardId6 = RandomFactory.createRandom().genRandomString(ID_LENGTH, base16Alphabet);
		final String votingCardId7 = RandomFactory.createRandom().genRandomString(ID_LENGTH, base16Alphabet);
		final String votingCardId8 = RandomFactory.createRandom().genRandomString(ID_LENGTH, base16Alphabet);
		final String votingCardId9 = RandomFactory.createRandom().genRandomString(ID_LENGTH, base16Alphabet);
		final String votingCardId10 = RandomFactory.createRandom().genRandomString(ID_LENGTH, base16Alphabet);

		final String targetUrl = String.format("/api/v1/voting-card-manager/voting-cards/%s", votingCardId);

		final List<VotingCardDto> expectedVotingCards = new ArrayList<>();
		expectedVotingCards.add(new VotingCardDto(votingCardId, VerificationCardState.CONFIRMED, LocalDateTime.now()));
		expectedVotingCards.add(new VotingCardDto(votingCardId2, VerificationCardState.CONFIRMED, LocalDateTime.now()));
		expectedVotingCards.add(new VotingCardDto(votingCardId3, VerificationCardState.CONFIRMED, LocalDateTime.now()));
		expectedVotingCards.add(new VotingCardDto(votingCardId4, VerificationCardState.CONFIRMED, LocalDateTime.now()));
		expectedVotingCards.add(new VotingCardDto(votingCardId5, VerificationCardState.CONFIRMED, LocalDateTime.now()));
		expectedVotingCards.add(new VotingCardDto(votingCardId6, VerificationCardState.CONFIRMED, LocalDateTime.now()));
		expectedVotingCards.add(new VotingCardDto(votingCardId7, VerificationCardState.CONFIRMED, LocalDateTime.now()));
		expectedVotingCards.add(new VotingCardDto(votingCardId8, VerificationCardState.CONFIRMED, LocalDateTime.now()));
		expectedVotingCards.add(new VotingCardDto(votingCardId9, VerificationCardState.CONFIRMED, LocalDateTime.now()));
		expectedVotingCards.add(new VotingCardDto(votingCardId10, VerificationCardState.CONFIRMED, LocalDateTime.now()));

		when(verificationCardService.searchVotingCards(votingCardId)).thenReturn(expectedVotingCards);

		// when
		final String jsonResult = webTestClient.get().uri(targetUrl)
				.exchange()
				.expectStatus().isOk()
				.returnResult(String.class)
				.getResponseBody()
				.blockFirst();

		final List<VotingCardDto> actualVotingCards = objectMapper.readValue(jsonResult, new TypeReference<>() {
		});

		// then
		assertEquals(expectedVotingCards,actualVotingCards);
	}
}
