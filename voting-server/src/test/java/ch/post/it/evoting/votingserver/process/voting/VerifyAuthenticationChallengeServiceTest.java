/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.process.voting;

import static ch.post.it.evoting.cryptoprimitives.utils.ByteArrays.cutToBitLength;
import static ch.post.it.evoting.cryptoprimitives.utils.Conversions.integerToByteArray;
import static ch.post.it.evoting.cryptoprimitives.utils.Conversions.stringToByteArray;
import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static ch.post.it.evoting.votingserver.process.Constants.TWO_POW_256;
import static com.google.common.primitives.Bytes.concat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.math.BigInteger;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import ch.post.it.evoting.cryptoprimitives.hashing.Argon2;
import ch.post.it.evoting.cryptoprimitives.hashing.Argon2Factory;
import ch.post.it.evoting.cryptoprimitives.hashing.Argon2Profile;
import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.hashing.HashFactory;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableBigInteger;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableString;
import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base64;
import ch.post.it.evoting.cryptoprimitives.math.Base64Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.BaseEncodingFactory;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.domain.configuration.SetupComponentVoterAuthenticationData;
import ch.post.it.evoting.evotinglibraries.domain.election.PrimesMappingTable;
import ch.post.it.evoting.evotinglibraries.domain.election.generators.PrimesMappingTableGenerator;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;
import ch.post.it.evoting.votingserver.process.ElectionEventEntity;
import ch.post.it.evoting.votingserver.process.ElectionEventRepository;
import ch.post.it.evoting.votingserver.process.ElectionEventService;
import ch.post.it.evoting.votingserver.process.VerificationCardEntity;
import ch.post.it.evoting.votingserver.process.VerificationCardRepository;
import ch.post.it.evoting.votingserver.process.VerificationCardService;
import ch.post.it.evoting.votingserver.process.VerificationCardSetEntity;
import ch.post.it.evoting.votingserver.process.VerificationCardSetRepository;
import ch.post.it.evoting.votingserver.process.VerificationCardSetService;
import ch.post.it.evoting.votingserver.process.VerificationCardStateEntity;
import ch.post.it.evoting.votingserver.process.VerificationCardStateRepository;
import ch.post.it.evoting.votingserver.process.VerificationCardStateService;
import ch.post.it.evoting.votingserver.process.configuration.setupvoting.VoterAuthenticationDataService;
import ch.post.it.evoting.votingserver.protocol.voting.authenticatevoter.VerifyAuthenticationChallengeAlgorithm;
import ch.post.it.evoting.votingserver.protocol.voting.authenticatevoter.VerifyAuthenticationChallengeOutput;
import ch.post.it.evoting.votingserver.protocol.voting.authenticatevoter.VerifyAuthenticationChallengeService;

@DisplayName("VerifyAuthenticationChallengeService calling")
class VerifyAuthenticationChallengeServiceTest {

	private static final Random RANDOM = RandomFactory.createRandom();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();
	private static final Alphabet base64Alphabet = Base64Alphabet.getInstance();
	private static Hash hash;
	private static Argon2 argon2;
	private static Base64 base64;
	private static VerifyAuthenticationChallengeService verifyAuthenticationChallengeService;
	private static VerificationCardRepository verificationCardRepository;
	private static VerificationCardStateRepository verificationCardStateRepository;
	private String electionEventId;
	private AuthenticationStep authenticationStep;
	private AuthenticationChallenge authenticationChallenge;

	@BeforeAll
	static void setupAll() {
		verificationCardRepository = mock(VerificationCardRepository.class);
		verificationCardStateRepository = mock(VerificationCardStateRepository.class);
		final VerificationCardStateService verificationCardStateService = new VerificationCardStateService(verificationCardStateRepository);
		final VerificationCardService verificationCardService = new VerificationCardService(verificationCardRepository, verificationCardStateService,
				5);

		final ElectionEventRepository electionEventRepository = mock(ElectionEventRepository.class);
		final ElectionEventService electionEventService = new ElectionEventService(electionEventRepository);
		final VerificationCardSetRepository verificationCardSetRepository = mock(VerificationCardSetRepository.class);
		final VerificationCardSetService verificationCardSetService = new VerificationCardSetService(electionEventService,
				verificationCardSetRepository);
		final VoterAuthenticationDataService voterAuthenticationDataService = new VoterAuthenticationDataService(verificationCardService,
				verificationCardSetService);
		hash = HashFactory.createHash();
		argon2 = Argon2Factory.createArgon2(Argon2Profile.TEST);
		base64 = BaseEncodingFactory.createBase64();
		final VerifyAuthenticationChallengeAlgorithm verifyAuthenticationChallengeAlgorithm = new VerifyAuthenticationChallengeAlgorithm(hash, argon2,
				base64, verificationCardService);
		verifyAuthenticationChallengeService = new VerifyAuthenticationChallengeService(verificationCardService, voterAuthenticationDataService,
				verifyAuthenticationChallengeAlgorithm);
	}

	@BeforeEach
	void setup() {
		Mockito.reset(verificationCardRepository, verificationCardStateRepository);
		electionEventId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		authenticationStep = Arrays.stream(AuthenticationStep.values()).findAny().orElseThrow();
		final String baseAuthenticationChallenge = RANDOM.genRandomString(44, base64Alphabet);
		authenticationChallenge = getAuthenticationChallenge(electionEventId, authenticationStep, baseAuthenticationChallenge);

		final VerificationCardEntity verificationCardEntity = genVerificationCardEntity(baseAuthenticationChallenge);
		when(verificationCardRepository.findByCredentialId(authenticationChallenge.derivedVoterIdentifier())).thenReturn(
				Optional.of(verificationCardEntity));
		when(verificationCardStateRepository.findById(verificationCardEntity.getVerificationCardId())).thenReturn(
				Optional.of(verificationCardEntity.getVerificationCardStateEntity()));
	}

	@Test
	@DisplayName("verifyAuthenticationChallenge with null arguments throws a NullPointerException")
	void verifyAuthenticationChallengeWithNullArgumentsThrows() {
		assertThrows(NullPointerException.class,
				() -> verifyAuthenticationChallengeService.verifyAuthenticationChallenge(null, authenticationStep, authenticationChallenge));
		assertThrows(NullPointerException.class,
				() -> verifyAuthenticationChallengeService.verifyAuthenticationChallenge(electionEventId, null, authenticationChallenge));
		assertThrows(NullPointerException.class,
				() -> verifyAuthenticationChallengeService.verifyAuthenticationChallenge(electionEventId, authenticationStep, null));
	}

	@Test
	@DisplayName("verifyAuthenticationChallenge with non UUID electionEventId throws a FailedValidationException")
	void verifyAuthenticationChallengeWithInvalidElectionEventIdThrows() {
		assertThrows(FailedValidationException.class,
				() -> verifyAuthenticationChallengeService.verifyAuthenticationChallenge("invalidId", authenticationStep, authenticationChallenge));
	}

	@Test
	@DisplayName("verifyAuthenticationChallenge with valid authentication challenge does not throw")
	void verifyAuthenticationChallengeWithValidAuthenticationChallengeDoesNotThrow() {
		assertDoesNotThrow(() -> verifyAuthenticationChallengeService.verifyAuthenticationChallenge(electionEventId, authenticationStep,
				authenticationChallenge));
	}

	@Test
	@DisplayName("verifyAuthenticationChallenge with invalid authentication challenge throws a VerifyAuthenticationChallengeException")
	void verifyAuthenticationChallengeWithInvalidAuthenticationChallengeThrows() {
		final BigInteger differentAuthenticationNonce = RANDOM.genRandomInteger(TWO_POW_256);
		final AuthenticationChallenge badAuthenticationChallenge = new AuthenticationChallenge(authenticationChallenge.derivedVoterIdentifier(),
				authenticationChallenge.derivedAuthenticationChallenge(), differentAuthenticationNonce);
		final VerifyAuthenticationChallengeException exception = assertThrows(VerifyAuthenticationChallengeException.class,
				() -> verifyAuthenticationChallengeService.verifyAuthenticationChallenge(electionEventId, authenticationStep,
						badAuthenticationChallenge));
		assertEquals(VerifyAuthenticationChallengeOutput.VerifyAuthenticationChallengeStatus.EXTENDED_FACTOR_INVALID, exception.getErrorStatus());
		assertEquals(4, exception.getRemainingAttempts().orElseThrow());
	}

	private VerificationCardEntity genVerificationCardEntity(final String baseAuthenticationChallenge) {
		final String verificationCardId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		final String verificationCardSetId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		final String votingCardSetId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		final String ballotBoxId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		final String ballotId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		final String credentialId = authenticationChallenge.derivedVoterIdentifier();
		final String votingCardId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		final PrimesMappingTable primesMappingTable = new PrimesMappingTableGenerator().generate(2);
		final GqGroup encryptionGroup = primesMappingTable.getEncryptionGroup();
		final ElectionEventEntity electionEventEntity = new ElectionEventEntity(electionEventId, encryptionGroup);
		final LocalDateTime ballotBoxStartTime = LocalDateTime.now().minusDays(1);
		final LocalDateTime ballotBoxFinishTime = LocalDateTime.now().plusDays(1);

		final VerificationCardSetEntity verificationCardSetEntity = new VerificationCardSetEntity.Builder()
				.setVerificationCardSetId(verificationCardSetId)
				.setElectionEventEntity(electionEventEntity)
				.setBallotBoxId(ballotBoxId)
				.setGracePeriod(900)
				.setPrimesMappingTable(primesMappingTable)
				.setBallotBoxStartTime(ballotBoxStartTime)
				.setBallotBoxFinishTime(ballotBoxFinishTime)
				.setTestBallotBox(true)
				.build();

		final SetupComponentVoterAuthenticationData voterAuthenticationData = new SetupComponentVoterAuthenticationData(electionEventId,
				verificationCardSetId, votingCardSetId, ballotBoxId, ballotId, verificationCardId, votingCardId, credentialId,
				baseAuthenticationChallenge);
		final VerificationCardStateEntity verificationCardStateEntity = new VerificationCardStateEntity(verificationCardId);
		return new VerificationCardEntity(verificationCardId, verificationCardSetEntity, credentialId,
				votingCardId, voterAuthenticationData, verificationCardStateEntity);
	}

	private AuthenticationChallenge getAuthenticationChallenge(final String electionEventId, final AuthenticationStep authenticationStep,
			final String baseAuthenticationChallenge) {
		final String derivedVoterIdentifier = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		final BigInteger authenticationNonce = RANDOM.genRandomInteger(TWO_POW_256);
		final byte[] salt = cutToBitLength(
				hash.recursiveHash(HashableString.from(electionEventId), HashableString.from(derivedVoterIdentifier), HashableString.from("dAuth"),
						HashableString.from(authenticationStep.getName()), HashableBigInteger.from(authenticationNonce)), 128);
		final BigInteger timeStep = BigInteger.valueOf(getTimeStep());
		final byte[] k = concat(stringToByteArray(baseAuthenticationChallenge), stringToByteArray("Auth"), integerToByteArray(timeStep));
		final byte[] authenticationBytes = argon2.getArgon2id(k, salt);
		final String derivedAuthenticationChallenge = base64.base64Encode(authenticationBytes);

		return new AuthenticationChallenge(derivedVoterIdentifier, derivedAuthenticationChallenge, authenticationNonce);
	}

	private long getTimeStep() {
		return Instant.now().getEpochSecond() / 300;
	}
}
