/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.process.voting.confirmvote;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.security.SignatureException;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.hashing.HashFactory;
import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base64Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.cryptoprimitives.symmetric.SymmetricFactory;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.GqGroupGenerator;
import ch.post.it.evoting.cryptoprimitives.utils.KeyDerivationFactory;
import ch.post.it.evoting.domain.InvalidPayloadSignatureException;
import ch.post.it.evoting.domain.configuration.SetupComponentVoterAuthenticationData;
import ch.post.it.evoting.domain.voting.confirmvote.ConfirmationKey;
import ch.post.it.evoting.domain.voting.confirmvote.ControlComponenthlVCCPayload;
import ch.post.it.evoting.domain.voting.confirmvote.ControlComponentlVCCSharePayload;
import ch.post.it.evoting.domain.voting.confirmvote.LongVoteCastReturnCodesShare;
import ch.post.it.evoting.evotinglibraries.domain.common.ContextIds;
import ch.post.it.evoting.evotinglibraries.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.evotinglibraries.domain.signature.Alias;
import ch.post.it.evoting.evotinglibraries.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.votingserver.messaging.InProgressMessage;
import ch.post.it.evoting.votingserver.messaging.InProgressMessageService;
import ch.post.it.evoting.votingserver.messaging.MessageHandler;
import ch.post.it.evoting.votingserver.messaging.ResponseCompletionService;
import ch.post.it.evoting.votingserver.messaging.Serializer;
import ch.post.it.evoting.votingserver.process.ElectionEventEntity;
import ch.post.it.evoting.votingserver.process.ElectionEventRepository;
import ch.post.it.evoting.votingserver.process.ElectionEventService;
import ch.post.it.evoting.votingserver.process.IdentifierValidationService;
import ch.post.it.evoting.votingserver.process.ReturnCodesMappingTableRepository;
import ch.post.it.evoting.votingserver.process.ReturnCodesMappingTableService;
import ch.post.it.evoting.votingserver.process.VerificationCardEntity;
import ch.post.it.evoting.votingserver.process.VerificationCardRepository;
import ch.post.it.evoting.votingserver.process.VerificationCardService;
import ch.post.it.evoting.votingserver.process.VerificationCardSetEntity;
import ch.post.it.evoting.votingserver.process.VerificationCardSetRepository;
import ch.post.it.evoting.votingserver.process.VerificationCardSetService;
import ch.post.it.evoting.votingserver.process.VerificationCardStateEntity;
import ch.post.it.evoting.votingserver.process.VerificationCardStateRepository;
import ch.post.it.evoting.votingserver.process.VerificationCardStateService;
import ch.post.it.evoting.votingserver.process.voting.ConfirmationKeyInvalidException;
import ch.post.it.evoting.votingserver.process.voting.ReturnCodesMappingTableSupplier;
import ch.post.it.evoting.votingserver.protocol.voting.confirmvote.ExtractVCCAlgorithm;
import ch.post.it.evoting.votingserver.protocol.voting.confirmvote.ExtractVCCService;
import ch.post.it.evoting.votingserver.shelf.WorkflowShelfService;

class VoteCastReturnCodeServiceTest {

	private static final Random RANDOM = RandomFactory.createRandom();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();
	private static final Alphabet base64Alphabet = Base64Alphabet.getInstance();
	private static final Serializer serializer = new Serializer(DomainObjectMapper.getNewInstance());
	private static final String FIRST_CORRELATION_ID = "1234567788990";
	private static final String SECOND_CORRELATION_ID = "9876543210";
	private static VoteCastReturnCodeService voteCastReturnCodeService;
	private static SignatureKeystore<Alias> signatureKeystoreService;
	private static ElectionEventRepository electionEventRepository;
	private static VerificationCardRepository verificationCardRepository;
	private static VerificationCardService verificationCardService;
	private static WorkflowShelfService workflowShelfService;
	private static ReturnCodesMappingTableRepository returnCodesMappingTableRepository;
	private static MessageHandler messageHandler;
	private static InProgressMessageService inProgressMessageService;
	private static ResponseCompletionService responseCompletionService;

	private static VerificationCardStateService verificationCardStateService;
	private ContextIds contextIds;
	private GqElement confirmationKey;

	@BeforeAll
	static void setupAll() {
		electionEventRepository = mock(ElectionEventRepository.class);
		final ElectionEventService electionEventService = new ElectionEventService(electionEventRepository);
		verificationCardRepository = mock(VerificationCardRepository.class);
		final VerificationCardStateRepository verificationCardStateRepository = mock(VerificationCardStateRepository.class);
		verificationCardStateService = spy(new VerificationCardStateService(verificationCardStateRepository));
		verificationCardService = new VerificationCardService(verificationCardRepository, verificationCardStateService, 5);
		signatureKeystoreService = mock(SignatureKeystore.class);
		final VerificationCardSetRepository verificationCardSetRepository = mock(VerificationCardSetRepository.class);
		final VerificationCardSetService verificationCardSetService = new VerificationCardSetService(electionEventService,
				verificationCardSetRepository);

		returnCodesMappingTableRepository = mock(ReturnCodesMappingTableRepository.class);
		final ReturnCodesMappingTableService returnCodesMappingTableService = new ReturnCodesMappingTableService(verificationCardSetService,
				returnCodesMappingTableRepository, 10);
		final ReturnCodesMappingTableSupplier returnCodesMappingTableSupplier = new ReturnCodesMappingTableSupplier(returnCodesMappingTableService);
		final IdentifierValidationService identifierValidationService = mock(IdentifierValidationService.class);
		doNothing().when(identifierValidationService).validateContextIds(any());
		final ExtractVCCAlgorithm extractVCCAlgorithm = new ExtractVCCAlgorithm(HashFactory.createHash(), SymmetricFactory.createSymmetric(),
				KeyDerivationFactory.createKeyDerivation());
		final ExtractVCCService extractVCCService = new ExtractVCCService(extractVCCAlgorithm, electionEventService,
				identifierValidationService, returnCodesMappingTableSupplier);

		messageHandler = mock(MessageHandler.class);

		inProgressMessageService = mock(InProgressMessageService.class);
		workflowShelfService = mock(WorkflowShelfService.class);
		responseCompletionService = mock(ResponseCompletionService.class);

		voteCastReturnCodeService = new VoteCastReturnCodeService(serializer, messageHandler, extractVCCService, electionEventService,
				workflowShelfService, verificationCardService, signatureKeystoreService, responseCompletionService);
	}

	@BeforeEach
	void setup() throws SignatureException {
		reset(electionEventRepository, verificationCardRepository, signatureKeystoreService, returnCodesMappingTableRepository);
		final String electionEventId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		final String verificationCardSetId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		final String verificationCardId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		contextIds = new ContextIds(electionEventId, verificationCardSetId, verificationCardId);

		final GqGroup encryptionGroup = GroupTestData.getGqGroup();
		final GqGroupGenerator gqGroupGenerator = new GqGroupGenerator(encryptionGroup);
		confirmationKey = gqGroupGenerator.genNonIdentityMember();

		final ElectionEventEntity electionEventEntity = new ElectionEventEntity(electionEventId, encryptionGroup);
		doReturn(Optional.of(electionEventEntity)).when(electionEventRepository).findById(electionEventId);

		final VerificationCardStateEntity verificationCardStateEntity = new VerificationCardStateEntity(verificationCardId);

		final String votingCardSetId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		final String ballotBoxId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		final String ballotId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		final String credentialId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		final String votingCardId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		final String baseAuthenticationChallenge = RANDOM.genRandomString(44, base64Alphabet);
		final VerificationCardSetEntity verificationCardSetEntity = new VerificationCardSetEntity();
		final SetupComponentVoterAuthenticationData voterAuthenticationData = new SetupComponentVoterAuthenticationData(electionEventId,
				verificationCardSetId, votingCardSetId, ballotBoxId, ballotId, verificationCardId, votingCardId, credentialId,
				baseAuthenticationChallenge);
		final VerificationCardEntity verificationCardEntity = new VerificationCardEntity(verificationCardId, verificationCardSetEntity, credentialId,
				votingCardId, voterAuthenticationData, verificationCardStateEntity);
		doReturn(Optional.of(verificationCardEntity)).when(verificationCardRepository).findById(verificationCardId);

		when(signatureKeystoreService.generateSignature(any(), any())).thenReturn(new byte[] { 1, 2, 3 });

		when(messageHandler.generateCorrelationId()).thenReturn(FIRST_CORRELATION_ID).thenReturn(SECOND_CORRELATION_ID);

		final ControlComponenthlVCCPayload controlComponenthlVCCPayload1 = getControlComponenthlVCCPayload(1, encryptionGroup);
		final ControlComponenthlVCCPayload controlComponenthlVCCPayload2 = getControlComponenthlVCCPayload(2, encryptionGroup);
		final ControlComponenthlVCCPayload controlComponenthlVCCPayload3 = getControlComponenthlVCCPayload(3, encryptionGroup);
		final ControlComponenthlVCCPayload controlComponenthlVCCPayload4 = getControlComponenthlVCCPayload(4, encryptionGroup);

		when(messageHandler.sendMessage(any(), eq(FIRST_CORRELATION_ID))).then(onCall -> {
			voteCastReturnCodeService.onControlComponenthlVCCPayload(FIRST_CORRELATION_ID,
					List.of(controlComponenthlVCCPayload1,
							controlComponenthlVCCPayload2,
							controlComponenthlVCCPayload3,
							controlComponenthlVCCPayload4));
			return true;
		});

		when(workflowShelfService.pullFromShelf(any(), any())).thenReturn(
				new VoteCastReturnCodeService.ShelfElement(FIRST_CORRELATION_ID, contextIds, encryptionGroup));

		when(inProgressMessageService.getAllNodesResponses(FIRST_CORRELATION_ID)).thenReturn(
				getControlComponenthlVCCInProgressMessages(FIRST_CORRELATION_ID, controlComponenthlVCCPayload1, controlComponenthlVCCPayload2,
						controlComponenthlVCCPayload3,
						controlComponenthlVCCPayload4));
	}

	@Test
	@DisplayName("with null arguments throws a NullPointerException")
	void retrieveShortVoteCastCodeWithNullArgumentsThrows() {
		assertThrows(NullPointerException.class, () -> voteCastReturnCodeService.retrieveShortVoteCastCode(null, confirmationKey));
		assertThrows(NullPointerException.class, () -> voteCastReturnCodeService.retrieveShortVoteCastCode(contextIds, null));
	}

	@Test
	@DisplayName("with confirmationKey from different group than encryptionGroup throws an IllegalArgumentException")
	void retrieveShortVoteCastCodeWithConfirmationKeyFromBadGroupThrows() {
		final GqGroup differentGqGroup = GroupTestData.getDifferentGqGroup(confirmationKey.getGroup());
		final GqElement badConfirmationKey = new GqGroupGenerator(differentGqGroup).genNonIdentityMember();

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> voteCastReturnCodeService.retrieveShortVoteCastCode(contextIds, badConfirmationKey));
		final String expectedErrorMessage = String.format("The encryption group does not match the confirmation key's group. [contextIds: %s]",
				contextIds);
		assertEquals(expectedErrorMessage, exception.getMessage());
	}

	@Test
	@DisplayName("when retrieving ControlComponentlVCCSharePayloads with a null signature throws an IllegalStateException")
	void retrieveShortVoteCastCodeWhenRetrievingControlComponentlVCCSharePayloadWithNullSignatureThrows() throws SignatureException {
		final GqGroup encryptionGroup = confirmationKey.getGroup();
		final String electionEventId = contextIds.electionEventId();
		final String verificationCardSetId = contextIds.verificationCardSetId();
		final String verificationCardId = contextIds.verificationCardId();
		final ControlComponentlVCCSharePayload controlComponentlVCCSharePayload1 = new ControlComponentlVCCSharePayload(encryptionGroup,
				electionEventId, verificationCardSetId, verificationCardId, 1, new ConfirmationKey(contextIds, confirmationKey), false);
		final ControlComponentlVCCSharePayload controlComponentlVCCSharePayload2 = new ControlComponentlVCCSharePayload(encryptionGroup,
				electionEventId, verificationCardSetId, verificationCardId, 2, new ConfirmationKey(contextIds, confirmationKey), false);
		final ControlComponentlVCCSharePayload controlComponentlVCCSharePayload3 = new ControlComponentlVCCSharePayload(encryptionGroup,
				electionEventId, verificationCardSetId, verificationCardId, 3, new ConfirmationKey(contextIds, confirmationKey), false);
		final ControlComponentlVCCSharePayload controlComponentlVCCSharePayload4 = new ControlComponentlVCCSharePayload(encryptionGroup,
				electionEventId, verificationCardSetId, verificationCardId, 4, new ConfirmationKey(contextIds, confirmationKey), false);
		final List<ControlComponentlVCCSharePayload> longVoteCastReturnCodesContributions = List.of(controlComponentlVCCSharePayload1,
				controlComponentlVCCSharePayload2, controlComponentlVCCSharePayload3, controlComponentlVCCSharePayload4);

		doReturn(0).when(verificationCardStateService).getUnsuccessfulConfirmationAttemptCount(anyString());

		when(signatureKeystoreService.verifySignature(any(), any(), any(), any())).thenReturn(true);

		when(messageHandler.sendMessage(any(), eq(SECOND_CORRELATION_ID))).then(onCall -> {
			voteCastReturnCodeService.onControlComponentlVCCSharePayloadResponse(SECOND_CORRELATION_ID, List.of(
					controlComponentlVCCSharePayload1,
					controlComponentlVCCSharePayload2,
					controlComponentlVCCSharePayload3,
					controlComponentlVCCSharePayload4));
			return true;
		});

		when(inProgressMessageService.getAllNodesResponses(SECOND_CORRELATION_ID)).thenReturn(
				getControlComponentlVCCShareInProgressMessages(SECOND_CORRELATION_ID, controlComponentlVCCSharePayload1,
						controlComponentlVCCSharePayload2,
						controlComponentlVCCSharePayload3,
						controlComponentlVCCSharePayload4));

		final List<String> shortChoiceReturnCodes = RANDOM.genUniqueDecimalStrings(4, 5);
		verificationCardService.saveSentState(verificationCardId, shortChoiceReturnCodes);
		final IllegalStateException exception = assertThrows(IllegalStateException.class,
				() -> voteCastReturnCodeService.retrieveShortVoteCastCode(contextIds, confirmationKey));
		final String expectedErrorMessage = String.format(
				"The signature of the Control Component lVCC Share payload is null. [nodeId: %s, contextIds: %s]", 1, contextIds);
		assertEquals(expectedErrorMessage, exception.getMessage());
	}

	@Test
	@DisplayName("with invalid payload signatures throws an InvalidPayloadSignatureException")
	void retrieveShortVoteCastCodeWithInvalidSignaturesThrows() {
		final GqGroup encryptionGroup = confirmationKey.getGroup();
		final String electionEventId = contextIds.electionEventId();
		final String verificationCardSetId = contextIds.verificationCardSetId();
		final String verificationCardId = contextIds.verificationCardId();

		final GqGroupGenerator gqGroupGenerator = new GqGroupGenerator(encryptionGroup);
		final LongVoteCastReturnCodesShare longVoteCastReturnCodesShare1 = new LongVoteCastReturnCodesShare(electionEventId, verificationCardSetId,
				verificationCardId, 1, gqGroupGenerator.genMember());
		final ControlComponentlVCCSharePayload controlComponentlVCCSharePayload1 = new ControlComponentlVCCSharePayload(electionEventId,
				verificationCardSetId, verificationCardId, 1, encryptionGroup, longVoteCastReturnCodesShare1, true,
				new ConfirmationKey(contextIds, confirmationKey), new CryptoPrimitivesSignature(new byte[] {}));
		final LongVoteCastReturnCodesShare longVoteCastReturnCodesShare2 = new LongVoteCastReturnCodesShare(electionEventId, verificationCardSetId,
				verificationCardId, 2, gqGroupGenerator.genMember());
		final ControlComponentlVCCSharePayload controlComponentlVCCSharePayload2 = new ControlComponentlVCCSharePayload(electionEventId,
				verificationCardSetId, verificationCardId, 2, encryptionGroup, longVoteCastReturnCodesShare2, true,
				new ConfirmationKey(contextIds, confirmationKey), new CryptoPrimitivesSignature(new byte[] {}));
		final LongVoteCastReturnCodesShare longVoteCastReturnCodesShare3 = new LongVoteCastReturnCodesShare(electionEventId, verificationCardSetId,
				verificationCardId, 3, gqGroupGenerator.genMember());
		final ControlComponentlVCCSharePayload controlComponentlVCCSharePayload3 = new ControlComponentlVCCSharePayload(electionEventId,
				verificationCardSetId, verificationCardId, 3, encryptionGroup, longVoteCastReturnCodesShare3, true,
				new ConfirmationKey(contextIds, confirmationKey), new CryptoPrimitivesSignature(new byte[] {}));
		final LongVoteCastReturnCodesShare longVoteCastReturnCodesShare4 = new LongVoteCastReturnCodesShare(electionEventId, verificationCardSetId,
				verificationCardId, 4, gqGroupGenerator.genMember());
		final ControlComponentlVCCSharePayload controlComponentlVCCSharePayload4 = new ControlComponentlVCCSharePayload(electionEventId,
				verificationCardSetId, verificationCardId, 4, encryptionGroup, longVoteCastReturnCodesShare4, true,
				new ConfirmationKey(contextIds, confirmationKey), new CryptoPrimitivesSignature(new byte[] {}));

		when(messageHandler.sendMessage(any(), eq(SECOND_CORRELATION_ID))).then(onCall -> {
			voteCastReturnCodeService.onControlComponentlVCCSharePayloadResponse(SECOND_CORRELATION_ID, List.of(controlComponentlVCCSharePayload1,
					controlComponentlVCCSharePayload2,
					controlComponentlVCCSharePayload3,
					controlComponentlVCCSharePayload4));
			return true;
		});

		when(inProgressMessageService.getAllNodesResponses(SECOND_CORRELATION_ID)).thenReturn(
				getControlComponentlVCCShareInProgressMessages(SECOND_CORRELATION_ID, controlComponentlVCCSharePayload1,
						controlComponentlVCCSharePayload2,
						controlComponentlVCCSharePayload3,
						controlComponentlVCCSharePayload4));

		final List<String> shortChoiceReturnCodes = RANDOM.genUniqueDecimalStrings(4, 5);
		verificationCardService.saveSentState(verificationCardId, shortChoiceReturnCodes);

		doReturn(0).when(verificationCardStateService).getUnsuccessfulConfirmationAttemptCount(anyString());

		final InvalidPayloadSignatureException exception = assertThrows(InvalidPayloadSignatureException.class,
				() -> voteCastReturnCodeService.retrieveShortVoteCastCode(contextIds, confirmationKey));

		final String expectedErrorMessage = String.format("Signature of payload %s is invalid. [nodeId: %s, contextIds: %s]",
				ControlComponentlVCCSharePayload.class.getSimpleName(), 1, contextIds);

		assertEquals(expectedErrorMessage, exception.getMessage());
	}

	@Test
	@DisplayName("when Encrypted short Vote Cast Return Code not found then throws an IllegalStateException")
	void retrieveShortVoteCastCodeWhenEncryptedShortVoteCastReturnCodeNotFoundThrows() throws SignatureException {
		final GqGroup encryptionGroup = confirmationKey.getGroup();
		final String electionEventId = contextIds.electionEventId();
		final String verificationCardSetId = contextIds.verificationCardSetId();
		final String verificationCardId = contextIds.verificationCardId();

		final GqGroupGenerator gqGroupGenerator = new GqGroupGenerator(encryptionGroup);
		final LongVoteCastReturnCodesShare longVoteCastReturnCodesShare1 = new LongVoteCastReturnCodesShare(electionEventId, verificationCardSetId,
				verificationCardId, 1, gqGroupGenerator.genMember());
		final ControlComponentlVCCSharePayload controlComponentlVCCSharePayload1 = new ControlComponentlVCCSharePayload(electionEventId,
				verificationCardSetId, verificationCardId, 1, encryptionGroup, longVoteCastReturnCodesShare1, true,
				new ConfirmationKey(contextIds, confirmationKey), new CryptoPrimitivesSignature(new byte[] {}));
		final LongVoteCastReturnCodesShare longVoteCastReturnCodesShare2 = new LongVoteCastReturnCodesShare(electionEventId, verificationCardSetId,
				verificationCardId, 2, gqGroupGenerator.genMember());
		final ControlComponentlVCCSharePayload controlComponentlVCCSharePayload2 = new ControlComponentlVCCSharePayload(electionEventId,
				verificationCardSetId, verificationCardId, 2, encryptionGroup, longVoteCastReturnCodesShare2, true,
				new ConfirmationKey(contextIds, confirmationKey), new CryptoPrimitivesSignature(new byte[] {}));
		final LongVoteCastReturnCodesShare longVoteCastReturnCodesShare3 = new LongVoteCastReturnCodesShare(electionEventId, verificationCardSetId,
				verificationCardId, 3, gqGroupGenerator.genMember());
		final ControlComponentlVCCSharePayload controlComponentlVCCSharePayload3 = new ControlComponentlVCCSharePayload(electionEventId,
				verificationCardSetId, verificationCardId, 3, encryptionGroup, longVoteCastReturnCodesShare3, true,
				new ConfirmationKey(contextIds, confirmationKey), new CryptoPrimitivesSignature(new byte[] {}));
		final LongVoteCastReturnCodesShare longVoteCastReturnCodesShare4 = new LongVoteCastReturnCodesShare(electionEventId, verificationCardSetId,
				verificationCardId, 4, gqGroupGenerator.genMember());
		final ControlComponentlVCCSharePayload controlComponentlVCCSharePayload4 = new ControlComponentlVCCSharePayload(electionEventId,
				verificationCardSetId, verificationCardId, 4, encryptionGroup, longVoteCastReturnCodesShare4, true,
				new ConfirmationKey(contextIds, confirmationKey), new CryptoPrimitivesSignature(new byte[] {}));
		final List<ControlComponentlVCCSharePayload> longVoteCastReturnCodesContributions = List.of(controlComponentlVCCSharePayload1,
				controlComponentlVCCSharePayload2, controlComponentlVCCSharePayload3, controlComponentlVCCSharePayload4);

		when(signatureKeystoreService.verifySignature(any(), any(), any(), any())).thenReturn(true);

		doReturn(Optional.empty()).when(returnCodesMappingTableRepository).findByHashedLongReturnCode(eq(verificationCardSetId), anyString());

		when(messageHandler.sendMessage(any(), eq(SECOND_CORRELATION_ID))).then(onCall -> {
			voteCastReturnCodeService.onControlComponentlVCCSharePayloadResponse(SECOND_CORRELATION_ID, List.of(
					controlComponentlVCCSharePayload1,
					controlComponentlVCCSharePayload2,
					controlComponentlVCCSharePayload3,
					controlComponentlVCCSharePayload4
			));
			return true;
		});

		when(inProgressMessageService.getAllNodesResponses(SECOND_CORRELATION_ID)).thenReturn(
				getControlComponentlVCCShareInProgressMessages(FIRST_CORRELATION_ID, controlComponentlVCCSharePayload1,
						controlComponentlVCCSharePayload2,
						controlComponentlVCCSharePayload3,
						controlComponentlVCCSharePayload4));

		final List<String> shortChoiceReturnCodes = RANDOM.genUniqueDecimalStrings(4, 5);
		verificationCardService.saveSentState(verificationCardId, shortChoiceReturnCodes);

		doReturn(0).when(verificationCardStateService).getUnsuccessfulConfirmationAttemptCount(anyString());

		final IllegalStateException exception = assertThrows(IllegalStateException.class,
				() -> voteCastReturnCodeService.retrieveShortVoteCastCode(contextIds, confirmationKey));
		final String expectedErrorMessage = String.format(
				"Encrypted short Vote Cast Return Code not found in CMtable. [electionEventId: %s, verificationCardId: %s]", electionEventId,
				verificationCardId);
		assertEquals(expectedErrorMessage, exception.getMessage());
	}

	@Test
	void testExceptionSerializationDeserialization() throws IOException {
		final ObjectMapper objectMapper = DomainObjectMapper.getNewInstance();

		final ConfirmationKeyInvalidException exception = new ConfirmationKeyInvalidException("blabla", 23);

		final byte[] bytes = objectMapper.writeValueAsBytes(exception);
		assertNotNull(bytes);

		final ConfirmationKeyInvalidException exception2 = objectMapper.readValue(bytes, ConfirmationKeyInvalidException.class);
		assertNotNull(exception2);
	}

	private ControlComponenthlVCCPayload getControlComponenthlVCCPayload(final int nodeId, final GqGroup encryptionGroup) {
		final int unsuccessfulConfirmationAttemptCount = 0;
		final CryptoPrimitivesSignature signature = new CryptoPrimitivesSignature(new byte[] { 1, 2, 3 });
		final ConfirmationKey confKey = new ConfirmationKey(contextIds, confirmationKey);
		return new ControlComponenthlVCCPayload(encryptionGroup, nodeId, "1234567890", confKey, unsuccessfulConfirmationAttemptCount, signature);
	}

	private static List<InProgressMessage> getControlComponentlVCCShareInProgressMessages(
			final String firstCorrelationId,
			final ControlComponentlVCCSharePayload controlComponentlVCCSharePayload1,
			final ControlComponentlVCCSharePayload controlComponentlVCCSharePayload2,
			final ControlComponentlVCCSharePayload controlComponentlVCCSharePayload3,
			final ControlComponentlVCCSharePayload controlComponentlVCCSharePayload4) {
		return List.of(
				new InProgressMessage(firstCorrelationId, 1, serializer.serialize(controlComponentlVCCSharePayload1)),
				new InProgressMessage(firstCorrelationId, 2, serializer.serialize(controlComponentlVCCSharePayload2)),
				new InProgressMessage(firstCorrelationId, 3, serializer.serialize(controlComponentlVCCSharePayload3)),
				new InProgressMessage(firstCorrelationId, 4, serializer.serialize(controlComponentlVCCSharePayload4)));

	}

	private static List<InProgressMessage> getControlComponenthlVCCInProgressMessages(
			final String firstCorrelationId,
			final ControlComponenthlVCCPayload controlComponenthlVCCPayload1,
			final ControlComponenthlVCCPayload controlComponenthlVCCPayload2,
			final ControlComponenthlVCCPayload controlComponenthlVCCPayload3,
			final ControlComponenthlVCCPayload controlComponenthlVCCPayload4) {
		return List.of(
				new InProgressMessage(firstCorrelationId, 1, serializer.serialize(controlComponenthlVCCPayload1)),
				new InProgressMessage(firstCorrelationId, 2, serializer.serialize(controlComponenthlVCCPayload2)),
				new InProgressMessage(firstCorrelationId, 3, serializer.serialize(controlComponenthlVCCPayload3)),
				new InProgressMessage(firstCorrelationId, 4, serializer.serialize(controlComponenthlVCCPayload4)));
	}
}
