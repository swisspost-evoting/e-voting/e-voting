/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.process;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import ch.post.it.evoting.domain.InvalidPayloadSignatureException;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;

@ControllerAdvice
public class VotingServerControllerAdvice {

	private static final Logger LOGGER = LoggerFactory.getLogger(VotingServerControllerAdvice.class);

	@ExceptionHandler(value = { IllegalStateException.class, FailedValidationException.class, IllegalArgumentException.class, InvalidPayloadSignatureException.class })
	public ResponseEntity handleIllegalStateException(final ServerHttpRequest serverHttpRequest, final Exception ex) {
		LOGGER.error("Failed to process request. [request: {}]", serverHttpRequest.getURI(), ex);
		return new ResponseEntity<>(HttpStatus.PRECONDITION_FAILED);
	}
}
