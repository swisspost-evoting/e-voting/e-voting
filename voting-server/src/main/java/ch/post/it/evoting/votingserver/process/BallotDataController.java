/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.process;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.post.it.evoting.domain.configuration.BallotDataPayload;
import ch.post.it.evoting.votingserver.idempotence.IdempotenceContext;
import ch.post.it.evoting.votingserver.idempotence.IdempotenceService;

@RestController
@RequestMapping("api/v1/processor/configuration/setupvoting/ballotdata")
public class BallotDataController {

	private static final Logger LOGGER = LoggerFactory.getLogger(BallotDataController.class);

	private final BallotDataService ballotDataService;
	private final IdempotenceService<IdempotenceContext> idempotenceService;

	public BallotDataController(
			final BallotDataService ballotDataService,
			final IdempotenceService<IdempotenceContext> idempotenceService) {
		this.ballotDataService = ballotDataService;
		this.idempotenceService = idempotenceService;
	}

	/**
	 * Saves data for a ballot given the election event id and the ballot id.
	 *
	 * @param electionEventId   the election event identifier.
	 * @param ballotId          the ballot identifier.
	 * @param ballotDataPayload the data for a ballot.
	 */
	@PostMapping("electionevent/{electionEventId}/ballot/{ballotId}")
	public void saveBallotData(
			@PathVariable(Constants.PARAMETER_VALUE_ELECTION_EVENT_ID)
			final String electionEventId,
			@PathVariable(Constants.PARAMETER_VALUE_BALLOT_ID)
			final String ballotId,
			@RequestBody
			final BallotDataPayload ballotDataPayload) {

		validateUUID(electionEventId);
		validateUUID(ballotId);
		checkNotNull(ballotDataPayload);

		LOGGER.info("Received request to save ballot data. [electionEventId: {}, ballotId: {}]", electionEventId, ballotId);

		// Cross validate ids.
		checkArgument(electionEventId.equals(ballotDataPayload.electionEventId()),
				"The request election event id does not match the payload election event id.");
		checkArgument(ballotId.equals(ballotDataPayload.ballotId()),
				"The request ballot id does not match the payload ballot id.");

		idempotenceService.execute(IdempotenceContext.SAVE_BALLOT_DATA, String.format("%s-%s", electionEventId, ballotId),
				() -> ballotDataService.saveBallotData(electionEventId, ballotId, ballotDataPayload.ballot(), ballotDataPayload.ballotTextsJson()));

		LOGGER.info("Ballot data saved. [electionEventId: {}, ballotId: {}].", electionEventId, ballotId);
	}

}
