/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.process;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateNonBlankUCS;
import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;

public record ElectionEventDTO(String electionEventId, String electionEventAlias, String electionEventDescription) {

	public ElectionEventDTO {
		validateUUID(electionEventId);
		validateNonBlankUCS(electionEventAlias);
		validateNonBlankUCS(electionEventDescription);
	}
}
