/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.messaging;

import static ch.post.it.evoting.evotinglibraries.domain.ControlComponentConstants.NODE_IDS;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Arrays;

import jakarta.persistence.Convert;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.IdClass;

import ch.post.it.evoting.domain.converters.BooleanConverter;

@Entity
@IdClass(InProgressMessageId.class)
public class InProgressMessage {

	@Id
	private String correlationId;

	@Id
	private int nodeId;

	private String requestMessageType;

	private String contextId;

	private byte[] responsePayload;

	@Convert(converter = BooleanConverter.class)
	private boolean detectedForAggregation;

	private String responseMessageType;

	public InProgressMessage() {
		//intentionally left blank
	}

	public InProgressMessage(final String correlationId, final int nodeId, final String requestMessageType, final String contextId) {
		checkNotNull(correlationId);
		checkArgument(NODE_IDS.contains(nodeId));
		checkNotNull(requestMessageType);
		checkNotNull(contextId);

		this.correlationId = correlationId;
		this.nodeId = nodeId;
		this.requestMessageType = requestMessageType;
		this.contextId = contextId;
		this.detectedForAggregation = false;
	}

	public InProgressMessage(final String correlationId, final int nodeId, final byte[] responsePayload) {
		checkNotNull(correlationId);
		checkArgument(NODE_IDS.contains(nodeId));
		checkNotNull(responsePayload);

		this.correlationId = correlationId;
		this.nodeId = nodeId;
		this.responsePayload = Arrays.copyOf(responsePayload, responsePayload.length);
		this.detectedForAggregation = false;
	}

	public byte[] getResponsePayload() {
		return Arrays.copyOf(responsePayload, responsePayload.length);
	}

	public void setResponsePayload(final byte[] responsePayload) {
		checkNotNull(responsePayload);
		final byte[] responsePayloadCopy = Arrays.copyOf(responsePayload, responsePayload.length);
		this.responsePayload = responsePayloadCopy;
	}

	public String getCorrelationId() {
		return correlationId;
	}

	public int getNodeId() {
		return nodeId;
	}

	public boolean isDetectedForAggregation() {
		return detectedForAggregation;
	}

	public void setDetectedForAggregation(final boolean b) {
		this.detectedForAggregation = b;
	}

	public String getResponseMessageType() {
		return responseMessageType;
	}

	public void setResponseMessageType(final String messageType) {
		this.responseMessageType = messageType;
	}

	public String getRequestMessageType() {
		return requestMessageType;
	}

	public String getContextId() {
		return contextId;
	}
}
