/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.messaging;

import static ch.post.it.evoting.domain.SharedQueue.CONTROL_COMPONENTS_ADDRESS;
import static ch.post.it.evoting.domain.SharedQueue.MESSAGE_HEADER_MESSAGE_TYPE;
import static ch.post.it.evoting.domain.SharedQueue.MESSAGE_HEADER_NODE_ID;
import static ch.post.it.evoting.domain.SharedQueue.VOTING_SERVER_ADDRESS;
import static ch.post.it.evoting.evotinglibraries.domain.ControlComponentConstants.NODE_IDS;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import static org.apache.activemq.artemis.api.core.Message.HDR_DUPLICATE_DETECTION_ID;

import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.function.BiConsumer;
import java.util.function.Function;

import jakarta.jms.JMSException;
import jakarta.jms.Message;
import jakarta.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.MoreCollectors;

@Component
public class MessageHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(MessageHandler.class);

	private final List<Configuration<?, ?>> configurations;
	private final InProgressMessageService inProgressMessageService;
	private final EntityManager entityManager;
	private final JmsTemplate mutlicastJmsTemplate;
	private final Serializer serializer;

	public MessageHandler(
			@Qualifier("multicastJmsTemplate")
			final JmsTemplate multicastJmsTemplate,
			final Serializer serializer,
			@Lazy
			final List<Configuration<?, ?>> configurations,
			final InProgressMessageService inProgressMessageService,
			final EntityManager entityManager) {
		this.mutlicastJmsTemplate = multicastJmsTemplate;
		this.serializer = serializer;
		this.configurations = configurations;
		this.inProgressMessageService = inProgressMessageService;
		this.entityManager = entityManager;
	}

	public String generateCorrelationId() {
		return UUID.randomUUID().toString();
	}

	/**
	 * As this method is transactional but contains a non-transactional operation (send to queue), it must be the last operation done in the current
	 * transaction.
	 */
	@Transactional
	public <T> String sendMessage(final T msg, final String correlationId) {
		checkNotNull(correlationId);
		return sendMessage(msg, correlationId, NODE_IDS);
	}

	/**
	 * As this method is transactional but contains a non-transactional operation (send to queue), it must be the last operation done in the current
	 * transaction.
	 */
	@Transactional
	public <T> String sendMessage(final T msg) {
		return sendMessage(msg, generateCorrelationId(), NODE_IDS);
	}

	/**
	 * As this method is transactional but contains a non-transactional operation (send to queue), it must be the last operation done in the current
	 * transaction.
	 */
	@Transactional
	public <T> String sendMessage(final T msg, final Integer nodeId) {
		return sendMessage(msg, generateCorrelationId(), Set.of(nodeId));
	}

	/**
	 * As this method is transactional but contains a non-transactional operation (send to queue), it must be the last operation done in the current
	 * transaction.
	 */
	@Transactional
	public <T> String sendMessage(final T msg, final String correlationId, final Integer nodeId) {
		return sendMessage(msg, correlationId, Set.of(nodeId));
	}

	@SuppressWarnings("unchecked")
	protected <T, U> String sendMessage(final T msg, final String correlationId, final Set<Integer> nodes) {
		checkNotNull(msg);
		checkNotNull(nodes);

		final Configuration<T, U> configuration = (Configuration<T, U>) configurations.stream()
				.filter(config -> config.requestMessageType().equals(msg.getClass()))
				.collect(MoreCollectors.onlyElement());

		if (configuration.broadcastRequest()) {
			checkState(nodes.containsAll(NODE_IDS) && nodes.size() == NODE_IDS.size());
		} else {
			checkState(nodes.size() == 1);
		}

		final byte[] payload = serializer.serialize(msg);
		final String requestMessageType = msg.getClass().getName();
		final String contextId = configuration.contextIdExtractor().apply(msg);

		nodes.forEach(nodeId -> inProgressMessageService.storeRequest(correlationId, nodeId, requestMessageType, contextId));

		//ensure the DB is up-to-date before executing non-transactional task (i.e. sending the message)
		entityManager.flush();

		// As this call is not transactional, it must be the last operation done in the current transaction.
		mutlicastJmsTemplate.convertAndSend(CONTROL_COMPONENTS_ADDRESS, payload, jmsMessage -> {
			jmsMessage.setJMSCorrelationID(correlationId);
			jmsMessage.setStringProperty(MESSAGE_HEADER_MESSAGE_TYPE, msg.getClass().getName());
			jmsMessage.setStringProperty(HDR_DUPLICATE_DETECTION_ID.toString(), UUID.randomUUID().toString());
			// Mixing case.
			if (!configuration.broadcastRequest()) {
				jmsMessage.setStringProperty(MESSAGE_HEADER_NODE_ID, String.valueOf(nodes.iterator().next()));
			}
			return jmsMessage;
		});

		return correlationId;
	}

	@Transactional
	@JmsListener(destination = VOTING_SERVER_ADDRESS, containerFactory = "customFactory")
	public <T, U> void onMessage(final Message message) throws JMSException {
		checkNotNull(message);
		final String messageType = checkNotNull(message.getStringProperty(MESSAGE_HEADER_MESSAGE_TYPE));
		final String correlationId = checkNotNull(message.getJMSCorrelationID());

		LOGGER.info("Received new response. [messageType: {}, correlationId: {}]", messageType, correlationId);

		final byte[] messageBody = message.getBody(byte[].class);

		@SuppressWarnings("unchecked")
		final Configuration<T, U> configuration = (Configuration<T, U>) getConfiguration(messageType);

		final U payload = configuration.responseDeserializer().apply(messageBody);
		final int nodeId = configuration.nodeIdExtractor().apply(payload);
		final BiConsumer<String, List<U>> responseHandler = configuration.ResponseHandler();
		final boolean aggregateResults = configuration.aggregateResponseResults();

		if (aggregateResults) {
			//Lock all inProgressMessages with the same correlationId to avoid concurrent modifications and having an incorrect count
			inProgressMessageService.storeResponse(correlationId, nodeId, messageBody, messageType);
			final int count = inProgressMessageService.countAllInProgressMessagesWithResponsePayload(correlationId);
			if (count == NODE_IDS.size()) {
				LOGGER.info("All nodes have returned their contributions. [correlationId: {}]", correlationId);
				inProgressMessageService.setDetectedForAggregation(correlationId);
				handleResponseForAggregatedMessages(correlationId, messageType);
			} else {
				LOGGER.info("Not all nodes have returned their contributions. [correlationId: {}, count: {}]", correlationId, count);
			}
		} else {
			LOGGER.info("Response received. [correlationId: {}, nodeId: {}]", correlationId, nodeId);
			inProgressMessageService.removeInProgressMessage(correlationId, nodeId);
			responseHandler.accept(correlationId, List.of(payload));
		}
	}

	public <T, U> void handleResponseForAggregatedMessages(final String correlationId, final String messageType) {
		checkNotNull(correlationId);
		checkNotNull(messageType);

		@SuppressWarnings("unchecked")
		final Configuration<T, U> configuration = (Configuration<T, U>) getConfiguration(messageType);
		final BiConsumer<String, List<U>> responseHandler = configuration.ResponseHandler();

		final List<InProgressMessage> allNodesResponses = inProgressMessageService.getAllNodesResponses(correlationId);

		checkState(allNodesResponses.stream().allMatch(InProgressMessage::isDetectedForAggregation),
				"Not all nodes have completed their aggregation");

		final List<U> payloads = allNodesResponses.stream()
				.map(InProgressMessage::getResponsePayload)
				.map(configuration.responseDeserializer)
				.sorted(Comparator.comparingInt(configuration.nodeIdExtractor::apply))
				.toList();

		checkState(payloads.size() == NODE_IDS.size(),
				"The number of payloads does not correspond to the number of control component nodes");

		responseHandler.accept(correlationId, payloads);
		inProgressMessageService.removeInProgressMessages(correlationId);

		LOGGER.info("Response for aggregated messages handled. [correlationId: {}]", correlationId);
	}

	private Configuration<?, ?> getConfiguration(final String responseMessageType) {
		final Class<?> messageClass;
		try {
			messageClass = Class.forName(responseMessageType);
		} catch (final ClassNotFoundException e) {
			throw new IllegalArgumentException(
					String.format("The given request message type is unknown. [responseMessageType: %s]", responseMessageType), e);
		}

		return configurations.stream()
				.filter(c -> messageClass.equals(c.responseMessageType()))
				.collect(MoreCollectors.onlyElement());
	}

	public record Configuration<T, U>(
			Class<T> requestMessageType,
			boolean broadcastRequest,
			Function<T, String> contextIdExtractor,
			Class<U> responseMessageType,
			Function<U, Integer> nodeIdExtractor,
			Function<byte[], U> responseDeserializer, BiConsumer<String, List<U>> ResponseHandler,
			boolean aggregateResponseResults) {
	}
}


