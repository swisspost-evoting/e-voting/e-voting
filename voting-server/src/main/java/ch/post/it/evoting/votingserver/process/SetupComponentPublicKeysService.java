/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.process;

import static ch.post.it.evoting.evotinglibraries.domain.ControlComponentConstants.NODE_IDS;
import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Preconditions;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.domain.InvalidPayloadSignatureException;
import ch.post.it.evoting.domain.configuration.SetupComponentPublicKeysResponsePayload;
import ch.post.it.evoting.evotinglibraries.domain.election.SetupComponentPublicKeys;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.SetupComponentPublicKeysPayload;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;
import ch.post.it.evoting.votingserver.messaging.MessageHandler;
import ch.post.it.evoting.votingserver.messaging.ResponseCompletionCompletableFuture;
import ch.post.it.evoting.votingserver.messaging.ResponseCompletionService;
import ch.post.it.evoting.votingserver.messaging.Serializer;

@Service
public class SetupComponentPublicKeysService {

	private static final String GROUP = "group";
	private static final Logger LOGGER = LoggerFactory.getLogger(SetupComponentPublicKeysService.class);

	private final ObjectMapper objectMapper;
	private final ElectionEventService electionEventService;
	private final SetupComponentPublicKeysRepository setupComponentPublicKeysRepository;
	private final MessageHandler messageHandler;
	private final Serializer serializer;
	private final ResponseCompletionService responseCompletionService;

	public SetupComponentPublicKeysService(
			final ObjectMapper objectMapper,
			final ElectionEventService electionEventService,
			final SetupComponentPublicKeysRepository setupComponentPublicKeysRepository,
			final MessageHandler messageHandler,
			final Serializer serializer,
			final ResponseCompletionService responseCompletionService) {
		this.objectMapper = objectMapper;
		this.electionEventService = electionEventService;
		this.setupComponentPublicKeysRepository = setupComponentPublicKeysRepository;
		this.messageHandler = messageHandler;
		this.serializer = serializer;
		this.responseCompletionService = responseCompletionService;
	}

	/**
	 * Saves the setup component public keys and uploads it to the control components.
	 *
	 * @param setupComponentPublicKeysPayload the request payload. Must be non null.
	 * @throws NullPointerException             if {@code setupComponentPublicKeysPayload} is null.
	 * @throws IllegalStateException            if an error occurred while verifying the signature of the election event context payload.
	 * @throws InvalidPayloadSignatureException if the signature of the election event context payload is invalid.
	 * @throws UncheckedIOException             if an error occurs while serializing the election event context.
	 */
	@Transactional
	public void saveSetupComponentPublicKeys(final SetupComponentPublicKeysPayload setupComponentPublicKeysPayload) {
		checkNotNull(setupComponentPublicKeysPayload);

		final String electionEventId = setupComponentPublicKeysPayload.getElectionEventId();

		// save setup component public keys in vote verification
		setupComponentPublicKeysRepository.save(
				buildSetupComponentPublicKeysEntity(electionEventId, setupComponentPublicKeysPayload.getSetupComponentPublicKeys()));

		LOGGER.info("Setup component public keys successfully saved. [electionEventId: {}]", electionEventId);
	}

	/**
	 * Sends the setup component public keys to the Control Components.
	 *
	 * @param setupComponentPublicKeysPayload the request payload. Must be non null.
	 * @return the correlation id of the request.
	 * @throws NullPointerException if {@code setupComponentPublicKeysPayload} is null.
	 */
	public String onRequest(final SetupComponentPublicKeysPayload setupComponentPublicKeysPayload) {
		checkNotNull(setupComponentPublicKeysPayload);

		final String correlationId = messageHandler.sendMessage(setupComponentPublicKeysPayload);

		LOGGER.info("Setup component public keys sent to the control components. [electionEventId: {}, correlationId: {}]",
				setupComponentPublicKeysPayload.getElectionEventId(), correlationId);

		return correlationId;
	}

	/**
	 * Waits for the response from the Control Components to ensure the setup component public keys have been successfully processed.
	 *
	 * @param correlationId the correlation id on which to wait. Must be non-null.
	 * @throws NullPointerException if {@code correlationId} is null.
	 */
	public void waitForResponse(final String correlationId) {
		checkNotNull(correlationId);

		final ResponseCompletionCompletableFuture<List<SetupComponentPublicKeysResponsePayload>> completableFuture = responseCompletionService.registerForResponseCompletion(
				correlationId, new TypeReference<>() {});

		completableFuture.get();
	}

	public void onResponse(final String correlationId, final List<SetupComponentPublicKeysResponsePayload> setupComponentPublicKeysResponsePayloads) {
		checkNotNull(correlationId);
		checkNotNull(setupComponentPublicKeysResponsePayloads);
		final List<SetupComponentPublicKeysResponsePayload> setupComponentPublicKeysResponsePayloadsCopy = setupComponentPublicKeysResponsePayloads.stream()
				.map(Preconditions::checkNotNull).toList();
		checkArgument(setupComponentPublicKeysResponsePayloadsCopy.size() == NODE_IDS.size());

		responseCompletionService.notifyResponseCompleted(correlationId, setupComponentPublicKeysResponsePayloadsCopy);

		final String electionEventId = setupComponentPublicKeysResponsePayloadsCopy.get(0).electionEventId();
		LOGGER.info("Setup component public keys successfully uploaded to the control components. [electionEventId: {}]", electionEventId);
	}

	/**
	 * Recovers the voting client public keys from the setup component public keys entity using the given election event id.
	 *
	 * @param electionEventId the election event id. Must be non-null and a valid UUID.
	 * @return the voting client public keys.
	 * @throws FailedValidationException if {@code electionEventId} is not valid.
	 */
	@Transactional
	public VotingClientPublicKeys getVotingClientPublicKeys(final String electionEventId) {
		validateUUID(electionEventId);

		// Retrieve the setup component public keys.
		final ElectionEventEntity electionEventEntity = electionEventService.retrieveElectionEventEntity(electionEventId);
		final SetupComponentPublicKeysEntity setupComponentPublicKeysEntity = setupComponentPublicKeysRepository
				.findById(electionEventId)
				.orElseThrow(() -> new IllegalStateException(
						String.format("Setup component public keys not found. [electionEventId: %s]", electionEventId)));
		LOGGER.info("Setup component public keys found. [electionEventId: {}]", electionEventId);

		// Retrieve group.
		final GqGroup encryptionGroup = electionEventEntity.getEncryptionGroup();

		// Deserialize the voting client public keys.
		final ElGamalMultiRecipientPublicKey electionPublicKey;
		final ElGamalMultiRecipientPublicKey choiceReturnCodesEncryptionPublicKey;
		try {
			electionPublicKey = objectMapper.reader()
					.withAttribute(GROUP, encryptionGroup)
					.readValue(setupComponentPublicKeysEntity.getElectionPublicKey(), ElGamalMultiRecipientPublicKey.class);
			choiceReturnCodesEncryptionPublicKey = objectMapper.reader()
					.withAttribute(GROUP, encryptionGroup)
					.readValue(setupComponentPublicKeysEntity.getChoiceReturnCodesEncryptionPublicKey(), ElGamalMultiRecipientPublicKey.class);
		} catch (final IOException e) {
			throw new UncheckedIOException(String.format("Failed to deserialize the voting client public keys. [electionEventId: %s]",
					setupComponentPublicKeysEntity.getElectionEventEntity().getElectionEventId()), e);
		}

		LOGGER.info("Voting client public keys recovered. [electionEventId: {}]", electionEventId);
		return new VotingClientPublicKeys(encryptionGroup, electionPublicKey, choiceReturnCodesEncryptionPublicKey);
	}

	private SetupComponentPublicKeysEntity buildSetupComponentPublicKeysEntity(final String electionEventId,
			final SetupComponentPublicKeys setupComponentPublicKeys) {

		final byte[] serializedCombinedControlComponentPublicKeys;
		final byte[] serializedElectoralBoardPublicKey;
		final byte[] serializedElectoralBoardSchnorrProofs;
		final byte[] serializedElectionPublicKey;
		final byte[] serializedChoiceReturnCodesPublicKey;
		final ElectionEventEntity electionEventEntity;
		try {
			serializedCombinedControlComponentPublicKeys = objectMapper.writeValueAsBytes(
					setupComponentPublicKeys.combinedControlComponentPublicKeys());
			serializedElectoralBoardPublicKey = objectMapper.writeValueAsBytes(setupComponentPublicKeys.electoralBoardPublicKey());
			serializedElectoralBoardSchnorrProofs = objectMapper.writeValueAsBytes(setupComponentPublicKeys.electoralBoardSchnorrProofs());
			serializedElectionPublicKey = objectMapper.writeValueAsBytes(setupComponentPublicKeys.electionPublicKey());
			serializedChoiceReturnCodesPublicKey = objectMapper.writeValueAsBytes(setupComponentPublicKeys.choiceReturnCodesEncryptionPublicKey());
			electionEventEntity = electionEventService.retrieveElectionEventEntity(electionEventId);
		} catch (final JsonProcessingException e) {
			throw new UncheckedIOException("Failed to serialize setup component public keys.", e);
		}

		return new SetupComponentPublicKeysEntity(electionEventEntity, serializedCombinedControlComponentPublicKeys,
				serializedElectoralBoardPublicKey,
				serializedElectoralBoardSchnorrProofs, serializedElectionPublicKey, serializedChoiceReturnCodesPublicKey);
	}

	public int extractNodeId(final SetupComponentPublicKeysResponsePayload setupComponentPublicKeysResponsePayload) {
		checkNotNull(setupComponentPublicKeysResponsePayload);

		return setupComponentPublicKeysResponsePayload.nodeId();
	}

	public SetupComponentPublicKeysResponsePayload deserialize(final byte[] bytes) {
		checkNotNull(bytes);

		final byte[] bytesCopy = Arrays.copyOf(bytes, bytes.length);

		return serializer.deserialize(bytesCopy, SetupComponentPublicKeysResponsePayload.class);
	}
}
