/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.process.tally.mixonline;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import jakarta.persistence.Version;

@Entity
@Table(name = "CONTROL_COMPONENT_BALLOT_BOX_PAYLOAD")
class ControlComponentBallotBoxPayloadEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "CONTROL_COMPONENT_BALLOT_BOX_PAYLOAD_GENERATOR")
	@SequenceGenerator(name = "CONTROL_COMPONENT_BALLOT_BOX_PAYLOAD_GENERATOR", sequenceName = "CONTROL_COMPONENT_BALLOT_BOX_PAYLOAD_SEQ", allocationSize = 1)
	private Long id;

	private String electionEventId;

	private String ballotBoxId;

	private int nodeId;

	@Column(name = "PAYLOAD")
	private byte[] controlComponentBallotBoxPayload;

	@Version
	private Integer changeControlId;

	protected ControlComponentBallotBoxPayloadEntity() {
	}

	ControlComponentBallotBoxPayloadEntity(final String electionEventId, final String ballotBoxId, final int nodeId,
			final byte[] controlComponentBallotBoxPayload) {
		this.electionEventId = validateUUID(electionEventId);
		this.ballotBoxId = validateUUID(ballotBoxId);
		this.nodeId = nodeId;
		this.controlComponentBallotBoxPayload = checkNotNull(controlComponentBallotBoxPayload);
	}

	public String getElectionEventId() {
		return electionEventId;
	}

	public String getBallotBoxId() {
		return ballotBoxId;
	}

	public int getNodeId() {
		return nodeId;
	}

	public byte[] getControlComponentBallotBoxPayload() {
		return controlComponentBallotBoxPayload;
	}
}
