/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.process;

import java.io.IOException;
import java.io.UncheckedIOException;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.evotinglibraries.domain.election.Ballot;

@Converter
public class BallotConverter implements AttributeConverter<Ballot, byte[]> {

	private final ObjectMapper objectMapper;

	public BallotConverter(
			final ObjectMapper objectMapper) {
		this.objectMapper = objectMapper;
	}

	@Override
	public byte[] convertToDatabaseColumn(final Ballot ballot) {
		try {
			return objectMapper.writeValueAsBytes(ballot);
		} catch (final JsonProcessingException e) {
			throw new UncheckedIOException("Failed to serialize the ballot.", e);
		}
	}

	@Override
	public Ballot convertToEntityAttribute(final byte[] ballotBytes) {
		try {
			return objectMapper.readValue(ballotBytes, Ballot.class);
		} catch (final IOException e) {
			throw new UncheckedIOException("Failed to deserialize the ballot.", e);
		}
	}

}
