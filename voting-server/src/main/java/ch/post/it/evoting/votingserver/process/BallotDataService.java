/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.process;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ch.post.it.evoting.evotinglibraries.domain.election.Ballot;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;

@Service
public class BallotDataService {

	private static final Logger LOGGER = LoggerFactory.getLogger(BallotDataService.class);

	private final ElectionEventService electionEventService;
	private final BallotDataRepository ballotDataRepository;

	public BallotDataService(
			final ElectionEventService electionEventService,
			final BallotDataRepository ballotDataRepository) {
		this.electionEventService = electionEventService;
		this.ballotDataRepository = ballotDataRepository;
	}

	/**
	 * Saves the ballot data.
	 *
	 * @param electionEventId the election event id of the ballot. Must be a valid uuid.
	 * @param ballotId        the ballot id of the ballot. Must be a valid uuid.
	 * @param ballot          the ballot to save.
	 * @param ballotTextsJson the ballot texts to save.
	 * @throws NullPointerException      if any parameter is null.
	 * @throws FailedValidationException if {@code electionEventId} or {@code ballotId} is not a valid uuid.
	 */
	@Transactional
	public void saveBallotData(final String electionEventId, final String ballotId, final Ballot ballot, final String ballotTextsJson) {
		validateUUID(electionEventId);
		validateUUID(ballotId);
		checkNotNull(ballot);
		checkNotNull(ballotTextsJson);

		final ElectionEventEntity electionEventEntity = electionEventService.retrieveElectionEventEntity(electionEventId);
		final BallotDataEntity ballotDataEntity = new BallotDataEntity(ballotId, electionEventEntity, ballot, ballotTextsJson);

		ballotDataRepository.save(ballotDataEntity);
		LOGGER.debug("Ballot data saved. [electionEventId: {}, ballotId: {}]", electionEventId, ballotId);
	}

	/**
	 * Gets the ballot data associated with the given ballot id.
	 *
	 * @param electionEventId the election event id of the ballot. Must be a valid uuid.
	 * @param ballotId        the ballot id of the ballot. Must be a valid uuid.
	 * @return the ballot data as a {@code BallotData}.
	 * @throws NullPointerException      if any parameter is null.
	 * @throws FailedValidationException if {@code electionEventId} or {@code ballotId} is not a valid uuid.
	 * @throws IllegalStateException     if no ballot is found for the given ids.
	 */
	@Transactional
	public BallotData getBallotData(final String electionEventId, final String ballotId) {
		validateUUID(electionEventId);
		validateUUID(ballotId);

		final BallotData ballotData = ballotDataRepository.findByElectionEventIdAndBallotId(electionEventId, ballotId)
				.map(ballotDataEntity -> new BallotData(ballotDataEntity.getBallot(), ballotDataEntity.getBallotTextsJson()))
				.orElseThrow(
						() -> new IllegalStateException(String.format("Ballot data not found. [electionEventId: %s, ballotId: %s]", electionEventId,
								ballotId)));
		LOGGER.debug("Ballot data retrieved. [electionEventId: {}, ballotId: {}", electionEventId, ballotId);

		return ballotData;
	}

}