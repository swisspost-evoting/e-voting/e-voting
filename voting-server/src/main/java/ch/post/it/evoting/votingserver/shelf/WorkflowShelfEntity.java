/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.shelf;

import static com.google.common.base.Preconditions.checkNotNull;

import java.time.LocalDateTime;
import java.util.Arrays;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "WORKFLOW_SHELF")
public class WorkflowShelfEntity {

	@Id
	private String id;

	private byte[] shelfData;

	private LocalDateTime creationDate;

	protected WorkflowShelfEntity() {
	}

	public WorkflowShelfEntity(final String id, final byte[] shelfData) {
		checkNotNull(id);
		byte[] dataCopy = Arrays.copyOf(checkNotNull(shelfData), shelfData.length);

		this.id = id;
		this.shelfData = dataCopy;
		this.creationDate = LocalDateTime.now();
	}

	public String getId() {
		return id;
	}

	public byte[] getShelfData() {
		return shelfData;
	}

	public LocalDateTime getCreationDate() {
		return creationDate;
	}
}
