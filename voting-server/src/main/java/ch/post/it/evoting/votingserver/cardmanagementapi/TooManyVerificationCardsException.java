/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.cardmanagementapi;

public class TooManyVerificationCardsException extends RuntimeException {

	private static final long serialVersionUID = 1;

	public TooManyVerificationCardsException(final String message) {
		super(message);
	}

}
