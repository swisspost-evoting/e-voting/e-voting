/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.process.tally.mixonline;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
public interface ShufflePayloadRepository extends CrudRepository<ShufflePayloadEntity, Long> {

	@Transactional
	List<ShufflePayloadEntity> findByElectionEventIdAndBallotBoxIdOrderByNodeId(final String electionEventId, final String ballotBoxId);

	Integer countByElectionEventIdAndBallotBoxId(final String electionEventId, final String ballotBoxId);

	@Query("select coalesce(max(e.nodeId), 0) from ShufflePayloadEntity e where e.electionEventId = ?1 and e.ballotBoxId = ?2")
	int maxNodeId(final String electionEventId, final String ballotBoxId);

}
