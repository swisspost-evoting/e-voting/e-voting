/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.process.configuration.setupvoting;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
public interface EncLongCodeShareRepository extends CrudRepository<EncLongCodeShareEntity, Long> {

	long countByElectionEventIdAndVerificationCardSetId(final String electionEventId, final String verificationCardSetId);

	@Transactional
	List<EncLongCodeShareEntity> findByElectionEventIdAndVerificationCardSetIdAndChunkId(final String electionEventId,
			final String verificationCardSetId, int chunkId);

}
