/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.process.configuration.setupvoting;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.post.it.evoting.evotinglibraries.domain.configuration.ControlComponentPublicKeysPayload;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.ElectionEventContextPayload;
import ch.post.it.evoting.votingserver.idempotence.IdempotenceContext;
import ch.post.it.evoting.votingserver.idempotence.IdempotenceService;
import ch.post.it.evoting.votingserver.process.ElectionEventContextService;

@RestController
@RequestMapping("/api/v1/configuration/setupvoting")
public class KeyGenerationController {

	private static final Logger LOGGER = LoggerFactory.getLogger(KeyGenerationController.class);
	private final KeyGenerationService keyGenerationService;
	private final IdempotenceService<IdempotenceContext> idempotenceService;
	private final ElectionEventContextService electionEventContextService;

	public KeyGenerationController(final KeyGenerationService keyGenerationService,
			final IdempotenceService<IdempotenceContext> idempotenceService,
			final ElectionEventContextService electionEventContextService) {
		this.keyGenerationService = keyGenerationService;
		this.idempotenceService = idempotenceService;
		this.electionEventContextService = electionEventContextService;
	}

	@PostMapping("/keygeneration/electionevent/{electionEventId}")
	public List<ControlComponentPublicKeysPayload> getKeyGenerations(
			@PathVariable
			final String electionEventId,
			@RequestBody
			final ElectionEventContextPayload electionEventContextPayload) {
		validateUUID(electionEventId);
		checkNotNull(electionEventContextPayload);
		checkArgument(electionEventId.equals(electionEventContextPayload.getElectionEventContext().electionEventId()));
		electionEventContextService.verifyPayloadSignature(electionEventContextPayload);

		LOGGER.debug("Saving the election event context. [contextId: {}]", electionEventId);
		idempotenceService.execute(IdempotenceContext.SAVE_ELECTION_EVENT_CONTEXT, electionEventId,
				() -> electionEventContextService.saveElectionEventContext(electionEventContextPayload));

		LOGGER.info("Requesting control components Key Generation. [contextId: {}]", electionEventId);

		final String correlationId = keyGenerationService.onRequest(electionEventContextPayload);
		return keyGenerationService.waitForResponse(correlationId);
	}
}
