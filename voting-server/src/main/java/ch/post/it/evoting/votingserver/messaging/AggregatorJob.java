/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.messaging;

import static ch.post.it.evoting.evotinglibraries.domain.ControlComponentConstants.NODE_IDS;
import static com.google.common.base.Preconditions.checkState;

import jakarta.annotation.PostConstruct;

import java.util.List;
import java.util.Map;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

@Component
@DisallowConcurrentExecution
public class AggregatorJob extends QuartzJobBean {

	private static final Logger LOGGER = LoggerFactory.getLogger(AggregatorJob.class);
	private final AggregatorService aggregatorService;
	private final Scheduler scheduler;
	private final boolean jobEnabled;
	public static final String AGGREGATOR_JOB_NAME = "aggregator-job";

	public AggregatorJob(final AggregatorService aggregatorService,
			final Scheduler scheduler,
			@Value("${aggregator-job.enabled:true}")
			final boolean jobEnabled) {
		this.aggregatorService = aggregatorService;
		this.scheduler = scheduler;
		this.jobEnabled = jobEnabled;
	}

	@PostConstruct
	public void init() {
		try {
			if (!jobEnabled) {
				scheduler.pauseJob(new JobKey(AGGREGATOR_JOB_NAME));
			}
		} catch (final SchedulerException e) {
			throw new IllegalStateException("Unable to pause the scheduler job", e);
		}
	}

	@Override
	protected void executeInternal(final JobExecutionContext context) {
		LOGGER.debug("Executing Aggregator Job...");
		final Map<String, List<InProgressMessage>> messages = aggregatorService.findMessagesForAggregationGroupedByCorrelationId();

		if (!messages.isEmpty()) {
			LOGGER.info("The aggregator job found messages to aggregate. [messages size: {}]", messages.size());
		}

		messages.forEach((correlationId, inProgressMessages) -> {
			checkState(inProgressMessages.size() == NODE_IDS.size(),
					"Not all nodes have responded yet. [correlationId: %s, inProgressMessages size: %s]", correlationId, inProgressMessages.size());
			aggregatorService.aggregateInProgressMessage(correlationId, inProgressMessages.getFirst().getResponseMessageType());
		});
		LOGGER.debug("Aggregator Job executed.");
	}
}
