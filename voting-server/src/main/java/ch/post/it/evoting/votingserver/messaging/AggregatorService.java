/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.messaging;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import jakarta.jms.JMSException;
import jakarta.jms.Message;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AggregatorService {

	private static final Logger LOGGER = LoggerFactory.getLogger(AggregatorService.class);
	private static final String AGGREGATED_MESSAGE_QUEUE = "voting-server-aggregation";
	private final InProgressMessageService inProgressMessageService;
	private final MessageHandler messageHandler;
	private final Serializer serializer;
	private final JmsTemplate jmsTemplate;

	public AggregatorService(final InProgressMessageService inProgressMessageService,
			final MessageHandler messageHandler,
			final Serializer serializer,
			@Qualifier("jmsTemplate")
			final JmsTemplate jmsTemplate) {
		this.inProgressMessageService = inProgressMessageService;
		this.messageHandler = messageHandler;
		this.serializer = serializer;
		this.jmsTemplate = jmsTemplate;
	}

	public Map<String, List<InProgressMessage>> findMessagesForAggregationGroupedByCorrelationId() {
		return inProgressMessageService.findAggregateReadyMessages().stream()
				.collect(Collectors.groupingBy(InProgressMessage::getCorrelationId));
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void aggregateInProgressMessage(final String correlationId, final String messageType) {
		checkNotNull(correlationId);
		checkNotNull(messageType);

		LOGGER.info("Messages ready to be aggregated. [correlationId: {}, messageType: {}].", correlationId, messageType);

		inProgressMessageService.setDetectedForAggregation(correlationId);
		sendMessage(correlationId, messageType);
	}

	private void sendMessage(final String correlationId, final String messageType) {
		checkNotNull(correlationId);
		checkNotNull(messageType);

		final byte[] payload = serializer.serialize(new AggregationInformationMessage(correlationId, messageType));
		jmsTemplate.convertAndSend(AGGREGATED_MESSAGE_QUEUE, payload);
		LOGGER.debug("Message information for aggregation sent through the queue. [correlationId: {}].", correlationId);
	}

	@Transactional
	@JmsListener(destination = AGGREGATED_MESSAGE_QUEUE, containerFactory = "customFactory")
	public void onMessageReceived(final Message message) throws JMSException {
		checkNotNull(message);

		final byte[] messageBody = message.getBody(byte[].class);
		final AggregationInformationMessage aggregationInformationMessage = serializer.deserialize(messageBody, AggregationInformationMessage.class);

		checkNotNull(aggregationInformationMessage);
		checkNotNull(aggregationInformationMessage.correlationId());
		checkNotNull(aggregationInformationMessage.messageType());

		LOGGER.debug("Message information for aggregation received from the queue. [correlationId: {}].",
				aggregationInformationMessage.correlationId());

		messageHandler.handleResponseForAggregatedMessages(aggregationInformationMessage.correlationId(),
				aggregationInformationMessage.messageType());
	}

	private record AggregationInformationMessage(String correlationId, String messageType) {
	}
}
