/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.process.tally.mixonline;

import static ch.post.it.evoting.evotinglibraries.domain.ControlComponentConstants.NODE_IDS;
import static ch.post.it.evoting.evotinglibraries.domain.validations.ControlComponentVotesHashPayloadValidation.validate;
import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Preconditions;
import com.google.common.collect.MoreCollectors;

import ch.post.it.evoting.domain.tally.MixDecryptOnlineRequestPayload;
import ch.post.it.evoting.domain.tally.MixDecryptOnlineResponsePayload;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.ControlComponentShufflePayload;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.ControlComponentVotesHashPayload;
import ch.post.it.evoting.evotinglibraries.domain.tally.ControlComponentBallotBoxPayload;
import ch.post.it.evoting.votingserver.messaging.MessageHandler;
import ch.post.it.evoting.votingserver.messaging.Serializer;

@Service
public class MixDecryptOnlineService {

	private static final Logger LOGGER = LoggerFactory.getLogger(MixDecryptOnlineService.class);
	private final Serializer serializer;
	private final MessageHandler messageHandler;
	private final MixnetPayloadService mixnetPayloadService;

	public MixDecryptOnlineService(
			final Serializer serializer,
			final MessageHandler messageHandler,
			final MixnetPayloadService mixnetPayloadService) {
		this.serializer = serializer;
		this.messageHandler = messageHandler;
		this.mixnetPayloadService = mixnetPayloadService;
	}

	public void onRequest(final String electionEventId, final String ballotBoxId,
			final List<ControlComponentVotesHashPayload> controlComponentVotesHashPayloads) {
		validateUUID(electionEventId);
		validateUUID(ballotBoxId);

		validate(electionEventId, ballotBoxId, controlComponentVotesHashPayloads);

		final int initialNodeId = 1;
		final List<ControlComponentShufflePayload> controlComponentShufflePayloads = Collections.emptyList();

		final MixDecryptOnlineRequestPayload mixDecryptOnlineRequestPayload = new MixDecryptOnlineRequestPayload(electionEventId, ballotBoxId,
				initialNodeId, controlComponentVotesHashPayloads, controlComponentShufflePayloads);

		LOGGER.info("Starting mixing. [electionEventId: {}, ballotBoxId: {}]", electionEventId, ballotBoxId);

		messageHandler.sendMessage(mixDecryptOnlineRequestPayload, initialNodeId);
	}

	@Transactional
	public void onResponse(final String correlationId, final List<MixDecryptOnlineResponsePayload> mixDecryptOnlineResponsePayloads) {
		checkNotNull(correlationId);
		checkNotNull(mixDecryptOnlineResponsePayloads);
		final List<MixDecryptOnlineResponsePayload> mixDecryptOnlineResponsePayloadsCopy = mixDecryptOnlineResponsePayloads.stream()
				.map(Preconditions::checkNotNull).toList();
		final MixDecryptOnlineResponsePayload mixDecryptOnlineResponsePayload = mixDecryptOnlineResponsePayloadsCopy.stream()
				.collect(MoreCollectors.onlyElement());

		final String electionEventId = mixDecryptOnlineResponsePayload.controlComponentBallotBoxPayload().getElectionEventId();
		final String ballotBoxId = mixDecryptOnlineResponsePayload.controlComponentBallotBoxPayload().getBallotBoxId();
		final int nodeId = mixDecryptOnlineResponsePayload.controlComponentBallotBoxPayload().getNodeId();

		// Save payloads
		final int lastSavedNodeId = mixnetPayloadService.getMaxNodeId(electionEventId, ballotBoxId);
		if (lastSavedNodeId < nodeId) {
			final ControlComponentBallotBoxPayload controlComponentBallotBoxPayload = mixDecryptOnlineResponsePayload.controlComponentBallotBoxPayload();
			mixnetPayloadService.saveControlComponentBallotBoxPayload(controlComponentBallotBoxPayload);

			final ControlComponentShufflePayload mixnetShufflePayload = mixDecryptOnlineResponsePayload.controlComponentShufflePayload();
			mixnetPayloadService.saveShufflePayload(mixnetShufflePayload);

			LOGGER.info(
					"Control component ballot box and shuffle payloads are successfully saved for node {}. [electionEventId:{}, ballotBoxId:{}, correlationId:{}]",
					nodeId, electionEventId, ballotBoxId, correlationId);
		}

		// Check mixing progress
		if (nodeId < NODE_IDS.size()) {

			// Prepare request for the next node
			final List<ControlComponentShufflePayload> shufflePayloads = mixnetPayloadService.getControlComponentShufflePayloadsOrderByNodeId(
					electionEventId, ballotBoxId);
			final List<ControlComponentVotesHashPayload> controlComponentVotesHashPayloads = mixnetPayloadService.getControlComponentVotesHashPayloads(
					electionEventId, ballotBoxId);

			final int nextNodeId = nodeId + 1;
			final MixDecryptOnlineRequestPayload mixDecryptOnlineRequestPayload = new MixDecryptOnlineRequestPayload(electionEventId, ballotBoxId,
					nextNodeId, controlComponentVotesHashPayloads, shufflePayloads);

			messageHandler.sendMessage(mixDecryptOnlineRequestPayload, correlationId, nextNodeId);
			LOGGER.info("Sent next mixing request to node {} [electionEventId:{}, ballotBoxId:{}, correlationId:{}]", nextNodeId, electionEventId,
					ballotBoxId, correlationId);

		} else {

			// All nodes have sent a response, process is completed
			LOGGER.info("Successfully mixed the ballot box. [electionEventId:{}, ballotBoxId:{}, correlationId:{}]", electionEventId, ballotBoxId,
					correlationId);

		}
	}

	public int extractNodeId(final MixDecryptOnlineResponsePayload mixDecryptOnlineResponsePayload) {
		checkNotNull(mixDecryptOnlineResponsePayload);

		return mixDecryptOnlineResponsePayload.controlComponentBallotBoxPayload().getNodeId();
	}

	public MixDecryptOnlineResponsePayload deserialize(final byte[] messageBytes) {
		checkNotNull(messageBytes);
		final byte[] messageBytesCopy = Arrays.copyOf(messageBytes, messageBytes.length);

		return serializer.deserialize(messageBytesCopy, MixDecryptOnlineResponsePayload.class);
	}
}
