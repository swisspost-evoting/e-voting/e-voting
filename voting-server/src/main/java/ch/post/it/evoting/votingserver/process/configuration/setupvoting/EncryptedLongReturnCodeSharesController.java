/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.process.configuration.setupvoting;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.post.it.evoting.domain.configuration.setupvoting.ComputingStatus;
import ch.post.it.evoting.domain.reactor.Box;
import ch.post.it.evoting.evotinglibraries.domain.returncodes.SetupComponentVerificationDataPayload;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

@RestController
@RequestMapping("/api/v1/configuration/")
public class EncryptedLongReturnCodeSharesController {

	private static final Logger LOGGER = LoggerFactory.getLogger(EncryptedLongReturnCodeSharesController.class);

	private final EncryptedLongReturnCodeSharesService encryptedLongReturnCodeSharesService;

	public EncryptedLongReturnCodeSharesController(final EncryptedLongReturnCodeSharesService encryptedLongReturnCodeSharesService) {
		this.encryptedLongReturnCodeSharesService = encryptedLongReturnCodeSharesService;
	}

	@PutMapping(value = "/electionevent/{electionEventId}/verificationcardset/{verificationCardSetId}/computegenenclongcodeshares", consumes = MediaType.APPLICATION_NDJSON_VALUE)
	public Mono<ResponseEntity<Void>> computeGenEncLongCodeShares(
			@PathVariable
			final String electionEventId,
			@PathVariable
			final String verificationCardSetId,
			@RequestBody
			final Flux<SetupComponentVerificationDataPayload> setupComponentVerificationDataPayloads) {

		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);
		checkNotNull(setupComponentVerificationDataPayloads);

		return setupComponentVerificationDataPayloads
				.publishOn(Schedulers.boundedElastic())
				.doOnNext(setupComponentVerificationDataPayload -> {
					checkNotNull(setupComponentVerificationDataPayload);
					checkArgument(setupComponentVerificationDataPayload.getElectionEventId().equals(electionEventId));
					checkArgument(setupComponentVerificationDataPayload.getVerificationCardSetId().equals(verificationCardSetId));
					final int chunkId = setupComponentVerificationDataPayload.getChunkId();
					checkArgument(chunkId >= 0);

					LOGGER.debug("Received request to generate EncLongCodeShares. [electionEventId: {}, verificationCardSetId: {},  chunkId: {}]",
							electionEventId, verificationCardSetId, chunkId);

					// Idempotence implemented in the onRequest method.
					encryptedLongReturnCodeSharesService.onRequest(setupComponentVerificationDataPayload);

					LOGGER.info("Broadcasted requests to generate EncLongCodeShares. [electionEventId: {}, verificationCardSetId: {},  chunkId: {}]",
							electionEventId, verificationCardSetId, chunkId);
				})
				.then(Mono.just(new ResponseEntity<>(HttpStatus.CREATED)));
	}

	@GetMapping(value = "/electionevent/{electionEventId}/verificationcardset/{verificationCardSetId}/chunkcount/{chunkCount}/status", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ComputingStatus> checkGenEncLongCodeSharesStatus(
			@PathVariable
			final String electionEventId,
			@PathVariable
			final String verificationCardSetId,
			@PathVariable
			final int chunkCount) {

		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);
		checkArgument(chunkCount >= 0);

		LOGGER.debug(
				"Received request to check EncLongCodeShares generation status. [electionEventId: {}, verificationCardSetId: {},  chunkCount: {}]",
				electionEventId, verificationCardSetId, chunkCount);

		final ComputingStatus computingStatus = encryptedLongReturnCodeSharesService.getEncLongCodeSharesComputingStatus(electionEventId,
				verificationCardSetId, chunkCount);

		LOGGER.info("Checked computing status. [electionEventId: {}, verificationCardSetId: {},  chunkCount: {}, status: {}]", electionEventId,
				verificationCardSetId, chunkCount, computingStatus);

		return new ResponseEntity<>(computingStatus, HttpStatus.OK);
	}

	@PostMapping(value = "/electionevent/{electionEventId}/verificationcardset/{verificationCardSetId}/download", consumes = MediaType.APPLICATION_NDJSON_VALUE, produces = MediaType.APPLICATION_NDJSON_VALUE)
	public Flux<Box<List<byte[]>>> downloadGenEncLongCodeShares(
			@PathVariable
			final String electionEventId,
			@PathVariable
			final String verificationCardSetId,
			@RequestBody
			final Flux<Integer> chunkIds) {

		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);
		checkNotNull(chunkIds);

		return chunkIds
				.map(chunkId -> {
					checkArgument(chunkId >= 0);
					LOGGER.debug(
							"Received request to download control component code shares payload. [electionEventId: {}, verificationCardSetId: {},  chunkId: {}]",
							electionEventId, verificationCardSetId, chunkId);

					final List<byte[]> controlComponentCodeSharesPayloadsBytes = encryptedLongReturnCodeSharesService.getEncLongCodeShares(
							electionEventId, verificationCardSetId, chunkId);

					LOGGER.info(
							"Retrieved control component code shares payload for download. [electionEventId: {}, verificationCardSetId: {},  chunkId: {}]",
							electionEventId, verificationCardSetId, chunkId);

					return new Box<>(controlComponentCodeSharesPayloadsBytes);
				})
				.limitRate(1)
				.subscribeOn(Schedulers.parallel());
	}

}
