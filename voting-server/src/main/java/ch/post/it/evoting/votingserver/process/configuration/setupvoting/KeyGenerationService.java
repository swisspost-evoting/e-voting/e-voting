/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.process.configuration.setupvoting;

import static ch.post.it.evoting.evotinglibraries.domain.ControlComponentConstants.NODE_IDS;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.UncheckedIOException;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.type.TypeReference;
import com.google.common.base.Preconditions;

import ch.post.it.evoting.domain.InvalidPayloadSignatureException;
import ch.post.it.evoting.evotinglibraries.domain.configuration.ControlComponentPublicKeysPayload;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.ElectionEventContextPayload;
import ch.post.it.evoting.votingserver.messaging.MessageHandler;
import ch.post.it.evoting.votingserver.messaging.ResponseCompletionCompletableFuture;
import ch.post.it.evoting.votingserver.messaging.ResponseCompletionService;
import ch.post.it.evoting.votingserver.messaging.Serializer;
import ch.post.it.evoting.votingserver.process.ElectionEventContextService;

@Service
public class KeyGenerationService {

	private static final Logger LOGGER = LoggerFactory.getLogger(KeyGenerationService.class);

	private final ResponseCompletionService responseCompletionService;
	private final MessageHandler messageHandler;
	private final Serializer serializer;
	private final ElectionEventContextService electionEventContextService;

	public KeyGenerationService(
			final ResponseCompletionService responseCompletionService,
			final MessageHandler messageHandler,
			final Serializer serializer,
			final ElectionEventContextService electionEventContextService) {
		this.responseCompletionService = responseCompletionService;
		this.messageHandler = messageHandler;
		this.serializer = serializer;
		this.electionEventContextService = electionEventContextService;
	}

	/**
	 * Uploads the election event context to the control components and generates the CC Keys.
	 *
	 * @param electionEventContextPayload the request payload. Must be non null.
	 * @throws NullPointerException             if {@code electionEventContextPayload} is null.
	 * @throws IllegalStateException            if an error occurred while verifying the signature of the election event context payload.
	 * @throws IllegalArgumentException         if the election event finish date is in the past.
	 * @throws InvalidPayloadSignatureException if the signature of the election event context payload is invalid.
	 * @throws UncheckedIOException             if an error occurs while serializing the election event context.
	 */
	@Transactional
	public String onRequest(final ElectionEventContextPayload electionEventContextPayload) {
		checkNotNull(electionEventContextPayload);
		electionEventContextService.verifyPayloadSignature(electionEventContextPayload);

		final String electionEventId = electionEventContextPayload.getElectionEventContext().electionEventId();

		final String correlationId = messageHandler.sendMessage(electionEventContextPayload);

		LOGGER.info("Election event context successfully uploaded to the control components. [electionEventId: {}, correlationId: {}]",
				electionEventId, correlationId);

		return correlationId;
	}

	public List<ControlComponentPublicKeysPayload> waitForResponse(final String correlationId) {
		checkNotNull(correlationId);

		final ResponseCompletionCompletableFuture<List<ControlComponentPublicKeysPayload>> completableFuture = responseCompletionService.registerForResponseCompletion(
				correlationId, new TypeReference<>() {
				});

		return completableFuture.get();
	}

	@Transactional
	public void onResponse(final String correlationId, final List<ControlComponentPublicKeysPayload> controlComponentPublicKeysPayloads) {
		checkNotNull(correlationId);
		checkNotNull(controlComponentPublicKeysPayloads);
		final List<ControlComponentPublicKeysPayload> controlComponentPublicKeysPayloadsCopy = controlComponentPublicKeysPayloads.stream()
				.map(Preconditions::checkNotNull).toList();
		checkArgument(controlComponentPublicKeysPayloadsCopy.size() == NODE_IDS.size());

		responseCompletionService.notifyResponseCompleted(correlationId, controlComponentPublicKeysPayloadsCopy);

		final String electionEventId = controlComponentPublicKeysPayloadsCopy.get(0).getElectionEventId();
		LOGGER.info("Successfully uploaded election event context and requested CC keys. [electionEventId: {}, correlationId: {}]",
				electionEventId, correlationId);
	}

	public int extractNodeId(final ControlComponentPublicKeysPayload controlComponentPublicKeysPayload) {
		checkNotNull(controlComponentPublicKeysPayload);

		return controlComponentPublicKeysPayload.getControlComponentPublicKeys().nodeId();
	}

	public ControlComponentPublicKeysPayload deserialize(final byte[] messageBytes) {
		checkNotNull(messageBytes);
		final byte[] messageBytesCopy = Arrays.copyOf(messageBytes, messageBytes.length);

		return serializer.deserialize(messageBytesCopy, ControlComponentPublicKeysPayload.class);
	}
}
