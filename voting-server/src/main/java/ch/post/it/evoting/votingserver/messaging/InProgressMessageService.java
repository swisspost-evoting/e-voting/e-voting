/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.messaging;

import static ch.post.it.evoting.evotinglibraries.domain.ControlComponentConstants.NODE_IDS;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.MoreCollectors;

@Service
public class InProgressMessageService {
	private final InProgressMessageRepository inProgressMessageRepository;

	public InProgressMessageService(final InProgressMessageRepository inProgressMessageRepository) {
		this.inProgressMessageRepository = inProgressMessageRepository;
	}

	@Transactional
	public void storeRequest(final String correlationId, final int nodeId, final String requestMessageType, final String contextId) {
		checkNotNull(correlationId);
		checkArgument(NODE_IDS.contains(nodeId));
		checkNotNull(requestMessageType);
		checkNotNull(contextId);

		final InProgressMessage inProgressMessage = new InProgressMessage(correlationId, nodeId, requestMessageType, contextId);
		inProgressMessageRepository.save(inProgressMessage);
	}

	@Transactional
	public void storeResponse(final String correlationId, final int nodeId, final byte[] payload, final String responseMessageType) {
		checkNotNull(correlationId);
		checkArgument(NODE_IDS.contains(nodeId));
		checkNotNull(payload);
		checkNotNull(responseMessageType);

		final byte[] payloadCopy = Arrays.copyOf(payload, payload.length);

		//Lock all for lines with the same correlationId to avoid concurrent modifications within the same correlationId
		final List<InProgressMessage> inProgressMessages = inProgressMessageRepository.findAllByCorrelationIdOrderByNodeId(correlationId);

		final InProgressMessage inProgressMessage = inProgressMessages.stream().filter(ipm -> ipm.getNodeId() == nodeId)
				.collect(MoreCollectors.onlyElement());

		inProgressMessage.setResponsePayload(payloadCopy);
		inProgressMessage.setResponseMessageType(responseMessageType);
		inProgressMessageRepository.save(inProgressMessage);
	}

	@Transactional
	public int countAllInProgressMessagesWithResponsePayload(final String correlationId) {
		checkNotNull(correlationId);
		return inProgressMessageRepository.countByCorrelationIdAndResponsePayloadIsNotNull(correlationId);
	}

	@Transactional
	public List<InProgressMessage> getAllNodesResponses(final String correlationId) {
		checkNotNull(correlationId);
		return inProgressMessageRepository.findAllByCorrelationIdAndResponsePayloadIsNotNull(correlationId);
	}

	@Transactional
	public void removeInProgressMessage(final String correlationId, final int nodeId) {
		checkNotNull(correlationId);
		checkArgument(NODE_IDS.contains(nodeId));

		final InProgressMessageId inProgressMessageId = new InProgressMessageId(correlationId, nodeId);
		inProgressMessageRepository.deleteById(inProgressMessageId);
	}

	@Transactional
	public void removeInProgressMessages(final String correlationId) {
		checkNotNull(correlationId);
		inProgressMessageRepository.deleteAllByCorrelationId(correlationId);
	}

	@Transactional
	public void setDetectedForAggregation(final String correlationId) {
		checkNotNull(correlationId);

		//lock all for lines with the same correlationId to avoid the message's receiver being able to read the rows before this transaction commit
		final List<InProgressMessage> inProgressMessages = inProgressMessageRepository.findAllByCorrelationIdAndResponsePayloadIsNotNullOrderByNodeId(
				correlationId);

		inProgressMessages.forEach(ipm -> ipm.setDetectedForAggregation(true));

		inProgressMessageRepository.saveAll(inProgressMessages);
	}

	@Transactional
	public List<InProgressMessage> findAggregateReadyMessages() {
		return inProgressMessageRepository.findAggregateReadyMessages(NODE_IDS.size());
	}
}
