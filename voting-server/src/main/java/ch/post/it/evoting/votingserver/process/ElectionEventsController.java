/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.process;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/processor/election-events/")
public class ElectionEventsController {

	private final ElectionEventContextService electionEventContextService;

	public ElectionEventsController(final ElectionEventContextService electionEventContextService) {
		this.electionEventContextService = electionEventContextService;
	}

	/**
	 * Lists all election events.
	 *
	 * @return The list of all election events.
	 */
	@GetMapping(value = "overview", produces = "application/json")
	@ResponseStatus(HttpStatus.OK)
	public List<ElectionEventDTO> getAllElectionEvents(){
		return electionEventContextService.retrieveAll();
	}
}
