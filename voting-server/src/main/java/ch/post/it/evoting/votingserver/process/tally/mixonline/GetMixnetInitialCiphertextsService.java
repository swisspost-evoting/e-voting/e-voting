/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.process.tally.mixonline;

import static ch.post.it.evoting.evotinglibraries.domain.ControlComponentConstants.NODE_IDS;
import static ch.post.it.evoting.evotinglibraries.domain.validations.ControlComponentVotesHashPayloadValidation.validate;
import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Preconditions;

import ch.post.it.evoting.domain.tally.GetMixnetInitialCiphertextsRequestPayload;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.ControlComponentVotesHashPayload;
import ch.post.it.evoting.votingserver.messaging.MessageHandler;
import ch.post.it.evoting.votingserver.messaging.Serializer;

@Service
public class GetMixnetInitialCiphertextsService {

	private static final Logger LOGGER = LoggerFactory.getLogger(GetMixnetInitialCiphertextsService.class);
	private final Serializer serializer;
	private final MessageHandler messageHandler;
	private final MixnetPayloadService mixnetPayloadService;
	private final MixDecryptOnlineService mixDecryptOnlineService;

	public GetMixnetInitialCiphertextsService(
			final Serializer serializer,
			final MessageHandler messageHandler,
			final MixnetPayloadService mixnetPayloadService,
			final MixDecryptOnlineService mixDecryptOnlineService) {
		this.serializer = serializer;
		this.messageHandler = messageHandler;
		this.mixnetPayloadService = mixnetPayloadService;
		this.mixDecryptOnlineService = mixDecryptOnlineService;
	}

	public void onRequest(final String electionEventId, final String ballotBoxId) {
		validateUUID(electionEventId);
		validateUUID(ballotBoxId);

		LOGGER.info("Starting GetMixnetInitialCiphertexts requests. [electionEventId: {}, ballotBoxId: {}]", electionEventId, ballotBoxId);

		messageHandler.sendMessage(new GetMixnetInitialCiphertextsRequestPayload(electionEventId, ballotBoxId));
	}

	@Transactional
	public void onResponse(final String correlationId, final List<ControlComponentVotesHashPayload> controlComponentVotesHashPayloads) {
		checkNotNull(correlationId);

		final List<ControlComponentVotesHashPayload> controlComponentVotesHashPayloadsCopy = checkNotNull(controlComponentVotesHashPayloads).stream()
				.map(Preconditions::checkNotNull)
				.toList();

		checkArgument(controlComponentVotesHashPayloadsCopy.size() == NODE_IDS.size(),
				"The Control Component Votes Hash Payloads must have %s elements.",
				NODE_IDS.size());

		final ControlComponentVotesHashPayload controlComponentVotesHashPayload = controlComponentVotesHashPayloadsCopy.get(0);
		final String electionEventId = controlComponentVotesHashPayload.getElectionEventId();
		final String ballotBoxId = controlComponentVotesHashPayload.getBallotBoxId();

		validate(electionEventId, ballotBoxId, controlComponentVotesHashPayloadsCopy);

		controlComponentVotesHashPayloadsCopy.forEach(mixnetPayloadService::saveControlComponentVotesHashPayload);

		mixDecryptOnlineService.onRequest(electionEventId, ballotBoxId, controlComponentVotesHashPayloadsCopy);
	}

	public int extractNodeId(final ControlComponentVotesHashPayload controlComponentVotesHashPayload) {
		checkNotNull(controlComponentVotesHashPayload);

		return controlComponentVotesHashPayload.getNodeId();
	}

	public ControlComponentVotesHashPayload deserialize(final byte[] messageBytes) {
		checkNotNull(messageBytes);
		final byte[] messageBytesCopy = Arrays.copyOf(messageBytes, messageBytes.length);

		return serializer.deserialize(messageBytesCopy, ControlComponentVotesHashPayload.class);
	}

}
