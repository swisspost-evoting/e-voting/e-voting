/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.messaging;

import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import ch.post.it.evoting.domain.ContextIdExtractor;
import ch.post.it.evoting.domain.configuration.SetupComponentPublicKeysResponsePayload;
import ch.post.it.evoting.domain.configuration.setupvoting.LongVoteCastReturnCodesAllowListResponsePayload;
import ch.post.it.evoting.domain.configuration.setupvoting.SetupComponentLVCCAllowListPayload;
import ch.post.it.evoting.domain.tally.GetMixnetInitialCiphertextsRequestPayload;
import ch.post.it.evoting.domain.tally.MixDecryptOnlineRequestPayload;
import ch.post.it.evoting.domain.tally.MixDecryptOnlineResponsePayload;
import ch.post.it.evoting.domain.voting.confirmvote.ControlComponenthlVCCPayload;
import ch.post.it.evoting.domain.voting.confirmvote.ControlComponenthlVCCRequestPayload;
import ch.post.it.evoting.domain.voting.confirmvote.ControlComponentlVCCSharePayload;
import ch.post.it.evoting.domain.voting.confirmvote.VotingServerConfirmPayload;
import ch.post.it.evoting.domain.voting.sendvote.CombinedControlComponentPartialDecryptPayload;
import ch.post.it.evoting.domain.voting.sendvote.ControlComponentLCCSharePayload;
import ch.post.it.evoting.domain.voting.sendvote.ControlComponentPartialDecryptPayload;
import ch.post.it.evoting.domain.voting.sendvote.VotingServerEncryptedVotePayload;
import ch.post.it.evoting.evotinglibraries.domain.configuration.ControlComponentPublicKeysPayload;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.ControlComponentVotesHashPayload;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.ElectionEventContextPayload;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.SetupComponentPublicKeysPayload;
import ch.post.it.evoting.evotinglibraries.domain.returncodes.ControlComponentCodeSharesPayload;
import ch.post.it.evoting.evotinglibraries.domain.returncodes.SetupComponentVerificationDataPayload;
import ch.post.it.evoting.votingserver.process.SetupComponentPublicKeysService;
import ch.post.it.evoting.votingserver.process.configuration.setupvoting.EncryptedLongReturnCodeSharesService;
import ch.post.it.evoting.votingserver.process.configuration.setupvoting.KeyGenerationService;
import ch.post.it.evoting.votingserver.process.configuration.setupvoting.LongVoteCastReturnCodesAllowListService;
import ch.post.it.evoting.votingserver.process.tally.mixonline.GetMixnetInitialCiphertextsService;
import ch.post.it.evoting.votingserver.process.tally.mixonline.MixDecryptOnlineService;
import ch.post.it.evoting.votingserver.process.voting.confirmvote.VoteCastReturnCodeService;
import ch.post.it.evoting.votingserver.process.voting.sendvote.ChoiceReturnCodesService;

@Configuration
public class MessageHandlerConfiguration {

	private final KeyGenerationService keyGenerationService;
	private final MixDecryptOnlineService mixDecryptOnlineService;
	private final ChoiceReturnCodesService choiceReturnCodesService;
	private final VoteCastReturnCodeService voteCastReturnCodeService;
	private final SetupComponentPublicKeysService setupComponentPublicKeysService;
	private final GetMixnetInitialCiphertextsService getMixnetInitialCiphertextsService;
	private final EncryptedLongReturnCodeSharesService encryptedLongReturnCodeSharesService;
	private final LongVoteCastReturnCodesAllowListService longVoteCastReturnCodesAllowListService;

	public MessageHandlerConfiguration(
			final KeyGenerationService keyGenerationService,
			final MixDecryptOnlineService mixDecryptOnlineService,
			final ChoiceReturnCodesService choiceReturnCodesService,
			final VoteCastReturnCodeService voteCastReturnCodeService,
			final SetupComponentPublicKeysService setupComponentPublicKeysService,
			final GetMixnetInitialCiphertextsService getMixnetInitialCiphertextsService,
			final EncryptedLongReturnCodeSharesService encryptedLongReturnCodeSharesService,
			final LongVoteCastReturnCodesAllowListService longVoteCastReturnCodesAllowListService) {
		this.keyGenerationService = keyGenerationService;
		this.mixDecryptOnlineService = mixDecryptOnlineService;
		this.choiceReturnCodesService = choiceReturnCodesService;
		this.setupComponentPublicKeysService = setupComponentPublicKeysService;
		this.getMixnetInitialCiphertextsService = getMixnetInitialCiphertextsService;
		this.encryptedLongReturnCodeSharesService = encryptedLongReturnCodeSharesService;
		this.longVoteCastReturnCodesAllowListService = longVoteCastReturnCodesAllowListService;
		this.voteCastReturnCodeService = voteCastReturnCodeService;
	}

	@Bean
	public List<MessageHandler.Configuration<?, ?>> messageHandlerConfigurations() {
		return List.of(
				keyGenerationConfiguration(),
				controlComponentMixDecOnline(),
				genEncLongCodeSharesConfiguration(),
				setupComponentLVCCAllowListConfiguration(),
				controlComponentLCCSharePayloadConfiguration(),
				controlComponentPartialDecryptPayloadConfiguration(),
				setupComponentPublicKeysResponsePayloadConfiguration(),
				controlComponenthlVCCPayloadConfiguration(),
				controlComponentlVCCSharePayloadConfiguration(),
				getMixnetInitialCiphertextsConfiguration()
		);
	}

	private MessageHandler.Configuration<ControlComponenthlVCCRequestPayload, ControlComponentlVCCSharePayload> controlComponentlVCCSharePayloadConfiguration() {
		return new MessageHandler.Configuration<>(
				ControlComponenthlVCCRequestPayload.class,
				true,
				ContextIdExtractor::extract,
				ControlComponentlVCCSharePayload.class,
				voteCastReturnCodeService::extractNodeId,
				voteCastReturnCodeService::deserializeControlComponentLVCCSharePayload,
				voteCastReturnCodeService::onControlComponentlVCCSharePayloadResponse,
				true);
	}

	private MessageHandler.Configuration<VotingServerConfirmPayload, ControlComponenthlVCCPayload> controlComponenthlVCCPayloadConfiguration() {
		return new MessageHandler.Configuration<>(
				VotingServerConfirmPayload.class,
				true,
				ContextIdExtractor::extract,
				ControlComponenthlVCCPayload.class,
				voteCastReturnCodeService::extractNodeId,
				voteCastReturnCodeService::deserializeControlComponenthlVCCPayload,
				voteCastReturnCodeService::onControlComponenthlVCCPayload,
				true);
	}

	private MessageHandler.Configuration<SetupComponentPublicKeysPayload, SetupComponentPublicKeysResponsePayload> setupComponentPublicKeysResponsePayloadConfiguration() {
		return new MessageHandler.Configuration<>(
				SetupComponentPublicKeysPayload.class,
				true,
				ContextIdExtractor::extract,
				SetupComponentPublicKeysResponsePayload.class,
				setupComponentPublicKeysService::extractNodeId,
				setupComponentPublicKeysService::deserialize,
				setupComponentPublicKeysService::onResponse,
				true);
	}

	private MessageHandler.Configuration<CombinedControlComponentPartialDecryptPayload, ControlComponentLCCSharePayload> controlComponentLCCSharePayloadConfiguration() {
		return new MessageHandler.Configuration<>(
				CombinedControlComponentPartialDecryptPayload.class,
				true,
				ContextIdExtractor::extract,
				ControlComponentLCCSharePayload.class,
				choiceReturnCodesService::extractNodeId,
				choiceReturnCodesService::deserializeLCCSharePayload,
				choiceReturnCodesService::onLongChoiceReturnCodesSharesResponse,
				true);
	}

	private MessageHandler.Configuration<VotingServerEncryptedVotePayload, ControlComponentPartialDecryptPayload> controlComponentPartialDecryptPayloadConfiguration() {
		return new MessageHandler.Configuration<>(
				VotingServerEncryptedVotePayload.class,
				true,
				ContextIdExtractor::extract,
				ControlComponentPartialDecryptPayload.class,
				choiceReturnCodesService::extractNodeId,
				choiceReturnCodesService::deserializePartialDecryptPayload,
				choiceReturnCodesService::onPartiallyDecryptPccResponse,
				true);
	}

	private MessageHandler.Configuration<ElectionEventContextPayload, ControlComponentPublicKeysPayload> keyGenerationConfiguration() {
		return new MessageHandler.Configuration<>(
				ElectionEventContextPayload.class,
				true,
				ContextIdExtractor::extract,
				ControlComponentPublicKeysPayload.class,
				keyGenerationService::extractNodeId,
				keyGenerationService::deserialize,
				keyGenerationService::onResponse,
				true);
	}

	private MessageHandler.Configuration<MixDecryptOnlineRequestPayload, MixDecryptOnlineResponsePayload> controlComponentMixDecOnline() {
		return new MessageHandler.Configuration<>(
				MixDecryptOnlineRequestPayload.class,
				false,
				ContextIdExtractor::extract,
				MixDecryptOnlineResponsePayload.class,
				mixDecryptOnlineService::extractNodeId,
				mixDecryptOnlineService::deserialize,
				mixDecryptOnlineService::onResponse,
				false);
	}

	private MessageHandler.Configuration<SetupComponentVerificationDataPayload, ControlComponentCodeSharesPayload> genEncLongCodeSharesConfiguration() {
		return new MessageHandler.Configuration<>(
				SetupComponentVerificationDataPayload.class,
				true,
				ContextIdExtractor::extract,
				ControlComponentCodeSharesPayload.class,
				encryptedLongReturnCodeSharesService::extractNodeId,
				encryptedLongReturnCodeSharesService::deserialize,
				encryptedLongReturnCodeSharesService::onResponse,
				false);
	}

	private MessageHandler.Configuration<SetupComponentLVCCAllowListPayload, LongVoteCastReturnCodesAllowListResponsePayload> setupComponentLVCCAllowListConfiguration() {
		return new MessageHandler.Configuration<>(
				SetupComponentLVCCAllowListPayload.class,
				true,
				ContextIdExtractor::extract,
				LongVoteCastReturnCodesAllowListResponsePayload.class,
				longVoteCastReturnCodesAllowListService::extractNodeId,
				longVoteCastReturnCodesAllowListService::deserializePayload,
				longVoteCastReturnCodesAllowListService::onResponse,
				true);
	}

	private MessageHandler.Configuration<GetMixnetInitialCiphertextsRequestPayload, ControlComponentVotesHashPayload> getMixnetInitialCiphertextsConfiguration() {
		return new MessageHandler.Configuration<>(
				GetMixnetInitialCiphertextsRequestPayload.class,
				true,
				ContextIdExtractor::extract,
				ControlComponentVotesHashPayload.class,
				getMixnetInitialCiphertextsService::extractNodeId,
				getMixnetInitialCiphertextsService::deserialize,
				getMixnetInitialCiphertextsService::onResponse,
				true);
	}
}
