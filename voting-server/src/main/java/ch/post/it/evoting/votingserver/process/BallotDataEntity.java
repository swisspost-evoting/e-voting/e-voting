/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.process;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import java.nio.charset.StandardCharsets;

import jakarta.persistence.Convert;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Version;

import ch.post.it.evoting.evotinglibraries.domain.election.Ballot;

@Entity
@Table(name = "BALLOT_DATA")
public class BallotDataEntity {

	@Id
	private String ballotId;

	@ManyToOne
	@JoinColumn(name = "ELECTION_EVENT_FK_ID", referencedColumnName = "ELECTION_EVENT_ID")
	private ElectionEventEntity electionEventEntity;

	@Convert(converter = BallotConverter.class)
	private Ballot ballot;

	private byte[] ballotTextsJson;

	@Version
	private Integer changeControlId;

	public BallotDataEntity() {
	}

	public BallotDataEntity(final String ballotId, final ElectionEventEntity electionEventEntity, final Ballot ballot, final String ballotTextsJson) {
		this.ballotId = validateUUID(ballotId);
		this.electionEventEntity = checkNotNull(electionEventEntity);
		this.ballot = checkNotNull(ballot);
		this.ballotTextsJson = checkNotNull(ballotTextsJson).getBytes(StandardCharsets.UTF_8);
	}

	public String getBallotId() {
		return ballotId;
	}

	public ElectionEventEntity getElectionEventEntity() {
		return electionEventEntity;
	}

	public Ballot getBallot() {
		return ballot;
	}

	public String getBallotTextsJson() {
		return new String(ballotTextsJson, StandardCharsets.UTF_8);
	}

	public Integer getChangeControlId() {
		return changeControlId;
	}

}
