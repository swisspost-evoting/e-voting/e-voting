/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.process;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import ch.post.it.evoting.evotinglibraries.domain.ConversionUtils;
import ch.post.it.evoting.evotinglibraries.domain.signature.Alias;

@Repository
public class KeystoreRepository {

	private final String keystoreLocation;
	private final String keystorePasswordLocation;
	private final Alias alias;

	public KeystoreRepository(
			@Value("${direct.trust.keystore.location}")
			final String keystoreLocation,
			@Value("${direct.trust.keystore.password.location}")
			final String keystorePasswordLocation) {
		this.keystoreLocation = keystoreLocation;
		this.keystorePasswordLocation = keystorePasswordLocation;
		this.alias = Alias.VOTING_SERVER;
	}

	/**
	 * @return the input stream containing the keystore.
	 * @throws IOException if an I/O error occurs during the creation of the input stream.
	 */
	public InputStream getKeyStore() throws IOException {
		return Files.newInputStream(Paths.get(keystoreLocation));
	}

	/**
	 * @return a new {@code char} array containing the keystore password characters.
	 * @throws IOException if an I/O error occurs reading the keystore password.
	 */
	public char[] getKeystorePassword() throws IOException {
		final byte[] passwordBytes = Files.readAllBytes(Paths.get(keystorePasswordLocation));
		final char[] passwordChars = ConversionUtils.byteArrayToCharArray(passwordBytes);
		Arrays.fill(passwordBytes, (byte) 0);

		return passwordChars;
	}

	/**
	 * @return the keystore alias {@link Alias#VOTING_SERVER}.
	 */
	public Alias getKeystoreAlias() {
		return alias;
	}
}
