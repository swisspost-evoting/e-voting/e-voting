/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.process.configuration.setupvoting;

import static ch.post.it.evoting.evotinglibraries.domain.ControlComponentConstants.NODE_IDS;
import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Preconditions;
import com.google.common.collect.MoreCollectors;

import ch.post.it.evoting.domain.configuration.setupvoting.ComputingStatus;
import ch.post.it.evoting.evotinglibraries.domain.returncodes.ControlComponentCodeSharesPayload;
import ch.post.it.evoting.evotinglibraries.domain.returncodes.SetupComponentVerificationDataPayload;
import ch.post.it.evoting.votingserver.idempotence.IdempotenceContext;
import ch.post.it.evoting.votingserver.idempotence.IdempotenceService;
import ch.post.it.evoting.votingserver.messaging.MessageHandler;
import ch.post.it.evoting.votingserver.messaging.Serializer;

@Service
public class EncryptedLongReturnCodeSharesService {

	private static final Logger LOGGER = LoggerFactory.getLogger(EncryptedLongReturnCodeSharesService.class);

	private final MessageHandler messageHandler;
	private final IdempotenceService<IdempotenceContext> idempotenceService;
	private final EncLongCodeShareRepository encLongCodeShareRepository;
	private final QueuedComputeChunkIdsService queuedComputeChunkIdsService;
	private final Serializer serializer;

	public EncryptedLongReturnCodeSharesService(
			final MessageHandler messageHandler,
			final IdempotenceService<IdempotenceContext> idempotenceService,
			final EncLongCodeShareRepository encLongCodeShareRepository,
			final QueuedComputeChunkIdsService queuedComputeChunkIdsService,
			final Serializer serializer) {
		this.messageHandler = messageHandler;
		this.idempotenceService = idempotenceService;
		this.encLongCodeShareRepository = encLongCodeShareRepository;
		this.queuedComputeChunkIdsService = queuedComputeChunkIdsService;
		this.serializer = serializer;
	}

	public List<byte[]> getEncLongCodeShares(final String electionEventId, final String verificationCardSetId,
			final int chunkId) {
		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);
		checkArgument(chunkId >= 0);

		final List<EncLongCodeShareEntity> encLongCodeShareEntities = encLongCodeShareRepository.findByElectionEventIdAndVerificationCardSetIdAndChunkId(
				electionEventId, verificationCardSetId, chunkId);

		final int numberOfEncLongCodeShares = encLongCodeShareEntities.size();
		final int numberOfControlComponents = NODE_IDS.size();

		if (numberOfEncLongCodeShares % numberOfControlComponents != 0) {
			throw new IllegalStateException(String.format(
					"The number of enc long code shares doesn't match the number of nodes. [numberOfEncLongCodeShares: %s, numberOfControlComponents: %s]",
					numberOfEncLongCodeShares, numberOfControlComponents));
		}

		return encLongCodeShareEntities.stream()
				.map(EncLongCodeShareEntity::getEncLongCodeShare)
				.toList();
	}

	@Transactional
	public ComputingStatus getEncLongCodeSharesComputingStatus(final String electionEventId, final String verificationCardSetId,
			final int chunkCount) {
		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);
		checkArgument(chunkCount >= 0);

		final int expectedCount = NODE_IDS.size() * chunkCount;
		final long actualCount = encLongCodeShareRepository.countByElectionEventIdAndVerificationCardSetId(electionEventId, verificationCardSetId);

		LOGGER.debug("Asked for computing status. [electionEventId: {}, verificationCardSetId: {}, expectedCount: {}, actualCount: {}]",
				electionEventId, verificationCardSetId, expectedCount, actualCount);

		if (actualCount < expectedCount) {
			return ComputingStatus.COMPUTING;
		} else if (actualCount == expectedCount) {
			return ComputingStatus.COMPUTED;
		} else {
			return ComputingStatus.COMPUTING_ERROR;
		}
	}

	@Transactional
	public void saveControlComponentCodeSharesPayload(final ControlComponentCodeSharesPayload controlComponentCodeSharesPayload) {
		checkNotNull(controlComponentCodeSharesPayload);

		final String electionEventId = controlComponentCodeSharesPayload.getElectionEventId();
		final String verificationCardSetId = controlComponentCodeSharesPayload.getVerificationCardSetId();
		final int chunkId = controlComponentCodeSharesPayload.getChunkId();
		final int nodeId = controlComponentCodeSharesPayload.getNodeId();

		final byte[] encLongCodeShare = serializer.serialize(controlComponentCodeSharesPayload);

		final EncLongCodeShareEntity encLongCodeShareEntity = new EncLongCodeShareEntity(electionEventId, verificationCardSetId, chunkId, nodeId,
				encLongCodeShare);
		encLongCodeShareRepository.save(encLongCodeShareEntity);

		LOGGER.debug("Saved enc long code share entity. [electionEventId: {}, verificationCardSetId: {}, chunkId: {}, nodeId: {}]",
				electionEventId, verificationCardSetId, chunkId, nodeId);
	}

	@Transactional
	public void onRequest(final SetupComponentVerificationDataPayload setupComponentVerificationDataPayload) {
		checkNotNull(setupComponentVerificationDataPayload);

		final String electionEventId = setupComponentVerificationDataPayload.getElectionEventId();
		final String verificationCardSetId = setupComponentVerificationDataPayload.getVerificationCardSetId();
		final int chunkId = setupComponentVerificationDataPayload.getChunkId();

		idempotenceService.execute(IdempotenceContext.COMPUTE_CHUNK, String.format("%s-%s-%s", electionEventId, verificationCardSetId, chunkId),
				() -> {
					queuedComputeChunkIdsService.saveQueuedComputeChunkId(electionEventId, verificationCardSetId, chunkId);
					messageHandler.sendMessage(setupComponentVerificationDataPayload);
				});
	}

	@Transactional
	public void onResponse(final String correlationId, final List<ControlComponentCodeSharesPayload> controlComponentCodeSharesPayloads) {
		checkNotNull(correlationId);
		checkNotNull(controlComponentCodeSharesPayloads);
		final ControlComponentCodeSharesPayload controlComponentCodeSharesPayload = controlComponentCodeSharesPayloads.stream()
				.map(Preconditions::checkNotNull).collect(MoreCollectors.onlyElement());

		saveControlComponentCodeSharesPayload(controlComponentCodeSharesPayload);

		final String contextId = getContextId(controlComponentCodeSharesPayload);

		LOGGER.info("Received and saved EncLongCodeShares calculation response. [contextId: {}, correlationId: {}]", contextId, correlationId);
	}

	public int extractNodeId(final ControlComponentCodeSharesPayload controlComponentCodeSharesPayload) {
		checkNotNull(controlComponentCodeSharesPayload);

		return controlComponentCodeSharesPayload.getNodeId();
	}

	public ControlComponentCodeSharesPayload deserialize(final byte[] messageBytes) {
		checkNotNull(messageBytes);
		final byte[] messageBytesCopy = Arrays.copyOf(messageBytes, messageBytes.length);

		return serializer.deserialize(messageBytesCopy, ControlComponentCodeSharesPayload.class);
	}

	private String getContextId(final ControlComponentCodeSharesPayload controlComponentCodeSharesPayload) {
		checkNotNull(controlComponentCodeSharesPayload);

		final String electionEventId = controlComponentCodeSharesPayload.getElectionEventId();
		final String verificationCardSetId = controlComponentCodeSharesPayload.getVerificationCardSetId();
		final int chunkId = controlComponentCodeSharesPayload.getChunkId();

		return String.join("-", Arrays.asList(electionEventId, verificationCardSetId, String.valueOf(chunkId)));
	}
}
