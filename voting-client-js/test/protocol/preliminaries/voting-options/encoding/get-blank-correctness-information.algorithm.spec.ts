/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */

import {NullPointerError} from "crypto-primitives-ts/lib/esm/error/null_pointer_error";
import {PrimitivesParams} from "../../../../../src/domain/primitives-params.types";
import {parsePrimitivesParams} from "../../../../../src/domain/primitives-params-parser";
import {
	getBlankCorrectnessInformation
} from "../../../../../src/protocol/preliminaries/voting-options/encoding/get-blank-correctness-information.algorithm";

import authenticateVoterResponseJson from "../../../../tools/data/authenticate-voter-response.json";

describe("GetBlankCorrectnessInformation algorithm", (): void => {

	const primitivesParams: PrimitivesParams = parsePrimitivesParams(
		authenticateVoterResponseJson.votingClientPublicKeys,
		authenticateVoterResponseJson.primesMappingTable
	);

	describe("should return", (): void => {

		test("the correct number of blank correctness information", (): void => {
			const blankCorrectnessInformationList: string[] = getBlankCorrectnessInformation(primitivesParams.primesMappingTable);
			const numberOfBlankOptions = 12;

			expect(blankCorrectnessInformationList.length).toEqual(numberOfBlankOptions);
		});

	});

	describe("should throw", (): void => {

		test("when primesMappingTable is null", (): void => {
			expect(() => getBlankCorrectnessInformation(null)).toThrow(NullPointerError);
		});

	});

});
