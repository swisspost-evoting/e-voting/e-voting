/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */

import {GqGroup} from "crypto-primitives-ts/lib/esm/math/gq_group";
import {ZqGroup} from "crypto-primitives-ts/lib/esm/math/zq_group";
import {ZqElement} from "crypto-primitives-ts/lib/esm/math/zq_element";
import {getGqGroup} from "../../../tools/data/group-test-data";
import {PrimitivesParams} from "../../../../src/domain/primitives-params.types";
import {LATIN_ALPHABET} from "../../../../src/domain/latin-alphabet";
import {ImmutableBigInteger} from "crypto-primitives-ts/lib/esm/immutable_big_integer";
import {parsePrimitivesParams} from "../../../../src/domain/primitives-params-parser";
import {checkExpLength, writeInToInteger} from "../../../../src/protocol/preliminaries/write-ins/write-in-to-integer.algorithm";

import realValuesJson from "../../../tools/data/write-in-to-integer.json";
import authenticateVoterResponseJson from "../../../tools/data/authenticate-voter-response.json";
import {IllegalArgumentError} from "crypto-primitives-ts/lib/esm/error/illegal_argument_error";

describe("WriteIn to integer algorithm", (): void => {

	describe("with real values", (): void => {

		const args = [];
		const parametersList = JSON.parse(JSON.stringify(realValuesJson));
		parametersList.forEach(testParameters => {

			// Context.
			const p: ImmutableBigInteger = readValue(testParameters.context.p);
			const q: ImmutableBigInteger = readValue(testParameters.context.q);
			const g: ImmutableBigInteger = readValue(testParameters.context.g);

			const gqGroup: GqGroup = new GqGroup(p, q, g);
			const zqGroup: ZqGroup = new ZqGroup(q);

			// Parse input parameter.
			const s = testParameters.input.s;

			// Parse output parameters.
			let output: ZqElement, error: string;
			if (testParameters.output.output) {
				output = ZqElement.create(readValue(testParameters.output.output), zqGroup);
			} else {
				error = testParameters.output.error;
			}

			args.push({
				encryptionGroup: gqGroup,
				s: s,
				output: output,
				error: error,
				description: testParameters.description
			});
		});

		args.forEach((arg): void => {
			test(arg.description, (): void => {
				if (arg.output) {
					const actual: ZqElement = writeInToInteger({encryptionGroup: arg.encryptionGroup}, arg.s);
					expect(arg.output.equals(actual)).toBe(true);
				} else {
					expect(() => writeInToInteger({encryptionGroup: arg.encryptionGroup}, arg.s))
						.toThrow(new IllegalArgumentError(arg.error));
				}
			});
		});

	});
});

describe("checkExpLength method", (): void => {
	const primitivesParams: PrimitivesParams = parsePrimitivesParams(
		authenticateVoterResponseJson.votingClientPublicKeys,
		authenticateVoterResponseJson.primesMappingTable
	);
	const encryptionGroup: GqGroup = primitivesParams.encryptionGroup;
	const a_int: number = LATIN_ALPHABET.length;
	const s_length_limit: number = Math.floor(encryptionGroup.q.bitLength() / Math.log2(a_int));

	test("should return true with exponential result smaller than q", function (): void {
		const a: ImmutableBigInteger = ImmutableBigInteger.fromNumber(a_int);
		const s_length: ImmutableBigInteger = ImmutableBigInteger.fromNumber(s_length_limit - 1);

		const actual: boolean = checkExpLength(a, s_length, encryptionGroup);

		expect(actual).toBe(true);
	});

	test("should return false with exponential result bigger than q", function (): void {
		const a: ImmutableBigInteger = ImmutableBigInteger.fromNumber(a_int);
		const s_length: ImmutableBigInteger = ImmutableBigInteger.fromNumber(s_length_limit + 1);

		const actual: boolean = checkExpLength(a, s_length, encryptionGroup);

		expect(actual).toBe(false);
	});

	test("should return a result with exponential result equal to q", function (): void {
		for (let i: number = 0; i < 100; i++) {
			const smallGroup: GqGroup = getGqGroup();
			const q_bitLength: number = smallGroup.q.bitLength();
			const a: ImmutableBigInteger = ImmutableBigInteger.fromNumber(2 ** q_bitLength);
			const s_length: ImmutableBigInteger = ImmutableBigInteger.fromNumber(1);

			const actual: boolean = checkExpLength(a, s_length, smallGroup);

			expect(actual).toBe(false);
		}
	});
});

function readValue(value: string): ImmutableBigInteger {
	return ImmutableBigInteger.fromString(value.substring(2), 16);
}