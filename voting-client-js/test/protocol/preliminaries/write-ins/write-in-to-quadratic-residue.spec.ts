/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */

import {GqGroup} from "crypto-primitives-ts/lib/esm/math/gq_group";
import {GqElement} from "crypto-primitives-ts/lib/esm/math/gq_element";
import {ImmutableBigInteger} from "crypto-primitives-ts/lib/esm/immutable_big_integer";
import {IllegalArgumentError} from "crypto-primitives-ts/lib/esm/error/illegal_argument_error";
import {writeInToQuadraticResidue} from "../../../../src/protocol/preliminaries/write-ins/write-in-to-quadratic-residue.algorithm";

import realValuesJson from "../../../tools/data/write-in-to-quadratic-residue.json";

describe("WriteIn to quadratic residue algorithm", (): void => {

	const args = [];
	const parametersList = JSON.parse(JSON.stringify(realValuesJson));
	parametersList.forEach(testParameters => {

		// Context.
		const p: ImmutableBigInteger = readValue(testParameters.context.p);
		const q: ImmutableBigInteger = readValue(testParameters.context.q);
		const g: ImmutableBigInteger = readValue(testParameters.context.g);

		const gqGroup: GqGroup = new GqGroup(p, q, g);

		// Parse input parameter.
		const s = testParameters.input.s;

		// Parse output parameters.
		let output: GqElement, error: string;
		if (testParameters.output.output) {
			output = GqElement.fromValue(readValue(testParameters.output.output), gqGroup);
		} else {
			error = testParameters.output.error;
		}

		args.push({
			encryptionGroup: gqGroup,
			s: s,
			output: output,
			error: error,
			description: testParameters.description
		});
	});

	args.forEach((arg): void => {
		test(arg.description, (): void => {
			if (arg.output) {
				const actual: GqElement = writeInToQuadraticResidue({encryptionGroup: arg.encryptionGroup}, arg.s);
				expect(arg.output.equals(actual)).toBe(true);
			} else {
				expect(() => writeInToQuadraticResidue({encryptionGroup: arg.encryptionGroup}, arg.s))
					.toThrow(new IllegalArgumentError(arg.error));
			}
		});
	});
});

function readValue(value: string): ImmutableBigInteger {
	return ImmutableBigInteger.fromString(value.substring(2), 16);
}

