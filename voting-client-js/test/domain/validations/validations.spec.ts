/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */

/* jshint maxlen: 6666 */

import {NullPointerError} from "crypto-primitives-ts/lib/cjs/error/null_pointer_error";
import {IllegalArgumentError} from "crypto-primitives-ts/lib/cjs/error/illegal_argument_error";
import {FailedValidationError} from "../../../src/domain/validations/failed-validation-error";
import {
	validateActualVotingOptions,
	validateBallotCastingKey,
	validateBase64String,
	validateExtendedAuthenticationFactor,
	validateNonBlankUCS,
	validateSemanticInformation,
	validateUUID,
	validateXsToken
} from "../../../src/domain/validations/validations";

import authenticateVoterResponseJson from "../../tools/data/authenticate-voter-response.json";

describe('Validations methods', function (): void {
	describe('validateUUID', function (): void {
		test('with valid argument should validate and return a UUID', function (): void {
			expect(() => validateUUID(authenticateVoterResponseJson.voterAuthenticationData.electionEventId)).not.toThrow();
		});

		test('with null argument should throw a NullPointerError', function (): void {
			expect(() => validateUUID(null)).toThrow(new NullPointerError());
		});
	})

	describe('validateBase64String', function (): void {
		test('with null argument should throw NullPointerError', function (): void {
			expect(() => validateBase64String(null)).toThrow(new NullPointerError());
		});

		test('with argument of invalid size should throw an IllegalArgumentError', function (): void {
			expect(() => validateBase64String("Aw=")).toThrow(new IllegalArgumentError());
		});

		test('with argument not in alphabet should throw a FailedValidationError', function (): void {
			expect(() => validateBase64String("Awç="))
				.toThrow(FailedValidationError);
		});

		test('with valid argument should validate and return a base64 string', function (): void {
			expect(() => validateBase64String(authenticateVoterResponseJson.verificationCardKeystore.verificationCardKeystore)).not.toThrow();
		});
	});


	describe('validateExtendedAuthenticationFactor', function (): void {
		test('with null argument should throw a NullPointerError', function (): void {
			expect(() => validateExtendedAuthenticationFactor(null)).toThrow(new NullPointerError());
		});

		test('with argument not in alphabet should throw a FailedValidationError', function (): void {
			expect(() => validateExtendedAuthenticationFactor("040494")).toThrow(FailedValidationError);
		});

		test('with valid extended authentication factor should validate and return', function (): void {
			const shortFactor = '1994';
			expect(validateExtendedAuthenticationFactor(shortFactor)).toBe(shortFactor);
			const longFactor = '04041994';
			expect(validateExtendedAuthenticationFactor(longFactor)).toBe(longFactor);
		});
	});

	describe('validateActualVotingOptions', function (): void {
		test('should fail with invalid XML xs:token actual voting option', function (): void {
			const invalidXMLTokens = ['aposidvnbq13458zœ¶@¼←“þ“ ¢@]œ“→”@µ€ĸ@{þ', ' spacestart', 'space between', 'spaceend ', 'pk23]', 'asdacà!32', ' a p2o3m', '<xyz>'];

			invalidXMLTokens.forEach(invalidXMLToken => {
				expect(() => validateActualVotingOptions([invalidXMLToken]))
					.toThrow(FailedValidationError);
			});
		});

		test('should accept actual voting option when valid XML xs:token', function (): void {
			const validXMLTokens = ['123-456', 'abc123', '789xyz', 'xyz', 'a_token', 'another-token', 'another|token'];

			validXMLTokens.forEach(validXMLToken => {
				expect(() => validateActualVotingOptions([validXMLToken]))
					.not.toThrow();
			});
		});
	});

	describe('validateSemanticInformation', function (): void {
		test('should fail with blank semantic information', function (): void {
			expect(() => validateSemanticInformation(' '))
				.toThrow(new IllegalArgumentError('String to validate must not be blank.'));
		});

		test('should fail with invalid prefix semantic information', function (): void {
			const invalidPrefix = 'NON__BLANK|First|Jack|JJ|1956-02-02';

			expect(() => validateSemanticInformation(invalidPrefix))
				.toThrow(new IllegalArgumentError('The semantic information prefix is not valid.'));
		});

		test('should accept semantic information when valid UTF8 and prefix', function (): void {
			const validSemanticInformations = ['NON_BLANK|Vier|Kandidat|Dagmar|1968-01-01', 'BLANK|EMPTY_CANDIDATE_POSITION-3', 'WRITE_IN|WRITE_IN_POSITION-3'];

			validSemanticInformations.forEach(validSemanticInformation => {
				expect(() => validateSemanticInformation(validSemanticInformation))
					.not.toThrow();
			});
		});
	});

	describe('validateBallotCastingKey', function () {
		test('with null argument should throw a NullPointerError', () => {
			expect(() => validateBallotCastingKey(null)).toThrow(new NullPointerError());
		});

		test('with ballot casting key wrong size should throw an IllegalArgumentError', () => {
			expect(() => validateBallotCastingKey('12345678')).toThrow(new IllegalArgumentError("The ballot casting key must have the correct size"));
			expect(() => validateBallotCastingKey('1234567890')).toThrow(new IllegalArgumentError("The ballot casting key must have the correct size"));
		});

		test('with non numerical values should throw an IllegalArgumentError', function (): void {
			expect(() => validateBallotCastingKey('1234P6789')).toThrow(new IllegalArgumentError("The ballot casting key must be a numeric value"));
		});

		test(' with all zero string should throw an IllegalArgumentError', function (): void {
			expect(() => validateBallotCastingKey('000000000')).toThrow(new IllegalArgumentError("The ballot casting key must contain at least one non-zero element"));
		});
	});

	describe('validateNonBlankUCS', function (): void {
		test('with null argument should throw a NullPointerError', function (): void {
			expect(() => validateNonBlankUCS(null)).toThrow(new NullPointerError());
		});

		test('should throw an IllegalArgumentError', function (): void {
			expect(() => validateNonBlankUCS('  ')).toThrow(new IllegalArgumentError("String to validate must not be blank."));
		});

		test('with valid input should return input', function (): void {
			const validInput = 'Valid input with (€&%)';
			expect(validateNonBlankUCS(validInput)).toBe(validInput);
		});
	});

	describe('validateXsToken', function (): void {
		test('with null argument should throw a NullPointerError', function (): void {
			expect(() => validateXsToken(null)).toThrow(new NullPointerError());
		});

		test('with an invalid token should throw a FailedValidationError', function (): void {
			const invalidXMLTokens = ['aposidvnbq13458zœ¶@¼←“þ“ ¢@]œ“→”@µ€ĸ@{þ', ' spacestart', 'space between', 'spaceend ', 'pk23]', 'asdacà!32', ' a p2o3m', '<xyz>'];

			invalidXMLTokens.forEach(invalidXMLToken => {
				expect(() => validateXsToken(invalidXMLToken))
					.toThrow(FailedValidationError);
			});
		});

		test('with a valid xs:token should return that token', function (): void {
			const validXMLTokens = ['123-456', 'abc123', '789xyz', 'xyz', 'a_token', 'another-token'];

			validXMLTokens.forEach(validXMLToken => {
				expect(validateXsToken(validXMLToken)).toBe(validXMLToken);
			});
		});
	});

});
