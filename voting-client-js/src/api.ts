/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */

import {WorkerWrapper} from "./worker-wrapper";
import {SendVoteResponse} from "./domain/send-vote.types";
import {ConfirmVoteResponse} from "./domain/confirm-vote.types";
import {AuthenticateVoterResponse} from "./domain/authenticate-voter.types";

/**
 * Provides an API to the Voter-Portal to communicate with the Voting-Server for all the voting phases described in the protocol.
 */

// Expose the OvApi object on the global window object
export class OnlineVotingApi {
	private static workerWrapper: WorkerWrapper = WorkerWrapper.getInstance();

	/**
	 * Authenticates the voter to the Voting-Server.
	 *
	 * @param {string} startVotingKey - the start voting key of a voter.
	 * @param {string} extendedAuthenticationFactor - the extended authentication factor, such as a year of birth OR date of birth.
	 * @param {string} electionEventId - the related election event identifier.
	 * @param {string} lang - the supported language code.
	 * @param {string} identification - the identification type received from the Voter-Portal.
	 * @returns {Promise<AuthenticateVoterResponse>} the authenticateVoter response.
	 */
	static authenticateVoter(startVotingKey: string, extendedAuthenticationFactor: string, electionEventId: string, lang: string, identification: string): Promise<AuthenticateVoterResponse> {
		// @ts-ignore
		return this.workerWrapper.invoke("authenticateVoter", startVotingKey, extendedAuthenticationFactor, electionEventId, lang, identification);
	}

	/**
	 * Sends the vote to the Voting-Server.
	 *
	 * @param {string[]} selectedEncodedVotingOptions - the voter's selected encoded voting options.
	 * @param {string[]} selectedWriteIns - the voter's selected write-ins.
	 * @returns {Promise<SendVoteResponse>} - the sendVote response.
	 */
	static sendVote(selectedEncodedVotingOptions: string[], selectedWriteIns: string []): Promise<SendVoteResponse> {
		// @ts-ignore
		return this.workerWrapper.invoke("sendVote", selectedEncodedVotingOptions, selectedWriteIns);
	}

	/**
	 * Confirms the vote to the Voting-Server.
	 *
	 * @param {string} ballotCastingKey - the 'ballot casting key'.
	 * @returns {Promise<ConfirmVoteResponse>} - the confirmVote response.
	 */
	static confirmVote(ballotCastingKey: string): Promise<ConfirmVoteResponse> {
		// @ts-ignore
		return this.workerWrapper.invoke("confirmVote", ballotCastingKey);
	}

	/**
	 * Selects the i18n texts associated to the locale on the supplied ballot.
	 * @param {Ballot} ballot - the ballot object
	 * @param {string} lang - the supported language code.
	 * @returns {Promise<Ballot>} the translated ballot.
	 */
	static translateBallot(ballot: any, lang: string): Promise<any> {
		// @ts-ignore
		return this.workerWrapper.invoke("translateBallot", ballot, lang);
	}
}

(window as any).OvApi = OnlineVotingApi;
