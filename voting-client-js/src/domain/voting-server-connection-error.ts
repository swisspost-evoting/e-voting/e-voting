/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */

export class VotingServerConnectionError extends Error {
	private errorResponse: object;
	private cause: string;

	constructor(cause: string) {
		super("Voting Server connection error.");

		this.errorResponse = {errorStatus: 'CONNECTION_ERROR'};
		this.cause = cause;

		// Set the prototype explicitly.
		Object.setPrototypeOf(this, VotingServerConnectionError.prototype);
	}
}