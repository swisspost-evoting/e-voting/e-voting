/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */

import {parseLegacyBallot, translateLegacyBallot} from "./ballot-parser";

/**
 * Provides a typescript api for the ballot parser.
 */

/**
 * Translates a copy of the given ballot in the provided language.
 * @param {any} ballotToTranslate - the ballot to translate.
 * @param {string} lang - the supported language code.
 * @returns {Promise<any>} the translated ballot.
 */
export async function translateBallot(ballotToTranslate: any, lang: string): Promise<any> {
	return translateLegacyBallot(ballotToTranslate, lang);
}

/**
 * Parses the ballot ant its texts in the provided language.
 * @param {any} ballotJson - the ballot to parse
 * @param {any} ballotTextsJson - the ballotText to parse.
 * @param {string} lang - the supported language code.
 * @returns {Promise<any>} the translated ballot.
 */
export async function parseBallot(ballotJson: any, ballotTextsJson: any, lang: string): Promise<any> {
	return parseLegacyBallot(ballotJson, ballotTextsJson, lang);
}
