/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */

import {GqGroup} from "crypto-primitives-ts/lib/esm/math/gq_group";
import {GroupVector} from "crypto-primitives-ts/lib/esm/group_vector";
import {PrimeGqElement} from "crypto-primitives-ts/lib/esm/math/prime_gq_element";
import {PrimitivesParams} from "./primitives-params.types";
import {PrimesMappingTable} from "./election/primes-mapping-table";
import {getActualVotingOptions} from "../protocol/preliminaries/voting-options/encoding/get-actual-voting-options.algorithm";
import {getSemanticInformation} from "../protocol/preliminaries/voting-options/encoding/get-semantic-information.algorithm";
import {getEncodedVotingOptions} from "../protocol/preliminaries/voting-options/encoding/get-encoded-voting-options.algorithm";
import {PrimesMappingTableEntry} from "./election/primes-mapping-table-entry";
import {getCorrectnessInformation} from "../protocol/preliminaries/voting-options/encoding/get-correctness-information.algorithm";
import {ElGamalMultiRecipientPublicKey} from "crypto-primitives-ts/lib/esm/elgamal/elgamal_multi_recipient_public_key";
import {getBlankCorrectnessInformation} from "../protocol/preliminaries/voting-options/encoding/get-blank-correctness-information.algorithm";
import {getWriteInEncodedVotingOptions} from "../protocol/preliminaries/voting-options/encoding/get-write-in-encoded-voting-options.algorithm";
import {PrimesMappingTableRaw, VotingClientPublicKeys} from "./authenticate-voter.types";
import {deserializeElGamalMultiRecipientPublicKey, deserializeGqGroup} from "./primitives-deserializer";

/**
 * Computes and deserializes the primitives parameters from an {@link AuthenticateVoterResponsePayload}.
 *
 * @param {VotingClientPublicKeys} votingClientPublicKeys - the voting client public keys.
 * @param {PrimesMappingTableRaw} primesMappingTableRaw - the raw primes mapping table.
 *
 * @returns {PrimitivesParams}, the primitives params object.
 */
export function parsePrimitivesParams(votingClientPublicKeys: VotingClientPublicKeys, primesMappingTableRaw: PrimesMappingTableRaw): PrimitivesParams {

	// Encryption group, we assume the signature has been checked and verified
	const encryptionParameters = deserializeGqGroup(votingClientPublicKeys.encryptionParameters);

	// Election public key
	const electionPublicKey: ElGamalMultiRecipientPublicKey = deserializeElGamalMultiRecipientPublicKey(
		votingClientPublicKeys.electionPublicKey,
		encryptionParameters
	);

	// Choice return codes encryption public key
	const choiceReturnCodesEncryptionPublicKey: ElGamalMultiRecipientPublicKey = deserializeElGamalMultiRecipientPublicKey(
		votingClientPublicKeys.choiceReturnCodesEncryptionPublicKey,
		encryptionParameters
	);

	// Primes Mapping Table
	const primesMappingTableEntries: PrimesMappingTableEntry[] = primesMappingTableRaw.pTable.map(entry => new PrimesMappingTableEntry(
		entry.actualVotingOption,
		PrimeGqElement.fromValue(entry.encodedVotingOption, encryptionParameters),
		entry.semanticInformation,
		entry.correctnessInformation
	));
	const primesMappingTable: PrimesMappingTable = new PrimesMappingTable(GroupVector.from(primesMappingTableEntries));

	// Extract Primes Mapping Table information
	const encodedVotingOptions: GroupVector<PrimeGqElement, GqGroup> = getEncodedVotingOptions(primesMappingTable, []);
	const actualVotingOptions: string[] = getActualVotingOptions(primesMappingTable, GroupVector.of());
	const semanticInformation: string[] = getSemanticInformation(primesMappingTable);
	const blankCorrectnessInformation: string[] = getBlankCorrectnessInformation(primesMappingTable);
	const correctnessInformation: string[] = getCorrectnessInformation(primesMappingTable, []);
	const totalNumberOfWriteIns: number = getWriteInEncodedVotingOptions(primesMappingTable).size;

	return {
		electionPublicKey: electionPublicKey,
		choiceReturnCodesEncryptionPublicKey: choiceReturnCodesEncryptionPublicKey,
		encryptionGroup: encryptionParameters,
		encodedVotingOptions: encodedVotingOptions,
		actualVotingOptions: actualVotingOptions,
		semanticInformation: semanticInformation,
		correctnessInformationSelections: blankCorrectnessInformation,
		correctnessInformationVotingOptions: correctnessInformation,
		totalNumberOfWriteIns: totalNumberOfWriteIns,
		primesMappingTable: primesMappingTable
	};
}
