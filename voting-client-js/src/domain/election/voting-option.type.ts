/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
export enum VotingOptionType {
	NON_BLANK = "NON_BLANK",
	BLANK = "BLANK",
	WRITE_IN = "WRITE_IN"
}