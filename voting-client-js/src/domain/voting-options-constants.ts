/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
"use strict";

export const MAXIMUM_SUPPORTED_NUMBER_OF_VOTING_OPTIONS: number = 5000;
export const MAXIMUM_SUPPORTED_NUMBER_OF_SELECTIONS: number = 120;
export const MAXIMUM_SUPPORTED_NUMBER_OF_WRITE_INS: number = 30;
export const MAXIMUM_ACTUAL_VOTING_OPTION_LENGTH: number = 50;
export const CHARACTER_LENGTH_OF_START_VOTING_KEY: number = 24;
export const CHARACTER_LENGTH_OF_UNIQUE_IDENTIFIER: number = 32;
export const CHARACTER_LENGTH_OF_BALLOT_CASTING_KEY: number = 9;
export const MAXIMUM_WRITE_IN_OPTION_LENGTH: number = 400;

