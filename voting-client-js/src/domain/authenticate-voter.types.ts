/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */

import {AuthenticationChallenge} from "../protocol/voting-phase/authenticate-voter/get-authentication-challenge.types";

/**
 * @property {string} votingCardState - the voting card state.
 * @property {object} [ballot] - the parsed ballot.
 * @property {string[]} [choiceReturnCodes] - the short choice return codes.
 * @property {string} [voteCastReturnCode] - the short vote cast return code.
 */
export interface AuthenticateVoterResponse {
	votingCardState: string;
	ballot?: object;
	choiceReturnCodes?: string[];
	voteCastReturnCode?: string;
}

/**
 * @property {string} electionEventId - the election event id.
 * @property {AuthenticationChallenge} authenticationChallenge - the authentication challenge.
 */
export interface AuthenticateVoterRequestPayload {
	electionEventId: string;
	authenticationChallenge: AuthenticationChallenge;
}

/**
 * @property {string} electionEventId - the election event id.
 * @property {string} verificationCardSetId - the verification card set id.
 * @property {string} votingCardSetId - the voting card set id.
 * @property {string} ballotBoxId - the ballot box id.
 * @property {string} ballotId - the ballot id.
 * @property {string} verificationCardId - the verification card id.
 * @property {string} ballotBoxId - the voting card id.
 * @property {string} credentialId - the credential id.
 */
export interface VoterAuthenticationData {
	electionEventId: string;
	verificationCardSetId: string;
	votingCardSetId: string;
	ballotBoxId: string;
	ballotId: string;
	verificationCardId: string;
	votingCardId: string;
	credentialId: string;
}

/**
 * @property {string} verificationCardId - the verification card id.
 * @property {string} verificationCardKeystore - the verification card keystore representation.
 */
export interface VerificationCardKeystore {
	verificationCardId: string;
	verificationCardKeystore: string;
}

/**
 * @property {string} p - the serialized p.
 * @property {string} q - the serialized q.
 * @property {string} g - the serialized g.
 */
export interface EncryptionParameters {
	p: string;
	q: string;
	g: string;
}

/**
 * @property {EncryptionParameters} encryptionParameters - the serialized encryption parameters.
 * @property {string[]} electionPublicKey - the serialized election public key.
 * @property {string[]} choiceReturnCodesEncryptionPublicKey - the serialized choice return codes encryption public key.
 */
export interface VotingClientPublicKeys {
	encryptionParameters: EncryptionParameters;
	electionPublicKey: string[];
	choiceReturnCodesEncryptionPublicKey: string[];
}

/**
 * @property {object} [ballot] - the ballot of the voter.
 * @property {object} [ballotTexts] - the ballot translations.
 * @property {string[]} [shortChoiceReturnCodes] - the short choice return codes.
 * @property {string} [shortVoteCastReturnCode] - the short vote cast return code.
 */
export interface VoterMaterial {
	ballot?: object;
	ballotTexts?: string;
	shortChoiceReturnCodes?: string[];
	shortVoteCastReturnCode: string;
}


/**
 * @property {string} actualVotingOption - the actual voting option entry.
 * @property {number} encodedVotingOption - the encoded voting option entry (prime number).
 * @property {string} semanticInformation - the semantic information entry.
 */
export interface PrimesMappingTableEntryRaw {
	actualVotingOption: string;
	encodedVotingOption: number;
	semanticInformation: string;
	correctnessInformation: string;
}

/**
 * @property {PrimesMappingTableEntryRaw[]} pTable - the prime mapping table entries.
 */
export interface PrimesMappingTableRaw {
	pTable: PrimesMappingTableEntryRaw[];
}

/**
 * @property {string} verificationCardState - the verification card state.
 * @property {VoterAuthenticationData} voterAuthenticationData - the voter context ids.
 * @property {VerificationCardKeystore} verificationCardKeystore - the verification card keystore.
 * @property {VotingClientPublicKeys} votingClientPublicKeys - contains the public keys.
 * @property {VoterMaterial} voterMaterial - the voter material.
 */
export interface AuthenticateVoterResponsePayload {
	verificationCardState: string;
	voterAuthenticationData: VoterAuthenticationData;
	verificationCardKeystore: VerificationCardKeystore;
	votingClientPublicKeys: VotingClientPublicKeys;
	voterMaterial: VoterMaterial;
	primesMappingTable: PrimesMappingTableRaw;
}