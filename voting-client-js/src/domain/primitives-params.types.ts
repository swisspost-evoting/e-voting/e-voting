/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */

import {GqGroup} from "crypto-primitives-ts/lib/esm/math/gq_group";
import {GroupVector} from "crypto-primitives-ts/lib/esm/group_vector";
import {PrimeGqElement} from "crypto-primitives-ts/lib/esm/math/prime_gq_element";
import {PrimesMappingTable} from "./election/primes-mapping-table";
import {ElGamalMultiRecipientPublicKey} from "crypto-primitives-ts/lib/esm/elgamal/elgamal_multi_recipient_public_key";

/**
 * @property {ElGamalMultiRecipientPublicKey} electionPublicKey electionPublicKey, the election public key.
 * @property {ElGamalMultiRecipientPublicKey} choiceReturnCodesEncryptionPublicKey choiceReturnCodesEncryptionPublicKey, the CCR encryption public key.
 * @property {GqGroup} encryptionGroup encryptionGroup, the encryption group.
 * @property {GroupVector<PrimeGqElement>} encodedVotingOptions encodedVotingOptions, the ballot encoded voting options.
 * @property {string[]} actualVotingOptions actualVotingOptions, the ballot actual voting options.
 * @property {string[]} semanticInformation semanticInformation, the ballot semantic information.
 * @property {string[]} correctnessInformationSelections ciSelections, the correctness information for selections.
 * @property {string[]} correctnessInformationVotingOptions ciVotingOptions, the correctness information for voting options.
 * @property {number} totalNumberOfWriteIns totalNumberOfWriteIns, the total number write-ins.
 */
export interface PrimitivesParams {
	electionPublicKey: ElGamalMultiRecipientPublicKey;
	choiceReturnCodesEncryptionPublicKey: ElGamalMultiRecipientPublicKey;
	encryptionGroup: GqGroup;
	encodedVotingOptions: GroupVector<PrimeGqElement, GqGroup>;
	actualVotingOptions: string[];
	semanticInformation: string[];
	correctnessInformationSelections: string[];
	correctnessInformationVotingOptions: string[];
	totalNumberOfWriteIns: number;
	primesMappingTable: PrimesMappingTable
}