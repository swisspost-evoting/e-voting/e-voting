/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */

export class ExtendedAuthenticationFactor {
	constructor(
		public length: number,
	) {}
}
