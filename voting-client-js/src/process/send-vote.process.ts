/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */

import {Vote} from "../protocol/voting-phase/send-vote/create-vote.types";
import {GqGroup} from "crypto-primitives-ts/lib/esm/math/gq_group";
import {createVote} from "../protocol/voting-phase/send-vote/create-vote.algorithm";
import {GroupVector} from "crypto-primitives-ts/lib/esm/group_vector";
import {validateUUID} from "../domain/validations/validations";
import {PrimeGqElement} from "crypto-primitives-ts/lib/esm/math/prime_gq_element";
import {SessionService} from "../session.service";
import {PrimitivesParams} from "../domain/primitives-params.types";
import {VotingServerService} from "../voting-server.service";
import {getActualVotingOptions} from "../protocol/preliminaries/voting-options/encoding/get-actual-voting-options.algorithm";
import {AuthenticationChallenge} from "../protocol/voting-phase/authenticate-voter/get-authentication-challenge.types";
import {VoterAuthenticationData} from "../domain/authenticate-voter.types";
import {getAuthenticationChallenge} from "../protocol/voting-phase/authenticate-voter/get-authentication-challenge.algorithm";
import {SendVoteRequestPayload, SendVoteResponse, SendVoteResponsePayload} from "../domain/send-vote.types";
import {
	serializeElGamalCiphertext,
	serializeExponentiationProof,
	serializeGqGroup,
	serializePlaintextEqualityProof
} from "../domain/primitives-serializer";

export class SendVoteProcess {
	private sessionService: SessionService;
	private votingServerService: VotingServerService;

	constructor() {
		this.sessionService = SessionService.getInstance();
		this.votingServerService = new VotingServerService();
	}

	/**
	 * This process implements the sendVote phase of the protocol:
	 *  - Receives the SelectedEncodedVotingOptions and SelectedWriteIns from the Voter Portal.
	 *  - Calls the CreateVote algorithm.
	 *  - Calls the GetAuthenticationChallenge algorithm.
	 *  - Sends a SendVoteRequestPayload to the Voting Server.
	 *  - Returns a SendVoteResponse to the Voter Portal.
	 *
	 * @param {string[]} selectedEncodedVotingOptions - the voter's selected encoded voting options.
	 * @param {string[]} selectedWriteIns - the voter's selected write-ins.
	 *
	 * @returns {Promise<SendVoteResponse>}, the sendVote response expected by the Voter-Portal.
	 */
	public async sendVote(selectedEncodedVotingOptions: string[], selectedWriteIns: string[]): Promise<SendVoteResponse> {
		const primitivesParams: PrimitivesParams = this.sessionService.primitivesParams();
		const voterAuthenticationData: VoterAuthenticationData = this.sessionService.voterAuthenticationData();

		const selectedVotingOptions: string [] = this.convertToActualVotingOptions(selectedEncodedVotingOptions);

		// Call CreateVote algorithm
		const createVoteOutput: Vote = createVote(
			{
				encryptionGroup: primitivesParams.encryptionGroup,
				electionEventId: validateUUID(voterAuthenticationData.electionEventId),
				verificationCardSetId: validateUUID(voterAuthenticationData.verificationCardSetId),
				verificationCardId: validateUUID(voterAuthenticationData.verificationCardId),
				primesMappingTable: primitivesParams.primesMappingTable,
				electionPublicKey: primitivesParams.electionPublicKey,
				choiceReturnCodesEncryptionPublicKey: primitivesParams.choiceReturnCodesEncryptionPublicKey,
			},
			selectedVotingOptions,
			selectedWriteIns,
			this.sessionService.verificationCardSecretKey()
		);

		const extendedAuthenticationFactor : string = this.sessionService.extendedAuthenticationFactor();

		// Call GetAuthenticationChallenge algorithm
		const authenticationChallenge: AuthenticationChallenge = await getAuthenticationChallenge(
			{
				electionEventId: voterAuthenticationData.electionEventId,
				extendedAuthenticationFactorLength: extendedAuthenticationFactor.length
			},
			"sendVote",
			this.sessionService.startVotingKey(),
			extendedAuthenticationFactor
		);

		// Prepare and post payload to the Voting Server
		const sendVoteRequestPayload: SendVoteRequestPayload = this.prepareSendVotePayload(createVoteOutput, authenticationChallenge);
		const sendVoteResponsePayload: SendVoteResponsePayload = await this.votingServerService.sendVote(sendVoteRequestPayload);

		return {
			choiceReturnCodes: sendVoteResponsePayload.shortChoiceReturnCodes
		};
	}

	/**
	 * Converts the selected encoded voting options to the corresponding actual voting options.
	 *
	 * @param {string[]} selectedEncodedVotingOptions - the voter selected encoded voting options.
	 *
	 * @returns {string[]} - the list of actual voting options.
	 */
	private convertToActualVotingOptions(selectedEncodedVotingOptions: string[]): string[] {
		const primitivesParams: PrimitivesParams = this.sessionService.primitivesParams();

		const selectedEncodedVotingOptionList: PrimeGqElement[] = selectedEncodedVotingOptions.map(function (o) {
			// Avoid type coercion in the voter's selections
			const votersSelection: number = parseInt(o, 10);
			return PrimeGqElement.fromValue(votersSelection, primitivesParams.encryptionGroup);
		});

		const p_vector: GroupVector<PrimeGqElement, GqGroup> = GroupVector.from(selectedEncodedVotingOptionList);

		return getActualVotingOptions(primitivesParams.primesMappingTable, p_vector);
	}

	/**
	 * Prepares the sendVote payload expected by the Voting-Server.
	 *
	 * @param {Vote} createVoteOutput - the output of the createVote algorithm.
	 * @param {AuthenticationChallenge} authenticationChallenge - the authentication challenge.
	 *
	 * @returns {SendVoteRequestPayload} - the sendVote request payload expected by the Voting-Server.
	 */
	private prepareSendVotePayload(createVoteOutput: Vote, authenticationChallenge: AuthenticationChallenge): SendVoteRequestPayload {
		// Serialize vote elements for payload
		const serializedEncryptedVote: string = serializeElGamalCiphertext(createVoteOutput.encryptedVote);
		const serializedExponentiatedEncryptedVote: string = serializeElGamalCiphertext(createVoteOutput.exponentiatedEncryptedVote);
		const serializedEncryptedPartialChoiceReturnCodes: string = serializeElGamalCiphertext(createVoteOutput.encryptedPartialChoiceReturnCodes);
		const serializedExponentiationProof: string = serializeExponentiationProof(createVoteOutput.exponentiationProof);
		const serializedPlaintextEqualityProof: string = serializePlaintextEqualityProof(createVoteOutput.plaintextEqualityProof);

		const primitivesParams: PrimitivesParams = this.sessionService.primitivesParams();
		const serializedEncryptionGroup: string = serializeGqGroup(primitivesParams.encryptionGroup);
		const voterAuthenticationData: VoterAuthenticationData = this.sessionService.voterAuthenticationData();

		return {
			contextIds: {
				electionEventId: voterAuthenticationData.electionEventId,
				verificationCardSetId: voterAuthenticationData.verificationCardSetId,
				verificationCardId: voterAuthenticationData.verificationCardId
			},
			encryptionGroup: JSON.parse(serializedEncryptionGroup),
			encryptedVerifiableVote: {
				contextIds: {
					electionEventId: voterAuthenticationData.electionEventId,
					verificationCardSetId: voterAuthenticationData.verificationCardSetId,
					verificationCardId: voterAuthenticationData.verificationCardId
				},
				encryptedVote: JSON.parse(serializedEncryptedVote),
				exponentiatedEncryptedVote: JSON.parse(serializedExponentiatedEncryptedVote),
				encryptedPartialChoiceReturnCodes: JSON.parse(serializedEncryptedPartialChoiceReturnCodes),
				exponentiationProof: JSON.parse(serializedExponentiationProof),
				plaintextEqualityProof: JSON.parse(serializedPlaintextEqualityProof)
			},
			authenticationChallenge: authenticationChallenge
		};
	}
}
