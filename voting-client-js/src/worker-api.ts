/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */

import {SendVoteProcess} from "./process/send-vote.process";
import {translateBallot} from "./domain/model/ballot-parser-api";
import {SendVoteResponse} from "./domain/send-vote.types";
import {ConfirmVoteProcess} from "./process/confirm-vote.process";
import {ConfirmVoteResponse} from "./domain/confirm-vote.types";
import {AuthenticateVoterProcess} from "./process/authenticate-voter.process";
import {AuthenticateVoterResponse} from "./domain/authenticate-voter.types";

/**
 * Provides an api for the worker calls.
 */
class WorkerApi {
	public static authenticateVoter(startVotingKey: string, extendedAuthenticationFactor: string, electionEventId: string, lang: string, identification: string): Promise<AuthenticateVoterResponse> {
		const authenticateVoterProcess: AuthenticateVoterProcess = new AuthenticateVoterProcess();
		return authenticateVoterProcess.authenticateVoter(startVotingKey, extendedAuthenticationFactor, electionEventId, lang, identification);
	}

	public static sendVote(selectedEncodedVotingOptions: string[], voterWriteIns: string[]): Promise<SendVoteResponse> {
		const sendVoteProcess: SendVoteProcess = new SendVoteProcess();
		return sendVoteProcess.sendVote(selectedEncodedVotingOptions, voterWriteIns);
	}

	public static confirmVote(ballotCastingKey: string): Promise<ConfirmVoteResponse> {
		const confirmVoteProcess: ConfirmVoteProcess = new ConfirmVoteProcess();
		return confirmVoteProcess.confirmVote(ballotCastingKey);
	}

	public static translateBallot(ballot: any, lang: string): Promise<any> {
		return translateBallot(ballot, lang);
	}
}

/**
 * @typedef {object} WorkerRequestPayload.
 * @property {string} operation, the operation to invoke.
 * @property {Array} args, arguments for the invoked operation.
 */

/**
 * @typedef {object} MessageEvent.
 * @property {WorkerRequestPayload} data, the request from the api.
 */

/**
 * Handles a request from the api.
 * @param {MessageEvent} workerMessage - the message received from the worker.
 * @returns {Promise<unknown>} - operation response.
 */
self.onmessage = function handleRequestFromApi(workerMessage: MessageEvent): void {
	const {operation, args} = workerMessage.data;
	let response;
	try {
		response = WorkerApi[operation].apply(WorkerApi, Array.isArray(args) ? args : [args]);
	} catch (error) {
		throw new Error(`Error calling the Worker API. [operation: "${operation}, error:${error}]`);
	}

	// Handling only the result and error (no progress or pending).
	response.then(
		(promiseResult: any) => {
			self.postMessage({
				operation: operation,
				result: promiseResult
			});
		},
		(promiseError: any) => {
			// Prepare log error
			let loggedMessage: string;
			let loggedCause: string;
			if (!promiseError.cause) {
				loggedMessage = "Unexpected error.";
				loggedCause = (promiseError.message ? promiseError.message : promiseError.name);
			} else {
				loggedMessage = promiseError.message;
				loggedCause = promiseError.cause;
			}

			const consoleError: ConsoleError = {
				operation: operation,
				message: loggedMessage,
				cause: loggedCause
			};
			console.error("Error:", consoleError);

			// Bubble up the error
			self.postMessage({
				operation: operation,
				error: promiseError.errorResponse ? promiseError.errorResponse : {status: 0}
			});
		}
	);
};

interface ConsoleError {
	operation: string;
	message: string;
	cause: string;
}
