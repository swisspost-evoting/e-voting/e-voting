/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */

import {checkNotNull} from "crypto-primitives-ts/lib/esm/validation/preconditions";
import {PrimesMappingTable} from "../../../../domain/election/primes-mapping-table";
import {getBlankCorrectnessInformation} from "./get-blank-correctness-information.algorithm";

/**
 * Implements the GetPsi algorithm.
 *
 * @param {PrimesMappingTable} primesMappingTable - pTable, the primes mapping table of size n. Must be non-null.
 *
 * @returns {number} -  the number of selectable voting options.
 */
export function getPsi(primesMappingTable: PrimesMappingTable): number {

	// Context.
	const pTable: PrimesMappingTable = checkNotNull(primesMappingTable);

	// Operation
	const tau_hat_vector: string[] = getBlankCorrectnessInformation(pTable)

	return tau_hat_vector.length;
}

