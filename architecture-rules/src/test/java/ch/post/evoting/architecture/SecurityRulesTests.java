/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.evoting.architecture;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.fields;
import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

import java.util.List;

import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;

import ch.post.it.evoting.cryptoprimitives.math.PrimeGqElement;
import ch.post.it.evoting.evotinglibraries.domain.election.PrimesMappingTable;
import ch.post.it.evoting.evotinglibraries.domain.election.PrimesMappingTableEntry;

@AnalyzeClasses(packages = "ch.post.it.evoting")
public class SecurityRulesTests {

	@ArchTest
	static final ArchRule NO_CLASSES_SHOULD_CALL_TO_LOWER_CASE_WITHOUT_LOCALE = noClasses().should().callMethod(String.class, "toLowerCase");

	@ArchTest
	static final ArchRule NO_CLASSES_SHOULD_CALL_TO_UPPER_CASE_WITHOUT_LOCALE = noClasses().should().callMethod(String.class, "toUpperCase");

	@ArchTest
	static final ArchRule NON_STATIC_FIELDS_SHOULD_NOT_BE_PUBLIC = fields().that().areNotStatic().should().notBePublic();

	@ArchTest
	static final ArchRule TEST_CLASSES_SHOULD_USE_SETUP_ELECTION_UTILS_FOR_INSTANTIATING_PRIMES_MAPPING_TABLES = noClasses().that()
			.haveSimpleNameEndingWith("Test")
			.should().callMethod(PrimesMappingTable.class, "from", List.class)
			.orShould().callConstructor(PrimesMappingTableEntry.class, String.class, PrimeGqElement.class, String.class, String.class);
}
