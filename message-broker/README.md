# Note on the folders

The files in the folders node0 and node1 are used only for the configuration of the message-broker in the **development** environment.
They are **not** used in production.