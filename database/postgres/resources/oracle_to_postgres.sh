#!/bin/bash
#
# (c) Copyright 2024 Swiss Post Ltd.
#
set -o errexit

# Function to process each SQL file
migrate_sql_file() {
    local sql_file="$1"

    if [ "${IN_PLACE_MIGRATION}" == true ]; then
      sed -i "s/NUMBER(/NUMERIC(/g" "$sql_file"
      sed -i "s/VARCHAR2(1)/BOOL/g" "$sql_file"
      sed -i "s/VARCHAR2/VARCHAR/g" "$sql_file"
      sed -i "s/BLOB/BYTEA/g" "$sql_file"
      sed -i "s/ CHAR)/)/g" "$sql_file"
      sed -i "s/NOCYCLE/NO CYCLE/g" "$sql_file"
    else
      sed -b -e "s/NUMBER(/NUMERIC(/g" -e "s/VARCHAR2(1)/BOOL/g" -e "s/VARCHAR2/VARCHAR/g" -e "s/BLOB/BYTEA/g" -e "s/ CHAR)/)/g" -e "s/NOCYCLE/NO CYCLE/g" "$sql_file" > "${sql_file%.*}_postgres.sql"
    fi
}

load_argument() {
  while getopts d:i option; do
    case "${option}" in
      i)
        IN_PLACE_MIGRATION=true
        echo "Doing migration in place."
        ;;
      d)
        SEARCH_DIRECTORY=${OPTARG}
        ;;
      *)
        exit 1
        ;;
    esac
  done

  if [ -z "${SEARCH_DIRECTORY}" ]; then
    echo "Missing directory. Please provide one with -d option."
    exit 1
  fi
}

### Main
load_argument "$@"

# Find all .sql files in the specified directory and its subdirectories
sql_files=$(find "$SEARCH_DIRECTORY" -type f -name "*.sql")

processed_files=0

# Loop through each SQL file and apply the function
for sql_file in $sql_files; do
    migrate_sql_file "$sql_file"
    ((++processed_files))
done

echo "Migration complete. $processed_files file(s) processed."