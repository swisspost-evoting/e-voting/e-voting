# Database Module

The database module streamlines end-to-end testing by gathering different SQL files and creating a PostgreSQL or an Oracle database. 
In development, we use PostgreSQL. PostgreSQL is an open-source, object-relational database system that offers advanced features such as reliable transactions and concurrency without read locks. It supports a wide range of data types and is designed for scalability and flexibility, making it suitable for a variety of applications, from single machines to data warehouses or Web services with many concurrent users.
On the other hand, in production environments, we use Oracle databases, which are known for their high performance, scalability, and reliability. Oracle databases are enterprise-grade
databases that can handle large amounts of data and support complex transactions, making them ideal for mission-critical applications. It's important
to note that the passwords stored in this repository are only meant for testing and development purposes and are never used in a production
environment.

## Usage

Used in the [end-to-end](https://gitlab.com/swisspost-evoting/e-voting/evoting-e2e-dev/-/blob/master/README.md) repository.
