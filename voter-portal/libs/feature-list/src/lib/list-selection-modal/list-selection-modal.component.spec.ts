/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {UiComponentsModule} from '@vp/ui-components';
import {ElectionContest} from '@vp/util-types';
import {MockModule, MockProvider} from 'ng-mocks';
import {TranslateTestingModule} from 'ngx-translate-testing';

import {ListSelectionModalComponent} from './list-selection-modal.component';

describe('ListSelectionModalComponent', () => {
	let component: ListSelectionModalComponent;
	let fixture: ComponentFixture<ListSelectionModalComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [ListSelectionModalComponent],
			providers: [MockProvider(NgbActiveModal)],
			imports: [
				FormsModule,
				ReactiveFormsModule,
				TranslateTestingModule.withTranslations({}),
				MockModule(UiComponentsModule),
			],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(ListSelectionModalComponent);
		component = fixture.componentInstance;

		component.electionContest = {} as ElectionContest;

		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
