/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {Candidate, CandidateList} from '@vp/util-types';

@Component({
	selector: 'vp-list-selector',
	templateUrl: './list-selector.component.html',
})
export class ListSelectorComponent implements OnInit {
	@Input() list!: CandidateList;
	candidates: Candidate[] = [];
	areDetailsCollapsed = true;

	constructor(private readonly activeModal: NgbActiveModal) {}

	ngOnInit() {
		this.list.candidates.forEach((candidate) => {
			const positions =
				candidate.details.candidateType_positionOnList?.split(',') ?? [];
			positions.forEach((position) => {
				this.candidates[parseInt(position) - 1] = candidate;
			});
		});
	}

	selectList() {
		this.activeModal.close(this.list);
	}
}
