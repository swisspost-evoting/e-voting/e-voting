/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
export const votingCardStar =
	'M12,18.87L5.22,22.45,6.5,14.89,1,9.52l7.61-1.1L12,1.55l3.39,6.88L23,9.52l-5.5,5.36,1.28,7.56Z';
