/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {votingCardDiamond} from './voting-card-diamond';
import {votingCardPentagon} from './voting-card-pentagon';
import {votingCardStar} from './voting-card-star';
import {votingCardTriangle} from './voting-card-triangle';

export default {
	'voting-card-triangle': votingCardTriangle,
	'voting-card-diamond': votingCardDiamond,
	'voting-card-pentagon': votingCardPentagon,
	'voting-card-star': votingCardStar,
};
