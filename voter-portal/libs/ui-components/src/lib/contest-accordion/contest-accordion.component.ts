/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {Component, Input} from '@angular/core';
import {Nullable} from '@vp/util-types';

@Component({
	selector: 'vp-contest-accordion',
	templateUrl: './contest-accordion.component.html',
})
export class ContestAccordionComponent {
	@Input() title: Nullable<string>;
	@Input() subtitle: Nullable<string>;
	@Input() cardClass: Nullable<string>;

	isExpanded = true;
}
