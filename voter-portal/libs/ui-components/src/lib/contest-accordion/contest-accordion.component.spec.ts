/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {Component, DebugElement} from '@angular/core';
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {By} from '@angular/platform-browser';
import {NgbAccordionModule} from '@ng-bootstrap/ng-bootstrap';
import {TranslateTestingModule} from 'ngx-translate-testing';

import {ContestAccordionComponent} from './contest-accordion.component';

@Component({
	template: `
		<vp-contest-accordion
			[title]="title"
			[subtitle]="subtitle"
			[cardClass]="cardClass"
			>{{ contents }}</vp-contest-accordion
		>
	`,
})
class TestHostComponent {
	title: string | undefined;
	subtitle: string | undefined;
	cardClass: string | undefined;
	contents: string | undefined;
}

describe('AccordionComponent', () => {
	let testHost: TestHostComponent;
	let fixture: ComponentFixture<TestHostComponent>;
	let accordionHeader: DebugElement;
	let accordionBody: DebugElement;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [ContestAccordionComponent, TestHostComponent],
			imports: [
				NgbAccordionModule,
				TranslateTestingModule.withTranslations({}),
			],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(TestHostComponent);
		testHost = fixture.componentInstance;

		fixture.detectChanges();

		accordionHeader = fixture.debugElement.query(By.css('.accordion-header'));
		accordionBody = fixture.debugElement.query(By.css('.accordion-body'));
	});

	it('should be open by default', () => {
		const accordionCollapse = fixture.debugElement.query(
			By.css('.accordion-collapse')
		);
		expect(accordionCollapse.classes['show']).toBeTruthy();
	});

	it('should show "Hide" in the header', () => {
		const displayedText = accordionHeader.query(By.css('span:last-child'))
			.nativeElement as HTMLElement;
		expect(displayedText.textContent).toBe('common.hide');
	});

	it('should properly show provided title', () => {
		const inputTitle = (testHost.title = 'Test Title');

		fixture.detectChanges();

		const displayedTitle = accordionHeader.query(By.css('.ballot-group-title'))
			.nativeElement as HTMLElement;
		expect(displayedTitle.textContent).toBe(inputTitle);
	});

	it('should properly show provided subtitle', () => {
		const inputSubtitle = (testHost.subtitle = 'Test Subtitle');

		fixture.detectChanges();

		const accordionSubtitle = accordionHeader.query(
			By.css('.ballot-group-title > span:nth-child(2)')
		);
		expect(accordionSubtitle).toBeTruthy();
		expect(accordionSubtitle.nativeElement.textContent).toBe(inputSubtitle);
	});

	it('should not show a subtitle if none is provided', () => {
		testHost.subtitle = undefined;

		fixture.detectChanges();

		const accordionSubtitle = accordionHeader.query(
			By.css('.ballot-group-title > span:nth-child(2)')
		);
		expect(accordionSubtitle).toBeFalsy();
	});

	it('should properly show provided contents as the accordion body', () => {
		const inputContents = (testHost.contents = 'Test Contents');

		fixture.detectChanges();

		const displayedBody = accordionBody.nativeElement as HTMLElement;
		expect(displayedBody.textContent).toBe(inputContents);
	});

	it('should properly pass the provided card class to the accordion', () => {
		const cardClass = (testHost.cardClass = 'test-class');

		fixture.detectChanges();

		const displayedAccordion = fixture.debugElement.query(
			By.css('.accordion-item')
		).nativeElement as HTMLElement;
		expect(displayedAccordion.classList).toContain(cardClass);
	});

	describe('header click', () => {
		beforeEach(() => {
			const headerButton = accordionHeader.query(By.css('.accordion-button'))
				.nativeElement as HTMLElement;
			headerButton.click();

			fixture.detectChanges();
		});

		it('should close the accordion body', () => {
			const displayedBody = accordionBody.nativeElement as HTMLElement;
			expect(displayedBody.classList).not.toContain('show');
		});

		it('should display "Show" in the header', () => {
			const displayedText = accordionHeader.query(By.css('span:last-child'))
				.nativeElement as HTMLElement;
			expect(displayedText.textContent).toBe('common.show');
		});
	});
});
