/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */

import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {NgbAccordionModule} from '@ng-bootstrap/ng-bootstrap';
import {TranslateModule} from '@ngx-translate/core';
import {UiPipesModule} from '@vp/ui-pipes';
import {BackendErrorComponent} from './backend-error/backend-error.component';
import {ClearableInputComponent} from './clearable-input/clearable-input.component';
import {ContestAccordionComponent} from './contest-accordion/contest-accordion.component';
import {IconComponent} from './icon/icon.component';
import {UiDirectivesModule} from '@vp/ui-directives';

@NgModule({
	imports: [
		CommonModule,
		NgbAccordionModule,
		TranslateModule,
		UiDirectivesModule,
		UiPipesModule,
	],
	declarations: [
		IconComponent,
		BackendErrorComponent,
		ContestAccordionComponent,
		ClearableInputComponent,
	],
	exports: [
		IconComponent,
		BackendErrorComponent,
		ContestAccordionComponent,
		ClearableInputComponent,
	],
})
export class UiComponentsModule {}
