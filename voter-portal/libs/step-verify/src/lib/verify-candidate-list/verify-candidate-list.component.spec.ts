/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {CandidatesComponent} from '@vp/feature-candidate';
import {MockComponent} from 'ng-mocks';

import {VerifyCandidateListComponent} from './verify-candidate-list.component';

describe('VerifyCandidateListComponent', () => {
	let component: VerifyCandidateListComponent;
	let fixture: ComponentFixture<VerifyCandidateListComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [
				VerifyCandidateListComponent,
				MockComponent(CandidatesComponent),
			],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(VerifyCandidateListComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
