/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {Component, Input} from '@angular/core';
import {ContestVerifyData} from '@vp/util-types';

@Component({
	selector: 'vp-verify-questions',
	templateUrl: './verify-questions.component.html',
})
export class VerifyQuestionsComponent {
	@Input() contestVerifyData: ContestVerifyData | undefined;
}
