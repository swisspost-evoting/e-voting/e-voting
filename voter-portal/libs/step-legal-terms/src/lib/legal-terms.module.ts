/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {TranslateModule} from '@ngx-translate/core';
import {UiPipesModule} from '@vp/ui-pipes';

import {LegalTermsRoutingModule} from './legal-terms-routing.module';
import {LegalTermsComponent} from './legal-terms/legal-terms.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
	imports: [
		CommonModule,
		LegalTermsRoutingModule,
		TranslateModule,
		UiPipesModule,
		FormsModule,
		ReactiveFormsModule,
	],
	declarations: [LegalTermsComponent],
	exports: [LegalTermsComponent],
})
export class LegalTermsModule {}
