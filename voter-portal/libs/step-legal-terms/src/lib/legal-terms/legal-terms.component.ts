/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Store} from '@ngrx/store';
import {TranslateService} from '@ngx-translate/core';
import {ConfigurationService} from '@vp/ui-services';
import {getConfigIdentification, LegalTermsActions} from '@vp/ui-state';
import {AdditionalLegalTerms, BackendConfig, LegalTerms} from '@vp/util-types';
import {map, startWith, Subscription} from 'rxjs';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
	selector: 'vp-legal-terms',
	templateUrl: './legal-terms.component.html',
})
export class LegalTermsComponent implements OnInit, OnDestroy {
	electionEventId!: string;
	additionalLegalTerms?: LegalTerms;
	additionalLegalTermsSubscription!: Subscription;
	agreementForm: FormGroup;

	constructor(
		private readonly route: ActivatedRoute,
		private readonly store: Store,
		private readonly configuration: ConfigurationService,
		private readonly translate: TranslateService,
		private readonly fb: FormBuilder
	) {
		this.agreementForm = this.fb.group({
			defaultAgreement: [this.agreed, Validators.requiredTrue],
		});
	}

	ngOnInit(): void {
		this.electionEventId = this.route.snapshot.paramMap.get(
			'electionEventId'
		) as string;

		this.additionalLegalTermsSubscription = this.translate.onLangChange
			.pipe(
				startWith({lang: this.translate.currentLang}),
				map(({lang}) => {
					const {additionalLegalTerms} = this.configuration;
					return (
						additionalLegalTerms &&
						additionalLegalTerms[lang as keyof AdditionalLegalTerms]
					);
				})
			)
			.subscribe((additionalLegalTerms: LegalTerms | undefined) => {
				// Remove all existing additionalAgreement controls
				Object.keys(this.agreementForm.controls).forEach((controlName) => {
					if (controlName.startsWith('additionalAgreement-')) {
						this.agreementForm.removeControl(controlName);
					}
				});

				this.additionalLegalTerms = additionalLegalTerms;
				if (additionalLegalTerms) {
					for (let i = 0; i < additionalLegalTerms.sections.length; i++) {
						this.agreementForm.addControl(
							`additionalAgreement-${i}`,
							this.fb.control(this.agreed, Validators.requiredTrue)
						);
					}
				}
			});
	}

	get agreed(): boolean {
		return !!this.store.selectSignal(getConfigIdentification)();
	}

	ngOnDestroy() {
		this.additionalLegalTermsSubscription.unsubscribe();
	}

	agree(): void {
		const config: BackendConfig = {
			lang: this.translate.currentLang,
			electionEventId: this.electionEventId,
			identification: this.configuration.identification
		};

		this.store.dispatch(LegalTermsActions.agreeClicked({config}));
	}
}
