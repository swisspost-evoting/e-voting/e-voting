/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LegalTermsComponent} from './legal-terms/legal-terms.component';
import {RouteData, VotingStep} from '@vp/util-types';
import {confirmDeactivationIfNeeded, isElectionEventIdValid,} from '@vp/ui-guards';

const routes: Routes = [
	{
		path: 'legal-terms/:electionEventId',
		data: {
			reachableSteps: [VotingStep.StartVoting],
		} as RouteData,
		canActivate: [isElectionEventIdValid],
		canDeactivate: [confirmDeactivationIfNeeded],
		component: LegalTermsComponent,
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class LegalTermsRoutingModule {}
