/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ReviewComponent} from './review/review.component';
import {RouteData, VotingStep} from '@vp/util-types';
import {areLegalTermsAccepted, confirmDeactivationIfNeeded,} from '@vp/ui-guards';

const routes: Routes = [
	{
		path: VotingStep.Review,
		data: {
			reachableSteps: [VotingStep.Choose],
		} as RouteData,
		canMatch: [areLegalTermsAccepted],
		canDeactivate: [confirmDeactivationIfNeeded],
		component: ReviewComponent,
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class ReviewRoutingModule {
}
