/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {Component, Input} from '@angular/core';
import {ContestAndContestUserData, TemplateType} from '@vp/util-types';

@Component({
	selector: 'vp-review-contest-container',
	templateUrl: './review-contest-container.component.html',
})
export class ReviewContestContainerComponent {
	@Input() contestAndValues: ContestAndContestUserData | undefined;
	readonly TemplateType = TemplateType;
}
