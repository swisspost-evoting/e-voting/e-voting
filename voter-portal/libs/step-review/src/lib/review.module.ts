/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {TranslateModule} from '@ngx-translate/core';
import {CandidateModule} from '@vp/feature-candidate';
import {ListModule} from '@vp/feature-list';
import {QuestionsModule} from '@vp/feature-question';
import {UiComponentsModule} from '@vp/ui-components';
import {ReviewCandidateListComponent} from './review-candidate-list/review-candidate-list.component';
import {ReviewContestContainerComponent} from './review-contest-container/review-contest-container.component';
import {ReviewQuestionsComponent} from './review-questions/review-questions.component';
import {ReviewRoutingModule} from './review-routing.module';
import {ReviewComponent} from './review/review.component';
import {SendVoteModalComponent} from './send-vote-modal/send-vote-modal.component';
import {UiDirectivesModule} from '@vp/ui-directives';

@NgModule({
	imports: [
		CommonModule,
		TranslateModule,
		ReviewRoutingModule,
		QuestionsModule,
		CandidateModule,
		ListModule,
		UiComponentsModule,
		UiDirectivesModule,
	],
	declarations: [
		ReviewComponent,
		ReviewContestContainerComponent,
		ReviewQuestionsComponent,
		ReviewCandidateListComponent,
		SendVoteModalComponent,
	],
})
export class ReviewModule {}
