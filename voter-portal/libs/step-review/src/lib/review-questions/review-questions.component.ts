/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {Component, Input} from '@angular/core';
import {ContestAndContestUserData} from '@vp/util-types';

@Component({
	selector: 'vp-review-questions',
	templateUrl: './review-questions.component.html',
})
export class ReviewQuestionsComponent {
	@Input() contestAndValues: ContestAndContestUserData | undefined;
}
