/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {MockStore, provideMockStore} from '@ngrx/store/testing';
import {SHARED_FEATURE_KEY} from '@vp/ui-state';
import {MockProvider} from 'ng-mocks';
import {TranslateTestingModule} from 'ngx-translate-testing';

import {SendVoteModalComponent} from './send-vote-modal.component';

describe('SendVoteModalComponent', () => {
	let fixture: ComponentFixture<SendVoteModalComponent>;
	let store: MockStore;
	let activeModal: NgbActiveModal;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [SendVoteModalComponent],
			imports: [TranslateTestingModule.withTranslations({})],
			providers: [
				MockProvider(NgbActiveModal),
				provideMockStore({ initialState: { [SHARED_FEATURE_KEY]: {} } }),
			],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(SendVoteModalComponent);
		store = TestBed.inject(MockStore);
		activeModal = TestBed.inject(NgbActiveModal);

		activeModal.close = jest.fn();

		fixture.detectChanges();
	});

	function setState(newState: object) {
		store.setState({ [SHARED_FEATURE_KEY]: newState });
	}

	it('should close when choice return codes are registered in the store', () => {
		setState({ choiceReturnCodes: ['mockChoiceReturnCode'] });

		fixture.detectChanges();

		expect(activeModal.close).toHaveBeenCalledTimes(1);
	});

	it('should close when an error is registered in the store', () => {
		setState({ error: 'mockError' });

		fixture.detectChanges();

		expect(activeModal.close).toHaveBeenCalledTimes(1);
	});
});
