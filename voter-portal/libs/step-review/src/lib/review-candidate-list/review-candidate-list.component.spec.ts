/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {CandidatesComponent} from '@vp/feature-candidate';
import {MockComponent} from 'ng-mocks';
import {ReviewCandidateListComponent} from './review-candidate-list.component';

describe('ReviewCandidateListComponent', () => {
	let component: ReviewCandidateListComponent;
	let fixture: ComponentFixture<ReviewCandidateListComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [
				ReviewCandidateListComponent,
				MockComponent(CandidatesComponent),
			],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(ReviewCandidateListComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
