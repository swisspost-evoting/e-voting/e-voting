/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {Environment} from '@vp/util-types';

export const environment: Environment = {
	production: true,
	voterPortalConfig: './assets/configuration/portalConfig.json',
	availableLanguages: [
		{ id: 'de', label: 'Deutsch' },
		{ id: 'fr', label: 'Français' },
		{ id: 'it', label: 'Italiano' },
		{ id: 'rm', label: 'Rumantsch' },
	],
	progressOverlayCloseDelay: 1000,
	progressOverlayNavigateDelay: 1100,
};
