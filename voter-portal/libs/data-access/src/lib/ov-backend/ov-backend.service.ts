/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {Injectable} from '@angular/core';
import {AuthenticateVoterResponse, BackendConfig, BackendError, Ballot, BallotUserData, OvApi, Voter,} from '@vp/util-types';
import {BackendService} from '../backend.service';
import {RepresentationBuilderService} from '../representation-builder/representation-builder.service';

declare global {
	const OvApi: () => OvApi;

	interface Window {
		OvApi: OvApi;
	}
}

function deepClone<T>(obj: T): T {
	return JSON.parse(JSON.stringify(obj));
}

@Injectable({
	providedIn: 'root',
})
export class OvBackendService implements BackendService {
	constructor(
		private readonly representationBuilder: RepresentationBuilderService
	) {}

	private get ovApi(): OvApi {
		return window.OvApi;
	}

	async authenticateVoter(
		voter: Voter,
		config: BackendConfig
	): Promise<AuthenticateVoterResponse> {
		let authenticateVoterResponse: AuthenticateVoterResponse;
		try {
			authenticateVoterResponse = await this.ovApi.authenticateVoter(
				voter.startVotingKey.toLowerCase(),
				voter.extendedFactor,
				config.electionEventId,
				`${config.lang}-CH`,
				config.identification
			);
		} catch (error) {
			throw new BackendError(error);
		}

		return authenticateVoterResponse;
	}

	async translateBallot(ballot: Ballot, lang: string): Promise<Ballot> {
		const mutableBallot = deepClone(ballot);
		let translatedBallot: Ballot;

		try {
			translatedBallot = await this.ovApi.translateBallot(
				mutableBallot,
				`${lang}-CH`
			);
		} catch (error) {
			throw new BackendError(error);
		}

		return translatedBallot;
	}

	async sendVote(
		ballot: Ballot,
		ballotUserData: BallotUserData
	): Promise<string[]> {
		const { primes, writeIns } =
			this.representationBuilder.getPrimesAndWriteIns(ballot, ballotUserData);

		try {
			const { choiceReturnCodes } = await this.ovApi.sendVote(primes, writeIns);
			return choiceReturnCodes;
		} catch (error) {
			throw new BackendError(error);
		}
	}

	async confirmVote(confirmationKey: string): Promise<string> {
		try {
			const { voteCastReturnCode } = await this.ovApi.confirmVote(
				confirmationKey
			);
			return voteCastReturnCode;
		} catch (error) {
			throw new BackendError(error);
		}
	}
}
