/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {TestBed} from '@angular/core/testing';
import {BackendConfig, BackendError, Ballot, BallotUserData, OvApi, Voter,} from '@vp/util-types';
import {MockProvider} from 'ng-mocks';
import {RepresentationBuilderService} from '../representation-builder/representation-builder.service';
import {OvBackendService} from './ov-backend.service';

const mockVoter: Voter = {
	startVotingKey: '',
	extendedFactor: '',
};

class MockOvApi implements OvApi {
	authenticateVoter = jest.fn().mockReturnValue(Promise.resolve({}));
	sendVote = jest.fn().mockReturnValue(Promise.resolve([]));
	confirmVote = jest.fn().mockReturnValue(Promise.resolve(''));
	translateBallot = jest.fn();
}

class MockRepresentationBuilderService extends RepresentationBuilderService {
	override getPrimesAndWriteIns = jest.fn().mockReturnValue(Promise.resolve());
}

describe('BackendService', () => {
	let backendService: OvBackendService;
	let representationBuilder: MockRepresentationBuilderService;
	let mockOvApi: MockOvApi;

	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [
				OvBackendService,
				MockProvider(
					RepresentationBuilderService,
					new MockRepresentationBuilderService()
				),
			],
		});

		backendService = TestBed.inject(OvBackendService);
		representationBuilder = TestBed.inject(
			RepresentationBuilderService
		) as MockRepresentationBuilderService;

		mockOvApi = new MockOvApi();
		Object.assign(global, { OvApi: () => mockOvApi });
		Object.assign(window, { OvApi: mockOvApi });
	});

	function throwError() {
		throw new Error();
	}

	describe('authenticateVoter', () => {
		it('should return the authentication response fetched from the OvApi', async () => {
			const mockOvApiResponse = {
				mockAuthenticateVoterResponse: 'mockAuthenticateVoterResponse',
			};

			mockOvApi.authenticateVoter.mockReturnValueOnce(mockOvApiResponse);
			const authenticateVoterResponse = await backendService.authenticateVoter(
				mockVoter,
				{} as BackendConfig
			);

			expect(authenticateVoterResponse).toBe(mockOvApiResponse);
		});

		it('should throw a backend error if the OvApi throws an error', async () => {
			mockOvApi.authenticateVoter.mockImplementationOnce(throwError);

			await expect(
				backendService.authenticateVoter(mockVoter, {} as BackendConfig)
			).rejects.toBeInstanceOf(BackendError);
		});
	});

	describe('translateBallot', () => {
		it('should return the translated ballot in the correct Swiss language', async () => {
			const mockLanguage = 'mockLanguage';
			const mockBallot = { id: 'mockBallot' };
			const mockTranslate = (
				ballot: Record<string, string>,
				swissLanguage: string
			) => {
				ballot['translatedInto'] = swissLanguage;
				return ballot;
			};

			mockOvApi.translateBallot.mockImplementationOnce(mockTranslate);
			const response = await backendService.translateBallot(
				mockBallot as Ballot,
				mockLanguage
			);

			expect(response).toEqual({
				...mockBallot,
				translatedInto: `${mockLanguage}-CH`,
			});
		});

		it('should throw a backend error if the OvApi throws an error', async () => {
			mockOvApi.translateBallot.mockImplementationOnce(throwError);

			await expect(
				backendService.translateBallot({} as Ballot, '')
			).rejects.toBeInstanceOf(BackendError);
		});

		it('should properly translate an immutable ballot', async () => {
			const mockBallot = { id: 'mockImmutableBallot' };
			Object.freeze(mockBallot);

			mockOvApi.translateBallot.mockImplementationOnce(
				(ballot: Record<string, string>) => {
					ballot['mockTranslation'] = 'mockTranslation';
				}
			);

			await expect(() =>
				backendService.translateBallot(mockBallot as Ballot, '')
			).not.toThrow();
		});
	});

	describe('sendVote', () => {
		it('should return the choice return codes fetched from the OvApi', async () => {
			const mockChoiceReturnCodes = ['mockChoiceReturnCode'];

			mockOvApi.sendVote.mockReturnValueOnce({
				choiceReturnCodes: mockChoiceReturnCodes,
			});
			const sendVoteResponse = await backendService.sendVote(
				{} as Ballot,
				{} as BallotUserData
			);

			expect(sendVoteResponse).toBe(mockChoiceReturnCodes);
		});

		it('should throw a backend error if the OvApi throws an error', async () => {
			mockOvApi.sendVote.mockImplementationOnce(throwError);

			await expect(
				backendService.sendVote({} as Ballot, {} as BallotUserData)
			).rejects.toBeInstanceOf(BackendError);
		});

		it('should call the OvApi with the primes and write-ins returned from the representation builder', async () => {
			const mockPrimes = ['mockPrime'];
			const mockWriteIns = ['mockWriteIn'];

			representationBuilder.getPrimesAndWriteIns.mockReturnValueOnce({
				primes: mockPrimes,
				writeIns: mockWriteIns,
			});
			await backendService.sendVote({} as Ballot, {} as BallotUserData);

			expect(mockOvApi.sendVote).toHaveBeenCalledWith(mockPrimes, mockWriteIns);
		});
	});

	describe('confirmVote', () => {
		it('should return the vote cast code fetched from the OvApi', async () => {
			const mockVoteCastReturnCode = 'mockVoteCastReturnCode';

			mockOvApi.confirmVote.mockReturnValueOnce({
				voteCastReturnCode: mockVoteCastReturnCode,
			});
			const confirmVoteResponse = await backendService.confirmVote('');

			expect(confirmVoteResponse).toBe(mockVoteCastReturnCode);
		});

		it('should throw a backend error if the OvApi throws an error', async () => {
			mockOvApi.confirmVote.mockImplementationOnce(throwError);

			await expect(backendService.confirmVote('')).rejects.toBeInstanceOf(
				BackendError
			);
		});
	});
});
