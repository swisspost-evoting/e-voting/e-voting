/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import {ConfirmationService} from '@vp/ui-confirmation';
import {SharedActions} from '@vp/ui-state';
import {ConfirmationModalConfig} from '@vp/util-types';
import {filter} from 'rxjs/operators';

export enum CancelState {
	NO_CANCEL_VOTE_OR_LEAVE_PROCESS = 'NO_CANCEL_OR_LEAVE_PROCESS',
	CANCEL_VOTE = 'CANCELLED',
	LEAVE_PROCESS = 'LEFT_PROCESS',
	QUIT = 'QUIT',
}

@Injectable({
	providedIn: 'root',
})
export class ProcessCancellationService {
	public cancelState = CancelState.NO_CANCEL_VOTE_OR_LEAVE_PROCESS;
	public backButtonPressed = false;

	constructor(
		private readonly store: Store,
		private readonly confirmationService: ConfirmationService
	) {}

	public cancelVote() {
		const processCancellationModalConfig: ConfirmationModalConfig = {
			title: 'cancelvote.title',
			content: ['cancelvote.loosechoices', 'cancelvote.returnoptions'],
			confirmLabel: 'cancelvote.yes',
			cancelLabel: 'cancelvote.no',
			modalOptions: { size: 'lg' },
		};

		this.confirmationService
			.confirm(processCancellationModalConfig)
			.pipe(filter((wasCancellationConfirmed) => wasCancellationConfirmed))
			.subscribe(() => {
				this.goToStartVoting(CancelState.CANCEL_VOTE);
			});
	}

	public leaveProcess() {
		const processExitModalConfig: ConfirmationModalConfig = {
			title: 'leaveprocess.title',
			content: 'leaveprocess.note',
			confirmLabel: 'leaveprocess.yes',
			cancelLabel: 'leaveprocess.no',
			modalOptions: { size: 'lg' },
		};

		this.confirmationService
			.confirm(processExitModalConfig)
			.pipe(filter((wasExitConfirmed) => wasExitConfirmed))
			.subscribe(() => {
				this.goToStartVoting(CancelState.LEAVE_PROCESS);
			});
	}

	public quit() {
		this.goToStartVoting(CancelState.QUIT);
	}

	public reset() {
		this.cancelState = CancelState.NO_CANCEL_VOTE_OR_LEAVE_PROCESS;
	}

	private goToStartVoting(cancelState: CancelState) {
		this.cancelState = cancelState;
		this.store.dispatch(SharedActions.loggedOut());
	}
}
