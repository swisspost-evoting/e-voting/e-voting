/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {environment} from '@vp/data-access';
import {
	AdditionalFAQs,
	AdditionalLegalTerms,
	ContestsCapabilities,
	ExtendedFactor,
	HeaderConfig,
	RequestTimeout,
	TranslatePlaceholders,
	VoterPortalConfig,
} from '@vp/util-types';
import {firstValueFrom} from 'rxjs';
import {tap} from 'rxjs/operators';

@Injectable({
	providedIn: 'root',
})
export class ConfigurationService implements VoterPortalConfig {
	identification!: ExtendedFactor;
	contestsCapabilities!: ContestsCapabilities;
	requestTimeout!: RequestTimeout;
	header!: HeaderConfig;
	translatePlaceholders?: TranslatePlaceholders;
	additionalLegalTerms?: AdditionalLegalTerms;
	additionalFAQs?: AdditionalFAQs;

	constructor(private readonly http: HttpClient) {
	}

	fetchPortalConfiguration(): Promise<VoterPortalConfig> {
		const voterPortalConfig$ = this.http
			.get<VoterPortalConfig>(environment.voterPortalConfig)
			.pipe(tap((configuration) => Object.assign(this, configuration)));

		return firstValueFrom(voterPortalConfig$);
	}
}
