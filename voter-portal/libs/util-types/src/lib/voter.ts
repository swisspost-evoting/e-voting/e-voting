/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
export interface Voter {
	startVotingKey: string;
	extendedFactor: string;
}
