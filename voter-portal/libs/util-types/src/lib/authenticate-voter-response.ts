/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {Ballot} from './ballot';

export type AuthenticateVoterResponse =
	| AuthenticateVoterResponseForUnsentVote
	| AuthenticateVoterResponseForSentVote
	| AuthenticateVoterResponseForCastVote;

export interface AuthenticateVoterResponseForUnsentVote
	extends AuthenticateVoterResponseBase {
	ballot: Ballot;
}

export interface AuthenticateVoterResponseForSentVote
	extends AuthenticateVoterResponseBase {
	ballot: Ballot;
	choiceReturnCodes: string[];
}

export interface AuthenticateVoterResponseForCastVote
	extends AuthenticateVoterResponseBase {
	voteCastReturnCode: string;
}

interface AuthenticateVoterResponseBase {
	votingCardState: VotingCardState;
}

export enum VotingCardState {
	Initial = 'INITIAL',
	Sent = 'SENT',
	Confirmed = 'CONFIRMED',
}
