/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */

import {ExtendedFactor} from './extended-factor';

export interface VoterPortalConfig {
	identification: ExtendedFactor;
	contestsCapabilities: ContestsCapabilities;
	requestTimeout: RequestTimeout;
	header: HeaderConfig;
	translatePlaceholders?: TranslatePlaceholders;
	additionalLegalTerms?: AdditionalLegalTerms;
	additionalFAQs?: AdditionalFAQs;
}

// Contests capabilities
export interface ContestsCapabilities {
	writeIns: boolean;
}

// Request timeout
export interface RequestTimeout {
	authenticateVoter: number;
	sendVote: number;
	confirmVote: number
}

// Header config
interface ResponsiveConfig<T = unknown> {
	desktop: T;
	mobile: T;
}

interface HeaderBarConfig {
	height: ResponsiveConfig<number>;
	color: string;
}

export interface HeaderConfig {
	logoPath: ResponsiveConfig<string>;
	logoHeight: ResponsiveConfig<number>;
	reverse?: boolean;
	background?: string;
	bars?: HeaderBarConfig[];
}

// Supported languages
type ForAllLanguages<T = unknown> = {
	[lang in 'de' | 'fr' | 'it' | 'rm' | 'en']: T;
};

// Translate placeholders
export type TranslatePlaceholders = ForAllLanguages<Record<string, string>>;

// Additional legal terms
export interface LegalTerms {
	stepper: string;
	mainTitle: string;
	sections: LegalTermSection[];
}

export interface LegalTermSection {
	sectionTitle: string;
	terms: string[];
	confirm: string;
}

export type AdditionalLegalTerms = ForAllLanguages<LegalTerms>;

// Additional FAQs
export interface FAQSectionContent {
	title: string;
	content: string[];
}

export type AdditionalFAQs = ForAllLanguages<FAQSectionContent[]>;
