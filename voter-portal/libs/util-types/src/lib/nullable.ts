/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
export type Nullable<T> = T | null | undefined;
