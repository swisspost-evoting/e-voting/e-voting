/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {VotingStep} from './voting-step';

export interface RouteData {
	reachableSteps?: VotingStep[];
	backAction?: BackAction;
}

export enum BackAction {
	ShowCancelVoteDialog = 'ShowCancelDialog',
	ShowLeaveProcessDialog = 'ShowLeaveDialog',
	GoToStartVotingPage = 'GoToStartVotingPage',
	GoToLegalTermsPage = 'GoToLegalTermsPage',
}
