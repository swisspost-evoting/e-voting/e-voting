/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
export interface CandidateFilters {
	searchTerm: string;
	listId?: string;
	selectedCandidatesOnly: boolean;
}
