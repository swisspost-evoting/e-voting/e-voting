/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {Candidate, CandidateList, Contest} from './ballot';

export interface BallotUserData {
	contests: ContestUserData[];
}

export interface ContestUserData {
	questions?: QuestionUserData[];
	candidates?: CandidateUserData[];
	listId?: string | null;
}

export interface CandidateUserData {
	candidateId: string | null;
	writeIn: string | null;
}

export interface QuestionUserData {
	id: string;
	chosenOption: string | null;
}

export interface ContestAndContestUserData {
	contest: Contest;
	contestUserData: ContestUserData;
}

export interface ContestVerifyData {
	contest: Contest;
	contestUserData?: ContestUserData;
	choiceReturnCodes: string[];
}

export interface CandidateSlotData {
	candidate?: Candidate;
	canCumulate?: boolean;
}

export interface ListSlotData {
	list?: CandidateList;
}
