/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
export enum VotingStep {
	LegalTerms = 'legal-terms',
	StartVoting = 'start-voting',
	Choose = 'choose',
	Review = 'review',
	Verify = 'verify',
	Confirm = 'confirm',
}
