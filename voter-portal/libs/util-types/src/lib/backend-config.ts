/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
export interface BackendConfig {
	lang: string;
	electionEventId: string;
	identification: string;
}
