/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
export enum ExtendedFactor {
	YearOfBirth = 'yob',
	DateOfBirth = 'dob',
}
