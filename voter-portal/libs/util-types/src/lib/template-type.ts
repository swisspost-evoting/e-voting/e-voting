/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
export enum TemplateType {
	ListsAndCandidates = 'listsAndCandidates',
	Options = 'options',
}
