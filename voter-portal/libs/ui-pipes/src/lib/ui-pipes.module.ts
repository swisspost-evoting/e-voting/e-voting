/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */

import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {MarkdownPipe} from './markdown/markdown.pipe';

@NgModule({
	imports: [CommonModule],
	declarations: [MarkdownPipe],
	exports: [MarkdownPipe],
})
export class UiPipesModule {}
