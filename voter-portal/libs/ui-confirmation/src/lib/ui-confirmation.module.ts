/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */

import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {TranslateModule} from '@ngx-translate/core';
import {ConfirmationModalComponent} from './confirmation-modal/confirmation-modal.component';

@NgModule({
	imports: [CommonModule, TranslateModule],
	declarations: [ConfirmationModalComponent],
	exports: [ConfirmationModalComponent],
})
export class UiConfirmationModule {}
