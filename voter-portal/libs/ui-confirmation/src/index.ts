/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */

export * from './lib/confirmation-modal/confirmation-modal.component';
export * from './lib/confirmation.service';

export * from './lib/ui-confirmation.module';
