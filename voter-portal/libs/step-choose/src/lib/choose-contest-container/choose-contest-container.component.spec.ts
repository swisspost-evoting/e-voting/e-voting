/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {DebugElement} from '@angular/core';
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {FormArray, FormControl, FormGroup} from '@angular/forms';
import {By} from '@angular/platform-browser';
import {UiComponentsModule} from '@vp/ui-components';
import {MockContest} from '@vp/util-testing';
import {Contest, TemplateType} from '@vp/util-types';
import {MockComponent, MockModule} from 'ng-mocks';
import {TranslateTestingModule} from 'ngx-translate-testing';
import {ChooseCandidateListComponent} from '../choose-candidate-list/choose-candidate-list.component';
import {ChooseQuestionsComponent} from '../choose-questions/choose-questions.component';

import {ChooseContestContainerComponent} from './choose-contest-container.component';

describe('ContestContainerComponent', () => {
	let component: ChooseContestContainerComponent;
	let fixture: ComponentFixture<ChooseContestContainerComponent>;
	let accordionComponent: DebugElement;
	let questionListComponent: DebugElement;
	let contest: Contest;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [
				ChooseContestContainerComponent,
				MockComponent(ChooseQuestionsComponent),
				MockComponent(ChooseCandidateListComponent),
			],
			imports: [
				TranslateTestingModule.withTranslations({}),
				MockModule(UiComponentsModule),
			],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(ChooseContestContainerComponent);
		component = fixture.componentInstance;

		contest = component.contest = MockContest([]);

		component.contestFormGroup = new FormGroup({
			questions: new FormArray([]),
			list: new FormControl(),
			candidates: new FormArray([]),
		});

		fixture.detectChanges();

		accordionComponent = fixture.debugElement.query(
			By.css('vp-contest-accordion')
		);
		questionListComponent = fixture.debugElement.query(
			By.css('vp-choose-questions')
		);
	});

	it('should pass the provided contest title as the title of the accordion component', () => {
		expect(accordionComponent.componentInstance.title).toContain(
			component.contest?.title
		);
	});

	it('should not pass a subtitle to the accordion component if the provided contest only has option choices', () => {
		contest.template = TemplateType.Options;
		component.ngOnInit();

		fixture.detectChanges();

		expect(accordionComponent.componentInstance.subtitle).toBeFalsy();
	});

	it('should pass a subtitle to the accordion component if the provided contest has candidate choices', () => {
		contest.template = TemplateType.ListsAndCandidates;
		component.ngOnInit();

		fixture.detectChanges();

		expect(accordionComponent.componentInstance.subtitle).toBe(
			'choose.candidates.subtitle'
		);
	});

	it('should show the provided contest instructions if there are any', () => {
		contest.howToVote = 'Test instructions';

		fixture.detectChanges();

		const contestInstructions = fixture.debugElement.query(
			By.css('.contest--instructions')
		);
		const displayedInstructions =
			contestInstructions.nativeElement as HTMLElement;
		expect(displayedInstructions.textContent).toContain(contest.howToVote);
	});

	it('should pass the provided contest to the question list component', () => {
		expect(questionListComponent.componentInstance.contest).toBe(
			component.contest
		);
	});

	it('should pass the provided contest to the question list component', () => {
		component.contestFormGroup = new FormGroup({});

		fixture.detectChanges();

		expect(questionListComponent.componentInstance.contestFormGroup).toBe(
			component.contestFormGroup
		);
	});

	it('should not show the question list component if the provided contest has candidate choices', () => {
		contest.template = TemplateType.ListsAndCandidates;

		fixture.detectChanges();

		questionListComponent = fixture.debugElement.query(
			By.css('vp-choose-questions')
		);
		expect(questionListComponent).toBeFalsy();
	});
});
