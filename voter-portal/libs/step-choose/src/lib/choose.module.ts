/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {NgbAlertModule} from '@ng-bootstrap/ng-bootstrap';
import {TranslateModule} from '@ngx-translate/core';
import {CandidateModule} from '@vp/feature-candidate';
import {ListModule} from '@vp/feature-list';
import {QuestionsModule} from '@vp/feature-question';
import {UiComponentsModule} from '@vp/ui-components';
import {UiDirectivesModule} from '@vp/ui-directives';
import {SharedStateModule} from '@vp/ui-state';
import {ChooseCandidateListComponent} from './choose-candidate-list/choose-candidate-list.component';
import {ChooseCandidateComponent} from './choose-candidate/choose-candidate.component';
import {ChooseContestContainerComponent} from './choose-contest-container/choose-contest-container.component';
import {ChooseListComponent} from './choose-list/choose-list.component';
import {ChooseQuestionsComponent} from './choose-questions/choose-questions.component';
import {ChooseRoutingModule} from './choose-routing.module';
import {ChooseComponent} from './choose/choose.component';

@NgModule({
	imports: [
		CommonModule,
		ChooseRoutingModule,
		ReactiveFormsModule,
		TranslateModule,
		NgbAlertModule,
		SharedStateModule,
		UiComponentsModule,
		QuestionsModule,
		CandidateModule,
		ListModule,
		UiDirectivesModule,
	],
	declarations: [
		ChooseComponent,
		ChooseContestContainerComponent,
		ChooseQuestionsComponent,
		ChooseCandidateListComponent,
		ChooseCandidateComponent,
		ChooseListComponent,
	],
	exports: [ChooseListComponent],
})
export class ChooseModule {}
