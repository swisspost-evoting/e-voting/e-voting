/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {FormArray, FormGroup} from '@angular/forms';
import {provideMockStore} from '@ngrx/store/testing';
import {UiComponentsModule} from '@vp/ui-components';
import {UiDirectivesModule} from '@vp/ui-directives';
import {ElectionContest} from '@vp/util-types';
import {MockComponent, MockModule} from 'ng-mocks';
import {TranslateTestingModule} from 'ngx-translate-testing';
import {ChooseListComponent} from '../choose-list/choose-list.component';

import {ChooseCandidateComponent} from './choose-candidate.component';

describe('ChooseCandidateListComponent', () => {
	let component: ChooseCandidateComponent;
	let fixture: ComponentFixture<ChooseCandidateComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [
				ChooseCandidateComponent,
				MockComponent(ChooseListComponent),
			],
			providers: [provideMockStore({})],
			imports: [
				MockModule(UiComponentsModule),
				MockModule(UiDirectivesModule),
				TranslateTestingModule.withTranslations({}),
			],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(ChooseCandidateComponent);
		component = fixture.componentInstance;

		component.electionContest = {
			id: 'contestId',
			template: 'contestTemplate',
		} as ElectionContest;

		component.contestFormGroup = new FormGroup({
			candidates: new FormArray([]),
		});

		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
