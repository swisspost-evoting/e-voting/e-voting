/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {
	Component,
	ElementRef,
	Input,
	OnChanges,
	OnDestroy,
	OnInit,
	SimpleChanges,
	ViewChild,
} from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CandidateSelectionModalComponent } from '@vp/feature-candidate';
import {
	Candidate,
	Contest,
	ContestUserData,
	ElectionContest,
} from '@vp/util-types';
import { Store } from '@ngrx/store';
import { getContestsAndContestsUserData } from '@vp/ui-state';
import { filter } from 'rxjs/operators';
import { Subscription } from 'rxjs';

@Component({
	selector: 'vp-choose-candidate',
	templateUrl: './choose-candidate.component.html',
})
export class ChooseCandidateComponent implements OnInit, OnChanges, OnDestroy {
	@ViewChild('mainButton') mainButton:
		| ElementRef<HTMLButtonElement>
		| undefined;

	@Input() candidate: Candidate | undefined;
	@Input() candidateIndex!: number;
	@Input() electionContest!: ElectionContest;
	@Input() contestFormGroup!: FormGroup;
	@Input() candidates!: FormArray;
	@Input() unoccupiedSeat: FormGroup | undefined;
	@Input() cumulByCandidate!: Map<string, number>;

	selectedInPrimary = false;
	implicitlyEligible = false;
	showSelectionMessage = false;
	showCumulationMessage = false;
	showDeletionMessage = false;

	private subscription!: Subscription;

	constructor(
		private readonly modalService: NgbModal,
		private readonly store: Store
	) {}

	ngOnInit(): void {
		this.subscription = this.store
			.select(getContestsAndContestsUserData)
			.pipe(
				filter(
					() =>
						this.electionContest.candidatesQuestion?.primarySecondaryType ===
						'SECONDARY'
				)
			)
			.subscribe(({ contests, contestsUserData }) => {
				let primaryElectionCandidateIndex = -1;
				if (!contests || !contestsUserData) return;

				primaryElectionCandidateIndex = this.getPrimaryElectionCandidateIndex(
					contests,
					contestsUserData
				);
				this.selectedInPrimary = primaryElectionCandidateIndex >= 0;
			});
	}

	ngOnChanges(changes: SimpleChanges): void {
		if (!changes['electionContest'] && !changes['candidate']) return;

		const foundCandidateId =
			this.electionContest.candidatesQuestion?.implicitlyEligibleCandidateIdentifications.find(
				(candidateAttribute) => {
					return this.candidate?.attribute === candidateAttribute;
				}
			);
		this.implicitlyEligible = foundCandidateId === this.candidate?.id;
	}

	ngOnDestroy() {
		this.subscription.unsubscribe();
	}

	get candidateFormGroup(): FormGroup | undefined {
		return this.candidates?.controls[this.candidateIndex] as FormGroup;
	}

	get candidateMaxCumul(): number {
		return this.candidate?.allRepresentations?.length ?? 0;
	}

	get candidateCanCumulate(): boolean {
		if (!this.candidate) {
			return false;
		}

		const candidateCumul = this.cumulByCandidate.get(this.candidate.id) ?? 0;
		const candidateMaxCumul = this.candidate.allRepresentations?.length ?? 0;
		return candidateCumul < candidateMaxCumul;
	}

	get isWriteIn(): boolean {
		return !!this.candidate?.isWriteIn;
	}

	get isSecondaryElection(): boolean {
		return (
			this.electionContest.candidatesQuestion?.primarySecondaryType ===
			'SECONDARY'
		);
	}

	get isSelectedInPrimary(): boolean {
		return this.selectedInPrimary;
	}

	private getPrimaryElectionCandidateIndex(
		contests: Contest[],
		contestsUserData: ContestUserData[]
	): number {
		// 1. find corresponding primary election (same electionGroupIdentification
		const primaryElection = contests.find((contest) => {
			const hasSameGroup =
				contest.candidatesQuestion?.electionGroupIdentification ===
				this.electionContest.candidatesQuestion?.electionGroupIdentification;
			const isPrimary =
				contest.candidatesQuestion?.primarySecondaryType === 'PRIMARY';
			return hasSameGroup && isPrimary;
		});

		if (!primaryElection) return -1;

		const primaryElectionIndex = contests.findIndex((contest) => {
			return contest.id === primaryElection.id;
		});

		if (primaryElectionIndex < 0) return -1;

		// 2. Get the candidates of the primary election selected by the user
		const primaryElectionUserSelection =
			contestsUserData[primaryElectionIndex].candidates;

		if (!primaryElectionUserSelection) return -1;

		// 3. Find the candidate in the primary election
		const candidateInPrimaryElection = primaryElection.lists
			?.flatMap((candidateList) => {
				return candidateList.candidates;
			})
			.find((candidate) => {
				if (!candidate.alias || !this.candidate?.alias) return false;

				return (
					candidate.alias.split('|')[1] === this.candidate?.alias.split('|')[1]
				);
			});

		if (!candidateInPrimaryElection) return -1;

		// 4. Find the index of the candidate selection
		return primaryElectionUserSelection.findIndex((candidateUserData) => {
			return (
				candidateUserData.candidateId === candidateInPrimaryElection.id ||
				(candidateUserData.writeIn && candidateInPrimaryElection.isWriteIn)
			);
		});
	}

	get isImplicitlyEligible(): boolean {
		return this.implicitlyEligible;
	}

	openCandidateSelectionModal(): void {
		const modalOptions = { fullscreen: 'xl', size: 'xl' };
		const modalRef = this.modalService.open(
			CandidateSelectionModalComponent,
			modalOptions
		);

		Object.assign(modalRef.componentInstance, {
			contest: this.electionContest,
			contestUserData: this.contestFormGroup.value,
			candidateIndex: this.candidateIndex,
		});

		modalRef.result
			.then((selectedCandidate: Candidate) => {
				this.showSelectionMessage = true;
				this.candidateFormGroup?.setValue({
					candidateId: selectedCandidate.id,
					writeIn: null,
				});
			})
			.catch(() => null);
	}

	cumulateCandidate() {
		if (this.unoccupiedSeat) {
			this.unoccupiedSeat.get('candidateId')?.setValue(this.candidate?.id);
			this.showCumulationMessage = true;
		}
	}

	clearCandidate(): void {
		this.candidateFormGroup?.setValue({
			candidateId: null,
			writeIn: null,
		});

		this.showDeletionMessage = true;
		setTimeout(() => this.mainButton?.nativeElement.focus());
	}

	removeMessages() {
		this.showSelectionMessage = false;
		this.showCumulationMessage = false;
		this.showDeletionMessage = false;
	}
}
