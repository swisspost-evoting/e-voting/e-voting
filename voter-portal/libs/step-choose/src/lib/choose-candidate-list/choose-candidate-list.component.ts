/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {
	AfterViewInit,
	Component,
	Input,
	OnDestroy,
	OnInit,
} from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';
import { FAQService } from '@vp/feature-faq';
import {
	CandidateUserData,
	Contest,
	ElectionContest,
	FAQSection,
} from '@vp/util-types';
import {Subject, Subscription, takeUntil} from 'rxjs';
import { filter, startWith } from 'rxjs/operators';
import { getContestsAndContestsUserData } from '@vp/ui-state';
import { Store } from '@ngrx/store';

@Component({
	selector: 'vp-choose-candidates',
	templateUrl: './choose-candidate-list.component.html',
})
export class ChooseCandidateListComponent
	implements OnInit, OnDestroy, AfterViewInit
{
	@Input() contestFormGroup!: FormGroup;
	@Input() showErrors = false;
	electionContest!: ElectionContest;
	primaryElectionContest?: Contest;
	cumulByCandidate!: Map<string, number>;
	unoccupiedSeat: FormGroup | undefined;
	private destroy$ = new Subject<void>;

	constructor(
		private readonly fAQService: FAQService,
		private readonly store: Store
	) {}

	@Input() set contest(value: Contest | undefined) {
		if (value) {
			this.electionContest = new ElectionContest(value);
		}
	}

	get candidates(): FormArray {
		return this.contestFormGroup?.get('candidates') as FormArray;
	}

	get cumulationAllowed(): boolean {
		return (
			!!this.electionContest.candidatesQuestion &&
			this.electionContest.candidatesQuestion.cumul > 1
		);
	}

	ngOnInit() {
		this.candidates.valueChanges
			.pipe(startWith(this.candidates.value as CandidateUserData[]), takeUntil(this.destroy$))
			.subscribe((candidatesUserData: CandidateUserData[]) => {
				this.cumulByCandidate = this.getCumulByCandidate(candidatesUserData);
				this.unoccupiedSeat = this.getUnoccupiedSeat();
			});

		this.store
			.select(getContestsAndContestsUserData)
			.pipe(
				filter(
					() =>
						this.electionContest.candidatesQuestion?.primarySecondaryType ===
						'SECONDARY'
				),
				takeUntil(this.destroy$)
			)
			.subscribe(({ contests, contestsUserData }) => {
				this.primaryElectionContest = contests?.find((contest) => {
					const hasSameGroup =
						contest.candidatesQuestion?.electionGroupIdentification ===
						this.electionContest.candidatesQuestion
							?.electionGroupIdentification;
					const isPrimary =
						contest.candidatesQuestion?.primarySecondaryType === 'PRIMARY';
					return hasSameGroup && isPrimary;
				});
			});
	}

	ngOnDestroy() {
		this.destroy$.next();
		this.destroy$.complete();
	}

	showWriteInsFAQ() {
		this.fAQService.showFAQ(FAQSection.HowToUseWriteIns);
	}

	private getUnoccupiedSeat(): FormGroup | undefined {
		return this.candidates.controls.find((control) => {
			return !control.value.candidateId;
		}) as FormGroup;
	}

	private getCumulByCandidate(candidatesUserData: CandidateUserData[]) {
		return candidatesUserData.reduce(
			(
				cumulByCandidate: Map<string, number>,
				{ candidateId }: CandidateUserData
			) => {
				if (candidateId) {
					const alreadyCountedCumul = cumulByCandidate.get(candidateId) ?? 0;
					cumulByCandidate.set(candidateId, alreadyCountedCumul + 1);
				}
				return cumulByCandidate;
			},
			new Map()
		);
	}

	ngAfterViewInit(): void {
		this.store
			.select(getContestsAndContestsUserData)
			.pipe(
				filter(
					() =>
						this.electionContest.candidatesQuestion?.primarySecondaryType ===
						'SECONDARY'
				)
			)
			.subscribe(({ contests, contestsUserData }) => {
				if (!contests || !contestsUserData || !this.primaryElectionContest)
					return;

				const primaryElectionIndex = contests.findIndex((contest) => {
					return contest.id === this.primaryElectionContest?.id;
				});

				if (primaryElectionIndex < 0) return;

				const primaryElectionUserSelection =
					contestsUserData[primaryElectionIndex].candidates;

				if (!primaryElectionUserSelection) return;

				const secondaryElectionUserSelection = this.candidates
					.value as CandidateUserData[];

				const newSecondaryElectionCandidatesSelection: CandidateUserData[] = [];
				secondaryElectionUserSelection.forEach(
					(secondaryElectionCandidateUserData) => {
						const candidateId = secondaryElectionCandidateUserData.candidateId;

						const secondaryElectionCandidate =
							this.electionContest.getCandidate(candidateId);

						const candidateInPrimaryElection =
							this.primaryElectionContest?.lists
								?.flatMap((candidateList) => {
									return candidateList.candidates;
								})
								.find((candidate) => {
									if (!candidate.alias || !secondaryElectionCandidate?.alias)
										return false;

									return (
										candidate.alias.split('|')[1] ===
										secondaryElectionCandidate.alias.split('|')[1]
									);
								});

						const candidateInPrimaryElectionIndex =
							primaryElectionUserSelection.findIndex((candidateUserData) => {
								return (
									candidateUserData.candidateId ===
									candidateInPrimaryElection?.id
								);
							});

						if (!this.electionContest.candidatesQuestion) return;

						const implicitCandidateIndex =
							this.electionContest.candidatesQuestion?.implicitlyEligibleCandidateIdentifications.findIndex(
								(implicitCandidateAttribute) => {
									return (
										implicitCandidateAttribute ===
										secondaryElectionCandidate?.attribute
									);
								}
							);

						const implicitDiscreetCandidateIndex =
							this.electionContest.candidatesQuestion?.implicitlyDiscreetCandidateIdentifications.findIndex(
								(implicitCandidateAttribute) => {
									return (
										implicitCandidateAttribute ===
										secondaryElectionCandidate?.attribute
									);
								}
							);

						if (!this.electionContest.lists) return;

						const writeInCandidateListIndex =
							this.electionContest.lists?.findIndex((candidateList) => {
								const candidateFromList = candidateList.candidates.find(
									(listCandidate) => {
										return listCandidate.id === candidateId;
									}
								);

								if (!candidateFromList) return false;

								return candidateFromList.isWriteIn;
							});

						if (
							candidateInPrimaryElectionIndex >= 0 ||
							implicitCandidateIndex >= 0 ||
							implicitDiscreetCandidateIndex >= 0 ||
							writeInCandidateListIndex >= 0
						) {
							newSecondaryElectionCandidatesSelection.push({
								candidateId: candidateId,
								writeIn: secondaryElectionCandidateUserData.writeIn,
							});
						} else {
							newSecondaryElectionCandidatesSelection.push({
								candidateId: null,
								writeIn: null,
							});
						}
					}
				);
				this.candidates.setValue(newSecondaryElectionCandidatesSelection);
			});
	}
}
