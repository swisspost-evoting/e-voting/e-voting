/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {FormArray, FormControl, FormGroup} from '@angular/forms';
import {ElectionContest} from '@vp/util-types';

import {ChooseListComponent} from './choose-list.component';

describe('ChooseListComponent', () => {
	let component: ChooseListComponent;
	let fixture: ComponentFixture<ChooseListComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [ChooseListComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(ChooseListComponent);
		component = fixture.componentInstance;

		component.electionContest = {} as ElectionContest;
		component.contestFormGroup = new FormGroup({
			listId: new FormControl(),
			candidates: new FormArray([]),
		});

		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
