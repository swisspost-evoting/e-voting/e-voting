/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {FormArray, FormControl, FormGroup} from '@angular/forms';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ListSelectionModalComponent} from '@vp/feature-list';
import {ConfirmationService} from '@vp/ui-confirmation';
import {Candidate, CandidateList, CandidateUserData, ConfirmationModalConfig, ContestUserData, ElectionContest,} from '@vp/util-types';
import {EMPTY, from, Observable, of, Subscription} from 'rxjs';
import {catchError, filter, map, switchMap} from 'rxjs/operators';

@Component({
	selector: 'vp-choose-list',
	templateUrl: './choose-list.component.html',
})
export class ChooseListComponent implements OnInit, OnDestroy {
	@Input() electionContest!: ElectionContest;
	@Input() contestFormGroup!: FormGroup;
	subscription!: Subscription;

	constructor(
		private readonly modalService: NgbModal,
		private readonly confirmationService: ConfirmationService
	) {}

	get listId(): FormControl {
		return this.contestFormGroup.get('listId') as FormControl;
	}

	get candidates(): FormArray {
		return this.contestFormGroup.get('candidates') as FormArray;
	}

	get contestUserData(): ContestUserData {
		return this.contestFormGroup.value as ContestUserData;
	}

	ngOnInit() {
		this.subscription = this.candidates.valueChanges
			.pipe(
				filter(
					() =>
						!!this.listId.value &&
						!this.electionContest.allowsListWithoutCandidate
				),
				filter((candidateUserData: CandidateUserData[]) => {
					return candidateUserData.every(({ candidateId }) => !candidateId);
				})
			)
			.subscribe(() => {
				this.listId.setValue(null);
			});
	}

	ngOnDestroy() {
		this.subscription.unsubscribe();
	}

	openListSelectionModal() {
		const modalOptions = { fullscreen: 'xl', size: 'xl' };
		const modalRef = this.modalService.open(
			ListSelectionModalComponent,
			modalOptions
		);
		modalRef.componentInstance.electionContest = this.electionContest;

		const hasListOrCandidatesSelection =
			!!this.contestUserData.listId ||
			this.contestUserData.candidates?.some(
				(candidate) => !!candidate.candidateId
			);

		from(modalRef.result)
			.pipe(
				switchMap((selectedList) =>
					!hasListOrCandidatesSelection
						? of(selectedList)
						: this.confirmListChange(selectedList)
				),
				catchError(() => EMPTY),
				filter((selectedList): selectedList is CandidateList => !!selectedList)
			)
			.subscribe((selectedList) => {
				this.listId.setValue(selectedList.id);

				const orderedCandidates: (Candidate | null)[] = Array.from(
					{ length: this.candidates.controls.length },
					() => null
				);

				selectedList.candidates.forEach((candidate) => {
					const positions =
						candidate.details.candidateType_positionOnList?.split(',') ?? [];
					positions.forEach((position) => {
						orderedCandidates[parseInt(position) - 1] = candidate;
					});
				});

				orderedCandidates.forEach((candidate, i) => {
					const candidateSeat = this.candidates.controls[i].get('candidateId');
					candidateSeat?.setValue(candidate?.id);
				});
			});
	}

	clearList() {
		const deletionModalConfig: ConfirmationModalConfig = {
			content: 'listandcandidates.clearlistquestion.text',
			confirmLabel: 'listandcandidates.clearlistquestion.yes',
		};

		this.confirmationService
			.confirm(deletionModalConfig)
			.pipe(filter((wasDeletionConfirmed) => wasDeletionConfirmed))
			.subscribe(() => {
				this.listId.setValue(null);
				this.candidates.controls.forEach((candidateFormGroup) => {
					const candidateIdControl = candidateFormGroup.get(
						'candidateId'
					) as FormControl;
					candidateIdControl.setValue(null);
				});
			});
	}

	private confirmListChange(
		newList: CandidateList
	): Observable<CandidateList | null> {
		const changeModalConfig = {
			content: 'listandcandidates.changelistquestion.text',
			confirmLabel: 'listandcandidates.changelistquestion.yes',
		};

		return this.confirmationService
			.confirm(changeModalConfig)
			.pipe(map((wasChangeConfirmed) => (wasChangeConfirmed ? newList : null)));
	}
}
