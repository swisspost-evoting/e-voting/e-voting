/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {Component} from '@angular/core';
import {Store} from '@ngrx/store';
import {FAQService} from '@vp/feature-faq';
import {ProcessCancellationService} from '@vp/ui-services';
import {getDefinedVoteCastReturnCode, getVoteCastedInPreviousSession,} from '@vp/ui-state';
import {FAQSection} from '@vp/util-types';
import {map} from 'rxjs/operators';

@Component({
	selector: 'vp-confirm',
	templateUrl: './confirm.component.html',
})
export class ConfirmComponent {
	readonly FAQSection = FAQSection;

	voteCastReturnCode$ = this.store.pipe(
		getDefinedVoteCastReturnCode,
		map((voteCastReturnCode) => this.formatVoteCastCode(voteCastReturnCode))
	);

	voteCastedInPreviousSession$ = this.store.select(
		getVoteCastedInPreviousSession
	);

	constructor(
		private readonly store: Store,
		private readonly faqService: FAQService,
		private readonly cancelProcessService: ProcessCancellationService
	) {}

	showFAQ(section: FAQSection): void {
		this.faqService.showFAQ(section);
	}

	formatVoteCastCode(voteCastReturnCode: string): string | null {
		return new RegExp(/^\d{8}$/).test(voteCastReturnCode)
			? `${voteCastReturnCode.slice(0, 4)} ${voteCastReturnCode.slice(4)}`
			: null;
	}

	quit() {
		this.cancelProcessService.quit();
	}
}
