/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ConfirmComponent} from './confirm/confirm.component';
import {BackAction, RouteData, VotingStep} from '@vp/util-types';
import {areLegalTermsAccepted, confirmDeactivationIfNeeded,} from '@vp/ui-guards';

const routes: Routes = [
	{
		path: VotingStep.Confirm,
		data: {
			reachableSteps: [],
			backAction: BackAction.GoToStartVotingPage,
		} as RouteData,
		canMatch: [areLegalTermsAccepted],
		canDeactivate: [confirmDeactivationIfNeeded],
		component: ConfirmComponent,
	},
];

@NgModule({
	declarations: [],
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class ConfirmRoutingModule {
}
