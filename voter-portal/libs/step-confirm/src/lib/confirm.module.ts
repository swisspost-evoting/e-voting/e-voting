/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {TranslateModule} from '@ngx-translate/core';
import {UiComponentsModule} from '@vp/ui-components';
import {ConfirmRoutingModule} from './confirm-routing.module';
import {ConfirmComponent} from './confirm/confirm.component';

@NgModule({
	imports: [
		CommonModule,
		ConfirmRoutingModule,
		TranslateModule,
		UiComponentsModule,
	],
	declarations: [ConfirmComponent],
})
export class ConfirmModule {}
