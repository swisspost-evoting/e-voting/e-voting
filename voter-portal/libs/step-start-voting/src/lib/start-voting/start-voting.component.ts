/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators,} from '@angular/forms';
import {Store} from '@ngrx/store';
import {FAQService} from '@vp/feature-faq';
import {CancelState, ConfigurationService, ProcessCancellationService,} from '@vp/ui-services';
import {getBackendError, getElectionEventId, getLoading, SharedActions, StartVotingActions,} from '@vp/ui-state';
import {ErrorStatus, FAQSection, Nullable} from '@vp/util-types';
import {Observable, take} from 'rxjs';

function focusFirstInvalidControl() {
	setTimeout(() => {
		const firstInvalid = document.querySelector<HTMLInputElement>(
			'.form-control.is-invalid'
		);
		firstInvalid?.focus();
	});
}

@Component({
	selector: 'vp-start-voting',
	templateUrl: './start-voting.component.html',
})
export class StartVotingComponent implements OnInit {
	@ViewChild('startVotingInput')
	startVotingInput: ElementRef<HTMLInputElement> | null = null;
	readonly CancelState = CancelState;
	readonly ErrorMessage = ErrorStatus;
	isLoading$: Observable<boolean> = this.store.select(getLoading);
	electionEventId$: Observable<Nullable<string>> =
		this.store.select(getElectionEventId);
	voterForm: FormGroup;
	formSubmitted = false;

	constructor(
		public readonly configuration: ConfigurationService,
		private readonly store: Store,
		private readonly fb: FormBuilder,
		private readonly faqService: FAQService,
		public readonly cancelProcessService: ProcessCancellationService
	) {
		this.voterForm = this.fb.group({
			startVotingKey: [
				'',
				[Validators.required, Validators.pattern(/^[0-9a-zA-Z]{24}$/)],
			],
		});

		if (this.configuration.identification) {
			this.voterForm.addControl(
				'extendedFactor',
				this.fb.control('', Validators.required)
			);
		}
	}

	get startVotingKey(): FormControl {
		return this.voterForm.get('startVotingKey') as FormControl;
	}

	ngOnInit(): void {
		if (
			this.cancelProcessService.cancelState !==
			CancelState.NO_CANCEL_VOTE_OR_LEAVE_PROCESS
		) {
			this.store.dispatch(SharedActions.serverErrorCleared());
		}
	}

	submitFormOnEnter(): void {
		if (this.voterForm.get('extendedFactor')?.value) {
			this.start();
		}
	}

	start(): void {
		this.formSubmitted = true;
		if (this.voterForm.invalid) {
			this.resetInvalidControls();
			focusFirstInvalidControl();
			return;
		}

		this.cancelProcessService.reset();
		this.store.dispatch(
			StartVotingActions.startClicked({ voter: this.voterForm.value })
		);
		this.store
			.select(getBackendError)
			.pipe(take(1))
			.subscribe(() => {
				focusFirstInvalidControl();
			});
	}

	showFAQ(): void {
		this.faqService.showFAQ(FAQSection.WhatIsStartVotingKey);
	}

	private resetInvalidControls() {
		Object.values(this.voterForm.controls)
			.filter((control) => control.invalid)
			.forEach((control) => control.reset());
	}
}
