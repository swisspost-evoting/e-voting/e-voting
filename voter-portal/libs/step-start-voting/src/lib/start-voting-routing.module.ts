/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {StartVotingComponent} from './start-voting/start-voting.component';
import {BackAction, RouteData, VotingStep} from '@vp/util-types';
import {areLegalTermsAccepted, confirmDeactivationIfNeeded} from '@vp/ui-guards';

const routes: Routes = [
	{
		path: VotingStep.StartVoting,
		data: {
			reachableSteps: [VotingStep.LegalTerms],
			backAction: BackAction.GoToLegalTermsPage,
		} as RouteData,
		canMatch: [areLegalTermsAccepted],
		canDeactivate: [confirmDeactivationIfNeeded],
		component: StartVotingComponent,
	},
];

@NgModule({
	declarations: [],
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class StartVotingRoutingModule {
}
