/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {AfterViewInit, Component, ElementRef, Input, ViewChild,} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {NgbCalendar, NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import {ConfigurationService, SwpDateAdapter} from '@vp/ui-services';
import {ErrorStatus, ExtendedFactor} from '@vp/util-types';
import {NgxMaskPipe} from 'ngx-mask';

@Component({
	selector: 'vp-extended-factor',
	templateUrl: './extended-factor.component.html',
	providers: [SwpDateAdapter, NgxMaskPipe],
})
export class ExtendedFactorComponent implements AfterViewInit {
	@ViewChild('dateOfBirthInput') dateOfBirthInput:
		| ElementRef<HTMLInputElement>
		| undefined;
	@Input() voterForm!: FormGroup;
	@Input() formSubmitted = false;
	datepickerMinDate: NgbDateStruct;
	datepickerMaxDate: NgbDateStruct;
	datepickerDefaultDate: NgbDateStruct;
	dateOfBirthMaskExpression = '';

	readonly ExtendedFactor = ExtendedFactor;
	readonly ErrorMessage = ErrorStatus;

	constructor(
		public readonly configuration: ConfigurationService,
		private readonly dateAdapter: SwpDateAdapter,
		private readonly calendar: NgbCalendar
	) {
		const today = this.calendar.getToday();
		this.datepickerMinDate = this.calendar.getPrev(today, 'y', 120);
		this.datepickerMaxDate = this.calendar.getPrev(today, 'y', 16);
		this.datepickerDefaultDate = this.calendar.getPrev(today, 'y', 40);
	}

	get extendedFactor(): FormControl {
		return this.voterForm.get('extendedFactor') as FormControl;
	}

	get datePickerSelectedDate(): NgbDateStruct | null {
		return this.dateAdapter.fromModel(this.extendedFactor.value);
	}

	set datePickerSelectedDate(date: NgbDateStruct | null) {
		this.extendedFactor.setValue(this.dateAdapter.toModel(date));
	}

	public ngAfterViewInit(): void {
		if (this.configuration.identification === ExtendedFactor.YearOfBirth) {
			this.extendedFactor.addValidators(Validators.pattern(/^\d{4}$/));
		}

		if (this.configuration.identification === ExtendedFactor.DateOfBirth) {
			this.extendedFactor.addValidators(Validators.pattern(/^\d{8}$/));
		}
	}
}
