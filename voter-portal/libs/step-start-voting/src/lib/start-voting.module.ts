/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { UiComponentsModule } from '@vp/ui-components';
import { SharedStateModule } from '@vp/ui-state';
import { NgxMaskDirective, provideNgxMask } from 'ngx-mask';
import { ExtendedFactorComponent } from './extended-factor/extended-factor.component';
import { StartVotingRoutingModule } from './start-voting-routing.module';
import { StartVotingComponent } from './start-voting/start-voting.component';
import { UiDirectivesModule } from '@vp/ui-directives';

@NgModule({
	imports: [
		// Warning, the order of the dependencies is important !
		// If the NgxMaskDirective is put 'at the wrong place', the input place older of textarea is not display when user input something.
		CommonModule,
		FormsModule,
		NgbDatepickerModule,
		NgxMaskDirective,
		ReactiveFormsModule,
		SharedStateModule,
		StartVotingRoutingModule,
		TranslateModule,
		UiComponentsModule,
		UiDirectivesModule,
	],
	declarations: [StartVotingComponent, ExtendedFactorComponent],
	providers: [provideNgxMask({ validation: false })],
})
export class StartVotingModule {}
