/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
export * from './lib/questions/questions.component';

export * from './lib/questions.module';
