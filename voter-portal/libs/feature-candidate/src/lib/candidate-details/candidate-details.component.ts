/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {Component, Input} from '@angular/core';
import {Candidate, Nullable} from '@vp/util-types';

@Component({
	selector: 'vp-candidate-details',
	templateUrl: './candidate-details.component.html',
})
export class CandidateDetailsComponent {
	@Input() candidate: Nullable<Candidate>;
	@Input() candidateWriteIn: Nullable<string>;
	@Input() candidatePlaceholder: Nullable<string>;
	@Input() headingLevel: Nullable<number>;
	@Input() headingDisplayLevel: Nullable<number>;
	@Input() screenReaderLabel: Nullable<string>;
	@Input() showAllDetails = false;

	get headingClass(): string {
		const headingLevel = this.headingDisplayLevel ?? this.headingLevel;
		return headingLevel ? `h${headingLevel}` : '';
	}
}
