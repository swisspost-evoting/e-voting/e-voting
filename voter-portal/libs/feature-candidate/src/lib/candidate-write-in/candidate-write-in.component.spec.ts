/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormControl, FormsModule } from '@angular/forms';
import { provideMockStore } from '@ngrx/store/testing';
import { FAQService } from '@vp/feature-faq';
import { MockProvider } from 'ng-mocks';
import { TranslateTestingModule } from 'ngx-translate-testing';
import { CandidateWriteInComponent } from './candidate-write-in.component';
import { ElectionContest, TemplateType } from '@vp/util-types';
import { MockContest, MockQuestions } from '@vp/util-testing';

describe('CandidateWriteInComponent', () => {
	let component: CandidateWriteInComponent;
	let fixture: ComponentFixture<CandidateWriteInComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [CandidateWriteInComponent],
			providers: [provideMockStore({}), MockProvider(FAQService)],
			imports: [TranslateTestingModule.withTranslations({}), FormsModule],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(CandidateWriteInComponent);
		component = fixture.componentInstance;
		component.electionContest = new ElectionContest({
			id: '00D96C6E8AE799AE268525EBC88C8F68',
			template: TemplateType.ListsAndCandidates,
		});
		component.writeInControl = new FormControl();
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
