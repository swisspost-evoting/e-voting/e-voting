/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {Component, Input, TemplateRef} from '@angular/core';

@Component({
	selector: 'vp-questions',
	template: `
		<ng-container
			[ngTemplateOutlet]="$any(leftColumnTemplate)"
			[ngTemplateOutletContext]="$any(templateTestingContext)"
		></ng-container>
		<ng-container
			[ngTemplateOutlet]="$any(rightColumnTemplate)"
			[ngTemplateOutletContext]="$any(templateTestingContext)"
		></ng-container>
	`,
})
export class QuestionsTestingComponent {
	@Input() leftColumnTemplate: TemplateRef<object> | undefined;
	@Input() rightColumnTemplate: TemplateRef<object> | undefined;
	@Input() contest: object | undefined;
	@Input() contestUserData: object | undefined;
	@Input() headingLevel: number | undefined;
	templateTestingContext: object | undefined;
}
