/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {Ballot} from '@vp/util-types';

let choiceCodeIndex = 0;

const MockChoiceCode = (): string => {
	choiceCodeIndex++;
	return `choicecode-${choiceCodeIndex}`;
};

export const MockChoiceReturnCodes = (ballot: Ballot): string[] => {
	return ballot.contests.reduce((allChoiceReturnCodes, contest) => {
		let choiceReturnCodes: string[] = [];

		if (contest.questions) {
			const questionChoiceReturnCodes = contest.questions.map(MockChoiceCode);
			choiceReturnCodes = [...choiceReturnCodes, ...questionChoiceReturnCodes];
		}

		if (contest.listQuestion?.maxChoices) {
			const listChoiceReturnCodes = Array.from(
				{ length: contest.listQuestion?.maxChoices },
				MockChoiceCode
			);
			choiceReturnCodes = [...choiceReturnCodes, ...listChoiceReturnCodes];
		}

		if (contest.candidatesQuestion?.maxChoices) {
			const candidateChoiceReturnCodes = Array.from(
				{ length: contest.candidatesQuestion?.maxChoices },
				MockChoiceCode
			);
			choiceReturnCodes = [...choiceReturnCodes, ...candidateChoiceReturnCodes];
		}

		return [...allChoiceReturnCodes, ...choiceReturnCodes];
	}, [] as string[]);
};
