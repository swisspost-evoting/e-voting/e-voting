/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {RandomString} from './random';

export const RandomElectionEventId = () => {
	return RandomString(32, '0123456789ABCDEF');
};
