/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {RandomString} from './random';

export const RandomStartVotingKey = () => {
	return RandomString(24);
};
