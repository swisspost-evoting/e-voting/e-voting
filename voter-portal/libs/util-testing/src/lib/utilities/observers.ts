/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
export class MockResizeObserver {
	observe() {
		/* do nothing */
	}

	unobserve() {
		/* do nothing */
	}

	disconnect() {
		/* do nothing */
	}
}
