/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {QuestionsTestingComponent} from './components/questions-testing.component';
import {TranslationListTestingDirective} from './directives/translation-list-testing.directive';

@NgModule({
	declarations: [QuestionsTestingComponent, TranslationListTestingDirective],
	imports: [CommonModule],
	exports: [QuestionsTestingComponent, TranslationListTestingDirective],
})
export class SharedTestingModule {}
