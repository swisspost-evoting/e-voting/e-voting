/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */

import {Router} from '@angular/router';
import {TestBed} from '@angular/core/testing';
import {MockStore, provideMockStore} from '@ngrx/store/testing';
import {MockProvider} from 'ng-mocks';
import {RandomElectionEventId, RandomItem, RandomString} from "@vp/util-testing";
import {areLegalTermsAccepted} from './are-legal-terms-accepted.guard';
import {SHARED_FEATURE_KEY} from "@vp/ui-state";

describe('areLegalTermsAccepted', () => {
	let router: Router;
	let store: MockStore;

	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [
				provideMockStore({}),
				MockProvider(Router, {
					createUrlTree: jest.fn().mockImplementation((commands) => commands.join('/')),
				} as unknown as Router),
			],
		});

		router = TestBed.inject(Router);
		store = TestBed.inject(MockStore);
	});

	it('should return true if configIdentification is truthy', () => {
		store.setState({
			[SHARED_FEATURE_KEY]: {
				config: {
					lang: RandomItem(['de', 'fr', 'it', 'rm']),
					identification: RandomString(3),
					electionEventId: RandomElectionEventId(),
				}
			}
		});

		TestBed.runInInjectionContext(() => {
			const result = areLegalTermsAccepted({}, []);

			expect(result).toBe(true);
		});
	});

	it('should return a UrlTree if configIdentification is falsy', () => {
		store.setState({
			[SHARED_FEATURE_KEY]: {
				config: {
					lang: '',
					identification: '',
					electionEventId: RandomElectionEventId(),
				}
			}
		});

		TestBed.runInInjectionContext(() => {
			const result = areLegalTermsAccepted({}, []);

			expect(router.createUrlTree).toHaveBeenCalledWith(['']);
			expect(result).toBe('');
		});
	});
});
