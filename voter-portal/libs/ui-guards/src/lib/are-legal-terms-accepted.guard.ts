/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {inject} from '@angular/core';
import {CanMatchFn, Router} from '@angular/router';
import {Store} from '@ngrx/store';
import {getConfigIdentification} from '@vp/ui-state';

export const areLegalTermsAccepted: CanMatchFn = () => {
	const store = inject(Store);
	const router = inject(Router);

	const configIdentification = store.selectSignal(getConfigIdentification)();

	// if the config id is set, then the legal terms have been accepted => allow navigation
	if (configIdentification) return true;

	// if the config id is not set, then the legal terms have not been accepted => redirect to base URL
	return router.createUrlTree(['']);
};
