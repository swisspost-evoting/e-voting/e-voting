/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import { Component } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import {
	ActivatedRouteSnapshot,
	Router,
	RouterStateSnapshot,
	UrlTree,
} from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { ProcessCancellationService } from '@vp/ui-services';
import { MockProvider } from 'ng-mocks';
import { BackAction, VotingStep } from '@vp/util-types';
import { getElectionEventId } from '@vp/ui-state';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { RandomElectionEventId } from '@vp/util-testing';
import { confirmDeactivationIfNeeded } from './confirm-deactivation-if-needed.guard';

describe('confirmDeactivationIfNeeded', () => {
	let cancellationService: ProcessCancellationService;
	let store: MockStore;
	let modalService: NgbModal;

	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [
				provideMockStore({}),
				MockProvider(Router, {
					createUrlTree: jest.fn().mockImplementation((commands) => {
						return commands.join('/');
					}),
				}),
				MockProvider(ProcessCancellationService),
				MockProvider(NgbModal),
			],
		});

		cancellationService = TestBed.inject(ProcessCancellationService);
		store = TestBed.inject(MockStore);
		modalService = TestBed.inject(NgbModal);

		cancellationService.backButtonPressed = true;
		jest.spyOn(modalService, 'hasOpenModals').mockReturnValue(false);
	});

	describe('route data with reachableSteps', () => {
		function runDeactivationGuard(nextURL: string) {
			return TestBed.runInInjectionContext(() =>
				confirmDeactivationIfNeeded(
					{} as Component,
					{
						data: { reachableSteps: ['authorized-url'] },
					} as unknown as ActivatedRouteSnapshot,
					{} as RouterStateSnapshot,
					{ url: nextURL } as RouterStateSnapshot,
				),
			);
		}

		it('should allow deactivation to navigate to an authorized URL', () => {
			const deactivationAllowed = runDeactivationGuard('authorized-url');

			expect(deactivationAllowed).toBe(true);
		});

		it('should not allow deactivation to navigate to an unauthorized URL', () => {
			const deactivationAllowed = runDeactivationGuard('unauthorized-url');

			expect(deactivationAllowed).toBe(false);
		});

		it('should allow deactivation if the "Back" button has not been pressed', () => {
			cancellationService.backButtonPressed = false;

			const deactivationAllowed = runDeactivationGuard('unauthorized-url');

			expect(deactivationAllowed).toBe(true);
		});

		it('should not allow deactivation if a modal is currently open', () => {
			jest.spyOn(modalService, 'hasOpenModals').mockReturnValueOnce(true);

			const deactivationAllowed = runDeactivationGuard('authorized-url');

			expect(deactivationAllowed).toBe(false);
		});
	});

	describe('route data with backAction', () => {
		function runDeactivationGuard(backAction: BackAction) {
			return TestBed.runInInjectionContext(() =>
				confirmDeactivationIfNeeded(
					{} as Component,
					{
						data: { reachableSteps: ['authorized-url'], backAction },
					} as unknown as ActivatedRouteSnapshot,
					{} as RouterStateSnapshot,
					{ url: 'unauthorized-url' } as RouterStateSnapshot,
				),
			);
		}

		it('should show the "Cancel Vote" dialog', () => {
			jest.spyOn(cancellationService, 'cancelVote');

			const deactivationAllowed = runDeactivationGuard(
				BackAction.ShowCancelVoteDialog,
			);

			expect(deactivationAllowed).toBe(false);
			expect(cancellationService.cancelVote).toBeCalled();
		});

		it('should show the "Leave Process" dialog', () => {
			jest.spyOn(cancellationService, 'leaveProcess');

			const deactivationAllowed = runDeactivationGuard(
				BackAction.ShowLeaveProcessDialog,
			);

			expect(deactivationAllowed).toBe(false);
			expect(cancellationService.leaveProcess).toBeCalled();
		});

		it('should go back to the start voting page', () => {
			const deactivationUrlTree = runDeactivationGuard(
				BackAction.GoToStartVotingPage,
			);

			expect(deactivationUrlTree).toBe(VotingStep.StartVoting);
		});

		it('should go back to the legal terms page', (done) => {
			const mockElectionEventId = RandomElectionEventId();
			store.overrideSelector(getElectionEventId, mockElectionEventId);

			const deactivationUrlTree$ = runDeactivationGuard(
				BackAction.GoToLegalTermsPage,
			) as Observable<UrlTree>;

			deactivationUrlTree$.pipe(take(1)).subscribe((urlTree) => {
				expect(urlTree).toBe(`legal-terms/${mockElectionEventId}`);
				done();
			});
		});
	});
});
