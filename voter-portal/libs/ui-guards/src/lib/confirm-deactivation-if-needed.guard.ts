/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {Component, inject} from '@angular/core';
import {ActivatedRouteSnapshot, Router, RouterStateSnapshot, UrlTree,} from '@angular/router';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Store} from '@ngrx/store';
import {ProcessCancellationService} from '@vp/ui-services';
import {getElectionEventId} from '@vp/ui-state';
import {BackAction, RouteData, VotingStep} from '@vp/util-types';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

export const confirmDeactivationIfNeeded = (
	_component: Component,
	currentRoute: ActivatedRouteSnapshot,
	_currentState: RouterStateSnapshot,
	nextState?: RouterStateSnapshot | undefined
): boolean | UrlTree | Observable<UrlTree> => {
	const cancellationService = inject(ProcessCancellationService);
	const router = inject(Router);
	const store = inject(Store);
	const modalService = inject(NgbModal);

	if (!cancellationService.backButtonPressed) {
		return true;
	}

	cancellationService.backButtonPressed = false;

	if (modalService.hasOpenModals()) {
		history.pushState(null, '', location.href);
		return false;
	}

	const routeData = currentRoute.data as RouteData;
	if (routeData?.reachableSteps && nextState?.url) {
		const isBackPathAllowed = routeData.reachableSteps.includes(
			nextState.url.replace(/^\//, '') as VotingStep
		);

		if (isBackPathAllowed) {
			return true;
		}
	}

	switch (routeData?.backAction) {
		case BackAction.ShowCancelVoteDialog:
			cancellationService.cancelVote();
			return false;
		case BackAction.ShowLeaveProcessDialog:
			cancellationService.leaveProcess();
			return false;
		case BackAction.GoToStartVotingPage:
			return router.createUrlTree([VotingStep.StartVoting]);
		case BackAction.GoToLegalTermsPage:
			return store.select(getElectionEventId).pipe(
				map((electionEventId) => {
					return router.createUrlTree(
						electionEventId ? [VotingStep.LegalTerms, electionEventId] : ['']
					);
				})
			);
		default:
			history.pushState(null, '', location.href);
			return false;
	}
};
