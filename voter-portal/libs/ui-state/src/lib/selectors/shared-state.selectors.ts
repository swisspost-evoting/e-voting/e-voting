/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {createFeatureSelector, createSelector} from '@ngrx/store';
import {SHARED_FEATURE_KEY, SharedState,} from '../reducer/shared-state.reducer';

export const getSharedState =
	createFeatureSelector<SharedState>(SHARED_FEATURE_KEY);

export const getLoading = createSelector(
	getSharedState,
	(state: SharedState) => state.loading
);

export const getIsAuthenticated = createSelector(
	getSharedState,
	(state: SharedState) => state?.isAuthenticated
);

export const getBackendError = createSelector(
	getSharedState,
	(state: SharedState) => state.error
);

export const hasBackendError = createSelector(
	getSharedState,
	(state: SharedState) => !!state.error
);

export const getBallot = createSelector(
	getSharedState,
	(state: SharedState) => state.ballot
);

export const getContests = createSelector(
	getSharedState,
	(state: SharedState) => state.ballot?.contests
);

export const getContestsUserData = createSelector(
	getSharedState,
	(state: SharedState) => state.ballotUserData?.contests
);

export const getBallotUserData = createSelector(
	getSharedState,
	(state: SharedState) => state.ballotUserData
);

export const getContestsAndContestsUserData = createSelector(
	getContests,
	getContestsUserData,
	(contests, contestsUserData) => ({
		contests,
		contestsUserData,
	})
);

export const getConfig = createSelector(
	getSharedState,
	(state: SharedState) => state?.config
);

export const getConfigIdentification = createSelector(
	getSharedState,
	(state: SharedState) => state?.config.identification
);

export const getChoiceReturnCodes = createSelector(
	getSharedState,
	(state: SharedState) => state.choiceReturnCodes
);

export const getVerifyData = createSelector(
	getChoiceReturnCodes,
	getContests,
	getContestsUserData,
	(choiceReturnCodes, contests, contestsUserData) => ({
		choiceReturnCodes,
		contests,
		contestsUserData,
	})
);

export const getVoteCastReturnCode = createSelector(
	getSharedState,
	(state: SharedState) => state.voteCastReturnCode
);

export const getSentButNotCast = createSelector(
	getSharedState,
	(state: SharedState) => state.sentButNotCast
);

export const getVoteCastedInPreviousSession = createSelector(
	getSharedState,
	(state: SharedState) => state.cast
);

export const getElectionEventId = createSelector(
	getSharedState,
	(state: SharedState) => state.config.electionEventId
);

export const getWriteInAlphabet = createSelector(
	getBallot,
	(ballot) => ballot?.writeInAlphabet
);
