/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {Injectable} from '@angular/core';
import {Actions, concatLatestFrom, createEffect, ofType} from '@ngrx/effects';
import {Store} from '@ngrx/store';
import {BackendService} from '@vp/data-access';
import {
	AuthenticateVoterResponseForCastVote,
	AuthenticateVoterResponseForSentVote,
	AuthenticateVoterResponseForUnsentVote,
	BackendError,
	Ballot,
	VotingCardState,
} from '@vp/util-types';
import {from, merge, of} from 'rxjs';
import {catchError, exhaustMap, filter, map, share} from 'rxjs/operators';
import {
	LanguageSelectorActions,
	LegalTermsActions,
	ReviewActions,
	SharedActions,
	StartVotingActions,
	VerifyActions,
} from '../actions/shared-state.actions';
import {catchConnectionError} from '../operators/catch-connection-error.operator';

import * as SharedOperators from '../operators/shared-state.operators';
import * as SharedReducers from '../reducer/shared-state.reducer';
import * as SharedSelectors from '../selectors/shared-state.selectors';
import {ConfigurationService} from '@vp/ui-services';

@Injectable()
export class SharedStateEffects {

	constructor(
		private readonly actions$: Actions,
		private readonly store: Store<SharedReducers.SharedState>,
		private readonly backendService: BackendService,
		private readonly configuration: ConfigurationService
	) {
	}

	clearBackendError$ = createEffect(() =>
		this.actions$.pipe(
			ofType(LegalTermsActions.agreeClicked),
			map(() => SharedActions.serverErrorCleared())
		)
	);

	translateBallot$ = createEffect(() =>
		this.actions$.pipe(
			ofType(LanguageSelectorActions.languageClicked),
			concatLatestFrom(() => [
				this.store.pipe(SharedOperators.getDefinedBallot),
				this.store.pipe(SharedOperators.getDefinedConfig),
			]),
			exhaustMap(([_action, ballot, config]) =>
				from(this.backendService.translateBallot(ballot, config.lang)).pipe(
					map((ballot: Ballot) =>
						LanguageSelectorActions.ballotTranslated({ballot})
					),
					catchError((error) =>
						of(LanguageSelectorActions.ballotTranslationFailed({error}))
					)
				)
			)
		)
	);

	authenticateVoter$ = createEffect(() => {
		const authenticateVoter$ = this.actions$.pipe(
			ofType(StartVotingActions.startClicked),
			concatLatestFrom(() => this.store.select(SharedSelectors.getConfig)),
			exhaustMap(([action, config]) =>
				from(this.backendService.authenticateVoter(action.voter, config)).pipe(
					catchConnectionError(this.configuration.requestTimeout.authenticateVoter),
					catchError((error) => of(error))
				)
			),
			share()
		);

		const authenticationFailed$ = authenticateVoter$.pipe(
			filter((response) => response instanceof BackendError),
			map((error) => StartVotingActions.authenticationFailed({error}))
		);

		const ballotLoaded$ = authenticateVoter$.pipe(
			filter((response): response is AuthenticateVoterResponseForUnsentVote => {
				return response.votingCardState === VotingCardState.Initial;
			}),
			map(({ballot}) => StartVotingActions.ballotLoaded({ballot}))
		);

		const choiceReturnCodesLoaded$ = authenticateVoter$.pipe(
			filter((response): response is AuthenticateVoterResponseForSentVote => {
				return response.votingCardState === VotingCardState.Sent;
			}),
			map(({choiceReturnCodes, ballot}) => {
				return StartVotingActions.choiceReturnCodesLoaded({
					choiceReturnCodes,
					ballot,
				});
			})
		);

		const voteCastReturnCodeLoaded$ = authenticateVoter$.pipe(
			filter((response): response is AuthenticateVoterResponseForCastVote => {
				return response.votingCardState === VotingCardState.Confirmed;
			}),
			map(({voteCastReturnCode}) => {
				return StartVotingActions.voteCastReturnCodeLoaded({
					voteCastReturnCode,
				});
			})
		);

		return merge(
			ballotLoaded$,
			choiceReturnCodesLoaded$,
			voteCastReturnCodeLoaded$,
			authenticationFailed$
		);
	});

	sendVote$ = createEffect(() =>
		this.actions$.pipe(
			ofType(ReviewActions.sealVoteClicked),
			concatLatestFrom(() => [
				this.store.pipe(SharedOperators.getDefinedBallot),
				this.store.pipe(SharedOperators.getDefinedBallotUserData),
			]),
			exhaustMap(([_action, ballot, ballotUserData]) =>
				from(this.backendService.sendVote(ballot, ballotUserData)).pipe(
					map((choiceReturnCodes) =>
						ReviewActions.sealedVoteLoaded({choiceReturnCodes})
					),
					catchConnectionError(this.configuration.requestTimeout.sendVote),
					catchError((error) =>
						of(ReviewActions.sealedVoteLoadFailed({error}))
					)
				)
			)
		)
	);

	confirmVote$ = createEffect(() =>
		this.actions$.pipe(
			ofType(VerifyActions.castVoteClicked),
			exhaustMap((action) =>
				from(this.backendService.confirmVote(action.confirmationKey)).pipe(
					map((voteCastReturnCode) =>
						VerifyActions.castVoteLoaded({voteCastReturnCode})
					),
					catchConnectionError(this.configuration.requestTimeout.confirmVote),
					catchError((error) => of(VerifyActions.castVoteLoadFailed({error})))
				)
			)
		)
	);
}
