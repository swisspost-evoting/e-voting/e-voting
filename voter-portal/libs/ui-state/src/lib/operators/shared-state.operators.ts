/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {select} from '@ngrx/store';
import {pipe} from 'rxjs';
import {filter, map} from 'rxjs/operators';
import * as SharedSelectors from '../selectors/shared-state.selectors';
import {Nullable} from '@vp/util-types';

function isDefined<T>(item: Nullable<T>): item is T {
	return !!item;
}

export const getDefinedBallot = pipe(
	select(SharedSelectors.getBallot),
	filter(isDefined)
);

export const getDefinedBallotUserData = pipe(
	select(SharedSelectors.getBallotUserData),
	filter(isDefined)
);

export const getDefinedConfig = pipe(
	select(SharedSelectors.getConfig),
	filter(isDefined)
);

export const getDefinedWriteInAlphabet = pipe(
	select(SharedSelectors.getWriteInAlphabet),
	filter(isDefined)
);

export const containsVoteResponse = pipe(
	select(SharedSelectors.getChoiceReturnCodes),
	map(isDefined)
);

export const getDefinedVoteCastReturnCode = pipe(
	select(SharedSelectors.getVoteCastReturnCode),
	filter(isDefined)
);

export const containsVoteCastReturnCode = pipe(
	select(SharedSelectors.getVoteCastReturnCode),
	map(isDefined)
);

export const getDefinedChoiceReturnCodes = pipe(
	select(SharedSelectors.getChoiceReturnCodes),
	filter(isDefined)
);

export const getDefinedBackendError = pipe(
	select(SharedSelectors.getBackendError),
	filter(isDefined)
);
