/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */

export * from './lib/faq-modal/faq-modal.component';
export * from './lib/faq.service';

export * from './lib/feature-faq.module';
