/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {registerLocaleData} from '@angular/common';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import localeDeCH from '@angular/common/locales/de-CH';
import localeEnGB from '@angular/common/locales/en-GB';
import localeFrCH from '@angular/common/locales/fr-CH';
import localeItCH from '@angular/common/locales/it-CH';
import localeRm from '@angular/common/locales/rm';
import {APP_INITIALIZER, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {NgbCollapseModule, NgbDateAdapter, NgbDateParserFormatter, NgbDatepickerI18n,} from '@ng-bootstrap/ng-bootstrap';
import {EffectsModule} from '@ngrx/effects';
import {StoreModule} from '@ngrx/store';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {TranslateCompiler, TranslateLoader, TranslateModule,} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {environment} from '@vp/data-access';
import {ChooseModule} from '@vp/step-choose';
import {ConfirmModule} from '@vp/step-confirm';
import {LegalTermsModule} from '@vp/step-legal-terms';
import {ReviewModule} from '@vp/step-review';
import {StartVotingModule} from '@vp/step-start-voting';
import {VerifyModule} from '@vp/step-verify';
import {UiComponentsModule} from '@vp/ui-components';
import {ConfigurationService, SwpDateAdapter, SwpDateParserFormatter, SwpDatepickerI18n, SwpTranslateCompiler,} from '@vp/ui-services';
import {metaReducers} from '@vp/ui-state';
import {AppRoutingModule} from './app-routing.module';

import {AppComponent} from './app.component';
import {HeaderComponent} from './header/header.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {StepperComponent} from './stepper/stepper.component';

registerLocaleData(localeDeCH, 'de');
registerLocaleData(localeItCH, 'it');
registerLocaleData(localeFrCH, 'fr');
registerLocaleData(localeRm, 'rm');
registerLocaleData(localeEnGB, 'en');

export function HttpLoaderFactory(http: HttpClient) {
	return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
	declarations: [
		AppComponent,
		HeaderComponent,
		StepperComponent,
		PageNotFoundComponent,
	],
	imports: [
		BrowserModule,
		StoreModule.forRoot(
			{},
			{
				metaReducers,
				runtimeChecks: {
					strictActionImmutability: true,
					strictStateImmutability: true,
				},
			}
		),
		EffectsModule.forRoot([]),
		!environment.production ? StoreDevtoolsModule.instrument() : [],
		HttpClientModule,
		TranslateModule.forRoot({
			defaultLanguage:
				environment.availableLanguages
					.map((l) => l.id)
					.find((l) => navigator.language.includes(l)) || 'de',
			loader: {
				provide: TranslateLoader,
				useFactory: HttpLoaderFactory,
				deps: [HttpClient],
			},
			compiler: {
				provide: TranslateCompiler,
				useClass: SwpTranslateCompiler,
				deps: [ConfigurationService],
			},
		}),
		LegalTermsModule,
		StartVotingModule,
		ChooseModule,
		ReviewModule,
		VerifyModule,
		ConfirmModule,
		NgbCollapseModule,
		UiComponentsModule,
		AppRoutingModule, // keep after all page modules for correct rooting
	],
	providers: [
		{
			provide: APP_INITIALIZER,
			useFactory: (configService: ConfigurationService) => () =>
				configService.fetchPortalConfiguration(),
			deps: [ConfigurationService],
			multi: true,
		},
		{ provide: NgbDateAdapter, useClass: SwpDateAdapter },
		{ provide: NgbDateParserFormatter, useClass: SwpDateParserFormatter },
		{ provide: NgbDatepickerI18n, useClass: SwpDatepickerI18n },
	],
	bootstrap: [AppComponent],
})
export class AppModule {}
