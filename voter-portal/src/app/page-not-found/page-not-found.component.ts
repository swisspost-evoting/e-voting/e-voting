/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {Component} from '@angular/core';

@Component({
	selector: 'vp-page-not-found',
	templateUrl: './page-not-found.component.html',
})
export class PageNotFoundComponent {}
