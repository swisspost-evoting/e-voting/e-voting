/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {Location} from '@angular/common';
import {fakeAsync, TestBed, tick} from '@angular/core/testing';
import {Router} from '@angular/router';
import {RouterTestingModule} from '@angular/router/testing';

import {routes} from './app-routing.module';

describe('AppRoutingModule', () => {
	let router: Router;
	let location: Location;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			imports: [RouterTestingModule.withRoutes(routes)],
		}).compileComponents();
	});

	beforeEach(() => {
		router = TestBed.inject(Router);
		location = TestBed.inject(Location);
		router.initialNavigation();
	});

	it('should redirect to the page not found if no route is set', () => {
		expect(location.path()).toBe('/page-not-found');
	});

	it('should redirect to the page not found when an invalid route is set', fakeAsync(() => {
		router.navigate(['/invalid-path']);

		tick();

		expect(location.path()).toBe('/page-not-found');
	}));
});
