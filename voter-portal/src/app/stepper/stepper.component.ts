/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {Component, OnDestroy, OnInit} from '@angular/core';
import {Title} from '@angular/platform-browser';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {concatLatestFrom} from '@ngrx/effects';
import {TranslateService} from '@ngx-translate/core';
import {AdditionalLegalTerms, VotingStep} from '@vp/util-types';
import {startWith, Subject} from 'rxjs';
import {filter, map, switchMap, takeUntil} from 'rxjs/operators';
import {ConfigurationService} from '@vp/ui-services';

@Component({
	selector: 'vp-stepper',
	templateUrl: './stepper.component.html',
})
export class StepperComponent implements OnInit, OnDestroy {
	steps = Object.values(VotingStep);
	currentStep: VotingStep | undefined;
	currentStepIndex: number | undefined;
	additionalFirstStep?: string;
	destroy$ = new Subject<void>();

	constructor(
		private readonly router: Router,
		private readonly activatedRoute: ActivatedRoute,
		private readonly translate: TranslateService,
		private readonly titleService: Title,
		private readonly configuration: ConfigurationService
	) {}

	ngOnInit() {
		this.translate.onLangChange
			.pipe(
				startWith({ lang: this.translate.currentLang }),
				map(({ lang }) => {
					const { additionalLegalTerms } = this.configuration;
					return (
						additionalLegalTerms &&
						additionalLegalTerms[lang as keyof AdditionalLegalTerms]
					);
				}),
				takeUntil(this.destroy$)
			)
			.subscribe((additionalLegalTerms) => {
				this.additionalFirstStep = additionalLegalTerms?.stepper;
			});

		const currentStepChanges$ = this.router.events.pipe(
			filter((routerEvent) => routerEvent instanceof NavigationEnd),
			map(() => {
				const currentRoute = this.activatedRoute.snapshot.firstChild;
				return currentRoute ? currentRoute.url[0].path : undefined;
			})
		);

		currentStepChanges$
			.pipe(takeUntil(this.destroy$))
			.subscribe((currentStep) => {
				this.currentStep = currentStep as VotingStep;
				this.currentStepIndex = this.steps.findIndex(
					(step) => step === currentStep
				);

				this.setFirstFocus();
			});

		currentStepChanges$
			.pipe(
				filter((currentStep): currentStep is string => !!currentStep),
				switchMap((currentStep) =>
					this.translate.stream(`stepper.${currentStep}`)
				),
				concatLatestFrom(() => this.translate.get('common.pageTitle')),
				takeUntil(this.destroy$)
			)
			.subscribe(([stepName, pageTitle]) => {
				this.titleService.setTitle(
					pageTitle + (stepName ? ` - ${stepName}` : '')
				);
			});
	}

	ngOnDestroy() {
		this.destroy$.next();
		this.destroy$.complete();
	}

	private setFirstFocus(): void {
		window.requestAnimationFrame(() => {
			const stepper = document.querySelector<HTMLElement>(
				'#voting-progress-stepper [aria-current="step"]'
			);

			if (stepper) {
				stepper.focus();
			}
		});
	}
}
