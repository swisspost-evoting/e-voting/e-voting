/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.process.preconfigure;

import java.util.List;

import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.BallotQuestionType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.TieBreakQuestionType;

/**
 * Adapter for {@link TieBreakQuestionType}.
 */
public final class TieBreakQuestionTypeAdapter implements QuestionType {

	final TieBreakQuestionType tieBreakQuestionType;

	public TieBreakQuestionTypeAdapter(final TieBreakQuestionType tieBreakQuestionType) {
		this.tieBreakQuestionType = tieBreakQuestionType;
	}

	@Override
	public String getQuestionIdentification() {
		return tieBreakQuestionType.getQuestionIdentification();
	}

	@Override
	public BallotQuestionType getBallotQuestion() {
		return tieBreakQuestionType.getBallotQuestion();
	}

	@Override
	public String getQuestionNumber() {
		return tieBreakQuestionType.getQuestionNumber();
	}

	@Override
	public List<AnswerType> getAnswer() {
		return tieBreakQuestionType.getAnswer().stream()
				.parallel()
				.<AnswerType>map(TiebreakAnswerTypeAdapter::new)
				.toList();
	}
}
