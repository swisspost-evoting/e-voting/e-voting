/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.protocol.configuration.setupvoting;

import static ch.post.it.evoting.securedatamanager.shared.Constants.TOO_SMALL_CHUNK_SIZE_MESSAGE;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.evotinglibraries.domain.election.PrimesMappingTable;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.ElectionEventContextPayload;
import ch.post.it.evoting.securedatamanager.setup.process.SetupKeyPairService;
import ch.post.it.evoting.securedatamanager.setup.process.precompute.VerificationCardSet;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventContextPayloadService;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventService;

/**
 * Service that invokes the GenVerDat algorithm.
 */
@Service
@ConditionalOnProperty("role.isSetup")
public class GenVerDatService {

	private static final Logger LOGGER = LoggerFactory.getLogger(GenVerDatService.class);

	private final GenVerDatAlgorithm genVerDatAlgorithm;
	private final ElectionEventService electionEventService;
	private final SetupKeyPairService setupKeyPairService;
	private final ElectionEventContextPayloadService electionEventContextPayloadService;

	@Value("${choiceCodeGenerationChunkSize:100}")
	private int chunkSize;

	@Autowired
	public GenVerDatService(
			final GenVerDatAlgorithm genVerDatAlgorithm,
			final ElectionEventService electionEventService,
			final SetupKeyPairService setupKeyPairService,
			final ElectionEventContextPayloadService electionEventContextPayloadService) {
		this.genVerDatAlgorithm = genVerDatAlgorithm;
		this.electionEventService = electionEventService;
		this.setupKeyPairService = setupKeyPairService;
		this.electionEventContextPayloadService = electionEventContextPayloadService;
	}

	/**
	 * Invokes the GenVerDat algorithm.
	 *
	 * @param precomputeContext             the context identifiers. Must be non-null.
	 * @param numberOfVotingCardsToGenerate the number of voting cards to generate. Must be positive.
	 */
	public List<GenVerDatOutput> genVerDat(final VerificationCardSet precomputeContext, final int numberOfVotingCardsToGenerate) {
		checkNotNull(precomputeContext);
		checkArgument(numberOfVotingCardsToGenerate >= 0, "The number of voting cards to generate must be positive.");
		checkArgument(chunkSize > 0, TOO_SMALL_CHUNK_SIZE_MESSAGE);

		final String electionEventId = precomputeContext.electionEventId();
		final String verificationCardSetId = precomputeContext.verificationCardSetId();

		checkArgument(electionEventService.exists(electionEventId), "The election event id does not exist.");

		// Retrieve the election event context payload.
		final ElectionEventContextPayload electionEventContextPayload = electionEventContextPayloadService.load(electionEventId);
		final GqGroup encryptionGroup = electionEventContextPayload.getEncryptionGroup();
		final int maximumNumberOfVotingOptions = electionEventContextPayload.getElectionEventContext().maximumNumberOfVotingOptions();

		// Get the setup public key.
		final ElGamalMultiRecipientPublicKey setupPublicKey = setupKeyPairService.load(electionEventId).getPublicKey();
		checkState(encryptionGroup.equals(setupPublicKey.getGroup()), "The group of the setup public key must be equal to the encryption group.");

		// Retrieve the primes mapping table.
		final PrimesMappingTable primesMappingTable = electionEventContextPayloadService.loadPrimesMappingTable(electionEventId,
				verificationCardSetId);

		// Build full-sized chunks (i.e. with `chunkSize` elements)
		final int fullChunkCount = numberOfVotingCardsToGenerate / chunkSize;
		final List<GenVerDatOutput> genVerDatOutputs = IntStream.range(0, fullChunkCount)
				.mapToObj(chunkId -> {

					LOGGER.debug("Generating verification data... [electionEventId: {}, verificationCardSetId: {}, chunkId: {}]", electionEventId,
							verificationCardSetId, chunkId);

					// Generate verification data. Since we chunk the payloads, we are not directly working with the number of eligible voters (N_E),
					// but rather with the chunk size as context of the algorithm.
					final GenVerDatContext genVerDatContext = new GenVerDatContext(encryptionGroup, electionEventId, chunkSize, primesMappingTable,
							maximumNumberOfVotingOptions);
					final GenVerDatOutput genVerDatOutput = genVerDatAlgorithm.genVerDat(genVerDatContext, setupPublicKey);

					LOGGER.info(
							"Successfully generated the verification data. [electionEventId: {}, verificationCardSetId: {}, chunkId: {}]",
							electionEventId, verificationCardSetId, chunkId);

					return genVerDatOutput;
				})
				.collect(Collectors.toCollection(ArrayList::new));

		// Build an eventual last chunk with the remaining elements.
		final int lastChunkSize = numberOfVotingCardsToGenerate % chunkSize;
		if (lastChunkSize > 0) {
			LOGGER.debug("Generating verification data... [electionEventId: {}, verificationCardSetId: {}, chunkId: {}]", electionEventId,
					verificationCardSetId, fullChunkCount);
			final GenVerDatContext genVerDatContext = new GenVerDatContext(encryptionGroup, electionEventId, lastChunkSize, primesMappingTable,
					maximumNumberOfVotingOptions);
			genVerDatOutputs.add(genVerDatAlgorithm.genVerDat(genVerDatContext, setupPublicKey));
			LOGGER.info(
					"Successfully generated the verification data. [electionEventId: {}, verificationCardSetId: {}, chunkId: {}]", electionEventId,
					verificationCardSetId, fullChunkCount);
		}

		LOGGER.info(
				"Successfully executed the GenVerDat algorithm. [electionEventId: {}, verificationCardSetId: {}]", electionEventId,
				verificationCardSetId);

		return genVerDatOutputs;
	}

}

