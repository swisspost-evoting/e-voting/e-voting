/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.protocol.configuration.setupvoting;

import static ch.post.it.evoting.evotinglibraries.domain.ControlComponentConstants.NODE_IDS;
import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import com.google.common.base.Preconditions;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.SchnorrProof;
import ch.post.it.evoting.evotinglibraries.domain.election.ControlComponentPublicKeys;
import ch.post.it.evoting.evotinglibraries.domain.election.ElectionEventContext;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.ElectionEventContextPayload;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventContextPayloadService;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventService;

@Service
@ConditionalOnProperty("role.isSetup")
public class GenVerCardSetKeysService {

	private static final Logger LOGGER = LoggerFactory.getLogger(GenVerCardSetKeysService.class);

	private final GenVerCardSetKeysAlgorithm genVerCardSetKeysAlgorithm;
	private final ElectionEventService electionEventService;
	private final ElectionEventContextPayloadService electionEventContextPayloadService;

	public GenVerCardSetKeysService(final GenVerCardSetKeysAlgorithm genVerCardSetKeysAlgorithm,
			final ElectionEventService electionEventService,
			final ElectionEventContextPayloadService electionEventContextPayloadService) {
		this.genVerCardSetKeysAlgorithm = genVerCardSetKeysAlgorithm;
		this.electionEventService = electionEventService;
		this.electionEventContextPayloadService = electionEventContextPayloadService;
	}

	/**
	 * Invokes the GenVerCardSetKeys algorithm.
	 *
	 * @param electionEventId            the election event id. Must be non-null and a valid UUID.
	 * @param controlComponentPublicKeys the control component public keys. Must be non-null.
	 */
	public ElGamalMultiRecipientPublicKey genVerCardSetKeys(final String electionEventId,
			final List<ControlComponentPublicKeys> controlComponentPublicKeys) {
		validateUUID(electionEventId);
		checkArgument(electionEventService.exists(electionEventId));

		final List<ControlComponentPublicKeys> controlComponentPublicKeysCopy = checkNotNull(controlComponentPublicKeys).stream()
				.map(Preconditions::checkNotNull)
				.toList();
		checkArgument(controlComponentPublicKeysCopy.size() == NODE_IDS.size(),
				"Wrong number of control component public keys. [expected: %s, actual: %s]", NODE_IDS, controlComponentPublicKeysCopy.size());

		final ElectionEventContextPayload electionEventContextPayload = electionEventContextPayloadService.load(electionEventId);
		final GqGroup encryptionGroup = electionEventContextPayload.getEncryptionGroup();
		final ElectionEventContext electionEventContext = electionEventContextPayload.getElectionEventContext();
		final int maximumNumberOfSelections = electionEventContext.maximumNumberOfSelections();

		final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> ccrjChoiceReturnCodesEncryptionPublicKey = controlComponentPublicKeysCopy.stream()
				.map(ControlComponentPublicKeys::ccrjChoiceReturnCodesEncryptionPublicKey)
				.collect(GroupVector.toGroupVector());
		final GroupVector<GroupVector<SchnorrProof, ZqGroup>, ZqGroup> ccrjSchnorrProofs = controlComponentPublicKeysCopy.stream()
				.map(ControlComponentPublicKeys::ccrjSchnorrProofs)
				.collect(GroupVector.toGroupVector());

		final GenVerCardSetKeysContext context = new GenVerCardSetKeysContext(encryptionGroup, electionEventId, maximumNumberOfSelections);
		final GenVerCardSetKeysInput input = new GenVerCardSetKeysInput(ccrjChoiceReturnCodesEncryptionPublicKey, ccrjSchnorrProofs);

		LOGGER.debug("Performing GenVerCardSetKeys algorithm... [electionEventId: {}]", electionEventId);

		return genVerCardSetKeysAlgorithm.genVerCardSetKeys(context, input);
	}
}
