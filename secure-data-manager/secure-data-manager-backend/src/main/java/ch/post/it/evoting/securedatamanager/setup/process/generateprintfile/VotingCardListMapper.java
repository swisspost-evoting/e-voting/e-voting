/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.process.generateprintfile;

import static ch.post.it.evoting.evotinglibraries.domain.election.ElectionAttributesAliasConstants.ALIAS_JOIN_DELIMITER;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import ch.post.it.evoting.cryptoprimitives.math.PrimeGqElement;
import ch.post.it.evoting.domain.configuration.ChoiceReturnCodeToEncodedVotingOptionEntry;
import ch.post.it.evoting.domain.configuration.VoterReturnCodes;
import ch.post.it.evoting.domain.configuration.setupvoting.VoterInitialCodes;
import ch.post.it.evoting.evotinglibraries.domain.election.PrimesMappingTable;
import ch.post.it.evoting.evotinglibraries.domain.election.PrimesMappingTableEntry;
import ch.post.it.evoting.evotinglibraries.domain.election.VotingOptionType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.AuthorizationObjectType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.AuthorizationType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.BallotType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.CandidatePositionType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.Configuration;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.DomainOfInfluenceType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.ElectionInformationType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.VariantBallotType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.VoteInformationType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingprint.AnswerType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingprint.CandidateListType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingprint.CandidateType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingprint.ContestType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingprint.ElectionType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingprint.ListType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingprint.QuestionType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingprint.VoteType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingprint.VotingCardList;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingprint.VotingCardType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingprint.WriteInCandidateType;
import ch.post.it.evoting.securedatamanager.setup.process.VoterInitialCodesPayloadService.VoterInitialCodesByVcs;

/**
 * Maps to {@link VotingCardList}.
 */
public class VotingCardListMapper {

	private VotingCardListMapper() {
		// static usage only.
	}

	/**
	 * Returns the voting card list to be output in evoting-print.
	 *
	 * @param configuration               the configuration of the event.
	 * @param voterInitialCodesPayloadMap the map of voter identification and voter initial codes payload.
	 * @param primesMappingTableMap       the map of verificationCardSetId and primes mapping table.
	 * @return the root object VotingCardList of the evoting-print file.
	 * @throws NullPointerException if any input is null.
	 */
	public static VotingCardList toVotingCardList(final Configuration configuration,
			final Map<String, VoterInitialCodesByVcs> voterInitialCodesPayloadMap, final Map<String, VoterReturnCodes> voterReturnCodesMap,
			final Map<String, PrimesMappingTable> primesMappingTableMap) {

		checkNotNull(configuration);
		checkNotNull(voterInitialCodesPayloadMap);
		checkNotNull(primesMappingTableMap);

		// Prepare reused list
		final List<AuthorizationType> authorizationTypeList = configuration.getAuthorizations().getAuthorization();
		final List<ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.VoteType> voteTypeList = configuration.getContest()
				.getVoteInformation().stream().parallel()
				.map(VoteInformationType::getVote)
				.toList();

		final List<ElectionInformationTypeExtended> electionInformationTypeExtendedList = configuration.getContest().getElectionGroupBallot().stream()
				.parallel()
				.flatMap(electionGroupBallotType -> electionGroupBallotType.getElectionInformation().stream().parallel()
						.map(electionInformationType -> new ElectionInformationTypeExtended(electionInformationType,
								electionGroupBallotType.getDomainOfInfluence())))
				.toList();

		final List<VotingCardType> votingCardTypesList = configuration.getRegister().getVoter().stream().parallel()
				.map(voterType -> {
					final List<String> domainOfInfluenceList = authorizationTypeList.stream().parallel()
							.filter(authorizationType -> authorizationType.getAuthorizationIdentification().equals(voterType.getAuthorization()))
							.map(AuthorizationType::getAuthorizationObject)
							.flatMap(Collection::stream)
							.map(AuthorizationObjectType::getDomainOfInfluence)
							.map(DomainOfInfluenceType::getDomainOfInfluenceIdentification)
							.toList();

					final String voterIdentification = voterType.getVoterIdentification();
					final VoterInitialCodesByVcs voterInitialCodesByVcs = voterInitialCodesPayloadMap.get(voterIdentification);
					final VoterInitialCodes voterInitialCodes = voterInitialCodesByVcs.voterInitialCodes();
					final VoterReturnCodes voterReturnCodes = voterReturnCodesMap.get(voterInitialCodes.verificationCardId());
					final Map<PrimeGqElement, String> encodedVotingOptionToChoiceCodeMap = voterReturnCodes.choiceReturnCodesToEncodedVotingOptions()
							.stream()
							.collect(Collectors.toMap(ChoiceReturnCodeToEncodedVotingOptionEntry::encodedVotingOption,
									ChoiceReturnCodeToEncodedVotingOptionEntry::choiceReturnCode));
					final PrimesMappingTable primesMappingTable = primesMappingTableMap.get(voterInitialCodesByVcs.verificationCardSetId());

					final VotingCardType votingCard = new VotingCardType();
					votingCard.setVoterIdentification(voterIdentification);
					votingCard.setVotingCardId(voterInitialCodes.votingCardId());
					votingCard.setStartVotingKey(voterInitialCodes.startVotingKey());
					votingCard.setBallotCastingKey(voterInitialCodes.ballotCastingKey());
					votingCard.setVoteCastReturnCode(voterReturnCodes.voteCastReturnCode());

					// Votes
					if (!voteTypeList.isEmpty()) {
						votingCard.setVote(toVotes(voteTypeList, domainOfInfluenceList, primesMappingTable, encodedVotingOptionToChoiceCodeMap));
					}

					// Elections
					if (!electionInformationTypeExtendedList.isEmpty()) {
						votingCard.setElection(toElections(electionInformationTypeExtendedList, domainOfInfluenceList, primesMappingTable,
								encodedVotingOptionToChoiceCodeMap));
					}

					return votingCard;
				})
				.toList();

		final ContestType contest = new ContestType();
		contest.setContestIdentification(configuration.getContest().getContestIdentification());
		contest.setVotingCard(votingCardTypesList);

		final VotingCardList votingCardList = new VotingCardList();
		votingCardList.setContest(contest);

		return votingCardList;
	}

	private static List<VoteType> toVotes(final List<ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.VoteType> voteTypeList,
			final List<String> domainOfInfluenceList, final PrimesMappingTable primesMappingTable,
			final Map<PrimeGqElement, String> encodedVotingOptionToChoiceCodeMap) {
		return voteTypeList.stream().parallel()
				.filter(configurationVoteType -> domainOfInfluenceList.contains(configurationVoteType.getDomainOfInfluence()))
				.map(configurationVoteType -> {
					final String voteIdentification = configurationVoteType.getVoteIdentification();
					final VoteType voteType = new VoteType();
					voteType.setVoteIdentification(voteIdentification);

					// StandardBallot
					voteType.getQuestion()
							.addAll(toStandardBallotQuestions(configurationVoteType, encodedVotingOptionToChoiceCodeMap, primesMappingTable));

					// VariantBallot -> StandardQuestion && VariantBallot -> TieBreakQuestion
					final List<VariantBallotType> variantBallotTypeList = configurationVoteType.getBallot().stream().parallel()
							.map(BallotType::getVariantBallot)
							.filter(Objects::nonNull)
							.toList();

					voteType.getQuestion()
							.addAll(toVariantBallotStandardQuestions(variantBallotTypeList, primesMappingTable,
									encodedVotingOptionToChoiceCodeMap));
					voteType.getQuestion()
							.addAll(toVariantBallotTieBreakQuestions(variantBallotTypeList, primesMappingTable,
									encodedVotingOptionToChoiceCodeMap));

					return voteType;
				})
				.toList();
	}

	private static List<QuestionType> toStandardBallotQuestions(
			final ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.VoteType configurationVoteType,
			final Map<PrimeGqElement, String> encodedVotingOptionToChoiceCodeMap, final PrimesMappingTable primesMappingTable) {
		return configurationVoteType.getBallot().stream().parallel()
				.map(BallotType::getStandardBallot)
				.filter(Objects::nonNull)
				.map(standardBallotType -> {
					final List<AnswerType> answers = standardBallotType.getAnswer().stream()
							.map(standardAnswerType -> {
								final String actualVotingOption = String.join(ALIAS_JOIN_DELIMITER, standardBallotType.getQuestionIdentification(),
										standardAnswerType.getAnswerIdentification());
								final String choiceCode = getChoiceCodeFromActualVotingOption(actualVotingOption, primesMappingTable,
										encodedVotingOptionToChoiceCodeMap)
										.orElseThrow(() -> new IllegalStateException("Standard ballot answer choice code must not be null"));
								return new AnswerType()
										.withAnswerIdentification(standardAnswerType.getAnswerIdentification())
										.withChoiceReturnCode(choiceCode);
							})
							.toList();

					return new QuestionType()
							.withQuestionIdentification(standardBallotType.getQuestionIdentification())
							.withAnswer(answers);
				})
				.toList();
	}

	private static List<QuestionType> toVariantBallotStandardQuestions(final List<VariantBallotType> variantBallotTypeList,
			final PrimesMappingTable primesMappingTable, final Map<PrimeGqElement, String> encodedVotingOptionToChoiceCodeMap) {
		return variantBallotTypeList.stream()
				.map(VariantBallotType::getStandardQuestion)
				.flatMap(Collection::stream)
				.map(standardQuestionType -> {
					final List<AnswerType> answers = standardQuestionType.getAnswer().stream()
							.map(standardAnswerType -> {
								final String actualVotingOption = String.join(ALIAS_JOIN_DELIMITER, standardQuestionType.getQuestionIdentification(),
										standardAnswerType.getAnswerIdentification());
								final String choiceCode = getChoiceCodeFromActualVotingOption(actualVotingOption, primesMappingTable,
										encodedVotingOptionToChoiceCodeMap)
										.orElseThrow(() -> new IllegalStateException("Variant ballot standard answer choice code must not be null"));

								return new AnswerType()
										.withAnswerIdentification(standardAnswerType.getAnswerIdentification())
										.withChoiceReturnCode(choiceCode);
							})
							.toList();

					return new QuestionType()
							.withQuestionIdentification(standardQuestionType.getQuestionIdentification())
							.withAnswer(answers);
				})
				.toList();
	}

	private static List<QuestionType> toVariantBallotTieBreakQuestions(final List<VariantBallotType> variantBallotTypeList,
			final PrimesMappingTable primesMappingTable, final Map<PrimeGqElement, String> encodedVotingOptionToChoiceCodeMap) {
		return variantBallotTypeList.stream()
				.map(VariantBallotType::getTieBreakQuestion)
				.flatMap(Collection::stream)
				.map(tieBreakQuestionType -> {
					final List<AnswerType> answers = tieBreakQuestionType.getAnswer().stream()
							.map(tiebreakAnswerType -> {
								final String actualVotingOption = String.join(ALIAS_JOIN_DELIMITER, tieBreakQuestionType.getQuestionIdentification(),
										tiebreakAnswerType.getAnswerIdentification());
								final String choiceCode = getChoiceCodeFromActualVotingOption(actualVotingOption, primesMappingTable,
										encodedVotingOptionToChoiceCodeMap)
										.orElseThrow(() -> new IllegalStateException("Variant ballot tiebreak answer choice code must not be null"));

								return new AnswerType()
										.withAnswerIdentification(tiebreakAnswerType.getAnswerIdentification())
										.withChoiceReturnCode(choiceCode);
							})
							.toList();

					return new QuestionType()
							.withQuestionIdentification(tieBreakQuestionType.getQuestionIdentification())
							.withAnswer(answers);
				})
				.toList();
	}

	private static List<ElectionType> toElections(final List<ElectionInformationTypeExtended> electionInformationTypeExtendedList,
			final List<String> domainOfInfluenceList, final PrimesMappingTable primesMappingTable,
			final Map<PrimeGqElement, String> encodedVotingOptionToChoiceCodeMap) {

		return electionInformationTypeExtendedList.stream().parallel()
				.filter(configurationElectionInformationTypeExtended -> domainOfInfluenceList.contains(
						configurationElectionInformationTypeExtended.domainOfInfluence))
				.map(configurationElectionInformationTypeExtended -> {
					final ElectionInformationType configurationElectionInformationType = configurationElectionInformationTypeExtended.electionInformationType;
					final ElectionType electionType = new ElectionType();
					electionType.setElectionIdentification(configurationElectionInformationType.getElection().getElectionIdentification());

					// Candidates
					// Only for elections without defined lists
					if (configurationElectionInformationType.getList().stream()
							.allMatch(ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.ListType::isListEmpty)) {
						electionType.getCandidate()
								.addAll(toCandidates(configurationElectionInformationType, primesMappingTable, encodedVotingOptionToChoiceCodeMap));
					}

					// Lists
					electionType.getList()
							.addAll(toLists(configurationElectionInformationType, primesMappingTable, encodedVotingOptionToChoiceCodeMap));

					// Write-ins
					if (configurationElectionInformationType.getElection().isWriteInsAllowed()) {
						electionType.getWriteInCandidate()
								.addAll(toWriteInsChoiceCodes(configurationElectionInformationType.getElection().getElectionIdentification(),
										primesMappingTable, encodedVotingOptionToChoiceCodeMap, configurationElectionInformationType));
					}

					return electionType;
				})
				.toList();
	}

	private static List<CandidateType> toCandidates(final ElectionInformationType configurationElectionInformationType,
			final PrimesMappingTable primesMappingTable, final Map<PrimeGqElement, String> encodedVotingOptionToChoiceCodeMap) {
		final String electionIdentification = configurationElectionInformationType.getElection().getElectionIdentification();
		return configurationElectionInformationType.getCandidate().stream()
				.map(configurationCandidateType -> {
					final List<String> choiceCodes = IntStream.range(0,
									configurationElectionInformationType.getElection().getCandidateAccumulation().intValue())
							.mapToObj(String::valueOf)
							.map(acc -> String.join(ALIAS_JOIN_DELIMITER, electionIdentification,
									configurationCandidateType.getCandidateIdentification(), acc))
							.map(actualVotingOption -> {
								final List<String> choiceCodeList = getChoiceCodeListFromActualVotingOption(
										actualVotingOption, primesMappingTable, encodedVotingOptionToChoiceCodeMap);

								checkState(!choiceCodeList.isEmpty(), "Candidate choice code list must not be empty");

								return choiceCodeList;
							})
							.flatMap(List::stream).toList();

					return new CandidateType()
							.withCandidateIdentification(configurationCandidateType.getCandidateIdentification())
							.withChoiceReturnCode(choiceCodes);
				})
				.filter(candidateType -> !candidateType.getChoiceReturnCode().isEmpty())
				.toList();
	}

	private static List<ListType> toLists(final ElectionInformationType configurationElectionInformationType,
			final PrimesMappingTable primesMappingTable, final Map<PrimeGqElement, String> encodedVotingOptionToChoiceCodeMap) {
		final String electionIdentification = configurationElectionInformationType.getElection().getElectionIdentification();
		return configurationElectionInformationType.getList().stream()
				.map(configListType -> {
					final String actualVotingOption = String.join(ALIAS_JOIN_DELIMITER, electionIdentification,
							configListType.getListIdentification());
					final String choiceCode = getChoiceCodeFromActualVotingOption(actualVotingOption, primesMappingTable,
							encodedVotingOptionToChoiceCodeMap)
							.orElse(null);

					final List<CandidateListType> candidateListTypes;
					if (!configListType.isListEmpty()) {
						candidateListTypes = toCandidateLists(configListType.getCandidatePosition(), primesMappingTable,
								encodedVotingOptionToChoiceCodeMap, electionIdentification, configurationElectionInformationType.getElection());
					} else {
						candidateListTypes = toCandidateEmptyLists(configListType.getCandidatePosition(), primesMappingTable,
								encodedVotingOptionToChoiceCodeMap, electionIdentification);
					}

					return new ListType()
							.withListIdentification(configListType.getListIdentification())
							.withChoiceReturnCode(choiceCode)
							.withCandidate(candidateListTypes);
				})
				.toList();
	}

	private static List<WriteInCandidateType> toWriteInsChoiceCodes(final String electionIdentification,
			final PrimesMappingTable primesMappingTable,
			final Map<PrimeGqElement, String> encodedVotingOptionToChoiceCodeMap,
			final ElectionInformationType configurationElectionInformationType) {

		// find all write-in aliases of this election
		final Set<String> allWriteInAliases = primesMappingTable.getPTable().stream()
				.parallel()
				.filter(primesMappingTableEntry -> primesMappingTableEntry.semanticInformation().startsWith(VotingOptionType.WRITE_IN.name()))
				.map(PrimesMappingTableEntry::actualVotingOption)
				.filter(actualVotingOption -> actualVotingOption.startsWith(electionIdentification))
				.collect(Collectors.toSet());
		checkState(allWriteInAliases.size() == configurationElectionInformationType.getWriteInCandidate().size(),
				"The number of write-in aliases must be equal to the number of write-in candidates for the election. [electionIdentification: %s]",
				electionIdentification);

		return configurationElectionInformationType.getWriteInCandidate().stream()
				.parallel()
				.sorted(Comparator.comparing(ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.WriteInCandidateType::getPosition))
				.map(ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.WriteInCandidateType::getWriteInCandidateIdentification)
				.map(writeInCandidateIdentification -> {
					final String writeInAlias = String.join(ALIAS_JOIN_DELIMITER, electionIdentification, writeInCandidateIdentification);
					checkState(allWriteInAliases.contains(writeInAlias),
							"At least one write-in candidate does not have an alias. [writeInAliasNotFound: %s]", writeInAlias);

					final String choiceCode = getChoiceCodeFromActualVotingOption(writeInAlias, primesMappingTable,
							encodedVotingOptionToChoiceCodeMap)
							.orElseThrow(() -> new IllegalStateException(
									String.format("The write-in candidate does not have a corresponding a choice code. [writeInAlias: %s]",
											writeInAlias)));
					return new WriteInCandidateType()
							.withWriteInCandidateIdentification(writeInCandidateIdentification)
							.withChoiceReturnCode(choiceCode);
				})
				.toList();
	}

	private static List<CandidateListType> toCandidateLists(final List<CandidatePositionType> candidatePositionTypeList,
			final PrimesMappingTable primesMappingTable, final Map<PrimeGqElement, String> encodedVotingOptionToChoiceCodeMap,
			final String electionIdentification, final ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.ElectionType electionType) {
		return candidatePositionTypeList.stream()
				.map(candidatePositionType ->
				{
					final List<String> choiceCodes = IntStream.range(0, electionType.getCandidateAccumulation().intValue())
							.mapToObj(String::valueOf)
							.map(acc -> String.join(ALIAS_JOIN_DELIMITER, electionIdentification,
									candidatePositionType.getCandidateIdentification(), acc))
							.map(actualVotingOption -> {
								final List<String> choiceCodeList = getChoiceCodeListFromActualVotingOption(
										actualVotingOption, primesMappingTable, encodedVotingOptionToChoiceCodeMap);

								checkState(!choiceCodeList.isEmpty(), "CandidateList choice code list must not be empty");

								return choiceCodeList;
							})
							.flatMap(List::stream)
							.toList();
					return new CandidateListType()
							.withCandidateListIdentification(candidatePositionType.getCandidateListIdentification())
							.withChoiceReturnCode(choiceCodes);
				})
				.toList();
	}

	private static List<CandidateListType> toCandidateEmptyLists(final List<CandidatePositionType> candidatePositionTypeList,
			final PrimesMappingTable primesMappingTable, final Map<PrimeGqElement, String> encodedVotingOptionToChoiceCodeMap,
			final String electionIdentification) {
		return candidatePositionTypeList.stream()
				.map(CandidatePositionType::getCandidateListIdentification)
				.map(candidateListIdentification -> {
					final String actualVotingOption = String.join(ALIAS_JOIN_DELIMITER, electionIdentification, candidateListIdentification);
					final String choiceCode = getChoiceCodeFromActualVotingOption(actualVotingOption, primesMappingTable,
							encodedVotingOptionToChoiceCodeMap)
							.orElseThrow(() -> new IllegalStateException(String.format(
									"The candidate list position does not have a corresponding a choice code. [candidateListIdentification: %s]",
									candidateListIdentification)));
					return new CandidateListType()
							.withCandidateListIdentification(candidateListIdentification)
							.withChoiceReturnCode(choiceCode);
				})
				.toList();
	}

	private static Optional<String> getChoiceCodeFromActualVotingOption(final String actualVotingOption, final PrimesMappingTable primesMappingTable,
			final Map<PrimeGqElement, String> encodedVotingOptionToChoiceCodeMap) {
		final List<String> choiceCodes = primesMappingTable.getPTable()
				.stream()
				.filter(primesMappingTableEntry -> primesMappingTableEntry.actualVotingOption().equals(actualVotingOption))
				.map(PrimesMappingTableEntry::encodedVotingOption)
				.map(encodedVotingOptionToChoiceCodeMap::get)
				.toList();

		if (choiceCodes.size() > 1) {
			throw new IllegalStateException(
					String.format("Unexpected number of choice code. [expectedNumber: 1, actualNumber: %s, actualVotingOption: %s]",
							choiceCodes.size(), actualVotingOption));
		}

		if (choiceCodes.isEmpty()) {
			return Optional.empty();
		}

		return Optional.of(choiceCodes.get(0));
	}

	private static List<String> getChoiceCodeListFromActualVotingOption(final String actualVotingOption,
			final PrimesMappingTable primesMappingTable, final Map<PrimeGqElement, String> encodedVotingOptionToChoiceCodeMap) {

		return primesMappingTable.getPTable()
				.stream()
				.filter(primesMappingTableEntry -> primesMappingTableEntry.actualVotingOption().equals(actualVotingOption))
				.map(PrimesMappingTableEntry::encodedVotingOption)
				.map(encodedVotingOptionToChoiceCodeMap::get)
				.toList();
	}

	private record ElectionInformationTypeExtended(ElectionInformationType electionInformationType, String domainOfInfluence) {
	}
}
