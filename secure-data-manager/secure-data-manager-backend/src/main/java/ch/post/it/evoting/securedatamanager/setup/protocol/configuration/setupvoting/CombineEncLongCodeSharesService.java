/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.protocol.configuration.setupvoting;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPrivateKey;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupMatrix;
import ch.post.it.evoting.evotinglibraries.domain.election.PrimesMappingTable;
import ch.post.it.evoting.evotinglibraries.domain.election.VerificationCardSetContext;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.ElectionEventContextPayload;
import ch.post.it.evoting.securedatamanager.setup.process.generate.EncryptedNodeLongReturnCodeSharesChunk;
import ch.post.it.evoting.securedatamanager.setup.process.generate.EncryptedSingleNodeLongReturnCodeSharesChunk;
import ch.post.it.evoting.securedatamanager.setup.process.SetupKeyPairService;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventContextPayloadService;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventService;

@Service
@ConditionalOnProperty("role.isSetup")
public class CombineEncLongCodeSharesService {

	private static final Logger LOGGER = LoggerFactory.getLogger(CombineEncLongCodeSharesService.class);

	private final ElectionEventService electionEventService;
	private final SetupKeyPairService setupKeyPairService;
	private final CombineEncLongCodeSharesAlgorithm combineEncLongCodeSharesAlgorithm;
	private final ElectionEventContextPayloadService electionEventContextPayloadService;

	public CombineEncLongCodeSharesService(final ElectionEventService electionEventService,
			final SetupKeyPairService setupKeyPairService,
			final CombineEncLongCodeSharesAlgorithm combineEncLongCodeSharesAlgorithm,
			final ElectionEventContextPayloadService electionEventContextPayloadService) {
		this.electionEventService = electionEventService;
		this.setupKeyPairService = setupKeyPairService;
		this.combineEncLongCodeSharesAlgorithm = combineEncLongCodeSharesAlgorithm;
		this.electionEventContextPayloadService = electionEventContextPayloadService;
	}

	/**
	 * Invokes the CombineEncLongCodeShares algorithm.
	 *
	 * @param electionEventId                        the election event id. Must be non-null and a valid UUID.
	 * @param verificationCardSetId                  the verification card set id. Must be non-null and a valid UUID.
	 * @param encryptedNodeLongReturnCodeSharesChunk the encrypted node long return code shares chunk. Must be non-null.
	 */
	public CombineEncLongCodeSharesOutput combineEncLongCodeShares(final String electionEventId, final String verificationCardSetId,
			final EncryptedNodeLongReturnCodeSharesChunk encryptedNodeLongReturnCodeSharesChunk) {
		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);
		checkNotNull(encryptedNodeLongReturnCodeSharesChunk);
		checkArgument(electionEventId.equals(encryptedNodeLongReturnCodeSharesChunk.getElectionEventId()),
				"The encrypted node long return code shares chunk does not correspond to the expected election event id. [expected: %s, actual: %s]",
				electionEventId, encryptedNodeLongReturnCodeSharesChunk.getElectionEventId());
		checkArgument(verificationCardSetId.equals(encryptedNodeLongReturnCodeSharesChunk.getVerificationCardSetId()),
				"The encrypted node long return code shares chunk does not correspond to the expected verification card set id. [expected: %s, actual: %s]",
				verificationCardSetId, encryptedNodeLongReturnCodeSharesChunk.getVerificationCardSetId());

		checkArgument(electionEventService.exists(electionEventId), "The given election event id does not exist. [electionEventId: %s]",
				electionEventId);

		final ElectionEventContextPayload electionEventContextPayload = electionEventContextPayloadService.load(electionEventId);
		checkArgument(electionEventContextPayload.getElectionEventContext().verificationCardSetContexts().stream()
						.parallel()
						.map(VerificationCardSetContext::getVerificationCardSetId)
						.anyMatch(vcs -> vcs.equals(verificationCardSetId)),
				"The given verification card set id does not exist. [verificationCardSetId: %s]", verificationCardSetId);

		final GqGroup encryptionGroup = electionEventContextPayload.getEncryptionGroup();
		final int maximumNumberOfVotingOptions = electionEventContextPayload.getElectionEventContext().maximumNumberOfVotingOptions();

		final PrimesMappingTable primesMappingTable = electionEventContextPayloadService.loadPrimesMappingTable(electionEventId,
				verificationCardSetId);

		final ElGamalMultiRecipientPrivateKey setupSecretKey = setupKeyPairService.load(electionEventId).getPrivateKey();

		// Prepare the Matrix of exponentiated, encrypted, hashed partial Choice Return Codes.
		final List<EncryptedSingleNodeLongReturnCodeSharesChunk> nodeReturnCodesValues = encryptedNodeLongReturnCodeSharesChunk.getNodeReturnCodesValues();
		final List<List<ElGamalMultiRecipientCiphertext>> partialChoiceReturnCodesColumns = nodeReturnCodesValues.stream()
				.parallel()
				.map(EncryptedSingleNodeLongReturnCodeSharesChunk::getExponentiatedEncryptedPartialChoiceReturnCodes)
				.toList();
		final GroupMatrix<ElGamalMultiRecipientCiphertext, GqGroup> partialChoiceReturnCodesMatrix = GroupMatrix.fromColumns(
				partialChoiceReturnCodesColumns);

		// Prepare the Matrix of exponentiated, encrypted, hashed Confirmation Keys.
		final List<List<ElGamalMultiRecipientCiphertext>> confirmationKeysColumns = nodeReturnCodesValues.stream()
				.parallel()
				.map(EncryptedSingleNodeLongReturnCodeSharesChunk::getExponentiatedEncryptedConfirmationKeys)
				.toList();
		final GroupMatrix<ElGamalMultiRecipientCiphertext, GqGroup> confirmationKeysMatrix = GroupMatrix.fromColumns(confirmationKeysColumns);

		final CombineEncLongCodeSharesContext combineEncLongCodeSharesContext = new CombineEncLongCodeSharesContext.Builder()
				.setEncryptionGroup(encryptionGroup)
				.setElectionEventId(electionEventId)
				.setVerificationCardSetId(verificationCardSetId)
				.setVerificationCardIds(encryptedNodeLongReturnCodeSharesChunk.getVerificationCardIds())
				.setNumberOfVotingOptions(primesMappingTable.getNumberOfVotingOptions())
				.setMaximumNumberOfVotingOptions(maximumNumberOfVotingOptions)
				.build();
		final CombineEncLongCodeSharesInput combineEncLongCodeSharesInput = new CombineEncLongCodeSharesInput.Builder()
				.setSetupSecretKey(setupSecretKey)
				.setExponentiatedEncryptedChoiceReturnCodesMatrix(partialChoiceReturnCodesMatrix)
				.setExponentiatedEncryptedConfirmationKeysMatrix(confirmationKeysMatrix)
				.build();

		final int chunkId = encryptedNodeLongReturnCodeSharesChunk.getChunkId();
		LOGGER.debug("Performing CombineEncLongCodeShares algorithm... [electionEventId: {}, verificationCardSetId: {}, chunkId: {}]",
				electionEventId, verificationCardSetId, chunkId);

		return combineEncLongCodeSharesAlgorithm.combineEncLongCodeShares(combineEncLongCodeSharesContext, combineEncLongCodeSharesInput);
	}
}
