/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.protocol.configuration.setupvoting;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.BASE64_ENCODED_HASH_OUTPUT_LENGTH;
import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.hasNoDuplicates;
import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateBase64Encoded;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;
import java.util.Objects;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientKeyPair;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;

/**
 * Regroups the output values needed by the GenVerDat algorithm.
 *
 * <ul>
 *     <li>vc, the vector of verification card ids. Non-null.</li>
 *     <li>SVK, the vector of Start Voting Keys. Non-null.</li>
 *     <li>K, the vector of verification card public keys. Non-null.</li>
 *     <li>k, the vector of verification card secret keys. Non-null.</li>
 *     <li>LpCC, the Partial Choice Return Codes allow list. Non-null.</li>
 *     <li>BCK, the vector of ballot casting keys. Non-null.</li>
 *     <li>cpCC, the vector of encrypted, hashed partial Choice Return Codes. Non-null.</li>
 *     <li>cck, the vector of encrypted, hashed Confirmation Keys. Non-null.</li>
 * </ul>
 */
public class GenVerDatOutput {

	private final int size;
	private final GqGroup gqGroup;

	private final List<String> verificationCardIds;
	private final List<String> startVotingKeys;
	private final List<ElGamalMultiRecipientKeyPair> verificationCardKeyPairs;
	private final List<String> partialChoiceReturnCodesAllowList;
	private final List<String> ballotCastingKeys;
	private final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> encryptedHashedPartialChoiceReturnCodes;
	private final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> encryptedHashedConfirmationKeys;

	private GenVerDatOutput(
			final List<String> verificationCardIds,
			final List<String> startVotingKeys,
			final List<ElGamalMultiRecipientKeyPair> verificationCardKeyPairs,
			final List<String> partialChoiceReturnCodesAllowList,
			final List<String> ballotCastingKeys,
			final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> encryptedHashedPartialChoiceReturnCodes,
			final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> encryptedHashedConfirmationKeys) {

		checkNotNull(verificationCardIds);
		checkNotNull(startVotingKeys);
		checkNotNull(verificationCardKeyPairs);
		checkNotNull(partialChoiceReturnCodesAllowList);
		checkNotNull(ballotCastingKeys);
		checkNotNull(encryptedHashedConfirmationKeys);
		final List<String> verificationCardIdsCopy = List.copyOf(verificationCardIds);
		final List<String> startVotingKeysCopy = List.copyOf(startVotingKeys);
		final List<ElGamalMultiRecipientKeyPair> verificationCardKeyPairsCopy = List.copyOf(verificationCardKeyPairs);
		final List<String> partialChoiceReturnCodesAllowListCopy = List.copyOf(partialChoiceReturnCodesAllowList);
		final List<String> ballotCastingKeysCopy = List.copyOf(ballotCastingKeys);

		checkArgument(!verificationCardIdsCopy.isEmpty(), "The output must not be empty.");
		checkArgument(!startVotingKeysCopy.isEmpty(), "The start voting keys must not be empty.");

		this.size = verificationCardIdsCopy.size();
		checkArgument(this.size == verificationCardKeyPairsCopy.size() && this.size == startVotingKeysCopy.size()
				&& this.size == ballotCastingKeysCopy.size() && this.size == encryptedHashedPartialChoiceReturnCodes.size()
				&& this.size == encryptedHashedConfirmationKeys.size(), "All vectors must have the same size.");

		final int n = encryptedHashedPartialChoiceReturnCodes.getElementSize();
		checkArgument(partialChoiceReturnCodesAllowListCopy.size() == n * this.size,
				String.format("There must be %d elements in the allow list.", n * this.size));

		partialChoiceReturnCodesAllowListCopy.forEach(
				element -> checkArgument(validateBase64Encoded(element).length() == BASE64_ENCODED_HASH_OUTPUT_LENGTH,
						String.format("Elements in allowList must be of length %s.", BASE64_ENCODED_HASH_OUTPUT_LENGTH)));

		final GqGroup group = verificationCardKeyPairsCopy.get(0).getGroup();
		checkArgument(group.equals(encryptedHashedPartialChoiceReturnCodes.get(0).getGroup()) && group
				.equals(encryptedHashedConfirmationKeys.get(0).getGroup()), "All vectors must belong to the same group.");

		checkArgument(hasNoDuplicates(verificationCardIdsCopy), "The vector of verification card ids must not contain any duplicated element.");

		this.gqGroup = group;
		this.verificationCardIds = verificationCardIdsCopy;
		this.startVotingKeys = startVotingKeysCopy;
		this.verificationCardKeyPairs = verificationCardKeyPairsCopy;
		this.partialChoiceReturnCodesAllowList = partialChoiceReturnCodesAllowListCopy;
		this.ballotCastingKeys = ballotCastingKeysCopy;
		this.encryptedHashedPartialChoiceReturnCodes = encryptedHashedPartialChoiceReturnCodes;
		this.encryptedHashedConfirmationKeys = encryptedHashedConfirmationKeys;
	}

	public List<String> getVerificationCardIds() {
		return List.copyOf(verificationCardIds);
	}

	public List<String> getStartVotingKeys() {
		return List.copyOf(startVotingKeys);
	}

	public List<ElGamalMultiRecipientKeyPair> getVerificationCardKeyPairs() {
		return List.copyOf(verificationCardKeyPairs);
	}

	public List<String> getPartialChoiceReturnCodesAllowList() {
		return List.copyOf(partialChoiceReturnCodesAllowList);
	}

	public List<String> getBallotCastingKeys() {
		return List.copyOf(ballotCastingKeys);
	}

	public List<ElGamalMultiRecipientCiphertext> getEncryptedHashedPartialChoiceReturnCodes() {
		return List.copyOf(encryptedHashedPartialChoiceReturnCodes);
	}

	public List<ElGamalMultiRecipientCiphertext> getEncryptedHashedConfirmationKeys() {
		return List.copyOf(encryptedHashedConfirmationKeys);
	}

	public int size() {
		return this.size;
	}

	public GqGroup getGroup() {
		return this.gqGroup;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		final GenVerDatOutput that = (GenVerDatOutput) o;
		return size == that.size && gqGroup.equals(that.gqGroup) && verificationCardIds.equals(that.verificationCardIds) && startVotingKeys.equals(
				that.startVotingKeys) && verificationCardKeyPairs.equals(that.verificationCardKeyPairs) && partialChoiceReturnCodesAllowList.equals(
				that.partialChoiceReturnCodesAllowList) && ballotCastingKeys.equals(that.ballotCastingKeys)
				&& encryptedHashedPartialChoiceReturnCodes.equals(that.encryptedHashedPartialChoiceReturnCodes)
				&& encryptedHashedConfirmationKeys.equals(
				that.encryptedHashedConfirmationKeys);
	}

	@Override
	public int hashCode() {
		return Objects.hash(size, gqGroup, verificationCardIds, startVotingKeys, verificationCardKeyPairs, partialChoiceReturnCodesAllowList,
				ballotCastingKeys, encryptedHashedPartialChoiceReturnCodes, encryptedHashedConfirmationKeys);
	}

	public static class Builder {
		private List<String> verificationCardIds;
		private List<String> startVotingKeys;
		private List<ElGamalMultiRecipientKeyPair> verificationCardKeyPairs;
		private List<String> partialChoiceReturnCodesAllowList;
		private List<String> ballotCastingKeys;
		private GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> encryptedHashedPartialChoiceReturnCodes;
		private GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> encryptedHashedConfirmationKeys;

		public Builder setVerificationCardIds(final List<String> verificationCardIds) {
			this.verificationCardIds = verificationCardIds;
			return this;
		}

		public Builder setStartVotingKeys(final List<String> startVotingKeys) {
			this.startVotingKeys = startVotingKeys;
			return this;
		}

		public Builder setVerificationCardKeyPairs(final List<ElGamalMultiRecipientKeyPair> verificationCardKeyPairs) {
			this.verificationCardKeyPairs = verificationCardKeyPairs;
			return this;
		}

		public Builder setPartialChoiceReturnCodesAllowList(final List<String> partialChoiceReturnCodesAllowList) {
			this.partialChoiceReturnCodesAllowList = partialChoiceReturnCodesAllowList;
			return this;
		}

		public Builder setBallotCastingKeys(final List<String> ballotCastingKeys) {
			this.ballotCastingKeys = ballotCastingKeys;
			return this;
		}

		public Builder setEncryptedHashedPartialChoiceReturnCodes(
				final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> encryptedHashedPartialChoiceReturnCodes) {
			this.encryptedHashedPartialChoiceReturnCodes = encryptedHashedPartialChoiceReturnCodes;
			return this;
		}

		public Builder setEncryptedHashedConfirmationKeys(
				final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> encryptedHashedConfirmationKeys) {
			this.encryptedHashedConfirmationKeys = encryptedHashedConfirmationKeys;
			return this;
		}

		public GenVerDatOutput build() {
			return new GenVerDatOutput(verificationCardIds, startVotingKeys, verificationCardKeyPairs, partialChoiceReturnCodesAllowList,
					ballotCastingKeys, encryptedHashedPartialChoiceReturnCodes, encryptedHashedConfirmationKeys);
		}
	}

}
