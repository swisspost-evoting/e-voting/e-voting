/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.shared.process.summary;

import java.util.List;

public class ElectionSummary {

	private final String electionId;
	private final int electionPosition;
	private final int electionType;
	private final int primarySecondaryType;
	private final List<Description> electionDescription;
	private final int numberOfMandates;
	private final boolean writeInsAllowed;
	private final int candidateAccumulation;
	private final List<CandidateSummary> candidates;
	private final List<ListSummary> lists;

	private ElectionSummary(final String electionId, final int electionPosition, final int electionType, final int primarySecondaryType, final List<Description> electionDescription, final int numberOfMandates, final boolean writeInsAllowed, final int candidateAccumulation, final List<CandidateSummary> candidates, final List<ListSummary> lists) {
		this.electionId = electionId;
		this.electionPosition = electionPosition;
		this.electionType = electionType;
		this.primarySecondaryType = primarySecondaryType;
		this.electionDescription = electionDescription;
		this.numberOfMandates = numberOfMandates;
		this.writeInsAllowed = writeInsAllowed;
		this.candidateAccumulation = candidateAccumulation;
		this.candidates = candidates;
		this.lists = lists;
	}

	public String getElectionId() {
		return electionId;
	}

	public int getElectionPosition() {
		return electionPosition;
	}

	public int getElectionType() {
		return electionType;
	}

	public int getPrimarySecondaryType() {
		return primarySecondaryType;
	}

	public List<Description> getElectionDescription() {
		return electionDescription;
	}

	public int getNumberOfMandates() {
		return numberOfMandates;
	}

	public boolean isWriteInsAllowed() {
		return writeInsAllowed;
	}

	public int getCandidateAccumulation() {
		return candidateAccumulation;
	}

	public List<CandidateSummary> getCandidates() {
		return candidates;
	}

	public List<ListSummary> getLists() {
		return lists;
	}

	public static class Builder {

		private String electionId;
		private int electionPosition;
		private int electionType;
		private int primarySecondaryType;
		private List<Description> electionDescription;
		private int numberOfMandates;
		private boolean writeInsAllowed;
		private int candidateAccumulation;
		private List<CandidateSummary> candidates;
		private List<ListSummary> lists;

		public Builder electionId(final String electionId) {
			this.electionId = electionId;
			return this;
		}

		public Builder electionPosition(final int position) {
			this.electionPosition = position;
			return this;
		}

		public Builder electionType(final int electionType) {
			this.electionType = electionType;
			return this;
		}

		/**
		 * Sets the primary secondary type.
		 *
		 * @param primarySecondaryType the primary secondary type. 1 if primary, 2 if secondary, 0 if none.
		 */
		public Builder primarySecondaryType(final int primarySecondaryType) {
			this.primarySecondaryType = primarySecondaryType;
			return this;
		}

		public Builder electionDescription(final List<Description> electionDescription) {
			this.electionDescription = electionDescription;
			return this;
		}

		public Builder numberOfMandates(final int numberOfMandates) {
			this.numberOfMandates = numberOfMandates;
			return this;
		}

		public Builder writeInsAllowed(final boolean writeInsAllowed) {
			this.writeInsAllowed = writeInsAllowed;
			return this;
		}

		public Builder candidateAccumulation(final int candidateAccumulation) {
			this.candidateAccumulation = candidateAccumulation;
			return this;
		}

		public Builder candidates(final List<CandidateSummary> candidates) {
			this.candidates = candidates;
			return this;
		}

		public Builder lists(final List<ListSummary> lists) {
			this.lists = lists;
			return this;
		}

		public ElectionSummary build() {
			return new ElectionSummary(electionId, electionPosition, electionType, primarySecondaryType, electionDescription, numberOfMandates, writeInsAllowed, candidateAccumulation, candidates, lists);
		}
	}
}
