/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.online.process.download;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static ch.post.it.evoting.securedatamanager.shared.process.Status.VCS_DOWNLOADED;
import static com.google.common.base.Preconditions.checkState;

import java.io.IOException;
import java.io.UncheckedIOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.securedatamanager.shared.process.ConfigurationEntityStatusService;
import ch.post.it.evoting.securedatamanager.shared.process.NodeContributionsResponsesFileRepository;
import ch.post.it.evoting.securedatamanager.shared.process.SetupComponentVerificationDataPayloadFileRepository;
import ch.post.it.evoting.securedatamanager.shared.process.VotingCardSetRepository;

/**
 * This is an application service that manages voting card sets.
 */
@Service
public class VotingCardSetDownloadService {

	private static final Logger LOGGER = LoggerFactory.getLogger(VotingCardSetDownloadService.class);

	private final boolean deleteNodeContributions;
	private final VotingCardSetRepository votingCardSetRepository;
	private final ConfigurationEntityStatusService configurationEntityStatusService;
	private final NodeContributionsResponsesFileRepository nodeContributionsResponsesFileRepository;
	private final EncryptedLongReturnCodeSharesDownloadService encryptedLongReturnCodeSharesDownloadService;
	private final SetupComponentVerificationDataPayloadFileRepository setupComponentVerificationDataPayloadFileRepository;

	public VotingCardSetDownloadService(
			@Value("${download-genenclongcodeshares.delete}")
			final boolean deleteNodeContributions,
			final VotingCardSetRepository votingCardSetRepository,
			final ConfigurationEntityStatusService configurationEntityStatusService,
			final NodeContributionsResponsesFileRepository nodeContributionsResponsesFileRepository,
			final EncryptedLongReturnCodeSharesDownloadService encryptedLongReturnCodeSharesDownloadService,
			final SetupComponentVerificationDataPayloadFileRepository setupComponentVerificationDataPayloadFileRepository) {
		this.deleteNodeContributions = deleteNodeContributions;
		this.votingCardSetRepository = votingCardSetRepository;
		this.configurationEntityStatusService = configurationEntityStatusService;
		this.nodeContributionsResponsesFileRepository = nodeContributionsResponsesFileRepository;
		this.encryptedLongReturnCodeSharesDownloadService = encryptedLongReturnCodeSharesDownloadService;
		this.setupComponentVerificationDataPayloadFileRepository = setupComponentVerificationDataPayloadFileRepository;
	}

	/**
	 * Download the computed values for a votingCardSet.
	 */
	public void download(final String votingCardSetId, final String electionEventId) {
		validateUUID(votingCardSetId);
		validateUUID(electionEventId);

		final String verificationCardSetId = votingCardSetRepository.getVerificationCardSetId(votingCardSetId);

		LOGGER.info("Downloading the computed values. [electionEventId: {}, votingCardSetId: {}, verificationCardSetId: {}]", electionEventId,
				votingCardSetId, verificationCardSetId);

		if (deleteNodeContributions) {
			try {
				nodeContributionsResponsesFileRepository.deleteNodeContributions(electionEventId, verificationCardSetId);
			} catch (final IOException e) {
				throw new UncheckedIOException(
						String.format(
								"Failed to delete node contribution before voting cards download. [electionEventId: %s, votingCardSetId: %s, verificationCardSetId: %s]",
								electionEventId, votingCardSetId, verificationCardSetId), e);
			}
		}

		final int chunkCount = setupComponentVerificationDataPayloadFileRepository.getCount(electionEventId, verificationCardSetId);

		checkState(chunkCount > 0, "No chunk found for download. [electionEventId: %s, votingCardSetId: %s, verificationCardSetId: %s]",
				electionEventId, votingCardSetId, verificationCardSetId);

		encryptedLongReturnCodeSharesDownloadService.downloadGenEncLongCodeShares(electionEventId, verificationCardSetId, chunkCount)
				.doOnNext(contributions -> {
					final int chunkId = contributions.getFirst().getChunkId();
					nodeContributionsResponsesFileRepository.writeNodeContributions(electionEventId, verificationCardSetId, chunkId, contributions);
					LOGGER.debug("Node contributions saved. [electionEventId: {}, votingCardSetId: {}, verificationCardSetId: {}, chunkId: {}]",
							electionEventId, votingCardSetId, verificationCardSetId, chunkId);
				})
				.sequential()
				.doOnComplete(() -> {
					LOGGER.debug("Download of all chunks completed. [electionEventId: {}, votingCardSetId: {}, verificationCardSetId: {}]",
							electionEventId, votingCardSetId, verificationCardSetId);

					configurationEntityStatusService.update(VCS_DOWNLOADED.name(), votingCardSetId, votingCardSetRepository);
					LOGGER.debug("Voting card set updated. [electionEventId: {}, votingCardSetId: {}, verificationCardSetId: {}]", electionEventId,
							votingCardSetId, verificationCardSetId);
				})
				.blockLast();
	}
}
