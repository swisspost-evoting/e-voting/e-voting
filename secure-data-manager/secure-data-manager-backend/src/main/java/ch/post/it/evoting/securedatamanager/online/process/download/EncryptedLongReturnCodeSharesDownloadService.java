/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.online.process.download;

import static ch.post.it.evoting.evotinglibraries.domain.ControlComponentConstants.NODE_IDS;
import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static ch.post.it.evoting.securedatamanager.shared.Constants.DOWNLOAD_UNSUCCESSFUL_MESSAGE;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.domain.reactor.Box;
import ch.post.it.evoting.evotinglibraries.domain.returncodes.ControlComponentCodeSharesPayload;
import ch.post.it.evoting.securedatamanager.shared.WebClientFactory;
import ch.post.it.evoting.securedatamanager.shared.process.NodeContributionsResponsesFileRepository;
import ch.post.it.evoting.securedatamanager.shared.process.SetupComponentVerificationDataPayloadFileRepository;

import reactor.core.publisher.Flux;
import reactor.core.publisher.ParallelFlux;
import reactor.core.scheduler.Schedulers;
import reactor.util.retry.RetryBackoffSpec;

@Service
public class EncryptedLongReturnCodeSharesDownloadService {

	private static final Logger LOGGER = LoggerFactory.getLogger(EncryptedLongReturnCodeSharesDownloadService.class);

	private final int splitConcurrency;
	private final long maxRequestBodySize;
	private final ObjectMapper objectMapper;
	private final boolean deleteNodeContributions;
	private final WebClientFactory webClientFactory;
	private final RetryBackoffSpec retryBackoffSpec;
	private final NodeContributionsResponsesFileRepository nodeContributionsResponsesFileRepository;
	private final SetupComponentVerificationDataPayloadFileRepository setupComponentVerificationDataPayloadFileRepository;

	public EncryptedLongReturnCodeSharesDownloadService(
			@Value("${download-genenclongcodeshares.split-concurrency}")
			final int splitConcurrency,
			@Value("${compute.max-request-size}")
			final long maxRequestBodySize,
			final ObjectMapper objectMapper,
			@Value("${download-genenclongcodeshares.delete}")
			final boolean deleteNodeContributions,
			final WebClientFactory webClientFactory,
			final RetryBackoffSpec retryBackoffSpec,
			final NodeContributionsResponsesFileRepository nodeContributionsResponsesFileRepository,
			final SetupComponentVerificationDataPayloadFileRepository setupComponentVerificationDataPayloadFileRepository) {
		this.splitConcurrency = splitConcurrency;
		this.maxRequestBodySize = maxRequestBodySize;
		this.objectMapper = objectMapper;
		this.deleteNodeContributions = deleteNodeContributions;
		this.webClientFactory = webClientFactory;
		this.retryBackoffSpec = retryBackoffSpec;
		this.setupComponentVerificationDataPayloadFileRepository = setupComponentVerificationDataPayloadFileRepository;
		this.nodeContributionsResponsesFileRepository = nodeContributionsResponsesFileRepository;
	}

	/**
	 * Download the control components' encrypted long Choice Return codes and the encrypted long Vote Cast Return code shares.
	 */
	public ParallelFlux<List<ControlComponentCodeSharesPayload>> downloadGenEncLongCodeShares(final String electionEventId,
			final String verificationCardSetId, final int chunkCount) {
		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);
		checkArgument(chunkCount > 0);

		final AtomicLong consumedBytes = new AtomicLong(0);

		return Flux.range(0, chunkCount)
				.filter(chunkId -> deleteNodeContributions ||
						!nodeContributionsResponsesFileRepository.existsNodeContributions(electionEventId, verificationCardSetId, chunkId))
				.transform(flux -> splitFluxBySize(flux, electionEventId, verificationCardSetId, consumedBytes, maxRequestBodySize))
				.flatMap(chunkIds -> downloadControlComponentCodeSharesPayloads(electionEventId, verificationCardSetId, chunkIds), splitConcurrency)
				.parallel()
				.runOn(Schedulers.boundedElastic())
				.map(controlComponentCodeSharesPayloadsBytes -> controlComponentCodeSharesPayloadsBytes.stream()
						.map(this::deserializePayload)
						.toList())
				.map(controlComponentCodeSharesPayloads -> checkPayloadsConsistency(electionEventId, verificationCardSetId,
						controlComponentCodeSharesPayloads));
	}

	private ControlComponentCodeSharesPayload deserializePayload(final byte[] bytes) {
		final ControlComponentCodeSharesPayload controlComponentCodeSharesPayload;
		try {
			controlComponentCodeSharesPayload = objectMapper.readValue(bytes, ControlComponentCodeSharesPayload.class);
		} catch (final IOException e) {
			throw new UncheckedIOException(e);
		}

		LOGGER.debug("Control Component Code Shares Payload deserialized. [electionEventId: {}, verificationCardSetId: {}, chunkId: {}, nodeId: {}]",
				controlComponentCodeSharesPayload.getElectionEventId(), controlComponentCodeSharesPayload.getVerificationCardSetId(),
				controlComponentCodeSharesPayload.getChunkId(), controlComponentCodeSharesPayload.getNodeId());

		return controlComponentCodeSharesPayload;
	}

	private Flux<List<Integer>> splitFluxBySize(final Flux<Integer> flux, final String electionEventId, final String verificationCardSetId,
			final AtomicLong consumedBytes, final long maxSize) {
		return flux.bufferUntil(payload -> {
			final long requestPayloadSize = setupComponentVerificationDataPayloadFileRepository.getPayloadSize(electionEventId,
					verificationCardSetId);
			final long responsePayloadSize = NODE_IDS.size() * requestPayloadSize;
			final long actualConsumption = consumedBytes.addAndGet(responsePayloadSize);
			if (actualConsumption > maxSize) {
				LOGGER.debug("size limit reached, cutting the flux after this element [actualConsumption: {}, limit: {}]", actualConsumption,
						maxSize);
				consumedBytes.set(0);
				return true;
			}
			return false;
		}, false);
	}

	private Flux<List<byte[]>> downloadControlComponentCodeSharesPayloads(final String electionEventId,
			final String verificationCardSetId, final List<Integer> chunkIds) {
		return webClientFactory.getWebClient(
						String.format("%s [electionEventId: %s, verificationCardSetId: %s]",
								DOWNLOAD_UNSUCCESSFUL_MESSAGE, electionEventId, verificationCardSetId))
				.post()
				.uri(uriBuilder -> uriBuilder.path(
								"api/v1/configuration/electionevent/{electionEventId}/verificationcardset/{verificationCardSetId}/download")
						.build(electionEventId, verificationCardSetId))
				.contentType(MediaType.APPLICATION_NDJSON)
				.accept(MediaType.APPLICATION_NDJSON)
				.body(Flux.fromIterable(chunkIds), Integer.class)
				.retrieve()
				.bodyToFlux(new ParameterizedTypeReference<Box<List<byte[]>>>() {
				})
				.retryWhen(retryBackoffSpec)
				.map(Box::boxed);
	}

	private static List<ControlComponentCodeSharesPayload> checkPayloadsConsistency(final String electionEventId,
			final String verificationCardSetId, final List<ControlComponentCodeSharesPayload> controlComponentCodeSharesPayloads) {
		final List<ControlComponentCodeSharesPayload> result = controlComponentCodeSharesPayloads.stream()
				.sorted(Comparator.comparingInt(ControlComponentCodeSharesPayload::getNodeId))
				.toList();

		checkState(controlComponentCodeSharesPayloads.stream().map(ControlComponentCodeSharesPayload::getChunkId).distinct().count() == 1,
				"All node contributions must have the same chunkId");

		checkState(NODE_IDS.equals(result.stream()
						.parallel()
						.map(controlComponentCodeSharesPayload -> {

							checkState(controlComponentCodeSharesPayload.getElectionEventId().equals(electionEventId),
									"The controlComponentCodeSharesPayload's election event id does not correspond to the election event id of the request");

							checkState(controlComponentCodeSharesPayload.getVerificationCardSetId().equals(verificationCardSetId),
									"The controlComponentCodeSharesPayload's verification card set id does not correspond to the verification card set id of the request");

							return controlComponentCodeSharesPayload.getNodeId();
						})
						.collect(Collectors.toSet())),
				"There must be exactly the expected number of control component code shares payloads with the expected node ids.");
		return result;
	}
}
