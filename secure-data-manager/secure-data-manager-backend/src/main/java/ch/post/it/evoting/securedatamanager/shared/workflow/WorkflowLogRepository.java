/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.shared.workflow;

import jakarta.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ch.post.it.evoting.securedatamanager.shared.process.AbstractEntityRepository;
import ch.post.it.evoting.securedatamanager.shared.database.DatabaseManager;

/**
 * Implementation of the Ballot Repository
 */
@Repository
public class WorkflowLogRepository extends AbstractEntityRepository {

	/**
	 * Constructor
	 *
	 * @param databaseManager the injected database manager
	 */
	@Autowired
	public WorkflowLogRepository(final DatabaseManager databaseManager) {
		super(databaseManager);
	}

	@PostConstruct
	@Override
	public void initialize() {
		super.initialize();
	}

	@Override
	protected String entityName() {
		return WorkflowLog.class.getSimpleName();
	}
}
