/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.process.preconfigure;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static ch.post.it.evoting.evotinglibraries.domain.election.ElectionAttributesAliasConstants.ALIAS_CANDIDATES;
import static ch.post.it.evoting.evotinglibraries.domain.election.ElectionAttributesAliasConstants.ALIAS_JOIN_DELIMITER;
import static ch.post.it.evoting.evotinglibraries.domain.election.ElectionAttributesAliasConstants.ALIAS_LISTS;
import static ch.post.it.evoting.evotinglibraries.domain.election.SemanticInformationUtils.getAnswerInformation;
import static ch.post.it.evoting.evotinglibraries.domain.election.SemanticInformationUtils.getBlankCandidatePositionInformation;
import static ch.post.it.evoting.evotinglibraries.domain.election.SemanticInformationUtils.getCandidateInformation;
import static ch.post.it.evoting.evotinglibraries.domain.election.SemanticInformationUtils.getListInformation;
import static ch.post.it.evoting.evotinglibraries.domain.election.SemanticInformationUtils.getWriteInPositionInformation;
import static ch.post.it.evoting.securedatamanager.setup.process.preconfigure.TranslationsFactory.getInLanguage;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import static java.util.function.Predicate.not;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.MoreCollectors;

import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.evotinglibraries.domain.election.Contest;
import ch.post.it.evoting.evotinglibraries.domain.election.ElectionAttributes;
import ch.post.it.evoting.evotinglibraries.domain.election.ElectionOption;
import ch.post.it.evoting.evotinglibraries.domain.election.PrimarySecondaryType;
import ch.post.it.evoting.evotinglibraries.domain.election.Question;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.AnswerInformationType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.BallotQuestionType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.CandidatePositionType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.CandidateType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.Configuration;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.ElectionDescriptionInformationType.ElectionDescriptionInfo;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.ElectionGroupBallotType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.ElectionInformationType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.ElectionType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.EligibilityType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.LanguageType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.ListDescriptionInformationType.ListDescriptionInfo;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.ListType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.ReferencedElectionInformationType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.StandardBallotType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.TieBreakQuestionType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.VariantBallotType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.VoteDescriptionInformationType.VoteDescriptionInfo;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.VoteInformationType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.VoteType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.WriteInCandidateType;
import ch.post.it.evoting.securedatamanager.setup.process.preconfigure.ElectionsConfigFactory.ElectionTypeAttributeIds;
import ch.post.it.evoting.securedatamanager.setup.process.preconfigure.ElectionsConfigFactory.VoteTypeAttributeIds;

/**
 * Creates the ballot contest of the elections_config.json.
 */
public class BallotContestFactory {

	private static final Logger LOGGER = LoggerFactory.getLogger(BallotContestFactory.class);
	private static final String ALIAS_PREFIX_DUMMY_LIST = "list_for_";
	private static final String ELECTION_DUMMY_VALUE = "-";
	private static final Random random = RandomFactory.createRandom();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();

	private BallotContestFactory() {
		// static usage only.
	}

	/**
	 * Creates the ballot contest for all elections and votes.
	 *
	 * @param configuration the {@link Configuration} representing the configuration-anonymized. Must be non-null.
	 * @return the mapping of the domain of influence of the election or vote to the ballot contest with its position.
	 */
	public static Map<String, List<ContestWithPosition>> createContest(final Configuration configuration,
			final Map<String, VoteTypeAttributeIds> voteTypeAttributeIdsMap,
			final Map<String, ElectionTypeAttributeIds> electionTypeAttributeIdsMap) {
		checkNotNull(configuration);
		checkNotNull(voteTypeAttributeIdsMap);
		checkNotNull(electionTypeAttributeIdsMap);

		// Create ballot contest for all elections
		final Map<String, List<ContestWithPosition>> electionContest = configuration.getContest().getElectionGroupBallot().stream().parallel()
				.collect(Collectors.groupingByConcurrent(
						ElectionGroupBallotType::getDomainOfInfluence,
						Collectors.flatMapping(
								electionGroupBallotType -> {
									final String electionGroupIdentification = electionGroupBallotType.getElectionGroupIdentification();
									final List<ElectionInformationType> electionInformationTypes = electionGroupBallotType.getElectionInformation()
											.stream()
											.toList();
									final List<String> electionIdentifications = electionInformationTypes.stream()
											.map(ElectionInformationType::getElection)
											.map(ElectionType::getElectionIdentification)
											.toList();
									final List<ElectionTypeAttributeIds> electionTypeAttributeIds = electionIdentifications.stream()
											.map(electionId -> checkNotNull(electionTypeAttributeIdsMap.get(electionId)))
											.toList();
									return IntStream.range(0, electionInformationTypes.size())
											.mapToObj(i -> new ContestWithPosition(
													createElectionContest(electionInformationTypes.get(i), electionTypeAttributeIds.get(i),
															electionGroupIdentification),
													electionGroupBallotType.getElectionGroupPosition()));
								},
								Collectors.toList())
				));

		// Create ballot contest for all votes
		final Map<String, List<ContestWithPosition>> voteContest = configuration.getContest().getVoteInformation().stream()
				.map(VoteInformationType::getVote)
				.collect(Collectors.groupingBy(
						VoteType::getDomainOfInfluence,
						Collectors.mapping(
								voteType -> new ContestWithPosition(createVoteContest(voteType, voteTypeAttributeIdsMap),
										voteType.getVotePosition().intValue()),
								Collectors.toList())
				));

		final Map<String, List<ContestWithPosition>> domainOfInfluenceToContest = mergeContests(electionContest, voteContest);

		LOGGER.info("Successfully created the ballot contest.");

		return domainOfInfluenceToContest;
	}

	private static Contest createElectionContest(final ElectionInformationType electionInformationType,
			final ElectionTypeAttributeIds electionTypeAttributeIds, final String electionGroupIdentification) {
		final ListType emptyListType = electionInformationType.getList().stream()
				.filter(ListType::isListEmpty)
				.collect(MoreCollectors.onlyElement());
		checkArgument(electionInformationType.getElection().getNumberOfMandates() == emptyListType.getCandidatePosition().size());

		final boolean electionWithLists = electionInformationType.getList().stream().anyMatch(not(ListType::isListEmpty));

		final ElectionType electionType = electionInformationType.getElection();
		final String contestId = random.genRandomString(ID_LENGTH, base16Alphabet);
		final String defaultTitle = getInLanguage(LanguageType.DE, electionType.getElectionDescription().getElectionDescriptionInfo(),
				ElectionDescriptionInfo::getLanguage, ElectionDescriptionInfo::getElectionDescription);
		final boolean fullBlank = true;

		final List<ElectionOption> options = createElectionContestOptions(electionInformationType, electionTypeAttributeIds, emptyListType,
				electionWithLists);
		final List<ElectionAttributes> attributes = createElectionContestAttributes(electionInformationType, electionTypeAttributeIds, emptyListType,
				electionWithLists);
		final List<Question> questions = createElectionContestQuestions(electionInformationType, electionGroupIdentification,
				electionTypeAttributeIds, emptyListType, electionWithLists);

		return new Contest(contestId, defaultTitle, defaultTitle, electionType.getElectionIdentification(), Contest.ELECTIONS_TEMPLATE, fullBlank,
				options, attributes, questions);
	}

	private static List<ElectionOption> createElectionContestOptions(final ElectionInformationType electionInformationType,
			final ElectionTypeAttributeIds electionTypeAttributeIds, final ListType emptyListType, final boolean electionWithLists) {
		final ElectionType electionType = electionInformationType.getElection();
		final List<ElectionOption> options = new ArrayList<>();

		// create an option for all lists if there is at least one non-empty list
		if (electionWithLists) {
			options.addAll(electionInformationType.getList().stream()
					.map(listType -> {
						final String semantics = getListInformation(listType.isListEmpty(), listType.getListDescription().getListDescriptionInfo(),
								ListDescriptionInfo::getListDescription);
						return createGenericOption(electionTypeAttributeIds.listAttributeIdsMap().get(listType.getListIdentification()), semantics);
					})
					.toList());
		}

		// create a candidate option as many times as there are accumulation
		final Map<String, String> candidateAttributeIdsMap = electionTypeAttributeIds.candidateAttributeIdsMap();
		final int candidateAccumulation = electionType.getCandidateAccumulation().intValue();
		final List<CandidateType> candidates = electionInformationType.getCandidate();
		if (candidates != null) {
			options.addAll(createGenericElectionContestOptionCandidate(candidates, candidateAccumulation, candidateAttributeIdsMap,
					electionType.getElectionIdentification()));
		}

		// create an option for all blank candidate positions
		options.addAll(emptyListType.getCandidatePosition().stream()
				.map(candidatePosition -> {
					final String semantics = getBlankCandidatePositionInformation(candidatePosition.getPositionOnList());
					return createGenericOption(candidateAttributeIdsMap.get(candidatePosition.getCandidateListIdentification()), semantics);
				})
				.toList());

		// create an option for all write-in candidate positions if write-ins allowed
		options.addAll(electionInformationType.getWriteInCandidate().stream()
				.map(writeInCandidate -> {
					final String writeInAlias = String.join(ALIAS_JOIN_DELIMITER, electionType.getElectionIdentification(),
							writeInCandidate.getWriteInCandidateIdentification());
					final String semantics = getWriteInPositionInformation(writeInCandidate.getPosition());
					return createGenericOption(candidateAttributeIdsMap.get(writeInAlias), semantics);
				}).toList());

		return options;
	}

	private static List<ElectionOption> createGenericElectionContestOptionCandidate(final List<CandidateType> candidates,
			final int candidateAccumulation, final Map<String, String> candidateAttributeIdsMap, final String electionIdentification) {
		return candidates.stream()
				.flatMap(candidateType -> {
					final String semantics = getCandidateInformation(candidateType.getFamilyName(), candidateType.getFirstName(),
							candidateType.getCallName(), candidateType.getDateOfBirth().toXMLFormat());
					return IntStream.range(0, candidateAccumulation)
							.mapToObj(String::valueOf)
							.map(acc -> String.join(ALIAS_JOIN_DELIMITER, electionIdentification, candidateType.getCandidateIdentification(), acc))
							.map(candidateAlias -> createGenericOption(candidateAttributeIdsMap.get(candidateAlias), semantics));
				}).toList();
	}

	private static List<ElectionAttributes> createElectionContestAttributes(final ElectionInformationType electionInformationType,
			final ElectionTypeAttributeIds electionTypeAttributeIds, final ListType emptyListType, final boolean electionWithLists) {

		// create an attribute for lists
		final List<ElectionAttributes> attributes = new ArrayList<>(
				createElectionContestAttributesList(electionInformationType, electionTypeAttributeIds, electionWithLists));

		// create an attribute for candidates
		final String candidatesAttributeId = electionTypeAttributeIds.candidatesAttributeId();
		attributes.add(new ElectionAttributes(candidatesAttributeId, ALIAS_CANDIDATES, List.of(), true));

		// create an attribute for all candidate identification considering accumulation
		attributes.addAll(electionInformationType.getCandidate().stream()
				.flatMap(candidateType -> createElectionContestAttributeCandidate(electionInformationType, candidateType, electionTypeAttributeIds,
						electionWithLists))
				.toList());

		// create an attribute for all blank candidate positions
		final String electionIdentification = electionInformationType.getElection().getElectionIdentification();
		final Map<String, String> candidateAttributeIdsMap = electionTypeAttributeIds.candidateAttributeIdsMap();
		attributes.addAll(emptyListType.getCandidatePosition().stream()
				.map(CandidatePositionType::getCandidateListIdentification)
				.map(blankCandidateListIdentification -> {
					final String blankAlias = String.join(ALIAS_JOIN_DELIMITER, electionIdentification, blankCandidateListIdentification);
					return new ElectionAttributes(candidateAttributeIdsMap.get(blankCandidateListIdentification), blankAlias,
							List.of(candidatesAttributeId), false);
				})
				.toList());

		// create an attribute for all write-in candidate positions if write-ins allowed
		attributes.addAll(electionInformationType.getWriteInCandidate().stream()
				.map(WriteInCandidateType::getWriteInCandidateIdentification)
				.map(writeInCandidateIdentification -> {
					final String writeInAlias = String.join(ALIAS_JOIN_DELIMITER, electionIdentification, writeInCandidateIdentification);
					return new ElectionAttributes(candidateAttributeIdsMap.get(writeInAlias), writeInAlias, List.of(candidatesAttributeId), false);
				}).toList());

		return attributes;
	}

	private static List<ElectionAttributes> createElectionContestAttributesList(final ElectionInformationType electionInformationType,
			final ElectionTypeAttributeIds electionTypeAttributeIds, final boolean electionWithLists) {
		final String electionIdentification = electionInformationType.getElection().getElectionIdentification();
		final Map<String, String> listAttributeIdsMap = electionTypeAttributeIds.listAttributeIdsMap();
		final String listsAttributeId = electionTypeAttributeIds.listsAttributeId();
		final List<ElectionAttributes> attributes = new ArrayList<>();

		// if at least one list is not empty
		if (electionWithLists) {
			//create an attribute for lists
			attributes.add(new ElectionAttributes(listsAttributeId, ALIAS_LISTS, List.of(), true));

			// create an attribute for all list identifications
			attributes.addAll(electionInformationType.getList().stream()
					.map(listType -> {
						final String listIdentification = listType.getListIdentification();
						final String alias = String.join(ALIAS_JOIN_DELIMITER, electionIdentification, listIdentification);
						return new ElectionAttributes(listAttributeIdsMap.get(listType.getListIdentification()), alias, List.of(listsAttributeId),
								false);
					})
					.toList());
		} else {
			// create an attribute for all candidate's dummy lists
			attributes.addAll(electionInformationType.getCandidate().stream()
					.map(candidateType -> new ElectionAttributes(listAttributeIdsMap.get(candidateType.getCandidateIdentification()),
							ALIAS_PREFIX_DUMMY_LIST + candidateType.getCandidateIdentification(), List.of(),
							false))
					.toList());
		}

		return attributes;
	}

	private static Stream<ElectionAttributes> createElectionContestAttributeCandidate(final ElectionInformationType electionInformationType,
			final CandidateType candidateType, final ElectionTypeAttributeIds electionTypeAttributeIds, final boolean electionWithLists) {
		final String electionIdentification = electionInformationType.getElection().getElectionIdentification();
		final String candidateIdentification = candidateType.getCandidateIdentification();
		final Map<String, String> listAttributeIdsMap = electionTypeAttributeIds.listAttributeIdsMap();

		final List<String> related = new ArrayList<>(List.of(electionTypeAttributeIds.candidatesAttributeId()));
		// search all lists where the candidate identification appears at least once
		if (electionWithLists) {
			related.addAll(electionInformationType.getList().stream()
					.filter(not(ListType::isListEmpty))
					.filter(listType -> listType.getCandidatePosition().stream()
							.anyMatch(candidatePositionType -> candidatePositionType.getCandidateIdentification().equals(candidateIdentification)))
					.map(ListType::getListIdentification)
					.map(listAttributeIdsMap::get)
					.toList());
		} else {
			related.add(listAttributeIdsMap.get(candidateIdentification));
		}

		return IntStream.range(0, electionInformationType.getElection().getCandidateAccumulation().intValue())
				.mapToObj(String::valueOf)
				.map(acc -> {
					final String candidateAlias = String.join(ALIAS_JOIN_DELIMITER, electionIdentification, candidateIdentification, acc);
					final String candidateAttributeId = electionTypeAttributeIds.candidateAttributeIdsMap().get(candidateAlias);
					return new ElectionAttributes(candidateAttributeId, candidateAlias, related, false);
				});
	}

	private static List<Question> createElectionContestQuestions(final ElectionInformationType electionInformationType, final String electionGroupId,
			final ElectionTypeAttributeIds electionTypeAttributeIds, final ListType emptyListType, final boolean electionWithLists) {
		final List<Question> questions = new ArrayList<>();

		final Map<String, String> candidateAttributeIdsMap = electionTypeAttributeIds.candidateAttributeIdsMap();
		final List<String> blankAttributeIds = emptyListType.getCandidatePosition().stream()
				.map(CandidatePositionType::getCandidateListIdentification)
				.map(candidateAttributeIdsMap::get)
				.toList();

		final ElectionType electionType = electionInformationType.getElection();
		final List<String> writeInAttributeIds = electionInformationType.getWriteInCandidate().stream()
				.map(WriteInCandidateType::getWriteInCandidateIdentification)
				.map(writeInCandidateIdentification -> candidateAttributeIdsMap.get(
						String.join(ALIAS_JOIN_DELIMITER, electionType.getElectionIdentification(), writeInCandidateIdentification)))
				.toList();

		final List<BigInteger> electionRelations = electionInformationType.getElection().getReferencedElection().stream()
				.map(ReferencedElectionInformationType::getElectionRelation).toList();

		PrimarySecondaryType primarySecondaryType = PrimarySecondaryType.STANDARD;
		if (!electionRelations.isEmpty()) {
			final boolean isSecondaryElection = electionRelations.stream()
					.anyMatch(electionRelation -> BigInteger.ONE.compareTo(electionRelation) == 0);
			primarySecondaryType = isSecondaryElection ? PrimarySecondaryType.SECONDARY : PrimarySecondaryType.PRIMARY;
		}

		final List<String> implicitlyEligibleCandidateIdentifications = createListOfCandidateIdentifications(electionType, electionInformationType,
				candidateAttributeIdsMap, primarySecondaryType, EligibilityType.IMPLICIT);

		final List<String> implicitlyDiscreetCandidateIdentifications = createListOfCandidateIdentifications(electionType, electionInformationType,
				candidateAttributeIdsMap, primarySecondaryType, EligibilityType.IMPLICIT_DISCREET);

		// create a candidate question
		questions.add(createElectionQuestion(random.genRandomString(ID_LENGTH, base16Alphabet),
				electionType.getNumberOfMandates(), electionType.getMinimalCandidateSelectionInList(),
				electionType.getCandidateAccumulation().intValue(), blankAttributeIds, writeInAttributeIds,
				electionTypeAttributeIds.candidatesAttributeId(), electionGroupId, primarySecondaryType, implicitlyEligibleCandidateIdentifications,
				implicitlyDiscreetCandidateIdentifications));

		// create a list question if at least one list is not empty
		if (electionWithLists) {
			checkState(!electionType.isWriteInsAllowed(), "There cannot be write-ins with multiple lists.");
			questions.add(createElectionQuestion(random.genRandomString(ID_LENGTH, base16Alphabet), 1, 0, 1,
					List.of(electionTypeAttributeIds.listAttributeIdsMap().get(emptyListType.getListIdentification())), List.of(),
					electionTypeAttributeIds.listsAttributeId(), electionGroupId, primarySecondaryType, implicitlyEligibleCandidateIdentifications,
					implicitlyDiscreetCandidateIdentifications));
		}

		return questions;
	}

	private static List<String> createListOfCandidateIdentifications(final ElectionType electionType,
			final ElectionInformationType electionInformationType,
			final Map<String, String> candidateAttributeIdsMap, final PrimarySecondaryType primarySecondaryType,
			final EligibilityType eligibilityType) {
		List<String> candidateIdentifications = new ArrayList<>();
		if (PrimarySecondaryType.SECONDARY.equals(primarySecondaryType)) {
			final int candidateAccumulation = electionType.getCandidateAccumulation().intValue();
			candidateIdentifications = electionInformationType.getCandidate().stream()
					.filter(candidateType -> candidateType.getEligibility().equals(eligibilityType))
					.flatMap(candidateType -> IntStream.range(0, candidateAccumulation)
							.mapToObj(String::valueOf)
							.map(acc -> String.join(ALIAS_JOIN_DELIMITER, electionType.getElectionIdentification(),
									candidateType.getCandidateIdentification(), acc))
							.map(candidateAttributeIdsMap::get)
							.filter(Objects::nonNull))
					.toList();
		}
		return candidateIdentifications;
	}

	private static Question createElectionQuestion(final String id, final int max, final int min, final int accumulation,
			final List<String> blankAttributes, final List<String> writeInAttributes, final String attribute, final String electionGroupId,
			final PrimarySecondaryType primarySecondaryType,
			final List<String> firstRoundElectedCandidates,
			final List<String> secondRoundDiscreetCandidates) {
		return createGenericQuestion(id, max, min, accumulation, blankAttributes, writeInAttributes, attribute, ELECTION_DUMMY_VALUE, false,
				ELECTION_DUMMY_VALUE, electionGroupId, primarySecondaryType, firstRoundElectedCandidates, secondRoundDiscreetCandidates);
	}

	private static Contest createVoteContest(final VoteType voteType, final Map<String, VoteTypeAttributeIds> voteTypeAttributeIdsMap) {
		final String contestId = random.genRandomString(ID_LENGTH, base16Alphabet);
		final String defaultTitle = getInLanguage(LanguageType.DE, voteType.getVoteDescription().getVoteDescriptionInfo(),
				VoteDescriptionInfo::getLanguage, VoteDescriptionInfo::getVoteDescription);

		final List<ElectionOption> options = createVoteContestOptions(voteType, voteTypeAttributeIdsMap);
		final List<ElectionAttributes> attributes = createVoteContestAttributes(voteType, voteTypeAttributeIdsMap);
		final List<Question> questions = createVoteContestQuestions(voteType, voteTypeAttributeIdsMap);

		return new Contest(contestId, defaultTitle, defaultTitle, voteType.getVoteIdentification(), Contest.VOTES_TEMPLATE, false, options,
				attributes, questions);
	}

	private static List<ElectionOption> createVoteContestOptions(final VoteType voteType,
			final Map<String, VoteTypeAttributeIds> voteTypeAttributeIdsMap) {
		return voteType.getBallot().stream()
				.flatMap(ballotType -> {
					final List<ElectionOption> options = new ArrayList<>();

					// create an option for all standard ballot answers
					final StandardBallotType standardBallotType = ballotType.getStandardBallot();
					if (standardBallotType != null) {
						final StandardBallotTypeAdapter standardBallotTypeAdapter = new StandardBallotTypeAdapter(standardBallotType);
						options.addAll(createVoteContestOptions(Stream.of(standardBallotTypeAdapter), voteTypeAttributeIdsMap));
					}

					// create an option for all variant ballot standard answers
					final VariantBallotType variantBallot = ballotType.getVariantBallot();
					if (variantBallot != null) {
						final Stream<QuestionType> standardQuestionTypeAdapterStream = variantBallot.getStandardQuestion().stream()
								.parallel()
								.map(StandardQuestionTypeAdapter::new);
						options.addAll(createVoteContestOptions(standardQuestionTypeAdapterStream, voteTypeAttributeIdsMap));

						// create an option for all variant ballot tie break answers
						final List<TieBreakQuestionType> tieBreakQuestion = variantBallot.getTieBreakQuestion();
						if (tieBreakQuestion != null) {
							final Stream<QuestionType> tieBreakQuestionAdapterStream = tieBreakQuestion.stream()
									.parallel()
									.map(TieBreakQuestionTypeAdapter::new);
							options.addAll(createVoteContestOptions(tieBreakQuestionAdapterStream, voteTypeAttributeIdsMap));
						}
					}

					return options.stream();
				})
				.toList();
	}

	private static List<ElectionOption> createVoteContestOptions(final Stream<QuestionType> questions,
			final Map<String, VoteTypeAttributeIds> voteTypeAttributeIdsMap) {
		return questions
				.flatMap(question -> {
					final VoteTypeAttributeIds attributeIds = voteTypeAttributeIdsMap.get(question.getQuestionIdentification());
					final BallotQuestionType ballotQuestionType = question.getBallotQuestion();
					return question.getAnswer().stream()
							.map(answer -> {
								final String attributeAnswerId = attributeIds.attributeAnswerId()
										.get(answer.getAnswerIdentification());
								final String semantics = getAnswerInformation(answer.isBlankAnswer(), ballotQuestionType.getBallotQuestionInfo(),
										BallotQuestionType.BallotQuestionInfo::getBallotQuestion, answer.getAnswerInfo(),
										AnswerInformationType::getAnswer);
								return createGenericOption(attributeAnswerId, semantics);
							});
				})
				.toList();
	}

	private static List<ElectionAttributes> createVoteContestAttributes(final VoteType voteType,
			final Map<String, VoteTypeAttributeIds> voteTypeAttributeIdsMap) {
		return voteType.getBallot().stream()
				.flatMap(ballotType -> {
					final List<ElectionAttributes> attributes = new ArrayList<>();

					// create an attribute for all standard ballot questions and answers
					final StandardBallotType standardBallotType = ballotType.getStandardBallot();
					if (standardBallotType != null) {
						final StandardBallotTypeAdapter standardBallotTypeAdapter = new StandardBallotTypeAdapter(standardBallotType);
						attributes.addAll(
								createVoteContestAttributes(Stream.of(standardBallotTypeAdapter), voteTypeAttributeIdsMap));
					}

					// create an attribute for all variant ballot standard questions and answers
					final VariantBallotType variantBallot = ballotType.getVariantBallot();
					if (variantBallot != null) {
						final Stream<QuestionType> standardQuestionTypeAdapterStream = variantBallot.getStandardQuestion().stream()
								.parallel()
								.map(StandardQuestionTypeAdapter::new);
						attributes.addAll(
								createVoteContestAttributes(standardQuestionTypeAdapterStream, voteTypeAttributeIdsMap));

						// create an attribute for all variant ballot tie break questions and answers
						final List<TieBreakQuestionType> tieBreakQuestion = variantBallot.getTieBreakQuestion();
						if (tieBreakQuestion != null) {
							final Stream<QuestionType> tieBreakQuestionAdapter = tieBreakQuestion.stream()
									.parallel()
									.map(TieBreakQuestionTypeAdapter::new);
							attributes.addAll(createVoteContestAttributes(tieBreakQuestionAdapter, voteTypeAttributeIdsMap));
						}
					}

					return attributes.stream();
				}).toList();
	}

	private static List<ElectionAttributes> createVoteContestAttributes(final Stream<QuestionType> questions,
			final Map<String, VoteTypeAttributeIds> voteTypeAttributeIdsMap) {
		return questions
				.flatMap(question -> {
					final String questionIdentification = question.getQuestionIdentification();
					final VoteTypeAttributeIds attributeIds = voteTypeAttributeIdsMap.get(questionIdentification);
					final String attributeQuestionId = attributeIds.attributeQuestionId();
					final List<ElectionAttributes> attributes = new ArrayList<>();

					// create an attribute for the question
					final ElectionAttributes attributeQuestion = new ElectionAttributes(attributeQuestionId, questionIdentification, List.of(), true);
					attributes.add(attributeQuestion);

					// create an attribute for all answers
					attributes.addAll(question.getAnswer().stream()
							.map(answer -> {
								final String answerIdentification = answer.getAnswerIdentification();
								final String alias = String.join(ALIAS_JOIN_DELIMITER, questionIdentification, answerIdentification);
								final String attributeAnswerId = attributeIds.attributeAnswerId().get(answerIdentification);
								return new ElectionAttributes(attributeAnswerId, alias, List.of(attributeQuestionId), false);
							})
							.toList());

					return attributes.stream();
				})
				.toList();
	}

	private static List<Question> createVoteContestQuestions(final VoteType voteType,
			final Map<String, VoteTypeAttributeIds> voteTypeAttributeIdsMap) {
		return voteType.getBallot().stream()
				.flatMap(ballotType -> {
					final String ballotIdentification = ballotType.getBallotIdentification();
					final List<Question> questions = new ArrayList<>();

					// create a question for all standard ballot questions
					final StandardBallotType standardBallot = ballotType.getStandardBallot();
					if (standardBallot != null) {
						final StandardBallotTypeAdapter standardBallotTypeAdapter = new StandardBallotTypeAdapter(standardBallot);
						questions.addAll(createVoteContestQuestion(Stream.of(standardBallotTypeAdapter), voteTypeAttributeIdsMap, false,
								ballotIdentification));
					}

					// create a question for all variant ballot standard questions
					final VariantBallotType variantBallot = ballotType.getVariantBallot();
					if (variantBallot != null) {
						final Stream<QuestionType> standardQuestionTypeAdapterStream = variantBallot.getStandardQuestion().stream()
								.parallel()
								.map(StandardQuestionTypeAdapter::new);
						questions.addAll(
								createVoteContestQuestion(standardQuestionTypeAdapterStream, voteTypeAttributeIdsMap, true, ballotIdentification));

						// create a question for all variant ballot tie break questions
						final List<TieBreakQuestionType> tieBreakQuestion = variantBallot.getTieBreakQuestion();
						if (tieBreakQuestion != null) {
							final Stream<QuestionType> tieBreakQuestionAdapter = tieBreakQuestion.stream()
									.parallel()
									.map(TieBreakQuestionTypeAdapter::new);
							questions.addAll(createVoteContestQuestion(tieBreakQuestionAdapter, voteTypeAttributeIdsMap, true, ballotIdentification));
						}
					}

					return questions.stream();
				}).toList();
	}

	private static List<Question> createVoteContestQuestion(final Stream<QuestionType> questions,
			final Map<String, VoteTypeAttributeIds> voteTypeAttributeIdsMap, final boolean variantBallot, final String ballotIdentification) {
		return questions
				.map(question -> {
					final String questionIdentification = question.getQuestionIdentification();
					final VoteTypeAttributeIds attributeIds = voteTypeAttributeIdsMap.get(questionIdentification);
					final String attributeQuestionId = attributeIds.attributeQuestionId();

					final AnswerType blankAnswer = question.getAnswer().stream()
							.filter(AnswerType::isBlankAnswer)
							.collect(MoreCollectors.onlyElement());
					final String attributeAnswerIdBlank = attributeIds.attributeAnswerId()
							.get(blankAnswer.getAnswerIdentification());
					final String questionNumber = question.getQuestionNumber();

					return createGenericQuestion(random.genRandomString(ID_LENGTH, base16Alphabet), 1, 0, 1, List.of(attributeAnswerIdBlank),
							List.of(), attributeQuestionId, questionNumber, variantBallot, ballotIdentification, "", PrimarySecondaryType.STANDARD,
							List.of(), List.of());
				})
				.toList();
	}

	private static ElectionOption createGenericOption(final String attribute, final String semantics) {
		final String optionId = random.genRandomString(ID_LENGTH, base16Alphabet);
		final String representation = "1";
		return new ElectionOption(optionId, attribute, semantics, representation);
	}

	private static Question createGenericQuestion(final String id, final int max, final int min, final int accumulation,
			final List<String> blankAttributes, final List<String> writeInAttributes, final String attribute, final String questionNumber,
			final boolean variantBallot, final String ballotIdentification, final String electionGroupId,
			final PrimarySecondaryType primarySecondaryType,
			final List<String> firstRoundElectedCandidates,
			final List<String> secondRoundDiscreetCandidates) {
		final List<List<String>> fusions = List.of();
		return new Question(id, max, min, accumulation, !writeInAttributes.isEmpty(), blankAttributes, writeInAttributes, attribute, fusions,
				questionNumber, variantBallot, ballotIdentification, electionGroupId, primarySecondaryType, firstRoundElectedCandidates,
				secondRoundDiscreetCandidates);
	}

	private static Map<String, List<ContestWithPosition>> mergeContests(final Map<String, List<ContestWithPosition>> electionContest,
			final Map<String, List<ContestWithPosition>> voteContest) {
		return Stream.of(electionContest, voteContest)
				.flatMap(map -> map.entrySet().stream())
				.collect(Collectors.toMap(
						Map.Entry::getKey,
						entry -> new ArrayList<>(entry.getValue()),
						(contestsLeft, contestsRight) -> {
							contestsLeft.addAll(contestsRight);
							return contestsLeft;
						})
				);
	}

	record ContestWithPosition(Contest contest, int position) {
	}
}
