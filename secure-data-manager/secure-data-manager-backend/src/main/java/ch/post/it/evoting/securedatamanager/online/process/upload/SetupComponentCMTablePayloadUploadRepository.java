/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.online.process.upload;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static ch.post.it.evoting.securedatamanager.shared.Constants.VOTER_PORTAL_CONNECTION_NOT_ENABLED_MESSAGE;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import ch.post.it.evoting.domain.configuration.SetupComponentCMTablePayload;
import ch.post.it.evoting.domain.configuration.SetupComponentCMTablePayloadChunks;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;
import ch.post.it.evoting.securedatamanager.shared.WebClientFactory;

import reactor.core.publisher.Flux;
import reactor.util.retry.RetryBackoffSpec;

@Repository
public class SetupComponentCMTablePayloadUploadRepository {
	private static final Logger LOGGER = LoggerFactory.getLogger(SetupComponentCMTablePayloadUploadRepository.class);

	private final boolean isVoterPortalEnabled;
	private final WebClientFactory webClientFactory;
	private final RetryBackoffSpec retryBackoffSpec;

	public SetupComponentCMTablePayloadUploadRepository(
			@Value("${voter.portal.enabled}")
			final boolean isVoterPortalEnabled,
			final WebClientFactory webClientFactory,
			final RetryBackoffSpec retryBackoffSpec) {
		this.isVoterPortalEnabled = isVoterPortalEnabled;
		this.webClientFactory = webClientFactory;
		this.retryBackoffSpec = retryBackoffSpec;
	}

	/**
	 * Uploads the setup component CMTable payload to the vote verification.
	 *
	 * @param electionEventId                    the election event id. Must be non-null and a valid UUID.
	 * @param verificationCardSetId              the verification card set id. Must be non-null and a valid UUID.
	 * @param setupComponentCMTablePayloadChunks the list of {@link SetupComponentCMTablePayload} to upload. Must be non-null.
	 * @throws NullPointerException      if any of the inputs is null.
	 * @throws FailedValidationException if {@code electionEventId} or {@code verificationCardSetId} is invalid.
	 */
	public void uploadReturnCodesMappingTable(final String electionEventId, final String verificationCardSetId,
			final SetupComponentCMTablePayloadChunks setupComponentCMTablePayloadChunks) {
		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);
		checkNotNull(setupComponentCMTablePayloadChunks);
		checkState(isVoterPortalEnabled, VOTER_PORTAL_CONNECTION_NOT_ENABLED_MESSAGE);

		LOGGER.debug("Uploading CMTable chunk. [electionEventId: {}, verificationCardSetId: {}]", electionEventId, verificationCardSetId);

		final ResponseEntity<Void> response = webClientFactory.getWebClient(String.format(
						"Request for uploading setup component CMTable payloads failed. [electionEventId: %s, verificationCardSetId: %s]",
						electionEventId, verificationCardSetId))
				.post()
				.uri(uriBuilder -> uriBuilder.path(
								"api/v1/processor/configuration/returncodesmappingtable/electionevent/{electionEventId}/verificationcardset/{verificationCardSetId}")
						.build(electionEventId, verificationCardSetId))
				.contentType(MediaType.APPLICATION_NDJSON)
				.accept(MediaType.APPLICATION_JSON)
				.body(Flux.fromIterable(setupComponentCMTablePayloadChunks.payloads()), SetupComponentCMTablePayload.class)
				.retrieve()
				.toBodilessEntity()
				.retryWhen(retryBackoffSpec)
				.block();

		checkState(checkNotNull(response).getStatusCode().is2xxSuccessful());

		LOGGER.debug("Successfully uploaded setup component CMTable payloads. [electionEventId: {}, verificationCardSetId: {}, chunkCount: {}]",
				electionEventId, verificationCardSetId, setupComponentCMTablePayloadChunks.getChunkCount());
	}
}
