/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.process.generateprintfile;

import static ch.post.it.evoting.evotinglibraries.domain.validations.EncryptionParametersSeedValidation.validateSeed;
import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static ch.post.it.evoting.securedatamanager.shared.Constants.SETUP_COMPONENT_EVOTING_PRINT_XML;
import static com.google.common.base.Preconditions.checkNotNull;

import java.nio.file.Path;
import java.security.SignatureException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Repository;

import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.evotinglibraries.domain.common.ChannelSecurityContextData;
import ch.post.it.evoting.evotinglibraries.domain.signature.Alias;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;
import ch.post.it.evoting.evotinglibraries.xml.XmlFileRepository;
import ch.post.it.evoting.evotinglibraries.xml.XsdConstants;
import ch.post.it.evoting.evotinglibraries.xml.hashable.HashableSetupComponentEvotingPrintFactory;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingprint.VotingCardList;
import ch.post.it.evoting.securedatamanager.shared.process.PathResolver;

@Repository
@ConditionalOnProperty("role.isSetup")
public class EvotingPrintFileRepository extends XmlFileRepository<VotingCardList> {

	private static final Logger LOGGER = LoggerFactory.getLogger(EvotingPrintFileRepository.class);

	private final PathResolver pathResolver;
	private final SignatureKeystore<Alias> signatureKeystoreService;
	private final String filename;

	public EvotingPrintFileRepository(
			final PathResolver pathResolver,
			final SignatureKeystore<Alias> signatureKeystoreService,
			@Value("${sdm.election.event.seed}")
			final String electionEventSeed) {
		this.pathResolver = pathResolver;
		this.signatureKeystoreService = signatureKeystoreService;
		this.filename = String.format(SETUP_COMPONENT_EVOTING_PRINT_XML, validateSeed(electionEventSeed));
	}

	/**
	 * Saves the given voting card list in the {@value ch.post.it.evoting.securedatamanager.shared.Constants#SETUP_COMPONENT_EVOTING_PRINT_XML} file
	 * while validating it against the related {@value XsdConstants#SETUP_COMPONENT_EVOTING_PRINT_XSD}.
	 *
	 * @param electionEventId the election event id. Must be non-null and a valid UUID.
	 * @param votingCardList  the voting card list. Must be non-null.
	 * @throws NullPointerException      if any input is null.
	 * @throws FailedValidationException if the election event id is not a valid UUID.
	 */
	public void save(final String electionEventId, final VotingCardList votingCardList) {
		validateUUID(electionEventId);
		checkNotNull(votingCardList);

		LOGGER.debug("Signing setup component evoting print... [electionEventId: {}]", electionEventId);

		final byte[] signature = getSignature(electionEventId, votingCardList);
		votingCardList.setSignature(signature);

		LOGGER.debug("Setup component evoting print successfully signed. Saving file... [filename: {}, electionEventId: {}]", filename, electionEventId);

		final Path xmlFilePath = pathResolver.resolvePrintingOutputPath().resolve(filename);
		final Path writePath = write(votingCardList, XsdConstants.SETUP_COMPONENT_EVOTING_PRINT_XSD, xmlFilePath);

		LOGGER.debug("File successfully saved. [electionEventId: {}, path: {}]", electionEventId, writePath);
	}

	private byte[] getSignature(final String electionEventId, final VotingCardList votingCardList) {
		final Hashable hashable = HashableSetupComponentEvotingPrintFactory.fromVotingCardList(votingCardList);
		final Hashable additionalContextData = ChannelSecurityContextData.setupComponentEvotingPrint();

		try {
			return signatureKeystoreService.generateSignature(hashable, additionalContextData);
		} catch (final SignatureException e) {
			throw new IllegalStateException(String.format("Could not sign setup component evoting print. [electionEventId: %s]", electionEventId));
		}
	}

	public String getFilename() {
		return filename;
	}
}
