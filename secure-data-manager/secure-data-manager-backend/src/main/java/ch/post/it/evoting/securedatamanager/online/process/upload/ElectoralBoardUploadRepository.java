/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.online.process.upload;

import static ch.post.it.evoting.securedatamanager.shared.Constants.VOTER_PORTAL_CONNECTION_NOT_ENABLED_MESSAGE;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import ch.post.it.evoting.evotinglibraries.domain.mixnet.SetupComponentPublicKeysPayload;
import ch.post.it.evoting.securedatamanager.shared.WebClientFactory;

import reactor.core.publisher.Mono;
import reactor.util.retry.RetryBackoffSpec;

/**
 * Implements the repository for electoral board uploadDataInContext.
 */
@Repository
public class ElectoralBoardUploadRepository {

	private final boolean isVoterPortalEnabled;
	private final WebClientFactory webClientFactory;
	private final RetryBackoffSpec retryBackoffSpec;

	public ElectoralBoardUploadRepository(
			final WebClientFactory webClientFactory,
			final RetryBackoffSpec retryBackoffSpec,
			@Value("${voter.portal.enabled}")
			final boolean isVoterPortalEnabled) {

		this.isVoterPortalEnabled = isVoterPortalEnabled;
		this.webClientFactory = webClientFactory;
		this.retryBackoffSpec = retryBackoffSpec;
	}

	/**
	 * Uploads the setup component public keys payload to the vote verification.
	 *
	 * @param setupComponentPublicKeysPayload the setup component public keys payload to upload. Must be non-null.
	 * @throws IllegalStateException if the upload was unsuccessful.
	 * @throws NullPointerException  if the setup component public keys payload is null.
	 */
	public void uploadSetupComponentPublicKeysData(final SetupComponentPublicKeysPayload setupComponentPublicKeysPayload) {
		checkNotNull(setupComponentPublicKeysPayload);

		checkState(isVoterPortalEnabled, VOTER_PORTAL_CONNECTION_NOT_ENABLED_MESSAGE);

		final String electionEventId = setupComponentPublicKeysPayload.getElectionEventId();
		final ResponseEntity<Void> response = webClientFactory.getWebClient(
						String.format("Request for uploading setup component public keys failed. [electionEventId: %s]", electionEventId))
				.post()
				.uri(uriBuilder -> uriBuilder.path("api/v1/processor/configuration/setupkeys/electionevent/{electionEventId}").build(electionEventId))
				.accept(MediaType.APPLICATION_JSON)
				.body(Mono.just(setupComponentPublicKeysPayload), SetupComponentPublicKeysPayload.class)
				.retrieve()
				.toBodilessEntity()
				.retryWhen(retryBackoffSpec)
				.block();

		checkState(checkNotNull(response).getStatusCode().is2xxSuccessful());
	}

}
