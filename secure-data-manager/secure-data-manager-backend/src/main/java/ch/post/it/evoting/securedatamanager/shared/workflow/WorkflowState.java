/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.shared.workflow;

import java.time.LocalDateTime;

public record WorkflowState(WorkflowStep step, LocalDateTime startDate, LocalDateTime endDate, WorkflowStatus status, String contextId,
							WorkflowExceptionCode exceptionCode) {
}
