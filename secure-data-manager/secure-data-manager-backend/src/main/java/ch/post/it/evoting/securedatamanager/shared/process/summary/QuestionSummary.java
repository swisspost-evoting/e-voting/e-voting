/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.shared.process.summary;

import java.util.List;

public record QuestionSummary(String questionNumber, List<Description> questionInfo, List<AnswerSummary> answers) {
}
