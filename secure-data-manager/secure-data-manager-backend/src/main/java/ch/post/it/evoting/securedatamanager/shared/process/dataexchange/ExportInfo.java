/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.shared.process.dataexchange;

public record ExportInfo(String outputFolder, String exportFilename) {
}
