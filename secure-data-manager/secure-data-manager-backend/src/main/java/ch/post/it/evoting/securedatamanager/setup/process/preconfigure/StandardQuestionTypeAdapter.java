/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.process.preconfigure;

import java.util.List;

import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.BallotQuestionType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.StandardQuestionType;

/**
 * Adapter for {@link StandardQuestionType}.
 */
public final class StandardQuestionTypeAdapter implements QuestionType {

	final StandardQuestionType standardQuestionType;

	public StandardQuestionTypeAdapter(final StandardQuestionType standardQuestionType) {
		this.standardQuestionType = standardQuestionType;
	}

	@Override
	public String getQuestionIdentification() {
		return standardQuestionType.getQuestionIdentification();
	}

	@Override
	public BallotQuestionType getBallotQuestion() {
		return standardQuestionType.getBallotQuestion();
	}

	@Override
	public String getQuestionNumber() {
		return standardQuestionType.getQuestionNumber();
	}

	@Override
	public List<AnswerType> getAnswer() {
		return standardQuestionType.getAnswer().stream()
				.parallel()
				.<AnswerType>map(StandardAnswerTypeAdapter::new)
				.toList();
	}
}
