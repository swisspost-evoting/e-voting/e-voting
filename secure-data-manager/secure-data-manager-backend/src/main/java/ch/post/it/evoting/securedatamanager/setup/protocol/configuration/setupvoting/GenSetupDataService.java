/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.protocol.configuration.setupvoting;

import static ch.post.it.evoting.evotinglibraries.domain.validations.EncryptionParametersSeedValidation.validateSeed;
import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.evotinglibraries.domain.election.Ballot;
import ch.post.it.evoting.securedatamanager.setup.process.precompute.BallotConfigService;
import ch.post.it.evoting.securedatamanager.shared.process.VotingCardSetRepository;
import ch.post.it.evoting.securedatamanager.shared.process.BallotBoxService;

@Service
@ConditionalOnProperty("role.isSetup")
public class GenSetupDataService {

	private final BallotConfigService ballotConfigService;
	private final BallotBoxService ballotBoxService;
	private final VotingCardSetRepository votingCardSetRepository;
	private final GenSetupDataAlgorithm genSetupDataAlgorithm;

	public GenSetupDataService(
			final BallotConfigService ballotConfigService,
			final BallotBoxService ballotBoxService,
			final VotingCardSetRepository votingCardSetRepository,
			final GenSetupDataAlgorithm genSetupDataAlgorithm) {
		this.ballotConfigService = ballotConfigService;
		this.ballotBoxService = ballotBoxService;
		this.votingCardSetRepository = votingCardSetRepository;
		this.genSetupDataAlgorithm = genSetupDataAlgorithm;
	}

	/**
	 * Invokes the GenSetupData algorithm.
	 *
	 * @param electionEventId the election event id. Must be non-null and a valid UUID.
	 * @param seed            the encryption parameters seed. Must be non-null and a valid seed.
	 */
	public GenSetupDataOutput genSetupData(final String electionEventId, final String seed) {
		validateUUID(electionEventId);
		validateSeed(seed);

		final List<String> votingCardSetIds = votingCardSetRepository.findAllVotingCardSetIds(electionEventId);

		final Map<String, List<PrimesMappingTableEntrySubset>> optionsInformationPerVerificationCardSetId = votingCardSetIds.stream()
				.parallel()
				.collect(Collectors.toMap(
						votingCardSetRepository::getVerificationCardSetId,
						votingCardSetId -> {
							final String ballotBoxId = votingCardSetRepository.getBallotBoxId(votingCardSetId);
							final String ballotId = ballotBoxService.getBallotId(ballotBoxId);
							final Ballot ballot = ballotConfigService.getBallot(electionEventId, ballotId);

							final List<String> actualVotingOptions = ballot.getActualVotingOptions();
							final List<String> semanticInformation = ballot.getSemanticInformation();
							final List<String> correctnessInformation = ballot.getCorrectnessInformation();

							return IntStream.range(0, actualVotingOptions.size()).parallel()
									.mapToObj(i -> new PrimesMappingTableEntrySubset(actualVotingOptions.get(i), semanticInformation.get(i),
											correctnessInformation.get(i)))
									.toList();
						}));

		final GenSetupDataContext genSetupDataContext = new GenSetupDataContext(optionsInformationPerVerificationCardSetId);
		return genSetupDataAlgorithm.genSetupData(genSetupDataContext, seed);
	}
}
