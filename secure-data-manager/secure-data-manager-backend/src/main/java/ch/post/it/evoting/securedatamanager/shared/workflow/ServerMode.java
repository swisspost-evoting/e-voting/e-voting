/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.shared.workflow;

public enum ServerMode {
	SERVER_MODE_ONLINE,
	SERVER_MODE_SETUP,
	SERVER_MODE_TALLY,
	SERVER_MODE_TESTING
}
