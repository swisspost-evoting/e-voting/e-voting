/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.protocol.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.math.GroupVector.toGroupVector;
import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.security.SignatureException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.ZqElement;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.domain.InvalidPayloadSignatureException;
import ch.post.it.evoting.domain.configuration.setupvoting.VoterInitialCodes;
import ch.post.it.evoting.evotinglibraries.domain.common.ChannelSecurityContextData;
import ch.post.it.evoting.evotinglibraries.domain.configuration.SetupComponentTallyDataPayload;
import ch.post.it.evoting.evotinglibraries.domain.election.PrimesMappingTable;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.ElectionEventContextPayload;
import ch.post.it.evoting.evotinglibraries.domain.signature.Alias;
import ch.post.it.evoting.evotinglibraries.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.securedatamanager.setup.process.VerificationCardSecretKey;
import ch.post.it.evoting.securedatamanager.setup.process.VerificationCardSecretKeyPayloadService;
import ch.post.it.evoting.securedatamanager.setup.process.VoterInitialCodesPayloadService;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventContextPayloadService;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventService;
import ch.post.it.evoting.securedatamanager.shared.process.SetupComponentTallyDataPayloadService;

@Service
@ConditionalOnProperty("role.isSetup")
public class GenCredDatService {

	private static final Logger LOGGER = LoggerFactory.getLogger(GenCredDatService.class);

	private final GenCredDatAlgorithm genCredDatAlgorithm;
	private final ElectionEventService electionEventService;
	private final SignatureKeystore<Alias> signatureKeystoreService;
	private final VoterInitialCodesPayloadService voterInitialCodesPayloadService;
	private final ElectionEventContextPayloadService electionEventContextPayloadService;
	private final SetupComponentTallyDataPayloadService setupComponentTallyDataPayloadService;
	private final VerificationCardSecretKeyPayloadService verificationCardSecretKeyPayloadService;

	public GenCredDatService(
			final GenCredDatAlgorithm genCredDatAlgorithm,
			final ElectionEventService electionEventService,
			final SignatureKeystore<Alias> signatureKeystoreService,
			final VoterInitialCodesPayloadService voterInitialCodesPayloadService,
			final ElectionEventContextPayloadService electionEventContextPayloadService,
			final SetupComponentTallyDataPayloadService setupComponentTallyDataPayloadService,
			final VerificationCardSecretKeyPayloadService verificationCardSecretKeyPayloadService) {
		this.genCredDatAlgorithm = genCredDatAlgorithm;
		this.electionEventService = electionEventService;
		this.signatureKeystoreService = signatureKeystoreService;
		this.voterInitialCodesPayloadService = voterInitialCodesPayloadService;
		this.electionEventContextPayloadService = electionEventContextPayloadService;
		this.setupComponentTallyDataPayloadService = setupComponentTallyDataPayloadService;
		this.verificationCardSecretKeyPayloadService = verificationCardSecretKeyPayloadService;
	}

	/**
	 * Invokes the GenCredDat algorithm.
	 *
	 * @param electionEventId                      the election event id. Must be non-null and a valid UUID.
	 * @param verificationCardSetId                the verification card set id. Must be non-null and a valid UUID.
	 * @param votingCardSetId                      the voting card set id. Must be non-null and a valid UUID.
	 * @param electionPublicKey                    the election public key. Must be non-null.
	 * @param choiceReturnCodesEncryptionPublicKey the choice return codes encryption public key. Must be non-null.
	 */
	public GenCredDatOutput genCredDat(final String electionEventId, final String verificationCardSetId, final String votingCardSetId,
			final ElGamalMultiRecipientPublicKey electionPublicKey, final ElGamalMultiRecipientPublicKey choiceReturnCodesEncryptionPublicKey) {
		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);
		validateUUID(votingCardSetId);
		checkNotNull(electionPublicKey);
		checkNotNull(choiceReturnCodesEncryptionPublicKey);
		checkArgument(electionEventService.exists(electionEventId));
		checkArgument(choiceReturnCodesEncryptionPublicKey.getGroup().equals(electionPublicKey.getGroup()),
				"The choice return codes encryption public key and the election public key must have the same group");

		final GqGroup encryptionGroup = loadElectionEventContextPayload(electionEventId).getEncryptionGroup();
		final PrimesMappingTable primesMappingTable = electionEventContextPayloadService.loadPrimesMappingTable(electionEventId,
				verificationCardSetId);

		final SetupComponentTallyDataPayload setupComponentTallyDataPayload = setupComponentTallyDataPayloadService.load(electionEventId,
				verificationCardSetId);
		final List<String> verificationCardIds = setupComponentTallyDataPayload.getVerificationCardIds();

		final List<VerificationCardSecretKey> verificationCardSecretKeyList = verificationCardSecretKeyPayloadService.load(electionEventId,
				verificationCardSetId).verificationCardSecretKeys();
		checkState(verificationCardSecretKeyList.stream().map(VerificationCardSecretKey::verificationCardId).toList()
						.equals(verificationCardIds),
				"The SetupComponentTallyDataPayload and VerificationCardSecretKeyPayload must have the same verification card ids.");

		final GroupVector<ZqElement, ZqGroup> verificationCardSecretKeys = verificationCardSecretKeyList.stream()
				.flatMap(verificationCardSecretKey -> verificationCardSecretKey.privateKey().stream())
				.collect(toGroupVector());

		final List<String> startVotingKeys = voterInitialCodesPayloadService.load(electionEventId, votingCardSetId).voterInitialCodes()
				.stream()
				.map(VoterInitialCodes::startVotingKey)
				.toList();

		final GenCredDatContext genCredDatContext = new GenCredDatContext.Builder()
				.setEncryptionGroup(encryptionGroup)
				.setElectionEventId(electionEventId)
				.setVerificationCardSetId(verificationCardSetId)
				.setVerificationCardIds(verificationCardIds)
				.setPrimesMappingTable(primesMappingTable)
				.setElectionPublicKey(electionPublicKey)
				.setChoiceReturnCodesEncryptionPublicKey(choiceReturnCodesEncryptionPublicKey)
				.build();
		final GenCredDatInput genCredDatInput = new GenCredDatInput(verificationCardSecretKeys, startVotingKeys);

		LOGGER.debug("Performing GenCredDat algorithm... [electionEventId: {}, verificationCardSetId: {}]", electionEventId,
				verificationCardSetId);

		return genCredDatAlgorithm.genCredDat(genCredDatContext, genCredDatInput);
	}

	private ElectionEventContextPayload loadElectionEventContextPayload(final String electionEventId) {
		final ElectionEventContextPayload electionEventContextPayload = electionEventContextPayloadService.load(electionEventId);

		final CryptoPrimitivesSignature signature = electionEventContextPayload.getSignature();

		checkState(signature != null, "The signature of the election event context payload is null. [electionEventId: %s]", electionEventId);

		final Hashable additionalContextData = ChannelSecurityContextData.electionEventContext(electionEventId);

		final boolean isSignatureValid;
		try {
			isSignatureValid = signatureKeystoreService.verifySignature(Alias.SDM_CONFIG, electionEventContextPayload,
					additionalContextData, electionEventContextPayload.getSignature().signatureContents());
		} catch (final SignatureException e) {
			throw new IllegalStateException(
					String.format("Could not verify the signature of the election event context payload. [electionEventId: %s]", electionEventId));
		}

		if (!isSignatureValid) {
			throw new InvalidPayloadSignatureException(ElectionEventContextPayload.class, String.format("[electionEventId: %s]", electionEventId));
		}

		return electionEventContextPayload;
	}
}
