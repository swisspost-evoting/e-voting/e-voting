/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.tally.process.decrypt;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.post.it.evoting.evotinglibraries.domain.election.BallotBox;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventService;

@RestController
@RequestMapping("/sdm-tally/decrypt")
@ConditionalOnProperty("role.isTally")
public class DecryptController {

	private static final Logger LOGGER = LoggerFactory.getLogger(DecryptController.class);

	private final DecryptService decryptService;
	private final ElectionEventService electionEventService;

	public DecryptController(
			final DecryptService decryptService,
			final ElectionEventService electionEventService) {
		this.decryptService = decryptService;
		this.electionEventService = electionEventService;
	}

	@PostMapping
	public void decrypt(
			@RequestBody
			final DecryptInput decryptInput) {

		checkNotNull(decryptInput);

		final String electionEventId = electionEventService.findElectionEventId();

		LOGGER.debug("Received request to decrypt ballot boxes. [electionEventId: {}]", electionEventId);

		decryptService.decrypt(electionEventId, decryptInput.ballotBoxIds(), decryptInput.electoralBoardPasswords());

		LOGGER.info("Decryption process has been started. [electionEventId: {}]", electionEventId);
	}

	/**
	 * Returns the list of all ballot downloaded boxes.
	 */
	@GetMapping(value = "ballot-boxes", produces = "application/json")
	public List<BallotBox> getBallotBoxes() {
		final String electionEventId = electionEventService.findElectionEventId();

		return decryptService.getBallotBoxes(electionEventId);
	}

	/**
	 * Returns the list of all decrypted test ballot boxes votes.
	 */
	@GetMapping(value = "test-votes", produces = "application/json")
	public List<BallotBoxTestVote> getBallotBoxTestVotes() {
		final String electionEventId = electionEventService.findElectionEventId();

		return decryptService.getBallotBoxTestVotes(electionEventId);
	}

}
