/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.online.process.mixdownload;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static ch.post.it.evoting.securedatamanager.shared.workflow.WorkflowExceptionCode.DEFAULT;
import static ch.post.it.evoting.securedatamanager.shared.workflow.WorkflowStatus.COMPLETE;
import static ch.post.it.evoting.securedatamanager.shared.workflow.WorkflowStep.DOWNLOAD_BALLOT_BOX;
import static ch.post.it.evoting.securedatamanager.shared.workflow.WorkflowStep.MIX_BALLOT_BOX;
import static ch.post.it.evoting.securedatamanager.shared.workflow.WorkflowStep.MIX_DOWNLOAD;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.domain.tally.BallotBoxStatus;
import ch.post.it.evoting.securedatamanager.shared.process.BallotBoxService;
import ch.post.it.evoting.securedatamanager.shared.workflow.WorkflowExceptionCode;
import ch.post.it.evoting.securedatamanager.shared.workflow.WorkflowExceptionHandler;
import ch.post.it.evoting.securedatamanager.shared.workflow.WorkflowService;

@Service
public class MixDownloadService {

	private static final Logger LOGGER = LoggerFactory.getLogger(MixDownloadService.class);

	private final ExecutorService executorService;
	private final WorkflowService workflowService;
	private final WorkflowExceptionHandler workflowExceptionHandler;
	private final BallotBoxService ballotBoxService;
	private MixDecryptOnlineService mixDecryptOnlineService;

	public MixDownloadService(
			final ExecutorService fixedThreadExecutorService,
			final WorkflowService workflowService,
			final WorkflowExceptionHandler workflowExceptionHandler,
			final BallotBoxService ballotBoxService,
			final MixDecryptOnlineService mixDecryptOnlineService) {
		this.executorService = fixedThreadExecutorService;
		this.workflowService = workflowService;
		this.workflowExceptionHandler = workflowExceptionHandler;
		this.ballotBoxService = ballotBoxService;
		this.mixDecryptOnlineService = mixDecryptOnlineService;
	}

	public void mixAndDownload(final String electionEventId, final List<String> ballotBoxIds) {
		validateUUID(electionEventId);

		workflowService.notifyInProgress(MIX_DOWNLOAD);

		final List<CompletableFuture<Void>> futures = ballotBoxIds.stream()
				.map(ballotBoxId -> {

					final Runnable mixDownloadTask = () -> {
						// Start mixing
						workflowService.notifyInProgress(MIX_BALLOT_BOX, ballotBoxId);
						mixDecryptOnlineService.startOnlineMixing(electionEventId, ballotBoxId);

						// Polling mixing status
						pollBallotBoxStatus(electionEventId, ballotBoxId);
						workflowService.notifyComplete(MIX_BALLOT_BOX, ballotBoxId);

						// Download
						workflowService.notifyInProgress(DOWNLOAD_BALLOT_BOX, ballotBoxId);
						mixDecryptOnlineService.downloadOnlineMixnetPayloads(electionEventId, ballotBoxId);
						workflowService.notifyComplete(DOWNLOAD_BALLOT_BOX, ballotBoxId);
					};

					return CompletableFuture.runAsync(mixDownloadTask, executorService)
							.whenComplete((unused, throwable) -> {
								if (throwable != null) {
									final BallotBoxStatus ballotBoxStatus = ballotBoxService.getBallotBoxStatus(ballotBoxId);
									// If the ballot box is in MIXED status, then the download step has failed.
									if (BallotBoxStatus.MIXED.equals(ballotBoxStatus)) {
										workflowService.notifyError(DOWNLOAD_BALLOT_BOX, ballotBoxId, DEFAULT);
									}
									// Else, the mix step has failed.
									else {
										workflowService.notifyError(MIX_BALLOT_BOX, ballotBoxId, DEFAULT);
									}
								}
							});
				})
				.toList();

		CompletableFuture.allOf(futures.toArray(new CompletableFuture[] {}))
				.whenComplete((unused, throwable) -> {
					if (throwable != null) {
						final WorkflowExceptionCode exceptionCode = workflowExceptionHandler.handleException(MIX_DOWNLOAD, throwable);
						workflowService.notifyError(MIX_DOWNLOAD, exceptionCode);
						LOGGER.error("The workflow step [{}] has failed.", MIX_DOWNLOAD.name(), throwable);
					} else {
						// Check children states to determine if the workflow step is complete.
						final boolean allComplete = this.workflowService.getWorkflowStateList().stream()
								.filter(workflowState -> workflowState.step().equals(DOWNLOAD_BALLOT_BOX))
								.allMatch(workflowState -> COMPLETE.equals(workflowState.status()));

						if (allComplete) {
							this.workflowService.notifyComplete(MIX_DOWNLOAD);
						} else {
							this.workflowService.notifyReady(MIX_DOWNLOAD);
						}

						LOGGER.info("The workflow step [{}] has been successfully processed.", MIX_DOWNLOAD.name());
					}

				});
	}

	private void pollBallotBoxStatus(final String electionEventId, final String ballotBoxId) {

		BallotBoxStatus mixingStatus;
		try (final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1)) {

			mixingStatus = BallotBoxStatus.MIXING;
			while (BallotBoxStatus.MIXING.equals(mixingStatus) || BallotBoxStatus.MIXING_NOT_STARTED.equals(mixingStatus)) {
				try {
					LOGGER.info("Wait before polling mix status, [ballotBoxId: {}]", ballotBoxId);
					final ScheduledFuture<?> sleep = scheduler.schedule(() -> {
					}, 10, TimeUnit.SECONDS);
					sleep.get();
				} catch (final InterruptedException | ExecutionException e) {
					Thread.currentThread().interrupt();
					final String errorMessage = String.format("Error while polling ballot box status. [electionEvenId: %s, ballotBoxId: %s]",
							electionEventId,
							ballotBoxId);
					throw new IllegalStateException(errorMessage, e);
				}
				
				LOGGER.info("Poll mix status, [verificationCardSetId: {}]", ballotBoxId);
				mixingStatus = mixDecryptOnlineService.getOnlineStatus(electionEventId, ballotBoxId);
				LOGGER.info("Mix status, [ballotBoxId: {}, status: {}]", ballotBoxId, mixingStatus);
			}
		}

		// Update status
		ballotBoxService.updateBallotBoxStatus(ballotBoxId, mixingStatus);

		// Check if mixing failed
		if (BallotBoxStatus.MIXING_ERROR.equals(mixingStatus)) {
			LOGGER.error("Mixing failed. [electionEventId: {}, ballotBoxId: {}]", electionEventId, ballotBoxId);
			throw new IllegalStateException(
					String.format("Mixing failed. [electionEventId: %s, ballotBoxId:%s]", electionEventId, ballotBoxId));
		}
	}

}
