/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.process.preconfigure;

import java.util.List;

import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.AnswerInformationType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.StandardAnswerType;

/**
 * Adapter for {@link StandardAnswerType}.
 */
public final class StandardAnswerTypeAdapter implements AnswerType {

	final StandardAnswerType standardAnswerType;

	public StandardAnswerTypeAdapter(final StandardAnswerType standardAnswerType) {
		this.standardAnswerType = standardAnswerType;
	}

	@Override
	public boolean isBlankAnswer() {
		return Boolean.TRUE.equals(standardAnswerType.isHiddenAnswer());
	}

	@Override
	public String getAnswerIdentification() {
		return standardAnswerType.getAnswerIdentification();
	}

	@Override
	public List<AnswerInformationType> getAnswerInfo() {
		return standardAnswerType.getAnswerInfo();
	}
}
