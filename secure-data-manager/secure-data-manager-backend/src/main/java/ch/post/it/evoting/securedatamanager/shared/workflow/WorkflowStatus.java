/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.shared.workflow;

public enum WorkflowStatus {
	IDLE,
	READY,
	IN_PROGRESS,
	COMPLETE,
	ERROR
}
