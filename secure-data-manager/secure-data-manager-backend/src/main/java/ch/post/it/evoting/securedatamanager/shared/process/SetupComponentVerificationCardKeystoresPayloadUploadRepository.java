/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.shared.process;

import static ch.post.it.evoting.securedatamanager.shared.Constants.VOTER_PORTAL_CONNECTION_NOT_ENABLED_MESSAGE;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import ch.post.it.evoting.domain.configuration.SetupComponentVerificationCardKeystoresPayload;
import ch.post.it.evoting.securedatamanager.shared.WebClientFactory;

import reactor.core.publisher.Mono;
import reactor.util.retry.RetryBackoffSpec;

@Repository
public class SetupComponentVerificationCardKeystoresPayloadUploadRepository {

	private final boolean isVoterPortalEnabled;
	private final WebClientFactory webClientFactory;
	private final RetryBackoffSpec retryBackoffSpec;

	public SetupComponentVerificationCardKeystoresPayloadUploadRepository(
			@Value("${voter.portal.enabled}")
			final boolean isVoterPortalEnabled,
			final WebClientFactory webClientFactory,
			final RetryBackoffSpec retryBackoffSpec) {
		this.isVoterPortalEnabled = isVoterPortalEnabled;
		this.webClientFactory = webClientFactory;
		this.retryBackoffSpec = retryBackoffSpec;
	}

	/**
	 * Uploads the setup component verification card keystores payload to the vote verification.
	 *
	 * @param setupComponentVerificationCardKeystoresPayload the {@link SetupComponentVerificationCardKeystoresPayload} to upload. Must be non-null.
	 * @throws NullPointerException if the input is null.
	 */
	public void upload(final SetupComponentVerificationCardKeystoresPayload setupComponentVerificationCardKeystoresPayload) {

		checkNotNull(setupComponentVerificationCardKeystoresPayload);

		checkState(isVoterPortalEnabled, VOTER_PORTAL_CONNECTION_NOT_ENABLED_MESSAGE);

		final String electionEventId = setupComponentVerificationCardKeystoresPayload.getElectionEventId();
		final String verificationCardSetId = setupComponentVerificationCardKeystoresPayload.getVerificationCardSetId();

		final ResponseEntity<Void> response = webClientFactory.getWebClient(
						String.format("Request for uploading setup component verification card keystores payload failed. "
								+ "[electionEventId: %s, verificationCardSetId: %s]", electionEventId, verificationCardSetId))
				.post()
				.uri(uriBuilder -> uriBuilder.path(
								"api/v1/processor/configuration/setupcomponentverificationcardkeystores/electionevent/{electionEventId}/verificationcardset/{verificationCardSetId}")
						.build(electionEventId, verificationCardSetId))
				.accept(MediaType.APPLICATION_JSON)
				.body(Mono.just(setupComponentVerificationCardKeystoresPayload), SetupComponentVerificationCardKeystoresPayload.class)
				.retrieve()
				.toBodilessEntity()
				.retryWhen(retryBackoffSpec)
				.block();

		checkState(checkNotNull(response).getStatusCode().is2xxSuccessful());
	}

}
