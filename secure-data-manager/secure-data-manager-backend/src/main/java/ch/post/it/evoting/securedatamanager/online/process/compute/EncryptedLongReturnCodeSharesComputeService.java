/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.online.process.compute;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.evotinglibraries.domain.returncodes.SetupComponentVerificationDataPayload;
import ch.post.it.evoting.securedatamanager.shared.WebClientFactory;

import reactor.core.publisher.Flux;
import reactor.util.retry.RetryBackoffSpec;

@Service
public class EncryptedLongReturnCodeSharesComputeService {

	private final WebClientFactory webClientFactory;
	private final RetryBackoffSpec retryBackoffSpec;

	public EncryptedLongReturnCodeSharesComputeService(
			final WebClientFactory webClientFactory,
			final RetryBackoffSpec retryBackoffSpec) {
		this.webClientFactory = webClientFactory;
		this.retryBackoffSpec = retryBackoffSpec;
	}

	/**
	 * Send a Setup Component verification data list to the control components to generate the encrypted long Return Code Shares (encrypted long
	 * Choice Return Code shares and encrypted long Vote Cast Return Code shares).
	 */
	public void computeGenEncLongCodeShares(final String electionEventId, final String verificationCardSetId,
			final List<SetupComponentVerificationDataPayload> setupComponentVerificationDataPayloads) {
		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);
		checkNotNull(setupComponentVerificationDataPayloads);
		setupComponentVerificationDataPayloads.forEach(setupComponentVerificationDataPayload ->
		{
			checkNotNull(setupComponentVerificationDataPayload);
			checkArgument(setupComponentVerificationDataPayload.getElectionEventId().equals(electionEventId));
			checkArgument(setupComponentVerificationDataPayload.getVerificationCardSetId().equals(verificationCardSetId));
		});

		final ResponseEntity<Void> response = webClientFactory.getWebClient(
						String.format("Compute unsuccessful. [electionEventId: %s, verificationCardSetId: %s]",
								electionEventId, verificationCardSetId))
				.put()
				.uri(uriBuilder -> uriBuilder.path(
								"api/v1/configuration/electionevent/{electionEventId}/verificationcardset/{verificationCardSetId}/computegenenclongcodeshares")
						.build(electionEventId, verificationCardSetId))
				.contentType(MediaType.APPLICATION_NDJSON)
				.accept(MediaType.APPLICATION_JSON)
				.body(Flux.fromIterable(setupComponentVerificationDataPayloads), SetupComponentVerificationDataPayload.class)
				.retrieve()
				.toBodilessEntity()
				.retryWhen(retryBackoffSpec)
				.block();

		checkState(checkNotNull(response).getStatusCode().is2xxSuccessful());
	}
}
