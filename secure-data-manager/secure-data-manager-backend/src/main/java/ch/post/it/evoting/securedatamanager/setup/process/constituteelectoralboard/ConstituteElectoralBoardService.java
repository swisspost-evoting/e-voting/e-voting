/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.process.constituteelectoralboard;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static ch.post.it.evoting.securedatamanager.shared.workflow.WorkflowStep.CONSTITUTE_ELECTORAL_BOARD;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import com.google.common.base.Preconditions;

import ch.post.it.evoting.securedatamanager.shared.process.BallotBoxService;
import ch.post.it.evoting.securedatamanager.shared.process.BoardMember;
import ch.post.it.evoting.securedatamanager.shared.process.ElectoralBoard;
import ch.post.it.evoting.securedatamanager.shared.process.ElectoralBoardService;
import ch.post.it.evoting.securedatamanager.shared.process.ResourceNotFoundException;
import ch.post.it.evoting.securedatamanager.shared.workflow.WorkflowStepRunner;
import ch.post.it.evoting.securedatamanager.shared.workflow.WorkflowTask;

@Service
@ConditionalOnProperty("role.isSetup")
public class ConstituteElectoralBoardService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ConstituteElectoralBoardService.class);

	private final BallotBoxService ballotBoxService;
	private final WorkflowStepRunner workflowStepRunner;
	private final ElectoralBoardService electoralBoardService;
	private final ElectoralBoardConfigService electoralBoardConfigService;

	public ConstituteElectoralBoardService(
			final BallotBoxService ballotBoxService,
			final WorkflowStepRunner workflowStepRunner,
			final ElectoralBoardService electoralBoardService,
			final ElectoralBoardConfigService electoralBoardConfigService) {
		this.ballotBoxService = ballotBoxService;
		this.workflowStepRunner = workflowStepRunner;
		this.electoralBoardService = electoralBoardService;
		this.electoralBoardConfigService = electoralBoardConfigService;
	}

	public void constitute(final String electionEventId, final List<char[]> electoralBoardMembersPasswords) {
		validateUUID(electionEventId);
		final List<char[]> passwords = checkNotNull(electoralBoardMembersPasswords).stream()
				.map(Preconditions::checkNotNull)
				.map(char[]::clone)
				.toList();
		// Wipe the passwords after usage.
		electoralBoardMembersPasswords.forEach(pw -> Arrays.fill(pw, '\u0000'));

		checkArgument(passwords.size() >= 2, "There must be at least two passwords.");

		LOGGER.debug("Constituting Electoral Board... [electionEventId: {}]", electionEventId);

		final WorkflowTask workflowTask = new WorkflowTask(
				() -> performConstitute(electionEventId, passwords),
				() -> LOGGER.info("Constitution of the Electoral Board successful. [electionEventId: {}]", electionEventId),
				throwable -> LOGGER.error("Constitution of the Electoral Board failed. [electionEventId: {}]", electionEventId, throwable)
		);

		workflowStepRunner.run(CONSTITUTE_ELECTORAL_BOARD, workflowTask);
	}

	public List<BoardMember> getElectoralBoardMembers() {
		final ElectoralBoard electoralBoard = electoralBoardService.getElectoralBoard();
		return electoralBoard.members();
	}

	private void performConstitute(final String electionEventId, final List<char[]> passwords) {
		final ElectoralBoard electoralBoard = electoralBoardService.getElectoralBoard();

		LOGGER.debug("Constituting the Electoral Board... [electionEventId: {}]", electionEventId);
		final boolean constituted = electoralBoardConfigService.constitute(electionEventId, electoralBoard.id(), passwords);
		if (!constituted) {
			throw new IllegalStateException(
					"Failed to constitute the Electoral Board. [electionEventId: %s, electoralBoardId: %s]".formatted(electionEventId,
							electoralBoard.id()));
		}
		LOGGER.info("Electoral Board successfully constituted. [electionEventId: {}]", electionEventId);

		LOGGER.debug("Signing Electoral Board... [electionEventId: {}]", electionEventId);
		try {
			electoralBoardConfigService.sign(electionEventId, electoralBoard.id());
		} catch (final ResourceNotFoundException e) {
			final String errorMessage = String.format("Failed to sign the Electoral Board. [electionEventId: %s, electoralBoardId: %s]",
					electionEventId, electoralBoard.id());
			throw new IllegalStateException(errorMessage, e);
		}
		LOGGER.info("Electoral Board successfully signed. [electionEventId: {}]", electionEventId);

		LOGGER.debug("Signing the Ballot Boxes... [electionEventId: {}]", electionEventId);
		ballotBoxService.signBallotBoxes(electionEventId);
		LOGGER.debug("Ballot Boxes successfully signed. [electionEventId: {}]", electionEventId);
	}

}
