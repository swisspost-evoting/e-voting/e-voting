/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.tally.protocol.tally.mixoffline;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.security.SignatureException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.domain.InvalidPayloadSignatureException;
import ch.post.it.evoting.evotinglibraries.domain.common.ChannelSecurityContextData;
import ch.post.it.evoting.evotinglibraries.domain.common.EncryptedVerifiableVote;
import ch.post.it.evoting.evotinglibraries.domain.configuration.SetupComponentTallyDataPayload;
import ch.post.it.evoting.evotinglibraries.domain.election.SetupComponentPublicKeys;
import ch.post.it.evoting.evotinglibraries.domain.election.VerificationCardSetContext;
import ch.post.it.evoting.evotinglibraries.domain.signature.Alias;
import ch.post.it.evoting.evotinglibraries.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.tally.mixoffline.VerifyVotingClientProofsAlgorithm;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.tally.mixoffline.VerifyVotingClientProofsContext;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.tally.mixoffline.VerifyVotingClientProofsInput;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventContextPayloadService;
import ch.post.it.evoting.securedatamanager.shared.process.SetupComponentTallyDataPayloadService;
import ch.post.it.evoting.securedatamanager.tally.process.decrypt.IdentifierValidationService;

@Service
@ConditionalOnProperty("role.isTally")
public class VerifyVotingClientProofsService {

	private static final Logger LOGGER = LoggerFactory.getLogger(VerifyVotingClientProofsService.class);

	private final SignatureKeystore<Alias> signatureKeystore;
	private final IdentifierValidationService identifierValidationService;
	private final VerifyVotingClientProofsAlgorithm verifyVotingClientProofsAlgorithm;
	private final ElectionEventContextPayloadService electionEventContextPayloadService;
	private final SetupComponentTallyDataPayloadService setupComponentTallyDataPayloadService;

	public VerifyVotingClientProofsService(
			final SignatureKeystore<Alias> signatureKeystore,
			final IdentifierValidationService identifierValidationService,
			final VerifyVotingClientProofsAlgorithm verifyVotingClientProofsAlgorithm,
			final ElectionEventContextPayloadService electionEventContextPayloadService,
			final SetupComponentTallyDataPayloadService setupComponentTallyDataPayloadService) {
		this.signatureKeystore = signatureKeystore;
		this.identifierValidationService = identifierValidationService;
		this.verifyVotingClientProofsAlgorithm = verifyVotingClientProofsAlgorithm;
		this.electionEventContextPayloadService = electionEventContextPayloadService;
		this.setupComponentTallyDataPayloadService = setupComponentTallyDataPayloadService;
	}

	/**
	 * Invokes the VerifyVotingClientProofs algorithm.
	 *
	 * @param electionEventId            the election event id. Must be non-null and a valid UUID.
	 * @param verificationCardSetContext the verification card set context. Must be non-null.
	 * @param setupComponentPublicKeys   the setup component public keys. Must be non-null.
	 * @param confirmedEncryptedVotes    the confirmed encrypted votes. Must be non-null.
	 */
	public boolean verifyVotingClientProofs(final String electionEventId, final VerificationCardSetContext verificationCardSetContext,
			final SetupComponentPublicKeys setupComponentPublicKeys, final List<EncryptedVerifiableVote> confirmedEncryptedVotes) {
		validateUUID(electionEventId);
		checkNotNull(verificationCardSetContext);
		checkNotNull(setupComponentPublicKeys);
		checkNotNull(confirmedEncryptedVotes);

		final String ballotBoxId = verificationCardSetContext.getBallotBoxId();
		identifierValidationService.validateBallotBoxRelatedIds(electionEventId, ballotBoxId);

		final GqGroup encryptionGroup = electionEventContextPayloadService.loadEncryptionGroup(electionEventId);

		final String verificationCardSetId = verificationCardSetContext.getVerificationCardSetId();
		final SetupComponentTallyDataPayload setupComponentTallyDataPayload = loadSetupComponentTallyDataPayload(electionEventId,
				verificationCardSetId);

		final List<String> verificationCardIds = setupComponentTallyDataPayload.getVerificationCardIds();
		final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> verificationCardPublicKeys = setupComponentTallyDataPayload.getVerificationCardPublicKeys();
		final Map<String, ElGamalMultiRecipientPublicKey> verificationCardPublicKeysMap = IntStream.range(0, verificationCardIds.size()).boxed()
				.collect(Collectors.toMap(verificationCardIds::get, verificationCardPublicKeys::get));

		final VerifyVotingClientProofsContext verifyVotingClientProofsContext = new VerifyVotingClientProofsContext.Builder()
				.setEncryptionGroup(encryptionGroup)
				.setElectionEventId(electionEventId)
				.setVerificationCardSetId(verificationCardSetId)
				.setPrimesMappingTable(verificationCardSetContext.getPrimesMappingTable())
				.setNumberOfEligibleVoters(verificationCardSetContext.getNumberOfVotingCards())
				.setElectionPublicKey(setupComponentPublicKeys.electionPublicKey())
				.setChoiceReturnCodesEncryptionPublicKey(setupComponentPublicKeys.choiceReturnCodesEncryptionPublicKey())
				.build();
		final VerifyVotingClientProofsInput verifyVotingClientProofsInput = new VerifyVotingClientProofsInput(confirmedEncryptedVotes,
				verificationCardPublicKeysMap);

		LOGGER.debug("Performing VerifyVotingClientProofs algorithm... [electionEventId: {}, ballotBoxId: {}]", electionEventId, ballotBoxId);

		return verifyVotingClientProofsAlgorithm.verifyVotingClientProofs(verifyVotingClientProofsContext, verifyVotingClientProofsInput);
	}

	private SetupComponentTallyDataPayload loadSetupComponentTallyDataPayload(final String electionEventId, final String verificationCardSetId) {
		final SetupComponentTallyDataPayload setupComponentTallyDataPayload = setupComponentTallyDataPayloadService.load(electionEventId,
				verificationCardSetId);

		final CryptoPrimitivesSignature signature = setupComponentTallyDataPayload.getSignature();

		checkState(signature != null,
				"The signature of the setup component tally data payload is null. [electionEventId: %s, verificationCardSetId: %s]",
				electionEventId, verificationCardSetId);

		final Hashable additionalContextData = ChannelSecurityContextData.setupComponentTallyData(electionEventId, verificationCardSetId);

		final boolean isSignatureValid;
		try {
			isSignatureValid = signatureKeystore.verifySignature(Alias.SDM_CONFIG, setupComponentTallyDataPayload,
					additionalContextData, setupComponentTallyDataPayload.getSignature().signatureContents());
		} catch (final SignatureException e) {
			throw new IllegalStateException(
					String.format(
							"Could not verify the signature of the setup component tally data payload. [electionEventId: %s, verificationCardSetId: %s]",
							electionEventId, verificationCardSetId));
		}

		if (!isSignatureValid) {
			throw new InvalidPayloadSignatureException(SetupComponentTallyDataPayload.class,
					String.format("[electionEventId: %s, verificationCardSetId: %s]", electionEventId, verificationCardSetId));
		}

		return setupComponentTallyDataPayload;
	}
}
