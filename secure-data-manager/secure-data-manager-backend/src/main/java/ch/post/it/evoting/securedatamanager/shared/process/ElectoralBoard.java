/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.shared.process;

import java.util.List;

public record ElectoralBoard(String id, String alias, List<BoardMember> members) {
}
