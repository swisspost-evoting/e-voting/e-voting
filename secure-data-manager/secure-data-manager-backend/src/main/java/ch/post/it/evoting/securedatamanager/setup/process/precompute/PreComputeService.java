/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.process.precompute;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static ch.post.it.evoting.securedatamanager.shared.workflow.WorkflowStep.PRE_COMPUTE;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.securedatamanager.shared.process.BallotRepository;
import ch.post.it.evoting.securedatamanager.shared.process.ResourceNotFoundException;
import ch.post.it.evoting.securedatamanager.shared.process.Status;
import ch.post.it.evoting.securedatamanager.shared.process.VotingCardSetRepository;
import ch.post.it.evoting.securedatamanager.shared.workflow.PreWorkflowTask;
import ch.post.it.evoting.securedatamanager.shared.workflow.WorkflowStepRunner;
import ch.post.it.evoting.securedatamanager.shared.workflow.WorkflowTask;

@Service
@ConditionalOnProperty("role.isSetup")
public class PreComputeService {

	private static final Logger LOGGER = LoggerFactory.getLogger(PreComputeService.class);

	private final BallotRepository ballotRepository;
	private final WorkflowStepRunner workflowStepRunner;
	private final BallotConfigService ballotConfigService;
	private final VotingCardSetRepository votingCardSetRepository;
	private final FixedBaseOptimizationsService fixedBaseOptimizationsService;
	private final VerificationCardSetPreComputeService verificationCardSetPreComputeService;

	public PreComputeService(
			final BallotRepository ballotRepository,
			final WorkflowStepRunner workflowStepRunner,
			final BallotConfigService ballotConfigService,
			final VotingCardSetRepository votingCardSetRepository,
			final FixedBaseOptimizationsService fixedBaseOptimizationsService,
			final VerificationCardSetPreComputeService verificationCardSetPreComputeService) {
		this.ballotRepository = ballotRepository;
		this.workflowStepRunner = workflowStepRunner;
		this.ballotConfigService = ballotConfigService;
		this.votingCardSetRepository = votingCardSetRepository;
		this.fixedBaseOptimizationsService = fixedBaseOptimizationsService;
		this.verificationCardSetPreComputeService = verificationCardSetPreComputeService;
	}

	/**
	 * Implements the pre-compute process:
	 * <ul>
	 *     <li>update the ballots,</li>
	 *     <li>limit the number of values in the cache,</li>
	 *     <li>pre-compute the voting card sets.</li>
	 * </ul>
	 *
	 * @param electionEventId the election event id. Must be non-null and valid.
	 */
	public void preCompute(final String electionEventId) {
		validateUUID(electionEventId);

		LOGGER.debug("Pre-computing the election event... [electionEventId:{}]", electionEventId);

		// Preparation work done before the actual pre-computation.
		final PreWorkflowTask<Void> preWorkflowTask = new PreWorkflowTask<>(() -> {
			performPreWorkflowTask(electionEventId);
			return null;
		});

		// Voting card sets pre-computation.
		// Retrieve all voting card sets not yet pre-computed.
		final List<String> votingCardSetIds = votingCardSetRepository.findAllVotingCardSetIdsByStatus(electionEventId, Status.LOCKED);

		final List<WorkflowTask> workflowTasks = votingCardSetIds.stream()
				.map(votingCardSetId ->
						new WorkflowTask(
								() -> performPreCompute(electionEventId, votingCardSetId),
								() -> LOGGER.info("Pre-compute of voting card set successful. [votingCardSetId: {}]", votingCardSetId),
								throwable -> LOGGER.error("Pre-compute of voting card set failed. [votingCardSetId: {}]", votingCardSetId, throwable)
						)
				)
				.toList();

		workflowStepRunner.run(PRE_COMPUTE, preWorkflowTask, workflowTasks);
	}

	private void performPreWorkflowTask(final String electionEventId) {
		// Ballots update.
		final List<String> ballotIds = ballotRepository.listIds(electionEventId);
		ballotIds.forEach(ballotId -> {
			try {
				ballotConfigService.updateBallot(electionEventId, ballotId);
			} catch (final ResourceNotFoundException e) {
				final String errorMessage = String.format("Failed to update ballot. [electionEventId: %s, ballotId: %s]", electionEventId, ballotId);
				throw new IllegalStateException(errorMessage, e);
			}
		});

		// Limit the number of small primes and public keys saved in the cache.
		fixedBaseOptimizationsService.prepareFixedBaseOptimizations(electionEventId);
	}

	private void performPreCompute(final String electionEventId, final String votingCardSetId) {
		// Construct the matching verification card set context.
		final String verificationCardSetId = votingCardSetRepository.getVerificationCardSetId(votingCardSetId);
		final String ballotBoxId = votingCardSetRepository.getBallotBoxId(votingCardSetId);
		final VerificationCardSet precomputeContext = new VerificationCardSet(electionEventId, ballotBoxId, votingCardSetId, verificationCardSetId);

		try {
			verificationCardSetPreComputeService.preComputeVerificationCardSet(precomputeContext);
		} catch (final ResourceNotFoundException e) {
			throw new IllegalStateException(
					String.format("Failed to pre-compute voting card set. [electionEventId: %s, votingCardSetId: %s]", electionEventId,
							votingCardSetId), e);
		}
	}

}
