/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.shared.workflow;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;

import java.io.UncheckedIOException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;

@Service
public class WorkflowLogService {
	private final ObjectMapper objectMapper;
	private final WorkflowLogRepository workflowLogRepository;

	private static final Random random = RandomFactory.createRandom();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();

	public WorkflowLogService(
			final ObjectMapper objectMapper,
			final WorkflowLogRepository workflowLogRepository) {
		this.objectMapper = objectMapper;
		this.workflowLogRepository = workflowLogRepository;
	}

	public synchronized void saveLog(final WorkflowStep workflowStep, final WorkflowStatus workflowStatus, final String contextId,
			final WorkflowExceptionCode exceptionCode) {
		final WorkflowLog log = new WorkflowLog(random.genRandomString(ID_LENGTH, base16Alphabet), workflowStep.name(), workflowStatus.name(),
				contextId, exceptionCode.name(), LocalDateTime.now());
		final String logJson;
		try {
			logJson = objectMapper.writeValueAsString(log);
		} catch (final JsonProcessingException e) {
			throw new UncheckedIOException(e);
		}

		workflowLogRepository.save(logJson);
	}

	public synchronized List<WorkflowLog> findLogs(final WorkflowStep workflowStep) {
		return this.findLogList(workflowStep);
	}

	public synchronized List<WorkflowLog> findLogs() {
		return this.findLogList(null);
	}

	private List<WorkflowLog> findLogList(final WorkflowStep workflowStep) {
		final Map<String, Object> criteria = new HashMap<>();
		if (workflowStep != null) {
			criteria.put("workflowStep", workflowStep.toString());
		}
		final String workflowLogDocuments = workflowLogRepository.list(criteria);

		try {
			final JsonNode resultNode = objectMapper.readTree(workflowLogDocuments);
			final JsonNode result = resultNode.path("result");
			return Arrays.asList(objectMapper.treeToValue(result, WorkflowLog[].class));
		} catch (final JsonProcessingException e) {
			throw new UncheckedIOException(e);
		}
	}

}
