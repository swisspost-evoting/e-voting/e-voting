/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.process.precompute;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;

/**
 * Verification card set containing the election event identifier, the ballot box identifier, the voting card set identifier and the verification card
 * set identifier.
 */
public record VerificationCardSet(String electionEventId, String ballotBoxId, String votingCardSetId, String verificationCardSetId) {

	public VerificationCardSet {
		validateUUID(electionEventId);
		validateUUID(ballotBoxId);
		validateUUID(votingCardSetId);
		validateUUID(verificationCardSetId);
	}
}
