/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.process.constituteelectoralboard;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.security.SignatureException;
import java.util.List;
import java.util.stream.IntStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.domain.InvalidPayloadSignatureException;
import ch.post.it.evoting.domain.configuration.SetupComponentVerificationCardKeystoresPayload;
import ch.post.it.evoting.domain.configuration.VerificationCardKeystore;
import ch.post.it.evoting.evotinglibraries.domain.common.ChannelSecurityContextData;
import ch.post.it.evoting.evotinglibraries.domain.configuration.SetupComponentTallyDataPayload;
import ch.post.it.evoting.evotinglibraries.domain.signature.Alias;
import ch.post.it.evoting.evotinglibraries.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;
import ch.post.it.evoting.securedatamanager.setup.protocol.configuration.setupvoting.GenCredDatOutput;
import ch.post.it.evoting.securedatamanager.setup.protocol.configuration.setupvoting.GenCredDatService;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventService;
import ch.post.it.evoting.securedatamanager.shared.process.SetupComponentTallyDataPayloadService;
import ch.post.it.evoting.securedatamanager.shared.process.SetupComponentVerificationCardKeystoresPayloadService;
import ch.post.it.evoting.securedatamanager.shared.process.VotingCardSetRepository;

/**
 * Creates and persists the setup component verification card keystores payload in the SDM file system.
 */
@Service
@ConditionalOnProperty("role.isSetup")
public class SetupComponentVerificationCardKeystoresPayloadGenerationService {

	private static final Logger LOGGER = LoggerFactory.getLogger(SetupComponentVerificationCardKeystoresPayloadGenerationService.class);

	private final GenCredDatService genCredDatService;
	private final ElectionEventService electionEventService;
	private final VotingCardSetRepository votingCardSetRepository;
	private final SignatureKeystore<Alias> signatureKeystoreService;
	private final SetupComponentTallyDataPayloadService setupComponentTallyDataPayloadService;
	private final SetupComponentVerificationCardKeystoresPayloadService setupComponentVerificationCardKeystoresPayloadService;

	public SetupComponentVerificationCardKeystoresPayloadGenerationService(
			final GenCredDatService genCredDatService,
			final ElectionEventService electionEventService,
			final VotingCardSetRepository votingCardSetRepository,
			final SignatureKeystore<Alias> signatureKeystoreService,
			final SetupComponentTallyDataPayloadService setupComponentTallyDataPayloadService,
			final SetupComponentVerificationCardKeystoresPayloadService setupComponentVerificationCardKeystoresPayloadService) {
		this.genCredDatService = genCredDatService;
		this.electionEventService = electionEventService;
		this.votingCardSetRepository = votingCardSetRepository;
		this.signatureKeystoreService = signatureKeystoreService;
		this.setupComponentTallyDataPayloadService = setupComponentTallyDataPayloadService;
		this.setupComponentVerificationCardKeystoresPayloadService = setupComponentVerificationCardKeystoresPayloadService;
	}

	/**
	 * Creates and saves the list of setup component verification card keystores payloads.
	 *
	 * @param electionEventId                      the election event id. Must be non-null ana a valid UUID.
	 * @param choiceReturnCodesEncryptionPublicKey the choice return codes encryption public key. Must be non-null.
	 * @param electionPublicKey                    the election public key. Must be non-null.
	 * @return the list of created setup component verification card keystores payloads. The returned list is unmodifiable.
	 * @throws NullPointerException      if any input is null.
	 * @throws FailedValidationException if the election event id is not a valid UUID.
	 * @throws IllegalArgumentException  if the choice return codes encryption public key and the election public key do not have the same group.
	 */
	public List<SetupComponentVerificationCardKeystoresPayload> generate(final String electionEventId,
			final ElGamalMultiRecipientPublicKey choiceReturnCodesEncryptionPublicKey, final ElGamalMultiRecipientPublicKey electionPublicKey) {
		validateUUID(electionEventId);
		checkNotNull(choiceReturnCodesEncryptionPublicKey);
		checkNotNull(electionPublicKey);
		checkArgument(electionEventService.exists(electionEventId));
		checkArgument(choiceReturnCodesEncryptionPublicKey.getGroup().equals(electionPublicKey.getGroup()),
				"The choice return codes encryption public key and the election public key must have the same group");

		LOGGER.debug("Generating setup component verification card keystores payloads... [electionEventId: {}]", electionEventId);

		final List<String> votingCardSetIds = votingCardSetRepository.findAllVotingCardSetIds(electionEventId);

		final List<SetupComponentVerificationCardKeystoresPayload> setupComponentVerificationCardKeystoresPayloads = votingCardSetIds.stream()
				.parallel()
				.map(votingCardSetId -> {
					final String verificationCardSetId = votingCardSetRepository.getVerificationCardSetId(votingCardSetId);

					final GenCredDatOutput genCredDatOutput = genCredDatService.genCredDat(electionEventId, verificationCardSetId, votingCardSetId,
							electionPublicKey, choiceReturnCodesEncryptionPublicKey);

					LOGGER.info("GenCredDat algorithm successfully performed. [electionEventId: {}, verificationCardSetId: {}]", electionEventId,
							verificationCardSetId);

					final SetupComponentTallyDataPayload setupComponentTallyDataPayload = loadSetupComponentTallyDataPayload(electionEventId,
							verificationCardSetId);

					return createSetupComponentVerificationCardKeystoresPayload(electionEventId, verificationCardSetId,
							setupComponentTallyDataPayload, genCredDatOutput);
				}).toList();

		setupComponentVerificationCardKeystoresPayloads.stream().parallel().forEach(setupComponentVerificationCardKeystoresPayloadService::save);

		LOGGER.info("Generated and saved setup component verification card keystores payloads. [electionEventId: {}]", electionEventId);

		return setupComponentVerificationCardKeystoresPayloads;
	}

	private SetupComponentVerificationCardKeystoresPayload createSetupComponentVerificationCardKeystoresPayload(final String electionEventId,
			final String verificationCardSetId, final SetupComponentTallyDataPayload setupComponentTallyDataPayload,
			final GenCredDatOutput genCredDatOutput) {
		final List<String> verificationCardIds = setupComponentTallyDataPayload.getVerificationCardIds();
		final List<VerificationCardKeystore> verificationCardKeystores = IntStream.range(0, verificationCardIds.size())
				.parallel()
				.mapToObj(i -> new VerificationCardKeystore(verificationCardIds.get(i), genCredDatOutput.verificationCardKeystores().get(i)))
				.toList();

		final SetupComponentVerificationCardKeystoresPayload setupComponentVerificationCardKeystoresPayload = new SetupComponentVerificationCardKeystoresPayload(
				electionEventId, verificationCardSetId, verificationCardKeystores);

		final Hashable hashable = ChannelSecurityContextData.setupComponentVerificationCardKeystores(electionEventId, verificationCardSetId);

		final byte[] signature;
		try {
			signature = signatureKeystoreService.generateSignature(setupComponentVerificationCardKeystoresPayload, hashable);
		} catch (final SignatureException e) {
			throw new IllegalStateException(
					String.format(
							"Could not sign setup component verification card keystores payload. [electionEventId: %s, , verificationCardSetId: %s]",
							electionEventId, verificationCardSetId));
		}

		final CryptoPrimitivesSignature setupComponentVerificationCardKeystoresPayloadSignature = new CryptoPrimitivesSignature(signature);
		setupComponentVerificationCardKeystoresPayload.setSignature(setupComponentVerificationCardKeystoresPayloadSignature);

		return setupComponentVerificationCardKeystoresPayload;
	}

	private SetupComponentTallyDataPayload loadSetupComponentTallyDataPayload(final String electionEventId, final String verificationCardSetId) {
		final SetupComponentTallyDataPayload setupComponentTallyDataPayload = setupComponentTallyDataPayloadService.load(electionEventId,
				verificationCardSetId);

		final CryptoPrimitivesSignature signature = setupComponentTallyDataPayload.getSignature();

		checkState(signature != null,
				"The signature of the setup component tally data payload is null. [electionEventId: %s, verificationCardSetId: %s]",
				electionEventId, verificationCardSetId);

		final Hashable additionalContextData = ChannelSecurityContextData.setupComponentTallyData(electionEventId, verificationCardSetId);

		final boolean isSignatureValid;
		try {
			isSignatureValid = signatureKeystoreService.verifySignature(Alias.SDM_CONFIG, setupComponentTallyDataPayload,
					additionalContextData, setupComponentTallyDataPayload.getSignature().signatureContents());
		} catch (final SignatureException e) {
			throw new IllegalStateException(
					String.format(
							"Could not verify the signature of the setup component tally data payload. [electionEventId: %s, verificationCardSetId: %s]",
							electionEventId, verificationCardSetId));
		}

		if (!isSignatureValid) {
			throw new InvalidPayloadSignatureException(SetupComponentTallyDataPayload.class,
					String.format("[electionEventId: %s, verificationCardSetId: %s]", electionEventId, verificationCardSetId));
		}

		return setupComponentTallyDataPayload;
	}

}
