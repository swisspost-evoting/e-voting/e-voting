/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.process.preconfigure;

public class PreviewSummaryException extends RuntimeException {

	public PreviewSummaryException(final String message) {
		super(message);
	}

}
