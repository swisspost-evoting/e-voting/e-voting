/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.shared.process.summary;

import java.util.List;

public class ElectoralBoardSummary {

	private final String electoralBoardId;
	private final String electoralBoardName;
	private final String electoralBoardDescription;

	private boolean constituted;

	private ElectoralBoardSummary(final String electoralBoardId, final String electoralBoardName, final String electoralBoardDescription, List<String> members) {
		this.electoralBoardId = electoralBoardId;
		this.electoralBoardName = electoralBoardName;
		this.electoralBoardDescription = electoralBoardDescription;
	}

	public String getElectoralBoardId() {
		return electoralBoardId;
	}

	public String getElectoralBoardName() {
		return electoralBoardName;
	}

	public String getElectoralBoardDescription() {
		return electoralBoardDescription;
	}

	public boolean isConstituted() {
		return constituted;
	}

	public void setConstituted(final boolean constituted) {
		this.constituted = constituted;
	}

	public static class Builder {

		private String electoralBoardId;
		private String electoralBoardName;
		private String electoralBoardDescription;
		private List<String> members;

		public Builder electoralBoardId(String electoralBoardId) {
			this.electoralBoardId = electoralBoardId;
			return this;
		}

		public Builder electoralBoardName(String electoralBoardName) {
			this.electoralBoardName = electoralBoardName;
			return this;
		}

		public Builder electoralBoardDescription(String electoralBoardDescription) {
			this.electoralBoardDescription = electoralBoardDescription;
			return this;
		}

		public Builder members(List<String> members) {
			this.members = members;
			return this;
		}

		public ElectoralBoardSummary build() {
			return new ElectoralBoardSummary(electoralBoardId, electoralBoardName, electoralBoardDescription, members);
		}
	}
}
