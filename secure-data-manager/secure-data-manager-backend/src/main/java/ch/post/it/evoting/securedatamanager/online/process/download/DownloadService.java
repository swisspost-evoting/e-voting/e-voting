/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.online.process.download;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static ch.post.it.evoting.securedatamanager.shared.workflow.WorkflowStep.DOWNLOAD;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.securedatamanager.shared.process.Status;
import ch.post.it.evoting.securedatamanager.shared.process.VotingCardSetRepository;
import ch.post.it.evoting.securedatamanager.shared.workflow.WorkflowStepRunner;
import ch.post.it.evoting.securedatamanager.shared.workflow.WorkflowTask;

@Service
public class DownloadService {

	private static final Logger LOGGER = LoggerFactory.getLogger(DownloadService.class);

	private final boolean sequential;
	private final WorkflowStepRunner workflowStepRunner;
	private final VotingCardSetRepository votingCardSetRepository;
	private final VotingCardSetDownloadService votingCardSetDownloadService;

	public DownloadService(
			@Value("${download-genenclongcodeshares.sequential}")
			final boolean sequential,
			final WorkflowStepRunner workflowStepRunner,
			final VotingCardSetRepository votingCardSetRepository,
			final VotingCardSetDownloadService votingCardSetDownloadService) {
		this.sequential = sequential;
		this.workflowStepRunner = workflowStepRunner;
		this.votingCardSetRepository = votingCardSetRepository;
		this.votingCardSetDownloadService = votingCardSetDownloadService;
	}

	public void download(final String electionEventId) {
		validateUUID(electionEventId);

		LOGGER.debug("Downloading voting card sets... [electionEventId: {}]", electionEventId);

		final List<String> votingCardSetIds = votingCardSetRepository.findAllVotingCardSetIdsByStatus(electionEventId, Status.COMPUTED);

		final List<WorkflowTask> workflowTasks = votingCardSetIds.stream()
				.map(votingCardSetId ->
						new WorkflowTask(
								() -> votingCardSetDownloadService.download(votingCardSetId, electionEventId),
								() -> LOGGER.info("Download of voting card set successful. [electionEventId: {}, votingCardSetId: {}]",
										electionEventId, votingCardSetId),
								throwable -> LOGGER.error("Download of voting card set failed. [electionEventId: {}, votingCardSetId: {}]",
										electionEventId, votingCardSetId, throwable)
						)
				)
				.toList();

		if (sequential) {
			workflowStepRunner.runSequential(DOWNLOAD, workflowTasks);
		} else {
			workflowStepRunner.run(DOWNLOAD, workflowTasks);
		}
	}

}
