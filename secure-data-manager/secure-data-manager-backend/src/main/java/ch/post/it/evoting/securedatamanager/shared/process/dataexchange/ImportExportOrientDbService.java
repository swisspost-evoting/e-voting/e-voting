/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.shared.process.dataexchange;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;

import jakarta.json.Json;
import jakarta.json.JsonArray;
import jakarta.json.JsonArrayBuilder;
import jakarta.json.JsonObject;
import jakarta.json.JsonObjectBuilder;
import jakarta.json.JsonValue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.domain.tally.BallotBoxStatus;
import ch.post.it.evoting.securedatamanager.shared.JsonConstants;
import ch.post.it.evoting.securedatamanager.shared.process.BallotBoxRepository;
import ch.post.it.evoting.securedatamanager.shared.process.BallotRepository;
import ch.post.it.evoting.securedatamanager.shared.process.BallotTextRepository;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventContextRepository;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventRepository;
import ch.post.it.evoting.securedatamanager.shared.process.ElectoralBoardRepository;
import ch.post.it.evoting.securedatamanager.shared.process.EntityRepository;
import ch.post.it.evoting.securedatamanager.shared.process.JsonUtils;
import ch.post.it.evoting.securedatamanager.shared.process.ResourceNotFoundException;
import ch.post.it.evoting.securedatamanager.shared.process.Status;
import ch.post.it.evoting.securedatamanager.shared.process.VotingCardSetRepository;
import ch.post.it.evoting.securedatamanager.shared.workflow.WorkflowLogRepository;
import ch.post.it.evoting.securedatamanager.shared.workflow.WorkflowStatus;

@Service
public class ImportExportOrientDbService {

	public static final String SDM = "sdm_";

	private static final Logger LOGGER = LoggerFactory.getLogger(ImportExportOrientDbService.class);

	private final ElectionEventRepository electionEventRepository;
	private final BallotBoxRepository ballotBoxRepository;
	private final BallotRepository ballotRepository;
	private final BallotTextRepository ballotTextRepository;
	private final VotingCardSetRepository votingCardSetRepository;
	private final ElectoralBoardRepository electoralBoardRepository;
	private final ElectionEventContextRepository electionEventContextRepository;

	private final WorkflowLogRepository workflowLogRepository;

	@SuppressWarnings("java:S107")
	public ImportExportOrientDbService(final ElectionEventRepository electionEventRepository,
			final BallotBoxRepository ballotBoxRepository,
			final BallotRepository ballotRepository,
			final BallotTextRepository ballotTextRepository,
			final VotingCardSetRepository votingCardSetRepository,
			final ElectoralBoardRepository electoralBoardRepository,
			final ElectionEventContextRepository electionEventContextRepository,
			final WorkflowLogRepository workflowLogRepository) {
		this.electionEventRepository = electionEventRepository;
		this.ballotBoxRepository = ballotBoxRepository;
		this.ballotRepository = ballotRepository;
		this.ballotTextRepository = ballotTextRepository;
		this.votingCardSetRepository = votingCardSetRepository;
		this.electoralBoardRepository = electoralBoardRepository;
		this.electionEventContextRepository = electionEventContextRepository;
		this.workflowLogRepository = workflowLogRepository;
	}

	public void exportOrientDb(final Path dbDump, final String eeid) throws ResourceNotFoundException, IOException {
		final JsonObjectBuilder dumpJsonBuilder = Json.createObjectBuilder();

		final String electionEvent = electionEventRepository.find(eeid);
		if (electionEvent == null || electionEvent.isEmpty() || JsonConstants.EMPTY_OBJECT.equals(electionEvent)) {
			throw new ResourceNotFoundException("Election Event not found");
		}

		final JsonArray electionEvents = Json.createArrayBuilder().add(JsonUtils.getJsonObject(electionEvent)).build();
		dumpJsonBuilder.add(JsonConstants.ELECTION_EVENTS, electionEvents);

		final String ballots = ballotRepository.listByElectionEvent(eeid);
		final JsonArray ballotsArray = JsonUtils.getJsonObject(ballots).getJsonArray(JsonConstants.RESULT);
		dumpJsonBuilder.add(JsonConstants.BALLOTS, ballotsArray);

		final JsonArrayBuilder ballotBoxTextsArrayBuilder = Json.createArrayBuilder();
		for (final JsonValue ballotValue : ballotsArray) {
			final JsonObject ballotObject = (JsonObject) ballotValue;
			final String id = ballotObject.getString(JsonConstants.ID);
			final String ballotTexts = ballotTextRepository.list(Collections.singletonMap(JsonConstants.BALLOT_DOT_ID, id));
			final JsonArray ballotTextsForBallot = JsonUtils.getJsonObject(ballotTexts).getJsonArray(JsonConstants.RESULT);
			for (final JsonValue ballotText : ballotTextsForBallot) {
				ballotBoxTextsArrayBuilder.add(ballotText);
			}
		}
		dumpJsonBuilder.add(JsonConstants.TEXTS, ballotBoxTextsArrayBuilder.build());

		final String ballotBoxes = ballotBoxRepository.listByElectionEvent(eeid);
		dumpJsonBuilder.add(JsonConstants.BALLOT_BOXES, JsonUtils.getJsonObject(ballotBoxes).getJsonArray(JsonConstants.RESULT));

		final String votingCardSets = votingCardSetRepository.listByElectionEvent(eeid);
		dumpJsonBuilder.add(JsonConstants.VOTING_CARD_SETS, JsonUtils.getJsonObject(votingCardSets).getJsonArray(JsonConstants.RESULT));

		final String electoralBoards = electoralBoardRepository.listByElectionEvent(eeid);
		dumpJsonBuilder.add(JsonConstants.ELECTORAL_BOARDS, JsonUtils.getJsonObject(electoralBoards).getJsonArray(JsonConstants.RESULT));

		final String electionEventContext = electionEventContextRepository.find(eeid);
		if (!(electionEventContext == null || electionEventContext.isEmpty()) && !JsonConstants.EMPTY_OBJECT.equals(electionEventContext)) {
			dumpJsonBuilder.add(JsonConstants.ELECTION_EVENT_CONTEXT, JsonUtils.getJsonObject(electionEventContext));
		}

		// Workflow logs
		final String workflowLogs = workflowLogRepository.list();
		dumpJsonBuilder.add(JsonConstants.WORKFLOW_LOGS, JsonUtils.getJsonObject(workflowLogs).getJsonArray(JsonConstants.RESULT));

		try {
			Files.writeString(dbDump, dumpJsonBuilder.build().toString());

			LOGGER.info("Database export to dump file has been completed successfully: {}", dbDump);
		} catch (final IOException e) {
			LOGGER.error("An error occurred writing DB dump to: {}", dbDump, e);
		}
	}

	public void importOrientDb(final Path dbDump) throws IOException {

		if (Files.notExists(dbDump)) {
			LOGGER.warn("There is no dump database to import");
			return;
		}

		final String dump;
		try {
			dump = Files.readString(dbDump);
		} catch (final IOException e) {
			throw new IOException("Error reading import file ", e);
		}

		final JsonObject dumpJson = JsonUtils.getJsonObject(dump);

		final JsonArray electionEvents = dumpJson.getJsonArray(JsonConstants.ELECTION_EVENTS);
		for (final JsonValue electionEvent : electionEvents) {
			saveOrUpdate(electionEvent, electionEventRepository, Status.class);
		}

		final JsonArray ballots = dumpJson.getJsonArray(JsonConstants.BALLOTS);
		for (final JsonValue ballot : ballots) {
			saveOrUpdate(ballot, ballotRepository, Status.class);
		}

		final JsonArray ballotTexts = dumpJson.getJsonArray(JsonConstants.TEXTS);
		for (final JsonValue ballotText : ballotTexts) {
			saveOrUpdate(ballotText, ballotTextRepository, Status.class);
		}

		final JsonArray ballotBoxes = dumpJson.getJsonArray(JsonConstants.BALLOT_BOXES);
		for (final JsonValue ballotBox : ballotBoxes) {
			saveOrUpdate(ballotBox, ballotBoxRepository, BallotBoxStatus.class);
		}

		final JsonArray votingCardSets = dumpJson.getJsonArray(JsonConstants.VOTING_CARD_SETS);
		for (final JsonValue votingCardSet : votingCardSets) {
			saveOrUpdate(votingCardSet, votingCardSetRepository, Status.class);
		}

		final JsonArray electoralBoards = dumpJson.getJsonArray(JsonConstants.ELECTORAL_BOARDS);
		for (final JsonValue electoralBoard : electoralBoards) {
			saveOrUpdate(electoralBoard, electoralBoardRepository, Status.class);
		}

		final JsonObject electionEventContext = dumpJson.getJsonObject(JsonConstants.ELECTION_EVENT_CONTEXT);
		if (electionEventContext != null) {
			saveOrUpdate(electionEventContext, electionEventContextRepository, Status.class);
		}

		final JsonArray workflowLogs = dumpJson.getJsonArray(JsonConstants.WORKFLOW_LOGS);
		for (final JsonValue workflowLog : workflowLogs) {
			saveOrUpdate(workflowLog, workflowLogRepository, WorkflowStatus.class);
		}
	}

	private static <E extends Enum<E>> void saveOrUpdate(final JsonValue entity, final EntityRepository repository, final Class<E> enumClazz) {
		final JsonObject entityObject = (JsonObject) entity;
		final String id = entityObject.getString(JsonConstants.ID);
		final String foundEntityString = repository.find(id);
		final JsonObject foundEntityObject = JsonUtils.getJsonObject(foundEntityString);

		if (foundEntityObject.isEmpty()) {

			repository.save(entity.toString());
		} else if (!foundEntityObject.containsKey(JsonConstants.STATUS) || !entityObject.containsKey(JsonConstants.STATUS)) {

			repository.update(entity.toString());
		} else {

			try {

				final String entityStatus = entityObject.getString(JsonConstants.STATUS);
				final E entityStatusEnumValue = Enum.valueOf(enumClazz, entityStatus);
				final String foundEntityStatus = foundEntityObject.getString(JsonConstants.STATUS);
				final E foundEntityStatusEnumValue = Enum.valueOf(enumClazz, foundEntityStatus);

				if (foundEntityStatusEnumValue.compareTo(entityStatusEnumValue) < 0) {
					repository.delete(id);
					repository.save(entity.toString());
				} else {
					LOGGER.debug("Entity can't be updated. [id: {}, class: {}]", id, enumClazz.getName());
				}
			} catch (final IllegalArgumentException e) {
				LOGGER.error("Not supported entity status found. You might need a new version of this tool that supports such type.", e);
			}
		}
	}
}
