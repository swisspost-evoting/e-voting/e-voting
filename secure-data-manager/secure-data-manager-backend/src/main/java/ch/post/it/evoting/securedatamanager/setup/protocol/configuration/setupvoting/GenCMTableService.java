/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.protocol.configuration.setupvoting;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPrivateKey;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.evotinglibraries.domain.election.PrimesMappingTable;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.ElectionEventContextPayload;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.preliminaries.votingoptions.PrimesMappingTableAlgorithms;
import ch.post.it.evoting.securedatamanager.setup.process.generate.EncryptedNodeLongReturnCodeSharesChunk;
import ch.post.it.evoting.securedatamanager.setup.process.SetupKeyPairService;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventContextPayloadService;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventService;

@Service
@ConditionalOnProperty("role.isSetup")
public class GenCMTableService {

	private static final Logger LOGGER = LoggerFactory.getLogger(GenCMTableService.class);

	private final SetupKeyPairService setupKeyPairService;
	private final GenCMTableAlgorithm genCMTableAlgorithm;
	private final ElectionEventService electionEventService;
	private final PrimesMappingTableAlgorithms primesMappingTableAlgorithms;
	private final ElectionEventContextPayloadService electionEventContextPayloadService;

	public GenCMTableService(final SetupKeyPairService setupKeyPairService,
			final GenCMTableAlgorithm genCMTableAlgorithm,
			final ElectionEventService electionEventService,
			final PrimesMappingTableAlgorithms primesMappingTableAlgorithms,
			final ElectionEventContextPayloadService electionEventContextPayloadService) {
		this.setupKeyPairService = setupKeyPairService;
		this.genCMTableAlgorithm = genCMTableAlgorithm;
		this.electionEventService = electionEventService;
		this.primesMappingTableAlgorithms = primesMappingTableAlgorithms;
		this.electionEventContextPayloadService = electionEventContextPayloadService;
	}

	/**
	 * Invokes the GenCMTable algorithm.
	 *
	 * @param electionEventId                        the election event id. Must be non-null and a valid UUID.
	 * @param encryptedNodeLongReturnCodeSharesChunk the encrypted node long return code shares chunk. Must be non-null.
	 * @param combineEncLongCodeSharesOutput         the output of the algorithm {@link CombineEncLongCodeSharesAlgorithm}. Must be non-null.
	 */
	public GenCMTableOutput genCMTable(final String electionEventId,
			final EncryptedNodeLongReturnCodeSharesChunk encryptedNodeLongReturnCodeSharesChunk,
			final CombineEncLongCodeSharesOutput combineEncLongCodeSharesOutput) {
		validateUUID(electionEventId);
		checkNotNull(encryptedNodeLongReturnCodeSharesChunk);
		checkNotNull(combineEncLongCodeSharesOutput);
		checkArgument(electionEventId.equals(encryptedNodeLongReturnCodeSharesChunk.getElectionEventId()),
				"The encrypted node long return code shares chunk does not correspond to the expected election event id. [expected: %s, actual: %s]",
				electionEventId, encryptedNodeLongReturnCodeSharesChunk.getElectionEventId());
		checkArgument(electionEventService.exists(electionEventId), "The given election event id does not exist. [electionEventId: %s]",
				electionEventId);

		final ElectionEventContextPayload electionEventContextPayload = electionEventContextPayloadService.load(electionEventId);
		final GqGroup encryptionGroup = electionEventContextPayload.getEncryptionGroup();
		final int maximumNumberOfVotingOptions = electionEventContextPayload.getElectionEventContext().maximumNumberOfVotingOptions();

		final String verificationCardSetId = encryptedNodeLongReturnCodeSharesChunk.getVerificationCardSetId();
		final PrimesMappingTable primesMappingTable = electionEventContextPayloadService.loadPrimesMappingTable(electionEventId,
				verificationCardSetId);
		final List<String> correctnessInformation = primesMappingTableAlgorithms.getCorrectnessInformation(primesMappingTable, List.of());

		final ElGamalMultiRecipientPrivateKey setupSecretKey = setupKeyPairService.load(electionEventId).getPrivateKey();

		final GenCMTableContext genCMTableContext = new GenCMTableContext.Builder()
				.setEncryptionGroup(encryptionGroup)
				.setElectionEventId(electionEventId)
				.setVerificationCardIds(encryptedNodeLongReturnCodeSharesChunk.getVerificationCardIds())
				.setCorrectnessInformation(correctnessInformation)
				.setMaximumNumberOfVotingOptions(maximumNumberOfVotingOptions)
				.build();
		final GenCMTableInput genCMTableInput = new GenCMTableInput(
				setupSecretKey,
				combineEncLongCodeSharesOutput.getEncryptedPreChoiceReturnCodesVector(),
				combineEncLongCodeSharesOutput.getPreVoteCastReturnCodesVector());

		final int chunkId = encryptedNodeLongReturnCodeSharesChunk.getChunkId();
		LOGGER.debug("Performing GenCMTable algorithm... [electionEventId: {}, verificationCardSetId: {}, chunkId: {}]", electionEventId,
				verificationCardSetId, chunkId);

		return genCMTableAlgorithm.genCMTable(genCMTableContext, genCMTableInput);
	}
}
