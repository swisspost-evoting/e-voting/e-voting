/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.shared.process;

public class ResourceNotFoundException extends Exception {

	private static final long serialVersionUID = 1;

	public ResourceNotFoundException(final Throwable cause) {
		super(cause);
	}

	public ResourceNotFoundException(final String message) {
		super(message);
	}

	public ResourceNotFoundException(final String message, final Throwable cause) {
		super(message, cause);
	}

}

