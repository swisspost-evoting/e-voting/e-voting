/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.process.generate;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import com.google.common.collect.MoreCollectors;

import ch.post.it.evoting.evotinglibraries.domain.election.VerificationCardSetContext;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.ElectionEventContextPayload;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;
import ch.post.it.evoting.securedatamanager.setup.protocol.configuration.setupvoting.CombineEncLongCodeSharesOutput;
import ch.post.it.evoting.securedatamanager.setup.protocol.configuration.setupvoting.CombineEncLongCodeSharesService;
import ch.post.it.evoting.securedatamanager.setup.protocol.configuration.setupvoting.GenCMTableOutput;
import ch.post.it.evoting.securedatamanager.setup.protocol.configuration.setupvoting.GenCMTableService;
import ch.post.it.evoting.securedatamanager.shared.process.SetupComponentVerificationDataPayloadFileRepository;
import ch.post.it.evoting.securedatamanager.shared.process.VotingCardSetRepository;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventContextPayloadService;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventService;

@Service
@ConditionalOnProperty("role.isSetup")
public class VotingCardSetDataGenerationService {

	private static final Logger LOGGER = LoggerFactory.getLogger(VotingCardSetDataGenerationService.class);

	private final GenCMTableService genCMTableService;
	private final ElectionEventService electionEventService;
	private final VotingCardSetRepository votingCardSetRepository;
	private final CombineEncLongCodeSharesService combineEncLongCodeSharesService;
	private final NodeContributionsResponsesService nodeContributionsResponsesService;
	private final ElectionEventContextPayloadService electionEventContextPayloadService;
	private final EncryptedNodeLongReturnCodeSharesService encryptedNodeLongReturnCodeSharesService;

	private final SetupComponentVerificationDataPayloadFileRepository setupComponentVerificationDataPayloadFileRepository;

	public VotingCardSetDataGenerationService(final GenCMTableService genCMTableService, final ElectionEventService electionEventService,
			final VotingCardSetRepository votingCardSetRepository, final CombineEncLongCodeSharesService combineEncLongCodeSharesService,
			final NodeContributionsResponsesService nodeContributionsResponsesService,
			final ElectionEventContextPayloadService electionEventContextPayloadService,
			final EncryptedNodeLongReturnCodeSharesService encryptedNodeLongReturnCodeSharesService,
			final SetupComponentVerificationDataPayloadFileRepository setupComponentVerificationDataPayloadFileRepository) {
		this.genCMTableService = genCMTableService;
		this.electionEventService = electionEventService;
		this.votingCardSetRepository = votingCardSetRepository;
		this.combineEncLongCodeSharesService = combineEncLongCodeSharesService;
		this.nodeContributionsResponsesService = nodeContributionsResponsesService;
		this.electionEventContextPayloadService = electionEventContextPayloadService;
		this.encryptedNodeLongReturnCodeSharesService = encryptedNodeLongReturnCodeSharesService;
		this.setupComponentVerificationDataPayloadFileRepository = setupComponentVerificationDataPayloadFileRepository;
	}

	/**
	 * Calls the CombineEncLongCodeShares and GenCMTable algorithms.
	 *
	 * @param electionEventId the election event id. Must be non-null and a valid UUID.
	 * @param votingCardSetId the voting card set id. Must be non-null and a valid UUID.
	 * @return a {@link ReturnCodesGenerationOutput} containing outputs of the CombineEncLongCodeShares and GenCMTable algorithms.
	 * @throws NullPointerException      if {@code electionEventId} or {@code votingCardSetId} is null.
	 * @throws FailedValidationException if {@code electionEventId} or {@code votingCardSetId} is invalid.
	 */
	public ReturnCodesGenerationOutput generate(final String electionEventId, final String votingCardSetId) {
		validateUUID(electionEventId);
		validateUUID(votingCardSetId);
		checkArgument(electionEventService.exists(electionEventId), "The given election event ID does not exist. [electionEventId: %s]",
				electionEventId);
		checkArgument(votingCardSetRepository.exists(electionEventId, votingCardSetId),
				"The given voting card set ID does not belong to the given election event ID. [votingCardSetId: %s, electionEventId: %s]",
				votingCardSetId, electionEventId);

		final String verificationCardSetId = votingCardSetRepository.getVerificationCardSetId(votingCardSetId);

		// Load node contributions chunk paths
		final List<Path> nodeContributionsChunkPaths = nodeContributionsResponsesService.loadAllChunkPaths(electionEventId, verificationCardSetId);
		verifyChunkPaths(electionEventId, verificationCardSetId, nodeContributionsChunkPaths);

		LOGGER.info("Node contributions chunk paths successfully loaded."
						+ " [electionEventId: {}, votingCardSetId: {}, verificationCardSetId: {}, chunks: {}]", electionEventId, votingCardSetId,
				verificationCardSetId, nodeContributionsChunkPaths.size());

		final List<ReturnCodesGenerationOutputChunk> returnCodesGenerationOutputChunks = nodeContributionsChunkPaths.stream().parallel()
				.map(nodeContributionsPath -> {
					// Load node contributions chunk and convert it
					final NodeContributionsResponsesService.NodeContributionsChunk nodeContributionsChunk = nodeContributionsResponsesService.loadNodeContributionsChunk(
							nodeContributionsPath);
					final EncryptedNodeLongReturnCodeSharesChunk encryptedNodeLongReturnCodeSharesChunk = encryptedNodeLongReturnCodeSharesService.convertNodeContributionsChunk(
							electionEventId, verificationCardSetId, nodeContributionsChunk);

					final int chunkId = encryptedNodeLongReturnCodeSharesChunk.getChunkId();

					final CombineEncLongCodeSharesOutput combineEncLongCodeSharesOutput = combineEncLongCodeSharesService.combineEncLongCodeShares(
							electionEventId, verificationCardSetId, encryptedNodeLongReturnCodeSharesChunk);
					LOGGER.info(
							"Encrypted long return code shares successfully combined. [electionEventId: {}, votingCardSetId: {}, verificationCardSetId: {}, chunkId: {}]",
							electionEventId, votingCardSetId, verificationCardSetId, chunkId);

					final GenCMTableOutput genCMTableOutput = genCMTableService.genCMTable(electionEventId, encryptedNodeLongReturnCodeSharesChunk,
							combineEncLongCodeSharesOutput);
					LOGGER.info(
							"Return codes mapping table successfully generated. [electionEventId: {}, votingCardSetId: {}, verificationCardSetId: {}, chunkId: {}]",
							electionEventId, votingCardSetId, verificationCardSetId, chunkId);

					return new ReturnCodesGenerationOutputChunk(encryptedNodeLongReturnCodeSharesChunk.getVerificationCardIds(),
							combineEncLongCodeSharesOutput.getLongVoteCastReturnCodesAllowList(), genCMTableOutput);
				}).toList();

		final ElectionEventContextPayload electionEventContextPayload = electionEventContextPayloadService.load(electionEventId);
		final int numberOfEligibleVoters = electionEventContextPayload.getElectionEventContext().verificationCardSetContexts().stream()
				.filter(verificationCardSetContext -> verificationCardSetContext.getVerificationCardSetId().equals(verificationCardSetId))
				.map(VerificationCardSetContext::getNumberOfVotingCards).collect(MoreCollectors.onlyElement());
		final Set<String> treatedVerificationCardIds = returnCodesGenerationOutputChunks.stream()
				.flatMap(chunk -> chunk.verificationCardIds().stream()).collect(Collectors.toUnmodifiableSet());
		checkState(numberOfEligibleVoters == treatedVerificationCardIds.size());

		LOGGER.info("Return codes generation finished. [electionEventId: {}, votingCardSetId: {}, verificationCardSetId: {}]", electionEventId,
				votingCardSetId, verificationCardSetId);

		final List<String> verificationCardIds = new ArrayList<>();
		final List<String> longVoteCastReturnCodesAllowList = new ArrayList<>();
		final List<String> shortVoteCastReturnCodes = new ArrayList<>();
		final List<List<String>> shortChoiceReturnCodes = new ArrayList<>();
		final SortedMap<String, String> returnCodesMappingTable = new TreeMap<>();
		returnCodesGenerationOutputChunks.forEach(returnCodesGenerationOutputChunk -> {
			verificationCardIds.addAll(returnCodesGenerationOutputChunk.verificationCardIds());
			longVoteCastReturnCodesAllowList.addAll(returnCodesGenerationOutputChunk.longVoteCastReturnCodesAllowList());
			shortChoiceReturnCodes.addAll(returnCodesGenerationOutputChunk.genCMTableOutput().shortChoiceReturnCodes());
			shortVoteCastReturnCodes.addAll(returnCodesGenerationOutputChunk.genCMTableOutput().shortVoteCastReturnCodes());
			returnCodesMappingTable.putAll(returnCodesGenerationOutputChunk.genCMTableOutput().returnCodesMappingTable());
		});

		return new ReturnCodesGenerationOutput.Builder().setElectionEventId(electionEventId).setVerificationCardSetId(verificationCardSetId)
				.setVerificationCardIds(verificationCardIds).setShortVoteCastReturnCodes(shortVoteCastReturnCodes)
				.setLongVoteCastReturnCodesAllowList(longVoteCastReturnCodesAllowList).setReturnCodesMappingTable(returnCodesMappingTable)
				.setShortChoiceReturnCodes(shortChoiceReturnCodes).build();
	}

	/**
	 * Verifies the consistency of the chunks' paths between the SetupComponentVerificationDataPayloads and ControlComponentCodeSharesPayloads.
	 */
	private void verifyChunkPaths(final String electionEventId, final String verificationCardSetId, final List<Path> nodeContributionsChunkPaths) {
		checkState(!nodeContributionsChunkPaths.isEmpty(), "No node contributions responses. [electionEventId: %s, verificationCardSetId: %s]",
				electionEventId, verificationCardSetId);

		final List<Path> verificationDataPayloadChunkPaths = setupComponentVerificationDataPayloadFileRepository.findAllPathsOrderByChunkId(
				electionEventId, verificationCardSetId);
		checkState(nodeContributionsChunkPaths.size() == verificationDataPayloadChunkPaths.size(),
				"The SetupComponentVerificationDataPayloads and ControlComponentCodeSharesPayloads have different chunk counts. "
						+ "[electionEventId: %s, verificationCardSetId: %s, setupComponentChunkCount: %s, , controlComponentChunkCount: %s]",
				electionEventId, verificationCardSetId, verificationDataPayloadChunkPaths.size(), nodeContributionsChunkPaths.size());

		IntStream.range(0, nodeContributionsChunkPaths.size()).forEach(i -> {
			final Path nodeContributionChunkPath = nodeContributionsChunkPaths.get(i);
			final Path verificationDataPayloadChunkPath = verificationDataPayloadChunkPaths.get(i);
			final int nodeContributionsChunkId = nodeContributionsResponsesService.getChunkId(nodeContributionChunkPath);
			final int verificationDataPayloadChunkId = setupComponentVerificationDataPayloadFileRepository.getChunkId(
					verificationDataPayloadChunkPath);
			checkState(i == nodeContributionsChunkId && nodeContributionsChunkId == verificationDataPayloadChunkId,
					"The chunk ID sequence is interrupted or incomplete. [electionEventId: %s, verificationCardSetId: %s]", electionEventId,
					verificationCardSetId);
		});
	}

	record ReturnCodesGenerationOutputChunk(List<String> verificationCardIds, List<String> longVoteCastReturnCodesAllowList,
											GenCMTableOutput genCMTableOutput) {
	}

}
