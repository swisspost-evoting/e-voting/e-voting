/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager;

import java.security.Security;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableRetry
@EnableScheduling
@EnableCaching
@SpringBootApplication
public class SecureDataManagerApplication {

	public static void main(final String[] args) {
		Security.addProvider(new BouncyCastleProvider());

		final ConfigurableApplicationContext applicationContext = new SpringApplicationBuilder(SecureDataManagerApplication.class).run(args);

		applicationContext.registerShutdownHook();
	}

}
