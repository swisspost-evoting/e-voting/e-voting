/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.shared.workflow;

import java.time.LocalDateTime;

public record WorkflowLog(String id, String workflowStep, String status, String contextId, String exceptionCode, LocalDateTime logDate) {
}
