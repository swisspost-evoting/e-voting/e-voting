/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.process.generate;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.securedatamanager.shared.JsonConstants;
import ch.post.it.evoting.securedatamanager.shared.process.BallotBoxRepository;
import ch.post.it.evoting.securedatamanager.shared.process.ConfigurationEntityStatusService;
import ch.post.it.evoting.securedatamanager.shared.process.JsonUtils;
import ch.post.it.evoting.securedatamanager.shared.process.Status;
import ch.post.it.evoting.securedatamanager.shared.process.VotingCardSetRepository;

@Service
@ConditionalOnProperty("role.isSetup")
public class VotingCardSetGenerateBallotService {

	private static final Logger LOGGER = LoggerFactory.getLogger(VotingCardSetGenerateBallotService.class);

	private final BallotBoxRepository ballotBoxRepository;
	private final VotingCardSetRepository votingCardSetRepository;
	private final BallotDataGeneratorService ballotDataGeneratorService;
	private final ConfigurationEntityStatusService configurationEntityStatusService;

	public VotingCardSetGenerateBallotService(
			final BallotBoxRepository ballotBoxRepository,
			final VotingCardSetRepository votingCardSetRepository,
			final BallotDataGeneratorService ballotDataGeneratorService,
			final ConfigurationEntityStatusService configurationEntityStatusService) {
		this.votingCardSetRepository = votingCardSetRepository;
		this.ballotBoxRepository = ballotBoxRepository;
		this.ballotDataGeneratorService = ballotDataGeneratorService;
		this.configurationEntityStatusService = configurationEntityStatusService;
	}

	/**
	 * Generates the needed ballot data.
	 *
	 * @param electionEventId The id of the election event.
	 * @param votingCardSetId The id of the voting card set for which the data is generated.
	 * @return a boolean about the result of the generation.
	 */
	public boolean generateBallotData(final String electionEventId, final String votingCardSetId) {

		LOGGER.info("Generating the ballot box and the ballot. [electionEventId: {}, votingCardSetId: {}]", electionEventId, votingCardSetId);

		validateUUID(electionEventId);
		validateUUID(votingCardSetId);

		// get ballot box from voting card set repository
		final String ballotBoxId = votingCardSetRepository.getBallotBoxId(votingCardSetId);
		if (ballotBoxId == null || ballotBoxId.isEmpty()) {
			return false;
		}

		// get ballot from ballot box repository
		final String ballotId = ballotBoxRepository.getBallotId(ballotBoxId);
		if (ballotId == null || ballotId.isEmpty()) {
			return false;
		}

		// generate ballot data
		if (!ballotDataGeneratorService.generate(ballotId, electionEventId)) {
			return false;
		}

		// set ballot status to ready
		final String ballotBoxAsJson = ballotBoxRepository.find(ballotBoxId);
		final String ballotBoxStatus = JsonUtils.getJsonObject(ballotBoxAsJson).getString(JsonConstants.STATUS);
		if (Status.LOCKED.name().equals(ballotBoxStatus)) {
			configurationEntityStatusService.update(Status.READY.name(), ballotBoxId, ballotBoxRepository);
		}

		LOGGER.info("Ballot generated successfully. [electionEventId: {}, votingCardSetId: {}]", electionEventId, votingCardSetId);
		return true;
	}
}
