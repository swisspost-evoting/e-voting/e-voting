/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.shared.process;

import jakarta.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ch.post.it.evoting.securedatamanager.shared.database.DatabaseManager;

@Repository
public class ElectionEventContextRepository extends AbstractEntityRepository {

	@Autowired
	public ElectionEventContextRepository(final DatabaseManager databaseManager) {
		super(databaseManager);
	}

	@PostConstruct
	@Override
	public void initialize() {
		super.initialize();
	}

	@Override
	protected String entityName() {
		return ElectionEventContextEntity.class.getSimpleName();
	}

}
