/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.protocol.configuration.setuptally;

import static ch.post.it.evoting.evotinglibraries.domain.ControlComponentConstants.NODE_IDS;
import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import com.google.common.base.Preconditions;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.SchnorrProof;
import ch.post.it.evoting.evotinglibraries.domain.election.ControlComponentPublicKeys;
import ch.post.it.evoting.evotinglibraries.domain.election.ElectionEventContext;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.ElectionEventContextPayload;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventContextPayloadService;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventService;

@Service
@ConditionalOnProperty("role.isSetup")
public class SetupTallyEBService {

	private static final Logger LOGGER = LoggerFactory.getLogger(SetupTallyEBService.class);

	private final ElectionEventService electionEventService;
	private final SetupTallyEBAlgorithm setupTallyEBAlgorithm;
	private final ElectionEventContextPayloadService electionEventContextPayloadService;

	public SetupTallyEBService(final ElectionEventService electionEventService,
			final SetupTallyEBAlgorithm setupTallyEBAlgorithm,
			final ElectionEventContextPayloadService electionEventContextPayloadService) {
		this.electionEventService = electionEventService;
		this.setupTallyEBAlgorithm = setupTallyEBAlgorithm;
		this.electionEventContextPayloadService = electionEventContextPayloadService;
	}

	/**
	 * Invokes the SetupTallyEB algorithm.
	 *
	 * @param electionEventId                the election event id. Must be non-null and a valid UUID.
	 * @param electoralBoardMembersPasswords the passwords of the electoral board members. Must be non-null and a valid EBPassword.
	 * @param controlComponentPublicKeys     the control component public keys. Must be non-null.
	 * @throws NullPointerException      if any parameter is null.
	 * @throws FailedValidationException if the election event id is not a valid UUID.
	 * @throws IllegalArgumentException  if
	 *                                   <ul>
	 *                                    <li>no election event exists with the given election event id.</li>
	 *                                    <li>there is a wrong number of control component public keys.</li>
	 *                                   </ul>
	 */
	public SetupTallyEBOutput setupTallyEB(final String electionEventId, final List<char[]> electoralBoardMembersPasswords,
			final List<ControlComponentPublicKeys> controlComponentPublicKeys) {
		validateUUID(electionEventId);
		checkArgument(electionEventService.exists(electionEventId));
		checkNotNull(electoralBoardMembersPasswords);

		electoralBoardMembersPasswords.forEach(Preconditions::checkNotNull);
		final List<char[]> passwords = electoralBoardMembersPasswords.stream().parallel().map(char[]::clone).toList();
		passwords.stream().parallel().forEach(Preconditions::checkNotNull);
		checkArgument(passwords.size() >= 2);

		// Wipe the passwords after usage
		electoralBoardMembersPasswords.stream().parallel().forEach(pw -> Arrays.fill(pw, '\u0000'));

		final List<ControlComponentPublicKeys> controlComponentPublicKeysCopy = checkNotNull(controlComponentPublicKeys).stream()
				.map(Preconditions::checkNotNull)
				.toList();
		checkArgument(controlComponentPublicKeysCopy.size() == NODE_IDS.size(),
				"Wrong number of control component public keys. [expected: %s, actual: %s]", NODE_IDS, controlComponentPublicKeysCopy.size());

		final ElectionEventContextPayload electionEventContextPayload = electionEventContextPayloadService.load(electionEventId);
		final ElectionEventContext electionEventContext = electionEventContextPayload.getElectionEventContext();

		final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> ccmElectionPublicKeys = controlComponentPublicKeysCopy.stream()
				.map(ControlComponentPublicKeys::ccmjElectionPublicKey)
				.collect(GroupVector.toGroupVector());
		final GroupVector<GroupVector<SchnorrProof, ZqGroup>, ZqGroup> ccmSchnorrProofs = controlComponentPublicKeysCopy.stream()
				.map(ControlComponentPublicKeys::ccmjSchnorrProofs)
				.collect(GroupVector.toGroupVector());

		final SetupTallyEBInput setupTallyEBInput = new SetupTallyEBInput(ccmElectionPublicKeys, ccmSchnorrProofs, passwords);

		LOGGER.debug("Performing Setup Tally EB algorithm... [electionEventId: {}]", electionEventId);

		return setupTallyEBAlgorithm.setupTallyEB(electionEventContext, setupTallyEBInput);
	}
}
