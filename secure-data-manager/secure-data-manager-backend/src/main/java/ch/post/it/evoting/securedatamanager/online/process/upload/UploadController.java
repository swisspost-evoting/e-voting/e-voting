/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.online.process.upload;

import static com.google.common.base.Preconditions.checkNotNull;

import java.net.URI;
import java.net.URISyntaxException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventService;
import ch.post.it.evoting.securedatamanager.shared.workflow.WorkflowStep;

/**
 * The configuration upload end-point.
 */
@RestController
@RequestMapping("/sdm-online/upload")
public class UploadController {
	private static final Logger LOGGER = LoggerFactory.getLogger(UploadController.class);

	private final String voterPortalHost;
	private final String voterPortalHostToReplace;
	private final String voterPortalUrlSuffix;
	private final UploadService uploadService;
	private final ElectionEventService electionEventService;

	public UploadController(
			@Value("${voter.portal.host}")
			final String voterPortalHost,
			@Value("${voter.portal.host.to.replace}")
			final String voterPortalHostToReplace,
			@Value("${voter.portal.url.suffix}")
			final String voterPortalUrlSuffix,
			final UploadService uploadService,
			final ElectionEventService electionEventService) {
		this.voterPortalHost = checkNotNull(voterPortalHost);
		this.voterPortalHostToReplace = checkNotNull(voterPortalHostToReplace);
		this.voterPortalUrlSuffix = checkNotNull(voterPortalUrlSuffix);
		this.uploadService = uploadService;
		this.electionEventService = electionEventService;
	}

	@PostMapping()
	public void uploadConfiguration(
			@RequestBody
			final int day
	) {
		final String electionEventId = electionEventService.findElectionEventId();

		final WorkflowStep uploadConfigurationStep = WorkflowStep.getUploadConfigurationStep(day);

		LOGGER.debug("Received request to upload configuration. [electionEventId: {}]", electionEventId);

		uploadService.upload(electionEventId, uploadConfigurationStep);

		LOGGER.info("The upload has been started. [electionEventId: {}]", electionEventId);
	}

	/**
	 * Returns the voter portal URL.
	 */
	@GetMapping(value = "voter-portal-url", produces = "application/json")
	public URI getVoterPortalUrl() throws URISyntaxException {
		final String electionEventId = electionEventService.findElectionEventId();

		final String voterPortalUrlPrefix = voterPortalHost.replace(voterPortalHostToReplace, "");

		return new URI(voterPortalUrlPrefix + voterPortalUrlSuffix + electionEventId);
	}

}
