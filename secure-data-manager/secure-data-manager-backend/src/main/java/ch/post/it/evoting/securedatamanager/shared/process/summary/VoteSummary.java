/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.shared.process.summary;

import java.util.List;
import java.util.Map;

/**
 * The vote summary.
 *
 * @param voteId            the vote id.
 * @param votePosition      the vote position.
 * @param voteDescription   the vote description.
 * @param domainOfInfluence the domain of influence.
 * @param authorizations    the authorizations.
 * @param ballots           the ballots.
 */
public record VoteSummary(String voteId, int votePosition, Map<String, String> voteDescription, String domainOfInfluence, List<AuthorizationSummary> authorizations,
						  List<BallotSummary> ballots) {
}
