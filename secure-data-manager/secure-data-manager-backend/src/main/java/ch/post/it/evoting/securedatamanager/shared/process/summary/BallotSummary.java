/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.shared.process.summary;

import java.util.List;

public record BallotSummary(String ballotId, int ballotPosition, List<Description> ballotDescription, List<QuestionSummary> questions) {

}
