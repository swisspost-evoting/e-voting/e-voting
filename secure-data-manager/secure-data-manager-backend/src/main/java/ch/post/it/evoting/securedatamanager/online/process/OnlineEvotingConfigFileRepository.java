/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.online.process;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Repository;

import ch.post.it.evoting.evotinglibraries.xml.XsdConstants;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.Configuration;
import ch.post.it.evoting.securedatamanager.shared.Constants;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventService;
import ch.post.it.evoting.securedatamanager.shared.process.EvotingConfigFileRepository;
import ch.post.it.evoting.securedatamanager.shared.process.PathResolver;

@Repository
@ConditionalOnProperty(prefix = "role", name = { "isSetup", "isTally" }, havingValue = "false")
public class OnlineEvotingConfigFileRepository extends EvotingConfigFileRepository {

	private static final String EVOTING_CONFIG_XML = Constants.CONFIG_FILE_NAME_CONFIGURATION_ANONYMIZED;
	private static final Logger LOGGER = LoggerFactory.getLogger(OnlineEvotingConfigFileRepository.class);

	private final PathResolver pathResolver;
	private final ElectionEventService electionEventService;

	public OnlineEvotingConfigFileRepository(
			final PathResolver pathResolver,
			final ElectionEventService electionEventService) {
		this.pathResolver = pathResolver;
		this.electionEventService = electionEventService;
	}

	@Override
	public void save(final Configuration configuration) {
		throw new UnsupportedOperationException("This method is not supported for the Online role.");
	}

	/**
	 * Loads the evoting-config and validates it against the related XSD. The evoting-config is located in the {@value EVOTING_CONFIG_XML} file and the
	 * related XSD in {@value XsdConstants#CANTON_CONFIG_XSD}.
	 * <p>
	 * If the contest configuration file or the related XSD does not exist this method returns an empty Optional.
	 * <p>
	 * This method also validates the signature of the loaded file.
	 *
	 * @return the contest configuration as an {@link Optional}.
	 * @throws IllegalStateException if the signature is invalid, or it could not be verified.
	 */
	@Override
	public Optional<Configuration> load() {
		final String electionEventId = electionEventService.findElectionEventId();

		LOGGER.debug("Loading canton config file. [electionEventId: {}]", electionEventId);

		final Path xmlFilePath = pathResolver.resolveConfigurationPath().resolve(EVOTING_CONFIG_XML);

		if (!Files.exists(xmlFilePath)) {
			LOGGER.debug("The requested file does not exist. [electionEventId: {}, xmlFilePath: {}]", electionEventId, xmlFilePath);
			return Optional.empty();
		}

		final Configuration configuration = read(xmlFilePath, XsdConstants.CANTON_CONFIG_XSD, Configuration.class);

		LOGGER.debug("File successfully loaded. [electionEventId: {}, file: {}]", electionEventId, EVOTING_CONFIG_XML);

		return Optional.of(configuration);
	}

}
