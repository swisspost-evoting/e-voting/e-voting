/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.protocol.configuration.setupvoting;

import static ch.post.it.evoting.evotinglibraries.domain.ControlComponentConstants.NODE_IDS;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Objects;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPrivateKey;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupMatrix;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;

/**
 * Regroups the inputs needed by the CombineEncLongCodeShares algorithm.
 *
 * <ul>
 *    <li>sk<sub>setup</sub>, the setup secret key. Non-null.</li>
 *    <li>C<sub>expPCC</sub>, the matrix of exponentiated, encrypted, hashed partial Choice Return Codes. Non-null.</li>
 *    <li>C<sub>expCK</sub>, the matrix of exponentiated, encrypted, hashed Confirmation Keys. Non-null.</li>
 * </ul>
 **/
public class CombineEncLongCodeSharesInput {

	private final ElGamalMultiRecipientPrivateKey setupSecretKey;
	private final GroupMatrix<ElGamalMultiRecipientCiphertext, GqGroup> exponentiatedEncryptedHashedPartialChoiceReturnCodesMatrix;
	private final GroupMatrix<ElGamalMultiRecipientCiphertext, GqGroup> exponentiatedEncryptedHashedConfirmationKeysMatrix;

	private CombineEncLongCodeSharesInput(final ElGamalMultiRecipientPrivateKey setupSecretKey,
			final GroupMatrix<ElGamalMultiRecipientCiphertext, GqGroup> exponentiatedEncryptedHashedPartialChoiceReturnCodesMatrix,
			final GroupMatrix<ElGamalMultiRecipientCiphertext, GqGroup> exponentiatedEncryptedHashedConfirmationKeysMatrix) {
		this.setupSecretKey = setupSecretKey;
		this.exponentiatedEncryptedHashedPartialChoiceReturnCodesMatrix = exponentiatedEncryptedHashedPartialChoiceReturnCodesMatrix;
		this.exponentiatedEncryptedHashedConfirmationKeysMatrix = exponentiatedEncryptedHashedConfirmationKeysMatrix;
	}

	public ElGamalMultiRecipientPrivateKey getSetupSecretKey() {
		return this.setupSecretKey;
	}

	public GroupMatrix<ElGamalMultiRecipientCiphertext, GqGroup> getExponentiatedEncryptedHashedPartialChoiceReturnCodesMatrix() {
		return this.exponentiatedEncryptedHashedPartialChoiceReturnCodesMatrix;
	}

	public GroupMatrix<ElGamalMultiRecipientCiphertext, GqGroup> getExponentiatedEncryptedHashedConfirmationKeysMatrix() {
		return this.exponentiatedEncryptedHashedConfirmationKeysMatrix;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		final CombineEncLongCodeSharesInput that = (CombineEncLongCodeSharesInput) o;
		return setupSecretKey.equals(that.setupSecretKey) && exponentiatedEncryptedHashedPartialChoiceReturnCodesMatrix.equals(
				that.exponentiatedEncryptedHashedPartialChoiceReturnCodesMatrix) && exponentiatedEncryptedHashedConfirmationKeysMatrix.equals(
				that.exponentiatedEncryptedHashedConfirmationKeysMatrix);
	}

	@Override
	public int hashCode() {
		return Objects.hash(setupSecretKey, exponentiatedEncryptedHashedPartialChoiceReturnCodesMatrix,
				exponentiatedEncryptedHashedConfirmationKeysMatrix);
	}

	public static class Builder {
		private ElGamalMultiRecipientPrivateKey setupSecretKey;
		private GroupMatrix<ElGamalMultiRecipientCiphertext, GqGroup> exponentiatedEncryptedHashedPartialChoiceReturnCodesMatrix;
		private GroupMatrix<ElGamalMultiRecipientCiphertext, GqGroup> exponentiatedEncryptedHashedConfirmationKeysMatrix;

		public Builder setSetupSecretKey(final ElGamalMultiRecipientPrivateKey setupSecretKey) {
			this.setupSecretKey = setupSecretKey;
			return this;
		}

		public Builder setExponentiatedEncryptedChoiceReturnCodesMatrix(
				final GroupMatrix<ElGamalMultiRecipientCiphertext, GqGroup> exponentiatedEncryptedChoiceReturnCodesMatrix) {
			this.exponentiatedEncryptedHashedPartialChoiceReturnCodesMatrix = exponentiatedEncryptedChoiceReturnCodesMatrix;
			return this;
		}

		public Builder setExponentiatedEncryptedConfirmationKeysMatrix(
				final GroupMatrix<ElGamalMultiRecipientCiphertext, GqGroup> exponentiatedEncryptedConfirmationKeysMatrix) {
			this.exponentiatedEncryptedHashedConfirmationKeysMatrix = exponentiatedEncryptedConfirmationKeysMatrix;
			return this;
		}

		/**
		 * Creates the CombineEncLongCodeSharesInput. All fields must have been set and be non-null.
		 *
		 * @throws NullPointerException      if any of the fields is null.
		 * @throws IllegalArgumentException  if
		 *                                   <ul>
		 *                                     <li>The matrix of exponentiated, encrypted, hashed partial Choice Return Codes does not have exactly four columns.</li>
		 *                                     <li>The matrix of exponentiated, encrypted, hashed Confirmation Keys does not have exactly four columns.</li>
		 *                                     <li>The matrix of exponentiated, encrypted, hashed partial Choice Return Codes does not have any rows.</li>
		 *                                     <li>The matrix of exponentiated, encrypted, hashed Confirmation Keys does not have any rows.</li>
		 *                                     <li>The exponentiated, encrypted, hashed partial Choice Return Codes matrix and exponentiated, encrypted, hashed Confirmation Keys matrix
		 *                                     do not have the same number of rows.</li>
		 *                                     <li>The Vector of verification card ids does not have the same size as the number of rows of the matrix's</li>
		 *                                     <li>All inputs do not have the same Gq group.</li>
		 *                                   </ul>
		 * @throws FailedValidationException if any verification card id does not comply with the required UUID format.
		 */
		public CombineEncLongCodeSharesInput build() {
			checkNotNull(exponentiatedEncryptedHashedPartialChoiceReturnCodesMatrix);
			checkNotNull(exponentiatedEncryptedHashedConfirmationKeysMatrix);
			checkNotNull(setupSecretKey);

			// Size checks.
			checkArgument(exponentiatedEncryptedHashedPartialChoiceReturnCodesMatrix.numColumns()
							== exponentiatedEncryptedHashedConfirmationKeysMatrix.numColumns(),
					"C_expPCC and C_expCK must have the same number of columns.");
			checkArgument(exponentiatedEncryptedHashedPartialChoiceReturnCodesMatrix.numColumns() == NODE_IDS.size(),
					"The matrix of exponentiated, encrypted, hashed partial Choice Return Codes does not have the expected number of columns. [expected: %s, actual: %s]",
					NODE_IDS.size(), exponentiatedEncryptedHashedPartialChoiceReturnCodesMatrix.numColumns());
			checkArgument(exponentiatedEncryptedHashedConfirmationKeysMatrix.numColumns() == NODE_IDS.size(),
					"The matrix of exponentiated, encrypted, hashed Confirmation Keys does not have the expected number of columns. [expected: %s, actual: %s]",
					NODE_IDS.size(), exponentiatedEncryptedHashedConfirmationKeysMatrix.numColumns());

			checkArgument(exponentiatedEncryptedHashedPartialChoiceReturnCodesMatrix.numRows()
							== exponentiatedEncryptedHashedConfirmationKeysMatrix.numRows(),
					"C_expPCC and C_expCK must have the same number of rows.");
			checkArgument(exponentiatedEncryptedHashedPartialChoiceReturnCodesMatrix.numRows() > 0,
					"The matrix of exponentiated, encrypted, hashed partial Choice Return Codes must have at least one row.");
			checkArgument(exponentiatedEncryptedHashedConfirmationKeysMatrix.numRows() > 0,
					"The matrix of exponentiated, encrypted, hashed Confirmation Keys must have at least one row.");

			final int ciphertextSize = exponentiatedEncryptedHashedPartialChoiceReturnCodesMatrix.get(0, 0).size();
			checkArgument(exponentiatedEncryptedHashedPartialChoiceReturnCodesMatrix.rowStream()
							.allMatch(columns -> columns.stream().allMatch(ciphertext -> ciphertext.size() == ciphertextSize)),
					"All ciphertext in the matrix of exponentiated, encrypted, hashed partial Choice Return Codes must have the same size.");

			checkArgument(exponentiatedEncryptedHashedConfirmationKeysMatrix.rowStream()
							.allMatch(columns -> columns.stream().allMatch(ciphertext -> ciphertext.size() == 1)),
					"All ciphertext in the matrix of exponentiated, encrypted, hashed Confirmation Keys must be of size 1.");

			// Cross group checks.
			checkArgument(exponentiatedEncryptedHashedPartialChoiceReturnCodesMatrix.getGroup()
							.equals(exponentiatedEncryptedHashedConfirmationKeysMatrix.getGroup()),
					"The Matrix of exponentiated, encrypted, hashed partial Choice Return Codes and Confirmation Keys must have the same group.");
			checkArgument(setupSecretKey.getGroup().hasSameOrderAs(exponentiatedEncryptedHashedPartialChoiceReturnCodesMatrix.getGroup()),
					"The setup secret key must have the same order as the group of the Matrix of exponentiated, encrypted, hashed partial Choice Return Codes.");

			return new CombineEncLongCodeSharesInput(setupSecretKey, exponentiatedEncryptedHashedPartialChoiceReturnCodesMatrix,
					exponentiatedEncryptedHashedConfirmationKeysMatrix);
		}
	}
}
