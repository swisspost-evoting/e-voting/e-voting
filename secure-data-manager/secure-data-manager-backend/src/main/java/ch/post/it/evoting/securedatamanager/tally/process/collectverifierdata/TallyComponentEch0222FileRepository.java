/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.tally.process.collectverifierdata;

import static ch.post.it.evoting.evotinglibraries.domain.validations.EncryptionParametersSeedValidation.validateSeed;
import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static ch.post.it.evoting.evotinglibraries.xml.XsdConstants.TALLY_COMPONENT_ECH_0222;
import static ch.post.it.evoting.evotinglibraries.xml.XsdConstants.TALLY_COMPONENT_ECH_0222_VERSION;
import static ch.post.it.evoting.securedatamanager.shared.Constants.TALLY_COMPONENT_ECH_0222_XML;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.SignatureException;
import java.util.List;
import java.util.Optional;

import jakarta.xml.bind.JAXBElement;

import javax.xml.namespace.QName;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Repository;
import org.w3c.dom.Element;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.MoreCollectors;

import ch.ech.xmlns.ech_0155._4.ExtensionType;
import ch.ech.xmlns.ech_0222._1.Delivery;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.evotinglibraries.domain.common.ChannelSecurityContextData;
import ch.post.it.evoting.evotinglibraries.domain.signature.Alias;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;
import ch.post.it.evoting.evotinglibraries.xml.XmlFileRepository;
import ch.post.it.evoting.evotinglibraries.xml.XsdConstants;
import ch.post.it.evoting.evotinglibraries.xml.hashable.HashableEch0222Factory;
import ch.post.it.evoting.securedatamanager.shared.process.PathResolver;

@Repository
@ConditionalOnProperty("role.isTally")
public class TallyComponentEch0222FileRepository extends XmlFileRepository<Delivery> {

	private static final Logger LOGGER = LoggerFactory.getLogger(TallyComponentEch0222FileRepository.class);

	private final PathResolver pathResolver;
	private final SignatureKeystore<Alias> signatureKeystoreService;
	private final String filename;

	public TallyComponentEch0222FileRepository(
			final PathResolver pathResolver,
			final SignatureKeystore<Alias> signatureKeystoreService,
			@Value("${sdm.election.event.seed}")
			final String electionEventSeed) {
		this.pathResolver = pathResolver;
		this.signatureKeystoreService = signatureKeystoreService;
		this.filename = String.format(TALLY_COMPONENT_ECH_0222_XML, TALLY_COMPONENT_ECH_0222_VERSION, validateSeed(electionEventSeed));
	}

	/**
	 * Saves the given delivery in the {@value ch.post.it.evoting.securedatamanager.shared.Constants#TALLY_COMPONENT_ECH_0222_XML} file while
	 * validating it against the related {@value XsdConstants#TALLY_COMPONENT_ECH_0222}.
	 *
	 * @param delivery        the delivery. Must be non-null.
	 * @param electionEventId the election event id. Must be non-null and a valid UUID.
	 * @throws NullPointerException      if any input is null.
	 * @throws FailedValidationException if the election event id is not a valid UUID.
	 */
	public void save(final Delivery delivery, final String electionEventId) {
		checkNotNull(delivery);
		validateUUID(electionEventId);

		LOGGER.debug("Signing tally component eCH-0222... [electionEventId: {}]", electionEventId);

		final byte[] signature = getSignature(delivery, electionEventId);

		try {
			final String signatureWithQuotes = new ObjectMapper().writeValueAsString(signature);
			delivery.getRawDataDelivery().setExtension(new ExtensionType()
					.withAny(List.of(new JAXBElement<>(new QName("signature"), String.class,
							signatureWithQuotes.substring(1, signatureWithQuotes.length() - 1)))));
		} catch (final JsonProcessingException e) {
			throw new UncheckedIOException("Could not serialize signature.", e);
		}

		LOGGER.debug("Tally component eCH-0222 successfully signed. Saving file... [filename: {}, electionEventId: {}]", filename,
				electionEventId);

		final Path xmlFilePath = pathResolver.resolveElectionEventPath(electionEventId).resolve(filename);

		write(delivery, TALLY_COMPONENT_ECH_0222, xmlFilePath);

		LOGGER.debug("File successfully saved. [filename: {}, electionEventId: {}]", filename, electionEventId);
	}

	private byte[] getSignature(final Delivery delivery, final String electionEventId) {
		final Hashable hashable = HashableEch0222Factory.fromDelivery(delivery);
		final Hashable additionalContextData = ChannelSecurityContextData.tallyComponentEch0222();

		try {
			return signatureKeystoreService.generateSignature(hashable, additionalContextData);
		} catch (final SignatureException e) {
			throw new IllegalStateException(String.format("Could not sign tally component eCH-0222. [electionEventId: %s]", electionEventId));
		}
	}

	/**
	 * Loads the tally component eCH-0222 for the given election event id and validates it against the related XSD. The tally component eCH-0222 is
	 * located in the {@value ch.post.it.evoting.securedatamanager.shared.Constants#TALLY_COMPONENT_ECH_0222_XML} file and the related XSD in
	 * {@value XsdConstants#TALLY_COMPONENT_ECH_0222}.
	 * <p>
	 * If the contest configuration file or the related XSD does not exist this method returns an empty Optional.
	 * <p>
	 * This method also validates the signature of the loaded file.
	 *
	 * @param electionEventId the election event id. Must be non-null and a valid UUID.
	 * @return the tally component eCH-0222 as an {@link Optional}.
	 * @throws NullPointerException      if the election event id is null.
	 * @throws IllegalArgumentException  if the contest identification is blank.
	 * @throws FailedValidationException if the election event id is not a valid UUID.
	 * @throws IllegalStateException     if the signature is invalid, or it could not be verified.
	 */
	public Optional<Delivery> load(final String electionEventId) {
		validateUUID(electionEventId);

		LOGGER.debug("Loading file... [filename: {}, electionEventId: {}]", filename, electionEventId);

		final Path xmlFilePath = pathResolver.resolveElectionEventPath(electionEventId).resolve(filename);

		if (!Files.exists(xmlFilePath)) {
			LOGGER.debug("The requested file does not exist. [path: {}, electionEventId: {}]", xmlFilePath, electionEventId);
			return Optional.empty();
		}

		final Delivery delivery = read(xmlFilePath, XsdConstants.TALLY_COMPONENT_ECH_0222, Delivery.class);

		checkState(isSignatureValid(delivery, electionEventId));

		LOGGER.debug("File successfully loaded [path: {}, electionEventId: {}].", filename, electionEventId);

		return Optional.of(delivery);
	}

	private boolean isSignatureValid(final Delivery delivery, final String electionEventId) {

		final ExtensionType extension = delivery.getRawDataDelivery().getExtension();

		checkState(extension != null, "The tally component eCH-0222 file does not contain the expected extension.");

		final Element signatureElement = extension.getAny().stream()
				.map(Element.class::cast)
				.filter(element -> element.getTagName().equals("signature"))
				.collect(MoreCollectors.onlyElement());

		final String signatureContent = signatureElement.getTextContent();

		checkState(signatureContent != null, "The signature of the tally component eCH-0222 file is null.");
		checkState(!signatureContent.isBlank(), "The signature of the tally component eCH-0222 file is blank.");

		final byte[] signature;
		try {
			signature = new ObjectMapper().readValue(String.format("\"%s\"", signatureContent), byte[].class);
		} catch (final JsonProcessingException e) {
			throw new UncheckedIOException("Could not deserialize signature.", e);
		}

		final Hashable hashable = HashableEch0222Factory.fromDelivery(delivery);
		final Hashable additionalContextData = ChannelSecurityContextData.tallyComponentEch0222();
		try {
			return signatureKeystoreService.verifySignature(Alias.SDM_TALLY, hashable, additionalContextData, signature);
		} catch (final SignatureException e) {
			throw new IllegalStateException(
					String.format("Unable to verify tally component eCH-0222 signature. [electionEventId: %s]", electionEventId), e);
		}
	}

}
