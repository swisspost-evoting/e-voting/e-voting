/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.process.generate;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static java.nio.file.Files.createDirectories;
import static java.nio.file.Files.exists;
import static java.nio.file.Files.readAllBytes;
import static java.nio.file.Files.write;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.securedatamanager.shared.Constants;
import ch.post.it.evoting.securedatamanager.shared.JsonConstants;
import ch.post.it.evoting.securedatamanager.shared.process.BallotRepository;
import ch.post.it.evoting.securedatamanager.shared.process.PathResolver;

/**
 * This service accesses a generator of ballot data.
 */
@Service
@ConditionalOnProperty("role.isSetup")
public class BallotDataGeneratorService {

	private static final Logger LOGGER = LoggerFactory.getLogger(BallotDataGeneratorService.class);

	private final PathResolver pathResolver;
	private final BallotRepository ballotRepository;

	@Autowired
	public BallotDataGeneratorService(
			final PathResolver pathResolver,
			final BallotRepository ballotRepository) {
		this.pathResolver = pathResolver;
		this.ballotRepository = ballotRepository;
	}

	/**
	 * This method generates all the data for a ballot.
	 *
	 * @param ballotId        The identifier of the ballot set for which to generate the data.
	 * @param electionEventId The identifier of the election event to whom this ballot set belongs.
	 * @return a boolean about the result of the generation.
	 */
	public boolean generate(final String ballotId, final String electionEventId) {
		validateUUID(ballotId);
		validateUUID(electionEventId);

		// basic validation of input
		if (ballotId.isEmpty()) {
			return false;
		}

		// read the ballot from the database
		final String json = ballotRepository.find(ballotId);

		// simple check if there is a voting card set data returned
		if (JsonConstants.EMPTY_OBJECT.equals(json)) {
			return false;
		}

		final byte[] bytes = json.getBytes(StandardCharsets.UTF_8);

		final Path file = pathResolver.resolveBallotPath(electionEventId, ballotId).resolve(Constants.CONFIG_FILE_NAME_BALLOT_JSON);
		try {
			createDirectories(file.getParent());
			// ballot.json must not be written for each ballot box from the
			// ballot because already working voting card generator can read
			// corrupted content, thus ballot.json must be written only once.
			// Write ballot.json only if the file does not exist or has a
			// different content. The content is pretty small, so it is possible
			// compare everything in memory. Synchronized block protects the
			// operation from a concurrent call, because services in Spring are
			// normally singletons.
			synchronized (this) {
				if (!exists(file) || !Arrays.equals(bytes, readAllBytes(file))) {
					write(file, bytes);
				}
			}
		} catch (final IOException e) {
			LOGGER.error("Failed to write ballot to file.", e);
			return false;
		}
		return true;
	}
}
