/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.shared;

import static ch.post.it.evoting.securedatamanager.shared.Constants.UNSUCCESSFUL_RESPONSE_MESSAGE;
import static com.google.common.base.Preconditions.checkArgument;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.time.Duration;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.KeyManagerFactory;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.http.codec.json.Jackson2JsonDecoder;
import org.springframework.http.codec.json.Jackson2JsonEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.evotinglibraries.domain.ConversionUtils;

import io.netty.channel.ChannelOption;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.handler.timeout.WriteTimeoutHandler;
import reactor.netty.http.client.HttpClient;

@Component
public class WebClientFactory {

	private static final String COMMUNICATION_ERROR_MESSAGE = "Unable to communicate with voting server.";
	private final ObjectMapper objectMapper;
	private final String votingServerUrl;
	private final long responseTimeout;
	private final int maximumMessageSize;
	private final String keystoreLocation;
	private final String keystorePasswordLocation;
	private final int connectionTimeout;
	private final boolean keepAlive;

	public WebClientFactory(
			final ObjectMapper objectMapper,
			@Value("${voting-server.url}")
			final String votingServerUrl,
			@Value("${voting-server.connection.timeout}")
			final int connectionTimeout,
			@Value("${voting-server.response.timeout}")
			final long responseTimeout,
			@Value("${voting-server.connection.max-message-size:1024}")
			final int maximumMessageSize,
			@Value("${voting-server.connection.keep-alive}")
			final boolean keepAlive,
			@Value("${voting-server.connection.client-keystore-location:}")
			final String keystoreLocation,
			@Value("${voting-server.connection.client-keystore-password-location:}")
			final String keystorePasswordLocation) throws IOException {
		this.objectMapper = objectMapper;
		this.connectionTimeout = connectionTimeout;
		this.votingServerUrl = votingServerUrl;
		this.responseTimeout = responseTimeout;
		this.maximumMessageSize = maximumMessageSize;
		this.keepAlive = keepAlive;
		this.keystoreLocation = keystoreLocation;
		this.keystorePasswordLocation = keystorePasswordLocation;

		validateKeystoreLocations(keystoreLocation, keystorePasswordLocation);
	}

	public WebClient getWebClient() {
		return getWebClient(UNSUCCESSFUL_RESPONSE_MESSAGE);
	}

	public WebClient getWebClient(final String unsuccessfulResponseErrorMessage) {
		final ExchangeStrategies strategies = ExchangeStrategies
				.builder()
				.codecs(clientDefaultCodecsConfigurer -> {
					clientDefaultCodecsConfigurer.defaultCodecs()
							.jackson2JsonEncoder(new Jackson2JsonEncoder(objectMapper, MediaType.APPLICATION_JSON, MediaType.APPLICATION_NDJSON));
					clientDefaultCodecsConfigurer.defaultCodecs()
							.jackson2JsonDecoder(new Jackson2JsonDecoder(objectMapper, MediaType.APPLICATION_JSON, MediaType.APPLICATION_NDJSON));
					clientDefaultCodecsConfigurer.defaultCodecs().maxInMemorySize(maximumMessageSize * 1024 * 1024);
				}).build();

		HttpClient httpClient = HttpClient.create()
				.keepAlive(keepAlive)
				.option(ChannelOption.SO_KEEPALIVE, keepAlive)
				.option(ChannelOption.CONNECT_TIMEOUT_MILLIS, connectionTimeout)
				.doOnError((request, error) -> {
					if (error instanceof final IOException ioException) {
						throw new UncheckedIOException(COMMUNICATION_ERROR_MESSAGE, ioException);
					}
					throw new IllegalStateException(unsuccessfulResponseErrorMessage, error);
				}, (response, error) -> {
					if (error instanceof final IOException ioException) {
						throw new UncheckedIOException(COMMUNICATION_ERROR_MESSAGE, ioException);
					}
					throw new IllegalStateException(unsuccessfulResponseErrorMessage, error);
				})
				.doOnConnected(conn -> conn
						.addHandlerFirst(new ReadTimeoutHandler(responseTimeout, TimeUnit.SECONDS))
						.addHandlerFirst(new WriteTimeoutHandler(responseTimeout, TimeUnit.SECONDS))
				)
				.responseTimeout(Duration.ofSeconds(responseTimeout));

		if (!keystoreLocation.isBlank()) {
			httpClient = httpClient.secure(sslContextSpec -> sslContextSpec.sslContext(getSslContext()));
		}

		return WebClient.builder()
				.clientConnector(new ReactorClientHttpConnector(httpClient))
				.exchangeStrategies(strategies)
				.baseUrl(votingServerUrl).build();
	}

	private SslContext getSslContext() {
		try {
			final byte[] fileContent = Files.readAllBytes(Paths.get(keystorePasswordLocation));
			final char[] keystorePassword = ConversionUtils.byteArrayToCharArray(fileContent);
			Arrays.fill(fileContent, (byte) 0);

			final KeyStore keyStore = KeyStore.getInstance(Paths.get(keystoreLocation).toFile(), keystorePassword);

			final KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
			keyManagerFactory.init(keyStore, keystorePassword);

			return SslContextBuilder.forClient()
					.keyManager(keyManagerFactory)
					.build();

		} catch (final KeyStoreException | IOException | NoSuchAlgorithmException | CertificateException | UnrecoverableKeyException e) {
			throw new IllegalArgumentException(e);
		}
	}

	private void validateKeystoreLocations(final String keystoreLocation, final String keystorePasswordLocation) throws IOException {
		if (!keystoreLocation.isBlank()) {
			final Path keystoreLocationPath = Paths.get(keystoreLocation).toRealPath(LinkOption.NOFOLLOW_LINKS);
			checkArgument(Files.isRegularFile(keystoreLocationPath), "The given Two-Way SSL keystore location is not a file. [path: %s]",
					keystoreLocationPath);
			checkArgument(keystoreLocation.endsWith(Constants.P12),
					"The given Two-Way SSL keystore location does not have the correct extension. [path: %s]",
					keystoreLocationPath);
		}
		if (!keystorePasswordLocation.isBlank()) {
			final Path keystorePasswordLocationPath = Paths.get(keystorePasswordLocation).toRealPath(LinkOption.NOFOLLOW_LINKS);
			checkArgument(Files.isRegularFile(keystorePasswordLocationPath),
					"The given Two-Way SSL keystore password location is not a file. [path: %s]",
					keystorePasswordLocationPath);
			checkArgument(keystorePasswordLocation.endsWith(Constants.TXT),
					"The given Two-Way SSL keystore password location does not have the correct extension. [path: %s]",
					keystorePasswordLocationPath);
		}
	}
}
