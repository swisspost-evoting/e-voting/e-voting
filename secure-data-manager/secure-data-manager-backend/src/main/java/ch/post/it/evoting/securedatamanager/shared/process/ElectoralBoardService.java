/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.shared.process;

import java.util.ArrayList;
import java.util.List;

import jakarta.json.JsonArray;

import org.springframework.stereotype.Service;

import ch.post.it.evoting.securedatamanager.shared.JsonConstants;

@Service
public class ElectoralBoardService {

	private final ElectionEventService electionEventService;
	private final ElectoralBoardRepository electoralBoardRepository;

	public ElectoralBoardService(
			final ElectionEventService electionEventService,
			final ElectoralBoardRepository electoralBoardRepository) {
		this.electionEventService = electionEventService;
		this.electoralBoardRepository = electoralBoardRepository;
	}

	public ElectoralBoard getElectoralBoard() {
		final String electionEventId = electionEventService.findElectionEventId();
		final JsonArray electoralBoardResult = JsonUtils.getJsonObject(electoralBoardRepository.listByElectionEvent(electionEventId))
				.getJsonArray(JsonConstants.RESULT);

		final String id = electoralBoardResult.getJsonObject(0).getString(JsonConstants.ID);
		final String alias = electoralBoardResult.getJsonObject(0).getString(JsonConstants.ALIAS);
		final JsonArray boardMembersJsonArray = electoralBoardResult.getJsonObject(0).getJsonArray(JsonConstants.BOARD_MEMBERS);
		final List<BoardMember> boardMembers = new ArrayList<>();
		for (int index = 0; index < boardMembersJsonArray.size(); index++) {
			boardMembers.add(new BoardMember(String.valueOf(index), boardMembersJsonArray.getString(index)));
		}
		return new ElectoralBoard(id, alias, boardMembers);
	}
}
