/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.process.generate;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;

import com.google.common.collect.ImmutableSortedMap;

import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;

/**
 * Regroups the outputs produced by the {@link VotingCardSetDataGenerationService}.
 */
@SuppressWarnings("java:S115")
public class ReturnCodesGenerationOutput {

	private final String electionEventId;
	private final String verificationCardSetId;
	private final List<String> verificationCardIds;
	private final List<String> shortVoteCastReturnCodes;
	private final List<String> longVoteCastReturnCodesAllowList;
	private final ImmutableSortedMap<String, String> returnCodesMappingTable;
	private final List<List<String>> shortChoiceReturnCodes;

	/**
	 * @throws NullPointerException      if any of the fields is null.
	 * @throws FailedValidationException if {@code verificationCardSetId} is invalid.
	 */
	private ReturnCodesGenerationOutput(final String electionEventId, final String verificationCardSetId, final List<String> verificationCardIds,
			final List<String> shortVoteCastReturnCodes, final List<String> longVoteCastReturnCodesAllowList,
			final SortedMap<String, String> returnCodesMappingTable, final List<List<String>> shortChoiceReturnCodes) {

		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);
		checkNotNull(verificationCardIds);
		checkNotNull(shortVoteCastReturnCodes);
		checkNotNull(longVoteCastReturnCodesAllowList);
		checkNotNull(returnCodesMappingTable);
		checkNotNull(shortChoiceReturnCodes);

		this.electionEventId = electionEventId;
		this.verificationCardSetId = verificationCardSetId;
		this.verificationCardIds = List.copyOf(verificationCardIds);
		this.shortVoteCastReturnCodes = List.copyOf(shortVoteCastReturnCodes);
		this.longVoteCastReturnCodesAllowList = List.copyOf(longVoteCastReturnCodesAllowList);
		this.returnCodesMappingTable = ImmutableSortedMap.copyOf(returnCodesMappingTable);
		this.shortChoiceReturnCodes = shortChoiceReturnCodes.stream()
				.map(List::copyOf)
				.toList();
	}

	public String getElectionEventId() {
		return electionEventId;
	}

	public String getVerificationCardSetId() {
		return verificationCardSetId;
	}

	public List<String> getVerificationCardIds() {
		return verificationCardIds;
	}

	public List<String> getShortVoteCastReturnCodes() {
		return shortVoteCastReturnCodes;
	}

	public List<String> getLongVoteCastReturnCodesAllowList() {
		return longVoteCastReturnCodesAllowList;
	}

	public SortedMap<String, String> getReturnCodesMappingTable() {
		return returnCodesMappingTable;
	}

	public List<List<String>> getShortChoiceReturnCodes() {
		return new ArrayList<>(shortChoiceReturnCodes);
	}

	public static class Builder {
		private String electionEventId;
		private String verificationCardSetId;
		private List<String> verificationCardIds;
		private List<String> shortVoteCastReturnCodes;
		private List<String> longVoteCastReturnCodesAllowList;
		private SortedMap<String, String> returnCodesMappingTable;
		private List<List<String>> shortChoiceReturnCodes;

		public Builder setElectionEventId(final String electionEventId) {
			this.electionEventId = electionEventId;
			return this;
		}

		public Builder setVerificationCardSetId(final String verificationCardSetId) {
			this.verificationCardSetId = verificationCardSetId;
			return this;
		}

		public Builder setVerificationCardIds(final List<String> verificationCardIds) {
			this.verificationCardIds = verificationCardIds;
			return this;
		}

		public Builder setShortVoteCastReturnCodes(final List<String> shortVoteCastReturnCodes) {
			this.shortVoteCastReturnCodes = shortVoteCastReturnCodes;
			return this;
		}

		public Builder setLongVoteCastReturnCodesAllowList(final List<String> longVoteCastReturnCodesAllowList) {
			this.longVoteCastReturnCodesAllowList = longVoteCastReturnCodesAllowList;
			return this;
		}

		public Builder setReturnCodesMappingTable(final SortedMap<String, String> returnCodesMappingTable) {
			this.returnCodesMappingTable = returnCodesMappingTable;
			return this;
		}

		public Builder setShortChoiceReturnCodes(final List<List<String>> shortChoiceReturnCodes) {
			this.shortChoiceReturnCodes = shortChoiceReturnCodes;
			return this;
		}

		public ReturnCodesGenerationOutput build() {
			return new ReturnCodesGenerationOutput(electionEventId, verificationCardSetId, verificationCardIds, shortVoteCastReturnCodes,
					longVoteCastReturnCodesAllowList, returnCodesMappingTable, shortChoiceReturnCodes);
		}
	}
}
