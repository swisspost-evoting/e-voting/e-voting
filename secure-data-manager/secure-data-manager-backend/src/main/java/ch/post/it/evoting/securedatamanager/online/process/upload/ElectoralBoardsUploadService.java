/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.online.process.upload;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import jakarta.json.Json;
import jakarta.json.JsonArray;
import jakarta.json.JsonObject;
import jakarta.json.JsonObjectBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.domain.InvalidPayloadSignatureException;
import ch.post.it.evoting.domain.configuration.SetupComponentVerificationCardKeystoresPayload;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.SetupComponentPublicKeysPayload;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;
import ch.post.it.evoting.securedatamanager.shared.JsonConstants;
import ch.post.it.evoting.securedatamanager.shared.database.DatabaseException;
import ch.post.it.evoting.securedatamanager.shared.process.Status;
import ch.post.it.evoting.securedatamanager.shared.process.SynchronizeStatus;
import ch.post.it.evoting.securedatamanager.shared.process.ElectoralBoardRepository;
import ch.post.it.evoting.securedatamanager.shared.process.VotingCardSetRepository;
import ch.post.it.evoting.securedatamanager.shared.process.JsonUtils;
import ch.post.it.evoting.securedatamanager.shared.process.SetupComponentPublicKeysPayloadService;
import ch.post.it.evoting.securedatamanager.shared.process.SetupComponentVerificationCardKeystoresPayloadService;

/**
 * Service which uploads files to voter portal after creating the electoral board
 */
@Service
public class ElectoralBoardsUploadService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ElectoralBoardsUploadService.class);

	private final ElectoralBoardRepository electoralBoardRepository;
	private final ElectoralBoardUploadRepository electoralBoardUploadRepository;
	private final SetupComponentPublicKeysPayloadService setupComponentPublicKeysPayloadService;
	private final VotingCardSetRepository votingCardSetRepository;
	private final SetupComponentVerificationCardKeystoresPayloadService setupComponentVerificationCardKeystoresPayloadService;

	public ElectoralBoardsUploadService(
			final VotingCardSetRepository votingCardSetRepository,
			final ElectoralBoardRepository electoralBoardRepository,
			final ElectoralBoardUploadRepository electoralBoardUploadRepository,
			final SetupComponentPublicKeysPayloadService setupComponentPublicKeysPayloadService,
			final SetupComponentVerificationCardKeystoresPayloadService setupComponentVerificationCardKeystoresPayloadService) {
		this.votingCardSetRepository = votingCardSetRepository;
		this.electoralBoardRepository = electoralBoardRepository;
		this.electoralBoardUploadRepository = electoralBoardUploadRepository;
		this.setupComponentPublicKeysPayloadService = setupComponentPublicKeysPayloadService;
		this.setupComponentVerificationCardKeystoresPayloadService = setupComponentVerificationCardKeystoresPayloadService;
	}

	/**
	 * Uploads the signed authentication context configuration to the voter portal:
	 * <ul>
	 *     <li>The setup component public keys payload.</li>
	 *     <li>The setup component verification card keystores payloads.</li>
	 * </ul>
	 */
	public void uploadSignedAuthenticationContextConfiguration(final String electionEventId) {
		validateUUID(electionEventId);

		final Map<String, Object> params = new HashMap<>();
		params.put(JsonConstants.STATUS, Status.SIGNED.name());
		params.put(JsonConstants.SYNCHRONIZED, SynchronizeStatus.PENDING.getIsSynchronized().toString());
		params.put(JsonConstants.ELECTION_EVENT_DOT_ID, electionEventId);

		final String documents = electoralBoardRepository.list(params);
		final JsonArray electoralBoards = JsonUtils.getJsonObject(documents).getJsonArray(JsonConstants.RESULT);

		for (int i = 0; i < electoralBoards.size(); i++) {

			final JsonObject electoralBoardsInArray = electoralBoards.getJsonObject(i);
			final String electoralBoardId = electoralBoardsInArray.getString(JsonConstants.ID);
			checkArgument(
					electionEventId.equals(electoralBoardsInArray.getJsonObject(JsonConstants.ELECTION_EVENT).getString(JsonConstants.ID)));

			LOGGER.debug("Uploading the signed authentication context configuration. [electionEventId: {}, electoralBoardId: {}]",
					electionEventId, electoralBoardId);

			uploadSetupComponentPublicKeysData(electionEventId);
			uploadSetupComponentVerificationCardKeystoresPayloads(electionEventId);

			final JsonObjectBuilder jsonObjectBuilder = Json.createObjectBuilder();
			jsonObjectBuilder.add(JsonConstants.ID, electoralBoardId);
			jsonObjectBuilder.add(JsonConstants.SYNCHRONIZED, SynchronizeStatus.SYNCHRONIZED.getIsSynchronized().toString());
			jsonObjectBuilder.add(JsonConstants.DETAILS, SynchronizeStatus.SYNCHRONIZED.getStatus());

			try {
				electoralBoardRepository.update(jsonObjectBuilder.build().toString());

				LOGGER.info("The electoral board was uploaded successfully. [electionEventId: {}, electoralBoardId: {}]", electionEventId,
						electoralBoardId);
			} catch (final DatabaseException ex) {
				LOGGER.error(String.format(
						"An error occurred while updating the signed electoral board. [electionEventId: %s, electoralBoardId: %s]",
						electionEventId, electoralBoardId), ex);
			}
		}

	}

	/**
	 * Uploads the setup component public keys payload.
	 *
	 * @param electionEventId the election event id.
	 * @throws IllegalStateException            if an error occurred while verifying the signature of the setup component public keys payload or the
	 *                                          request for uploading the setup component public keys failed.
	 * @throws InvalidPayloadSignatureException if the signature of the setup component public keys payload is invalid.
	 */
	private void uploadSetupComponentPublicKeysData(final String electionEventId) {

		LOGGER.info("Uploading setup component public keys payload... [electionEventId: {}]", electionEventId);

		final SetupComponentPublicKeysPayload setupComponentPublicKeysPayload = setupComponentPublicKeysPayloadService.load(electionEventId);

		electoralBoardUploadRepository.uploadSetupComponentPublicKeysData(setupComponentPublicKeysPayload);
		LOGGER.info("Successfully uploaded setup component public keys payload. [electionEventId: {}]", electionEventId);
	}

	/**
	 * Uploads the setup component verification card keystores payloads.
	 *
	 * @param electionEventId the election event id. Must be non-null and a valid UUID.
	 * @throws FailedValidationException if the election event id is not a valid UUID.
	 * @throws NullPointerException      if the election event id is null.
	 */
	private void uploadSetupComponentVerificationCardKeystoresPayloads(final String electionEventId) {
		validateUUID(electionEventId);

		LOGGER.info("Uploading setup component verification card keystores payloads... [electionEventId: {}]", electionEventId);

		final Map<String, Object> votingCardSetsCriteria = new HashMap<>();
		votingCardSetsCriteria.put(JsonConstants.STATUS, Status.SIGNED.name());
		votingCardSetsCriteria.put(JsonConstants.ELECTION_EVENT_DOT_ID, electionEventId);

		final String votingCardSetsEntities = votingCardSetRepository.list(votingCardSetsCriteria);
		final JsonArray votingCardSets = JsonUtils.getJsonObject(votingCardSetsEntities).getJsonArray(JsonConstants.RESULT);

		final List<SetupComponentVerificationCardKeystoresPayload> setupComponentVerificationCardKeystoresPayloads = IntStream.range(0,
						votingCardSets.size())
				.mapToObj(votingCardSets::getJsonObject)
				.map(votingCardSet -> votingCardSet.getString(JsonConstants.VERIFICATION_CARD_SET_ID))
				.map(verificationCardId -> setupComponentVerificationCardKeystoresPayloadService.load(electionEventId, verificationCardId))
				.toList();

		setupComponentVerificationCardKeystoresPayloadService.upload(setupComponentVerificationCardKeystoresPayloads);

		LOGGER.info("Successfully uploaded setup component verification card keystores payloads. [electionEventId: {}]", electionEventId);
	}

}
