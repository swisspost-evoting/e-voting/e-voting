/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.tally.process.decrypt;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static ch.post.it.evoting.securedatamanager.shared.workflow.WorkflowStatus.COMPLETE;
import static ch.post.it.evoting.securedatamanager.shared.workflow.WorkflowStep.DECRYPT;
import static ch.post.it.evoting.securedatamanager.shared.workflow.WorkflowStep.DECRYPT_BALLOT_BOX;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.stream.IntStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import com.google.common.base.Preconditions;

import ch.post.it.evoting.domain.tally.BallotBoxStatus;
import ch.post.it.evoting.evotinglibraries.domain.election.BallotBox;
import ch.post.it.evoting.evotinglibraries.domain.election.PrimesMappingTable;
import ch.post.it.evoting.evotinglibraries.domain.election.PrimesMappingTableEntry;
import ch.post.it.evoting.evotinglibraries.domain.tally.TallyComponentVotesPayload;
import ch.post.it.evoting.evotinglibraries.domain.validations.Validations;
import ch.post.it.evoting.securedatamanager.shared.process.BallotBoxService;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventContextPayloadService;
import ch.post.it.evoting.securedatamanager.shared.workflow.WorkflowExceptionCode;
import ch.post.it.evoting.securedatamanager.shared.workflow.WorkflowExceptionHandler;
import ch.post.it.evoting.securedatamanager.shared.workflow.WorkflowService;
import ch.post.it.evoting.securedatamanager.tally.process.TallyComponentVotesService;
import ch.post.it.evoting.securedatamanager.tally.process.VerifyElectoralBoardPasswordService;

@Service
@ConditionalOnProperty("role.isTally")
public class DecryptService {

	private static final Logger LOGGER = LoggerFactory.getLogger(DecryptService.class);

	private final ExecutorService executorService;
	private final WorkflowService workflowService;
	private final BallotBoxService ballotBoxService;
	private final MixOfflineFacade mixOfflineFacade;
	private final WorkflowExceptionHandler workflowExceptionHandler;
	private final TallyComponentVotesService tallyComponentVotesService;
	private final ElectionEventContextPayloadService electionEventContextPayloadService;
	private final VerifyElectoralBoardPasswordService verifyElectoralBoardPasswordService;

	public DecryptService(
			final ExecutorService fixedThreadExecutorService,
			final WorkflowService workflowService,
			final BallotBoxService ballotBoxService,
			final MixOfflineFacade mixOfflineFacade,
			final WorkflowExceptionHandler workflowExceptionHandler,
			final TallyComponentVotesService tallyComponentVotesService,
			final ElectionEventContextPayloadService electionEventContextPayloadService,
			final VerifyElectoralBoardPasswordService verifyElectoralBoardPasswordService) {
		this.executorService = fixedThreadExecutorService;
		this.workflowService = workflowService;
		this.ballotBoxService = ballotBoxService;
		this.mixOfflineFacade = mixOfflineFacade;
		this.workflowExceptionHandler = workflowExceptionHandler;
		this.tallyComponentVotesService = tallyComponentVotesService;
		this.electionEventContextPayloadService = electionEventContextPayloadService;
		this.verifyElectoralBoardPasswordService = verifyElectoralBoardPasswordService;
	}

	public void decrypt(final String electionEventId, final List<String> ballotBoxIds, final List<char[]> electoralBoardPasswords) {
		validateUUID(electionEventId);
		final List<String> ballotBoxIdsCopy = checkNotNull(ballotBoxIds).stream().map(Validations::validateUUID).toList();

		final List<char[]> electoralBoardPasswordsCopy = checkNotNull(electoralBoardPasswords).stream()
				.map(Preconditions::checkNotNull)
				.map(char[]::clone)
				.toList();
		electoralBoardPasswords.forEach(pw -> Arrays.fill(pw, '0'));

		checkArgument(electoralBoardPasswordsCopy.size() >= 2, "There must be at least two passwords.");

		validatePasswords(electionEventId, electoralBoardPasswordsCopy.stream().map(char[]::clone).toList())
				.thenRun(() -> performDecrypt(electionEventId, ballotBoxIdsCopy, electoralBoardPasswordsCopy));
	}

	public List<BallotBox> getBallotBoxes(final String electionEventId) {
		validateUUID(electionEventId);
		return ballotBoxService.getBallotBoxes(electionEventId);
	}

	public List<BallotBoxTestVote> getBallotBoxTestVotes(final String electionEventId) {
		validateUUID(electionEventId);
		return getBallotBoxes(electionEventId).stream()
				.filter(ballotBox -> ballotBox.status().equals(BallotBoxStatus.DECRYPTED.name()))
				.filter(BallotBox::test)
				.map(ballotBox -> {
					final TallyComponentVotesPayload tallyComponentVotesPayload = tallyComponentVotesService.load(electionEventId,
							ballotBox.ballot().id(), ballotBox.id());

					final PrimesMappingTable primesMappingTable = electionEventContextPayloadService.loadPrimesMappingTableByBallotBox(
							electionEventId,
							ballotBox.id());

					final List<List<String>> testVotes = tallyComponentVotesPayload.getActualSelectedVotingOptions().stream()
							.map(actualVotingOptionsByVoter -> actualVotingOptionsByVoter.stream()
									.map(primesMappingTable::getPrimesMappingTableEntry)
									.map(PrimesMappingTableEntry::semanticInformation)
									.toList())
							.toList();

					return new BallotBoxTestVote(ballotBox.id(), ballotBox.defaultTitle(), tallyComponentVotesPayload.getDecodedWriteInVotes(),
							testVotes);
				}).toList();
	}

	/**
	 * Validates the electoral board passwords and returns a future that completes normally when all passwords have been validated or exceptionally if
	 * any validation failed.
	 */
	private CompletableFuture<Void> validatePasswords(final String electionEventId, final List<char[]> electoralBoardPasswords) {
		workflowService.notifyInProgress(DECRYPT);
		LOGGER.debug("Validating electoral board passwords... [electionEventId: {}]", electionEventId);

		return CompletableFuture.allOf(IntStream.range(0, electoralBoardPasswords.size())
						.mapToObj(memberIndex -> {
							final Runnable verificationTask = () -> {
								final List<char[]> electoralBoardPasswordsCopy = electoralBoardPasswords.stream().map(char[]::clone).toList();

								checkArgument(verifyElectoralBoardPasswordService.verifyElectoralBoardMemberPassword(
										electionEventId, memberIndex, electoralBoardPasswordsCopy.get(memberIndex)), "The password is invalid.");
							};

							return CompletableFuture.runAsync(verificationTask, executorService)
									.whenComplete((verificationResult, throwable) -> {
										if (throwable != null) {
											LOGGER.error(
													"Verification of electoral board password has failed. [electionEventId: {}, memberIndex: {}]",
													electionEventId, memberIndex, throwable);
										} else {
											LOGGER.debug("Verification of electoral board password successful. [electionEventId: {} memberIndex: {}]",
													electionEventId, memberIndex);
										}
									});
						})
						.toArray(CompletableFuture[]::new))
				.whenComplete((unused, throwable) -> {
					// Wipe the passwords after usage.
					electoralBoardPasswords.forEach(pw -> Arrays.fill(pw, '0'));

					if (throwable != null) {
						final WorkflowExceptionCode exceptionCode = workflowExceptionHandler.handleException(DECRYPT, throwable);
						workflowService.notifyError(DECRYPT, exceptionCode);
						LOGGER.error("Verification of electoral board passwords has failed. [electionEventId: {}]", electionEventId);
					} else {
						LOGGER.info("Verification of electoral board passwords successful. [electionEventId: {}]", electionEventId);
					}
				});
	}

	/**
	 * Decrypts the ballot boxes asynchronously.
	 */
	private void performDecrypt(final String electionEventId, final List<String> ballotBoxIdsCopy, final List<char[]> electoralBoardPasswords) {
		LOGGER.debug("Decrypting ballot boxes... [electionEventId: {}]", electionEventId);

		final List<CompletableFuture<Void>> futures = ballotBoxIdsCopy.stream()
				.map(ballotBoxId -> {
					final Runnable decryptTask = () -> {
						final List<char[]> electoralBoardPasswordsCopy = electoralBoardPasswords.stream().map(char[]::clone).toList();
						workflowService.notifyInProgress(DECRYPT_BALLOT_BOX, ballotBoxId);
						mixOfflineFacade.mixOffline(electionEventId, ballotBoxId, electoralBoardPasswordsCopy);
					};

					return CompletableFuture.runAsync(decryptTask, executorService)
							.whenComplete((unused, throwable) -> {
								if (throwable != null) {
									workflowService.notifyError(DECRYPT_BALLOT_BOX, ballotBoxId, WorkflowExceptionCode.DEFAULT);
									LOGGER.error("Decryption of ballot box failed. [electionEventId: {}, ballotBoxId: {}]", electionEventId,
											ballotBoxId, throwable);
								} else {
									workflowService.notifyComplete(DECRYPT_BALLOT_BOX, ballotBoxId);
									LOGGER.info("Decryption of ballot box successful. [electionEventId: {}, ballotBoxId: {}]", electionEventId,
											ballotBoxId);
								}
							});
				})
				.toList();

		CompletableFuture.allOf(futures.toArray(new CompletableFuture[] {}))
				.whenComplete((unused, throwable) -> {
					// Wipe the passwords after usage.
					electoralBoardPasswords.forEach(pw -> Arrays.fill(pw, '0'));

					if (throwable != null) {
						final WorkflowExceptionCode exceptionCode = workflowExceptionHandler.handleException(DECRYPT, throwable);
						workflowService.notifyError(DECRYPT, exceptionCode);
						LOGGER.error("The workflow step [{}] has failed.", DECRYPT.name(), throwable);
					} else {
						// Check children states to determine if the workflow step is complete.
						final boolean allComplete = this.workflowService.getWorkflowStateList().stream()
								.filter(workflowState -> workflowState.step().equals(DECRYPT_BALLOT_BOX))
								.allMatch(workflowState -> COMPLETE.equals(workflowState.status()));

						if (allComplete) {
							this.workflowService.notifyComplete(DECRYPT);
						} else {
							this.workflowService.notifyReady(DECRYPT);
						}

						LOGGER.info("The workflow step [{}] has been successfully processed.", DECRYPT.name());
					}
				});
	}

}
