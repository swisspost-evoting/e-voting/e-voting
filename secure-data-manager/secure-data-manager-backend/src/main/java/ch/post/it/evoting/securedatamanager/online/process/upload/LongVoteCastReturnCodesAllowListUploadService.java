/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.online.process.upload;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static ch.post.it.evoting.securedatamanager.shared.Constants.VOTER_PORTAL_CONNECTION_NOT_ENABLED_MESSAGE;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.domain.configuration.setupvoting.SetupComponentLVCCAllowListPayload;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;
import ch.post.it.evoting.securedatamanager.shared.WebClientFactory;
import ch.post.it.evoting.securedatamanager.shared.process.SetupComponentLVCCAllowListPayloadService;

import reactor.core.publisher.Mono;
import reactor.util.retry.RetryBackoffSpec;

@Service
public class LongVoteCastReturnCodesAllowListUploadService {

	private static final Logger LOGGER = LoggerFactory.getLogger(LongVoteCastReturnCodesAllowListUploadService.class);

	private final SetupComponentLVCCAllowListPayloadService setupComponentLVCCAllowListPayloadService;
	private final WebClientFactory webClientFactory;
	private final RetryBackoffSpec retryBackoffSpec;
	private final boolean isVoterPortalEnabled;

	public LongVoteCastReturnCodesAllowListUploadService(
			final SetupComponentLVCCAllowListPayloadService setupComponentLVCCAllowListPayloadService,
			final WebClientFactory webClientFactory,
			final RetryBackoffSpec retryBackoffSpec,
			@Value("${voter.portal.enabled}")
			final boolean isVoterPortalEnabled) {
		this.setupComponentLVCCAllowListPayloadService = setupComponentLVCCAllowListPayloadService;
		this.webClientFactory = webClientFactory;
		this.retryBackoffSpec = retryBackoffSpec;
		this.isVoterPortalEnabled = isVoterPortalEnabled;
	}

	/**
	 * Uploads the setup component LVCC allow list payload corresponding to the given election event id and verification card set id to the control
	 * components through the message broker orchestrator. If the election event id is empty, the upload is not done.
	 *
	 * @param electionEventId       the election event id. Must be non-null. If the election event id is not empty, it must be a valid UUID.
	 * @param verificationCardSetId the verification card set id. Must be non-null and a valid UUID.
	 * @throws NullPointerException      if {@code electionEventId} or {@code verificationCardSetId} is null.
	 * @throws FailedValidationException if {@code electionEventId} is not empty and not a valid UUID or if {@code verificationCardSetId} is not a
	 *                                   valid UUID.
	 */
	public void upload(final String electionEventId, final String verificationCardSetId) {
		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);
		checkState(isVoterPortalEnabled, VOTER_PORTAL_CONNECTION_NOT_ENABLED_MESSAGE);

		LOGGER.debug("Uploading setup component LVCC allow list payload... [electionEventId: {}, verificationCardSetId: {}]", electionEventId,
				verificationCardSetId);

		final SetupComponentLVCCAllowListPayload setupComponentLVCCAllowListPayload = setupComponentLVCCAllowListPayloadService.load(electionEventId,
				verificationCardSetId);

		final ResponseEntity<Void> response = webClientFactory.getWebClient(
						String.format("Request for uploading long vote cast return codes allow list failed. [electionEventId: %s, verificationCardSetId: %s]",
								electionEventId, verificationCardSetId))
				.post()
				.uri(uriBuilder -> uriBuilder.path(
								"api/v1/configuration/setupvoting/longvotecastreturncodesallowlist/electionevent/{electionEventId}/verificationcardset/{verificationCardSetId}")
						.build(electionEventId, verificationCardSetId))
				.body(Mono.just(setupComponentLVCCAllowListPayload), SetupComponentLVCCAllowListPayload.class)
				.accept(MediaType.APPLICATION_JSON)
				.retrieve()
				.toBodilessEntity()
				.retryWhen(retryBackoffSpec)
				.block();

		checkState(checkNotNull(response).getStatusCode().is2xxSuccessful());

		LOGGER.info("Successfully uploaded setup component LVCC allow list payload. [electionEventId: {}, verificationCardSetId: {}]",
				electionEventId, verificationCardSetId);
	}
}
