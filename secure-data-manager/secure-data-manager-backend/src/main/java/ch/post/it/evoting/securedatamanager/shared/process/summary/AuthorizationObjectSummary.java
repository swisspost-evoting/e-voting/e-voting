/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.shared.process.summary;

public class AuthorizationObjectSummary {

	private final String domainOfInfluenceId;
	private final String domainOfInfluenceType;
	private final String domainOfInfluenceName;
	private final String countingCircleId;
	private final String countingCircleName;

	private AuthorizationObjectSummary(String domainOfInfluenceId, String domainOfInfluenceType, String domainOfInfluenceName,
			String countingCircleId, String countingCircleName) {
		this.domainOfInfluenceId = domainOfInfluenceId;
		this.domainOfInfluenceType = domainOfInfluenceType;
		this.domainOfInfluenceName = domainOfInfluenceName;
		this.countingCircleId = countingCircleId;
		this.countingCircleName = countingCircleName;
	}

	public String getDomainOfInfluenceId() {
		return domainOfInfluenceId;
	}

	public String getDomainOfInfluenceType() {
		return domainOfInfluenceType;
	}

	public String getDomainOfInfluenceName() {
		return domainOfInfluenceName;
	}

	public String getCountingCircleId() {
		return countingCircleId;
	}

	public String getCountingCircleName() {
		return countingCircleName;
	}

	public static class Builder {

		private String domainOfInfluenceId;
		private String domainOfInfluenceType;
		private String domainOfInfluenceName;
		private String countingCircleId;
		private String countingCircleName;

		public Builder domainOfInfluenceId(String domainOfInfluenceId) {
			this.domainOfInfluenceId = domainOfInfluenceId;
			return this;
		}

		public Builder domainOfInfluenceType(String domainOfInfluenceType) {
			this.domainOfInfluenceType = domainOfInfluenceType;
			return this;
		}

		public Builder domainOfInfluenceName(String domainOfInfluenceName) {
			this.domainOfInfluenceName = domainOfInfluenceName;
			return this;
		}

		public Builder countingCircleId(String countingCircleId) {
			this.countingCircleId = countingCircleId;
			return this;
		}

		public Builder countingCircleName(String countingCircleName) {
			this.countingCircleName = countingCircleName;
			return this;
		}

		public AuthorizationObjectSummary build() {
			return new AuthorizationObjectSummary(domainOfInfluenceId, domainOfInfluenceType, domainOfInfluenceName, countingCircleId, countingCircleName);
		}
	}
}
