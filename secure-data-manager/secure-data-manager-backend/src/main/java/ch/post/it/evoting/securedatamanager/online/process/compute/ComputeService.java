/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.online.process.compute;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static ch.post.it.evoting.securedatamanager.shared.Constants.GET_STATUS_UNSUCCESSFUL_MESSAGE;
import static ch.post.it.evoting.securedatamanager.shared.workflow.WorkflowStep.COMPUTE;
import static com.google.common.base.Preconditions.checkState;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.domain.configuration.setupvoting.ComputingStatus;
import ch.post.it.evoting.securedatamanager.shared.WebClientFactory;
import ch.post.it.evoting.securedatamanager.shared.process.ConfigurationEntityStatusService;
import ch.post.it.evoting.securedatamanager.shared.process.SetupComponentVerificationDataPayloadFileRepository;
import ch.post.it.evoting.securedatamanager.shared.process.Status;
import ch.post.it.evoting.securedatamanager.shared.process.VotingCardSetRepository;
import ch.post.it.evoting.securedatamanager.shared.workflow.WorkflowExceptionCode;
import ch.post.it.evoting.securedatamanager.shared.workflow.WorkflowExceptionHandler;
import ch.post.it.evoting.securedatamanager.shared.workflow.WorkflowService;

@Service
@ConditionalOnProperty(prefix = "role", name = { "isSetup", "isTally" }, havingValue = "false")
public class ComputeService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ComputeService.class);

	private final ExecutorService executorService;
	private final WorkflowService workflowService;
	private final WorkflowExceptionHandler workflowExceptionHandler;
	private final WebClientFactory webClientFactory;
	private final VotingCardSetRepository votingCardSetRepository;
	private final VerificationCardSetComputeService verificationCardSetComputeService;
	private final ConfigurationEntityStatusService configurationEntityStatusService;

	private final SetupComponentVerificationDataPayloadFileRepository setupComponentVerificationDataPayloadFileRepository;

	public ComputeService(
			final ExecutorService fixedThreadExecutorService,
			final WorkflowService workflowService,
			final WorkflowExceptionHandler workflowExceptionHandler,
			final WebClientFactory webClientFactory,
			final VotingCardSetRepository votingCardSetRepository,
			final ConfigurationEntityStatusService configurationEntityStatusService,
			final VerificationCardSetComputeService verificationCardSetComputeService,
			final SetupComponentVerificationDataPayloadFileRepository setupComponentVerificationDataPayloadFileRepository) {
		this.executorService = fixedThreadExecutorService;
		this.workflowService = workflowService;
		this.workflowExceptionHandler = workflowExceptionHandler;
		this.webClientFactory = webClientFactory;
		this.votingCardSetRepository = votingCardSetRepository;
		this.configurationEntityStatusService = configurationEntityStatusService;
		this.verificationCardSetComputeService = verificationCardSetComputeService;
		this.setupComponentVerificationDataPayloadFileRepository = setupComponentVerificationDataPayloadFileRepository;
	}

	public void compute(final String electionEventId) {
		validateUUID(electionEventId);

		LOGGER.info("Computing the voting card sets. [electionEventId: {}]", electionEventId);

		final List<String> votingCardSetIds = votingCardSetRepository.findAllVotingCardSetIds(electionEventId);
		final List<String> precomputedVotingCardSetIds = votingCardSetRepository.findAllVotingCardSetIdsByStatus(electionEventId, Status.PRECOMPUTED);
		final List<String> computingVotingCardSetIds = votingCardSetRepository.findAllVotingCardSetIdsByStatus(electionEventId, Status.COMPUTING);

		// Notify the workflow that the computation is in progress.
		workflowService.notifyInProgress(COMPUTE);

		// Run the computations asynchronously.
		final List<CompletableFuture<Void>> computationFutures = votingCardSetIds.stream()
				// Prepare the pre-computation tasks.
				.map(votingCardSetId -> {
					final Runnable computationTask = () -> {
						final String verificationCardSetId = votingCardSetRepository.getVerificationCardSetId(votingCardSetId);
						if (precomputedVotingCardSetIds.contains(votingCardSetId)) {
							// Start computation.
							verificationCardSetComputeService.computeVerificationCardSet(electionEventId, votingCardSetId, verificationCardSetId);

							// Polling status.
							pollVerificationCardSetStatus(electionEventId, votingCardSetId, verificationCardSetId);
						}
						// If the voting card set is already computing, poll the status.
						if (computingVotingCardSetIds.contains(votingCardSetId)) {
							pollVerificationCardSetStatus(electionEventId, votingCardSetId, verificationCardSetId);
						}
					};

					return CompletableFuture.runAsync(computationTask, executorService)
							.exceptionally(throwable -> {
								LOGGER.error("Computation of voting card set failed. [electionEvenId: {}, votingCardSetId: {}]", electionEventId,
										votingCardSetId, throwable);
								return null;
							});
				})
				.toList();

		CompletableFuture.allOf(computationFutures.toArray(new CompletableFuture[] {}))
				.thenRun(() -> {
					final List<String> computedVotingCardSetIds = votingCardSetRepository.findAllVotingCardSetIdsByStatus(electionEventId,
							Status.COMPUTED);
					if (computedVotingCardSetIds.size() == votingCardSetIds.size()) {
						workflowService.notifyComplete(COMPUTE);
						LOGGER.info("The voting card sets has been successfully computed. [electionEventId: {}]", electionEventId);
					} else {
						workflowService.notifyError(COMPUTE, WorkflowExceptionCode.DEFAULT);
						LOGGER.error("Not all voting card sets were computed. [electionEventId: {}]", electionEventId);
					}
				})
				.exceptionally(throwable -> {
					final WorkflowExceptionCode exceptionCode = workflowExceptionHandler.handleException(COMPUTE, throwable);
					workflowService.notifyError(COMPUTE, exceptionCode);
					LOGGER.error("Some of the computations failed. [electionEventId: {}]", electionEventId);
					return null;
				});
	}

	/**
	 * Check if the control components finished the generation of the encrypted long Return Code Shares and update the status of the voting card set
	 * they belong to. The SDM checks if the number of generated encrypted long Return Code Shares (number of chunks) in the voting server corresponds
	 * to the number of Setup Component verification data (number of chunks) in the SDM.
	 */
	private void pollVerificationCardSetStatus(final String electionEventId, final String votingCardSetId,
			final String verificationCardSetId) {
		final int chunkCount = setupComponentVerificationDataPayloadFileRepository.getCount(electionEventId, verificationCardSetId);

		ComputingStatus computingStatus = ComputingStatus.COMPUTING;
		try (final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1)) {

			while (ComputingStatus.COMPUTING.equals(computingStatus)) {
				try {
					LOGGER.info("Wait before polling compute status. [verificationCardSetId: {}]", verificationCardSetId);
					final ScheduledFuture<?> sleep = scheduler.schedule(() -> {
					}, 10, TimeUnit.SECONDS);
					sleep.get();
				} catch (final InterruptedException | ExecutionException e) {
					Thread.currentThread().interrupt();
					final String errorMessage = String.format(
							"Error while polling verification card set status. [electionEvenId: %s, verificationCardSetId: %s]", electionEventId,
							verificationCardSetId);
					throw new IllegalStateException(errorMessage, e);
				}

				LOGGER.info("Poll compute status. [verificationCardSetId: {}]", verificationCardSetId);
				computingStatus = webClientFactory.getWebClient(
								String.format(GET_STATUS_UNSUCCESSFUL_MESSAGE + "[electionEventId: %s, verificationCardSetId: %s, chunkCount: %s]",
										electionEventId,
										verificationCardSetId, chunkCount))
						.get()
						.uri(uriBuilder -> uriBuilder.path(
										"api/v1/configuration/electionevent/{electionEventId}/verificationcardset/{verificationCardSetId}/chunkcount/{chunkCount}/status")
								.build(electionEventId, verificationCardSetId, chunkCount))
						.accept(MediaType.APPLICATION_JSON)
						.retrieve()
						.bodyToMono(ComputingStatus.class)
						.block();
				LOGGER.info("Compute status. [verificationCardSetId: {}, status: {}]", verificationCardSetId, computingStatus);
			}
		}

		// Update status
		checkState(computingStatus != null);
		configurationEntityStatusService.update(computingStatus.name(), votingCardSetId, votingCardSetRepository);

		if (ComputingStatus.COMPUTING_ERROR.equals(computingStatus)) {
			throw new IllegalStateException(String.format("Computation failed. [electionEventId: %s, verificationCardSetId:%s]", electionEventId,
					verificationCardSetId));
		}
	}

}
