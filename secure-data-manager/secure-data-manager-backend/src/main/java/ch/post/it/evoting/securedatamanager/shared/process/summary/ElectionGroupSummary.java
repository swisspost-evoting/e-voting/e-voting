/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.shared.process.summary;

import java.util.List;

/**
 * Summary of the election group.
 *
 * @param electionGroupId          the election group identifier.
 * @param electionGroupDescription the election group description.
 * @param authorizations           the list of authorizations.
 * @param elections                the list of elections.
 */
public record ElectionGroupSummary(String electionGroupId, List<Description> electionGroupDescription, int electionGroupPosition,
								   List<AuthorizationSummary> authorizations, List<ElectionSummary> elections) {

}
