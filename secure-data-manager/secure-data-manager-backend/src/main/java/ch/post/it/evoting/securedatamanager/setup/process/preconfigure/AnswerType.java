/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.process.preconfigure;

import java.util.List;

import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.AnswerInformationType;

public sealed interface AnswerType permits StandardAnswerTypeAdapter, TiebreakAnswerTypeAdapter {

	boolean isBlankAnswer();

	String getAnswerIdentification();

	List<AnswerInformationType> getAnswerInfo();
}
