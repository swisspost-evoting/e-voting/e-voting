/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.protocol.configuration.setupvoting;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;
import java.util.Objects;

import com.google.common.base.Preconditions;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;

/**
 * Regroups the output values returned by the CombineEncLongCodeShares algorithm.
 *
 * <ul>
 *     <li>c<sub>pC</sub>, The vector of encrypted pre-Choice Return Codes Non-null.</li>
 *     <li>p<sub>VCC</sub>, The vector of pre-Vote Cast Return Codes. Non-null</li>
 *     <li>L<sub>lVCC</sub>, The long Vote Cast Return Codes allow list. Non-null</li>
 * </ul>
 */
public class CombineEncLongCodeSharesOutput {

	private final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> encryptedPreChoiceReturnCodesVector;
	private final GroupVector<GqElement, GqGroup> preVoteCastReturnCodesVector;
	private final List<String> longVoteCastReturnCodesAllowList;

	private CombineEncLongCodeSharesOutput(final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> encryptedPreChoiceReturnCodesVector,
			final GroupVector<GqElement, GqGroup> preVoteCastReturnCodesVector,
			final List<String> longVoteCastReturnCodesAllowList) {

		this.encryptedPreChoiceReturnCodesVector = encryptedPreChoiceReturnCodesVector;
		this.preVoteCastReturnCodesVector = preVoteCastReturnCodesVector;
		this.longVoteCastReturnCodesAllowList = longVoteCastReturnCodesAllowList;
	}

	public GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> getEncryptedPreChoiceReturnCodesVector() {
		return encryptedPreChoiceReturnCodesVector;
	}

	public GroupVector<GqElement, GqGroup> getPreVoteCastReturnCodesVector() {
		return preVoteCastReturnCodesVector;
	}

	public List<String> getLongVoteCastReturnCodesAllowList() {
		return List.copyOf(longVoteCastReturnCodesAllowList);
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		final CombineEncLongCodeSharesOutput that = (CombineEncLongCodeSharesOutput) o;
		return encryptedPreChoiceReturnCodesVector.equals(that.encryptedPreChoiceReturnCodesVector) && preVoteCastReturnCodesVector.equals(
				that.preVoteCastReturnCodesVector) && longVoteCastReturnCodesAllowList.equals(that.longVoteCastReturnCodesAllowList);
	}

	@Override
	public int hashCode() {
		return Objects.hash(encryptedPreChoiceReturnCodesVector, preVoteCastReturnCodesVector, longVoteCastReturnCodesAllowList);
	}

	public static class Builder {
		private GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> encryptedPreChoiceReturnCodesVector;
		private GroupVector<GqElement, GqGroup> preVoteCastReturnCodesVector;
		private List<String> longVoteCastReturnCodesAllowList;

		public Builder setEncryptedPreChoiceReturnCodesVector(
				final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> encryptedPreChoiceReturnCodesVector) {
			this.encryptedPreChoiceReturnCodesVector = encryptedPreChoiceReturnCodesVector;
			return this;
		}

		public Builder setPreVoteCastReturnCodesVector(final GroupVector<GqElement, GqGroup> preVoteCastReturnCodesVector) {
			this.preVoteCastReturnCodesVector = preVoteCastReturnCodesVector;
			return this;
		}

		public Builder setLongVoteCastReturnCodesAllowList(final List<String> longVoteCastReturnCodesAllowList) {
			this.longVoteCastReturnCodesAllowList = longVoteCastReturnCodesAllowList;
			return this;
		}

		/**
		 * Creates the CombineEncLongCodeSharesOutput. All fields must have been set and be non-null.
		 *
		 * @return a new CombineEncLongCodeSharesOutput.
		 * @throws NullPointerException     if any of the fields is null.
		 * @throws IllegalArgumentException if
		 *                                  <ul>
		 *                                      <li>All lists/vectors do not have the exactly same size.</li>
		 *                                      <li>The vector of exponentiated, encrypted, hashed partial Choice Return Codes and the
		 *                                      vector of exponentiated, encrypted, hashed Confirmation Keys do not have the same group order.</li>
		 *                                  </ul>
		 */
		public CombineEncLongCodeSharesOutput build() {
			checkNotNull(encryptedPreChoiceReturnCodesVector);
			checkNotNull(preVoteCastReturnCodesVector);
			final List<String> longVoteCastReturnCodesAllowListCopy = checkNotNull(longVoteCastReturnCodesAllowList).stream()
					.map(Preconditions::checkNotNull)
					.toList();

			// Size checks.
			checkArgument(!encryptedPreChoiceReturnCodesVector.isEmpty(),
					"The vector of encrypted pre-Choice Return Codes must have more than zero elements.");

			final int N_E = encryptedPreChoiceReturnCodesVector.size();

			checkArgument(preVoteCastReturnCodesVector.size() == N_E,
					"The vector of pre-Vote Cast Return Codes is of incorrect size [size: expected: %s, actual: %s].",
					N_E, preVoteCastReturnCodesVector.size());

			checkArgument(longVoteCastReturnCodesAllowListCopy.size() == N_E,
					"The long Vote Cast Return Codes allow list is of incorrect size [size: expected: %s, actual: %s].",
					N_E, longVoteCastReturnCodesAllowListCopy.size());

			// Cross group checks.
			checkArgument(preVoteCastReturnCodesVector.getGroup().equals(encryptedPreChoiceReturnCodesVector.getGroup()),
					"The vector of encrypted pre-Choice Return Codes and the vector of pre-Vote Cast Return Codes do not have the same group order.");

			return new CombineEncLongCodeSharesOutput(encryptedPreChoiceReturnCodesVector, preVoteCastReturnCodesVector,
					longVoteCastReturnCodesAllowListCopy);
		}
	}
}
