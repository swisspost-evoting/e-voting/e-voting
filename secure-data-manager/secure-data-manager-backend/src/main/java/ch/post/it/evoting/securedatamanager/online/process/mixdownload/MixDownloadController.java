/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.online.process.mixdownload;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.post.it.evoting.evotinglibraries.domain.election.BallotBox;
import ch.post.it.evoting.securedatamanager.shared.process.BallotBoxService;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventService;

@RestController
@RequestMapping("/sdm-online/mix-download")
public class MixDownloadController {
	private static final Logger LOGGER = LoggerFactory.getLogger(MixDownloadController.class);

	private final MixDownloadService mixDownloadService;

	private final BallotBoxService ballotBoxService;
	private final ElectionEventService electionEventService;

	public MixDownloadController(
			final MixDownloadService mixDownloadService,
			final BallotBoxService ballotBoxService,
			final ElectionEventService electionEventService) {
		this.mixDownloadService = mixDownloadService;
		this.ballotBoxService = ballotBoxService;
		this.electionEventService = electionEventService;
	}

	@PostMapping()
	public void mixAndDownload(
			@RequestBody
			final List<String> ballotBoxIds) {
		final String electionEventId = electionEventService.findElectionEventId();

		LOGGER.debug("Starting mix & download. [electionEventId: {}]", electionEventId);

		mixDownloadService.mixAndDownload(electionEventId, ballotBoxIds);

		LOGGER.info("The mix & download has been started. [electionEventId: {}]", electionEventId);
	}

	/**
	 * Returns the list of all ballot boxes.
	 */
	@GetMapping(value = "ballot-boxes", produces = "application/json")
	public List<BallotBox> getBallotBoxes() {
		final String electionEventId = electionEventService.findElectionEventId();

		return ballotBoxService.getBallotBoxes(electionEventId);
	}

}
