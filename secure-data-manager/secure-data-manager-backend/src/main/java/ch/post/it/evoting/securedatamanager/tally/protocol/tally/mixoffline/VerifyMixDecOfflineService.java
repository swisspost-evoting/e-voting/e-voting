/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.tally.protocol.tally.mixoffline;

import static ch.post.it.evoting.evotinglibraries.domain.ControlComponentConstants.NODE_IDS;
import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.util.HashSet;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import com.google.common.base.Preconditions;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.mixnet.VerifiableShuffle;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.VerifiableDecryptions;
import ch.post.it.evoting.evotinglibraries.domain.election.ControlComponentPublicKeys;
import ch.post.it.evoting.evotinglibraries.domain.election.SetupComponentPublicKeys;
import ch.post.it.evoting.evotinglibraries.domain.election.VerificationCardSetContext;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.ControlComponentShufflePayload;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.preliminaries.votingoptions.PrimesMappingTableAlgorithms;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.tally.mixoffline.VerifyMixDecInput;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.tally.mixoffline.VerifyMixDecOfflineAlgorithm;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.tally.mixoffline.VerifyMixDecOfflineContext;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.tally.mixonline.GetMixnetInitialCiphertextsOutput;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventContextPayloadService;
import ch.post.it.evoting.securedatamanager.tally.process.decrypt.IdentifierValidationService;

@Service
@ConditionalOnProperty("role.isTally")
public class VerifyMixDecOfflineService {

	private static final Logger LOGGER = LoggerFactory.getLogger(VerifyMixDecOfflineService.class);

	private final IdentifierValidationService identifierValidationService;
	private final VerifyMixDecOfflineAlgorithm verifyMixDecOfflineAlgorithm;
	private final PrimesMappingTableAlgorithms primesMappingTableAlgorithms;
	private final ElectionEventContextPayloadService electionEventContextPayloadService;

	public VerifyMixDecOfflineService(
			final IdentifierValidationService identifierValidationService,
			final VerifyMixDecOfflineAlgorithm verifyMixDecOfflineAlgorithm,
			final PrimesMappingTableAlgorithms primesMappingTableAlgorithms,
			final ElectionEventContextPayloadService electionEventContextPayloadService) {
		this.identifierValidationService = identifierValidationService;
		this.verifyMixDecOfflineAlgorithm = verifyMixDecOfflineAlgorithm;
		this.primesMappingTableAlgorithms = primesMappingTableAlgorithms;
		this.electionEventContextPayloadService = electionEventContextPayloadService;
	}

	/**
	 * Invokes the VerifyMixDecOffline algorithm.
	 *
	 * @param electionEventId                   the election event id. Must be non-null and a valid UUID.
	 * @param verificationCardSetContext        the verification card set context. Must be non-null.
	 * @param setupComponentPublicKeys          the setup component public keys. Must be non-null.
	 * @param controlComponentShufflePayloads   the control component shuffle payloads. Must be non-null.
	 * @param getMixnetInitialCiphertextsOutput the output of the algorithm GetMixnetInitialCiphertexts. Must be non-null.
	 */
	public boolean verifyMixDecOffline(final String electionEventId, final VerificationCardSetContext verificationCardSetContext,
			final SetupComponentPublicKeys setupComponentPublicKeys, final List<ControlComponentShufflePayload> controlComponentShufflePayloads,
			final GetMixnetInitialCiphertextsOutput getMixnetInitialCiphertextsOutput) {
		validateUUID(electionEventId);
		checkNotNull(verificationCardSetContext);
		checkNotNull(setupComponentPublicKeys);
		checkNotNull(controlComponentShufflePayloads);
		checkNotNull(getMixnetInitialCiphertextsOutput);

		final String ballotBoxId = verificationCardSetContext.getBallotBoxId();
		identifierValidationService.validateBallotBoxRelatedIds(electionEventId, ballotBoxId);

		final GqGroup encryptionGroup = electionEventContextPayloadService.loadEncryptionGroup(electionEventId);
		verifyConsistency(encryptionGroup, electionEventId, ballotBoxId, controlComponentShufflePayloads);

		final int numberOfAllowedWriteInsPlusOne = primesMappingTableAlgorithms.getDelta(verificationCardSetContext.getPrimesMappingTable());

		final ElGamalMultiRecipientPublicKey electionPublicKey = setupComponentPublicKeys.electionPublicKey();
		final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> ccmjElectionPublicKey = setupComponentPublicKeys.combinedControlComponentPublicKeys()
				.stream()
				.map(ControlComponentPublicKeys::ccmjElectionPublicKey)
				.collect(GroupVector.toGroupVector());
		final ElGamalMultiRecipientPublicKey electoralBoardPublicKey = setupComponentPublicKeys.electoralBoardPublicKey();

		final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> initialCiphertexts = getMixnetInitialCiphertextsOutput.mixnetInitialCiphertexts();

		final List<VerifiableShuffle> precedingVerifiableShuffledVotes = controlComponentShufflePayloads.stream()
				.map(ControlComponentShufflePayload::getVerifiableShuffle)
				.toList();
		final List<VerifiableDecryptions> precedingVerifiableDecryptedVotes = controlComponentShufflePayloads.stream()
				.map(ControlComponentShufflePayload::getVerifiableDecryptions)
				.toList();

		final VerifyMixDecOfflineContext verifyMixDecOfflineContext = new VerifyMixDecOfflineContext(encryptionGroup, electionEventId, ballotBoxId,
				numberOfAllowedWriteInsPlusOne, electionPublicKey, ccmjElectionPublicKey, electoralBoardPublicKey);
		final VerifyMixDecInput verifyMixDecInput = new VerifyMixDecInput(initialCiphertexts, precedingVerifiableShuffledVotes,
				precedingVerifiableDecryptedVotes);

		LOGGER.debug("Performing VerifyMixDecOffline algorithm... [electionEventId: {}, ballotBoxId: {}]", electionEventId, ballotBoxId);

		return verifyMixDecOfflineAlgorithm.verifyMixDecOffline(verifyMixDecOfflineContext, verifyMixDecInput);
	}

	private void verifyConsistency(final GqGroup encryptionGroup, final String electionEventId, final String ballotBoxId,
			final List<ControlComponentShufflePayload> controlComponentShufflePayloads) {
		final List<Integer> shufflePayloadsNodeIds = controlComponentShufflePayloads.stream()
				.map(Preconditions::checkNotNull)
				.map(ControlComponentShufflePayload::getNodeId)
				.toList();
		checkState(NODE_IDS.size() == shufflePayloadsNodeIds.size() && NODE_IDS.equals(new HashSet<>(shufflePayloadsNodeIds)),
				"Wrong number of control component shuffle payloads.");

		controlComponentShufflePayloads.stream().parallel()
				.forEach(controlComponentShufflePayload -> {
					checkArgument(controlComponentShufflePayload.getEncryptionGroup().equals(encryptionGroup),
							"All control component shuffle payloads must have the same group.");
					checkArgument(controlComponentShufflePayload.getElectionEventId().equals(electionEventId),
							"All control component shuffle payloads must have the same election event id.");
					checkArgument(controlComponentShufflePayload.getBallotBoxId().equals(ballotBoxId),
							"All control component shuffle payloads must have the same ballot box id.");
				});
	}
}
