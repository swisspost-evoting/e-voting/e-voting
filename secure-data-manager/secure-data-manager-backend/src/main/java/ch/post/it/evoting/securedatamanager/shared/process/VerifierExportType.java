/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.shared.process;

public enum VerifierExportType {
	CONTEXT,
	SETUP,
	TALLY
}
