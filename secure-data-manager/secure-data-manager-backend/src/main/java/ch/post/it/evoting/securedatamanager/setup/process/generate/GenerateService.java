/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.process.generate;

import static ch.post.it.evoting.securedatamanager.shared.workflow.WorkflowStep.GENERATE;
import static com.google.common.base.Preconditions.checkState;

import java.util.List;
import java.util.function.Consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.securedatamanager.shared.process.ConfigurationEntityStatusService;
import ch.post.it.evoting.securedatamanager.shared.process.Status;
import ch.post.it.evoting.securedatamanager.shared.process.SynchronizeStatus;
import ch.post.it.evoting.securedatamanager.shared.process.VotingCardSetRepository;
import ch.post.it.evoting.securedatamanager.shared.workflow.WorkflowStepRunner;
import ch.post.it.evoting.securedatamanager.shared.workflow.WorkflowTask;

@Service
@ConditionalOnProperty("role.isSetup")
public class GenerateService {

	private static final Logger LOGGER = LoggerFactory.getLogger(GenerateService.class);

	private final WorkflowStepRunner workflowStepRunner;
	private final VotingCardSetRepository votingCardSetRepository;
	private final ConfigurationEntityStatusService configurationEntityStatusService;
	private final VotingCardSetGenerateBallotService votingCardSetGenerateBallotService;
	private final ReturnCodesPayloadsGenerateService returnCodesPayloadsGenerateService;

	public GenerateService(
			final WorkflowStepRunner workflowStepRunner,
			final VotingCardSetRepository votingCardSetRepository,
			final ConfigurationEntityStatusService configurationEntityStatusService,
			final VotingCardSetGenerateBallotService votingCardSetGenerateBallotService,
			final ReturnCodesPayloadsGenerateService returnCodesPayloadsGenerateService) {
		this.workflowStepRunner = workflowStepRunner;
		this.votingCardSetRepository = votingCardSetRepository;
		this.configurationEntityStatusService = configurationEntityStatusService;
		this.votingCardSetGenerateBallotService = votingCardSetGenerateBallotService;
		this.returnCodesPayloadsGenerateService = returnCodesPayloadsGenerateService;
	}

	/**
	 * Generates the voting card set data. The generation contains 3 steps: generate the ballot box data, generate the ballot file and finally the
	 * Return Codes payloads.
	 *
	 * @param electionEventId The id of the election event.
	 * @return a boolean.
	 */
	public void generate(final String electionEventId) {

		LOGGER.info("Generating the voting card set. [electionEventId: {}]", electionEventId);

		// Retrieve all voting card sets not yet generated.
		final List<String> downloadVotingCardSetIds = votingCardSetRepository.findAllVotingCardSetIdsByStatus(electionEventId, Status.VCS_DOWNLOADED);

		// If there are no voting card sets to generated, do nothing.
		if (!downloadVotingCardSetIds.isEmpty()) {
			LOGGER.info("Found voting card sets to generate. [electionEventId: {}, amount: {}]", electionEventId, downloadVotingCardSetIds.size());
		} else {
			LOGGER.info("No voting card sets to generate. [electionEventId: {}}", electionEventId);
		}

		// Prepare generate tasks.
		final List<WorkflowTask> generateWorkflowTasks = downloadVotingCardSetIds.stream()
				.map(votingCardSetId -> {
					final Runnable generateTask = () -> {
						// Generate the needed data: ballot
						if (!votingCardSetGenerateBallotService.generateBallotData(electionEventId, votingCardSetId)) {
							throw new IllegalStateException(
									String.format("Some of the generates failed. [electionEventId: %s, votingCardSetId: %s]", electionEventId,
											votingCardSetId));
						}
						// Generate the Return Codes payloads
						returnCodesPayloadsGenerateService.generate(electionEventId, votingCardSetId);
					};
					final Runnable successAction = () -> LOGGER.info(
							"Generation of voting card set successful. [electionEventId: {}, votingCardSetId: {}]", electionEventId, votingCardSetId);
					final Consumer<Throwable> failureAction = throwable -> LOGGER.error(
							"Generation of voting card set failed. [electionEventId: {}, votingCardSetId: {}]", electionEventId, votingCardSetId,
							throwable);

					return new WorkflowTask(generateTask, successAction, failureAction);
				})
				.toList();

		final Runnable completeAction = () -> {
			final List<String> generatedVotingCardSetIds = votingCardSetRepository.findAllVotingCardSetIdsByStatus(electionEventId,
					Status.GENERATED);
			checkState(votingCardSetRepository.findAllVotingCardSetIds(electionEventId).size() == generatedVotingCardSetIds.size(),
					"The voting card sets are not all generated. [electionEventId: %s, votingCardSetIds: %s]", electionEventId,
					generatedVotingCardSetIds);

			generatedVotingCardSetIds.stream().parallel()
					.forEach(votingCardSetId -> {
						configurationEntityStatusService.updateWithSynchronizedStatus(Status.SIGNED.name(), votingCardSetId,
								votingCardSetRepository, SynchronizeStatus.PENDING);
						LOGGER.info("Changed the status of the voting card set. [votingCardSetId: {}]", votingCardSetId);
					});
		};

		workflowStepRunner.run(GENERATE, generateWorkflowTasks, completeAction);
	}

}
