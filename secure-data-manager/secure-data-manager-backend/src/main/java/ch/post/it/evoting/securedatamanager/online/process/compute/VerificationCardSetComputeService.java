/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.online.process.compute;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkState;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.securedatamanager.shared.WebClientFactory;
import ch.post.it.evoting.securedatamanager.shared.process.ConfigurationEntityStatusService;
import ch.post.it.evoting.securedatamanager.shared.process.SetupComponentVerificationDataPayloadFileRepository;
import ch.post.it.evoting.securedatamanager.shared.process.Status;
import ch.post.it.evoting.securedatamanager.shared.process.VotingCardSetRepository;

import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;
import reactor.util.retry.RetryBackoffSpec;

/**
 * This is an application service that deals with the computation of voting card data.
 */
@Service
public class VerificationCardSetComputeService {

	private static final Logger LOGGER = LoggerFactory.getLogger(VerificationCardSetComputeService.class);

	private final long maxRequestBodySize;
	private final WebClientFactory webClientFactory;
	private final RetryBackoffSpec retryBackoffSpec;
	private final VotingCardSetRepository votingCardSetRepository;
	private final ConfigurationEntityStatusService configurationEntityStatusService;
	private final EncryptedLongReturnCodeSharesComputeService encryptedLongReturnCodeSharesComputeService;
	private final SetupComponentVerificationDataPayloadFileRepository setupComponentVerificationDataPayloadFileRepository;

	public VerificationCardSetComputeService(
			@Value("${compute.max-request-size}")
			final long maxRequestBodySize,
			final WebClientFactory webClientFactory,
			final RetryBackoffSpec retryBackoffSpec,
			final VotingCardSetRepository votingCardSetRepository,
			final ConfigurationEntityStatusService configurationEntityStatusService,
			final EncryptedLongReturnCodeSharesComputeService encryptedLongReturnCodeSharesComputeService,
			final SetupComponentVerificationDataPayloadFileRepository setupComponentVerificationDataPayloadFileRepository) {
		this.maxRequestBodySize = maxRequestBodySize;
		this.webClientFactory = webClientFactory;
		this.retryBackoffSpec = retryBackoffSpec;
		this.votingCardSetRepository = votingCardSetRepository;
		this.configurationEntityStatusService = configurationEntityStatusService;
		this.encryptedLongReturnCodeSharesComputeService = encryptedLongReturnCodeSharesComputeService;
		this.setupComponentVerificationDataPayloadFileRepository = setupComponentVerificationDataPayloadFileRepository;
	}

	/**
	 * Computes the voting card sets.
	 */
	public void computeVerificationCardSet(final String electionEventId, final String votingCardSetId, final String verificationCardSetId) {
		validateUUID(electionEventId);
		validateUUID(votingCardSetId);
		validateUUID(verificationCardSetId);
		LOGGER.debug("Starting computation of voting card set... [electionEventId: {}, verificationCardSetId: {}, votingCardSetId: {}]",
				electionEventId, verificationCardSetId, votingCardSetId);

		final int chunkCount = setupComponentVerificationDataPayloadFileRepository.getCount(electionEventId, verificationCardSetId);
		checkState(chunkCount > 0, "No chunk found for computation. [electionEventId: %s, verificationCardSetId: %s]", electionEventId,
				verificationCardSetId);

		final List<Integer> queuedChunkIds = getQueuedComputeChunkIds(electionEventId, verificationCardSetId);
		if (!queuedChunkIds.isEmpty()) {
			final String queuedChunkIdsString = queuedChunkIds.stream().map(Object::toString).collect(Collectors.joining(","));
			LOGGER.warn(
					"Ignoring some chunks as they were already queued for computation. [electionEventId: {}, verificationCardSetId: {}, chunkIds: {}]",
					electionEventId, verificationCardSetId, queuedChunkIdsString);
		}

		final AtomicLong consumedBytes = new AtomicLong(0);

		// Send for processing.
		Flux.range(0, chunkCount)
				.filter(chunkId -> !queuedChunkIds.contains(chunkId))
				.transform(flux -> splitFluxBySize(flux, electionEventId, verificationCardSetId, consumedBytes, maxRequestBodySize))
				.publishOn(Schedulers.boundedElastic())
				.flatMap(chunkIdList ->
						Flux.fromIterable(chunkIdList)
								.publishOn(Schedulers.boundedElastic())
								.map(chunkId -> setupComponentVerificationDataPayloadFileRepository.retrieve(electionEventId, verificationCardSetId,
										chunkId))
								.collectList()
				)
				.doOnNext(
						list -> {
							encryptedLongReturnCodeSharesComputeService.computeGenEncLongCodeShares(electionEventId, verificationCardSetId, list);
							LOGGER.info(
									"Computation of batch started. [electionEventId: {}, votingCardSetId: {}, verificationCardSetId: {}, batchSize: {}]",
									electionEventId, votingCardSetId, verificationCardSetId, list.size());
						})
				.doOnComplete(() -> {
					// All chunks have been sent, update status.
					configurationEntityStatusService.update(Status.COMPUTING.name(), votingCardSetId, votingCardSetRepository);
					LOGGER.info("Computation of voting card set started. [electionEventId: {}, votingCardSetId: {}, verificationCardSetId: {}]",
							electionEventId, votingCardSetId, verificationCardSetId);
				})
				.blockLast();
	}

	private List<Integer> getQueuedComputeChunkIds(final String electionEventId, final String verificationCardSetId) {
		final List<Integer> queuedComputeChunkIds = webClientFactory.getWebClient(
						String.format("Request for queued compute chunk ids unsuccessful. [electionEventId: %s, verificationCardSetId: %s]", electionEventId,
								verificationCardSetId))
				.get()
				.uri(uriBuilder -> uriBuilder.path(
								"/api/v1/configuration/electionevent/{electionEventId}/verificationcardset/{verificationCardSetId}/queuedcomputechunkids")
						.build(electionEventId, verificationCardSetId))
				.accept(MediaType.APPLICATION_JSON)
				.retrieve()
				.bodyToFlux(Integer.class)
				.collectList()
				.retryWhen(retryBackoffSpec)
				.block();

		checkState(Objects.nonNull(queuedComputeChunkIds),
				"Queued compute chunk ids cannot be null. [electionEventId: %s, verificationCardSetId: %s]", electionEventId, verificationCardSetId);

		return queuedComputeChunkIds;
	}

	private Flux<List<Integer>> splitFluxBySize(final Flux<Integer> flux,
			final String electionEventId, final String verificationCardSetId, final AtomicLong consumedBytes, final long maxSize) {
		return flux.bufferUntil(payload -> {
			final long payloadSize = setupComponentVerificationDataPayloadFileRepository.getPayloadSize(electionEventId,
					verificationCardSetId);
			final long actualConsumption = consumedBytes.addAndGet(payloadSize);
			if (actualConsumption > maxSize) {
				LOGGER.debug("size limit reached, cutting the flux after this element [actualConsumption: {}, limit: {}]", actualConsumption,
						maxSize);
				consumedBytes.set(0);
				return true;
			}
			return false;
		}, false);
	}

}
