/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.protocol.configuration.setupvoting;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.EXTENDED_AUTHENTICATION_FACTORS;
import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.util.List;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import com.google.common.base.Preconditions;
import com.google.common.collect.MoreCollectors;

import ch.post.it.evoting.evotinglibraries.domain.common.ExtendedAuthenticationFactor;
import ch.post.it.evoting.evotinglibraries.domain.election.VerificationCardSetContext;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.ElectionEventContextPayload;
import ch.post.it.evoting.evotinglibraries.domain.validations.StartVotingKeyValidation;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.Configuration;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventContextPayloadService;

@Service
@ConditionalOnProperty("role.isSetup")
public class GetVoterAuthenticationDataService {

	private static final Logger LOGGER = LoggerFactory.getLogger(GetVoterAuthenticationDataService.class);

	private final ElectionEventContextPayloadService electionEventContextPayloadService;
	private final GetVoterAuthenticationDataAlgorithm getVoterAuthenticationDataAlgorithm;

	public GetVoterAuthenticationDataService(
			final ElectionEventContextPayloadService electionEventContextPayloadService,
			final GetVoterAuthenticationDataAlgorithm getVoterAuthenticationDataAlgorithm) {
		this.electionEventContextPayloadService = electionEventContextPayloadService;
		this.getVoterAuthenticationDataAlgorithm = getVoterAuthenticationDataAlgorithm;
	}

	/**
	 * Invokes the GetVoterAuthenticationData algorithm.
	 *
	 * @param electionEventId       the election event id. Must be non-null and a valid UUID.
	 * @param verificationCardSetId the verification card set id. Must be non-null and a valid UUID.
	 */
	public GetVoterAuthenticationDataOutput getVoterAuthenticationData(final String electionEventId, final String verificationCardSetId,
			final Configuration configuration, final List<String> startVotingKeys, final List<String> extendedAuthenticationFactors) {
		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);
		checkNotNull(configuration);

		final List<String> startVotingKeysCopy = checkNotNull(startVotingKeys).stream().parallel()
				.map(StartVotingKeyValidation::validate)
				.toList();
		final List<String> extendedAuthenticationFactorsCopy = checkNotNull(extendedAuthenticationFactors).stream().parallel()
				.map(Preconditions::checkNotNull)
				.toList();

		final ElectionEventContextPayload electionEventContextPayload = electionEventContextPayloadService.load(electionEventId);
		final int numberOfEligibleVoters = electionEventContextPayload.getElectionEventContext()
				.verificationCardSetContexts()
				.stream()
				.filter(verificationCardSetContext -> verificationCardSetContext.getVerificationCardSetId().equals(verificationCardSetId))
				.map(VerificationCardSetContext::getNumberOfVotingCards)
				.collect(MoreCollectors.onlyElement());

		final int extendedAuthenticationFactor = getExtendedAuthenticationFactorLength(configuration);

		final GetVoterAuthenticationDataContext voterAuthenticationDataContext = new GetVoterAuthenticationDataContext(electionEventId,
				numberOfEligibleVoters, extendedAuthenticationFactor);
		final GetVoterAuthenticationDataInput voterAuthenticationDataInput = new GetVoterAuthenticationDataInput(startVotingKeysCopy,
				extendedAuthenticationFactorsCopy);

		LOGGER.debug("Performing GetVoterAuthenticationData algorithm... [electionEventId: {}, verificationCardSetId: {}]",
				electionEventId, verificationCardSetId);

		return getVoterAuthenticationDataAlgorithm.getVoterAuthenticationData(voterAuthenticationDataContext, voterAuthenticationDataInput);
	}

	private int getExtendedAuthenticationFactorLength(final Configuration configuration) {

		final List<String> keyNames = configuration.getContest().getExtendedAuthenticationKeys().getKeyName();
		checkState(keyNames.size() == 1, "There must be a single extended authentication key name. [size: %s]", keyNames.size());

		final String extendedAuthenticationFactorName = keyNames.get(0);
		final ExtendedAuthenticationFactor extendedAuthenticationFactor = EXTENDED_AUTHENTICATION_FACTORS.get(extendedAuthenticationFactorName);

		checkState(Objects.nonNull(extendedAuthenticationFactor), "Unsupported extended authentication factor. [name: %s]",
				extendedAuthenticationFactorName);

		return extendedAuthenticationFactor.length();
	}
}
