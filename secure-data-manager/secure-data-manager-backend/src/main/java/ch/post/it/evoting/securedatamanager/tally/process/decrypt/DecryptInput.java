/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.tally.process.decrypt;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import com.google.common.base.Preconditions;

import ch.post.it.evoting.evotinglibraries.domain.validations.Validations;

public record DecryptInput(List<String> ballotBoxIds, List<char[]> electoralBoardPasswords) {

	public DecryptInput {
		ballotBoxIds = checkNotNull(ballotBoxIds).stream()
				.map(Validations::validateUUID)
				.toList();

		electoralBoardPasswords = checkNotNull(electoralBoardPasswords).stream()
				.map(Preconditions::checkNotNull)
				.map(char[]::clone)
				.toList();

		checkArgument(electoralBoardPasswords.size() >= 2, "There must be at least two passwords.");
	}

	@Override
	public List<String> ballotBoxIds() {
		return List.copyOf(this.ballotBoxIds);
	}

	@Override
	public List<char[]> electoralBoardPasswords() {
		return this.electoralBoardPasswords.stream()
				.map(char[]::clone)
				.toList();
	}

}
