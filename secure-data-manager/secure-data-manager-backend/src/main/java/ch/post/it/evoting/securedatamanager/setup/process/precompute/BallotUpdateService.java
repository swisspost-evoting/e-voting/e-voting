/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.process.precompute;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.io.IOException;
import java.io.StringReader;
import java.io.UncheckedIOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import jakarta.json.Json;
import jakarta.json.JsonObject;
import jakarta.json.JsonReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.math.PrimeGqElement;
import ch.post.it.evoting.evotinglibraries.domain.election.Ballot;
import ch.post.it.evoting.evotinglibraries.domain.election.Contest;
import ch.post.it.evoting.evotinglibraries.domain.election.ElectionAttributes;
import ch.post.it.evoting.evotinglibraries.domain.election.PrimesMappingTable;
import ch.post.it.evoting.evotinglibraries.domain.election.PrimesMappingTableEntry;

/**
 * Service for updating information of the ballots for a given election event.
 */
@Service
@ConditionalOnProperty("role.isSetup")
public class BallotUpdateService {

	private static final Logger LOGGER = LoggerFactory.getLogger(BallotUpdateService.class);

	private final ObjectMapper objectMapper;

	public BallotUpdateService(final ObjectMapper objectMapper) {
		this.objectMapper = objectMapper;
	}

	/**
	 * Update the options' representation value of the ballot.
	 *
	 * @param electionEventId The electionEventId
	 * @param ballot          The ballot to be updated.
	 * @return Return a JsonObject with the updated ballot.
	 */
	public JsonObject updateOptionsRepresentation(final String electionEventId, final JsonObject ballot,
			final PrimesMappingTable primesMappingTable) {
		validateUUID(electionEventId);
		checkNotNull(ballot);
		checkNotNull(primesMappingTable);

		final JsonObject updateBallot = updateBallotOptions(ballot, primesMappingTable);
		final String ballotId = ballot.getString("id");
		LOGGER.info("Update ballot options' representation. [electionEventId: {}, ballotId: {}]", electionEventId, ballotId);

		return updateBallot;
	}

	private JsonObject updateBallotOptions(final JsonObject ballotObject, final PrimesMappingTable primesMappingTable) {

		final Ballot ballot = toBallot(ballotObject);

		final Map<String, String> actualVotingOptions = ballot.contests().stream()
				.parallel()
				.map(Contest::attributes)
				.flatMap(List::stream)
				.collect(Collectors.toMap(ElectionAttributes::getId, ElectionAttributes::getAlias));

		final Map<String, PrimeGqElement> encodedVotingOptions = primesMappingTable.getPTable().stream()
				.parallel()
				.collect(Collectors.toMap(PrimesMappingTableEntry::actualVotingOption, PrimesMappingTableEntry::encodedVotingOption));

		ballot.contests().stream()
				.map(Contest::options)
				.flatMap(Collection::stream)
				.forEach(electionOption -> {
					final String actualVotingOption = actualVotingOptions.get(electionOption.getAttribute());
					checkState(actualVotingOption != null, "The attribute id does not have a corresponding alias. [attributeId:%s]",
							electionOption.getAttribute());
					final PrimeGqElement encodedVotingOption = encodedVotingOptions.get(actualVotingOption);
					checkState(encodedVotingOption != null,
							"The actual voting option does not have a corresponding encoded voting option. [actualVotingOption:%s]",
							actualVotingOption);
					electionOption.setRepresentation(encodedVotingOption.getValue().toString());
				});

		return toJsonObject(ballot);
	}

	private JsonObject toJsonObject(final Ballot ballot) {
		final JsonObject updatedBallot;
		try {
			final String ballotJson = objectMapper.writeValueAsString(ballot);

			final JsonReader jsonReader = Json.createReader(new StringReader(ballotJson));
			updatedBallot = jsonReader.readObject();
			jsonReader.close();

		} catch (final IOException e) {
			throw new UncheckedIOException("Cannot deserialize the Ballot object to a ballot JsonObject.", e);
		}
		return updatedBallot;
	}

	private Ballot toBallot(final JsonObject ballotObject) {
		final Ballot ballot;
		try {
			ballot = objectMapper.readValue(ballotObject.toString(), Ballot.class);
		} catch (final IOException e) {
			throw new UncheckedIOException("Cannot deserialize the ballot json string to a valid Ballot object.", e);
		}
		return ballot;
	}

}
