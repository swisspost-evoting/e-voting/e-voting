/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.online.votingserverhealth;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.Duration;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import ch.post.it.evoting.securedatamanager.shared.WebClientFactory;

@Service
public class VotingServerHealthService {

	private static final Logger LOGGER = LoggerFactory.getLogger(VotingServerHealthService.class);

	private final List<Consumer<VotingServerHealth>> listeners = new LinkedList<>();
	private final WebClientFactory webClientFactory;
	private final String voterPortalHost;
	private final long readTimeout;

	public VotingServerHealthService(
			final WebClientFactory webClientFactory,
			@Value("${voter.portal.host}")
			final String voterPortalHost,
			@Value("${voting-server.health-check.fixed-rate}")
			final long fixedRate,
			@Value("${voting-server.health-check.read-timeout}")
			final long readTimeout) throws URISyntaxException {
		this.webClientFactory = webClientFactory;
		this.voterPortalHost = checkNotNull(voterPortalHost);
		this.readTimeout = readTimeout;

		checkArgument(readTimeout > 0, "The read timeout must be greater than 0. [readTimeout: %s]", readTimeout);
		checkArgument(readTimeout < fixedRate, "The read timeout must be less than the fixed rate. [readTimeout: %s, fixedRate: %s]", readTimeout,
				fixedRate);

		final URI votingServerURI = new URI(voterPortalHost);
		checkNotNull(votingServerURI.getScheme());
		checkArgument(Objects.isNull(votingServerURI.getQuery()) && Objects.isNull(votingServerURI.getFragment()) &&
				votingServerURI.getPath().isEmpty(), "The voting server host is not valid.");
	}

	public void addListener(final Consumer<VotingServerHealth> consumer) {
		listeners.add(consumer);

		// force the execution to get an immediate result for new listeners.
		run();
	}

	public void removeListener(final Consumer<VotingServerHealth> listener) {
		listeners.remove(listener);
	}

	@Scheduled(fixedRateString = "${voting-server.health-check.fixed-rate}")
	public void run() {
		if (!listeners.isEmpty()) {
			final VotingServerHealth health = getHealth();
			LOGGER.debug("Voting Server health retrieved. [status: {}]", health != null ? health.status() : "");
			listeners.forEach(consumer -> consumer.accept(health));
		}
	}

	private VotingServerHealth getHealth() {
		try {
			return webClientFactory.getWebClient()
					.get()
					.uri(builder -> builder.path("actuator/health").build())
					.accept(MediaType.APPLICATION_JSON)
					.retrieve()
					.bodyToMono(Health.class)
					.map(Health::isUp)
					.map(up -> new VotingServerHealth(up, voterPortalHost))
					.block(Duration.ofMillis(readTimeout));
		} catch (final Exception e) {
			LOGGER.warn("Unable to get the voting server healthiness. Assuming it's not healthy.", e);
		}
		return new VotingServerHealth(false, voterPortalHost);
	}

	@JsonIgnoreProperties(ignoreUnknown = true)
	private record Health(String status) {
		public boolean isUp() {
			final boolean isUp = "UP".equalsIgnoreCase(status);
			if (!isUp) {
				LOGGER.warn("Voting Server is not up. [status: {}]", status);
			}
			return isUp;
		}
	}
}
