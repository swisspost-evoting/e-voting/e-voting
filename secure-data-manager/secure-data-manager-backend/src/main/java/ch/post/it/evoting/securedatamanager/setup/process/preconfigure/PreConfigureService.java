/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.process.preconfigure;

import static ch.post.it.evoting.securedatamanager.shared.workflow.WorkflowStep.PRE_CONFIGURE;
import static com.google.common.base.Preconditions.checkState;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.SignatureException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.evotinglibraries.domain.common.ChannelSecurityContextData;
import ch.post.it.evoting.evotinglibraries.domain.signature.Alias;
import ch.post.it.evoting.evotinglibraries.xml.XmlFileRepository;
import ch.post.it.evoting.evotinglibraries.xml.XsdConstants;
import ch.post.it.evoting.evotinglibraries.xml.hashable.HashableCantonConfigFactory;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.Configuration;
import ch.post.it.evoting.securedatamanager.shared.process.EvotingConfigService;
import ch.post.it.evoting.securedatamanager.shared.process.PathResolver;
import ch.post.it.evoting.securedatamanager.shared.process.summary.ConfigurationSummary;
import ch.post.it.evoting.securedatamanager.shared.process.summary.SummaryService;
import ch.post.it.evoting.securedatamanager.shared.workflow.PreWorkflowTask;
import ch.post.it.evoting.securedatamanager.shared.workflow.WorkflowStepRunner;
import ch.post.it.evoting.securedatamanager.shared.workflow.WorkflowTask;

@Service
@ConditionalOnProperty("role.isSetup")
public class PreConfigureService {

	private static final Logger LOGGER = LoggerFactory.getLogger(PreConfigureService.class);

	private final PathResolver pathResolver;
	private final SummaryService summaryService;
	private final WorkflowStepRunner workflowStepRunner;
	private final EvotingConfigService evotingConfigService;
	private final PreConfigureRepository preConfigureRepository;
	private final SignatureKeystore<Alias> signatureKeystoreService;
	private final XmlFileRepository<Configuration> xmlFileRepository;
	private final ElectionEventConfigService electionEventConfigService;

	public PreConfigureService(
			final PathResolver pathResolver,
			final SummaryService summaryService,
			final WorkflowStepRunner workflowStepRunner,
			final EvotingConfigService evotingConfigService,
			final PreConfigureRepository preConfigureRepository,
			final SignatureKeystore<Alias> signatureKeystoreService,
			final XmlFileRepository<Configuration> xmlFileRepository,
			final ElectionEventConfigService electionEventConfigService) {
		this.pathResolver = pathResolver;
		this.summaryService = summaryService;
		this.evotingConfigService = evotingConfigService;
		this.xmlFileRepository = xmlFileRepository;
		this.workflowStepRunner = workflowStepRunner;
		this.preConfigureRepository = preConfigureRepository;
		this.signatureKeystoreService = signatureKeystoreService;
		this.electionEventConfigService = electionEventConfigService;
	}

	/**
	 * Gets for preview the {@link ConfigurationSummary} from the configuration anonymized file located in the external configuration path.
	 *
	 * @return the configuration summary.
	 */
	public ConfigurationSummary previewConfigurationSummary() {
		LOGGER.debug("Previewing the configuration summary...");

		final Configuration configuration = loadExternalEvotingConfig();

		return summaryService.getConfigurationSummary(configuration);
	}

	/**
	 * Asynchronously pre-configure the election event. Fist, a pre-task is executed to copy the configuration anonymized file from the external to
	 * the internal location.
	 */
	public void preConfigureElectionEvent() {
		LOGGER.debug("Preconfiguring the election event...");

		final PreWorkflowTask<Void> preWorkflowTask = new PreWorkflowTask<>(() -> {
			copyEvotingConfig();
			return null;
		});

		final WorkflowTask workflowTask = new WorkflowTask(
				this::performPreconfigure,
				() -> LOGGER.info("Preconfigure of election event successful."),
				throwable -> LOGGER.error("Preconfigure of election event failed.", throwable)
		);

		workflowStepRunner.run(PRE_CONFIGURE, preWorkflowTask, workflowTask);
	}

	/**
	 * Copy the evoting-config file from the external to the internal path.
	 */
	private void copyEvotingConfig() {
		final Configuration configuration = loadExternalEvotingConfig();

		evotingConfigService.save(configuration);
		LOGGER.info("Successfully copied the evoting-config file.");
	}

	/**
	 * Implements the preconfigure process:
	 * <ul>
	 *     <li>creates the election_config.json file,</li>
	 *     <li>loads the database from the election_config,</li>
	 *     <li>creates the election event.</li>
	 * </ul>
	 */
	private void performPreconfigure() {
		final String electionEventId;
		try {
			LOGGER.debug("Creating the elections_config file...");
			electionEventId = preConfigureRepository.createElectionsConfig();
			LOGGER.info("Successfully created the elections_config. [electionEventId: {}]", electionEventId);

			LOGGER.debug("Reading and saving configuration from elections_config file...");
			preConfigureRepository.loadElectionEventInDatabase();
			LOGGER.info("Successfully saved the elections_config. [electionEventId: {}]", electionEventId);
		} catch (final IOException e) {
			throw new UncheckedIOException(e);
		}

		LOGGER.debug("Creating the election event...");
		electionEventConfigService.create(electionEventId);
		LOGGER.info("Successfully created the election event. [electionEventId: {}]", electionEventId);
	}

	private Configuration loadExternalEvotingConfig() {
		final Path externalConfigurationPath = pathResolver.resolveExternalConfigurationPath();

		if (!Files.exists(externalConfigurationPath)) {
			throw new PreviewSummaryException(
					String.format("The configuration-anonymized file does not exist. [path: %s]", externalConfigurationPath));
		}

		final Configuration configuration = xmlFileRepository.read(externalConfigurationPath, XsdConstants.CANTON_CONFIG_XSD, Configuration.class);
		checkState(isSignatureValid(configuration), "The signature of the configuration-anonymized is not valid.");

		return configuration;
	}

	private boolean isSignatureValid(final Configuration configuration) {
		final byte[] signature = configuration.getSignature();
		final Hashable hashable = HashableCantonConfigFactory.fromConfiguration(configuration);
		final Hashable additionalContextData = ChannelSecurityContextData.cantonConfig();
		try {
			return signatureKeystoreService.verifySignature(Alias.CANTON, hashable, additionalContextData, signature);
		} catch (final SignatureException e) {
			throw new IllegalStateException("Unable to verify evoting-config signature.", e);
		}
	}

}
