/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.process.precompute;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.BIRTH_DATE;
import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.EXTENDED_AUTHENTICATION_FACTORS;
import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.security.SignatureException;
import java.util.List;
import java.util.Objects;
import java.util.stream.IntStream;

import jakarta.json.JsonArray;
import jakarta.json.JsonObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.MoreCollectors;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientKeyPair;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.domain.configuration.SetupComponentVoterAuthenticationData;
import ch.post.it.evoting.domain.configuration.SetupComponentVoterAuthenticationDataPayload;
import ch.post.it.evoting.domain.configuration.setupvoting.VoterInitialCodes;
import ch.post.it.evoting.domain.configuration.setupvoting.VoterInitialCodesPayload;
import ch.post.it.evoting.evotinglibraries.domain.common.ChannelSecurityContextData;
import ch.post.it.evoting.evotinglibraries.domain.common.ExtendedAuthenticationFactor;
import ch.post.it.evoting.evotinglibraries.domain.configuration.SetupComponentTallyDataPayload;
import ch.post.it.evoting.evotinglibraries.domain.returncodes.SetupComponentVerificationData;
import ch.post.it.evoting.evotinglibraries.domain.returncodes.SetupComponentVerificationDataPayload;
import ch.post.it.evoting.evotinglibraries.domain.signature.Alias;
import ch.post.it.evoting.evotinglibraries.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.evotinglibraries.domain.signature.SignedPayload;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.AuthorizationType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.Configuration;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.ExtendedAuthenticationKeyType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.ExtendedAuthenticationKeysType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.VoterType;
import ch.post.it.evoting.securedatamanager.setup.process.VerificationCardSecretKey;
import ch.post.it.evoting.securedatamanager.setup.process.VerificationCardSecretKeyPayload;
import ch.post.it.evoting.securedatamanager.setup.process.VerificationCardSecretKeyPayloadService;
import ch.post.it.evoting.securedatamanager.setup.process.VoterInitialCodesPayloadService;
import ch.post.it.evoting.securedatamanager.setup.protocol.configuration.setupvoting.GenVerDatOutput;
import ch.post.it.evoting.securedatamanager.setup.protocol.configuration.setupvoting.GetVoterAuthenticationDataOutput;
import ch.post.it.evoting.securedatamanager.setup.protocol.configuration.setupvoting.GetVoterAuthenticationDataService;
import ch.post.it.evoting.securedatamanager.shared.JsonConstants;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventContextPayloadService;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventService;
import ch.post.it.evoting.securedatamanager.shared.process.EvotingConfigService;
import ch.post.it.evoting.securedatamanager.shared.process.JsonUtils;
import ch.post.it.evoting.securedatamanager.shared.process.SetupComponentTallyDataPayloadService;
import ch.post.it.evoting.securedatamanager.shared.process.SetupComponentVerificationDataPayloadFileRepository;

/**
 * Service that deals with the persistence of payloads generated in the pre-computation of voting card sets.
 */
@Service
@ConditionalOnProperty("role.isSetup")
public class VotingCardSetPreComputationPersistenceService {

	public static final String VOTING_CARD_SUFFIX = "A13ECA37FA";

	private static final Logger LOGGER = LoggerFactory.getLogger(VotingCardSetPreComputationPersistenceService.class);

	private final ElectionEventService electionEventService;
	private final ElectionEventContextPayloadService electionEventContextPayloadService;
	private final EvotingConfigService evotingConfigService;
	private final SetupComponentVerificationDataPayloadFileRepository setupComponentVerificationDataPayloadFileRepository;
	private final SignatureKeystore<Alias> signatureKeystoreService;
	private final SetupComponentTallyDataPayloadService setupComponentTallyDataPayloadService;
	private final VerificationCardSecretKeyPayloadService verificationCardSecretKeyPayloadService;
	private final VoterInitialCodesPayloadService voterInitialCodesPayloadService;
	private final SetupComponentVoterAuthenticationPayloadService setupComponentVoterAuthenticationPayloadService;
	private final GetVoterAuthenticationDataService getVoterAuthenticationDataService;

	public VotingCardSetPreComputationPersistenceService(
			final ElectionEventService electionEventService,
			final ElectionEventContextPayloadService electionEventContextPayloadService,
			final EvotingConfigService evotingConfigService,
			final SetupComponentVerificationDataPayloadFileRepository setupComponentVerificationDataPayloadFileRepository,
			final SignatureKeystore<Alias> signatureKeystoreService,
			final VerificationCardSecretKeyPayloadService verificationCardSecretKeyPayloadService,
			final SetupComponentTallyDataPayloadService setupComponentTallyDataPayloadService,
			final VoterInitialCodesPayloadService voterInitialCodesPayloadService,
			final SetupComponentVoterAuthenticationPayloadService setupComponentVoterAuthenticationPayloadService,
			final GetVoterAuthenticationDataService getVoterAuthenticationDataService) {

		this.electionEventService = electionEventService;
		this.electionEventContextPayloadService = electionEventContextPayloadService;
		this.evotingConfigService = evotingConfigService;
		this.setupComponentVerificationDataPayloadFileRepository = setupComponentVerificationDataPayloadFileRepository;
		this.signatureKeystoreService = signatureKeystoreService;
		this.verificationCardSecretKeyPayloadService = verificationCardSecretKeyPayloadService;
		this.setupComponentTallyDataPayloadService = setupComponentTallyDataPayloadService;
		this.voterInitialCodesPayloadService = voterInitialCodesPayloadService;
		this.setupComponentVoterAuthenticationPayloadService = setupComponentVoterAuthenticationPayloadService;
		this.getVoterAuthenticationDataService = getVoterAuthenticationDataService;
	}

	/**
	 * Persists the payloads containing the information from the output of the GenVerDat algorithm.
	 */
	public void persistPreComputationPayloads(final VerificationCardSet precomputeContext, final List<GenVerDatOutput> genVerDatOutputs,
			final String ballotId, final String ballotAlias, final String ballotBoxesByElectionEvent) {
		checkNotNull(precomputeContext);
		checkNotNull(genVerDatOutputs);
		validateUUID(ballotId);
		checkNotNull(ballotAlias);
		checkNotNull(ballotBoxesByElectionEvent);

		final String electionEventId = precomputeContext.electionEventId();
		final String verificationCardSetId = precomputeContext.verificationCardSetId();

		checkArgument(electionEventService.exists(electionEventId), "The election event id of the given context does not exist.");

		final GqGroup encryptionGroup = electionEventContextPayloadService.loadEncryptionGroup(precomputeContext.electionEventId());
		// Load the configuration-anonymized
		final Configuration configuration = evotingConfigService.load();

		final List<VoterType> voters = getVotersFromConfigurationAnonymized(electionEventId, verificationCardSetId, ballotId, ballotAlias,
				configuration);
		final List<String> extendedAuthenticationFactors = voters.stream()
				.parallel()
				.map(VoterType::getExtendedAuthenticationKeys)
				.map(ExtendedAuthenticationKeysType::getExtendedAuthenticationKey)
				.map(extendedAuthenticationKeyTypes -> parseExtendedAuthenticationFactor(extendedAuthenticationKeyTypes.get(0)))
				.toList();
		final List<String> verificationCardIds = genVerDatOutputs.stream()
				.parallel()
				.map(GenVerDatOutput::getVerificationCardIds)
				.flatMap(List::stream)
				.toList();
		final List<String> startVotingKeys = genVerDatOutputs.stream()
				.parallel()
				.map(GenVerDatOutput::getStartVotingKeys)
				.flatMap(List::stream)
				.toList();
		final List<ElGamalMultiRecipientKeyPair> verificationCardKeyPairs = genVerDatOutputs.stream()
				.parallel()
				.map(GenVerDatOutput::getVerificationCardKeyPairs)
				.flatMap(List::stream)
				.toList();

		checkArgument(voters.size() == startVotingKeys.size(),
				"The number of start voting keys does not correspond to the number of voters. [electionEventId: %s, verificationCardSetId: %s]",
				electionEventId, verificationCardSetId);
		checkArgument(verificationCardIds.size() == startVotingKeys.size(),
				"The number of start voting keys does not correspond to the number of verification card ids. [electionEventId: %s, verificationCardSetId: %s]",
				electionEventId, verificationCardSetId);

		// Build and persist payloads to request the return code generation (choice return codes and vote cast return code) from the control components.
		persistSetupComponentVerificationData(precomputeContext, encryptionGroup, genVerDatOutputs);
		persistVerificationCardSecretKeyPayload(precomputeContext, encryptionGroup, verificationCardIds, verificationCardKeyPairs);
		persistVoterInitialCodesPayload(precomputeContext, genVerDatOutputs, verificationCardIds, startVotingKeys, voters,
				extendedAuthenticationFactors);
		persistSetupComponentTallyDataPayload(precomputeContext, encryptionGroup, verificationCardIds, verificationCardKeyPairs,
				ballotBoxesByElectionEvent);
		persistVoterAuthenticationData(precomputeContext, verificationCardIds, startVotingKeys, extendedAuthenticationFactors, ballotId,
				configuration);

		LOGGER.info("Successfully persisted all pre-computation payloads. [electionEventId: {}, verificationCardSetId: {}]", electionEventId,
				verificationCardSetId);
	}

	private void persistSetupComponentVerificationData(final VerificationCardSet precomputeContext, final GqGroup encryptionGroup,
			final List<GenVerDatOutput> genVerDatOutputs) {
		final String electionEventId = precomputeContext.electionEventId();
		final String verificationCardSetId = precomputeContext.verificationCardSetId();

		// Delete all existent SetupComponentVerificationDataPayloads in case the pre-computation has failed before.
		setupComponentVerificationDataPayloadFileRepository.remove(electionEventId, verificationCardSetId);

		IntStream.range(0, genVerDatOutputs.size())
				.parallel()
				.forEach(chunkId -> {
					final GenVerDatOutput genVerDatOutput = genVerDatOutputs.get(chunkId);
					final List<SetupComponentVerificationData> setupComponentVerificationData = IntStream.range(0, genVerDatOutput.size())
							.parallel()
							.mapToObj(i -> new SetupComponentVerificationData(
									genVerDatOutput.getVerificationCardIds().get(i),
									genVerDatOutput.getVerificationCardKeyPairs().get(i).getPublicKey(),
									genVerDatOutput.getEncryptedHashedPartialChoiceReturnCodes().get(i),
									genVerDatOutput.getEncryptedHashedConfirmationKeys().get(i)))
							.toList();
					final SetupComponentVerificationDataPayload payload = new SetupComponentVerificationDataPayload(electionEventId,
							verificationCardSetId, genVerDatOutput.getPartialChoiceReturnCodesAllowList(), chunkId, encryptionGroup,
							setupComponentVerificationData);

					final Hashable additionalContextData = ChannelSecurityContextData.setupComponentVerificationData(electionEventId,
							verificationCardSetId);
					payload.setSignature(getPayloadSignature(payload, additionalContextData));

					setupComponentVerificationDataPayloadFileRepository.store(payload);

					LOGGER.debug(
							"Successfully created and persisted the setup component verification data payload. [electionEventId: {}, verificationCardSet: {}, chunkId {}]",
							electionEventId, verificationCardSetId, chunkId);
				});
	}

	private void persistVerificationCardSecretKeyPayload(final VerificationCardSet precomputeContext, final GqGroup encryptionGroup,
			final List<String> verificationCardIds, final List<ElGamalMultiRecipientKeyPair> verificationCardKeyPairs) {
		final String electionEventId = precomputeContext.electionEventId();
		final String verificationCardSetId = precomputeContext.verificationCardSetId();

		final List<VerificationCardSecretKey> verificationCardSecretKeys = IntStream.range(0, verificationCardIds.size())
				.parallel()
				.mapToObj(i -> new VerificationCardSecretKey(verificationCardIds.get(i), verificationCardKeyPairs.get(i).getPrivateKey()))
				.toList();
		final VerificationCardSecretKeyPayload verificationCardSecretKeyPayload = new VerificationCardSecretKeyPayload(encryptionGroup,
				electionEventId, verificationCardSetId, verificationCardSecretKeys);

		verificationCardSecretKeyPayloadService.save(verificationCardSecretKeyPayload);
		LOGGER.info("Successfully created and persisted the verification card secret key payload. [electionEventId: {}, verificationCardSetId: {}]",
				electionEventId, verificationCardSetId);

	}

	private void persistVoterInitialCodesPayload(final VerificationCardSet precomputeContext, final List<GenVerDatOutput> genVerDatOutputs,
			final List<String> verificationCardIds, final List<String> startVotingKeys, final List<VoterType> voters,
			final List<String> extendedAuthenticationFactors) {
		final String electionEventId = precomputeContext.electionEventId();
		final String verificationCardSetId = precomputeContext.verificationCardSetId();
		final String votingCardSetId = precomputeContext.votingCardSetId();

		final List<String> voterIdentifications = voters.stream()
				.parallel()
				.map(VoterType::getVoterIdentification)
				.toList();
		final List<String> ballotCastingKeys = genVerDatOutputs.stream()
				.parallel()
				.map(GenVerDatOutput::getBallotCastingKeys)
				.flatMap(List::stream)
				.toList();

		final List<VoterInitialCodes> voterInitialCodes = IntStream.range(0, voters.size())
				.parallel()
				.mapToObj(voter -> new VoterInitialCodes(voterIdentifications.get(voter), getVotingCardId(verificationCardIds.get(voter)),
						verificationCardIds.get(voter), startVotingKeys.get(voter), extendedAuthenticationFactors.get(voter),
						ballotCastingKeys.get(voter)))
				.toList();
		final VoterInitialCodesPayload payload = new VoterInitialCodesPayload(electionEventId, verificationCardSetId, voterInitialCodes);

		voterInitialCodesPayloadService.save(payload, votingCardSetId);
		LOGGER.info(
				"Successfully created and persisted the voter initial codes payload. [electionEventId: {}, verificationCardSetId: {}, votingCardSetId: {}]",
				electionEventId, verificationCardSetId, votingCardSetId);
	}

	/**
	 * Truncates the {@code verificationCardId} then add a suffix.
	 *
	 * @param verificationCardId the verification card id to truncate.
	 * @return the votingCardId based on the provided verificationCardId.
	 */
	@VisibleForTesting
	String getVotingCardId(final String verificationCardId) {
		final String votingCardId = verificationCardId.substring(0, ID_LENGTH - VOTING_CARD_SUFFIX.length()) + VOTING_CARD_SUFFIX;
		return validateUUID(votingCardId);
	}

	private List<VoterType> getVotersFromConfigurationAnonymized(final String electionEventId, final String verificationCardSetId,
			final String ballotId, final String ballotAlias, final Configuration configuration) {

		// Get the authorization identification for the corresponding ballot id
		final String authorizationIdentification = configuration.getAuthorizations().getAuthorization().stream()
				.map(AuthorizationType::getAuthorizationIdentification)
				.filter(ballotAlias::equals)
				.findFirst()
				.orElseThrow(() -> new IllegalStateException(String.format(
						"No authorization identification for the given ballot alias. [electionEventId: %s, verificationCardSetId: %s, ballotId: %s, ballotAlias: %s]",
						electionEventId, verificationCardSetId, ballotId, ballotAlias)));

		// Get the voters for the corresponding authorization identification
		return configuration.getRegister().getVoter().stream()
				.filter(voter -> voter.getAuthorization().equals(authorizationIdentification))
				.toList();
	}

	private String parseExtendedAuthenticationFactor(final ExtendedAuthenticationKeyType extendedAuthenticationKeyType) {
		final String extendedAuthenticationKeyTypeName = extendedAuthenticationKeyType.getName();

		final ExtendedAuthenticationFactor extendedAuthenticationFactor = EXTENDED_AUTHENTICATION_FACTORS.get(extendedAuthenticationKeyTypeName);
		checkState(Objects.nonNull(extendedAuthenticationFactor), "Unsupported extended authentication factor. [name: %s]",
				extendedAuthenticationKeyTypeName);

		final String extendedAuthenticationKeyTypeValue = extendedAuthenticationKeyType.getValue();

		checkState(extendedAuthenticationKeyTypeValue.matches(extendedAuthenticationFactor.regex()),
				"The extended authentication factor does not have the correct format. [value: %s]", extendedAuthenticationKeyTypeValue);

		if (extendedAuthenticationKeyTypeName.equals(BIRTH_DATE)) {
			final String[] extendedAuthenticationFactorSplit = extendedAuthenticationKeyTypeValue.split("-");
			return extendedAuthenticationFactorSplit[2] + extendedAuthenticationFactorSplit[1] + extendedAuthenticationFactorSplit[0];
		}

		return extendedAuthenticationKeyTypeValue;
	}

	private void persistSetupComponentTallyDataPayload(final VerificationCardSet precomputeContext, final GqGroup encryptionGroup,
			final List<String> verificationCardIds, final List<ElGamalMultiRecipientKeyPair> verificationCardKeyPairs,
			final String ballotBoxesByElectionEvent) {
		final String electionEventId = precomputeContext.electionEventId();
		final String verificationCardSetId = precomputeContext.verificationCardSetId();
		final String ballotBoxId = precomputeContext.ballotBoxId();

		final JsonArray ballotBoxesJsonArray = JsonUtils.getJsonObject(ballotBoxesByElectionEvent).getJsonArray(JsonConstants.RESULT);
		final String ballotBoxDefaultTitle = ballotBoxesJsonArray.stream()
				.parallel()
				.map(JsonObject.class::cast)
				.filter(ballotBoxJsonObject -> ballotBoxId.equals(ballotBoxJsonObject.getString(JsonConstants.ID)))
				.map(ballotBoxJsonObject -> ballotBoxJsonObject.getString(JsonConstants.DEFAULT_TITLE))
				.collect(MoreCollectors.onlyElement());

		final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> verificationCardPublicKeys = verificationCardKeyPairs.stream()
				.parallel()
				.map(ElGamalMultiRecipientKeyPair::getPublicKey)
				.collect(GroupVector.toGroupVector());

		final SetupComponentTallyDataPayload setupComponentTallyDataPayload = new SetupComponentTallyDataPayload(electionEventId,
				verificationCardSetId, ballotBoxDefaultTitle, encryptionGroup, verificationCardIds, verificationCardPublicKeys);

		final Hashable additionalContextData = ChannelSecurityContextData.setupComponentTallyData(electionEventId, verificationCardSetId);
		setupComponentTallyDataPayload.setSignature(getPayloadSignature(setupComponentTallyDataPayload, additionalContextData));

		setupComponentTallyDataPayloadService.save(setupComponentTallyDataPayload);

		LOGGER.debug("Successfully created and persisted the setup component tally data payload. [electionEventId: {}, verificationCardSet: {}]",
				electionEventId, verificationCardSetId);
	}

	private void persistVoterAuthenticationData(final VerificationCardSet precomputeContext, final List<String> verificationCardIds,
			final List<String> startVotingKeys, final List<String> extendedAuthenticationFactors, final String ballotId,
			final Configuration configuration) {
		final String electionEventId = precomputeContext.electionEventId();
		final String verificationCardSetId = precomputeContext.verificationCardSetId();
		final String votingCardSetId = precomputeContext.votingCardSetId();
		final String ballotBoxId = precomputeContext.ballotBoxId();

		checkArgument(verificationCardIds.size() == startVotingKeys.size(),
				"The number of start voting keys does not correspond to the number of verification card ids. [electionEventId: %s, verificationCardSetId: %s]",
				electionEventId, verificationCardSetId);

		final GetVoterAuthenticationDataOutput getVoterAuthenticationDataOutput = getVoterAuthenticationDataService.getVoterAuthenticationData(
				electionEventId, verificationCardSetId, configuration, startVotingKeys, extendedAuthenticationFactors);

		LOGGER.info("GetVoterAuthenticationData algorithm successfully performed. [electionEventId: {}, verificationCardSetId: {}]", electionEventId,
				verificationCardSetId);

		final List<SetupComponentVoterAuthenticationData> setupComponentVoterAuthenticationData = IntStream.range(0, verificationCardIds.size())
				.parallel()
				.mapToObj(i -> {
					final String verificationCardId = verificationCardIds.get(i);
					final String votingCardId = getVotingCardId(verificationCardId);
					final String credentialId = getVoterAuthenticationDataOutput.derivedVoterIdentifiers().get(i);
					final String baseAuthenticationChallenge = getVoterAuthenticationDataOutput.baseAuthenticationChallenges().get(i);
					return new SetupComponentVoterAuthenticationData(electionEventId, verificationCardSetId, votingCardSetId, ballotBoxId, ballotId,
							verificationCardId, votingCardId, credentialId, baseAuthenticationChallenge);
				})
				.toList();
		final SetupComponentVoterAuthenticationDataPayload payload = new SetupComponentVoterAuthenticationDataPayload(
				electionEventId, verificationCardSetId, setupComponentVoterAuthenticationData);

		final Hashable additionalContextData = ChannelSecurityContextData.setupComponentVoterAuthenticationData(electionEventId,
				verificationCardSetId);
		payload.setSignature(getPayloadSignature(payload, additionalContextData));

		setupComponentVoterAuthenticationPayloadService.save(payload);

		LOGGER.info("Successfully created and persisted all voter authentication data. [electionEventId: {}, verificationCardSetId: {}]",
				electionEventId, verificationCardSetId);
	}

	private CryptoPrimitivesSignature getPayloadSignature(final SignedPayload payload, final Hashable additionalContextData) {

		final byte[] signature;
		try {
			signature = signatureKeystoreService.generateSignature(payload, additionalContextData);
		} catch (final SignatureException e) {
			throw new IllegalStateException(String.format("Failed to generate payload signature. [name: %s]", payload.getClass().getName()));
		}

		return new CryptoPrimitivesSignature(signature);
	}
}

