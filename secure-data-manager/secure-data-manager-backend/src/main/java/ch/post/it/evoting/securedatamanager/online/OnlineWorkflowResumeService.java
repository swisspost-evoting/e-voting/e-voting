/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.online;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.domain.tally.BallotBoxStatus;
import ch.post.it.evoting.securedatamanager.online.process.compute.ComputeService;
import ch.post.it.evoting.securedatamanager.online.process.download.DownloadService;
import ch.post.it.evoting.securedatamanager.online.process.mixdownload.MixDownloadService;
import ch.post.it.evoting.securedatamanager.shared.process.BallotBoxService;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventService;
import ch.post.it.evoting.securedatamanager.shared.workflow.WorkflowService;
import ch.post.it.evoting.securedatamanager.shared.workflow.WorkflowStep;

@Service
@ConditionalOnProperty(prefix = "role", name = { "isSetup", "isTally" }, havingValue = "false")
public class OnlineWorkflowResumeService {

	private static final Logger LOGGER = LoggerFactory.getLogger(OnlineWorkflowResumeService.class);

	private final ComputeService computeService;
	private final DownloadService downloadService;
	private final WorkflowService workflowService;
	private final BallotBoxService ballotBoxService;
	private final MixDownloadService mixDownloadService;
	private final ElectionEventService electionEventService;

	public OnlineWorkflowResumeService(
			final ComputeService computeService,
			final DownloadService downloadService,
			final WorkflowService workflowService,
			final BallotBoxService ballotBoxService,
			final MixDownloadService mixDownloadService,
			final ElectionEventService electionEventService) {
		this.computeService = computeService;
		this.downloadService = downloadService;
		this.workflowService = workflowService;
		this.ballotBoxService = ballotBoxService;
		this.mixDownloadService = mixDownloadService;
		this.electionEventService = electionEventService;
	}

	public void resumeWorkflow() {
		final String electionEventId = electionEventService.findElectionEventId();

		// If the compute step is in progress, resume the computation.
		if (workflowService.isStepInProgress(WorkflowStep.COMPUTE)) {
			LOGGER.info("Resume the computation step. [electionEventId: {}]", electionEventId);
			computeService.compute(electionEventId);
			return;
		}

		// If the download step is in progress, resume the download.
		if (workflowService.isStepInProgress(WorkflowStep.DOWNLOAD)) {
			LOGGER.info("Resume the computation step. [electionEventId: {}]", electionEventId);
			downloadService.download(electionEventId);
			return;
		}

		// If the mix_download step is in progress, resume for the ballot boxes in progress.
		if (workflowService.isStepInProgress(WorkflowStep.MIX_DOWNLOAD)) {
			final List<String> ballotBoxIds = ballotBoxService.getBallotBoxesIdByStatus(electionEventId,
					List.of(BallotBoxStatus.MIXING, BallotBoxStatus.MIXED, BallotBoxStatus.MIXING_ERROR));
			LOGGER.info("Resume the mix download step. [electionEventId: {}, ballotBoxIds: {}]", electionEventId, String.join(", ", ballotBoxIds));
			mixDownloadService.mixAndDownload(electionEventId, ballotBoxIds);
			return;
		}
	}
}
