/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.online.process.upload;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.UncheckedIOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import jakarta.json.Json;
import jakarta.json.JsonArray;
import jakarta.json.JsonObject;
import jakarta.json.JsonObjectBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.domain.configuration.BallotDataPayload;
import ch.post.it.evoting.domain.tally.BallotBoxStatus;
import ch.post.it.evoting.evotinglibraries.domain.election.Ballot;
import ch.post.it.evoting.securedatamanager.shared.JsonConstants;
import ch.post.it.evoting.securedatamanager.shared.WebClientFactory;
import ch.post.it.evoting.securedatamanager.shared.database.DatabaseException;
import ch.post.it.evoting.securedatamanager.shared.process.BallotRepository;
import ch.post.it.evoting.securedatamanager.shared.process.BallotTextRepository;
import ch.post.it.evoting.securedatamanager.shared.process.JsonUtils;
import ch.post.it.evoting.securedatamanager.shared.process.SynchronizeStatus;

import reactor.core.publisher.Mono;
import reactor.util.retry.RetryBackoffSpec;

/**
 * Service responsible for uploading ballots and ballot texts
 */
@Service
public class BallotUploadService {

	private static final Logger LOGGER = LoggerFactory.getLogger(BallotUploadService.class);

	private final ObjectMapper objectMapper;
	private final BallotRepository ballotRepository;
	private final WebClientFactory webClientFactory;
	private final RetryBackoffSpec retryBackoffSpec;
	private final BallotTextRepository ballotTextRepository;

	public BallotUploadService(
			final ObjectMapper objectMapper,
			final BallotRepository ballotRepository,
			final WebClientFactory webClientFactory,
			final RetryBackoffSpec retryBackoffSpec,
			final BallotTextRepository ballotTextRepository) {
		this.objectMapper = objectMapper;
		this.ballotRepository = ballotRepository;
		this.ballotTextRepository = ballotTextRepository;
		this.webClientFactory = webClientFactory;
		this.retryBackoffSpec = retryBackoffSpec;
	}

	/**
	 * Uploads the available ballots and ballot texts to the voter portal.
	 */
	public void uploadBallots(final String electionEventId) {
		validateUUID(electionEventId);

		// Get the synchronizable ballots
		final Map<String, Object> ballotParams = new HashMap<>();
		ballotParams.put(JsonConstants.STATUS, BallotBoxStatus.UPDATED.name());
		ballotParams.put(JsonConstants.SYNCHRONIZED, SynchronizeStatus.PENDING.getIsSynchronized().toString());
		ballotParams.put(JsonConstants.ELECTION_EVENT_DOT_ID, electionEventId);
		final String ballotDocuments = ballotRepository.list(ballotParams);

		// Upload each ballot and its ballotTexts, then update status.
		final JsonArray ballotsJson = JsonUtils.getJsonObject(ballotDocuments).getJsonArray(JsonConstants.RESULT);
		for (int i = 0; i < ballotsJson.size(); i++) {
			final JsonObject ballotJson = ballotsJson.getJsonObject(i);

			final Ballot ballot;
			try {
				ballot = objectMapper.readValue(ballotJson.toString(), Ballot.class);
			} catch (final JsonProcessingException e) {
				throw new UncheckedIOException("Failed to deserialized Ballot", e);
			}
			checkArgument(electionEventId.equals(ballotJson.getJsonObject(JsonConstants.ELECTION_EVENT).getString(JsonConstants.ID)));
			final String ballotId = ballotJson.getString(JsonConstants.ID);

			final String ballotTextsString = ballotTextRepository.list(Collections.singletonMap(JsonConstants.BALLOT_DOT_ID, ballot.id()));
			final JsonArray ballotTexts = JsonUtils.getJsonObject(ballotTextsString).getJsonArray(JsonConstants.RESULT);

			final BallotDataPayload ballotDataPayload = new BallotDataPayload(electionEventId, ballotId, ballot, ballotTexts.toString());
			final boolean result = uploadBallotDataPayload(ballotDataPayload);

			// Update status
			final JsonObjectBuilder jsonObjectBuilder = Json.createObjectBuilder();
			if (result) {
				LOGGER.info("Ballot and ballot texts uploaded. [electionEventId: {}, ballotId: {}]", electionEventId, ballotId);
				jsonObjectBuilder.add(JsonConstants.ID, ballotId);
				jsonObjectBuilder.add(JsonConstants.SYNCHRONIZED, SynchronizeStatus.SYNCHRONIZED.getIsSynchronized().toString());
				jsonObjectBuilder.add(JsonConstants.DETAILS, SynchronizeStatus.SYNCHRONIZED.getStatus());
			} else {
				LOGGER.error("An error occurred while uploading the ballot and ballotTexts");
				jsonObjectBuilder.add(JsonConstants.ID, ballotId);
				jsonObjectBuilder.add(JsonConstants.DETAILS, SynchronizeStatus.FAILED.getStatus());
			}

			try {
				ballotRepository.update(jsonObjectBuilder.build().toString());
			} catch (final DatabaseException ex) {
				LOGGER.error("An error occurred while updating the signed ballot", ex);
			}
		}
	}

	private boolean uploadBallotDataPayload(final BallotDataPayload ballotDataPayload) {
		checkNotNull(ballotDataPayload);

		final String electionEventId = ballotDataPayload.electionEventId();
		final String ballotId = ballotDataPayload.ballotId();

		final ResponseEntity<Void> response = webClientFactory.getWebClient()
				.post()
				.uri(uriBuilder -> uriBuilder.path(
								"api/v1/processor/configuration/setupvoting/ballotdata/electionevent/{electionEventId}/ballot/{ballotId}")
						.build(electionEventId, ballotId))
				.accept(MediaType.APPLICATION_JSON)
				.body(Mono.just(ballotDataPayload), BallotDataPayload.class)
				.retrieve()
				.toBodilessEntity()
				.retryWhen(retryBackoffSpec)
				.block();

		return checkNotNull(response).getStatusCode().is2xxSuccessful();
	}
}
