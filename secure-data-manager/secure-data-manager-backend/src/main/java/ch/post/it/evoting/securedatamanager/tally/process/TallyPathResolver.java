/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.tally.process;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.securedatamanager.shared.process.PathResolver;

@Service
@ConditionalOnProperty("role.isTally")
public class TallyPathResolver extends PathResolver {

	private final Path verifierOutput;

	public TallyPathResolver(
			@Value("${sdm.workspace}")
			final Path workspace,
			@Value("${sdm.output.verifier.folder.path:}")
			final Path verifierOutput) throws IOException {
		super(workspace);

		checkNotNull(verifierOutput, "The verifier output path is required for tally.");

		this.verifierOutput = verifierOutput.toRealPath(LinkOption.NOFOLLOW_LINKS);

		checkArgument(Files.isDirectory(this.verifierOutput), "The given verifier output path is not a directory. [path: %s]", this.verifierOutput);
	}

	@Override
	public Path resolveOutputPath() {
		throw new UnsupportedOperationException("The output path is not available in the tally SDM.");
	}

	@Override
	public Path resolveVerifierOutputPath() {
		return verifierOutput;
	}

	@Override
	public Path resolvePrintingOutputPath() {
		throw new UnsupportedOperationException("The printing output path is not available in the tally SDM.");
	}

	@Override
	public Path resolveExternalConfigurationPath() {
		throw new UnsupportedOperationException("The external configuration path is not available in the tally SDM.");
	}

	@Override
	public Path resolveVotingCardSetPath(final String electionEventId, final String votingCardSetId) {
		throw new UnsupportedOperationException(
				"The voting card set path is not available in the tally SDM. [electionEventId: %s, votingCardSetId: %s]".formatted(electionEventId,
						votingCardSetId));
	}

	@Override
	public Path resolveVotingCardSetsPath(final String electionEventId) {
		throw new UnsupportedOperationException(
				"The voting card set paths are not available in the tally SDM. [electionEventId: %s]".formatted(electionEventId));
	}
}
