/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.shared.process;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.URL;
import java.util.Collections;
import java.util.List;

import jakarta.json.Json;
import jakarta.json.JsonArray;
import jakarta.json.JsonObject;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.orientechnologies.common.exception.OException;

import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;
import ch.post.it.evoting.securedatamanager.shared.JsonConstants;
import ch.post.it.evoting.securedatamanager.shared.database.DatabaseException;
import ch.post.it.evoting.securedatamanager.shared.database.DatabaseFixture;
import ch.post.it.evoting.securedatamanager.shared.database.DatabaseManager;

/**
 * Tests of {@link VotingCardSetRepository}.
 */
class VotingCardSetRepositoryTest {

	private static final String VOTING_CARD_SET_ID = "1d9bf23fecd24f899c30b11fe1a6cb5f";
	private static final String BALLOT_BOX_ID = "268872255b9b44f39c5404f3ebd85c07";
	private static final String ELECTION_EVENT_ID = "101549c5a4a04c7b88a0cb9be8ab3df6";
	private static final String VERIFICATION_CARD_SET_ID = "504d6292395b4aba8bde7e25496a0c14";
	private static final String INVALID_VOTING_CARD_SET_ID = "1d9";
	private static final int NUMBER_OF_VOTING_CARDS = 3;
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();

	private DatabaseFixture fixture;
	private BallotBoxRepository ballotBoxRepository;
	private BallotRepository ballotRepository;
	private VotingCardSetRepository repository;

	@BeforeEach
	void setUp() throws OException, IOException {
		fixture = new DatabaseFixture(getClass());
		fixture.setUp();
		final DatabaseManager manager = fixture.databaseManager();
		ballotBoxRepository = mock(BallotBoxRepository.class);
		ballotRepository = mock(BallotRepository.class);
		repository = new VotingCardSetRepository(manager, ballotBoxRepository, ballotRepository);
		repository.ballotBoxRepository = ballotBoxRepository;
		repository.ballotRepository = ballotRepository;
		repository.initialize();
		final URL resource = getClass().getResource(getClass().getSimpleName() + ".json");
		fixture.createDocuments(repository.entityName(), resource);
	}

	@AfterEach
	void tearDown() {
		fixture.tearDown();
	}

	@Test
	void testGetBallotBoxId() {
		assertEquals(BALLOT_BOX_ID, repository.getBallotBoxId(VOTING_CARD_SET_ID));
	}

	@Test
	void testGetBallotBoxIdNotFound() {
		final String ballotBoxId = "101549c5a4a04c7b88a0cb9be8ab3df6";
		assertTrue(repository.getBallotBoxId(ballotBoxId).isEmpty());
	}

	@Test
	void testUpdateRelatedBallot() {
		final JsonObject ballotBox = Json.createObjectBuilder().add(JsonConstants.ALIAS, "ballotBoxAlias")
				.add(JsonConstants.BALLOT, Json.createObjectBuilder().add(JsonConstants.ID, "ballotId")).build();
		when(ballotBoxRepository.find(BALLOT_BOX_ID)).thenReturn(ballotBox.toString());
		final JsonObject ballot = Json.createObjectBuilder().add(JsonConstants.ALIAS, "ballotAlias").build();
		when(ballotRepository.find("ballotId")).thenReturn(ballot.toString());
		repository.updateRelatedBallot(singletonList(VOTING_CARD_SET_ID));
		final String json = repository.find(VOTING_CARD_SET_ID);
		final JsonObject object = JsonUtils.getJsonObject(json);
		assertEquals("ballotBoxAlias", object.getString(JsonConstants.BALLOT_BOX_ALIAS));
		assertEquals("ballotAlias", object.getString(JsonConstants.BALLOT_ALIAS));
	}

	@Test
	void testUpdateRelatedVerificationCardSet() {
		repository.updateRelatedVerificationCardSet(VOTING_CARD_SET_ID, "verificationCardSetId");
		final String json = repository.find(VOTING_CARD_SET_ID);
		assertEquals("verificationCardSetId", JsonUtils.getJsonObject(json).getString(JsonConstants.VERIFICATION_CARD_SET_ID));
	}

	@Test
	void testUpdateRelatedVerificationCardSetNotFound() {
		assertThrows(DatabaseException.class, () -> repository.updateRelatedVerificationCardSet("unknownVotingCardSet", "verificationCardSetId"));
	}

	@Test
	void testListByElectionEvent() {
		final String json = repository.listByElectionEvent(ELECTION_EVENT_ID);
		final JsonArray array = JsonUtils.getJsonObject(json).getJsonArray(JsonConstants.RESULT);
		assertEquals(1, array.size());
		final JsonObject object = array.getJsonObject(0);
		assertEquals(VOTING_CARD_SET_ID, object.getString(JsonConstants.ID));

	}

	@Test
	void testListByElectionEventUnknown() {
		final String unknownElectionEvent = RandomFactory.createRandom().genRandomString(ID_LENGTH, base16Alphabet);
		final String json = repository.listByElectionEvent(unknownElectionEvent);
		final JsonArray array = JsonUtils.getJsonObject(json).getJsonArray(JsonConstants.RESULT);
		assertTrue(array.isEmpty());
	}

	@Test
	void testGetVotingCardSetAlias() {
		final String alias = repository.getVotingCardSetAlias(VOTING_CARD_SET_ID);
		assertEquals("vcs_133", alias);
	}

	@Test
	void testGetVotingCardSetAliasWithNullAndInvalidParameter() {
		assertThrows(NullPointerException.class, () -> repository.getVotingCardSetAlias(null));
		assertThrows(FailedValidationException.class, () -> repository.getVotingCardSetAlias(INVALID_VOTING_CARD_SET_ID));
	}

	@Test
	void testGetNumberOfVotingCards() throws ResourceNotFoundException {
		final int numberOfVotingCards = repository.getNumberOfVotingCards(ELECTION_EVENT_ID, VOTING_CARD_SET_ID);
		assertEquals(NUMBER_OF_VOTING_CARDS, numberOfVotingCards);
	}

	@Test
	void testGetNumberOfVotingCardsWithInvalidParametersThrows() {
		assertThrows(FailedValidationException.class, () -> repository.getNumberOfVotingCards("", VOTING_CARD_SET_ID));
		assertThrows(FailedValidationException.class, () -> repository.getNumberOfVotingCards(ELECTION_EVENT_ID, ""));
	}

	@Test
	void testFindAllVotingCardSetIds() {
		final List<String> votingCardSetIds = repository.findAllVotingCardSetIds(ELECTION_EVENT_ID);
		assertEquals(Collections.singletonList(VOTING_CARD_SET_ID), votingCardSetIds);
	}

	@Test
	void testFindAllVotingCardSetIdsFailedValidation() {
		assertThrows(FailedValidationException.class, () -> repository.findAllVotingCardSetIds("ELECTION_EVENT_ID"));
	}

	@Test
	void testGetVotingCardSetIdFromVerificationCardId() {
		assertEquals(VOTING_CARD_SET_ID, repository.getVotingCardSetId(VERIFICATION_CARD_SET_ID));
	}
}
