/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.protocol.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalFactory.createElGamal;
import static ch.post.it.evoting.cryptoprimitives.hashing.HashFactory.createHash;
import static ch.post.it.evoting.cryptoprimitives.math.BaseEncodingFactory.createBase64;
import static ch.post.it.evoting.cryptoprimitives.symmetric.SymmetricFactory.createSymmetric;
import static ch.post.it.evoting.cryptoprimitives.utils.KeyDerivationFactory.createKeyDerivation;
import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.google.common.base.Throwables;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamal;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientKeyPair;
import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ElGamalGenerator;
import ch.post.it.evoting.domain.generators.ControlComponentCodeSharesPayloadGenerator;
import ch.post.it.evoting.evotinglibraries.domain.election.ElectionEventContext;
import ch.post.it.evoting.evotinglibraries.domain.election.VerificationCardSetContext;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.ElectionEventContextPayload;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.generators.ElectionEventContextPayloadGenerator;
import ch.post.it.evoting.evotinglibraries.domain.returncodes.ControlComponentCodeShare;
import ch.post.it.evoting.evotinglibraries.domain.returncodes.ControlComponentCodeSharesPayload;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.preliminaries.votingoptions.PrimesMappingTableAlgorithms;
import ch.post.it.evoting.securedatamanager.setup.process.SetupKeyPairService;
import ch.post.it.evoting.securedatamanager.setup.process.generate.EncryptedNodeLongReturnCodeSharesChunk;
import ch.post.it.evoting.securedatamanager.setup.process.generate.EncryptedSingleNodeLongReturnCodeSharesChunk;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventContextPayloadService;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventService;

@DisplayName("genCMTable called with")
class GenCMTableServiceTest {

	private static final Random random = RandomFactory.createRandom();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();
	private static final ElectionEventService electionEventService = mock(ElectionEventService.class);
	private static GenCMTableService genCMTableService;
	private static String electionEventId;
	private static EncryptedNodeLongReturnCodeSharesChunk.Builder encryptedNodeLongReturnCodeSharesChunkBuilder;
	private static EncryptedNodeLongReturnCodeSharesChunk encryptedNodeLongReturnCodeSharesChunk;
	private static CombineEncLongCodeSharesOutput combineEncLongCodeSharesOutput;
	private static VerificationCardSetContext verificationCardSetContext;

	@BeforeAll
	static void setUpAll() {
		final ElGamal elGamal = createElGamal();
		final GenCMTableAlgorithm genCMTableAlgorithm = new GenCMTableAlgorithm(createHash(), createBase64(), random, elGamal, createSymmetric(),
				createKeyDerivation());
		final SetupKeyPairService setupKeyPairService = mock(SetupKeyPairService.class);
		final ElectionEventContextPayloadService electionEventContextPayloadService = mock(ElectionEventContextPayloadService.class);
		genCMTableService = new GenCMTableService(setupKeyPairService, genCMTableAlgorithm, electionEventService, new PrimesMappingTableAlgorithms(),
				electionEventContextPayloadService);

		final ElectionEventContextPayloadGenerator electionEventContextPayloadGenerator = new ElectionEventContextPayloadGenerator();
		final ElectionEventContextPayload electionEventContextPayload = electionEventContextPayloadGenerator.generate();
		final ElectionEventContext electionEventContext = electionEventContextPayload.getElectionEventContext();
		electionEventId = electionEventContext.electionEventId();

		verificationCardSetContext = electionEventContext.verificationCardSetContexts().getFirst();

		final GqGroup encryptionGroup = electionEventContextPayload.getEncryptionGroup();
		final int numberOfVotingOptions = verificationCardSetContext.getPrimesMappingTable().getNumberOfVotingOptions();
		final GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> encryptedPreChoiceReturnCodesVector = GroupVector.of(
				elGamal.neutralElement(numberOfVotingOptions, encryptionGroup),
				elGamal.neutralElement(numberOfVotingOptions, encryptionGroup),
				elGamal.neutralElement(numberOfVotingOptions, encryptionGroup),
				elGamal.neutralElement(numberOfVotingOptions, encryptionGroup)
		);
		final GroupVector<GqElement, GqGroup> preVoteCastReturnCodesVector = GroupVector.of(encryptionGroup.getGenerator(),
				encryptionGroup.getGenerator(),
				encryptionGroup.getGenerator(), encryptionGroup.getGenerator());
		final List<String> longVoteCastReturnCodesAllowList = List.of("lVCC1", "lVCC2", "lVCC3", "lVCC4");
		combineEncLongCodeSharesOutput = new CombineEncLongCodeSharesOutput.Builder()
				.setEncryptedPreChoiceReturnCodesVector(encryptedPreChoiceReturnCodesVector)
				.setPreVoteCastReturnCodesVector(preVoteCastReturnCodesVector)
				.setLongVoteCastReturnCodesAllowList(longVoteCastReturnCodesAllowList)
				.build();

		when(electionEventContextPayloadService.load(electionEventId)).thenReturn(electionEventContextPayload);
		when(electionEventContextPayloadService.loadPrimesMappingTable(electionEventId, verificationCardSetContext.getVerificationCardSetId()))
				.thenReturn(verificationCardSetContext.getPrimesMappingTable());

		final ElGamalGenerator elGamalGenerator = new ElGamalGenerator(encryptionGroup);
		final ElGamalMultiRecipientKeyPair setupKeyPair = elGamalGenerator.genRandomKeyPair(
				electionEventContext.maximumNumberOfVotingOptions());
		when(setupKeyPairService.load(electionEventId)).thenReturn(setupKeyPair);
	}

	@BeforeEach
	void setUp() {
		final String verificationCardSetId = verificationCardSetContext.getVerificationCardSetId();
		final GqGroup encryptionGroup = verificationCardSetContext.getPrimesMappingTable().getEncryptionGroup();
		final int numberOfVotingOptions = verificationCardSetContext.getPrimesMappingTable().getNumberOfVotingOptions();
		final List<ControlComponentCodeSharesPayload> controlComponentCodeSharesPayloads = new ControlComponentCodeSharesPayloadGenerator(
				encryptionGroup).generate(electionEventId, verificationCardSetId, 1, numberOfVotingOptions);
		final int chunkId = controlComponentCodeSharesPayloads.getFirst().getChunkId();
		final List<String> verificationCardIds = controlComponentCodeSharesPayloads.getFirst().getControlComponentCodeShares().stream()
				.map(ControlComponentCodeShare::verificationCardId)
				.toList();
		final List<EncryptedSingleNodeLongReturnCodeSharesChunk> nodeReturnCodesValues = controlComponentCodeSharesPayloads.stream()
				.map(payload -> {
					final List<ElGamalMultiRecipientCiphertext> exponentiatedEncryptedConfirmationKeys = payload.getControlComponentCodeShares()
							.stream()
							.map(ControlComponentCodeShare::exponentiatedEncryptedConfirmationKey)
							.toList();
					final List<ElGamalMultiRecipientCiphertext> exponentiatedEncryptedPartialChoiceReturnCodes = payload.getControlComponentCodeShares()
							.stream()
							.map(ControlComponentCodeShare::exponentiatedEncryptedPartialChoiceReturnCodes)
							.toList();
					return new EncryptedSingleNodeLongReturnCodeSharesChunk.Builder()
							.setNodeId(payload.getNodeId())
							.setChunkId(payload.getChunkId())
							.setVerificationCardIds(verificationCardIds)
							.setExponentiatedEncryptedConfirmationKeys(exponentiatedEncryptedConfirmationKeys)
							.setExponentiatedEncryptedPartialChoiceReturnCodes(exponentiatedEncryptedPartialChoiceReturnCodes)
							.build();
				})
				.toList();
		encryptedNodeLongReturnCodeSharesChunkBuilder = new EncryptedNodeLongReturnCodeSharesChunk.Builder()
				.setElectionEventId(electionEventId)
				.setVerificationCardSetId(verificationCardSetId)
				.setVerificationCardIds(verificationCardIds)
				.setChunkId(chunkId)
				.setNodeReturnCodesValues(nodeReturnCodesValues);
		encryptedNodeLongReturnCodeSharesChunk = encryptedNodeLongReturnCodeSharesChunkBuilder.build();

		when(electionEventService.exists(electionEventId)).thenReturn(true);
	}

	private static Stream<Arguments> provideNullParameters() {
		return Stream.of(
				Arguments.of(null, encryptedNodeLongReturnCodeSharesChunk, combineEncLongCodeSharesOutput),
				Arguments.of(electionEventId, null, combineEncLongCodeSharesOutput),
				Arguments.of(electionEventId, encryptedNodeLongReturnCodeSharesChunk, null)
		);
	}

	@ParameterizedTest
	@MethodSource("provideNullParameters")
	@DisplayName("null parameters throws NullPointerException")
	void genCMTableWithNullParametersThrows(final String electionEventId,
			final EncryptedNodeLongReturnCodeSharesChunk encryptedNodeLongReturnCodeSharesChunk,
			final CombineEncLongCodeSharesOutput combineEncLongCodeSharesOutput) {
		assertThrows(NullPointerException.class,
				() -> genCMTableService.genCMTable(electionEventId, encryptedNodeLongReturnCodeSharesChunk,
						combineEncLongCodeSharesOutput));
	}

	@Test
	@DisplayName("invalid election event id throws FailedValidationException")
	void genCMTableWithInvalidElectionEventIdThrows() {
		assertThrows(FailedValidationException.class,
				() -> genCMTableService.genCMTable("InvalidElectionEventId", encryptedNodeLongReturnCodeSharesChunk,
						combineEncLongCodeSharesOutput));
	}

	@Test
	@DisplayName("encrypted node long return code shares chunk of other election event throws IllegalArgumentException")
	void genCMTableWithOtherElectionEventChunkThrows() {
		final String otherElectionEventId = random.genRandomString(ID_LENGTH, base16Alphabet);
		final EncryptedNodeLongReturnCodeSharesChunk nonConsistentChunk = encryptedNodeLongReturnCodeSharesChunkBuilder
				.setElectionEventId(otherElectionEventId)
				.build();

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> genCMTableService.genCMTable(electionEventId, nonConsistentChunk, combineEncLongCodeSharesOutput));

		final String expected = String.format(
				"The encrypted node long return code shares chunk does not correspond to the expected election event id. [expected: %s, actual: %s]",
				electionEventId, nonConsistentChunk.getElectionEventId());
		assertEquals(expected, Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("non existent election event id throws IllegalArgumentException")
	void genCMTableWithNonExistentElectionEventIdThrows() {
		when(electionEventService.exists(electionEventId)).thenReturn(false);

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> genCMTableService.genCMTable(electionEventId, encryptedNodeLongReturnCodeSharesChunk,
						combineEncLongCodeSharesOutput));

		final String expected = String.format("The given election event id does not exist. [electionEventId: %s]", electionEventId);
		assertEquals(expected, Throwables.getRootCause(exception).getMessage());
	}
}
