/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.process.preconfigure;

import static ch.post.it.evoting.cryptoprimitives.math.PrimeGqElement.PrimeGqElementFactory.getSmallPrimeGroupMembers;
import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.util.Map;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientKeyPair;
import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.evotinglibraries.domain.election.generators.PrimesMappingTableGenerator;
import ch.post.it.evoting.securedatamanager.setup.process.SetupKeyPairService;
import ch.post.it.evoting.securedatamanager.setup.protocol.configuration.setupvoting.GenSetupDataOutput;
import ch.post.it.evoting.securedatamanager.setup.protocol.configuration.setupvoting.GenSetupDataService;
import ch.post.it.evoting.securedatamanager.shared.JsonConstants;
import ch.post.it.evoting.securedatamanager.shared.process.ConfigurationEntityStatusService;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventRepository;

@DisplayName("An electionEventService")
@ExtendWith(MockitoExtension.class)
class ElectionEventConfigServiceTest {

	public static final String SEED = "NE_20231124_TT05";
	private static final Random random = RandomFactory.createRandom();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();
	private static final String ELECTION_EVENT_ID = random.genRandomString(ID_LENGTH, base16Alphabet);
	private static GenSetupDataOutput genSetupDataOutput;

	@Mock
	private GenSetupDataService genSetupDataService;
	@Mock
	private SetupKeyPairService setupKeyPairService;
	@Mock
	private ConfigurationEntityStatusService configurationEntityStatusService;
	@Mock
	private ElectionEventRepository electionEventRepository;
	@Mock
	private ElectionEventContextPersistenceService electionEventContextPersistenceService;

	private ElectionEventConfigService electionEventConfigService;

	@BeforeEach
	void setUp() {
		electionEventConfigService = new ElectionEventConfigService(SEED, genSetupDataService, setupKeyPairService, electionEventRepository,
				configurationEntityStatusService, electionEventContextPersistenceService);
	}

	@BeforeAll
	static void setUpAll() {
		final GqGroup gqGroup = GroupTestData.getLargeGqGroup();
		final ElGamalMultiRecipientKeyPair keyPair = ElGamalMultiRecipientKeyPair.genKeyPair(gqGroup, 2, random);
		final PrimesMappingTableGenerator primesMappingTableGenerator = new PrimesMappingTableGenerator(gqGroup);
		genSetupDataOutput = new GenSetupDataOutput.Builder()
				.setEncryptionGroup(gqGroup)
				.setSmallPrimes(getSmallPrimeGroupMembers(gqGroup, 5000))
				.setMaximumNumberOfVotingOptions(2)
				.setMaximumNumberOfSelections(1)
				.setMaximumNumberOfWriteInsPlusOne(1)
				.setPrimesMappingTables(Map.of(random.genRandomString(ID_LENGTH, base16Alphabet), primesMappingTableGenerator.generate()))
				.setSetupKeyPair(keyPair)
				.build();
	}

	@DisplayName("executing create(), with an unsuccessful election event generation, returns an unsuccessful response.")
	@Test
	void createElectionEventGenerateFails() {
		when(genSetupDataService.genSetupData(anyString(), anyString())).thenReturn(genSetupDataOutput);

		doNothing().when(setupKeyPairService).save(anyString(), any());
		doNothing().when(electionEventContextPersistenceService).persist(anyString(), anyString(), any());

		when(electionEventRepository.find(anyString())).thenReturn(JsonConstants.EMPTY_OBJECT);

		assertFalse(electionEventConfigService.create(ELECTION_EVENT_ID));
	}

	@DisplayName("executing create(), with a successful election event generation, returns a successful response and write the related file.")
	@Test
	void create() {
		when(genSetupDataService.genSetupData(anyString(), anyString())).thenReturn(genSetupDataOutput);
		when(configurationEntityStatusService.update(anyString(), anyString(), any())).thenReturn("");

		doNothing().when(setupKeyPairService).save(anyString(), any());
		doNothing().when(electionEventContextPersistenceService).persist(anyString(), anyString(), any());

		when(electionEventRepository.find(anyString())).thenReturn(JsonConstants.ELECTION_EVENT);

		assertTrue(electionEventConfigService.create(ELECTION_EVENT_ID));
	}

}
