/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.protocol.configuration.setupvoting;

import static ch.post.it.evoting.evotinglibraries.domain.ControlComponentConstants.NODE_IDS;
import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamal;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalFactory;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPrivateKey;
import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.hashing.HashFactory;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base64;
import ch.post.it.evoting.cryptoprimitives.math.BaseEncodingFactory;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupMatrix;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.test.tools.TestGroupSetup;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;

/**
 * Tests of CombineEncLongCodeSharesAlgorithm.
 */
@DisplayName("CombineEncLongCodeSharesAlgorithm")
class CombineEncLongCodeSharesAlgorithmTest extends TestGroupSetup {

	private static final int NUMBER_OF_VOTING_OPTIONS = 5;
	private static final List<String> VERIFICATION_CARD_IDS = List.of("e3318008e47d439a92577fcb2c738192", "4f51188102c2421385d250bf48b8b8dd",
			"9b5be5f5068a499d9998d48cb394aee1", "f51188102c2421385d250bf48b845b8a");
	private final int MAXIMUM_NUMBER_OF_VOTING_OPTIONS = 10;
	private final ElGamal elGamal = ElGamalFactory.createElGamal();

	@Nested
	@DisplayName("calling combineEncLongCodeShares with")
	class CombineEncLongCodeSharesTest {
		private final Hash hash = HashFactory.createHash();
		private final ElGamal elGamal = ElGamalFactory.createElGamal();
		private final Base64 base64 = BaseEncodingFactory.createBase64();
		private final CombineEncLongCodeSharesAlgorithm combineEncLongCodeSharesAlgorithm = new CombineEncLongCodeSharesAlgorithm(hash,
				elGamal, base64);
		private final int NUM_ROWS = 4;
		private final int NUM_COLS = 4;
		private final List<String> verificationCardIds_3 = VERIFICATION_CARD_IDS.subList(0, 2);
		private final ElGamalMultiRecipientPrivateKey setupSecretKey = elGamalGenerator.genRandomPrivateKey(MAXIMUM_NUMBER_OF_VOTING_OPTIONS);
		private final String electionEventId = "0123456789abcdef0123456789abcdef";
		private final String verificationCardSetId = "abcdef0123456789abcdef0123456789";
		private final GroupMatrix<ElGamalMultiRecipientCiphertext, GqGroup> exponentiatedEncryptedPartialChoiceReturnCodesMatrix =
				elGamalGenerator.genRandomCiphertextMatrix(NUM_ROWS, NUM_COLS, NUMBER_OF_VOTING_OPTIONS);
		private final GroupMatrix<ElGamalMultiRecipientCiphertext, GqGroup> exponentiatedEncryptedConfirmationKeysMatrix =
				elGamalGenerator.genRandomCiphertextMatrix(NUM_ROWS, NUM_COLS, 1);
		private final CombineEncLongCodeSharesInput input =
				new CombineEncLongCodeSharesInput.Builder()
						.setExponentiatedEncryptedChoiceReturnCodesMatrix(exponentiatedEncryptedPartialChoiceReturnCodesMatrix)
						.setExponentiatedEncryptedConfirmationKeysMatrix(exponentiatedEncryptedConfirmationKeysMatrix)
						.setSetupSecretKey(setupSecretKey)
						.build();
		private final CombineEncLongCodeSharesContext context =
				new CombineEncLongCodeSharesContext.Builder()
						.setEncryptionGroup(gqGroup)
						.setElectionEventId(electionEventId)
						.setVerificationCardSetId(verificationCardSetId)
						.setVerificationCardIds(VERIFICATION_CARD_IDS)
						.setNumberOfVotingOptions(NUMBER_OF_VOTING_OPTIONS)
						.setMaximumNumberOfVotingOptions(MAXIMUM_NUMBER_OF_VOTING_OPTIONS)
						.build();

		@Test
		@DisplayName("The context is null")
		void testWithNullContext() {
			assertThrows(NullPointerException.class, () -> combineEncLongCodeSharesAlgorithm.combineEncLongCodeShares(null, input));
		}

		@Test
		@DisplayName("The input is null")
		void testWithNullInput() {
			assertThrows(NullPointerException.class, () -> combineEncLongCodeSharesAlgorithm.combineEncLongCodeShares(context, null));
		}

		@Test
		@DisplayName("Vector of Verification Card Ids with different columns")
		void differentSizeVerificationCardIdsThrows() {
			final CombineEncLongCodeSharesContext context = new CombineEncLongCodeSharesContext.Builder()
					.setEncryptionGroup(gqGroup)
					.setElectionEventId(electionEventId)
					.setVerificationCardSetId(verificationCardSetId)
					.setVerificationCardIds(verificationCardIds_3)
					.setNumberOfVotingOptions(NUMBER_OF_VOTING_OPTIONS)
					.setMaximumNumberOfVotingOptions(MAXIMUM_NUMBER_OF_VOTING_OPTIONS)
					.build();

			final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
					() -> combineEncLongCodeSharesAlgorithm.combineEncLongCodeShares(context, input));

			final String expectedMessage = "The number of rows of the matrices C_expPCC and C_expCK must be equal to the number of eligible voters.";
			final String actualMessage = exception.getMessage();

			assertEquals(expectedMessage, actualMessage);
		}

		@Test
		@DisplayName("The context and input are not null")
		void testWithNonNullContextAndInput() {
			final CombineEncLongCodeSharesOutput output = combineEncLongCodeSharesAlgorithm.combineEncLongCodeShares(context, input);

			assertNotNull(output);

			final int N_E = input.getExponentiatedEncryptedHashedPartialChoiceReturnCodesMatrix().numRows();

			final List<Integer> sizes = Arrays.asList(
					output.getEncryptedPreChoiceReturnCodesVector().size(),
					output.getPreVoteCastReturnCodesVector().size(),
					output.getLongVoteCastReturnCodesAllowList().size());

			assertTrue(sizes.stream().allMatch(size -> size == N_E));
		}
	}

	@Nested
	@DisplayName("CombineEncLongCodeSharesContext with")
	class CombineEncLongCodeSharesContextTest {

		private final String electionEventId = "0123456789abcdef0123456789abcdef";
		private final String verificationCardSetId = "abcdef0123456789abcdef0123456789";

		@Test
		@DisplayName("The encryptionParameters null")
		void contextWithNullEncryptionParameters() {
			final CombineEncLongCodeSharesContext.Builder builder = new CombineEncLongCodeSharesContext.Builder()
					.setEncryptionGroup(null)
					.setElectionEventId(electionEventId)
					.setVerificationCardSetId(verificationCardSetId)
					.setVerificationCardIds(VERIFICATION_CARD_IDS)
					.setNumberOfVotingOptions(NUMBER_OF_VOTING_OPTIONS)
					.setMaximumNumberOfVotingOptions(MAXIMUM_NUMBER_OF_VOTING_OPTIONS);

			assertThrows(NullPointerException.class, builder::build);
		}

		@Test
		@DisplayName("The Election Event Id null")
		void contextWithNullElectionEventId() {
			final CombineEncLongCodeSharesContext.Builder builder = new CombineEncLongCodeSharesContext.Builder()
					.setEncryptionGroup(gqGroup)
					.setElectionEventId(null)
					.setVerificationCardSetId(verificationCardSetId)
					.setVerificationCardIds(VERIFICATION_CARD_IDS)
					.setNumberOfVotingOptions(NUMBER_OF_VOTING_OPTIONS)
					.setMaximumNumberOfVotingOptions(MAXIMUM_NUMBER_OF_VOTING_OPTIONS);

			assertThrows(NullPointerException.class, builder::build);
		}

		@Test
		@DisplayName("The Verification Card Set Id null")
		void contextWithNullVerificationCardSetId() {
			final CombineEncLongCodeSharesContext.Builder builder = new CombineEncLongCodeSharesContext.Builder()
					.setEncryptionGroup(gqGroup)
					.setElectionEventId(electionEventId)
					.setVerificationCardSetId(null)
					.setVerificationCardIds(VERIFICATION_CARD_IDS)
					.setNumberOfVotingOptions(NUMBER_OF_VOTING_OPTIONS)
					.setMaximumNumberOfVotingOptions(MAXIMUM_NUMBER_OF_VOTING_OPTIONS);

			assertThrows(NullPointerException.class, builder::build);
		}

		@Test
		@DisplayName("The Verification Card Ids null")
		void contextWithNullVerificationCardIds() {
			final CombineEncLongCodeSharesContext.Builder builder = new CombineEncLongCodeSharesContext.Builder()
					.setEncryptionGroup(gqGroup)
					.setElectionEventId(electionEventId)
					.setVerificationCardSetId(verificationCardSetId)
					.setVerificationCardIds(null)
					.setNumberOfVotingOptions(NUMBER_OF_VOTING_OPTIONS)
					.setMaximumNumberOfVotingOptions(MAXIMUM_NUMBER_OF_VOTING_OPTIONS);

			assertThrows(NullPointerException.class, builder::build);
		}

		@Test
		@DisplayName("The Election Event Id invalid UUID")
		void contextWithInvalidElectionEventId() {
			final String invalidElectionEventId = "zdiauzdi134";

			final CombineEncLongCodeSharesContext.Builder builder = new CombineEncLongCodeSharesContext.Builder()
					.setEncryptionGroup(gqGroup)
					.setElectionEventId(invalidElectionEventId)
					.setVerificationCardSetId(verificationCardSetId)
					.setVerificationCardIds(VERIFICATION_CARD_IDS)
					.setNumberOfVotingOptions(NUMBER_OF_VOTING_OPTIONS)
					.setMaximumNumberOfVotingOptions(MAXIMUM_NUMBER_OF_VOTING_OPTIONS);

			final FailedValidationException exception = assertThrows(FailedValidationException.class, builder::build);

			final String expectedMessage = String.format(
					"The given string does not comply with the required format. [string: %s, format: ^[0123456789abcdefABCDEF]{32}$].",
					invalidElectionEventId);
			final String actualMessage = exception.getMessage();

			assertEquals(expectedMessage, actualMessage);
		}

		@Test
		@DisplayName("The Verification Card Set Id invalid UUID")
		void contextWithInvalidVerificationCardSetId() {
			final String invalidVerificationCardSetId = "zdiauzdi134";

			final CombineEncLongCodeSharesContext.Builder builder = new CombineEncLongCodeSharesContext.Builder()
					.setEncryptionGroup(gqGroup)
					.setElectionEventId(electionEventId)
					.setVerificationCardSetId(invalidVerificationCardSetId)
					.setVerificationCardIds(VERIFICATION_CARD_IDS)
					.setNumberOfVotingOptions(NUMBER_OF_VOTING_OPTIONS)
					.setMaximumNumberOfVotingOptions(MAXIMUM_NUMBER_OF_VOTING_OPTIONS);

			final FailedValidationException exception = assertThrows(FailedValidationException.class, builder::build);

			final String expectedMessage = String.format(
					"The given string does not comply with the required format. [string: %s, format: ^[0123456789abcdefABCDEF]{32}$].",
					invalidVerificationCardSetId);
			final String actualMessage = exception.getMessage();

			assertEquals(expectedMessage, actualMessage);
		}

		@Test
		@DisplayName("The number of voting options is negative")
		void contextWithNegativeNumberOfVotingOptions() {
			final CombineEncLongCodeSharesContext.Builder builder = new CombineEncLongCodeSharesContext.Builder()
					.setEncryptionGroup(gqGroup)
					.setElectionEventId(electionEventId)
					.setVerificationCardSetId(verificationCardSetId)
					.setVerificationCardIds(VERIFICATION_CARD_IDS)
					.setNumberOfVotingOptions(-50)
					.setMaximumNumberOfVotingOptions(MAXIMUM_NUMBER_OF_VOTING_OPTIONS);

			final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, builder::build);

			final String expectedMessage = "The number of voting options must be strictly positive.";
			final String actualMessage = exception.getMessage();

			assertEquals(expectedMessage, actualMessage);
		}

		@Test
		@DisplayName("The maximum number of voting options is negative")
		void contextWithNegativeMaximumNumberOfVotingOptions() {
			final CombineEncLongCodeSharesContext.Builder builder = new CombineEncLongCodeSharesContext.Builder()
					.setEncryptionGroup(gqGroup)
					.setElectionEventId(electionEventId)
					.setVerificationCardSetId(verificationCardSetId)
					.setVerificationCardIds(VERIFICATION_CARD_IDS)
					.setNumberOfVotingOptions(NUMBER_OF_VOTING_OPTIONS)
					.setMaximumNumberOfVotingOptions(-50);

			final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, builder::build);

			final String expectedMessage = "The maximum number of voting options must be strictly positive.";
			final String actualMessage = exception.getMessage();

			assertEquals(expectedMessage, actualMessage);
		}

		@Test
		@DisplayName("Constructor with Vector of Verification Card Ids of invalid UUID format")
		void contextWithInvalidVerificationCardIds() {
			final String verificationCardId_invalid = "f51188102c2421385zS";
			final CombineEncLongCodeSharesContext.Builder builder = new CombineEncLongCodeSharesContext.Builder()
					.setEncryptionGroup(gqGroup)
					.setElectionEventId(electionEventId)
					.setVerificationCardSetId(verificationCardSetId)
					.setVerificationCardIds(Collections.singletonList(verificationCardId_invalid))
					.setNumberOfVotingOptions(NUMBER_OF_VOTING_OPTIONS)
					.setMaximumNumberOfVotingOptions(MAXIMUM_NUMBER_OF_VOTING_OPTIONS);

			final FailedValidationException exception = assertThrows(FailedValidationException.class, builder::build);

			final String expectedMessage = String.format(
					"The given string does not comply with the required format. [string: %s, format: ^[0123456789abcdefABCDEF]{32}$].",
					verificationCardId_invalid);
			final String actualMessage = exception.getMessage();

			assertEquals(expectedMessage, actualMessage);
		}

		@Test
		@DisplayName("Constructor with Vector of Verification Card Ids with duplicates")
		void contextWithDuplicatesInVerificationCardIds() {
			final List<String> duplicateVerificationCardIds = List.of(VERIFICATION_CARD_IDS.get(0), VERIFICATION_CARD_IDS.get(0));
			final CombineEncLongCodeSharesContext.Builder builder = new CombineEncLongCodeSharesContext.Builder()
					.setEncryptionGroup(gqGroup)
					.setElectionEventId(electionEventId)
					.setVerificationCardSetId(verificationCardSetId)
					.setVerificationCardIds(duplicateVerificationCardIds)
					.setNumberOfVotingOptions(NUMBER_OF_VOTING_OPTIONS)
					.setMaximumNumberOfVotingOptions(MAXIMUM_NUMBER_OF_VOTING_OPTIONS);

			final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, builder::build);

			final String expectedMessage = "The verification card id list must not have duplicates.";
			final String actualMessage = exception.getMessage();

			assertEquals(expectedMessage, actualMessage);
		}

		@Test
		@DisplayName("Constructor ector of Verification Card Ids with the padding character =")
		void contexWithPaddingStartVerificationCardIds() {
			final Random random = RandomFactory.createRandom();
			final Base16Alphabet base16Alphabet = Base16Alphabet.getInstance();
			final List<String> verificationCardIdsElementWithPadding = List.copyOf(
					Stream.generate(() -> random.genRandomString(ID_LENGTH - 1, base16Alphabet) + "=")
							.limit(VERIFICATION_CARD_IDS.size())
							.toList());

			final CombineEncLongCodeSharesContext.Builder builder = new CombineEncLongCodeSharesContext.Builder()
					.setEncryptionGroup(gqGroup)
					.setElectionEventId(electionEventId)
					.setVerificationCardSetId(verificationCardSetId)
					.setVerificationCardIds(verificationCardIdsElementWithPadding)
					.setNumberOfVotingOptions(NUMBER_OF_VOTING_OPTIONS)
					.setMaximumNumberOfVotingOptions(MAXIMUM_NUMBER_OF_VOTING_OPTIONS);

			final FailedValidationException ex = assertThrows(FailedValidationException.class, builder::build);

			final String expectedMessage = "The given string does not comply with the required format.";

			assertTrue(ex.getMessage().startsWith(expectedMessage));
		}

		@Test
		@DisplayName("output for input with same size and Gq group")
		void contextWithCorrectValues() {
			final CombineEncLongCodeSharesContext context =
					new CombineEncLongCodeSharesContext.Builder()
							.setEncryptionGroup(gqGroup)
							.setElectionEventId(electionEventId)
							.setVerificationCardSetId(verificationCardSetId)
							.setVerificationCardIds(VERIFICATION_CARD_IDS)
							.setNumberOfVotingOptions(NUMBER_OF_VOTING_OPTIONS)
							.setMaximumNumberOfVotingOptions(MAXIMUM_NUMBER_OF_VOTING_OPTIONS)
							.build();

			assertEquals(gqGroup, context.getEncryptionGroup());
			assertEquals(electionEventId, context.getElectionEventId());
			assertEquals(verificationCardSetId, context.getVerificationCardSetId());
			assertEquals(VERIFICATION_CARD_IDS, context.getVerificationCardIds());
			assertEquals(NUMBER_OF_VOTING_OPTIONS, context.getNumberOfVotingOptions());
			assertEquals(MAXIMUM_NUMBER_OF_VOTING_OPTIONS, context.getMaximumNumberOfVotingOptions());
		}
	}

	@Nested
	@DisplayName("CombineEncLongCodeSharesInput with")
	class CombineEncLongCodeSharesInputTest {

		private final ElGamalMultiRecipientPrivateKey setupSecretKey = elGamalGenerator.genRandomPrivateKey(MAXIMUM_NUMBER_OF_VOTING_OPTIONS);
		private GroupMatrix<ElGamalMultiRecipientCiphertext, GqGroup> exponentiatedEncryptedPartialChoiceReturnCodesMatrix_2x2;
		private GroupMatrix<ElGamalMultiRecipientCiphertext, GqGroup> exponentiatedEncryptedPartialChoiceReturnCodesMatrix_3x4;
		private GroupMatrix<ElGamalMultiRecipientCiphertext, GqGroup> exponentiatedEncryptedPartialChoiceReturnCodesMatrix_4x4;
		private GroupMatrix<ElGamalMultiRecipientCiphertext, GqGroup> exponentiatedEncryptedPartialConfirmationKeysMatrix_3x2;
		private GroupMatrix<ElGamalMultiRecipientCiphertext, GqGroup> exponentiatedEncryptedPartialConfirmationKeysMatrix_4x4_grp_1;
		private GroupMatrix<ElGamalMultiRecipientCiphertext, GqGroup> exponentiatedEncryptedPartialConfirmationKeysMatrix_4x4_grp_2;

		@BeforeEach
		void setUp() {
			exponentiatedEncryptedPartialChoiceReturnCodesMatrix_2x2 = elGamalGenerator.genRandomCiphertextMatrix(2, 2, NUMBER_OF_VOTING_OPTIONS);
			exponentiatedEncryptedPartialChoiceReturnCodesMatrix_3x4 = elGamalGenerator.genRandomCiphertextMatrix(3, 4, NUMBER_OF_VOTING_OPTIONS);
			exponentiatedEncryptedPartialChoiceReturnCodesMatrix_4x4 = elGamalGenerator.genRandomCiphertextMatrix(4, 4, NUMBER_OF_VOTING_OPTIONS);
			exponentiatedEncryptedPartialConfirmationKeysMatrix_3x2 = elGamalGenerator.genRandomCiphertextMatrix(3, 2, 1);
			exponentiatedEncryptedPartialConfirmationKeysMatrix_4x4_grp_1 = elGamalGenerator.genRandomCiphertextMatrix(4, 4, 1);
			exponentiatedEncryptedPartialConfirmationKeysMatrix_4x4_grp_2 = otherGroupElGamalGenerator.genRandomCiphertextMatrix(4, 4, 1);
		}

		@Test
		@DisplayName("Constructor with Matrix Choice Return Codes null")
		void inputWithChoiceReturnCodesMatrixNull() {
			final CombineEncLongCodeSharesInput.Builder builder = new CombineEncLongCodeSharesInput.Builder()
					.setExponentiatedEncryptedChoiceReturnCodesMatrix(null)
					.setExponentiatedEncryptedConfirmationKeysMatrix(exponentiatedEncryptedPartialConfirmationKeysMatrix_4x4_grp_1)
					.setSetupSecretKey(setupSecretKey);

			assertThrows(NullPointerException.class, builder::build);
		}

		@Test
		@DisplayName("Constructor with Matrix of Confirmation Keys null")
		void inputWithNullMatrixConfirmationKeys() {
			final CombineEncLongCodeSharesInput.Builder builder = new CombineEncLongCodeSharesInput.Builder()
					.setExponentiatedEncryptedChoiceReturnCodesMatrix(exponentiatedEncryptedPartialChoiceReturnCodesMatrix_2x2)
					.setExponentiatedEncryptedConfirmationKeysMatrix(null)
					.setSetupSecretKey(setupSecretKey);

			assertThrows(NullPointerException.class, builder::build);
		}

		@Test
		@DisplayName("Constructor with Setup Secret Key null")
		void inputWithNullSetupSecretKey() {
			final CombineEncLongCodeSharesInput.Builder builder = new CombineEncLongCodeSharesInput.Builder()
					.setExponentiatedEncryptedChoiceReturnCodesMatrix(exponentiatedEncryptedPartialChoiceReturnCodesMatrix_2x2)
					.setExponentiatedEncryptedConfirmationKeysMatrix(exponentiatedEncryptedPartialConfirmationKeysMatrix_4x4_grp_1)
					.setSetupSecretKey(null);

			assertThrows(NullPointerException.class, builder::build);
		}

		@Test
		@DisplayName("Constructor with different column size of both matrices")
		void inputWithDifferentMatrixColumnSizes() {
			final CombineEncLongCodeSharesInput.Builder builder = new CombineEncLongCodeSharesInput.Builder()
					.setExponentiatedEncryptedChoiceReturnCodesMatrix(exponentiatedEncryptedPartialChoiceReturnCodesMatrix_2x2)
					.setExponentiatedEncryptedConfirmationKeysMatrix(exponentiatedEncryptedPartialConfirmationKeysMatrix_4x4_grp_1)
					.setSetupSecretKey(setupSecretKey);

			final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, builder::build);

			final String expectedMessage = "C_expPCC and C_expCK must have the same number of columns.";
			final String actualMessage = exception.getMessage();

			assertEquals(expectedMessage, actualMessage);
		}

		@Test
		@DisplayName("Constructor with matrices column size != 4")
		void inputWithWrongMatrixColumnSize() {
			final CombineEncLongCodeSharesInput.Builder builder = new CombineEncLongCodeSharesInput.Builder()
					.setExponentiatedEncryptedChoiceReturnCodesMatrix(exponentiatedEncryptedPartialChoiceReturnCodesMatrix_2x2)
					.setExponentiatedEncryptedConfirmationKeysMatrix(exponentiatedEncryptedPartialConfirmationKeysMatrix_3x2)
					.setSetupSecretKey(setupSecretKey);

			final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, builder::build);

			final String expectedMessage = String.format(
					"The matrix of exponentiated, encrypted, hashed partial Choice Return Codes does not have the expected number of columns. [expected: %s, actual: %s]",
					NODE_IDS.size(), exponentiatedEncryptedPartialChoiceReturnCodesMatrix_2x2.numColumns());
			final String actualMessage = exception.getMessage();

			assertEquals(expectedMessage, actualMessage);
		}

		@Test
		@DisplayName("Constructor with matrix with different row numbers")
		void inputWithDifferentMatrixRowSizes() {
			final CombineEncLongCodeSharesInput.Builder builder = new CombineEncLongCodeSharesInput.Builder()
					.setExponentiatedEncryptedChoiceReturnCodesMatrix(exponentiatedEncryptedPartialChoiceReturnCodesMatrix_3x4)
					.setExponentiatedEncryptedConfirmationKeysMatrix(exponentiatedEncryptedPartialConfirmationKeysMatrix_4x4_grp_1)
					.setSetupSecretKey(setupSecretKey);

			final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, builder::build);

			final String expectedMessage = "C_expPCC and C_expCK must have the same number of rows.";
			final String actualMessage = exception.getMessage();

			assertEquals(expectedMessage, actualMessage);
		}

		@Test
		@DisplayName("Constructor with matrices having different group")
		void inputWithDifferentGroupMatrix() {
			final CombineEncLongCodeSharesInput.Builder builder = new CombineEncLongCodeSharesInput.Builder()
					.setExponentiatedEncryptedChoiceReturnCodesMatrix(exponentiatedEncryptedPartialChoiceReturnCodesMatrix_4x4)
					.setExponentiatedEncryptedConfirmationKeysMatrix(exponentiatedEncryptedPartialConfirmationKeysMatrix_4x4_grp_2)
					.setSetupSecretKey(setupSecretKey);

			final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, builder::build);

			final String expectedMessage = "The Matrix of exponentiated, encrypted, hashed partial Choice Return Codes and Confirmation Keys must have the same group.";
			final String actualMessage = exception.getMessage();

			assertEquals(expectedMessage, actualMessage);
		}

		@Test
		@DisplayName("Constructor with correct values")
		void inputWithCrrectValues() {
			final CombineEncLongCodeSharesInput input =
					new CombineEncLongCodeSharesInput.Builder()
							.setExponentiatedEncryptedChoiceReturnCodesMatrix(exponentiatedEncryptedPartialChoiceReturnCodesMatrix_4x4)
							.setExponentiatedEncryptedConfirmationKeysMatrix(exponentiatedEncryptedPartialConfirmationKeysMatrix_4x4_grp_1)
							.setSetupSecretKey(setupSecretKey)
							.build();

			assertNotNull(input);

			assertEquals(exponentiatedEncryptedPartialChoiceReturnCodesMatrix_4x4,
					input.getExponentiatedEncryptedHashedPartialChoiceReturnCodesMatrix());
			assertEquals(exponentiatedEncryptedPartialConfirmationKeysMatrix_4x4_grp_1,
					input.getExponentiatedEncryptedHashedConfirmationKeysMatrix());
			assertEquals(setupSecretKey, input.getSetupSecretKey());
		}
	}

	@Nested
	@DisplayName("CombineEncLongCodeSharesOutput with")
	class CombineEncLongCodeSharesOutputTest {

		private GroupVector<ElGamalMultiRecipientCiphertext, GqGroup> encryptedPreChoiceReturnCodesVector_4;
		private GroupVector<GqElement, GqGroup> preVoteCastReturnCodes4G1;
		private GroupVector<GqElement, GqGroup> preVoteCastReturnCodes3G1;
		private GroupVector<GqElement, GqGroup> preVoteCastReturnCodes4G2;
		private List<String> longVoteCastReturnCodesVector_4;
		private List<String> longVoteCastReturnCodesVector_3;

		@BeforeEach
		void setUp() {
			encryptedPreChoiceReturnCodesVector_4 = GroupVector.of(elGamal.neutralElement(NUMBER_OF_VOTING_OPTIONS, gqGroup),
					elGamal.neutralElement(NUMBER_OF_VOTING_OPTIONS, gqGroup),
					elGamal.neutralElement(NUMBER_OF_VOTING_OPTIONS, gqGroup),
					elGamal.neutralElement(NUMBER_OF_VOTING_OPTIONS, gqGroup));

			preVoteCastReturnCodes4G1 = GroupVector.of(gqGroup.getGenerator(), gqGroup.getGenerator(), gqGroup.getGenerator(),
					gqGroup.getGenerator());

			preVoteCastReturnCodes3G1 = GroupVector.of(gqGroup.getGenerator(), gqGroup.getGenerator(),
					gqGroup.getGenerator());

			preVoteCastReturnCodes4G2 = GroupVector.of(otherGqGroup.getGenerator(), otherGqGroup.getGenerator(),
					otherGqGroup.getGenerator(),
					otherGqGroup.getGenerator());

			longVoteCastReturnCodesVector_4 = Arrays.asList("lVCC1", "lVCC2", "lVCC3", "longVCC4");

			longVoteCastReturnCodesVector_3 = Arrays.asList("lVCC1", "lVCC2", "lVCC3");
		}

		@Test
		@DisplayName("The Vector encrypted pre-Choice Return Codes null")
		void outputWithNullEncryptedPreChoiceReturnCodes() {
			final CombineEncLongCodeSharesOutput.Builder builder = new CombineEncLongCodeSharesOutput.Builder()
					.setEncryptedPreChoiceReturnCodesVector(null)
					.setPreVoteCastReturnCodesVector(preVoteCastReturnCodes4G1)
					.setLongVoteCastReturnCodesAllowList(longVoteCastReturnCodesVector_4);

			assertThrows(NullPointerException.class, builder::build);
		}

		@Test
		@DisplayName("The Vector pre-Choice Return Codes null")
		void outputWithNullPreVoteCastReturnCodes() {
			final CombineEncLongCodeSharesOutput.Builder builder = new CombineEncLongCodeSharesOutput.Builder()
					.setEncryptedPreChoiceReturnCodesVector(encryptedPreChoiceReturnCodesVector_4)
					.setPreVoteCastReturnCodesVector(null)
					.setLongVoteCastReturnCodesAllowList(longVoteCastReturnCodesVector_4);

			assertThrows(NullPointerException.class, builder::build);
		}

		@Test
		@DisplayName("The Vector long Vote Cast Return Codes null")
		void outputWithNullLongVoteCastReturnCodesVector() {
			final CombineEncLongCodeSharesOutput.Builder builder = new CombineEncLongCodeSharesOutput.Builder()
					.setEncryptedPreChoiceReturnCodesVector(encryptedPreChoiceReturnCodesVector_4)
					.setPreVoteCastReturnCodesVector(preVoteCastReturnCodes4G1)
					.setLongVoteCastReturnCodesAllowList(null);
			assertThrows(NullPointerException.class, builder::build);
		}

		@Test
		@DisplayName("The Vector Encrypted Pre-Choice Return Codes empty")
		void outputWithEmptyPreChoiceReturnCodes() {
			final CombineEncLongCodeSharesOutput.Builder builder = new CombineEncLongCodeSharesOutput.Builder()
					.setEncryptedPreChoiceReturnCodesVector(GroupVector.from(Collections.emptyList()))
					.setPreVoteCastReturnCodesVector(preVoteCastReturnCodes4G1)
					.setLongVoteCastReturnCodesAllowList(longVoteCastReturnCodesVector_3);

			final Exception exception = assertThrows(IllegalArgumentException.class, builder::build);

			final String expectedMessage = "The vector of encrypted pre-Choice Return Codes must have more than zero elements.";
			final String actualMessage = exception.getMessage();

			assertEquals(expectedMessage, actualMessage);
		}

		@Test
		@DisplayName("The Vector of pre-Vote Cast Return Codes empty")
		void outputWithEmptyPreVoteCastReturnCodes() {
			final CombineEncLongCodeSharesOutput.Builder builder = new CombineEncLongCodeSharesOutput.Builder()
					.setEncryptedPreChoiceReturnCodesVector(encryptedPreChoiceReturnCodesVector_4)
					.setPreVoteCastReturnCodesVector(GroupVector.from(Collections.emptyList()))
					.setLongVoteCastReturnCodesAllowList(longVoteCastReturnCodesVector_3);

			final Exception exception = assertThrows(IllegalArgumentException.class, builder::build);

			final String expectedMessage = String.format(
					"The vector of pre-Vote Cast Return Codes is of incorrect size [size: expected: %s, actual: %s].",
					encryptedPreChoiceReturnCodesVector_4.size(), 0);
			final String actualMessage = exception.getMessage();

			assertEquals(expectedMessage, actualMessage);
		}

		@Test
		@DisplayName("Vector of long Vote Cast Return Codes empty")
		void outputWithEmptyLongVoteCastReturnCodesVector() {
			final CombineEncLongCodeSharesOutput.Builder builder = new CombineEncLongCodeSharesOutput.Builder()
					.setEncryptedPreChoiceReturnCodesVector(encryptedPreChoiceReturnCodesVector_4)
					.setPreVoteCastReturnCodesVector(preVoteCastReturnCodes4G1)
					.setLongVoteCastReturnCodesAllowList(Collections.emptyList());

			final Exception exception = assertThrows(IllegalArgumentException.class, builder::build);

			final String expectedMessage = String.format(
					"The long Vote Cast Return Codes allow list is of incorrect size [size: expected: %s, "
							+ "actual: %s].", encryptedPreChoiceReturnCodesVector_4.size(), 0);
			final String actualMessage = exception.getMessage();

			assertEquals(expectedMessage, actualMessage);
		}

		@Test
		@DisplayName("Vector of pre-Vote Cast Return Codes is of incorrect size")
		void buildInputWithWrongChoiceReturnCodesMatrixColumnSize() {
			final CombineEncLongCodeSharesOutput.Builder builder = new CombineEncLongCodeSharesOutput.Builder()
					.setEncryptedPreChoiceReturnCodesVector(encryptedPreChoiceReturnCodesVector_4)
					.setPreVoteCastReturnCodesVector(preVoteCastReturnCodes3G1)
					.setLongVoteCastReturnCodesAllowList(longVoteCastReturnCodesVector_4);

			final Exception exception = assertThrows(IllegalArgumentException.class, builder::build);

			final String expectedMessage = String.format(
					"The vector of pre-Vote Cast Return Codes is of incorrect size [size: expected: %s, actual: %s].",
					encryptedPreChoiceReturnCodesVector_4.size(), preVoteCastReturnCodes3G1.size());
			final String actualMessage = exception.getMessage();

			assertEquals(expectedMessage, actualMessage);
		}

		@Test
		@DisplayName("Vector of long Vote Cast Return Codes is of incorrect size")
		void buildInputWithWrongLongVoteCastReturnCodesSize() {
			final CombineEncLongCodeSharesOutput.Builder builder = new CombineEncLongCodeSharesOutput.Builder()
					.setEncryptedPreChoiceReturnCodesVector(encryptedPreChoiceReturnCodesVector_4)
					.setPreVoteCastReturnCodesVector(preVoteCastReturnCodes4G1)
					.setLongVoteCastReturnCodesAllowList(longVoteCastReturnCodesVector_3);

			final Exception exception = assertThrows(IllegalArgumentException.class, builder::build);

			final String expectedMessage = String.format(
					"The long Vote Cast Return Codes allow list is of incorrect size [size: expected: %s, actual: %s].",
					encryptedPreChoiceReturnCodesVector_4.size(), longVoteCastReturnCodesVector_3.size());
			final String actualMessage = exception.getMessage();

			assertEquals(expectedMessage, actualMessage);
		}

		@Test
		@DisplayName("the Vector of pre-Choice Return Codes and the Vector of pre-Vote Cast Return Codes have different Gd groups")
		void buildInputWithWrongConfirmationKeysMatrixRowSize() {
			final CombineEncLongCodeSharesOutput.Builder builder = new CombineEncLongCodeSharesOutput.Builder()
					.setEncryptedPreChoiceReturnCodesVector(encryptedPreChoiceReturnCodesVector_4)
					.setPreVoteCastReturnCodesVector(preVoteCastReturnCodes4G2)
					.setLongVoteCastReturnCodesAllowList(longVoteCastReturnCodesVector_4);

			final Exception exception = assertThrows(IllegalArgumentException.class, builder::build);

			final String expectedMessage = "The vector of encrypted pre-Choice Return Codes and the vector of pre-Vote Cast Return Codes do not have the same group order.";
			final String actualMessage = exception.getMessage();

			assertEquals(expectedMessage, actualMessage);
		}

		@Test
		@DisplayName("output for input with same size and Gd group")
		void outputWithCorrectValues() {
			final CombineEncLongCodeSharesOutput.Builder builder = new CombineEncLongCodeSharesOutput.Builder()
					.setEncryptedPreChoiceReturnCodesVector(encryptedPreChoiceReturnCodesVector_4)
					.setPreVoteCastReturnCodesVector(preVoteCastReturnCodes4G1)
					.setLongVoteCastReturnCodesAllowList(longVoteCastReturnCodesVector_4);

			final CombineEncLongCodeSharesOutput output = builder.build();

			assertEquals(encryptedPreChoiceReturnCodesVector_4, output.getEncryptedPreChoiceReturnCodesVector(),
					"EncryptedPreChoiceReturnCodesVector expected");
			assertEquals(preVoteCastReturnCodes4G1, output.getPreVoteCastReturnCodesVector(),
					"PreVoteCastReturnCodesVector expected");
			assertEquals(longVoteCastReturnCodesVector_4, output.getLongVoteCastReturnCodesAllowList(),
					"LongVoteCastReturnCodesVector expected");
		}
	}
}
