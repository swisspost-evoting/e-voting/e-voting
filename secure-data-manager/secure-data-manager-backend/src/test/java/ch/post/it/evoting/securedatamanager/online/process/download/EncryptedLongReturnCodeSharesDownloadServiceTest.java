/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.online.process.download;

import static java.nio.file.Files.exists;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.IntStream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.evotinglibraries.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.evotinglibraries.domain.returncodes.ControlComponentCodeSharesPayload;
import ch.post.it.evoting.securedatamanager.shared.Constants;
import ch.post.it.evoting.securedatamanager.shared.VotingCardSetServiceTestSpringConfig;
import ch.post.it.evoting.securedatamanager.shared.process.ConfigurationEntityStatusService;
import ch.post.it.evoting.securedatamanager.shared.process.PathResolver;
import ch.post.it.evoting.securedatamanager.shared.process.ResourceNotFoundException;
import ch.post.it.evoting.securedatamanager.shared.process.SetupComponentVerificationDataPayloadFileRepository;
import ch.post.it.evoting.securedatamanager.shared.process.Status;
import ch.post.it.evoting.securedatamanager.shared.process.VotingCardSetRepository;
import ch.post.it.evoting.securedatamanager.shared.process.VotingCardSetServiceTestBase;

import reactor.core.publisher.Flux;

@ExtendWith(MockitoExtension.class)
@SpringJUnitConfig(VotingCardSetServiceTestSpringConfig.class)
class EncryptedLongReturnCodeSharesDownloadServiceTest extends VotingCardSetServiceTestBase {

	private static final String ELECTION_EVENT_ID = "A3D790FD1AC543F9B0A05CA79A20C9E2";
	private static final String VERIFICATION_CARD_SET_ID = "E8C82C55701C4BBDBEF2CD8A675549D1";
	private static final String PRECOMPUTED_VALUES_PATH = "computeTest";
	private static final String VOTE_VERIFICATION_FOLDER = "voteVerification";

	private final ObjectMapper objectMapper = DomainObjectMapper.getNewInstance();
	@Autowired
	private VotingCardSetDownloadService votingCardSetDownloadService;
	@Autowired
	private VotingCardSetRepository votingCardSetRepositoryMock;
	@Autowired
	private PathResolver pathResolver;
	@Autowired
	private ConfigurationEntityStatusService configurationEntityStatusServiceMock;
	@Autowired
	private EncryptedLongReturnCodeSharesDownloadService encryptedLongReturnCodeSharesDownloadServiceMock;
	@Autowired
	private SetupComponentVerificationDataPayloadFileRepository setupComponentVerificationDataPayloadFileRepository;

	@Test
	void download() throws ResourceNotFoundException, URISyntaxException {
		setStatusForVotingCardSetFromRepository(Status.COMPUTED.name(), votingCardSetRepositoryMock);

		final Path basePath = getPathOfFileInResources(Paths.get(PRECOMPUTED_VALUES_PATH));
		final Path verificationCardSetPath = basePath.resolve(ELECTION_EVENT_ID).resolve(VOTE_VERIFICATION_FOLDER).resolve(VERIFICATION_CARD_SET_ID);

		final List<List<ControlComponentCodeSharesPayload>> payloads = IntStream.range(0, 3).mapToObj(
				i -> {
					final Resource contributionsResource = new ClassPathResource(
							String.format("GenEncLongCodeSharesServiceTest/controlComponentCodeSharesPayload.%s.json", i));
					try {
						final List<ControlComponentCodeSharesPayload> contributions = objectMapper.readValue(contributionsResource.getFile(),
								new TypeReference<>() {
								});
						return contributions;
					} catch (final IOException e) {
						throw new UncheckedIOException(e);
					}
				}
		).toList();

		when(pathResolver.resolveVerificationCardSetPath(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID)).thenReturn(verificationCardSetPath);
		when(configurationEntityStatusServiceMock.update(anyString(), anyString(), any())).thenReturn("");
		when(setupComponentVerificationDataPayloadFileRepository.getCount(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID)).thenReturn(3);
		when(encryptedLongReturnCodeSharesDownloadServiceMock.downloadGenEncLongCodeShares(anyString(), anyString(), anyInt()))
				.thenReturn(Flux.fromIterable(payloads).parallel());
		when(votingCardSetRepositoryMock.getVerificationCardSetId(anyString())).thenReturn(VERIFICATION_CARD_SET_ID);

		assertAll(() -> assertDoesNotThrow(() -> votingCardSetDownloadService.download(VOTING_CARD_SET_ID, ELECTION_EVENT_ID)),
				() -> assertTrue(
						exists(verificationCardSetPath.resolve(Constants.CONFIG_FILE_CONTROL_COMPONENT_CODE_SHARES_PAYLOAD + ".0" + Constants.JSON))),
				() -> assertTrue(
						exists(verificationCardSetPath.resolve(Constants.CONFIG_FILE_CONTROL_COMPONENT_CODE_SHARES_PAYLOAD + ".1" + Constants.JSON))),
				() -> assertTrue(exists(verificationCardSetPath.resolve(
						Constants.CONFIG_FILE_CONTROL_COMPONENT_CODE_SHARES_PAYLOAD + ".2" + Constants.JSON))));

	}

}
