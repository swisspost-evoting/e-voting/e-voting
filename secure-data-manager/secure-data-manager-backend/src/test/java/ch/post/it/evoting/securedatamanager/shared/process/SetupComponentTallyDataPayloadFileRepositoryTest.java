/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.shared.process;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.io.TempDir;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.hashing.HashFactory;
import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.evotinglibraries.domain.SerializationUtils;
import ch.post.it.evoting.evotinglibraries.domain.configuration.SetupComponentTallyDataPayload;
import ch.post.it.evoting.evotinglibraries.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.evotinglibraries.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;
import ch.post.it.evoting.securedatamanager.setup.process.SetupPathResolver;

@DisplayName("A SetupComponentTallyDataPayloadFileRepository")
class SetupComponentTallyDataPayloadFileRepositoryTest {
	private static final Random random = RandomFactory.createRandom();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();
	private static final Hash hashService = HashFactory.createHash();

	private static final String NON_EXISTING_ELECTION_EVENT_ID = random.genRandomString(ID_LENGTH, base16Alphabet);
	private static final String EXISTING_ELECTION_EVENT_ID = random.genRandomString(ID_LENGTH, base16Alphabet);
	private static final String VERIFICATION_CARD_SET_ID = random.genRandomString(ID_LENGTH, base16Alphabet);

	private static ObjectMapper objectMapper;
	private static SetupComponentTallyDataPayloadFileRepository setupComponentTallyDataPayloadFileRepository;

	@BeforeAll
	static void setUpAll(
			@TempDir
			final Path tempDir) throws IOException {

		objectMapper = DomainObjectMapper.getNewInstance();

		final PathResolver pathResolver = new SetupPathResolver(tempDir, Path.of(""), Path.of(""), Path.of(""), Path.of(""));

		setupComponentTallyDataPayloadFileRepository = new SetupComponentTallyDataPayloadFileRepository(objectMapper, pathResolver);

		final SetupComponentTallyDataPayloadFileRepository repository = new SetupComponentTallyDataPayloadFileRepository(objectMapper, pathResolver);

		repository.save(validSetupComponentTallyDataPayload(EXISTING_ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID));
	}

	private static SetupComponentTallyDataPayload validSetupComponentTallyDataPayload(final String electionEventId,
			final String verificationCardSetId) {
		final GqGroup encryptionGroup = SerializationUtils.getGqGroup();
		final List<String> verificationCardIds = List.of(
				random.genRandomString(ID_LENGTH, base16Alphabet),
				random.genRandomString(ID_LENGTH, base16Alphabet),
				random.genRandomString(ID_LENGTH, base16Alphabet));
		final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> verificationCardPublicKeys = GroupVector.of(
				SerializationUtils.getPublicKey(),
				SerializationUtils.getPublicKey(),
				SerializationUtils.getPublicKey());

		final String ballotBoxAlias = random.genRandomString(ID_LENGTH, base16Alphabet);
		final SetupComponentTallyDataPayload setupComponentTallyDataPayload = new SetupComponentTallyDataPayload(electionEventId,
				verificationCardSetId, ballotBoxAlias, encryptionGroup, verificationCardIds, verificationCardPublicKeys);

		final byte[] payloadHash = hashService.recursiveHash(setupComponentTallyDataPayload);
		final CryptoPrimitivesSignature signature = new CryptoPrimitivesSignature(payloadHash);
		setupComponentTallyDataPayload.setSignature(signature);

		return setupComponentTallyDataPayload;
	}

	@Nested
	@DisplayName("saving")
	@TestInstance(TestInstance.Lifecycle.PER_CLASS)
	class SaveTest {

		private SetupComponentTallyDataPayloadFileRepository setupComponentTallyDataPayloadFileRepositoryTemp;
		private SetupComponentTallyDataPayload setupComponentTallyDataPayload;

		@BeforeAll
		void setUpAll(
				@TempDir
				final Path tempDir) throws IOException {

			final PathResolver pathResolver = new SetupPathResolver(tempDir, Path.of(""), Path.of(""), Path.of(""), Path.of(""));
			setupComponentTallyDataPayloadFileRepositoryTemp = new SetupComponentTallyDataPayloadFileRepository(objectMapper, pathResolver);
		}

		@BeforeEach
		void setUp() {
			setupComponentTallyDataPayload = validSetupComponentTallyDataPayload(EXISTING_ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID);
		}

		@Test
		@DisplayName("valid setup component tally data payload creates file")
		void save() {
			final Path savedPath = setupComponentTallyDataPayloadFileRepositoryTemp.save(setupComponentTallyDataPayload);

			assertTrue(Files.exists(savedPath));
		}

		@Test
		@DisplayName("null setup component tally data payload throws NullPointerException")
		void saveNullSetupComponentTallyData() {
			assertThrows(NullPointerException.class, () -> setupComponentTallyDataPayloadFileRepositoryTemp.save(null));
		}
	}

	@Nested
	@DisplayName("calling existsById")
	@TestInstance(TestInstance.Lifecycle.PER_CLASS)
	class ExistsByIdTest {

		@Test
		@DisplayName("for existing setup component tally data payload returns true")
		void existingSetupComponentTallyData() {
			assertTrue(setupComponentTallyDataPayloadFileRepository.existsById(EXISTING_ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID));
		}

		@Test
		@DisplayName("with null input throws NullPointerException")
		void nullInput() {
			assertThrows(NullPointerException.class,
					() -> setupComponentTallyDataPayloadFileRepository.existsById(null, VERIFICATION_CARD_SET_ID));
			assertThrows(NullPointerException.class,
					() -> setupComponentTallyDataPayloadFileRepository.existsById(EXISTING_ELECTION_EVENT_ID, null));
		}

		@Test
		@DisplayName("with invalid input throws FailedValidationException")
		void invalidInput() {
			assertThrows(FailedValidationException.class,
					() -> setupComponentTallyDataPayloadFileRepository.existsById("invalidId", VERIFICATION_CARD_SET_ID));
			assertThrows(FailedValidationException.class,
					() -> setupComponentTallyDataPayloadFileRepository.existsById(EXISTING_ELECTION_EVENT_ID, "invalidId"));
		}

		@Test
		@DisplayName("for non existing setup component tally data payload returns false")
		void nonExistingSetupComponentTallyData() {
			assertFalse(setupComponentTallyDataPayloadFileRepository.existsById(NON_EXISTING_ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID));
		}

	}

	@Nested
	@DisplayName("calling findById")
	@TestInstance(TestInstance.Lifecycle.PER_CLASS)
	class FindByIdTest {

		@Test
		@DisplayName("for existing setup component tally data payload returns it")
		void existingSetupComponentTallyData() {
			assertTrue(setupComponentTallyDataPayloadFileRepository.findById(EXISTING_ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID).isPresent());
		}

		@Test
		@DisplayName("for non existing setup component tally data payload return empty optional")
		void nonExistingSetupComponentTallyData() {
			assertFalse(setupComponentTallyDataPayloadFileRepository.findById(NON_EXISTING_ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID).isPresent());
		}

	}

}
