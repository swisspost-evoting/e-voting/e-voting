/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.process.constituteelectoralboard;

import static ch.post.it.evoting.evotinglibraries.domain.ControlComponentConstants.NODE_IDS;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.evotinglibraries.domain.configuration.ControlComponentPublicKeysPayload;
import ch.post.it.evoting.evotinglibraries.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.securedatamanager.setup.process.SetupPathResolver;

@DisplayName("ControlComponentPublicKeysPayloadFileRepository")
class ControlComponentPublicKeysPayloadFileRepositoryTest {
	private static final String ELECTION_EVENT_ID = "314bd34dcf6e4de4b771a92fa3849d3d";
	private static final String WRONG_ELECTION_EVENT_ID = "414bd34dcf6e4de4b771a92fa3849d3d";
	private static final String CORRUPTED_ELECTION_EVENT_ID = "514bd34dcf6e4de4b771a92fa3849d3d";
	private static ControlComponentPublicKeysPayloadFileRepository controlComponentPublicKeysPayloadFileRepository;

	@BeforeAll
	static void setUpAll() throws URISyntaxException, IOException {
		final ObjectMapper objectMapper = DomainObjectMapper.getNewInstance();

		final Path path = Paths.get(
				Objects.requireNonNull(ControlComponentPublicKeysPayloadFileRepositoryTest.class.getResource("/controlComponentPublicKeysTest/"))
						.toURI());
		final Path emptyPath = Path.of("");
		final SetupPathResolver pathResolver = new SetupPathResolver(path, emptyPath, emptyPath, emptyPath, emptyPath);
		controlComponentPublicKeysPayloadFileRepository = new ControlComponentPublicKeysPayloadFileRepository(objectMapper, pathResolver);
	}

	@Nested
	@DisplayName("calling findAllOrderByNodeId")
	class FindAllTest {

		@Test
		@DisplayName("returns all payloads")
		void allPayloads() {
			final List<ControlComponentPublicKeysPayload> publicKeys = controlComponentPublicKeysPayloadFileRepository.findAllOrderByNodeId(
					ELECTION_EVENT_ID);

			assertEquals(publicKeys.size(), NODE_IDS.size());
		}

		@Test
		@DisplayName("for non existing election event returns empty list")
		void nonExistingElectionEvent() {
			final List<ControlComponentPublicKeysPayload> payloads = controlComponentPublicKeysPayloadFileRepository.findAllOrderByNodeId(
					WRONG_ELECTION_EVENT_ID);

			assertEquals(Collections.emptyList(), payloads);
		}

		@Test
		@DisplayName("with one corrupted payload throws UncheckedIOException")
		void corruptedPayload() {
			final UncheckedIOException exception = assertThrows(UncheckedIOException.class,
					() -> controlComponentPublicKeysPayloadFileRepository.findAllOrderByNodeId(CORRUPTED_ELECTION_EVENT_ID));

			assertEquals("Failed to deserialize encryption group.", exception.getMessage());
		}

	}

}
