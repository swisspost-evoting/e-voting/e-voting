/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.protocol.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalFactory.createElGamal;
import static ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ZeroKnowledgeProofFactory.createZeroKnowledgeProof;
import static ch.post.it.evoting.evotinglibraries.domain.ControlComponentConstants.NODE_IDS;
import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.google.common.base.Throwables;

import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.evotinglibraries.domain.election.ControlComponentPublicKeys;
import ch.post.it.evoting.evotinglibraries.domain.election.ElectionEventContext;
import ch.post.it.evoting.evotinglibraries.domain.election.SetupComponentPublicKeys;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.ElectionEventContextPayload;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.generators.ElectionEventContextPayloadGenerator;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.generators.SetupComponentPublicKeysPayloadGenerator;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.preliminaries.proofofcorrectkeygeneration.VerifyCCSchnorrProofsAlgorithm;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventContextPayloadService;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventService;

@DisplayName("genVerCardSetKeys called with")
class GenVerCardSetKeysServiceTest {

	private static final Random random = RandomFactory.createRandom();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();
	private static final ElectionEventService electionEventService = mock(ElectionEventService.class);

	private static GenVerCardSetKeysService genVerCardSetKeysService;
	private static String electionEventId;
	private static List<ControlComponentPublicKeys> controlComponentPublicKeys;

	@BeforeAll
	static void setUpAll() {
		final VerifyCCSchnorrProofsAlgorithm verifyCCSchnorrProofsAlgorithm = spy(new VerifyCCSchnorrProofsAlgorithm(createZeroKnowledgeProof()));
		final GenVerCardSetKeysAlgorithm genVerCardSetKeysAlgorithm = new GenVerCardSetKeysAlgorithm(createElGamal(), verifyCCSchnorrProofsAlgorithm);
		final ElectionEventContextPayloadService electionEventContextPayloadService = mock(ElectionEventContextPayloadService.class);
		genVerCardSetKeysService = new GenVerCardSetKeysService(genVerCardSetKeysAlgorithm, electionEventService, electionEventContextPayloadService);

		final ElectionEventContextPayloadGenerator electionEventContextPayloadGenerator = new ElectionEventContextPayloadGenerator();
		final ElectionEventContextPayload electionEventContextPayload = electionEventContextPayloadGenerator.generate();
		final ElectionEventContext electionEventContext = electionEventContextPayload.getElectionEventContext();
		electionEventId = electionEventContext.electionEventId();
		final GqGroup encryptionGroup = electionEventContextPayload.getEncryptionGroup();
		final SetupComponentPublicKeys setupComponentPublicKeys = new SetupComponentPublicKeysPayloadGenerator(encryptionGroup)
				.generate(electionEventContext.maximumNumberOfSelections(), electionEventContext.maximumNumberOfWriteInsPlusOne())
				.getSetupComponentPublicKeys();
		controlComponentPublicKeys = setupComponentPublicKeys.combinedControlComponentPublicKeys();

		when(electionEventService.exists(electionEventId)).thenReturn(true);

		when(electionEventContextPayloadService.load(electionEventId)).thenReturn(electionEventContextPayload);

		doReturn(true).when(verifyCCSchnorrProofsAlgorithm).verifyCCSchnorrProofs(any(), any());
	}

	private static Stream<Arguments> provideNullParameters() {
		final List<ControlComponentPublicKeys> controlComponentPublicKeysWithNull = new ArrayList<>(controlComponentPublicKeys);
		controlComponentPublicKeysWithNull.add(null);

		return Stream.of(
				Arguments.of(null, controlComponentPublicKeys),
				Arguments.of(electionEventId, null),
				Arguments.of(electionEventId, controlComponentPublicKeysWithNull)
		);
	}

	@ParameterizedTest
	@MethodSource("provideNullParameters")
	@DisplayName("null parameters throws NullPointerException")
	void genVerCardSetKeysWithNullParametersThrows(final String electionEventId, final List<ControlComponentPublicKeys> controlComponentPublicKeys) {
		assertThrows(NullPointerException.class,
				() -> genVerCardSetKeysService.genVerCardSetKeys(electionEventId, controlComponentPublicKeys));
	}

	@Test
	@DisplayName("invalid election event id throws FailedValidationException")
	void genVerCardSetKeysWithInvalidElectionEventIdThrows() {
		assertThrows(FailedValidationException.class,
				() -> genVerCardSetKeysService.genVerCardSetKeys("InvalidElectionEventId", controlComponentPublicKeys));
	}

	@Test
	@DisplayName("non existent election event id throws IllegalArgumentException")
	void genVerCardSetKeysWithNonExistentElectionEventIdThrows() {
		final String nonExistentElectionEventId = random.genRandomString(ID_LENGTH, base16Alphabet);

		when(electionEventService.exists(nonExistentElectionEventId)).thenReturn(false);

		assertThrows(IllegalArgumentException.class,
				() -> genVerCardSetKeysService.genVerCardSetKeys(nonExistentElectionEventId, controlComponentPublicKeys));
	}

	@Test
	@DisplayName("wrong number of control component public keys throws IllegalArgumentException")
	void genVerCardSetKeysWithWrongNumberOfControlComponentPublicKeysThrows() {
		final List<ControlComponentPublicKeys> tooFewControlComponentPublicKeys = List.of();

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> genVerCardSetKeysService.genVerCardSetKeys(electionEventId, tooFewControlComponentPublicKeys));

		final String expected = String.format("Wrong number of control component public keys. [expected: %s, actual: %s]", NODE_IDS,
				tooFewControlComponentPublicKeys.size());
		assertEquals(expected, Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("valid parameters does not throw")
	void genVerCardSetKeysWithValidParametersDoesNotThrow() {
		assertDoesNotThrow(
				() -> genVerCardSetKeysService.genVerCardSetKeys(electionEventId, controlComponentPublicKeys));
	}

}
