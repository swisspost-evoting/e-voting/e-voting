/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.shared.process.dataexchange;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Scanner;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import ch.post.it.evoting.securedatamanager.shared.process.BallotBoxRepository;
import ch.post.it.evoting.securedatamanager.shared.process.BallotRepository;
import ch.post.it.evoting.securedatamanager.shared.process.BallotTextRepository;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventContextRepository;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventRepository;
import ch.post.it.evoting.securedatamanager.shared.process.ElectoralBoardRepository;
import ch.post.it.evoting.securedatamanager.shared.process.ResourceNotFoundException;
import ch.post.it.evoting.securedatamanager.shared.process.VotingCardSetRepository;
import ch.post.it.evoting.securedatamanager.shared.workflow.WorkflowLogRepository;

@ExtendWith(MockitoExtension.class)
class ImportExportOrientDbServiceTest {

	private static final String EE_ID = "eeid";

	@Mock
	private VotingCardSetRepository votingCardSetRepository;
	@Mock
	private BallotRepository ballotRepository;
	@Mock
	private ElectionEventContextRepository electionEventContextRepository;
	@Mock
	private ElectionEventRepository electionEventRepository;
	@Mock
	private BallotTextRepository ballotTextRepository;
	@Mock
	private BallotBoxRepository ballotBoxRepository;
	@Mock
	private ElectoralBoardRepository electoralBoardRepository;
	@Mock
	private WorkflowLogRepository workflowLogRepository;
	@TempDir
	private Path workingDirectory;

	private String votingCardSetJson;
	private String electionEventJson;
	private String ballotJson;
	private String ballotTextJson;
	private String ballotBoxJson;
	private String electoralBoardJson;
	private String workflowLogJson;
	private ImportExportOrientDbService importExportOrientDbService;

	@BeforeEach
	public void setUp() throws URISyntaxException {
		importExportOrientDbService = spy(new ImportExportOrientDbService(
				electionEventRepository,
				ballotBoxRepository,
				ballotRepository,
				ballotTextRepository,
				votingCardSetRepository,
				electoralBoardRepository,
				electionEventContextRepository,
				workflowLogRepository));
	}

	@Test
	void exportOrientDb() throws IOException, ResourceNotFoundException {

		// given
		when(electionEventRepository.find(anyString())).thenReturn(getElectionEventJson());
		when(ballotRepository.listByElectionEvent(anyString())).thenReturn(getBallotJson());
		when(ballotTextRepository.list(any())).thenReturn(getBallotTextJson());
		when(ballotBoxRepository.listByElectionEvent(any())).thenReturn(getBallotBoxJson());
		when(electoralBoardRepository.listByElectionEvent(anyString())).thenReturn(getElectoralBoardJson());
		when(votingCardSetRepository.listByElectionEvent(anyString())).thenReturn(getVotingCardSetJson());
		when(workflowLogRepository.list()).thenReturn(getWorkflowLogJson());

		// when
		importExportOrientDbService.exportOrientDb(workingDirectory.resolve("db-export.json"), EE_ID);

		// then
		assertTrue(Files.exists(workingDirectory.resolve("db-export.json")));
	}

	/**
	 * Returns a sample voting card set JSON string.
	 */
	private String getVotingCardSetJson() throws IOException {
		if (null == votingCardSetJson) {
			votingCardSetJson = readJson("votingCardSet.json");
		}

		return votingCardSetJson;
	}

	private String getElectionEventJson() throws IOException {
		if (null == electionEventJson) {
			electionEventJson = readJson("electionEventRepository.json");
		}

		return electionEventJson;
	}

	private String getBallotJson() throws IOException {
		if (null == ballotJson) {
			ballotJson = readJson("ballot_result_with_representations.json");
		}

		return ballotJson;
	}

	private String getBallotTextJson() throws IOException {
		if (null == ballotTextJson) {
			ballotTextJson = readJson("ballotText_result.json");
		}

		return ballotTextJson;
	}

	private String getBallotBoxJson() throws IOException {
		if (null == ballotBoxJson) {
			ballotBoxJson = readJson("ballotBox_result.json");
		}

		return ballotBoxJson;
	}

	private String getElectoralBoardJson() throws IOException {
		if (null == electoralBoardJson) {
			electoralBoardJson = readJson("electoralBoard_result.json");
		}

		return electoralBoardJson;
	}

	private String getWorkflowLogJson() throws IOException {
		if (null == workflowLogJson) {
			workflowLogJson = readJson("workflow_result.json");
		}

		return workflowLogJson;
	}

	private String readJson(final String fileName) throws IOException {
		try (final InputStream is = getClass().getResourceAsStream("/" + fileName); final Scanner scanner = new Scanner(is, StandardCharsets.UTF_8)) {
			return scanner.useDelimiter("\\A").next();
		}
	}
}
