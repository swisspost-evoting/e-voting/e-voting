/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.protocol.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.hashing.Argon2Factory.createArgon2;
import static ch.post.it.evoting.cryptoprimitives.hashing.HashFactory.createHash;
import static ch.post.it.evoting.cryptoprimitives.math.BaseEncodingFactory.createBase64;
import static ch.post.it.evoting.cryptoprimitives.symmetric.SymmetricFactory.createSymmetric;
import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.BCK_LENGTH;
import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

import java.security.SignatureException;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.google.common.base.Throwables;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.hashing.Argon2Profile;
import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base64Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.BaseEncodingFactory;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ElGamalGenerator;
import ch.post.it.evoting.domain.ElectionSetupUtils;
import ch.post.it.evoting.domain.InvalidPayloadSignatureException;
import ch.post.it.evoting.domain.configuration.setupvoting.VoterInitialCodes;
import ch.post.it.evoting.domain.configuration.setupvoting.VoterInitialCodesPayload;
import ch.post.it.evoting.evotinglibraries.domain.configuration.SetupComponentTallyDataPayload;
import ch.post.it.evoting.evotinglibraries.domain.election.SetupComponentPublicKeys;
import ch.post.it.evoting.evotinglibraries.domain.election.VerificationCardSetContext;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.ElectionEventContextPayload;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.generators.ElectionEventContextPayloadGenerator;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.generators.SetupComponentPublicKeysPayloadGenerator;
import ch.post.it.evoting.evotinglibraries.domain.signature.Alias;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.preliminaries.votingoptions.GetHashContextAlgorithm;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.preliminaries.votingoptions.PrimesMappingTableAlgorithms;
import ch.post.it.evoting.securedatamanager.setup.process.VerificationCardSecretKey;
import ch.post.it.evoting.securedatamanager.setup.process.VerificationCardSecretKeyPayload;
import ch.post.it.evoting.securedatamanager.setup.process.VerificationCardSecretKeyPayloadService;
import ch.post.it.evoting.securedatamanager.setup.process.VoterInitialCodesPayloadService;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventContextPayloadService;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventService;
import ch.post.it.evoting.securedatamanager.shared.process.SetupComponentTallyDataPayloadService;

@DisplayName("genCredDat called with")
class GenCredDatServiceTest {

	private static final Random random = RandomFactory.createRandom();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();
	private static final ElectionEventService electionEventService = mock(ElectionEventService.class);
	private static final SignatureKeystore<Alias> signatureKeystoreService = mock(SignatureKeystore.class);
	private static final ElectionEventContextPayloadService electionEventContextPayloadService = mock(ElectionEventContextPayloadService.class);
	private static final VerificationCardSecretKeyPayloadService verificationCardSecretKeyPayloadService = mock(
			VerificationCardSecretKeyPayloadService.class);

	private static GenCredDatService genCredDatService;
	private static String electionEventId;
	private static String verificationCardSetId;
	private static String votingCardSetId;
	private static ElGamalMultiRecipientPublicKey electionPublicKey;
	private static ElGamalMultiRecipientPublicKey choiceReturnCodesEncryptionPublicKey;
	private static VerificationCardSecretKeyPayload verificationCardSecretKeyPayload;
	private static ElectionEventContextPayload electionEventContextPayload;

	@BeforeAll
	static void setUpAll() {
		final Hash hash = createHash();
		final GetHashContextAlgorithm getHashContextAlgorithm = new GetHashContextAlgorithm(BaseEncodingFactory.createBase64(), hash,
				new PrimesMappingTableAlgorithms());
		final GenCredDatAlgorithm genCredDatAlgorithm = new GenCredDatAlgorithm(hash, createSymmetric(), createBase64(),
				createArgon2(Argon2Profile.LESS_MEMORY), getHashContextAlgorithm);
		final VoterInitialCodesPayloadService voterInitialCodesPayloadService = mock(VoterInitialCodesPayloadService.class);
		final SetupComponentTallyDataPayloadService setupComponentTallyDataPayloadService = mock(SetupComponentTallyDataPayloadService.class);
		genCredDatService = new GenCredDatService(genCredDatAlgorithm, electionEventService, signatureKeystoreService,
				voterInitialCodesPayloadService, electionEventContextPayloadService, setupComponentTallyDataPayloadService,
				verificationCardSecretKeyPayloadService);

		final ElectionEventContextPayloadGenerator electionEventContextPayloadGenerator = new ElectionEventContextPayloadGenerator();
		electionEventContextPayload = electionEventContextPayloadGenerator.generate();
		electionEventId = electionEventContextPayload.getElectionEventContext().electionEventId();
		final VerificationCardSetContext verificationCardSetContext = electionEventContextPayload.getElectionEventContext()
				.verificationCardSetContexts()
				.getFirst();
		verificationCardSetId = verificationCardSetContext.getVerificationCardSetId();
		votingCardSetId = random.genRandomString(ID_LENGTH, base16Alphabet);

		final GqGroup encryptionGroup = electionEventContextPayload.getEncryptionGroup();
		final SetupComponentPublicKeys setupComponentPublicKeys = new SetupComponentPublicKeysPayloadGenerator(encryptionGroup).generate()
				.getSetupComponentPublicKeys();
		electionPublicKey = setupComponentPublicKeys.electionPublicKey();
		choiceReturnCodesEncryptionPublicKey = setupComponentPublicKeys.choiceReturnCodesEncryptionPublicKey();

		when(electionEventService.exists(electionEventId)).thenReturn(true);

		when(electionEventContextPayloadService.loadPrimesMappingTable(electionEventId, verificationCardSetId))
				.thenReturn(verificationCardSetContext.getPrimesMappingTable());

		final ElGamalGenerator elGamalGenerator = new ElGamalGenerator(encryptionGroup);
		final List<String> verificationCardIds = IntStream.range(0, verificationCardSetContext.getNumberOfVotingCards())
				.mapToObj(i -> random.genRandomString(ID_LENGTH, base16Alphabet))
				.toList();
		final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> verificationCardPublicKeys = IntStream.range(0, verificationCardIds.size())
				.mapToObj(i -> elGamalGenerator.genRandomPublicKey(3))
				.collect(GroupVector.toGroupVector());
		final SetupComponentTallyDataPayload setupComponentTallyDataPayload = new SetupComponentTallyDataPayload(electionEventId,
				verificationCardSetId, "ballot box default title", encryptionGroup, verificationCardIds, verificationCardPublicKeys);
		when(setupComponentTallyDataPayloadService.load(electionEventId, verificationCardSetId)).thenReturn(setupComponentTallyDataPayload);

		final List<VerificationCardSecretKey> verificationCardSecretKeys = verificationCardIds.stream()
				.map(verificationCardId -> new VerificationCardSecretKey(verificationCardId, elGamalGenerator.genRandomPrivateKey(3)))
				.toList();
		verificationCardSecretKeyPayload = new VerificationCardSecretKeyPayload(encryptionGroup, electionEventId, verificationCardSetId,
				verificationCardSecretKeys);

		final List<VoterInitialCodes> voterInitialCodes = verificationCardIds.stream()
				.map(ignored -> generateVoterInitialCodes())
				.toList();
		final VoterInitialCodesPayload voterInitialCodesPayload = new VoterInitialCodesPayload(electionEventId, verificationCardSetId,
				voterInitialCodes);
		when(voterInitialCodesPayloadService.load(electionEventId, votingCardSetId)).thenReturn(voterInitialCodesPayload);
	}

	@BeforeEach
	void setUp() throws SignatureException {
		when(signatureKeystoreService.verifySignature(any(), any(), any(), any())).thenReturn(true);
		when(electionEventContextPayloadService.load(electionEventId)).thenReturn(electionEventContextPayload);
		when(verificationCardSecretKeyPayloadService.load(electionEventId, verificationCardSetId)).thenReturn(verificationCardSecretKeyPayload);
	}

	@AfterEach
	void tearDown() {
		reset(signatureKeystoreService);
	}

	private static Stream<Arguments> provideNullParameters() {
		return Stream.of(
				Arguments.of(null, verificationCardSetId, votingCardSetId, electionPublicKey, choiceReturnCodesEncryptionPublicKey),
				Arguments.of(electionEventId, null, votingCardSetId, electionPublicKey, choiceReturnCodesEncryptionPublicKey),
				Arguments.of(electionEventId, verificationCardSetId, null, electionPublicKey, choiceReturnCodesEncryptionPublicKey),
				Arguments.of(electionEventId, verificationCardSetId, votingCardSetId, null, choiceReturnCodesEncryptionPublicKey),
				Arguments.of(electionEventId, verificationCardSetId, votingCardSetId, electionPublicKey, null)
		);
	}

	@ParameterizedTest
	@MethodSource("provideNullParameters")
	@DisplayName("null parameters throws NullPointerException")
	void genCredDatWithNullParametersThrows(final String electionEventId, final String verificationCardSetId, final String votingCardSetId,
			final ElGamalMultiRecipientPublicKey electionPublicKey, final ElGamalMultiRecipientPublicKey choiceReturnCodesEncryptionPublicKey) {
		assertThrows(NullPointerException.class,
				() -> genCredDatService.genCredDat(electionEventId, verificationCardSetId, votingCardSetId, electionPublicKey,
						choiceReturnCodesEncryptionPublicKey));
	}

	@Test
	@DisplayName("invalid election event id throws FailedValidationException")
	void genCredDatWithInvalidElectionEventIdThrows() {
		assertThrows(FailedValidationException.class,
				() -> genCredDatService.genCredDat("InvalidElectionEventId", verificationCardSetId, votingCardSetId, electionPublicKey,
						choiceReturnCodesEncryptionPublicKey));
	}

	@Test
	@DisplayName("invalid verification card set id throws FailedValidationException")
	void genCredDatWithInvalidVerificationCardSetIdThrows() {
		assertThrows(FailedValidationException.class,
				() -> genCredDatService.genCredDat(electionEventId, "InvalidVerificationCardSetId", votingCardSetId, electionPublicKey,
						choiceReturnCodesEncryptionPublicKey));
	}

	@Test
	@DisplayName("invalid voting card set id throws FailedValidationException")
	void genCredDatWithInvalidVotingCardSetIdThrows() {
		assertThrows(FailedValidationException.class,
				() -> genCredDatService.genCredDat(electionEventId, verificationCardSetId, "InvalidVotingCardSetId", electionPublicKey,
						choiceReturnCodesEncryptionPublicKey));
	}

	@Test
	@DisplayName("non existent election event id throws IllegalArgumentException")
	void genCredDatWithNonExistentElectionEventIdThrows() {
		final String nonExistentElectionEventId = random.genRandomString(ID_LENGTH, base16Alphabet);

		when(electionEventService.exists(nonExistentElectionEventId)).thenReturn(false);

		assertThrows(IllegalArgumentException.class,
				() -> genCredDatService.genCredDat(nonExistentElectionEventId, verificationCardSetId, votingCardSetId, electionPublicKey,
						choiceReturnCodesEncryptionPublicKey));
	}

	@Test
	@DisplayName("non consistent encryption group throws IllegalArgumentException")
	void genCredDatWithNonConsistentEncryptionGroupThrows() {
		final ElGamalMultiRecipientPublicKey differentGroupElectionPublicKey = mock(ElGamalMultiRecipientPublicKey.class);

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> genCredDatService.genCredDat(electionEventId, verificationCardSetId, votingCardSetId, differentGroupElectionPublicKey,
						choiceReturnCodesEncryptionPublicKey));

		final String expected = "The choice return codes encryption public key and the election public key must have the same group";
		assertEquals(expected, Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("election event context payload with null signature throws IllegalStateException")
	void genCredDatWithElectionEventContextPayloadWithoutSignatureThrows() {
		final ElectionEventContextPayload electionEventContextPayloadWithoutSignature = new ElectionEventContextPayload(
				electionEventContextPayload.getEncryptionGroup(),
				electionEventContextPayload.getSeed(),
				electionEventContextPayload.getSmallPrimes(),
				electionEventContextPayload.getElectionEventContext()
		);
		final String otherElectionEventId = electionEventContextPayloadWithoutSignature.getElectionEventContext().electionEventId();
		when(electionEventService.exists(otherElectionEventId)).thenReturn(true);

		when(electionEventContextPayloadService.load(otherElectionEventId)).thenReturn(electionEventContextPayloadWithoutSignature);

		final IllegalStateException exception = assertThrows(IllegalStateException.class,
				() -> genCredDatService.genCredDat(otherElectionEventId, verificationCardSetId, votingCardSetId, electionPublicKey,
						choiceReturnCodesEncryptionPublicKey));

		final String expected = String.format("The signature of the election event context payload is null. [electionEventId: %s]",
				otherElectionEventId);
		assertEquals(expected, Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("error verifying election event context payload signature throws IllegalStateException")
	void genCredDatWithErrorVerifyingElectionEventContextPayloadSignatureThrows() throws SignatureException {
		when(signatureKeystoreService.verifySignature(any(), any(), any(), any())).thenThrow(SignatureException.class);

		final IllegalStateException exception = assertThrows(IllegalStateException.class,
				() -> genCredDatService.genCredDat(electionEventId, verificationCardSetId, votingCardSetId, electionPublicKey,
						choiceReturnCodesEncryptionPublicKey));

		final String expected = String.format("Could not verify the signature of the election event context payload. [electionEventId: %s]",
				electionEventId);
		assertEquals(expected, Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("invalid election event context payload signature throws InvalidPayloadSignatureException")
	void genCredDatWithInvalidElectionEventContextPayloadSignatureThrows() throws SignatureException {
		when(signatureKeystoreService.verifySignature(any(), any(), any(), any())).thenReturn(false);

		final InvalidPayloadSignatureException exception = assertThrows(InvalidPayloadSignatureException.class,
				() -> genCredDatService.genCredDat(electionEventId, verificationCardSetId, votingCardSetId, electionPublicKey,
						choiceReturnCodesEncryptionPublicKey));

		final String expected = String.format("Signature of payload %s is invalid. [electionEventId: %s]",
				ElectionEventContextPayload.class.getSimpleName(), electionEventId);
		assertEquals(expected, Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("verification card secret key payload and setup component tally data payload with different verification card ids throws IllegalStateException")
	void genCredDatWithInputWithDifferentVerificationCardIdsThrows() {
		final List<VerificationCardSecretKey> otherVerificationCardIdsKeys = verificationCardSecretKeyPayload.verificationCardSecretKeys().stream()
				.map(verificationCardSecretKey -> new VerificationCardSecretKey(random.genRandomString(ID_LENGTH, base16Alphabet),
						verificationCardSecretKey.privateKey()))
				.toList();
		final VerificationCardSecretKeyPayload otherVerificationCardIdsPayload = new VerificationCardSecretKeyPayload(
				verificationCardSecretKeyPayload.encryptionGroup(), electionEventId, verificationCardSetId, otherVerificationCardIdsKeys);

		when(verificationCardSecretKeyPayloadService.load(electionEventId, verificationCardSetId)).thenReturn(otherVerificationCardIdsPayload);

		final IllegalStateException exception = assertThrows(IllegalStateException.class,
				() -> genCredDatService.genCredDat(electionEventId, verificationCardSetId, votingCardSetId, electionPublicKey,
						choiceReturnCodesEncryptionPublicKey));

		final String expected = "The SetupComponentTallyDataPayload and VerificationCardSecretKeyPayload must have the same verification card ids.";
		assertEquals(expected, Throwables.getRootCause(exception).getMessage());
	}

	private static VoterInitialCodes generateVoterInitialCodes() {
		final Alphabet base64Alphabet = Base64Alphabet.getInstance();
		final String voterIdentification = random.genRandomString(50, base64Alphabet);
		final String votingCardId = random.genRandomString(ID_LENGTH, base16Alphabet);
		final String verificationCardId = random.genRandomString(ID_LENGTH, base16Alphabet);
		final String startVotingKey = ElectionSetupUtils.genStartVotingKey();
		final String extendedAuthenticationFactor = String.join("", random.genUniqueDecimalStrings(4, 2));
		final String ballotCastingKey = random.genUniqueDecimalStrings(BCK_LENGTH, 1).get(0);
		return new VoterInitialCodes(voterIdentification, votingCardId, verificationCardId, startVotingKey, extendedAuthenticationFactor,
				ballotCastingKey);
	}

}
