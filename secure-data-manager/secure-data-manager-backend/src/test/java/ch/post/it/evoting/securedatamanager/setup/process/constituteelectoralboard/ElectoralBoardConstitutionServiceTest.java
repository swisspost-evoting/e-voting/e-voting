/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.process.constituteelectoralboard;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.CharBuffer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.SignatureException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.junit.jupiter.MockitoExtension;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalFactory;
import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.hashing.HashFactory;
import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.BaseEncodingFactory;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.math.ZqElement;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.SchnorrProof;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ZeroKnowledgeProof;
import ch.post.it.evoting.evotinglibraries.domain.configuration.ControlComponentPublicKeysPayload;
import ch.post.it.evoting.evotinglibraries.domain.election.ControlComponentPublicKeys;
import ch.post.it.evoting.evotinglibraries.domain.election.PrimesMappingTable;
import ch.post.it.evoting.evotinglibraries.domain.election.generators.PrimesMappingTableGenerator;
import ch.post.it.evoting.evotinglibraries.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.ElectionEventContextPayload;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.generators.ElectionEventContextPayloadGenerator;
import ch.post.it.evoting.evotinglibraries.domain.signature.Alias;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.preliminaries.electioneventcontext.GetHashElectionEventContextAlgorithm;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.preliminaries.proofofcorrectkeygeneration.VerifyCCSchnorrProofsAlgorithm;
import ch.post.it.evoting.securedatamanager.setup.process.SetupPathResolver;
import ch.post.it.evoting.securedatamanager.setup.protocol.configuration.setuptally.SetupTallyEBAlgorithm;
import ch.post.it.evoting.securedatamanager.setup.protocol.configuration.setuptally.SetupTallyEBService;
import ch.post.it.evoting.securedatamanager.setup.protocol.configuration.setupvoting.GenVerCardSetKeysAlgorithm;
import ch.post.it.evoting.securedatamanager.setup.protocol.configuration.setupvoting.GenVerCardSetKeysService;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventContextPayloadService;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventService;
import ch.post.it.evoting.securedatamanager.shared.process.ElectoralBoardHashesPayloadFileRepository;
import ch.post.it.evoting.securedatamanager.shared.process.ElectoralBoardHashesPayloadService;
import ch.post.it.evoting.securedatamanager.shared.process.PathResolver;
import ch.post.it.evoting.securedatamanager.shared.process.SetupComponentPublicKeysPayloadFileRepository;
import ch.post.it.evoting.securedatamanager.shared.process.SetupComponentPublicKeysPayloadService;

@DisplayName("An ElectoralBoardConstitutionService")
@ExtendWith(MockitoExtension.class)
class ElectoralBoardConstitutionServiceTest {

	private static final Random random = RandomFactory.createRandom();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();

	private static final String ELECTION_EVENT_ID = random.genRandomString(ID_LENGTH, base16Alphabet);
	private static final String VERIFICATION_CARD_SET_ID = random.genRandomString(ID_LENGTH, base16Alphabet);

	private static final String CONTROL_COMPONENT_PUBLIC_KEYS_PAYLOAD_1 =
			ElectoralBoardConstitutionServiceTest.class.getSimpleName() + "/controlComponentPublicKeysPayload.1.json";
	private static final String CONTROL_COMPONENT_PUBLIC_KEYS_PAYLOAD_2 =
			ElectoralBoardConstitutionServiceTest.class.getSimpleName() + "/controlComponentPublicKeysPayload.2.json";
	private static final String CONTROL_COMPONENT_PUBLIC_KEYS_PAYLOAD_3 =
			ElectoralBoardConstitutionServiceTest.class.getSimpleName() + "/controlComponentPublicKeysPayload.3.json";
	private static final String CONTROL_COMPONENT_PUBLIC_KEYS_PAYLOAD_4 =
			ElectoralBoardConstitutionServiceTest.class.getSimpleName() + "/controlComponentPublicKeysPayload.4.json";
	private static final List<char[]> PASSWORDS = List.of("Password_ElectoralBoard1".toCharArray(), "Password_ElectoralBoard2".toCharArray());
	private static final List<byte[]> HASHES = PASSWORDS.stream().map(pw -> CharBuffer.wrap(pw).toString().getBytes(StandardCharsets.UTF_8)).toList();

	private static PathResolver pathResolver;
	private static ElectoralBoardConstitutionService electoralBoardConstitutionService;

	@BeforeAll
	static void setUp(
			@TempDir
			final Path tempDir) throws IOException, URISyntaxException, SignatureException {
		final ObjectMapper objectMapper = DomainObjectMapper.getNewInstance();

		final Path controlComponentPublicKeysPayload1 =
				Paths.get(Objects.requireNonNull(
						ElectoralBoardConstitutionServiceTest.class.getClassLoader().getResource(CONTROL_COMPONENT_PUBLIC_KEYS_PAYLOAD_1)).toURI());
		final Path controlComponentPublicKeysPayload2 =
				Paths.get(Objects.requireNonNull(
						ElectoralBoardConstitutionServiceTest.class.getClassLoader().getResource(CONTROL_COMPONENT_PUBLIC_KEYS_PAYLOAD_2)).toURI());
		final Path controlComponentPublicKeysPayload3 =
				Paths.get(Objects.requireNonNull(
						ElectoralBoardConstitutionServiceTest.class.getClassLoader().getResource(CONTROL_COMPONENT_PUBLIC_KEYS_PAYLOAD_3)).toURI());
		final Path controlComponentPublicKeysPayload4 =
				Paths.get(Objects.requireNonNull(
						ElectoralBoardConstitutionServiceTest.class.getClassLoader().getResource(CONTROL_COMPONENT_PUBLIC_KEYS_PAYLOAD_4)).toURI());

		final List<ControlComponentPublicKeysPayload> controlComponentPublicKeysPayloadList = Arrays.asList(
				objectMapper.readValue(controlComponentPublicKeysPayload1.toFile(), ControlComponentPublicKeysPayload.class),
				objectMapper.readValue(controlComponentPublicKeysPayload2.toFile(), ControlComponentPublicKeysPayload.class),
				objectMapper.readValue(controlComponentPublicKeysPayload3.toFile(), ControlComponentPublicKeysPayload.class),
				objectMapper.readValue(controlComponentPublicKeysPayload4.toFile(), ControlComponentPublicKeysPayload.class));

		final List<ControlComponentPublicKeys> controlComponentPublicKeysList = controlComponentPublicKeysPayloadList.stream()
				.map(ControlComponentPublicKeysPayload::getControlComponentPublicKeys)
				.toList();

		final ControlComponentPublicKeysConfigService controlComponentPublicKeysConfigServiceMock = mock(
				ControlComponentPublicKeysConfigService.class);
		when(controlComponentPublicKeysConfigServiceMock.loadOrderByNodeId(ELECTION_EVENT_ID)).thenReturn(controlComponentPublicKeysList);

		final ElectionEventService electionEventServiceMock = mock(ElectionEventService.class);
		when(electionEventServiceMock.getDateFrom(any())).thenReturn(LocalDateTime.now());
		when(electionEventServiceMock.getDateTo(any())).thenReturn(LocalDateTime.now());
		when(electionEventServiceMock.exists(ELECTION_EVENT_ID)).thenReturn(true);

		final ZeroKnowledgeProof zeroKnowledgeProofServiceMock = mock(ZeroKnowledgeProof.class);
		final ZqGroup zqGroup = controlComponentPublicKeysList.getFirst().ccmjSchnorrProofs().getGroup();
		when(zeroKnowledgeProofServiceMock.genSchnorrProof(any(), any(), anyList())).thenReturn(
				new SchnorrProof(ZqElement.create(3, zqGroup), ZqElement.create(2, zqGroup)));
		when(zeroKnowledgeProofServiceMock.verifySchnorrProof(any(), any(), any())).thenReturn(true);

		pathResolver = new SetupPathResolver(tempDir, Path.of(""), Path.of(""), Path.of(""), Path.of(""));

		final SetupComponentPublicKeysPayloadFileRepository setupComponentPublicKeysPayloadFileRepository = new SetupComponentPublicKeysPayloadFileRepository(
				objectMapper, pathResolver);

		final ElectoralBoardHashesPayloadFileRepository electoralBoardHashesPayloadFileRepository = new ElectoralBoardHashesPayloadFileRepository(
				objectMapper, pathResolver);

		final ElectionEventContextPayloadService electionEventContextPayloadService = mock(ElectionEventContextPayloadService.class);
		final GqGroup encryptionGroup = controlComponentPublicKeysList.getFirst().ccmjElectionPublicKey().getGroup();
		final ElectionEventContextPayloadGenerator electionEventContextPayloadGenerator = new ElectionEventContextPayloadGenerator(encryptionGroup);
		final ElectionEventContextPayload electionEventContextPayload = electionEventContextPayloadGenerator.generate(
				controlComponentPublicKeysList.getFirst().ccrjChoiceReturnCodesEncryptionPublicKey().size(),
				controlComponentPublicKeysList.getFirst().ccmjElectionPublicKey().size());

		final SetupComponentPublicKeysPayloadService setupComponentPublicKeysPayloadService = new SetupComponentPublicKeysPayloadService(
				setupComponentPublicKeysPayloadFileRepository);

		final ElectoralBoardHashesPayloadService electoralBoardHashesPayloadService = new ElectoralBoardHashesPayloadService(
				electoralBoardHashesPayloadFileRepository);

		final PrimesMappingTableGenerator primesMappingTableGenerator = new PrimesMappingTableGenerator(encryptionGroup);
		final PrimesMappingTable primesMappingTable = primesMappingTableGenerator.generate(1);
		when(electionEventContextPayloadService.loadPrimesMappingTable(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID)).thenReturn(primesMappingTable);

		final SignatureKeystore<Alias> signatureKeystoreServiceSdmConfigEBMock = mock(SignatureKeystore.class);
		final ElectoralBoardPersistenceService electoralBoardPersistenceService = new ElectoralBoardPersistenceService(
				setupComponentPublicKeysPayloadService, signatureKeystoreServiceSdmConfigEBMock, electoralBoardHashesPayloadService);
		when(signatureKeystoreServiceSdmConfigEBMock.generateSignature(any(), any())).thenReturn(new byte[] { 1, 2, 3 });

		when(electionEventContextPayloadService.load(ELECTION_EVENT_ID)).thenReturn(electionEventContextPayload);

		final Hash hash = HashFactory.createHash();
		final GetHashElectionEventContextAlgorithm getHashElectionEventContextAlgorithm = new GetHashElectionEventContextAlgorithm(
				BaseEncodingFactory.createBase64(), hash);
		final SetupTallyEBAlgorithm setupTallyEBAlgorithm = new SetupTallyEBAlgorithm(hash, ElGamalFactory.createElGamal(),
				zeroKnowledgeProofServiceMock,
				new VerifyCCSchnorrProofsAlgorithm(zeroKnowledgeProofServiceMock), getHashElectionEventContextAlgorithm);
		final SetupTallyEBService setupTallyEBService = new SetupTallyEBService(electionEventServiceMock, setupTallyEBAlgorithm,
				electionEventContextPayloadService);

		final GenVerCardSetKeysAlgorithm genVerCardSetKeysAlgorithm = new GenVerCardSetKeysAlgorithm(ElGamalFactory.createElGamal(),
				new VerifyCCSchnorrProofsAlgorithm(zeroKnowledgeProofServiceMock));
		final GenVerCardSetKeysService genVerCardSetKeysService = new GenVerCardSetKeysService(genVerCardSetKeysAlgorithm,
				electionEventServiceMock, electionEventContextPayloadService);
		electoralBoardConstitutionService = new ElectoralBoardConstitutionService(controlComponentPublicKeysConfigServiceMock,
				genVerCardSetKeysService, setupTallyEBService, electoralBoardPersistenceService,
				mock(SetupComponentVerificationCardKeystoresPayloadGenerationService.class), electionEventServiceMock);
	}

	@Test
	@DisplayName("constitutes successfully and persists the payloads.")
	void constituteHappyPath() {
		assertDoesNotThrow(() -> electoralBoardConstitutionService.constitute(ELECTION_EVENT_ID, PASSWORDS, HASHES));
		assertTrue(Files.exists(pathResolver.resolveElectionEventPath(ELECTION_EVENT_ID).resolve("setupComponentElectoralBoardHashesPayload.json")));
	}

	@Test
	@DisplayName("provided with non-valid election event id or null arguments throws.")
	void nonValidArguments() {
		assertAll(
				() -> assertThrows(NullPointerException.class, () -> electoralBoardConstitutionService.constitute(null, PASSWORDS, HASHES)),
				() -> assertThrows(FailedValidationException.class, () -> electoralBoardConstitutionService.constitute("invalid", PASSWORDS, HASHES)),
				() -> assertThrows(NullPointerException.class,
						() -> electoralBoardConstitutionService.constitute(ELECTION_EVENT_ID, null, HASHES)),
				() -> assertThrows(NullPointerException.class,
						() -> electoralBoardConstitutionService.constitute(ELECTION_EVENT_ID, PASSWORDS, null))
		);
	}

}
