/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager;

import static org.mockito.Mockito.mock;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.web.client.RestTemplate;

import ch.post.it.evoting.securedatamanager.online.process.download.EncryptedLongReturnCodeSharesDownloadService;
import ch.post.it.evoting.securedatamanager.online.process.download.VotingCardSetDownloadService;
import ch.post.it.evoting.securedatamanager.setup.process.generate.BallotDataGeneratorService;
import ch.post.it.evoting.securedatamanager.setup.process.generate.EncryptedNodeLongReturnCodeSharesService;
import ch.post.it.evoting.securedatamanager.setup.process.generate.VotingCardSetGenerateBallotService;
import ch.post.it.evoting.securedatamanager.shared.process.BallotBoxRepository;
import ch.post.it.evoting.securedatamanager.shared.process.ConfigurationEntityStatusService;
import ch.post.it.evoting.securedatamanager.shared.process.NodeContributionsResponsesFileRepository;
import ch.post.it.evoting.securedatamanager.shared.process.SetupComponentVerificationDataPayloadFileRepository;
import ch.post.it.evoting.securedatamanager.shared.process.VotingCardSetRepository;

/**
 * MVC Configuration
 */
@Configuration
@ComponentScan(basePackages = { "ch.post.it.evoting.securedatamanager.shared" })
@PropertySource("classpath:config/application.properties")
@Profile("test")
public class SdmDatabaseConfig {

	@Value("${sdm.workspace}")
	private String workspace;

	@Bean
	public static PropertySourcesPlaceholderConfigurer propertiesResolver() {
		return new PropertySourcesPlaceholderConfigurer();
	}

	@Bean
	public VotingCardSetDownloadService votingCardSetDownloadService(
			final VotingCardSetRepository votingCardSetRepository,
			final ConfigurationEntityStatusService configurationEntityStatusService,
			final NodeContributionsResponsesFileRepository nodeContributionsResponsesFileRepository,
			final EncryptedLongReturnCodeSharesDownloadService encryptedLongReturnCodeSharesDownloadService,
			final SetupComponentVerificationDataPayloadFileRepository setupComponentVerificationDataPayloadFileRepository) {
		return new VotingCardSetDownloadService(false, votingCardSetRepository, configurationEntityStatusService,
				nodeContributionsResponsesFileRepository, encryptedLongReturnCodeSharesDownloadService,
				setupComponentVerificationDataPayloadFileRepository);
	}

	@Bean
	public VotingCardSetGenerateBallotService votingCardSetGenerateBallotService(
			final BallotBoxRepository ballotBoxRepository,
			final VotingCardSetRepository votingCardSetRepository,
			final BallotDataGeneratorService ballotDataGeneratorService,
			final ConfigurationEntityStatusService configurationEntityStatusService) {
		return new VotingCardSetGenerateBallotService(ballotBoxRepository, votingCardSetRepository, ballotDataGeneratorService,
				configurationEntityStatusService);
	}

	@Bean
	public BallotDataGeneratorService getBallotDataGeneratorService() {
		return mock(BallotDataGeneratorService.class);
	}

	@Bean
	public RestTemplate getRestTemplate() {
		return mock(RestTemplate.class);
	}

	@Bean
	EncryptedNodeLongReturnCodeSharesService encryptedNodeLongCodeSharesService() {
		return mock(EncryptedNodeLongReturnCodeSharesService.class);
	}

	@Bean
	EncryptedLongReturnCodeSharesDownloadService encryptedLongReturnCodeSharesDownloadService() {
		return mock(EncryptedLongReturnCodeSharesDownloadService.class);
	}
}
