/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.protocol.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalFactory.createElGamal;
import static ch.post.it.evoting.cryptoprimitives.hashing.HashFactory.createHash;
import static ch.post.it.evoting.cryptoprimitives.math.BaseEncodingFactory.createBase64;
import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static ch.post.it.evoting.securedatamanager.shared.Constants.TOO_SMALL_CHUNK_SIZE_MESSAGE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.google.common.base.Throwables;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientKeyPair;
import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ElGamalGenerator;
import ch.post.it.evoting.evotinglibraries.domain.election.VerificationCardSetContext;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.ElectionEventContextPayload;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.generators.ElectionEventContextPayloadGenerator;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.preliminaries.votingoptions.PrimesMappingTableAlgorithms;
import ch.post.it.evoting.securedatamanager.setup.process.SetupKeyPairService;
import ch.post.it.evoting.securedatamanager.setup.process.precompute.VerificationCardSet;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventContextPayloadService;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventService;

@DisplayName("genVerDat called with")
class GenVerDatServiceTest {

	private static final Random random = RandomFactory.createRandom();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();
	private static final ElectionEventService electionEventService = mock(ElectionEventService.class);
	private static final SetupKeyPairService setupKeyPairService = mock(SetupKeyPairService.class);

	private static GenVerDatService genVerDatService;
	private static VerificationCardSet verificationCardSet;
	private static int numberOfVotingCardsToGenerate;

	@BeforeAll
	static void setUpAll() {
		final GenVerDatAlgorithm genVerDatAlgorithm = new GenVerDatAlgorithm(createElGamal(), createHash(), random, createBase64(),
				new PrimesMappingTableAlgorithms());
		final ElectionEventContextPayloadService electionEventContextPayloadService = mock(ElectionEventContextPayloadService.class);
		genVerDatService = new GenVerDatService(genVerDatAlgorithm, electionEventService, setupKeyPairService, electionEventContextPayloadService);

		final ElectionEventContextPayloadGenerator electionEventContextPayloadGenerator = new ElectionEventContextPayloadGenerator();
		final ElectionEventContextPayload electionEventContextPayload = electionEventContextPayloadGenerator.generate();
		final String electionEventId = electionEventContextPayload.getElectionEventContext().electionEventId();
		final VerificationCardSetContext verificationCardSetContext = electionEventContextPayload.getElectionEventContext()
				.verificationCardSetContexts()
				.getFirst();
		verificationCardSet = new VerificationCardSet(electionEventId, verificationCardSetContext.getBallotBoxId(),
				random.genRandomString(ID_LENGTH, base16Alphabet), verificationCardSetContext.getVerificationCardSetId());
		numberOfVotingCardsToGenerate = verificationCardSetContext.getNumberOfVotingCards();

		when(electionEventService.exists(electionEventId)).thenReturn(true);

		when(electionEventContextPayloadService.load(electionEventId)).thenReturn(electionEventContextPayload);
		when(electionEventContextPayloadService.loadPrimesMappingTable(electionEventId, verificationCardSetContext.getVerificationCardSetId()))
				.thenReturn(verificationCardSetContext.getPrimesMappingTable());

		final ElGamalGenerator elGamalGenerator = new ElGamalGenerator(electionEventContextPayload.getEncryptionGroup());
		final ElGamalMultiRecipientKeyPair setupKeyPair = elGamalGenerator.genRandomKeyPair(
				electionEventContextPayload.getElectionEventContext().maximumNumberOfVotingOptions());
		when(setupKeyPairService.load(electionEventId)).thenReturn(setupKeyPair);
	}

	@Test
	@DisplayName("null verification card set throws NullPointerException")
	void genVerDatWithNullVerificationCardSetThrows() {
		assertThrows(NullPointerException.class, () -> genVerDatService.genVerDat(null, numberOfVotingCardsToGenerate));
	}

	@Test
	@DisplayName("negative number of voting cards to generate throws IllegalArgumentException")
	void genVerDatWithNegativeNumberOfVotingCardsToGenerateThrows() {
		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> genVerDatService.genVerDat(verificationCardSet, -5));

		final String expected = "The number of voting cards to generate must be positive.";
		assertEquals(expected, Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("negative chunk size throws IllegalArgumentException")
	void genVerDatWithNegativeChunkSizeThrows() {
		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> genVerDatService.genVerDat(verificationCardSet, numberOfVotingCardsToGenerate));

		assertEquals(TOO_SMALL_CHUNK_SIZE_MESSAGE, Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("zero chunk size throws IllegalArgumentException")
	void genVerDatWithZeroChunkSizeThrows() {
		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> genVerDatService.genVerDat(verificationCardSet, numberOfVotingCardsToGenerate));

		assertEquals(TOO_SMALL_CHUNK_SIZE_MESSAGE, Throwables.getRootCause(exception).getMessage());
	}

}
