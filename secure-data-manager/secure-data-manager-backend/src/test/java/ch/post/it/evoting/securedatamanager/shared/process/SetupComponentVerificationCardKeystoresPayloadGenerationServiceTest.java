/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.shared.process;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.SignatureException;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.hashing.HashFactory;
import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.cryptoprimitives.test.tools.TestGroupSetup;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ElGamalGenerator;
import ch.post.it.evoting.domain.ElectionSetupUtils;
import ch.post.it.evoting.domain.InvalidPayloadSignatureException;
import ch.post.it.evoting.domain.configuration.SetupComponentVerificationCardKeystoresPayload;
import ch.post.it.evoting.domain.configuration.setupvoting.VoterInitialCodes;
import ch.post.it.evoting.domain.configuration.setupvoting.VoterInitialCodesPayload;
import ch.post.it.evoting.evotinglibraries.domain.configuration.SetupComponentTallyDataPayload;
import ch.post.it.evoting.evotinglibraries.domain.signature.Alias;
import ch.post.it.evoting.evotinglibraries.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;
import ch.post.it.evoting.securedatamanager.setup.process.constituteelectoralboard.SetupComponentVerificationCardKeystoresPayloadGenerationService;
import ch.post.it.evoting.securedatamanager.setup.protocol.configuration.setupvoting.GenCredDatOutput;
import ch.post.it.evoting.securedatamanager.setup.protocol.configuration.setupvoting.GenCredDatService;

@DisplayName("SetupComponentVerificationCardKeystoresPayloadGenerationService")
class SetupComponentVerificationCardKeystoresPayloadGenerationServiceTest extends TestGroupSetup {

	private static final Random random = RandomFactory.createRandom();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();
	private final int SIZE = 6;
	private final GqGroup gqGroup = GroupTestData.getLargeGqGroup();
	private final ElGamalGenerator elGamalGenerator = new ElGamalGenerator(gqGroup);
	private final ElGamalMultiRecipientPublicKey choiceReturnCodesEncryptionPublicKey = elGamalGenerator.genRandomPublicKey(SIZE);
	private final ElGamalMultiRecipientPublicKey electionPublicKey = elGamalGenerator.genRandomPublicKey(SIZE);
	private final ElGamalMultiRecipientPublicKey electoralBoardPublicKey = elGamalGenerator.genRandomPublicKey(SIZE);
	private final String electionEventId = random.genRandomString(ID_LENGTH, base16Alphabet);
	private final String verificationCardSetId = random.genRandomString(ID_LENGTH, base16Alphabet);
	private final String ballotBoxAlias = random.genRandomString(ID_LENGTH, base16Alphabet);
	private final String votingCardSetId = random.genRandomString(ID_LENGTH, base16Alphabet);
	private final VotingCardSetRepository votingCardSetRepository = mock(VotingCardSetRepository.class);
	private final GenCredDatService genCredDatService = mock(GenCredDatService.class);
	private final ElectionEventService electionEventService = mock(ElectionEventService.class);
	private final SignatureKeystore<Alias> signatureKeystoreService = mock(SignatureKeystore.class);
	private final SetupComponentTallyDataPayloadService setupComponentTallyDataPayloadService = mock(SetupComponentTallyDataPayloadService.class);
	private final SetupComponentVerificationCardKeystoresPayloadService setupComponentVerificationCardKeystoresPayloadService
			= mock(SetupComponentVerificationCardKeystoresPayloadService.class);

	private final SetupComponentVerificationCardKeystoresPayloadGenerationService sut =
			new SetupComponentVerificationCardKeystoresPayloadGenerationService(
					genCredDatService,
					electionEventService,
					votingCardSetRepository,
					signatureKeystoreService,
					setupComponentTallyDataPayloadService,
					setupComponentVerificationCardKeystoresPayloadService);

	@Nested
	@DisplayName("calling generate with")
	class GenerateArgumentsTest {

		@Test
		@DisplayName("an invalid election event id input throws a FailedValidationException.")
		void invalidElectionEventIdArguments() {
			final String invalidElectionEventId = "electionEventId";
			assertThrows(FailedValidationException.class,
					() -> sut.generate(invalidElectionEventId, choiceReturnCodesEncryptionPublicKey, electionPublicKey));
		}

		@Test
		@DisplayName("an election event id not in the database throws an IllegalArgumentException")
		void nonExistingElectionEventIdArgument() {
			final String nonExistingElectionEventId = "0123456789abcdef0123456789abcdef";
			assertThrows(IllegalArgumentException.class,
					() -> sut.generate(nonExistingElectionEventId, choiceReturnCodesEncryptionPublicKey, electionPublicKey));
		}

		@Test
		@DisplayName("any null input throws a NullPointerException.")
		void nullArguments() {
			assertAll(
					() -> assertThrows(NullPointerException.class,
							() -> sut.generate(null, choiceReturnCodesEncryptionPublicKey, electionPublicKey)),
					() -> assertThrows(NullPointerException.class,
							() -> sut.generate(electionEventId, null, electionPublicKey)),
					() -> assertThrows(NullPointerException.class,
							() -> sut.generate(electionEventId, choiceReturnCodesEncryptionPublicKey, null))
			);
		}

		@Test
		@DisplayName("inputs with different groups throws an IllegalArgumentException.")
		void invalidArguments() {
			when(electionEventService.exists(electionEventId)).thenReturn(true);

			final ElGamalMultiRecipientPublicKey choiceReturnCodesEncryptionPublicKeyDifferentGroup = otherGroupElGamalGenerator.genRandomPublicKey(
					SIZE);
			final ElGamalMultiRecipientPublicKey electionPublicKeyDifferentGroup = otherGroupElGamalGenerator.genRandomPublicKey(SIZE);

			assertAll(
					() -> {
						final IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class,
								() -> sut.generate(electionEventId, choiceReturnCodesEncryptionPublicKeyDifferentGroup, electionPublicKey));

						assertEquals("The choice return codes encryption public key and the election public key must have the same group",
								illegalArgumentException.getMessage());
					},
					() -> {
						final IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class,
								() -> sut.generate(electionEventId, choiceReturnCodesEncryptionPublicKey, electionPublicKeyDifferentGroup));

						assertEquals("The choice return codes encryption public key and the election public key must have the same group",
								illegalArgumentException.getMessage());
					}
			);
		}
	}

	@Nested
	@DisplayName("executing generate")
	class GenerateExecutionTest {

		private final List<String> votingCardSetIds = List.of(votingCardSetId);

		@BeforeEach
		void setup() throws SignatureException, IOException {

			final List<String> verificationCardIds = Stream.generate(
							() -> random.genRandomString(ID_LENGTH, base16Alphabet))
					.limit(SIZE)
					.toList();

			final List<ElGamalMultiRecipientPublicKey> verificationCardPublicKeys =
					Stream.generate(() -> elGamalGenerator.genRandomPublicKey(SIZE)).limit(SIZE).toList();

			final SetupComponentTallyDataPayload setupComponentTallyDataPayload = new SetupComponentTallyDataPayload(electionEventId,
					verificationCardSetId, ballotBoxAlias, gqGroup, verificationCardIds, GroupVector.from(verificationCardPublicKeys));

			final byte[] payloadHash = HashFactory.createHash().recursiveHash(setupComponentTallyDataPayload);
			setupComponentTallyDataPayload.setSignature(new CryptoPrimitivesSignature(payloadHash));

			final GenCredDatOutput genCredDatOutput =
					new GenCredDatOutput(Stream.generate(() -> random.genRandomString(SIZE, base16Alphabet)).limit(SIZE).toList());

			// Services mocks:
			when(electionEventService.exists(electionEventId)).thenReturn(true);

			// In generate method:
			when(votingCardSetRepository.findAllVotingCardSetIds(anyString())).thenReturn(votingCardSetIds);

			when(votingCardSetRepository.getVerificationCardSetId(anyString())).thenReturn(verificationCardSetId);

			final VoterInitialCodesPayload voterInitialCodesPayload = mock(VoterInitialCodesPayload.class);
			final VoterInitialCodes voterInitialCodes = mock(VoterInitialCodes.class);
			when(voterInitialCodes.startVotingKey()).thenReturn(ElectionSetupUtils.genStartVotingKey());
			when(voterInitialCodesPayload.voterInitialCodes()).thenReturn(verificationCardIds.stream().map(id -> voterInitialCodes).toList());

			final ClassLoader classLoader = GenerateExecutionTest.class.getClassLoader();

			when(genCredDatService.genCredDat(any(), any(), any(), any(), any())).thenReturn(genCredDatOutput);

			// In createSetupComponentVerificationCardKeystoresPayload method:
			when(signatureKeystoreService.generateSignature(any(), any())).thenReturn("signature".getBytes(StandardCharsets.UTF_8));

			// In loadSetupComponentTallyDataPayload method:
			when(setupComponentTallyDataPayloadService.load(anyString(), anyString())).thenReturn(setupComponentTallyDataPayload);
			when(signatureKeystoreService.verifySignature(any(), any(), any(), any())).thenReturn(true);
		}

		@Test
		@DisplayName("behaves as expected.")
		void happyPath() throws SignatureException {
			final List<SetupComponentVerificationCardKeystoresPayload> payloads =
					assertDoesNotThrow(() -> sut.generate(electionEventId, choiceReturnCodesEncryptionPublicKey, electoralBoardPublicKey));

			final int votingCardSetIdsSize = votingCardSetIds.size();

			verify(votingCardSetRepository, times(votingCardSetIdsSize)).findAllVotingCardSetIds(anyString());
			verify(setupComponentTallyDataPayloadService, times(votingCardSetIdsSize)).load(anyString(), anyString());
			verify(signatureKeystoreService, times(1)).generateSignature(any(SetupComponentVerificationCardKeystoresPayload.class), any());
			verify(signatureKeystoreService, times(votingCardSetIdsSize)).verifySignature(any(), any(SetupComponentTallyDataPayload.class), any(),
					any());
			verify(votingCardSetRepository, times(votingCardSetIdsSize)).getVerificationCardSetId(anyString());
			verify(genCredDatService, times(votingCardSetIdsSize)).genCredDat(any(), any(), any(), any(), any());
			verify(signatureKeystoreService, times(votingCardSetIdsSize)).generateSignature(any(), any());

			final Predicate<SetupComponentVerificationCardKeystoresPayload> validatePayload = payload ->
					electionEventId.equals(payload.getElectionEventId()) && verificationCardSetId.equals(payload.getVerificationCardSetId())
							&& payload.getVerificationCardKeystores() != null;

			assertFalse(payloads.isEmpty());
			assertEquals(votingCardSetIds.size(), payloads.size());
			assertTrue(payloads.stream().allMatch(validatePayload));
		}

		@Test
		@DisplayName("with an empty voting card set found returns an empty list of SetupComponentVerificationCardKeystoresPayload.")
		void givenAnEmptylistOfVotingCardSetIdsThenNodata() throws SignatureException {
			when(votingCardSetRepository.findAllVotingCardSetIds(anyString())).thenReturn(List.of());

			final List<SetupComponentVerificationCardKeystoresPayload> payloads =
					assertDoesNotThrow(() -> sut.generate(electionEventId, choiceReturnCodesEncryptionPublicKey, electoralBoardPublicKey));

			assertTrue(payloads.isEmpty());

			verify(votingCardSetRepository, times(1)).findAllVotingCardSetIds(anyString());

			verify(setupComponentTallyDataPayloadService, times(0)).load(anyString(), anyString());

			verify(votingCardSetRepository, times(0)).getVerificationCardSetId(anyString());
			verify(signatureKeystoreService, times(0)).generateSignature(any(), any());

			verify(genCredDatService, times(0)).genCredDat(any(), any(), any(), any(), any());
		}

		@Test
		@DisplayName("with an invalid signature of setupComponentTallyDataPayload throws an InvalidPayloadSignatureException.")
		void setupComponentTallyDataPayloadWithInvalidSignature() throws SignatureException {
			when(signatureKeystoreService.verifySignature(any(), any(), any(), any())).thenReturn(false);

			assertThrows(InvalidPayloadSignatureException.class,
					() -> sut.generate(electionEventId, choiceReturnCodesEncryptionPublicKey, electoralBoardPublicKey));
		}

	}
}
