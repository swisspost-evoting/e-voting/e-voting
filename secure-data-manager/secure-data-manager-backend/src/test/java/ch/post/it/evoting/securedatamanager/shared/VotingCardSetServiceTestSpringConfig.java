/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.shared;

import static org.mockito.Mockito.mock;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.evotinglibraries.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.securedatamanager.online.process.download.EncryptedLongReturnCodeSharesDownloadService;
import ch.post.it.evoting.securedatamanager.online.process.download.VotingCardSetDownloadService;
import ch.post.it.evoting.securedatamanager.setup.process.generate.BallotDataGeneratorService;
import ch.post.it.evoting.securedatamanager.setup.process.generate.EncryptedNodeLongReturnCodeSharesService;
import ch.post.it.evoting.securedatamanager.setup.process.generate.ReturnCodesPayloadsGenerateService;
import ch.post.it.evoting.securedatamanager.setup.process.generate.VotingCardSetGenerateBallotService;
import ch.post.it.evoting.securedatamanager.setup.process.preconfigure.ElectionEventContextPersistenceService;
import ch.post.it.evoting.securedatamanager.shared.process.BallotBoxRepository;
import ch.post.it.evoting.securedatamanager.shared.process.BallotRepository;
import ch.post.it.evoting.securedatamanager.shared.process.ConfigurationEntityStatusService;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventRepository;
import ch.post.it.evoting.securedatamanager.shared.process.ElectoralBoardRepository;
import ch.post.it.evoting.securedatamanager.shared.process.NodeContributionsResponsesFileRepository;
import ch.post.it.evoting.securedatamanager.shared.process.PathResolver;
import ch.post.it.evoting.securedatamanager.shared.process.SetupComponentCMTablePayloadService;
import ch.post.it.evoting.securedatamanager.shared.process.SetupComponentVerificationDataPayloadFileRepository;
import ch.post.it.evoting.securedatamanager.shared.process.VotingCardSetRepository;

@Configuration
public class VotingCardSetServiceTestSpringConfig {

	@Bean
	public ConfigurationEntityStatusService configurationEntityStatusService() {
		return mock(ConfigurationEntityStatusService.class);
	}

	@Bean
	public PathResolver pathResolver() {
		return mock(PathResolver.class);
	}

	@Bean
	public VotingCardSetRepository votingCardSetRepository() {
		return mock(VotingCardSetRepository.class);
	}

	@Bean
	public ElectionEventRepository electionEventRepository() {
		return mock(ElectionEventRepository.class);
	}

	@Bean
	public ObjectMapper objectMapper() {
		return DomainObjectMapper.getNewInstance();
	}

	@Bean
	public BallotRepository ballotRepository() {
		return mock(BallotRepository.class);
	}

	@Bean
	public BallotBoxRepository ballotBoxRepository() {
		return mock(BallotBoxRepository.class);
	}

	@Bean
	public BallotDataGeneratorService ballotDataGeneratorService() {
		return mock(BallotDataGeneratorService.class);
	}

	@Bean
	public EncryptedLongReturnCodeSharesDownloadService votingCardSetChoiceCodesService() {
		return mock(EncryptedLongReturnCodeSharesDownloadService.class);
	}

	@Bean
	public SetupComponentVerificationDataPayloadFileRepository setupComponentVerificationDataPayloadFileSystemRepository() {
		return mock(SetupComponentVerificationDataPayloadFileRepository.class);
	}

	@Bean
	public ElectoralBoardRepository electoralAuthorityRepository() {
		return mock(ElectoralBoardRepository.class);
	}

	@Bean
	public EncryptedNodeLongReturnCodeSharesService encryptedNodeLongCodeSharesService() {
		return mock(EncryptedNodeLongReturnCodeSharesService.class);
	}

	@Bean
	public NodeContributionsResponsesFileRepository nodeContributionsResponsesFileRepository(final ObjectMapper objectMapper,
			final PathResolver pathResolver) {
		return new NodeContributionsResponsesFileRepository(objectMapper, pathResolver);
	}

	@Bean
	public VotingCardSetDownloadService votingCardSetDownloadService(
			final VotingCardSetRepository votingCardSetRepository,
			final ConfigurationEntityStatusService configurationEntityStatusService,
			final NodeContributionsResponsesFileRepository nodeContributionsResponsesFileRepository,
			final EncryptedLongReturnCodeSharesDownloadService encryptedLongReturnCodeSharesDownloadService,
			final SetupComponentVerificationDataPayloadFileRepository setupComponentVerificationDataPayloadFileRepository) {
		return new VotingCardSetDownloadService(true, votingCardSetRepository, configurationEntityStatusService,
				nodeContributionsResponsesFileRepository, encryptedLongReturnCodeSharesDownloadService,
				setupComponentVerificationDataPayloadFileRepository);
	}

	@Bean
	public VotingCardSetGenerateBallotService votingCardSetGenerateBallotService(
			final BallotBoxRepository ballotBoxRepository,
			final VotingCardSetRepository votingCardSetRepository,
			final BallotDataGeneratorService ballotDataGeneratorService,
			final ConfigurationEntityStatusService configurationEntityStatusService) {
		return new VotingCardSetGenerateBallotService(ballotBoxRepository, votingCardSetRepository, ballotDataGeneratorService,
				configurationEntityStatusService);
	}

	@Bean
	public ElectionEventContextPersistenceService electionEventContextPersistenceService() {
		return mock(ElectionEventContextPersistenceService.class);
	}

	@Bean
	public SetupComponentCMTablePayloadService returnCodesMappingTablePayloadService() {
		return mock(SetupComponentCMTablePayloadService.class);
	}

	@Bean
	public ReturnCodesPayloadsGenerateService returnCodesMappingTablePayloadGenerationService() {
		return mock(ReturnCodesPayloadsGenerateService.class);
	}

}
