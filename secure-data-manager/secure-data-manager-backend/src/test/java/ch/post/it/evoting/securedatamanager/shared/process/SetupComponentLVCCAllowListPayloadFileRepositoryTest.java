/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.shared.process;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static ch.post.it.evoting.securedatamanager.shared.process.SetupComponentLVCCAllowListPayloadFileRepository.PAYLOAD_FILE_NAME;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Base64;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.io.TempDir;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.hashing.HashFactory;
import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.domain.configuration.setupvoting.SetupComponentLVCCAllowListPayload;
import ch.post.it.evoting.evotinglibraries.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.evotinglibraries.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;
import ch.post.it.evoting.securedatamanager.setup.process.SetupPathResolver;

@DisplayName("A SetupComponentLVCCAllowListPayloadFileRepository")
class SetupComponentLVCCAllowListPayloadFileRepositoryTest {

	private static final Random random = RandomFactory.createRandom();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();
	private static final String ELECTION_EVENT_ID = random.genRandomString(ID_LENGTH, base16Alphabet);
	private static final String VERIFICATION_CARD_SET_ID = random.genRandomString(ID_LENGTH, base16Alphabet);
	private static final String CORRUPTED_VERIFICATION_CARD_SET_ID = random.genRandomString(ID_LENGTH, base16Alphabet);
	private static final String NON_EXISTING_VERIFICATION_CARD_SET_ID = random.genRandomString(ID_LENGTH, base16Alphabet);

	private static final Hash hash = HashFactory.createHash();

	private static PathResolver pathResolver;
	private static ObjectMapper objectMapper;
	private static SetupComponentLVCCAllowListPayloadFileRepository setupComponentLVCCAllowListPayloadFileRepository;

	@BeforeAll
	static void setUpAll(
			@TempDir
			final Path tempDir) throws IOException {
		objectMapper = DomainObjectMapper.getNewInstance();

		pathResolver = new SetupPathResolver(tempDir, Path.of(""), Path.of(""), Path.of(""), Path.of(""));

		setupComponentLVCCAllowListPayloadFileRepository = new SetupComponentLVCCAllowListPayloadFileRepository(objectMapper, pathResolver);

		final SetupComponentLVCCAllowListPayloadFileRepository repository =
				new SetupComponentLVCCAllowListPayloadFileRepository(objectMapper, pathResolver);

		repository.save(validLongVoteCastReturnCodesAllowListPayload());
		repository.save(unsignedLongVoteCastReturnCodesAllowListPayload());
	}

	private static SetupComponentLVCCAllowListPayload validLongVoteCastReturnCodesAllowListPayload() {
		final String lVCC1 = Base64.getEncoder().encodeToString(new byte[] { 26 });
		final String lVCC2 = Base64.getEncoder().encodeToString(new byte[] { 56 });
		final String lVCC3 = Base64.getEncoder().encodeToString(new byte[] { 3 });
		final SetupComponentLVCCAllowListPayload setupComponentLVCCAllowListPayload =
				new SetupComponentLVCCAllowListPayload(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID, Arrays.asList(lVCC1, lVCC2, lVCC3));

		byte[] payloadHash = hash.recursiveHash(setupComponentLVCCAllowListPayload);

		setupComponentLVCCAllowListPayload.setSignature(
				new CryptoPrimitivesSignature(payloadHash));

		return setupComponentLVCCAllowListPayload;
	}

	private static SetupComponentLVCCAllowListPayload unsignedLongVoteCastReturnCodesAllowListPayload() {
		final String lVCC1 = Base64.getEncoder().encodeToString(new byte[] { 26 });
		final String lVCC2 = Base64.getEncoder().encodeToString(new byte[] { 56 });
		final String lVCC3 = Base64.getEncoder().encodeToString(new byte[] { 3 });
		return new SetupComponentLVCCAllowListPayload(ELECTION_EVENT_ID, CORRUPTED_VERIFICATION_CARD_SET_ID, Arrays.asList(lVCC1, lVCC2, lVCC3));
	}

	@Nested
	@DisplayName("saving")
	@TestInstance(TestInstance.Lifecycle.PER_CLASS)
	class SaveTest {

		private SetupComponentLVCCAllowListPayloadFileRepository setupComponentLVCCAllowListPayloadFileRepositoryTemp;

		private SetupComponentLVCCAllowListPayload setupComponentLVCCAllowListPayload;

		@BeforeAll
		void setUpAll(
				@TempDir
				final Path tempDir) throws IOException {

			final PathResolver pathResolver = new SetupPathResolver(tempDir, Path.of(""), Path.of(""), Path.of(""), Path.of(""));
			setupComponentLVCCAllowListPayloadFileRepositoryTemp = new SetupComponentLVCCAllowListPayloadFileRepository(objectMapper,
					pathResolver);
		}

		@BeforeEach
		void setUp() {
			setupComponentLVCCAllowListPayload = validLongVoteCastReturnCodesAllowListPayload();
		}

		@Test
		@DisplayName("valid vote cast return codes allow list payload creates file")
		void save() {
			final Path savedPath = setupComponentLVCCAllowListPayloadFileRepositoryTemp.save(setupComponentLVCCAllowListPayload);

			assertTrue(Files.exists(savedPath));
		}

		@Test
		@DisplayName("null vote cast return codes allow list payload throws NullPointerException")
		void saveNullLongVoteCastReturnCodesAllowList() {
			assertThrows(NullPointerException.class, () -> setupComponentLVCCAllowListPayloadFileRepositoryTemp.save(null));
		}

	}

	@Nested
	@DisplayName("calling existsByElectionEventIdAndVerificationCardSetId")
	@TestInstance(TestInstance.Lifecycle.PER_CLASS)
	class ExistsByElectionEventIdAndVerificationCardSetIdTest {

		@Test
		@DisplayName("for existing vote cast return codes allow list payload returns true")
		void existingLongVoteCastReturnCodesAllowList() {
			assertTrue(setupComponentLVCCAllowListPayloadFileRepository.existsByElectionEventIdAndVerificationCardSetId(ELECTION_EVENT_ID,
					VERIFICATION_CARD_SET_ID));
		}

		@Test
		@DisplayName("with invalid id throws an exception")
		void invalidIdThrows() {
			assertAll(
					() -> assertThrows(NullPointerException.class,
							() -> setupComponentLVCCAllowListPayloadFileRepository.existsByElectionEventIdAndVerificationCardSetId(
									null, VERIFICATION_CARD_SET_ID)),
					() -> assertThrows(FailedValidationException.class,
							() -> setupComponentLVCCAllowListPayloadFileRepository.existsByElectionEventIdAndVerificationCardSetId(
									"invalidElectionEventId", VERIFICATION_CARD_SET_ID)),
					() -> assertThrows(NullPointerException.class,
							() -> setupComponentLVCCAllowListPayloadFileRepository.existsByElectionEventIdAndVerificationCardSetId(
									ELECTION_EVENT_ID, null)),
					() -> assertThrows(FailedValidationException.class,
							() -> setupComponentLVCCAllowListPayloadFileRepository.existsByElectionEventIdAndVerificationCardSetId(
									ELECTION_EVENT_ID, "invalidVerificationCardSetId")));
		}

		@Test
		@DisplayName("for non existing vote cast return codes allow list payload returns false")
		void nonExistingLongVoteCastReturnCodesAllowList() {
			assertFalse(setupComponentLVCCAllowListPayloadFileRepository.existsByElectionEventIdAndVerificationCardSetId(ELECTION_EVENT_ID,
					NON_EXISTING_VERIFICATION_CARD_SET_ID));
		}

	}

	@Nested
	@DisplayName("calling findByElectionEventIdAndVerificationCardSetId")
	@TestInstance(TestInstance.Lifecycle.PER_CLASS)
	class FindByElectionEventIdAndVerificationCardSetIdTest {

		@Test
		@DisplayName("for existing vote cast return codes allow list payload returns it")
		void existingLongVoteCastReturnCodesAllowList() {
			assertTrue(setupComponentLVCCAllowListPayloadFileRepository.findByElectionEventIdAndVerificationCardSetId(ELECTION_EVENT_ID,
					VERIFICATION_CARD_SET_ID).isPresent());
		}

		@Test
		@DisplayName("for non existing vote cast return codes allow list payload return empty optional")
		void nonExistingLongVoteCastReturnCodesAllowList() {
			assertFalse(setupComponentLVCCAllowListPayloadFileRepository.findByElectionEventIdAndVerificationCardSetId(ELECTION_EVENT_ID,
							NON_EXISTING_VERIFICATION_CARD_SET_ID)
					.isPresent());
		}

		@Test
		@DisplayName("for corrupted vote cast return codes allow list payload throws UncheckedIOException")
		void corruptedLongVoteCastReturnCodesAllowList() {
			final UncheckedIOException exception = assertThrows(UncheckedIOException.class,
					() -> setupComponentLVCCAllowListPayloadFileRepository.findByElectionEventIdAndVerificationCardSetId(ELECTION_EVENT_ID,
							CORRUPTED_VERIFICATION_CARD_SET_ID));

			final Path verificationCardSetPath = pathResolver.resolveVerificationCardSetPath(ELECTION_EVENT_ID, CORRUPTED_VERIFICATION_CARD_SET_ID);
			final Path longVoteCastReturnCodesAllowListPath = verificationCardSetPath.resolve(PAYLOAD_FILE_NAME);
			final String errorMessage = String.format(
					"Failed to deserialize setup component LVCC allow list payload. [electionEventId: %s, verificationCardSetId: %s, path: %s]",
					ELECTION_EVENT_ID, CORRUPTED_VERIFICATION_CARD_SET_ID, longVoteCastReturnCodesAllowListPath);

			assertEquals(errorMessage, exception.getMessage());
		}

		@Test
		@DisplayName("with invalid id throws an exception")
		void invalidIdThrows() {
			assertAll(
					() -> assertThrows(NullPointerException.class,
							() -> setupComponentLVCCAllowListPayloadFileRepository.findByElectionEventIdAndVerificationCardSetId(
									null, VERIFICATION_CARD_SET_ID)),
					() -> assertThrows(FailedValidationException.class,
							() -> setupComponentLVCCAllowListPayloadFileRepository.findByElectionEventIdAndVerificationCardSetId(
									"invalidElectionEventId", VERIFICATION_CARD_SET_ID)),
					() -> assertThrows(NullPointerException.class,
							() -> setupComponentLVCCAllowListPayloadFileRepository.findByElectionEventIdAndVerificationCardSetId(
									ELECTION_EVENT_ID, null)),
					() -> assertThrows(FailedValidationException.class,
							() -> setupComponentLVCCAllowListPayloadFileRepository.findByElectionEventIdAndVerificationCardSetId(
									ELECTION_EVENT_ID, "invalidVerificationCardSetId")));
		}

	}

}
