/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.protocol.configuration.setuptally;

import static ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalFactory.createElGamal;
import static ch.post.it.evoting.cryptoprimitives.hashing.HashFactory.createHash;
import static ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ZeroKnowledgeProofFactory.createZeroKnowledgeProof;
import static ch.post.it.evoting.evotinglibraries.domain.ControlComponentConstants.NODE_IDS;
import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.google.common.base.Throwables;

import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.BaseEncodingFactory;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ZeroKnowledgeProof;
import ch.post.it.evoting.evotinglibraries.domain.election.ControlComponentPublicKeys;
import ch.post.it.evoting.evotinglibraries.domain.election.ElectionEventContext;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.ElectionEventContextPayload;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.SetupComponentPublicKeysPayload;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.generators.ElectionEventContextPayloadGenerator;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.generators.SetupComponentPublicKeysPayloadGenerator;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.preliminaries.electioneventcontext.GetHashElectionEventContextAlgorithm;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.preliminaries.proofofcorrectkeygeneration.VerifyCCSchnorrProofsAlgorithm;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventContextPayloadService;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventService;

@DisplayName("setupTallyEB called with")
class SetupTallyEBServiceTest {

	private static final Random random = RandomFactory.createRandom();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();
	private static final ElectionEventService electionEventService = mock(ElectionEventService.class);

	private static SetupTallyEBService setupTallyEBService;
	private static String electionEventId;
	private static List<char[]> electoralBoardMembersPasswords;
	private static List<ControlComponentPublicKeys> controlComponentPublicKeys;

	@BeforeAll
	static void setUpAll() {
		final ZeroKnowledgeProof zeroKnowledgeProof = spy(createZeroKnowledgeProof());
		final VerifyCCSchnorrProofsAlgorithm verifyCCSchnorrProofsAlgorithm = new VerifyCCSchnorrProofsAlgorithm(zeroKnowledgeProof);
		final Hash hash = createHash();
		final GetHashElectionEventContextAlgorithm getHashElectionEventContextAlgorithm = new GetHashElectionEventContextAlgorithm(
				BaseEncodingFactory.createBase64(), hash);
		final SetupTallyEBAlgorithm setupTallyEBAlgorithm = new SetupTallyEBAlgorithm(hash, createElGamal(), zeroKnowledgeProof,
				verifyCCSchnorrProofsAlgorithm, getHashElectionEventContextAlgorithm);
		final ElectionEventContextPayloadService electionEventContextPayloadService = mock(ElectionEventContextPayloadService.class);
		setupTallyEBService = new SetupTallyEBService(electionEventService, setupTallyEBAlgorithm, electionEventContextPayloadService);

		final ElectionEventContextPayloadGenerator electionEventContextPayloadGenerator = new ElectionEventContextPayloadGenerator();
		final ElectionEventContextPayload electionEventContextPayload = electionEventContextPayloadGenerator.generate();
		final ElectionEventContext electionEventContext = electionEventContextPayload.getElectionEventContext();
		electionEventId = electionEventContext.electionEventId();
		electoralBoardMembersPasswords = List.of("Password_ElectoralBoard1".toCharArray(), "Password_ElectoralBoard2".toCharArray());

		final GqGroup encryptionGroup = electionEventContextPayload.getEncryptionGroup();
		final SetupComponentPublicKeysPayload setupComponentPublicKeysPayload = new SetupComponentPublicKeysPayloadGenerator(encryptionGroup)
				.generate(electionEventContext.maximumNumberOfSelections(), electionEventContext.maximumNumberOfWriteInsPlusOne());
		controlComponentPublicKeys = setupComponentPublicKeysPayload.getSetupComponentPublicKeys().combinedControlComponentPublicKeys();

		when(electionEventService.exists(electionEventId)).thenReturn(true);

		when(electionEventContextPayloadService.load(electionEventId)).thenReturn(electionEventContextPayload);

		doReturn(true).when(zeroKnowledgeProof).verifySchnorrProof(any(), any(), any());
	}

	private static Stream<Arguments> provideNullParameters() {
		final List<char[]> electoralBoardMembersPasswordsWithNull = new ArrayList<>(electoralBoardMembersPasswords);
		electoralBoardMembersPasswordsWithNull.add(null);

		final List<ControlComponentPublicKeys> controlComponentPublicKeysWithNull = new ArrayList<>(controlComponentPublicKeys);
		controlComponentPublicKeysWithNull.add(null);

		return Stream.of(
				Arguments.of(null, electoralBoardMembersPasswords, controlComponentPublicKeys),
				Arguments.of(electionEventId, null, controlComponentPublicKeys),
				Arguments.of(electionEventId, electoralBoardMembersPasswordsWithNull, controlComponentPublicKeys),
				Arguments.of(electionEventId, electoralBoardMembersPasswords, null),
				Arguments.of(electionEventId, electoralBoardMembersPasswords, controlComponentPublicKeysWithNull)
		);
	}

	@ParameterizedTest
	@MethodSource("provideNullParameters")
	@DisplayName("null parameters throws NullPointerException")
	void setupTallyEBWithNullParametersThrows(final String electionEventId, final List<char[]> electoralBoardMembersPasswords,
			final List<ControlComponentPublicKeys> controlComponentPublicKeys) {
		assertThrows(NullPointerException.class,
				() -> setupTallyEBService.setupTallyEB(electionEventId, electoralBoardMembersPasswords, controlComponentPublicKeys));
	}

	@Test
	@DisplayName("invalid election event id throws FailedValidationException")
	void setupTallyEBWithInvalidElectionEventIdThrows() {
		assertThrows(FailedValidationException.class,
				() -> setupTallyEBService.setupTallyEB("InvalidElectionEventId", electoralBoardMembersPasswords, controlComponentPublicKeys));
	}

	@Test
	@DisplayName("non existent election event id throws IllegalArgumentException")
	void setupTallyEBWithNonExistentElectionEventIdThrows() {
		final String nonExistentElectionEventId = random.genRandomString(ID_LENGTH, base16Alphabet);

		when(electionEventService.exists(nonExistentElectionEventId)).thenReturn(false);

		assertThrows(IllegalArgumentException.class,
				() -> setupTallyEBService.setupTallyEB(nonExistentElectionEventId, electoralBoardMembersPasswords, controlComponentPublicKeys));
	}

	@Test
	@DisplayName("wrong number of electoral board members passwords throws IllegalArgumentException")
	void setupTallyEBWithWrongNumberOfElectoralBoardMembersPasswordsThrows() {
		final List<char[]> tooFewElectoralBoardMembersPasswords = List.of("Password_ElectoralBoard1".toCharArray());

		assertThrows(IllegalArgumentException.class,
				() -> setupTallyEBService.setupTallyEB(electionEventId, tooFewElectoralBoardMembersPasswords, controlComponentPublicKeys));
	}

	@Test
	@DisplayName("wrong number of control component public keys throws IllegalArgumentException")
	void setupTallyEBWithWrongNumberOfControlComponentPublicKeysThrows() {
		final List<ControlComponentPublicKeys> tooFewControlComponentPublicKeys = List.of();

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> setupTallyEBService.setupTallyEB(electionEventId, electoralBoardMembersPasswords, tooFewControlComponentPublicKeys));

		final String expected = String.format("Wrong number of control component public keys. [expected: %s, actual: %s]", NODE_IDS,
				tooFewControlComponentPublicKeys.size());
		assertEquals(expected, Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("valid parameters does not throw")
	void setupTallyEBWithValidParametersDoesNotThrow() {
		assertDoesNotThrow(() -> setupTallyEBService.setupTallyEB(electionEventId, electoralBoardMembersPasswords, controlComponentPublicKeys));
	}

}
