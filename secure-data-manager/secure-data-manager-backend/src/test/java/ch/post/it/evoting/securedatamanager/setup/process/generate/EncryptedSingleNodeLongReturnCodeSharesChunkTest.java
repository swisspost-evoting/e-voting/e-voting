/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.process.generate;

import static ch.post.it.evoting.evotinglibraries.domain.ControlComponentConstants.NODE_IDS;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ElGamalGenerator;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;

@DisplayName("EncryptedSingleNodeLongReturnCodeSharesChunk")
class EncryptedSingleNodeLongReturnCodeSharesChunkTest {

	private static final String VERIFICATION_CARD_ID = "1de81acb944d4161a7148a2240edea47";
	private static final String VALID_ID = "2de81acb944d4161a7148a2240edea47";

	private static final int VALID_CHUNK_ID = 1;
	private static ElGamalMultiRecipientCiphertext ciphertext;

	@BeforeAll
	static void setup() {
		final GqGroup gqGroup = GroupTestData.getGqGroup();
		final ElGamalGenerator elGamalGenerator = new ElGamalGenerator(gqGroup);
		ciphertext = elGamalGenerator.genRandomCiphertext(1);
	}

	@Test
	@DisplayName("calling builder with invalid node id values throws IllegalArgumentException")
	void buildWithInvalidNodeIdThrows() {
		final EncryptedSingleNodeLongReturnCodeSharesChunk.Builder builder_0 = new EncryptedSingleNodeLongReturnCodeSharesChunk.Builder()
				.setNodeId(0);
		final EncryptedSingleNodeLongReturnCodeSharesChunk.Builder builder_N_plus_1 = new EncryptedSingleNodeLongReturnCodeSharesChunk.Builder()
				.setNodeId(NODE_IDS.size() + 1);
		assertAll(
				() -> assertThrows(IllegalArgumentException.class, builder_0::build),
				() -> assertThrows(IllegalArgumentException.class, builder_N_plus_1::build)
		);
	}

	@Test
	@DisplayName("calling builder with invalid chunk id values throws IllegalArgumentException")
	void buildWithInvalidChunkIdThrows() {
		final EncryptedSingleNodeLongReturnCodeSharesChunk.Builder builder_minus_1 = new EncryptedSingleNodeLongReturnCodeSharesChunk.Builder()
				.setNodeId(1)
				.setChunkId(-1);
		final IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class, builder_minus_1::build);
		assertEquals("Control component chunk id must be positive. [chunkId: -1]", illegalArgumentException.getMessage());
	}

	private static Stream<Arguments> provideNullInputsForEncryptedSingleNodeLongReturnCodeShares() {
		return Stream.of(
				Arguments.of(null, Collections.singletonList(ciphertext), Collections.singletonList(ciphertext)),
				Arguments.of(Collections.singletonList(VERIFICATION_CARD_ID), null, Collections.singletonList(ciphertext)),
				Arguments.of(Collections.singletonList(VERIFICATION_CARD_ID), Collections.singletonList(ciphertext), null)
		);
	}

	@ParameterizedTest
	@MethodSource("provideNullInputsForEncryptedSingleNodeLongReturnCodeShares")
	@DisplayName("calling builder with null values throws NullPointerException")
	void buildWithInvalidValuesThrows(final List<String> verificationCardIds, final List<ElGamalMultiRecipientCiphertext> partialChoiceReturnCodes,
			final List<ElGamalMultiRecipientCiphertext> confirmationKeys) {
		final EncryptedSingleNodeLongReturnCodeSharesChunk.Builder builder = new EncryptedSingleNodeLongReturnCodeSharesChunk.Builder()
				.setNodeId(1)
				.setChunkId(VALID_CHUNK_ID)
				.setVerificationCardIds(verificationCardIds)
				.setExponentiatedEncryptedPartialChoiceReturnCodes(partialChoiceReturnCodes)
				.setExponentiatedEncryptedConfirmationKeys(confirmationKeys);
		assertThrows(NullPointerException.class, builder::build);
	}

	@Test
	@DisplayName("calling builder with invalid verification card ids values throws FailedValidationException")
	void buildWithInvalidVerificationCardIdsThrows() {
		final EncryptedSingleNodeLongReturnCodeSharesChunk.Builder builder = new EncryptedSingleNodeLongReturnCodeSharesChunk.Builder()
				.setNodeId(1)
				.setChunkId(VALID_CHUNK_ID)
				.setVerificationCardIds(Arrays.asList(VERIFICATION_CARD_ID, "invalid verification card id"));
		assertThrows(FailedValidationException.class, builder::build);
	}

	private static Stream<Arguments> provideWrongSizeListInputsForEncryptedSingleNodeLongReturnCodeShares() {
		return Stream.of(
				Arguments.of(
						Collections.singletonList(VERIFICATION_CARD_ID),
						Arrays.asList(ciphertext, ciphertext),
						Arrays.asList(ciphertext, ciphertext)
				),
				Arguments.of(
						Arrays.asList(VERIFICATION_CARD_ID, VERIFICATION_CARD_ID),
						Collections.singletonList(ciphertext),
						Arrays.asList(ciphertext, ciphertext)

				),
				Arguments.of(
						Arrays.asList(VERIFICATION_CARD_ID, VERIFICATION_CARD_ID),
						Arrays.asList(ciphertext, ciphertext),
						Collections.singletonList(ciphertext)
				)
		);
	}

	@ParameterizedTest
	@MethodSource("provideWrongSizeListInputsForEncryptedSingleNodeLongReturnCodeShares")
	@DisplayName("calling builder with wrong size list throws IllegalArgumentException")
	void buildWithWrongSizeListInputsThrows(final List<String> verificationCardIds,
			final List<ElGamalMultiRecipientCiphertext> partialChoiceReturnCodes,
			final List<ElGamalMultiRecipientCiphertext> confirmationKeys) {
		final EncryptedSingleNodeLongReturnCodeSharesChunk.Builder builder = new EncryptedSingleNodeLongReturnCodeSharesChunk.Builder()
				.setNodeId(1)
				.setVerificationCardIds(verificationCardIds)
				.setExponentiatedEncryptedPartialChoiceReturnCodes(partialChoiceReturnCodes)
				.setExponentiatedEncryptedConfirmationKeys(confirmationKeys);
		assertThrows(IllegalArgumentException.class, builder::build);
	}

	@Test
	@DisplayName("calling builder with non distinct values does throw IllegalArgumentException")
	void buildWithNonDistinctValues() {
		final EncryptedSingleNodeLongReturnCodeSharesChunk.Builder builder = new EncryptedSingleNodeLongReturnCodeSharesChunk.Builder()
				.setNodeId(1)
				.setChunkId(VALID_CHUNK_ID)
				.setVerificationCardIds(Arrays.asList(VERIFICATION_CARD_ID, VERIFICATION_CARD_ID))
				.setExponentiatedEncryptedPartialChoiceReturnCodes(Arrays.asList(ciphertext, ciphertext))
				.setExponentiatedEncryptedConfirmationKeys(Arrays.asList(ciphertext, ciphertext));
		final IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, builder::build);
		assertEquals("All verification card ids must be distinct.", ex.getMessage());
	}

	@Test
	@DisplayName("calling builder with valid values does not throw")
	void buildWithValidValues() {
		final EncryptedSingleNodeLongReturnCodeSharesChunk.Builder builder = new EncryptedSingleNodeLongReturnCodeSharesChunk.Builder()
				.setNodeId(1)
				.setChunkId(VALID_CHUNK_ID)
				.setVerificationCardIds(Arrays.asList(VERIFICATION_CARD_ID, VALID_ID))
				.setExponentiatedEncryptedPartialChoiceReturnCodes(Arrays.asList(ciphertext, ciphertext))
				.setExponentiatedEncryptedConfirmationKeys(Arrays.asList(ciphertext, ciphertext));
		assertDoesNotThrow(builder::build);
	}

}
