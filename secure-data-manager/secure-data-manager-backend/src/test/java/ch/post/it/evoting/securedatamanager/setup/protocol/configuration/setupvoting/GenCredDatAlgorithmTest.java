/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.protocol.configuration.setupvoting;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.security.SecureRandom;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.hashing.Argon2;
import ch.post.it.evoting.cryptoprimitives.hashing.Argon2Factory;
import ch.post.it.evoting.cryptoprimitives.hashing.Argon2Profile;
import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.hashing.HashFactory;
import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base64;
import ch.post.it.evoting.cryptoprimitives.math.BaseEncodingFactory;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.PrimeGqElement;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.math.ZqElement;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.symmetric.Symmetric;
import ch.post.it.evoting.cryptoprimitives.symmetric.SymmetricFactory;
import ch.post.it.evoting.cryptoprimitives.test.tools.TestGroupSetup;
import ch.post.it.evoting.domain.ElectionSetupUtils;
import ch.post.it.evoting.evotinglibraries.domain.election.PrimesMappingTable;
import ch.post.it.evoting.evotinglibraries.domain.election.generators.PrimesMappingTableGenerator;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.preliminaries.votingoptions.GetHashContextAlgorithm;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.preliminaries.votingoptions.PrimesMappingTableAlgorithms;

/**
 * Tests of GenCredDatAlgorithm.
 */
@DisplayName("GenCredDatAlgorithm")
class GenCredDatAlgorithmTest extends TestGroupSetup {

	private static final int bound = 12;
	private static final int maxSize = 4;
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();
	private static final PrimesMappingTableGenerator primesMappingTableGenerator = new PrimesMappingTableGenerator();

	private final Random rand = RandomFactory.createRandom();
	private final SecureRandom srand = new SecureRandom();
	private final int size = srand.nextInt(bound) + 1;
	private final List<String> verificationCardIds = Stream.generate(
					() -> rand.genRandomString(ID_LENGTH, base16Alphabet))
			.limit(size).toList();
	private final List<String> startVotingKeys = Stream.generate(ElectionSetupUtils::genStartVotingKey)
			.limit(size)
			.toList();
	private final GroupVector<ZqElement, ZqGroup> verificationCardSecretKeys = Stream.generate(zqGroupGenerator::genRandomZqElementMember).limit(size)
			.collect(GroupVector.toGroupVector());
	private final int delta_max = srand.nextInt(bound) + 1;
	private final ElGamalMultiRecipientPublicKey electionPublicKey = elGamalGenerator.genRandomPublicKey(delta_max);
	private final int psi = srand.nextInt(bound) + 1;
	private final ElGamalMultiRecipientPublicKey choiceReturnCodesEncryptionPublicKey = elGamalGenerator.genRandomPublicKey(psi);
	private final GroupVector<PrimeGqElement, GqGroup> encodedVotingOptions = Stream.generate(gqGroupGenerator::genSmallPrimeMember).limit(maxSize)
			.distinct()
			.collect(GroupVector.toGroupVector());
	private final List<String> actualVotingOptions = Stream.generate(
					() -> rand.genRandomString(ID_LENGTH, base16Alphabet))
			.limit(encodedVotingOptions.size()).toList();
	private final PrimesMappingTable primesMappingTable = primesMappingTableGenerator.generate(actualVotingOptions, encodedVotingOptions);
	private final String electionEventId = rand.genRandomString(ID_LENGTH, base16Alphabet);
	private final String verificationCardSetId = rand.genRandomString(ID_LENGTH, base16Alphabet);
	private final Hash hash = HashFactory.createHash();
	private final Symmetric symmetric = SymmetricFactory.createSymmetric();
	private final Base64 base64 = BaseEncodingFactory.createBase64();
	private final Argon2 argon2 = Argon2Factory.createArgon2(Argon2Profile.TEST);
	private final GetHashContextAlgorithm getHashContextAlgorithm = new GetHashContextAlgorithm(base64, hash, new PrimesMappingTableAlgorithms());

	private GenCredDatContext context;
	private GenCredDatInput input;
	private GenCredDatAlgorithm genCredDatAlgorithm;

	@BeforeEach
	void setup() {
		context = buildCredDataContext();
		input = new GenCredDatInput(verificationCardSecretKeys, startVotingKeys);

		assertNotNull(hash, "hashService");
		assertNotNull(symmetric, "symmetricService");

		genCredDatAlgorithm = new GenCredDatAlgorithm(hash, symmetric, base64, argon2, getHashContextAlgorithm);
	}

	@Test
	void happyPath() {
		final GenCredDatOutput output = genCredDatAlgorithm.genCredDat(context, input);

		assertNotNull(output, "output is null");
		assertNotNull(output.verificationCardKeystores(), "output.verificationCardKeystores() is null");
		assertEquals(output.verificationCardKeystores().size(), context.getVerificationCardIds().size());
	}

	@Test
	void nullContextArgument() {
		assertThrows(NullPointerException.class, () -> genCredDatAlgorithm.genCredDat(null, input));
	}

	@Test
	void nullInputArgument() {
		assertThrows(NullPointerException.class, () -> genCredDatAlgorithm.genCredDat(context, null));
	}

	private GenCredDatContext buildCredDataContext() {
		return new GenCredDatContext
				.Builder()
				.setEncryptionGroup(gqGroup)
				.setElectionEventId(electionEventId)
				.setVerificationCardSetId(verificationCardSetId)
				.setVerificationCardIds(verificationCardIds)
				.setPrimesMappingTable(primesMappingTable)
				.setElectionPublicKey(electionPublicKey)
				.setChoiceReturnCodesEncryptionPublicKey(choiceReturnCodesEncryptionPublicKey)
				.build();
	}

}
