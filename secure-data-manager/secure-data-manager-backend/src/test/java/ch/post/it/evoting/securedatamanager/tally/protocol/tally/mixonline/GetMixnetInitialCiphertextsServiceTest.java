/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.tally.protocol.tally.mixonline;

import static ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalFactory.createElGamal;
import static ch.post.it.evoting.cryptoprimitives.hashing.HashFactory.createHash;
import static ch.post.it.evoting.cryptoprimitives.math.BaseEncodingFactory.createBase64;
import static ch.post.it.evoting.evotinglibraries.domain.ControlComponentConstants.NODE_IDS;
import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.google.common.base.Throwables;

import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.domain.generators.ControlComponentBallotBoxPayloadGenerator;
import ch.post.it.evoting.evotinglibraries.domain.election.SetupComponentPublicKeys;
import ch.post.it.evoting.evotinglibraries.domain.election.VerificationCardSetContext;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.ElectionEventContextPayload;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.generators.ElectionEventContextPayloadGenerator;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.generators.SetupComponentPublicKeysPayloadGenerator;
import ch.post.it.evoting.evotinglibraries.domain.tally.ControlComponentBallotBoxPayload;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.preliminaries.votingoptions.PrimesMappingTableAlgorithms;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.tally.mixonline.GetMixnetInitialCiphertextsAlgorithm;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventContextPayloadService;
import ch.post.it.evoting.securedatamanager.tally.process.decrypt.IdentifierValidationService;

@DisplayName("getMixnetInitialCiphertexts called with")
class GetMixnetInitialCiphertextsServiceTest {

	private static final Random random = RandomFactory.createRandom();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();

	private static GetMixnetInitialCiphertextsService getMixnetInitialCiphertextsService;
	private static String electionEventId;
	private static VerificationCardSetContext verificationCardSetContext;
	private static SetupComponentPublicKeys setupComponentPublicKeys;
	private static List<ControlComponentBallotBoxPayload> controlComponentBallotBoxPayloads;
	private static int psi;
	private static int delta;
	private static ControlComponentBallotBoxPayloadGenerator controlComponentBallotBoxPayloadGenerator;

	@BeforeAll
	static void setUpAll() {
		final IdentifierValidationService identifierValidationService = mock(IdentifierValidationService.class);
		final GetMixnetInitialCiphertextsAlgorithm getMixnetInitialCiphertextsAlgorithm = new GetMixnetInitialCiphertextsAlgorithm(createHash(),
				createBase64(), createElGamal());
		final ElectionEventContextPayloadService electionEventContextPayloadService = mock(ElectionEventContextPayloadService.class);
		final PrimesMappingTableAlgorithms primesMappingTableAlgorithms = new PrimesMappingTableAlgorithms();
		getMixnetInitialCiphertextsService = new GetMixnetInitialCiphertextsService(identifierValidationService, primesMappingTableAlgorithms,
				electionEventContextPayloadService, getMixnetInitialCiphertextsAlgorithm);

		final ElectionEventContextPayloadGenerator electionEventContextPayloadGenerator = new ElectionEventContextPayloadGenerator();
		final ElectionEventContextPayload electionEventContextPayload = electionEventContextPayloadGenerator.generate();
		electionEventId = electionEventContextPayload.getElectionEventContext().electionEventId();
		verificationCardSetContext = electionEventContextPayload.getElectionEventContext().verificationCardSetContexts().getFirst();

		final GqGroup encryptionGroup = electionEventContextPayload.getEncryptionGroup();
		psi = primesMappingTableAlgorithms.getPsi(verificationCardSetContext.getPrimesMappingTable());
		delta = primesMappingTableAlgorithms.getDelta(verificationCardSetContext.getPrimesMappingTable());
		setupComponentPublicKeys = new SetupComponentPublicKeysPayloadGenerator(encryptionGroup).generate(psi, delta)
				.getSetupComponentPublicKeys();
		controlComponentBallotBoxPayloadGenerator = new ControlComponentBallotBoxPayloadGenerator(encryptionGroup);
		controlComponentBallotBoxPayloads = controlComponentBallotBoxPayloadGenerator.generate(electionEventId,
				verificationCardSetContext.getBallotBoxId(), psi, delta);

		doNothing().when(identifierValidationService).validateBallotBoxRelatedIds(electionEventId, verificationCardSetContext.getBallotBoxId());
		when(electionEventContextPayloadService.loadEncryptionGroup(electionEventId))
				.thenReturn(electionEventContextPayload.getEncryptionGroup());
	}

	private static Stream<Arguments> provideNullParameters() {
		final List<ControlComponentBallotBoxPayload> controlComponentBallotBoxPayloadsWithNull = new ArrayList<>(controlComponentBallotBoxPayloads);
		controlComponentBallotBoxPayloadsWithNull.add(null);

		return Stream.of(
				Arguments.of(null, verificationCardSetContext, setupComponentPublicKeys, controlComponentBallotBoxPayloads),
				Arguments.of(electionEventId, null, setupComponentPublicKeys, controlComponentBallotBoxPayloads),
				Arguments.of(electionEventId, verificationCardSetContext, null, controlComponentBallotBoxPayloads),
				Arguments.of(electionEventId, verificationCardSetContext, setupComponentPublicKeys, null),
				Arguments.of(electionEventId, verificationCardSetContext, setupComponentPublicKeys, controlComponentBallotBoxPayloadsWithNull)
		);
	}

	@ParameterizedTest
	@MethodSource("provideNullParameters")
	@DisplayName("null parameters throws NullPointerException")
	void getMixnetInitialCiphertextsWithNullParametersThrows(final String electionEventId,
			final VerificationCardSetContext verificationCardSetContext, final SetupComponentPublicKeys setupComponentPublicKeys,
			final List<ControlComponentBallotBoxPayload> controlComponentBallotBoxPayloads) {
		assertThrows(NullPointerException.class,
				() -> getMixnetInitialCiphertextsService.getMixnetInitialCiphertexts(electionEventId, verificationCardSetContext,
						setupComponentPublicKeys, controlComponentBallotBoxPayloads));
	}

	@Test
	@DisplayName("invalid election event id throws FailedValidationException")
	void getMixnetInitialCiphertextsWithInvalidElectionEventIdThrows() {
		assertThrows(FailedValidationException.class,
				() -> getMixnetInitialCiphertextsService.getMixnetInitialCiphertexts("InvalidElectionEventId", verificationCardSetContext,
						setupComponentPublicKeys, controlComponentBallotBoxPayloads));
	}

	@Test
	@DisplayName("wrong number of control component ballot box payloads throws IllegalStateException")
	void getMixnetInitialCiphertextsWithWrongNumberOfControlComponentBallotBoxPayloadsThrows() {
		final List<ControlComponentBallotBoxPayload> tooFewControlComponentBallotBoxPayloads = List.of();

		final IllegalStateException exception = assertThrows(IllegalStateException.class,
				() -> getMixnetInitialCiphertextsService.getMixnetInitialCiphertexts(electionEventId, verificationCardSetContext,
						setupComponentPublicKeys, tooFewControlComponentBallotBoxPayloads));

		final String expected = "Wrong number of control component ballot box payloads.";
		assertEquals(expected, Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("wrong node ids in control component ballot box payloads throws IllegalStateException")
	void getMixnetInitialCiphertextsWithWrongNodeIdsInControlComponentBallotBoxPayloadsThrows() {
		final List<ControlComponentBallotBoxPayload> wrongNodeIdsControlComponentBallotBoxPayloads = controlComponentBallotBoxPayloads.stream()
				.map(controlComponentBallotBoxPayload ->
						new ControlComponentBallotBoxPayload(
								controlComponentBallotBoxPayload.getEncryptionGroup(),
								controlComponentBallotBoxPayload.getElectionEventId(),
								controlComponentBallotBoxPayload.getBallotBoxId(),
								controlComponentBallotBoxPayload.getNodeId() == NODE_IDS.last() ?
										NODE_IDS.first() :
										controlComponentBallotBoxPayload.getNodeId(),
								controlComponentBallotBoxPayload.getConfirmedEncryptedVotes()))
				.toList();

		final IllegalStateException exception = assertThrows(IllegalStateException.class,
				() -> getMixnetInitialCiphertextsService.getMixnetInitialCiphertexts(electionEventId, verificationCardSetContext,
						setupComponentPublicKeys, wrongNodeIdsControlComponentBallotBoxPayloads));

		final String expected = "Wrong number of control component ballot box payloads.";
		assertEquals(expected, Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("different group payloads throws IllegalArgumentException")
	void getMixnetInitialCiphertextsWithDifferentGroupPayloadsThrows() {
		final List<ControlComponentBallotBoxPayload> differentGroupControlComponentBallotBoxPayloads = controlComponentBallotBoxPayloads.stream()
				.map(controlComponentBallotBoxPayload ->
						new ControlComponentBallotBoxPayload(
								controlComponentBallotBoxPayload.getNodeId() == NODE_IDS.last() ?
										GroupTestData.getDifferentGqGroup(controlComponentBallotBoxPayload.getEncryptionGroup()) :
										controlComponentBallotBoxPayload.getEncryptionGroup(),
								controlComponentBallotBoxPayload.getElectionEventId(),
								controlComponentBallotBoxPayload.getBallotBoxId(),
								controlComponentBallotBoxPayload.getNodeId(),
								controlComponentBallotBoxPayload.getConfirmedEncryptedVotes()))
				.toList();

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> getMixnetInitialCiphertextsService.getMixnetInitialCiphertexts(electionEventId, verificationCardSetContext,
						setupComponentPublicKeys, differentGroupControlComponentBallotBoxPayloads));

		final String expected = "All control component ballot box payloads must have the same group.";
		assertEquals(expected, Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("different election event id payloads throws IllegalArgumentException")
	void getMixnetInitialCiphertextsWithDifferentElectionEventIdPayloadsThrows() {
		final ControlComponentBallotBoxPayload otherElectionEventIdPayload = controlComponentBallotBoxPayloadGenerator.generate(
						random.genRandomString(ID_LENGTH, base16Alphabet), verificationCardSetContext.getBallotBoxId(), psi, delta)
				.getLast();

		final List<ControlComponentBallotBoxPayload> differentElectionEventIdControlComponentBallotBoxPayloads = controlComponentBallotBoxPayloads.stream()
				.map(controlComponentBallotBoxPayload ->
						controlComponentBallotBoxPayload.getNodeId() == NODE_IDS.last() ?
								otherElectionEventIdPayload :
								controlComponentBallotBoxPayload
				).toList();

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> getMixnetInitialCiphertextsService.getMixnetInitialCiphertexts(electionEventId, verificationCardSetContext,
						setupComponentPublicKeys, differentElectionEventIdControlComponentBallotBoxPayloads));

		final String expected = "All control component ballot box payloads must have the same election event id.";
		assertEquals(expected, Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("different ballot box id payloads throws IllegalArgumentException")
	void getMixnetInitialCiphertextsWithDifferentBallotBoxIdPayloadsThrows() {
		final List<ControlComponentBallotBoxPayload> differentBallotBoxIdControlComponentBallotBoxPayloads = controlComponentBallotBoxPayloads.stream()
				.map(controlComponentBallotBoxPayload ->
						new ControlComponentBallotBoxPayload(
								controlComponentBallotBoxPayload.getEncryptionGroup(),
								controlComponentBallotBoxPayload.getElectionEventId(),
								controlComponentBallotBoxPayload.getNodeId() == NODE_IDS.last() ?
										random.genRandomString(ID_LENGTH, base16Alphabet) :
										controlComponentBallotBoxPayload.getBallotBoxId(),
								controlComponentBallotBoxPayload.getNodeId(),
								controlComponentBallotBoxPayload.getConfirmedEncryptedVotes()))
				.toList();

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> getMixnetInitialCiphertextsService.getMixnetInitialCiphertexts(electionEventId, verificationCardSetContext,
						setupComponentPublicKeys,
						differentBallotBoxIdControlComponentBallotBoxPayloads));

		final String expected = "All control component ballot box payloads must have the same ballot box id.";
		assertEquals(expected, Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("valid parameters does not throw")
	void getMixnetInitialCiphertextsWithValidParametersDoesNotThrow() {
		assertDoesNotThrow(() -> getMixnetInitialCiphertextsService.getMixnetInitialCiphertexts(electionEventId, verificationCardSetContext,
				setupComponentPublicKeys, controlComponentBallotBoxPayloads));
	}
}
