/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.protocol.configuration.setupvoting;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.google.common.base.Throwables;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalFactory;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.evotinglibraries.domain.election.Ballot;
import ch.post.it.evoting.evotinglibraries.domain.election.generators.BallotGenerator;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.preliminaries.votingoptions.PrimesMappingTableAlgorithms;

/**
 * Tests of GenSetupDataAlgorithm.
 */
@DisplayName("GenSetupDataAlgorithm")
class GenSetupDataAlgorithmTest {

	private final Random random = RandomFactory.createRandom();
	private final GenSetupDataAlgorithm genSetupDataAlgorithm = new GenSetupDataAlgorithm(random,
			new PrimesMappingTableAlgorithms(), new GetElectionEventEncryptionParametersAlgorithm(ElGamalFactory.createElGamal()));
	private GenSetupDataContext context;

	@BeforeEach
	void setUp() {
		final BallotGenerator ballotGenerator = new BallotGenerator();
		final Ballot ballot = ballotGenerator.generate();
		final List<String> actualVotingOptions = ballot.getActualVotingOptions();
		final List<String> semanticInformation = ballot.getSemanticInformation();
		final List<String> correctnessInformation = ballot.getCorrectnessInformation();

		final List<PrimesMappingTableEntrySubset> primesMappingTableEntrySubset = IntStream.range(0, actualVotingOptions.size())
				.mapToObj(
						j -> new PrimesMappingTableEntrySubset(actualVotingOptions.get(j), semanticInformation.get(j), correctnessInformation.get(j)))
				.toList();
		final Map<String, List<PrimesMappingTableEntrySubset>> optionsInformationMap = Map.of(
				random.genRandomString(ID_LENGTH, Base16Alphabet.getInstance()),
				primesMappingTableEntrySubset);

		context = new GenSetupDataContext(optionsInformationMap);
	}

	@Test
	@DisplayName("calling genSetupData with null argument throws a NullPointerException.")
	void genSetupDataWithNullGroupThrows() {
		assertThrows(NullPointerException.class, () -> genSetupDataAlgorithm.genSetupData(null, "NE_20231124_TT05"));
		assertThrows(NullPointerException.class, () -> genSetupDataAlgorithm.genSetupData(context, null));
	}

	@Test
	@DisplayName("invalid seed throws FailedValidationException")
	void invalidSeedThrows() {
		final String wrongSizeSeed = "NE_20231124_TT055";
		FailedValidationException exception = assertThrows(FailedValidationException.class,
				() -> genSetupDataAlgorithm.genSetupData(context, wrongSizeSeed));
		String expected = String.format("The seed doesn't comply with the required format. [seed: %s]", wrongSizeSeed);
		assertEquals(expected, Throwables.getRootCause(exception).getMessage());

		final String invalidDateSeed = "NE_20231324_TT05";
		exception = assertThrows(FailedValidationException.class,
				() -> genSetupDataAlgorithm.genSetupData(context, invalidDateSeed));
		expected = String.format("The seed doesn't comply with the required format. [seed: %s]", invalidDateSeed);
		assertEquals(expected, Throwables.getRootCause(exception).getMessage());

	}

	@Test
	@DisplayName("calling genSetupData with a correct argument generates a keypair with the right size.")
	void genSetupData() {
		final GenSetupDataOutput output = genSetupDataAlgorithm.genSetupData(context, "NE_20231124_TT05");

		assertEquals(output.getMaximumNumberOfVotingOptions(), output.getSetupKeyPair().size());
	}
}
