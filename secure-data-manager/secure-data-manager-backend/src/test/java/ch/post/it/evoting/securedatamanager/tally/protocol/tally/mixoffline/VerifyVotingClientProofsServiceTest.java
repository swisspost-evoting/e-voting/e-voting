/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.tally.protocol.tally.mixoffline;

import static ch.post.it.evoting.cryptoprimitives.hashing.HashFactory.createHash;
import static ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ZeroKnowledgeProofFactory.createZeroKnowledgeProof;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

import java.security.SignatureException;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.google.common.base.Throwables;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.hashing.HashFactory;
import ch.post.it.evoting.cryptoprimitives.math.BaseEncodingFactory;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ElGamalGenerator;
import ch.post.it.evoting.domain.InvalidPayloadSignatureException;
import ch.post.it.evoting.domain.generators.ControlComponentBallotBoxPayloadGenerator;
import ch.post.it.evoting.evotinglibraries.domain.common.ContextIds;
import ch.post.it.evoting.evotinglibraries.domain.common.EncryptedVerifiableVote;
import ch.post.it.evoting.evotinglibraries.domain.configuration.SetupComponentTallyDataPayload;
import ch.post.it.evoting.evotinglibraries.domain.election.SetupComponentPublicKeys;
import ch.post.it.evoting.evotinglibraries.domain.election.VerificationCardSetContext;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.ElectionEventContextPayload;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.generators.ElectionEventContextPayloadGenerator;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.generators.SetupComponentPublicKeysPayloadGenerator;
import ch.post.it.evoting.evotinglibraries.domain.signature.Alias;
import ch.post.it.evoting.evotinglibraries.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.preliminaries.votingoptions.GetHashContextAlgorithm;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.preliminaries.votingoptions.PrimesMappingTableAlgorithms;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.tally.mixoffline.VerifyVotingClientProofsAlgorithm;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventContextPayloadService;
import ch.post.it.evoting.securedatamanager.shared.process.SetupComponentTallyDataPayloadService;
import ch.post.it.evoting.securedatamanager.tally.process.decrypt.IdentifierValidationService;

@DisplayName("verifyVotingClientProofs called with")
class VerifyVotingClientProofsServiceTest {

	private static final SignatureKeystore<Alias> signatureKeystoreService = mock(SignatureKeystore.class);
	private static final SetupComponentTallyDataPayloadService setupComponentTallyDataPayloadService = mock(
			SetupComponentTallyDataPayloadService.class);

	private static VerifyVotingClientProofsService verifyVotingClientProofsService;
	private static String electionEventId;
	private static String verificationCardSetId;
	private static VerificationCardSetContext verificationCardSetContext;
	private static SetupComponentPublicKeys setupComponentPublicKeys;
	private static List<EncryptedVerifiableVote> confirmedEncryptedVotes;

	@BeforeAll
	static void setUpAll() {
		final IdentifierValidationService identifierValidationService = mock(IdentifierValidationService.class);
		final PrimesMappingTableAlgorithms primesMappingTableAlgorithms = new PrimesMappingTableAlgorithms();
		final GetHashContextAlgorithm getHashContextAlgorithm = new GetHashContextAlgorithm(BaseEncodingFactory.createBase64(),
				HashFactory.createHash(), primesMappingTableAlgorithms);
		final VerifyVotingClientProofsAlgorithm verifyVotingClientProofsAlgorithm = new VerifyVotingClientProofsAlgorithm(createZeroKnowledgeProof(),
				getHashContextAlgorithm, primesMappingTableAlgorithms);
		final ElectionEventContextPayloadService electionEventContextPayloadService = mock(ElectionEventContextPayloadService.class);
		verifyVotingClientProofsService = new VerifyVotingClientProofsService(signatureKeystoreService, identifierValidationService,
				verifyVotingClientProofsAlgorithm, electionEventContextPayloadService, setupComponentTallyDataPayloadService);

		final ElectionEventContextPayloadGenerator electionEventContextPayloadGenerator = new ElectionEventContextPayloadGenerator();
		final ElectionEventContextPayload electionEventContextPayload = electionEventContextPayloadGenerator.generate();
		electionEventId = electionEventContextPayload.getElectionEventContext().electionEventId();
		verificationCardSetContext = electionEventContextPayload.getElectionEventContext().verificationCardSetContexts().getFirst();
		verificationCardSetId = verificationCardSetContext.getVerificationCardSetId();

		final int psi = primesMappingTableAlgorithms.getPsi(verificationCardSetContext.getPrimesMappingTable());
		final int delta = primesMappingTableAlgorithms.getDelta(verificationCardSetContext.getPrimesMappingTable());
		final GqGroup encryptionGroup = electionEventContextPayload.getEncryptionGroup();
		setupComponentPublicKeys = new SetupComponentPublicKeysPayloadGenerator(encryptionGroup).generate(psi, delta).getSetupComponentPublicKeys();
		confirmedEncryptedVotes = new ControlComponentBallotBoxPayloadGenerator(encryptionGroup).generate(electionEventId,
						verificationCardSetContext.getBallotBoxId(), psi, delta)
				.getFirst()
				.getConfirmedEncryptedVotes();

		doNothing().when(identifierValidationService).validateBallotBoxRelatedIds(electionEventId, verificationCardSetContext.getBallotBoxId());
		when(electionEventContextPayloadService.loadEncryptionGroup(electionEventId)).thenReturn(encryptionGroup);
	}

	@BeforeEach
	void setUp() throws SignatureException {
		when(signatureKeystoreService.verifySignature(any(), any(), any(), any())).thenReturn(true);

		final SetupComponentTallyDataPayload setupComponentTallyDataPayload = getSetupComponentTallyDataPayload();
		setupComponentTallyDataPayload.setSignature(new CryptoPrimitivesSignature(createHash().recursiveHash(setupComponentTallyDataPayload)));
		when(setupComponentTallyDataPayloadService.load(electionEventId, verificationCardSetId)).thenReturn(setupComponentTallyDataPayload);
	}

	@AfterEach
	void tearDown() {
		reset(signatureKeystoreService);
	}

	private static Stream<Arguments> provideNullParameters() {
		return Stream.of(
				Arguments.of(null, verificationCardSetContext, setupComponentPublicKeys, confirmedEncryptedVotes),
				Arguments.of(electionEventId, null, setupComponentPublicKeys, confirmedEncryptedVotes),
				Arguments.of(electionEventId, verificationCardSetContext, null, confirmedEncryptedVotes),
				Arguments.of(electionEventId, verificationCardSetContext, setupComponentPublicKeys, null)
		);
	}

	@ParameterizedTest
	@MethodSource("provideNullParameters")
	@DisplayName("null parameters throws NullPointerException")
	void verifyVotingClientProofsWithNullParametersThrows(final String electionEventId, final VerificationCardSetContext verificationCardSetContext,
			final SetupComponentPublicKeys setupComponentPublicKeys, final List<EncryptedVerifiableVote> confirmedEncryptedVotes) {
		assertThrows(NullPointerException.class,
				() -> verifyVotingClientProofsService.verifyVotingClientProofs(electionEventId, verificationCardSetContext, setupComponentPublicKeys,
						confirmedEncryptedVotes));
	}

	@Test
	@DisplayName("invalid election event id throws FailedValidationException")
	void verifyVotingClientProofsWithInvalidElectionEventIdThrows() {
		assertThrows(FailedValidationException.class,
				() -> verifyVotingClientProofsService.verifyVotingClientProofs("InvalidElectionEventId", verificationCardSetContext,
						setupComponentPublicKeys, confirmedEncryptedVotes));
	}

	@Test
	@DisplayName("setup component tally data payload with null signature throws IllegalStateException")
	void verifyVotingClientProofsWithSetupComponentTallyDataPayloadWithoutSignatureThrows() {
		final SetupComponentTallyDataPayload noSignatureSetupComponentTallyDataPayload = getSetupComponentTallyDataPayload();

		when(setupComponentTallyDataPayloadService.load(electionEventId, verificationCardSetId))
				.thenReturn(noSignatureSetupComponentTallyDataPayload);

		final IllegalStateException exception = assertThrows(IllegalStateException.class,
				() -> verifyVotingClientProofsService.verifyVotingClientProofs(electionEventId, verificationCardSetContext,
						setupComponentPublicKeys, confirmedEncryptedVotes));

		final String expected = String.format(
				"The signature of the setup component tally data payload is null. [electionEventId: %s, verificationCardSetId: %s]",
				electionEventId, verificationCardSetId);
		assertEquals(expected, Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("error verifying setup component tally data payload signature throws IllegalStateException")
	void verifyVotingClientProofsWithErrorVerifyingSetupComponentTallyDataPayloadSignatureThrows() throws SignatureException {
		when(signatureKeystoreService.verifySignature(any(), any(), any(), any())).thenThrow(SignatureException.class);

		final IllegalStateException exception = assertThrows(IllegalStateException.class,
				() -> verifyVotingClientProofsService.verifyVotingClientProofs(electionEventId, verificationCardSetContext,
						setupComponentPublicKeys, confirmedEncryptedVotes));

		final String expected = String.format(
				"Could not verify the signature of the setup component tally data payload. [electionEventId: %s, verificationCardSetId: %s]",
				electionEventId, verificationCardSetId);
		assertEquals(expected, Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("invalid setup component tally data payload signature throws InvalidPayloadSignatureException")
	void verifyVotingClientProofsWithInvalidSetupComponentTallyDataPayloadSignatureThrows() throws SignatureException {
		when(signatureKeystoreService.verifySignature(any(), any(), any(), any())).thenReturn(false);

		final InvalidPayloadSignatureException exception = assertThrows(InvalidPayloadSignatureException.class,
				() -> verifyVotingClientProofsService.verifyVotingClientProofs(electionEventId, verificationCardSetContext,
						setupComponentPublicKeys, confirmedEncryptedVotes));

		final String expected = String.format("Signature of payload %s is invalid. [electionEventId: %s, verificationCardSetId: %s]",
				SetupComponentTallyDataPayload.class.getSimpleName(), electionEventId, verificationCardSetId);
		assertEquals(expected, Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("valid parameters does not throw")
	void verifyVotingClientProofsWithValidParametersDoesNotThrow() {
		assertDoesNotThrow(() -> verifyVotingClientProofsService.verifyVotingClientProofs(electionEventId, verificationCardSetContext,
				setupComponentPublicKeys, confirmedEncryptedVotes));
	}

	private static SetupComponentTallyDataPayload getSetupComponentTallyDataPayload() {
		final GqGroup encryptionGroup = verificationCardSetContext.getPrimesMappingTable().getEncryptionGroup();
		final ElGamalGenerator elGamalGenerator = new ElGamalGenerator(encryptionGroup);
		final List<String> verificationCardIds = confirmedEncryptedVotes.stream()
				.map(EncryptedVerifiableVote::contextIds)
				.map(ContextIds::verificationCardId)
				.toList();
		final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> verificationCardPublicKeys = IntStream.range(0,
						verificationCardSetContext.getNumberOfVotingCards())
				.mapToObj(i -> elGamalGenerator.genRandomPublicKey(3))
				.collect(GroupVector.toGroupVector());
		return new SetupComponentTallyDataPayload(electionEventId, verificationCardSetId, "ballot box default title", encryptionGroup,
				verificationCardIds, verificationCardPublicKeys);
	}
}
