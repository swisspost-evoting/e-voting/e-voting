<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ (c) Copyright 2024 Swiss Post Ltd.
  -->
<project xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://maven.apache.org/POM/4.0.0" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">

	<modelVersion>4.0.0</modelVersion>

	<parent>
		<groupId>ch.post.it.evoting.securedatamanager</groupId>
		<artifactId>secure-data-manager</artifactId>
		<version>1.4.4.4</version>
	</parent>

	<artifactId>secure-data-manager-backend</artifactId>
	<name>Secure Data Manager - Backend</name>

	<properties>
		<orientdb.version>3.2.34</orientdb.version>
		<zip4j.version>2.11.5</zip4j.version>
	</properties>

	<dependencies>
		<!-- PROD DEPENDENCIES -->

		<!-- ch.post.it.evoting -->
		<dependency>
			<groupId>ch.post.it.evoting.cryptoprimitives</groupId>
			<artifactId>crypto-primitives</artifactId>
		</dependency>
		<dependency>
			<groupId>ch.post.it.evoting.domain</groupId>
			<artifactId>domain</artifactId>
		</dependency>
		<dependency>
			<groupId>ch.post.it.evoting.evotinglibraries</groupId>
			<artifactId>e-voting-libraries-direct-trust</artifactId>
		</dependency>
		<dependency>
			<groupId>ch.post.it.evoting.evotinglibraries</groupId>
			<artifactId>e-voting-libraries-domain</artifactId>
		</dependency>
		<dependency>
			<groupId>ch.post.it.evoting.evotinglibraries</groupId>
			<artifactId>e-voting-libraries-protocol-algorithms</artifactId>
		</dependency>
		<dependency>
			<groupId>ch.post.it.evoting.evotinglibraries</groupId>
			<artifactId>e-voting-libraries-xml</artifactId>
		</dependency>

		<!-- com.fasterxml.jackson -->
		<dependency>
			<groupId>com.fasterxml.jackson.core</groupId>
			<artifactId>jackson-annotations</artifactId>
		</dependency>
		<dependency>
			<groupId>com.fasterxml.jackson.core</groupId>
			<artifactId>jackson-core</artifactId>
		</dependency>
		<dependency>
			<groupId>com.fasterxml.jackson.core</groupId>
			<artifactId>jackson-databind</artifactId>
		</dependency>

		<dependency>
			<groupId>com.google.guava</groupId>
			<artifactId>guava</artifactId>
		</dependency>

		<dependency>
			<groupId>com.orientechnologies</groupId>
			<artifactId>orientdb-core</artifactId>
			<version>${orientdb.version}</version>
		</dependency>

		<!-- jakarta -->
		<dependency>
			<groupId>jakarta.annotation</groupId>
			<artifactId>jakarta.annotation-api</artifactId>
		</dependency>
		<dependency>
			<groupId>jakarta.xml.bind</groupId>
			<artifactId>jakarta.xml.bind-api</artifactId>
		</dependency>

		<dependency>
			<groupId>net.lingala.zip4j</groupId>
			<artifactId>zip4j</artifactId>
			<version>${zip4j.version}</version>
		</dependency>

		<!-- org.bouncycastle -->
		<dependency>
			<groupId>org.bouncycastle</groupId>
			<artifactId>bcprov-jdk18on</artifactId>
		</dependency>

		<!-- org.glassfish -->
		<dependency>
			<groupId>org.glassfish</groupId>
			<artifactId>jakarta.json</artifactId>
		</dependency>
		<dependency>
			<groupId>org.glassfish.jaxb</groupId>
			<artifactId>jaxb-runtime</artifactId>
			<scope>runtime</scope>
		</dependency>

		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-api</artifactId>
		</dependency>

		<!-- org.springframework -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-webflux</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-context</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.retry</groupId>
			<artifactId>spring-retry</artifactId>
		</dependency>
		<!-- Needed by spring-retry -->
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-aspects</artifactId>
		</dependency>

		<!-- DEV DEPENDENCIES -->

		<!-- ch.post.it.evoting -->
		<dependency>
			<groupId>ch.post.it.evoting.cryptoprimitives</groupId>
			<artifactId>crypto-primitives</artifactId>
			<classifier>tests</classifier>
			<type>test-jar</type>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>ch.post.it.evoting.domain</groupId>
			<artifactId>domain</artifactId>
			<type>test-jar</type>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>ch.post.it.evoting.evotinglibraries</groupId>
			<artifactId>e-voting-libraries-direct-trust</artifactId>
			<type>test-jar</type>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>ch.post.it.evoting.evotinglibraries</groupId>
			<artifactId>e-voting-libraries-domain</artifactId>
			<type>test-jar</type>
			<scope>test</scope>
		</dependency>

		<!-- org.junit.jupiter -->
		<dependency>
			<groupId>org.junit.jupiter</groupId>
			<artifactId>junit-jupiter-api</artifactId>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.junit.jupiter</groupId>
			<artifactId>junit-jupiter-params</artifactId>
			<scope>test</scope>
		</dependency>

		<!-- org.mockito -->
		<dependency>
			<groupId>org.mockito</groupId>
			<artifactId>mockito-core</artifactId>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.mockito</groupId>
			<artifactId>mockito-junit-jupiter</artifactId>
			<scope>test</scope>
		</dependency>

		<!-- org.springframework -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-actuator</artifactId>
		</dependency>
	</dependencies>

	<build>
		<resources>
			<resource>
				<filtering>false</filtering>
				<directory>src/main/resources</directory>
			</resource>
		</resources>
		<plugins>
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
				<configuration>
					<!-- The main class to start by executing java -jar -->
					<mainClass>ch.post.it.evoting.securedatamanager.SecureDataManagerApplication</mainClass>
					<classifier>runnable</classifier>
				</configuration>
				<executions>
					<execution>
						<goals>
							<goal>repackage</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-jar-plugin</artifactId>
				<executions>
					<execution>
						<goals>
							<goal>test-jar</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<configuration>
					<compilerArgs>
						<arg>-Xlint:all,-processing</arg>
					</compilerArgs>
					<showDeprecation>true</showDeprecation>
					<showWarnings>true</showWarnings>
				</configuration>
			</plugin>
			<plugin>
				<artifactId>maven-resources-plugin</artifactId>
				<executions>
					<execution>
						<id>copy-resources</id>
						<goals>
							<goal>copy-resources</goal>
						</goals>
						<!-- here the phase you need -->
						<phase>package</phase>
						<configuration>
							<outputDirectory>${basedir}/target/sdmConfig</outputDirectory>
							<resources>
								<resource>
									<directory>sdmConfig</directory>
								</resource>
							</resources>
						</configuration>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-release-plugin</artifactId>
				<configuration>
					<preparationGoals>clean replacer:replace verify</preparationGoals>
				</configuration>
			</plugin>
			<plugin>
				<groupId>ch.galinet</groupId>
				<artifactId>reproducible-build-maven-plugin</artifactId>
			</plugin>
		</plugins>
	</build>
</project>
