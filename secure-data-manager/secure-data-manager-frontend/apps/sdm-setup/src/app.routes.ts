/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {
	GenerateComponent,
	GeneratePrintFileComponent,
	PreComputeComponent,
	PreConfigureComponent,
} from '@sdm/setup-process-day1';
import {ConstituteElectoralBoardComponent} from '@sdm/setup-process-day2';
import {DataCollectionComponent} from '@sdm/shared-feature-data-collection';
import {
	ExportComponent,
	ImportComponent,
} from '@sdm/shared-feature-data-exchange';
import {SdmRoute, WorkflowStep} from '@sdm/shared-util-types';

export const appRoutes: SdmRoute[] = [
	{
		path: 'setup-1',
		children: [
			{
				path: 'pre-configure',
				component: PreConfigureComponent,
				title: 'preConfigure.title',
				data: {
					workflowStep: WorkflowStep.PreConfigure,
				},
			},
			{
				path: 'pre-compute',
				component: PreComputeComponent,
				title: 'preCompute.title',
				data: {
					workflowStep: WorkflowStep.PreCompute,
				},
			},
			{
				path: 'export-1',
				component: ExportComponent,
				title: 'dataExchange.export.title.1',
				data: {
					workflowStep: WorkflowStep.ExportToOnline1,
					exchangeIndex: '1',
				},
			},
		],
	},

	{
		path: 'setup-2',
		children: [
			{
				path: 'import-2',
				component: ImportComponent,
				title: 'dataExchange.import.title.2',
				data: {
					workflowStep: WorkflowStep.ImportFromOnline2,
					exchangeIndex: '2',
				},
			},
			{
				path: 'generate',
				component: GenerateComponent,
				title: 'generate.title',
				data: {
					workflowStep: WorkflowStep.Generate,
				},
			},
			{
				path: 'generate-print-file',
				component: GeneratePrintFileComponent,
				title: 'generatePrintFile.title',
				data: {
					workflowStep: WorkflowStep.GeneratePrintFile,
				},
			},
			{
				path: 'export-3',
				component: ExportComponent,
				title: 'dataExchange.export.title.3',
				data: {
					workflowStep: WorkflowStep.ExportToOnline3,
					exchangeIndex: '3',
				},
			},
		],
	},

	{
		path: 'setup-3',
		children: [
			{
				path: 'constitute-electoral-board',
				component: ConstituteElectoralBoardComponent,
				title: 'constituteElectoralBoard.title',
				data: {
					workflowStep: WorkflowStep.ConstituteElectoralBoard,
				},
			},
			{
				path: 'data-collect-setup',
				component: DataCollectionComponent,
				title: 'dataCollection.title.setup',
				data: {
					workflowStep: WorkflowStep.SetupDataCollection,
					mode: 'setup',
				},
			},
			{
				path: 'export-4',
				component: ExportComponent,
				title: 'dataExchange.export.title.4',
				data: {
					workflowStep: WorkflowStep.ExportToOnline4,
					exchangeIndex: '4',
					hideNextButton: true,
				},
			},
		],
	},

	{path: '', redirectTo: 'setup-1', pathMatch: 'full'},
];
