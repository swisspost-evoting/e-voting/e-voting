/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {CommonModule} from '@angular/common';
import {Component} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {RouterLinkNextDirective} from '@sdm/shared-ui-directives';
import {WorkflowStatus} from '@sdm/shared-util-types';
import {DataExchangeService} from '../data-exchange.service';
import {PageActionsComponent} from '@sdm/shared-ui-components';
import {ProgressComponent} from '@sdm/shared-feature-progress';
import {takeUntilDestroyed} from '@angular/core/rxjs-interop';
import {WorkflowStateService} from '@sdm/shared-ui-services';
import {ExportInformationComponent} from '../export-information/export-information.component';

@Component({
	selector: 'sdm-export',
	standalone: true,
	imports: [
		CommonModule,
		ReactiveFormsModule,
		TranslateModule,
		PageActionsComponent,
		RouterLinkNextDirective,
		ProgressComponent,
		ExportInformationComponent,
	],
	templateUrl: './export.component.html',
})
export class ExportComponent {
	exchangeIndex = '';
	hideNextButton = false;
	private workflowStatus!: WorkflowStatus;

	constructor(
		private readonly dataExchangeService: DataExchangeService,
		private readonly activatedRoute: ActivatedRoute,
		readonly workflowStates: WorkflowStateService,
	) {
		this.exchangeIndex = this.activatedRoute.snapshot.data['exchangeIndex'];
		this.hideNextButton = this.activatedRoute.snapshot.data['hideNextButton'];

		const step = this.activatedRoute.snapshot.data['workflowStep'];
		if (!step) return;
		workflowStates
			.get(step)
			.pipe(takeUntilDestroyed())
			.subscribe((state) => {
				this.workflowStatus = state.status;
			});
	}

	export() {
		this.dataExchangeService.export(this.exchangeIndex).subscribe();
	}

	get isComplete(): boolean {
		return this.isCurrentStatus(WorkflowStatus.Complete);
	}

	private isCurrentStatus(status: WorkflowStatus): boolean {
		return this.workflowStatus === status;
	}

	// @ts-ignore
	public importToExportLaptop(): string {
		if (
			this.exchangeIndex === '1' ||
			this.exchangeIndex === '3' ||
			this.exchangeIndex === '4'
		) {
			return 'assets/setup-online.gif';
		} else if (this.exchangeIndex === '2') {
			return 'assets/online-setup.gif';
		} else if (this.exchangeIndex === '5') {
			return 'assets/online-tally.gif';
		}
	}
}
