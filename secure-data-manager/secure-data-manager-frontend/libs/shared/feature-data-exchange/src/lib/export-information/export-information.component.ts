/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import { Component, Input, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { ExportInfo } from '@sdm/shared-util-types';
import { DataExchangeService } from '@sdm/shared-feature-data-exchange';

@Component({
  selector: 'sdm-export-information',
  standalone: true,
  imports: [CommonModule, TranslateModule],
  templateUrl: './export-information.component.html',
})
export class ExportInformationComponent implements OnInit {
  @Input({ required: true }) exchangeIndex!: string;
  exportInformation?: ExportInfo;

  constructor(private readonly dataExchangeService: DataExchangeService) {}

  ngOnInit() {
    this.dataExchangeService
      .getExportInfo(this.exchangeIndex)
      .subscribe((exportInformation) => {
        this.exportInformation = exportInformation;
      });
  }
}
