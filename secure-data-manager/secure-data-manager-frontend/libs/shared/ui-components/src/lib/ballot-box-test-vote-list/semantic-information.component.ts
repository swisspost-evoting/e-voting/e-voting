/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {Component, Input} from '@angular/core';
import {CommonModule} from "@angular/common";
import {TranslateModule, TranslateService} from "@ngx-translate/core";
import {MarkdownPipe} from "ngx-markdown";

@Component({
	selector: 'sdm-semantic-information',
	standalone: true,
	template: `
		<ng-container *ngFor="let segment of segments">
			<p [class.fst-italic]="isSpecialCase" [innerHTML]="segment | markdown: { inline: true } | async"></p>
		</ng-container>
	`,
	imports: [CommonModule, TranslateModule, MarkdownPipe],
})
export class SemanticInformationComponent {
	@Input() text?: string;
	protected isSpecialCase = false;


	constructor(private readonly translate: TranslateService) {

	}

	get segments(): string[] {
		if (this.text) {
			// Split the text by the pipe character, and remove the type element
			const parsedValues = this.text.split('|').slice(1);
			const firstSegment = parsedValues[0];

			// Define mapping for special cases
			const specialCases = {
				'EMPTY_CANDIDATE_POSITION-': 'ballotBoxTestVoteList.emptyCandidatePosition',
				'WRITE_IN_POSITION-': 'ballotBoxTestVoteList.writeInPosition'
			};

			// Check if first segment matches any special case
			this.isSpecialCase = false;
			for (const [prefix, translationKey] of Object.entries(specialCases)) {
				if (firstSegment.startsWith(prefix)) {
					this.isSpecialCase = true;
					parsedValues[0] = this.translate.instant(translationKey);
					break;
				}
			}

			// Remove the second element to prevent duplicate candidate names.
			// This removes the French translation if it's not a candidate,
			// providing a compromise to avoid modifying the current voting option representation.
			parsedValues.splice(1, 1);

			return parsedValues;

		} else {
			return [];
		}
	}
}
