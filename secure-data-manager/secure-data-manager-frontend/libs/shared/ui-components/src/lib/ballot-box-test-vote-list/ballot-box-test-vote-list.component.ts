/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {Component, Input} from '@angular/core';
import {CommonModule} from "@angular/common";
import {BallotBoxTestVote} from "@sdm/shared-util-types";
import {TranslateModule} from "@ngx-translate/core";
import {SemanticInformationComponent} from "./semantic-information.component";

@Component({
	selector: 'sdm-ballot-box-test-vote-list',
	standalone: true,
	imports: [CommonModule, TranslateModule, SemanticInformationComponent],
	templateUrl: './ballot-box-test-vote-list.component.html'
})
export class BallotBoxTestVoteListComponent {
	@Input() ballotBoxTestVotes?: BallotBoxTestVote[];
}
