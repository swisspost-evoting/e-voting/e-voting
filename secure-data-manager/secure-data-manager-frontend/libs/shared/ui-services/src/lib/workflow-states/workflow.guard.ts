/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {inject} from '@angular/core';
import {CanActivateChildFn, Router} from '@angular/router';
import {WorkflowStatus} from '@sdm/shared-util-types';
import {map} from 'rxjs';
import {RoutingService} from '../routing/routing.service';
import {WorkflowStateService} from './workflow-state.service';

export const workflowGuard: CanActivateChildFn = (childRoute, routerState) => {
	const path = childRoute.routeConfig?.path;
	const isIntermediary = path && !new RegExp(`${path}$`).test(routerState.url);

	const step = childRoute.data?.['workflowStep'];
	if (isIntermediary && !step) return true;

	const workflowStates = inject(WorkflowStateService);
	const routingService = inject(RoutingService);
	const router = inject(Router);

	return workflowStates.getSnapshot().pipe(
		map((stateList) => {
			if (step) {
				const state = stateList.find(
					(workflowState) => workflowState.step === step,
				);
				if (!state || state.status !== WorkflowStatus.Idle) return true;
			}

			const currentStep = stateList.find(
				(workflowState) => workflowState.status === WorkflowStatus.Ready || workflowState.status === WorkflowStatus.Error,
			);
			if (!currentStep) return true;

			const linkToCurrentStep = routingService.getLinkTo(currentStep.step);
			if (!linkToCurrentStep) return true;

			return router.createUrlTree(linkToCurrentStep);
		}),
	);
};
