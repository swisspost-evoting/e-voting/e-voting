/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {Component} from '@angular/core';
import {SimplebarAngularModule} from "simplebar-angular";

@Component({
	selector: 'sdm-scrollbar',
	imports: [SimplebarAngularModule],
	standalone: true,
	templateUrl: './scrollbar.component.html',
})
export class ScrollbarComponent {
}
