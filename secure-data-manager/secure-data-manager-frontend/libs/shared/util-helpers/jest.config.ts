/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */

/* eslint-disable */
export default {
  displayName: 'shared-util-helpers',
  preset: '../../../jest.preset.js',
  testEnvironment: 'node',
  transform: {
    '^.+\\.[tj]s$': ['ts-jest', { tsconfig: '<rootDir>/tsconfig.spec.json' }],
  },
  moduleFileExtensions: ['ts', 'js', 'html'],
  coverageDirectory: '../../../coverage/libs/shared/util-helpers',
};
