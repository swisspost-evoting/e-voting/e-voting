/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
export enum WorkflowStatus {
	Idle = 'IDLE',
	Ready = 'READY',
	InProgress = 'IN_PROGRESS',
	Complete = 'COMPLETE',
	Error = 'ERROR',
}
