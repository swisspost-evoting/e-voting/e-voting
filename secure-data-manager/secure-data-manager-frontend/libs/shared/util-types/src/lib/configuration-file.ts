/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */

export interface ConfigurationFile {
	alias: string;
	date: string;
	descriptions: { [language: string]: string };
}
