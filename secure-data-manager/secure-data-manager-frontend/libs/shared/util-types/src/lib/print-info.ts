/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */

export interface PrintInfo {
  outputFolder: string;
  filename: string;
}
