import {BallotBoxCategory} from "./ballot-box-category";

export interface BallotBox {
	id: string;
	defaultTitle: string;
	category: BallotBoxCategory;
	test: boolean;
	dateFrom: string;
	dateTo: string;
	gracePeriod: number;
}