/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
export enum ElectionEventStatus {
  Locked = 'LOCKED',
  Ready = 'READY',
}
