/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */

export interface ExportInfo {
	outputFolder: string;
	exportFilename: string;
}
