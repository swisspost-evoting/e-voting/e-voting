import {BallotBoxCategory} from "./ballot-box-category";

export interface BallotBoxTestVote {
	id: string;
	name: string;
	writeIns: [string[]];
	testVotes: [string[]];
}