/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import { Locale } from './locale';

export interface Environment {
  production: boolean;
  workflowEnabled: boolean;
  backendPath: string;
  remoteServerAvailable: boolean;
  locales: Locale[];
}
