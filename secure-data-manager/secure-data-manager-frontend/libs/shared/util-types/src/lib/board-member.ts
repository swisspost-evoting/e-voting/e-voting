/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
export interface BoardMember {
  id: string;
  name: string;
}
