/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */

export interface DecryptInput {
	ballotBoxIds: string[];
	electoralBoardPasswords: string[][];
}
