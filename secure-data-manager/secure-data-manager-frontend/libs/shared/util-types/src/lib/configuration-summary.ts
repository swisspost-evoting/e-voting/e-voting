/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */

export interface ConfigurationSummary {
  contestId: string;
  contestDescription: { [lang: string]: string };
  contestDate: Date;
  electionEventSeed: string;
  evotingFromDate: Date;
  evotingToDate: Date;
  gracePeriod: number;
  voterTotal: number;
  extendedAuthenticationType: string[];
  electoralBoard: ElectoralBoardSummary;
  electionGroups: ElectionGroupSummary[];
  votes: VoteSummary[];
  authorizations: AuthorizationSummary[];
  preconfigureSummary: PreconfigureSummary;
  configurationSignature: string;
}

export interface ElectionGroupSummary {
  electionGroupId: string;
  electionGroupDescription: Description[];
  electionGroupPosition: number;
  authorizations: AuthorizationSummary[];
  elections: ElectionSummary[];
}

export interface ElectionSummary {
  electionId: string;
  electionPosition: number;
  electionType: number;
  primarySecondaryType: number;
  electionDescription: Description[];
  numberOfMandates: number;
  writeInsAllowed: boolean;
  candidateAccumulation: number;
  candidates: CandidateSummary[];
  lists: ListSummary[];
}

export interface CandidateSummary {
  candidateId: string;
  familyName: string;
  firstName: string;
  callName: string;
  dateOfBirth: Date;
  isIncumbent: boolean;
  referenceOnPosition: string;
  eligibility: string;
}

export interface ListSummary {
  listId: string;
  listIndentureNumber: string;
  listDescription: Description[];
  listOrderOfPrecedence: number;
  isEmpty: boolean;
  candidates: CandidateSummary[];
}

export interface VoteSummary {
  voteId: string;
  votePosition: number;
  voteDescription: {
    [language: string]: string;
  };
  authorizations: AuthorizationSummary[];
  ballots: BallotSummary[];
}

export interface BallotSummary {
  ballotId: string;
  ballotPosition: number;
  ballotDescription: Description[];
  questions: QuestionSummary[];
}

export interface QuestionSummary {
  questionNumber: string;
  questionInfo: Description[];
  answers: AnswerSummary[];
}

export interface AnswerSummary {
  answerPosition: number;
  answerInfo: {
    [language: string]: string;
  };
}

export interface ElectoralBoardSummary {
  electoralBoardId: string;
  electoralBoardName: string;
  electoralBoardDescription: string;
  members: string[];
}

export interface AuthorizationSummary {
  authorizationId: string;
  authorizationName: string;
  authorizationAlias: string;
  isTest: boolean;
  fromDate: Date;
  toDate: Date;
  numberOfVoters: number;
  authorizationObjects: AuthorizationObjectSummary[];
}

export interface AuthorizationObjectSummary {
  domainOfInfluenceId: string;
  domainOfInfluenceType: string;
  domainOfInfluenceName: string;
  countingCircleId: string;
  countingCircleName: string;
}

export interface PreconfigureSummary {
  encryptionGroup: string;
  maximumNumberOfVotingOptions: number;
  maximumNumberOfSelections: number;
  maximumNumberOfWriteInsPlusOne: number;
  verificationCardSets: VerificationCardSetSummary[];
}

export interface VerificationCardSetSummary {
  verificationCardSetAlias: string;
  testBallotBox: boolean;
  numberOfVotingCards: number;
  numberOfVotingOptions: number;
  gracePeriod: number;
}

export interface Description {
  language: string;
  shortDescription: string;
  longDescription: string;
}

export interface ContestSummary {
  id: string;
  position: number;
  electionGroup?: ElectionGroupSummary;
  vote?: VoteSummary;
}
