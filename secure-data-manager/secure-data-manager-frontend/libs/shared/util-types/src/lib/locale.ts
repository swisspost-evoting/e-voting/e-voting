/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
export interface Locale {
  id: string;
  name: string;
  data: unknown;
}
