/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */

export interface VotingServerHealth {
  status: boolean;
  serverName: string;
}
