/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {CommonModule} from '@angular/common';
import {Component, Input} from '@angular/core';
import {TranslateModule} from '@ngx-translate/core';
import {PageActionsComponent} from '@sdm/shared-ui-components';
import {ElectoralBoardSummary} from '@sdm/shared-util-types';
import {RouterLinkNextDirective} from '@sdm/shared-ui-directives';
import {ProgressComponent} from '@sdm/shared-feature-progress';
import {MarkdownPipe} from 'ngx-markdown';

@Component({
  selector: 'sdm-electoral-board-summary',
  standalone: true,
  imports: [
    CommonModule,
    TranslateModule,
    PageActionsComponent,
    RouterLinkNextDirective,
    ProgressComponent,
    MarkdownPipe,
  ],
  templateUrl: './electoral.board.summary.component.html',
})
export class ElectoralBoardSummaryComponent {
  @Input({ required: true }) electoralBoardSummary!: ElectoralBoardSummary;
}
