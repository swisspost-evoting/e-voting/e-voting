/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {CommonModule} from '@angular/common';
import {Component, Input, OnChanges, SimpleChanges} from '@angular/core';
import {TranslateModule} from '@ngx-translate/core';
import {PageActionsComponent} from '@sdm/shared-ui-components';
import {AuthorizationSummary, ElectionGroupSummary,} from '@sdm/shared-util-types';
import {RouterLinkNextDirective} from '@sdm/shared-ui-directives';
import {ProgressComponent} from '@sdm/shared-feature-progress';
import {MarkdownPipe} from 'ngx-markdown';

@Component({
  selector: 'sdm-election-summary',
  standalone: true,
  imports: [
    CommonModule,
    TranslateModule,
    PageActionsComponent,
    RouterLinkNextDirective,
    ProgressComponent,
    MarkdownPipe,
  ],
  templateUrl: './election.summary.component.html',
})
export class ElectionSummaryComponent implements OnChanges {
  @Input({ required: true }) electionGroupSummary!: ElectionGroupSummary;
  testAuthorizations: AuthorizationSummary[] = [];
  regularAuthorizations: AuthorizationSummary[] = [];

  private setAuthorizations() {
    const authorizations = this.electionGroupSummary.authorizations;

    this.regularAuthorizations = authorizations.filter(
      (authorization) => !authorization.isTest,
    );
    this.testAuthorizations = authorizations.filter(
      (authorization) => authorization.isTest,
    );
  }

  ngOnChanges(changes: SimpleChanges) {
    if (!changes['electionGroupSummary']) return;

    this.setAuthorizations();
  }
}
