/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { MockProvider } from 'ng-mocks';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { TranslateModule } from '@ngx-translate/core';
import { SummaryService, ToastService } from '@sdm/shared-ui-services';
import { SummaryComponent } from './summary.component';

describe('SummaryComponent', () => {
  let component: SummaryComponent;
  let fixture: ComponentFixture<SummaryComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      providers: [
        MockProvider(ActivatedRoute, {
          data: of({
            exchangeIndex: 'testExchangeIndex',
            nextRoute: 'testNextRoute',
          }),
        }),
        MockProvider(ToastService),
        MockProvider(SummaryService),
      ],
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SummaryComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
