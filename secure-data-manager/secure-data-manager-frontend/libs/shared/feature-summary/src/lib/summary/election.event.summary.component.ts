/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {CommonModule} from '@angular/common';
import {Component, Input} from '@angular/core';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {PageActionsComponent} from '@sdm/shared-ui-components';
import {ConfigurationSummary} from '@sdm/shared-util-types';
import {RouterLinkNextDirective} from '@sdm/shared-ui-directives';
import {ProgressComponent} from '@sdm/shared-feature-progress';
import {MarkdownPipe} from 'ngx-markdown';

@Component({
	selector: 'sdm-election-event-summary',
	standalone: true,
	imports: [
		CommonModule,
		TranslateModule,
		PageActionsComponent,
		RouterLinkNextDirective,
		ProgressComponent,
		MarkdownPipe,
	],
	templateUrl: './election.event.summary.component.html',
})
export class ElectionEventSummaryComponent {
	@Input({required: true}) configurationSummary!: ConfigurationSummary;

	constructor(private readonly translate: TranslateService) {
	}

	get eventName() {
		if (!this.configurationSummary) return '';
		return (
			this.configurationSummary.contestDescription[this.translate.currentLang] ||
			this.configurationSummary.contestDescription[this.translate.defaultLang]
		);
	}
}
