/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {CommonModule} from '@angular/common';
import {Component, Input, OnInit} from '@angular/core';
import {TranslateModule} from '@ngx-translate/core';
import {PageActionsComponent} from '@sdm/shared-ui-components';
import {AuthorizationSummary, ConfigurationSummary, ContestSummary,} from '@sdm/shared-util-types';
import {RouterLinkNextDirective} from '@sdm/shared-ui-directives';
import {ProgressComponent} from '@sdm/shared-feature-progress';
import {SummaryService} from '@sdm/shared-ui-services';
import {MarkdownPipe} from 'ngx-markdown';
import {ElectionEventSummaryComponent} from './election.event.summary.component';
import {ElectoralBoardSummaryComponent} from './electoral.board.summary.component';
import {AuthorizationSummaryComponent} from './authorization.summary.component';
import {ElectionSummaryComponent} from './election.summary.component';
import {VoteSummaryComponent} from './vote.summary.component';

@Component({
	selector: 'sdm-configuration-summary',
	standalone: true,
	imports: [
		CommonModule,
		TranslateModule,
		PageActionsComponent,
		RouterLinkNextDirective,
		ProgressComponent,
		MarkdownPipe,
		ElectionEventSummaryComponent,
		ElectoralBoardSummaryComponent,
		AuthorizationSummaryComponent,
		ElectionSummaryComponent,
		VoteSummaryComponent,
	],
	templateUrl: './summary.component.html',
})
export class SummaryComponent implements OnInit {
	@Input({required: false})
	configurationSummary?: ConfigurationSummary;
	contests: ContestSummary[] = [];
	authorizations: AuthorizationSummary[] = [];

	constructor(
		private readonly summaryService: SummaryService,
	) {
	}

	private sortByName(a: AuthorizationSummary, b: AuthorizationSummary): number {
		return a.authorizationName.localeCompare(b.authorizationName);
	}

	private updateDetails() {
		const electionContests: ContestSummary[] =
			this.configurationSummary!.electionGroups.map((electionGroup) => {
				return {
					id: electionGroup.electionGroupId,
					position: electionGroup.electionGroupPosition,
					electionGroup: electionGroup,
				};
			});

		const voteContests: ContestSummary[] = this.configurationSummary!.votes.map(
			(vote) => {
				return {
					id: vote.voteId,
					position: vote.votePosition,
					vote: vote,
				};
			},
		);

		this.contests = [...electionContests, ...voteContests].sort(
			(a, b) => a.position - b.position,
		);

		this.authorizations = this.configurationSummary!.authorizations.sort(
			(a, b) => this.sortByName(a, b),
		);
	}

	ngOnInit(): void {
		if (this.configurationSummary) {
			this.updateDetails();
			return;
		}

		this.summaryService
			.getConfigurationSummary()
			.subscribe((configurationSummary) => {
				this.configurationSummary = configurationSummary;
				this.updateDetails();
			});
	}
}
