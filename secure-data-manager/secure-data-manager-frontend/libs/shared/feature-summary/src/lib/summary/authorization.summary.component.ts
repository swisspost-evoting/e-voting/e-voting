/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {CommonModule} from '@angular/common';
import {Component, Input, OnInit} from '@angular/core';
import {TranslateModule} from '@ngx-translate/core';
import {PageActionsComponent} from '@sdm/shared-ui-components';
import {AuthorizationSummary} from '@sdm/shared-util-types';
import {RouterLinkNextDirective} from '@sdm/shared-ui-directives';
import {ProgressComponent} from '@sdm/shared-feature-progress';
import {MarkdownPipe} from 'ngx-markdown';

@Component({
	selector: 'sdm-authorization-summary',
	standalone: true,
	imports: [
		CommonModule,
		TranslateModule,
		PageActionsComponent,
		RouterLinkNextDirective,
		ProgressComponent,
		MarkdownPipe,
	],
	templateUrl: './authorization.summary.component.html',
})
export class AuthorizationSummaryComponent implements OnInit {
	@Input({required: true}) authorizationSummary!: AuthorizationSummary[];
	types = ['Regular', 'Test'];
	authorizationsByType = new Map<string, AuthorizationSummary[]>();

	private sortByName(a: AuthorizationSummary, b: AuthorizationSummary): number {
		return a.authorizationName.localeCompare(b.authorizationName);
	}

	ngOnInit(): void {
		this.authorizationsByType.set('Regular', this.authorizationSummary.filter((authorization) => !authorization.isTest).sort(this.sortByName));
		this.authorizationsByType.set('Test', this.authorizationSummary.filter((authorization) => authorization.isTest).sort(this.sortByName));
	}
}
