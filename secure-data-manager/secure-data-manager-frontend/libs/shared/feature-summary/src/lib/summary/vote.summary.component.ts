/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {CommonModule} from '@angular/common';
import {Component, Input, OnChanges, SimpleChanges} from '@angular/core';
import {TranslateModule} from '@ngx-translate/core';
import {PageActionsComponent} from '@sdm/shared-ui-components';
import {AuthorizationSummary, QuestionSummary, VoteSummary,} from '@sdm/shared-util-types';
import {RouterLinkNextDirective} from '@sdm/shared-ui-directives';
import {ProgressComponent} from '@sdm/shared-feature-progress';
import {MarkdownPipe} from 'ngx-markdown';

@Component({
  selector: 'sdm-vote-summary',
  standalone: true,
  imports: [
    CommonModule,
    TranslateModule,
    PageActionsComponent,
    RouterLinkNextDirective,
    ProgressComponent,
    MarkdownPipe,
  ],
  templateUrl: './vote.summary.component.html',
})
export class VoteSummaryComponent implements OnChanges {
  @Input({ required: true }) voteSummary!: VoteSummary;
  testAuthorizations: AuthorizationSummary[] = [];
  regularAuthorizations: AuthorizationSummary[] = [];
  questions: QuestionSummary[] = [];

  private setAuthorizations() {
    const authorizations = this.voteSummary.authorizations;

    this.regularAuthorizations = authorizations.filter(
      (authorization) => !authorization.isTest,
    );
    this.testAuthorizations = authorizations.filter(
      (authorization) => authorization.isTest,
    );
    this.questions = this.voteSummary.ballots.flatMap(
      (ballot) => ballot.questions,
    );
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!changes['voteSummary']) return;

    this.setAuthorizations();
  }
}
