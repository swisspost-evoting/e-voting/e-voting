/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */

import {CommonModule} from '@angular/common';
import {Component, ViewChild} from '@angular/core';
import {ActivatedRouteSnapshot, RouterOutlet} from '@angular/router';
import {HeaderComponent} from '../header/header.component';
import {StepperComponent} from '../stepper/stepper.component';
import {environment} from "@sdm/shared-ui-config";

@Component({
	selector: 'sdm-root',
	standalone: true,
	imports: [
		CommonModule,
		HeaderComponent,
		RouterOutlet,
		StepperComponent,
	],
	templateUrl: './app.component.html',
})
export class AppComponent {
	@ViewChild('routerOutlet', {read: RouterOutlet}) routerOutlet!: RouterOutlet;
	activatedRouteSnapshot?: ActivatedRouteSnapshot;
	workflowEnabled?: boolean;

	constructor() {
		this.workflowEnabled = environment.workflowEnabled;
	}

	registerRoute() {
		this.activatedRouteSnapshot = this.routerOutlet.activatedRoute.snapshot;
	}
}
