/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
export const toastModuleConfig = {
  timeOut: 5000,
  extendedTimeOut: 1000,
  toastClass: 'toast show',
  titleClass: 'toast-header',
  messageClass: 'toast-body',
  iconClasses: {
    success: 'toast-success',
    error: 'toast-danger',
    warning: 'toast-warning',
    info: 'toast-light',
  },
};
