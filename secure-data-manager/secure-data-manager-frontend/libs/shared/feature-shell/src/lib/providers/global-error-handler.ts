/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */

import {ErrorHandler, Injectable, Injector} from '@angular/core';
import {environment} from '@sdm/shared-ui-config';
import {ToastService} from '@sdm/shared-ui-services';

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {
  constructor(private readonly injector: Injector) {}

  private _toastService?: ToastService;

  get toastService(): ToastService {
    if (!this._toastService)
      this._toastService = this.injector.get(ToastService);
    return this._toastService;
  }

  handleError(error?: unknown) {
    this.toastService.error('error.generic');

    if (error && !environment.production) {
      console.error(error);
    }
  }
}
