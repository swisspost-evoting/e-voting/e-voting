import {RoutingService} from '@sdm/shared-ui-services';
import {RouterLink} from '@angular/router';
import {Directive} from '@angular/core';

@Directive({
  selector: '[sdmRouterLinkNext]',
  standalone: true,
  hostDirectives: [RouterLink]
})
export class RouterLinkNextDirective {
  constructor(
    readonly routerLink: RouterLink,
    readonly routingService: RoutingService,
  ) {
    this.routerLink.routerLink = this.routingService.getLinkToNextStep();
  }
}
