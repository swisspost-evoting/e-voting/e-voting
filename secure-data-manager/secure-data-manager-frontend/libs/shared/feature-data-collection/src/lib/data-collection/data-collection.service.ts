/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from "rxjs";
import {DatasetInfo} from "@sdm/shared-util-types";
import {environment} from '@sdm/shared-ui-config';

@Injectable({
	providedIn: 'root',
})
export class DataCollectionService {
	private urlSuffix = `collect-verifier-data`;

	constructor(private httpClient: HttpClient) {
	}

	getDatasetFilenameList(mode: string): Observable<DatasetInfo> {
		return this.httpClient.get<DatasetInfo>(`${environment.backendPath}/sdm-${mode}/${this.urlSuffix}`);
	}

	collect(mode: string): void {
		this.httpClient.post(`${environment.backendPath}/sdm-${mode}/${this.urlSuffix}`, null).subscribe();
	}
}
