/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {CommonModule} from '@angular/common';
import {Component} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {PageActionsComponent} from '@sdm/shared-ui-components';
import {DatasetInfo, WorkflowStatus} from '@sdm/shared-util-types';
import {DataCollectionService} from './data-collection.service';
import {RouterLinkNextDirective} from '@sdm/shared-ui-directives';
import {ProgressComponent} from "@sdm/shared-feature-progress";

@Component({
	selector: 'sdm-data-collection',
	standalone: true,
	imports: [
		CommonModule,
		TranslateModule,
		PageActionsComponent,
		RouterLinkNextDirective,
		ProgressComponent,
	],
	templateUrl: './data-collection.component.html',
})
export class DataCollectionComponent {
	mode = '';
	hideNextButton = false;
	datasetInfo?: DatasetInfo;

	constructor(
		private readonly dataCollectionService: DataCollectionService,
		private readonly route: ActivatedRoute,
	) {
		this.mode = this.route.snapshot.data['mode'];
		this.hideNextButton = this.route.snapshot.data['hideNextButton'];

		this.dataCollectionService
			.getDatasetFilenameList(this.mode)
			.subscribe((datasetInfo) => {
				this.datasetInfo = datasetInfo;
			});
	}

	collect() {
		this.dataCollectionService.collect(this.mode);
	}

	get datasetFilenameList(): string[] {
		if (!this.datasetInfo) return [];
		return this.datasetInfo.filenames;
	}
}
