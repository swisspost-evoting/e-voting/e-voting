/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {RandomItem} from './random-item';

export function RandomBoolean(): boolean {
  return RandomItem([true, false]);
}
