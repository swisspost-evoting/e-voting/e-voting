/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */

import {Component} from '@angular/core';

@Component({
  template: '',
  standalone: true,
})
export class MockComponent {}
