# shared-util-testing

This library was generated with [Nx](https://nx.dev).

## Building

Run `nx build shared-util-testing` to build the library.

## Running unit tests

Run `nx test shared-util-testing` to execute the unit tests via [Jest](https://jestjs.io).
