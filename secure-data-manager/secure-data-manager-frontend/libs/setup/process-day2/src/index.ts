/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */

export * from './lib/constitute-electoral-board/constitute-electoral-board.component';
