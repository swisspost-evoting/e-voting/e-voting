/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {CommonModule} from '@angular/common';
import {Component} from '@angular/core';
import {TranslateModule} from '@ngx-translate/core';
import {PageActionsComponent} from '@sdm/shared-ui-components';
import {RouterLinkNextDirective} from '@sdm/shared-ui-directives';
import {ProgressComponent} from '@sdm/shared-feature-progress';
import {PreComputeService} from './pre-compute.service';

@Component({
	selector: 'sdm-pre-compute',
	standalone: true,
	imports: [
	  CommonModule,
	  TranslateModule,
	  ProgressComponent,
	  PageActionsComponent,
	  RouterLinkNextDirective,
	],
	templateUrl: './pre-compute.component.html',
})
export class PreComputeComponent {
	constructor(private readonly preComputeService: PreComputeService) {}
  
	preCompute() {
	  this.preComputeService.preComputeVotingCardSets();
	}
}
