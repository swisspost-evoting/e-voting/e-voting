/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from "@sdm/shared-ui-config";

@Injectable({
	providedIn: 'root',
})
export class PreComputeService {
	private url = `${environment.backendPath}/sdm-setup/pre-compute`;

	constructor(private httpClient: HttpClient) {
	}

	preComputeVotingCardSets(): void {
		this.httpClient.post(this.url, {}).subscribe();
	}
}
