/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {ConfigurationSummary} from '@sdm/shared-util-types';
import {Observable} from 'rxjs';
import {environment} from '@sdm/shared-ui-config';

@Injectable({
	providedIn: 'root',
})
export class PreConfigureService {
	private url = `${environment.backendPath}/sdm-setup/pre-configure`;

	constructor(private httpClient: HttpClient) {
	}

	previewSummary(): Observable<ConfigurationSummary> {
		return this.httpClient.get<ConfigurationSummary>(`${environment.backendPath}/sdm-setup/pre-configure/preview`);
	}

	preConfigureElectionEvent(): void {
		this.httpClient.post(this.url, null).subscribe();
	}
}
