/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {CommonModule} from '@angular/common';
import {Component, DestroyRef} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';
import {PageActionsComponent, PolicyComponent} from '@sdm/shared-ui-components';
import {RouterLinkNextDirective} from '@sdm/shared-ui-directives';
import {
	ConfigurationSummary,
	WorkflowStatus,
	WorkflowStep,
} from '@sdm/shared-util-types';
import {NgxMaskDirective} from 'ngx-mask';
import {PreConfigureService} from './pre-configure.service';
import {ProgressComponent} from '@sdm/shared-feature-progress';
import {SummaryService, WorkflowStateService} from '@sdm/shared-ui-services';
import {SummaryComponent} from '@sdm/shared-feature-summary';
import {MarkdownPipe} from 'ngx-markdown';
import {ScrollbarComponent} from '@sdm/shared-feature-scrollbar';
import {catchError, EMPTY, switchMap} from 'rxjs';
import {takeUntilDestroyed} from '@angular/core/rxjs-interop';

@Component({
	selector: 'sdm-preconfigure',
	standalone: true,
	imports: [
		CommonModule,
		TranslateModule,
		NgxMaskDirective,
		PolicyComponent,
		PageActionsComponent,
		RouterLinkNextDirective,
		ProgressComponent,
		MarkdownPipe,
		FormsModule,
		ScrollbarComponent,
		SummaryComponent,
		TranslateModule,
	],
	templateUrl: './pre-configure.component.html',
})
export class PreConfigureComponent {
	loadingSummaryError = false;
	workflowStatus: WorkflowStatus = WorkflowStatus.Idle;
	loadingSummary = false;
	isContestConfirmed = false;
	configurationSummary: ConfigurationSummary | null = null;
	protected readonly WorkflowStatus = WorkflowStatus;

	constructor(
		private readonly preConfigureService: PreConfigureService,
		private readonly summaryService: SummaryService,
		private readonly workflowStates: WorkflowStateService,
		private readonly destroyRef: DestroyRef,
	) {
		this.loadConfigurationSummary();
	}

	reload() {
		this.configurationSummary = null;
		this.loadConfigurationSummary();
	}

	preConfigure() {
		this.preConfigureService.preConfigureElectionEvent();
	}

	private loadConfigurationSummary() {
		this.workflowStates
			.get(WorkflowStep.PreConfigure)
			.pipe(
				switchMap((state) => {
						this.workflowStatus = state.status

						if (state.status === WorkflowStatus.Ready) {
							this.loadingSummary = true;
							this.loadingSummaryError = false;
							return this.preConfigureService.previewSummary();
						}
						if (state.status === WorkflowStatus.Complete) {
							this.loadingSummary = true;
							this.loadingSummaryError = false;
							return this.summaryService.getConfigurationSummary();
						}

						return EMPTY;
					}
				),
				catchError(() => {
					this.loadingSummaryError = true
					this.loadingSummary = false;
					return EMPTY;
				}),
				takeUntilDestroyed(this.destroyRef)
			)
			.subscribe((configurationSummary) => {
				this.configurationSummary = configurationSummary;
				this.loadingSummary = false;
			});
	}
}
