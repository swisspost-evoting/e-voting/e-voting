/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import { CommonModule, DatePipe } from '@angular/common';
import { Component, DestroyRef } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { PageActionsComponent } from '@sdm/shared-ui-components';
import { RouterLinkNextDirective } from '@sdm/shared-ui-directives';
import { GeneratePrintFileService } from './generate-print-file.service';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { ProgressComponent } from '@sdm/shared-feature-progress';
import { PrintInfo } from '@sdm/shared-util-types';

@Component({
  selector: 'sdm-generate-print-file',
  standalone: true,
  providers: [DatePipe],
  imports: [
    CommonModule,
    TranslateModule,
    PageActionsComponent,
    RouterLinkNextDirective,
    ProgressComponent,
  ],
  templateUrl: './generate-print-file.component.html',
})
export class GeneratePrintFileComponent {
  printInfo?: PrintInfo;

  constructor(
    private generateService: GeneratePrintFileService,
    private readonly destroyRef: DestroyRef,
  ) {
    this.generateService.getPrintInfo().subscribe((printInfo) => {
      this.printInfo = printInfo;
    });
  }

  generatePrintFile() {
    this.generateService
      .generatePrintFile()
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe();
  }
}
