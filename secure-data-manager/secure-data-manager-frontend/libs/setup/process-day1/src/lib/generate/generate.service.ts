import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from "@sdm/shared-ui-config";

@Injectable({
	providedIn: 'root',
})
export class GenerateService {
	private url = `${environment.backendPath}/sdm-setup/generate`;

	constructor(private http: HttpClient) {
	}

	generate(): void {
		this.http.post(this.url, null).subscribe();
	}
}
