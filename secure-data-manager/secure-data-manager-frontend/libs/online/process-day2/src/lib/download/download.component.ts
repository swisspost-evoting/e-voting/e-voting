/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {Component} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TranslateModule} from '@ngx-translate/core';
import {PageActionsComponent} from '@sdm/shared-ui-components';
import {RouterLinkNextDirective} from '@sdm/shared-ui-directives';
import { ProgressComponent } from '@sdm/shared-feature-progress';
import {DownloadService} from './download.service';

@Component({
  selector: 'sdm-download',
  standalone: true,
  imports: [
    CommonModule,
    TranslateModule,
    ProgressComponent,
    PageActionsComponent,
    RouterLinkNextDirective,
  ],
  templateUrl: './download.component.html',
})
export class DownloadComponent {
  constructor(private downloadService: DownloadService) {}

  download() {
    this.downloadService.download();
  }
}
