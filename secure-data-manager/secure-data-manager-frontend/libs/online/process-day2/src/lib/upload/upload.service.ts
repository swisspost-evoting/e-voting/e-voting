/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '@sdm/shared-ui-config';
import {Observable} from 'rxjs';

@Injectable({
	providedIn: 'root',
})
export class UploadService {
	private url = `${environment.backendPath}/sdm-online/upload`;

	constructor(private httpClient: HttpClient) {
	}

	upload(day: number): Observable<Object> {
		return this.httpClient.post(this.url, day);
	}

    getVoterPortalUrl(): Observable<string> {
      return this.httpClient.get<string>(`${this.url}/voter-portal-url`);
    }
}
