/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {CommonModule} from '@angular/common';
import {Component} from '@angular/core';
import {TranslateModule} from '@ngx-translate/core';
import {RouterLinkNextDirective} from '@sdm/shared-ui-directives';
import {PageActionsComponent} from '@sdm/shared-ui-components';
import {RequestCcKeysService} from './request-cc-keys.service';
import {ProgressComponent} from "@sdm/shared-feature-progress";

@Component({
  selector: 'sdm-request-cc-keys',
  standalone: true,
  imports: [
    CommonModule,
    TranslateModule,
    PageActionsComponent,
    RouterLinkNextDirective,
    ProgressComponent,
  ],
  templateUrl: './request-cc-keys.component.html',
})
export class RequestCcKeysComponent {
  constructor(private requestCcKeysService: RequestCcKeysService) {}

  requestCcKeys() {
    this.requestCcKeysService.requestCcKeys();
  }
}
