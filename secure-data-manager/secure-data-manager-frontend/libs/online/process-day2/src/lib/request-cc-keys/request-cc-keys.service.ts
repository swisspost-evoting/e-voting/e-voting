/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from "@sdm/shared-ui-config";

@Injectable({
	providedIn: 'root',
})
export class RequestCcKeysService {
	private url = `${environment.backendPath}/sdm-online/request-cc-keys`;

	constructor(private httpClient: HttpClient) {
	}

	requestCcKeys(): void {
		this.httpClient.post(this.url, null).subscribe();
	}
}
