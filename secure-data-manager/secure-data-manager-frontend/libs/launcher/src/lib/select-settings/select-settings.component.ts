/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {CommonModule} from '@angular/common';
import {Component, OnInit} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {Router, RouterLink} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';

import {Settings, WorkflowStatus} from '@sdm/shared-util-types';
import {PageActionsComponent} from '@sdm/shared-ui-components';
import {RouterLinkNextDirective} from '@sdm/shared-ui-directives';

@Component({
	selector: 'sdm-select-settings',
	standalone: true,
	imports: [
		CommonModule,
		TranslateModule,
		RouterLink,
		FormsModule,
		PageActionsComponent,
		RouterLinkNextDirective,
	],
	templateUrl: './select-settings.component.html',
})
export class SelectSettingsComponent implements OnInit {
	settingsList: Settings[] = [];
	selectedSettings: Settings | null = null;

	protected readonly WorkflowStatus = WorkflowStatus;

	constructor(private readonly router: Router) {
	}

	selectSettings() {
		this.router.navigate(['manage-settings'], {
			state: {settings: this.selectedSettings},
		});
	}

	copySettings() {
		this.router.navigate(['manage-settings'], {
			state: {
				settings: {
					...this.selectedSettings,
					id: -1,
					workspaceFolder: '',
					outputFolder: '',
					printFolder: '',
					verifierFolder: '',
				},
			},
		});
	}

	addSettings() {
		this.router.navigate(['manage-settings'], {
			state: {
				settings: {
					id: -1,
				},
			},
		});
	}

	ngOnInit(): void {
		this.settingsList = window.SettingsApi?.getSettingsList();
	}
}
