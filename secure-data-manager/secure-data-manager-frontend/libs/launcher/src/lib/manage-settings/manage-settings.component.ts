/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {CommonModule} from '@angular/common';
import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, ReactiveFormsModule, Validators} from '@angular/forms';
import {Router, RouterLink} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';

import {Settings} from '@sdm/shared-util-types';
import {SeedValidator} from './seed.validator';
import {PasswordValidators} from '@sdm/shared-feature-passwords';
import {PolicyComponent} from '@sdm/shared-ui-components';
import {NgxMaskDirective} from 'ngx-mask';
import {ScrollbarComponent} from '@sdm/shared-feature-scrollbar';

@Component({
	selector: 'sdm-select-settings',
	standalone: true,
	imports: [
		CommonModule,
		TranslateModule,
		RouterLink,
		ReactiveFormsModule,
		PolicyComponent,
		NgxMaskDirective,
		ScrollbarComponent,
	],
	templateUrl: './manage-settings.component.html',
})
export class ManageSettingsComponent implements OnInit {
	settings: Settings | null = null;
	startingSDM = false;
	workspaceValid = true;
	hidePassword = true;
	sdmModes: string[] = ['SETUP', 'ONLINE', 'TALLY'];

	chunkSizes = [
		{value: '100', label: 'normalElectionEvent'},
		{value: '20', label: 'bigElectionEvent'},
	];
	passwordPolicies = [
		'length',
		'digit',
		'specialChar',
		'lowerCaseChar',
		'uppercaseChar',
	];
	electionEventSeedPolicies = ['prefix', 'date', 'suffix', 'delimiter'];

	settingsForm: FormGroup<{
		electionEventSeed: FormControl<string>;
		mode: FormControl<string>;
		onlineHost: FormControl<string>;
		twoWaySSLLocation: FormControl<string>;
		twoWaySSLPwdLocation: FormControl<string>;
		outputFolder: FormControl<string>;
		externalConfigurationFolder: FormControl<string>;
		printFolder: FormControl<string>;
		verifierFolder: FormControl<string>;
		workspaceFolder: FormControl<string>;
		password: FormControl<string>;
		choiceCodeGenerationChunkSize: FormControl<number>;
		directTrustLocation: FormControl<string>;
		directTrustPwdLocation: FormControl<string>;
	}>;

	constructor(
		private readonly router: Router,
		private readonly fb: FormBuilder,
	) {
		this.settingsForm = this.fb.group(
			{
				electionEventSeed: [
					'',
					{validators: [Validators.required, SeedValidator.validate()]},
				],
				mode: ['', {validators: [Validators.required]}],
				onlineHost: [''],
				twoWaySSLLocation: [''],
				twoWaySSLPwdLocation: [''],
				workspaceFolder: ['', {validators: [Validators.required]}],
				outputFolder: [''],
				externalConfigurationFolder: [''],
				printFolder: [''],
				verifierFolder: [''],
				password: [
					'',
					{validators: [Validators.required, PasswordValidators.validate]},
				],
				choiceCodeGenerationChunkSize: [100],
				directTrustLocation: [''],
				directTrustPwdLocation: [''],
			},
			{validators: this.modeValidator},
		);
	}

	get mode(): FormControl<string> {
		return this.settingsForm.controls.mode;
	}

	get password(): FormControl<string> {
		return this.settingsForm.controls.password;
	}

	modeValidator(form: FormGroup) {
		const mode = form.controls['mode'].value;
		const onlineHost = form.controls['onlineHost'].value;
		const directTrustLocation = form.controls['directTrustLocation'].value;
		const directTrustPwdLocation =
			form.controls['directTrustPwdLocation'].value;
		const choiceCodeGenerationChunkSize =
			form.controls['choiceCodeGenerationChunkSize'].value;
		const outputFolder = form.controls['outputFolder'].value;
		const externalConfigurationFolder =
			form.controls['externalConfigurationFolder'].value;
		const printFolder = form.controls['printFolder'].value;
		const verifierFolder = form.controls['verifierFolder'].value;

		if (mode === 'SETUP') {
			if (!directTrustLocation || directTrustLocation.trim() === '') {
				return {directTrustLocationRequired: true};
			}
			if (!directTrustPwdLocation || directTrustPwdLocation.trim() === '') {
				return {directTrustPwdLocationRequired: true};
			}
			if (!outputFolder || outputFolder.trim() === '') {
				return {outputFolderRequired: true};
			}
			if (
				!externalConfigurationFolder ||
				externalConfigurationFolder.trim() === ''
			) {
				return {externalConfigurationFolderRequired: true};
			}
			if (!printFolder || printFolder.trim() === '') {
				return {printFolderRequired: true};
			}
			if (!verifierFolder || verifierFolder.trim() === '') {
				return {verifierFolderRequired: true};
			}
			if (
				!choiceCodeGenerationChunkSize ||
				choiceCodeGenerationChunkSize.trim() === ''
			) {
				return {choiceCodeGenerationChunkSizeRequired: true};
			}
		}

		if (mode === 'ONLINE') {
			if (!onlineHost || onlineHost.trim() === '') {
				return {onlineHostRequired: true};
			}
			if (!outputFolder || outputFolder.trim() === '') {
				return {outputFolderRequired: true};
			}
		}

		if (mode === 'TALLY') {
			if (!directTrustLocation || directTrustLocation.trim() === '') {
				return {directTrustLocationRequired: true};
			}
			if (!directTrustPwdLocation || directTrustPwdLocation.trim() === '') {
				return {directTrustPwdLocationRequired: true};
			}
			if (!verifierFolder || verifierFolder.trim() === '') {
				return {verifierFolderRequired: true};
			}
		}

		return null;
	}

	passwordHasError(policy: string): boolean {
		return (
			!!this.settingsForm.controls.password.errors?.[policy] ||
			this.settingsForm.controls.password.value === ''
		);
	}

	electionEventSeedHasError(policy: string): boolean {
		if (
			!this.settingsForm.controls.electionEventSeed.value ||
			this.settingsForm.controls.electionEventSeed.value === ''
		)
			return true;

		const errors = this.settingsForm.controls.electionEventSeed.errors;
		if (!errors?.['seed']) return false;

		return errors['seed'][policy];
	}

	ngOnInit(): void {
		this.settings = history.state['settings'];
		this.settingsForm.patchValue({
			electionEventSeed: this.settings?.electionEventSeed,
			mode: this.settings?.mode,
			onlineHost: this.settings?.onlineHost,
			workspaceFolder: this.settings?.workspaceFolder,
			externalConfigurationFolder: this.settings?.externalConfigurationFolder,
			outputFolder: this.settings?.outputFolder,
			printFolder: this.settings?.printFolder,
			verifierFolder: this.settings?.verifierFolder,
			password: this.settings?.exportPwd,
			choiceCodeGenerationChunkSize:
			this.settings?.choiceCodeGenerationChunkSize,
			directTrustLocation: this.settings?.directTrustLocation,
			directTrustPwdLocation: this.settings?.directTrustPwdLocation,
			twoWaySSLLocation: this.settings?.twoWaySSLLocation,
			twoWaySSLPwdLocation: this.settings?.twoWaySSLPwdLocation,
		});
	}

	previous() {
		this.router.navigate(['select-settings']);
	}

	startSDM() {
		this.saveSettings(true);
	}

	save() {
		this.saveSettings(false);
		this.delay(100).then(() => this.router.navigate(['select-settings']));
	}

	delete() {
		window.SettingsApi?.deleteSettings(this.settings?.id ?? -1);
		this.delay(100).then(() => this.router.navigate(['select-settings']));
	}

	inputTransformFn = (value: unknown): string => {
		return typeof value === 'string' ? value.toUpperCase() : String(value);
	};

	outputTransformFn = (value: string | number | null | undefined): string => {
		return value ? String(value).toUpperCase() : '';
	};

	private delay(ms: number) {
		return new Promise((resolve) => setTimeout(resolve, ms));
	}

	private saveSettings(startSDM: boolean) {
		this.startingSDM = true;
		this.workspaceValid = window.SettingsApi?.isValidPath(
			this.settingsForm.controls['workspaceFolder'].value,
		);
		if (!this.workspaceValid) {
			this.startingSDM = false;
			return;
		}
		let settings: Settings = {
			id: this.settings?.id ?? -1,
			electionEventSeed: this.settingsForm.controls['electionEventSeed'].value,
			workspaceFolder: this.settingsForm.controls['workspaceFolder'].value,
			mode: this.settingsForm.controls['mode'].value,
			onlineHost: this.settingsForm.controls['onlineHost'].value,
			twoWaySSLLocation: this.settingsForm.controls['twoWaySSLLocation'].value,
			twoWaySSLPwdLocation: this.settingsForm.controls['twoWaySSLPwdLocation'].value,
			outputFolder: this.settingsForm.controls['outputFolder'].value,
			externalConfigurationFolder: this.settingsForm.controls['externalConfigurationFolder'].value,
			printFolder: this.settingsForm.controls['printFolder'].value,
			verifierFolder: this.settingsForm.controls['verifierFolder'].value,
			exportPwd: this.settingsForm.controls['password'].value,
			verifierDatasetPwd: this.settingsForm.controls['password'].value,
			databasePwd: this.settingsForm.controls['password'].value,
			choiceCodeGenerationChunkSize: this.settingsForm.controls['choiceCodeGenerationChunkSize'].value,
			directTrustLocation: this.settingsForm.controls['directTrustLocation'].value,
			directTrustPwdLocation: this.settingsForm.controls['directTrustPwdLocation'].value,
		};
		window.SettingsApi?.saveSettings(settings, startSDM);
	}
}
