/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {Injectable} from '@angular/core';
import {BallotBox, BallotBoxTestVote, DecryptInput} from '@sdm/shared-util-types';
import {Observable} from 'rxjs';
import {HttpClient} from "@angular/common/http";
import {environment} from "@sdm/shared-ui-config";

@Injectable({
	providedIn: 'root'
})
export class DecryptService {
	private url = `${environment.backendPath}/sdm-tally/decrypt`;

	constructor(private httpClient: HttpClient) {
	}

	decrypt(ballotBoxes: BallotBox[], electoralBoardPasswords: string[]) {
		const ballotBoxIds = ballotBoxes.map((ballotBox) => ballotBox.id);
		const passwordsAsCharArray: string[][] = electoralBoardPasswords.map((pwd) => [...pwd]);
		const values: DecryptInput = {
			ballotBoxIds: ballotBoxIds,
			electoralBoardPasswords: passwordsAsCharArray
		}
		return this.httpClient.post(this.url, values).subscribe();
	}

	getBallotBoxes(): Observable<BallotBox[]> {
		return this.httpClient.get<BallotBox[]>(`${this.url}/ballot-boxes`);
	}

	getBallotBoxTestVotes(): Observable<BallotBoxTestVote[]> {
		return this.httpClient.get<BallotBoxTestVote[]>(`${this.url}/test-votes`);
	}
}
