/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
const nxPreset = require('@nx/jest/preset').default;

module.exports = { ...nxPreset };
