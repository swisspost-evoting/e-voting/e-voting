/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {getJestProjects} from '@nx/jest';

export default {
  projects: getJestProjects(),
};
