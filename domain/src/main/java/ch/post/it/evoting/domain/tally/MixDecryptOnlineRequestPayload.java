/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.tally;

import static ch.post.it.evoting.cryptoprimitives.utils.Validations.allEqual;
import static ch.post.it.evoting.evotinglibraries.domain.ControlComponentConstants.NODE_IDS;
import static ch.post.it.evoting.evotinglibraries.domain.validations.ControlComponentVotesHashPayloadValidation.validate;
import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;

import java.util.List;
import java.util.stream.IntStream;

import com.google.common.base.Preconditions;

import ch.post.it.evoting.evotinglibraries.domain.mixnet.ControlComponentShufflePayload;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.ControlComponentVotesHashPayload;

public record MixDecryptOnlineRequestPayload(String electionEventId,
											 String ballotBoxId,
											 int nodeId,
											 List<ControlComponentVotesHashPayload> controlComponentVotesHashPayloads,
											 List<ControlComponentShufflePayload> controlComponentShufflePayloads) {

	public MixDecryptOnlineRequestPayload(final String electionEventId,
			final String ballotBoxId,
			final int nodeId,
			final List<ControlComponentVotesHashPayload> controlComponentVotesHashPayloads,
			final List<ControlComponentShufflePayload> controlComponentShufflePayloads) {

		this.electionEventId = validateUUID(electionEventId);
		this.ballotBoxId = validateUUID(ballotBoxId);
		this.nodeId = nodeId;
		this.controlComponentVotesHashPayloads = validate(electionEventId, ballotBoxId, controlComponentVotesHashPayloads);
		this.controlComponentShufflePayloads = controlComponentShufflePayloads.stream().map(Preconditions::checkNotNull).toList();

		checkArgument(NODE_IDS.contains(this.nodeId), "The node id must be part of the known node ids. [nodeId: %s]", nodeId);

		checkArgument(this.controlComponentShufflePayloads.size() == this.nodeId - 1,
				"The must be exactly (nodeId-1) control component shuffle payloads.");
		checkArgument(
				this.controlComponentShufflePayloads.isEmpty() || this.controlComponentShufflePayloads.get(0).getElectionEventId()
						.equals(this.electionEventId),
				"The election event id of the control component shuffle payloads and the mix decrypt online request payload must be the same.");
		checkArgument(
				this.controlComponentShufflePayloads.isEmpty() || this.controlComponentShufflePayloads.get(0).getBallotBoxId()
						.equals(this.ballotBoxId),
				"The ballot box id of the control component shuffle payloads and the mix decrypt online request payload must be the same.");
		checkArgument(allEqual(this.controlComponentShufflePayloads.stream(), ControlComponentShufflePayload::getElectionEventId),
				"All control component shuffle payloads must have the same election event id.");
		checkArgument(allEqual(this.controlComponentShufflePayloads.stream(), ControlComponentShufflePayload::getBallotBoxId),
				"All control component shuffle payloads must have the same ballot box id.");
		checkArgument(allEqual(this.controlComponentShufflePayloads.stream(), ControlComponentShufflePayload::getEncryptionGroup),
				"All control component shuffle payloads must have the same encryption group.");

		checkArgument(IntStream.range(1, this.nodeId)
						.allMatch(orderedNodeId -> this.controlComponentShufflePayloads.get(orderedNodeId - 1).getNodeId() == orderedNodeId),
				"The control component payloads must be sorted ascending by node id.");

	}

	@Override
	public List<ControlComponentVotesHashPayload> controlComponentVotesHashPayloads() {
		return List.copyOf(controlComponentVotesHashPayloads);
	}

	@Override
	public List<ControlComponentShufflePayload> controlComponentShufflePayloads() {
		return List.copyOf(controlComponentShufflePayloads);
	}
}
