/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.converters;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.io.UncheckedIOException;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.evotinglibraries.domain.election.PrimesMappingTable;

@Converter
public class PrimesMappingTableConverter implements AttributeConverter<PrimesMappingTable, byte[]> {

	private final ObjectMapper objectMapper;

	public PrimesMappingTableConverter(final ObjectMapper objectMapper) {
		this.objectMapper = objectMapper;
	}

	@Override
	public byte[] convertToDatabaseColumn(final PrimesMappingTable primesMappingTable) {
		checkNotNull(primesMappingTable);
		try {
			return objectMapper.writeValueAsBytes(primesMappingTable);
		} catch (final JsonProcessingException e) {
			throw new UncheckedIOException("Failed to serialize primes mapping table to bytes.", e);
		}
	}

	@Override
	public PrimesMappingTable convertToEntityAttribute(final byte[] bytes) {
		checkNotNull(bytes);
		try {
			return objectMapper.readValue(bytes, PrimesMappingTable.class);
		} catch (final IOException e) {
			throw new UncheckedIOException("Failed to deserialize bytes into primes mapping table.", e);
		}
	}

}
