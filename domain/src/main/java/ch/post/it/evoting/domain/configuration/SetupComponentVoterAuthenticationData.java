/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.configuration;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.BASE64_ENCODED_HASH_OUTPUT_LENGTH;
import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateBase64Encoded;
import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;

import java.util.List;

import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableList;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableString;

public record SetupComponentVoterAuthenticationData(String electionEventId, String verificationCardSetId, String votingCardSetId, String ballotBoxId,
													String ballotId, String verificationCardId, String votingCardId, String credentialId,
													String baseAuthenticationChallenge)
		implements HashableList {

	public SetupComponentVoterAuthenticationData {
		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);
		validateUUID(votingCardSetId);
		validateUUID(ballotBoxId);
		validateUUID(ballotId);
		validateUUID(verificationCardId);
		validateUUID(votingCardId);
		validateUUID(credentialId);
		checkArgument(validateBase64Encoded(baseAuthenticationChallenge).length() == BASE64_ENCODED_HASH_OUTPUT_LENGTH,
				"The base authentication challenge must be a valid Base64 string of size l_HB64.");
	}

	@Override
	public List<? extends Hashable> toHashableForm() {
		return List.of(HashableString.from(electionEventId),
				HashableString.from(verificationCardSetId),
				HashableString.from(votingCardSetId),
				HashableString.from(ballotBoxId),
				HashableString.from(ballotId),
				HashableString.from(verificationCardId),
				HashableString.from(votingCardId),
				HashableString.from(credentialId),
				HashableString.from(baseAuthenticationChallenge));
	}

}
