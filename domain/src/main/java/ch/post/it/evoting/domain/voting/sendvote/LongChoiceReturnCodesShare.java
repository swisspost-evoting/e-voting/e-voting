/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.voting.sendvote;

import static ch.post.it.evoting.evotinglibraries.domain.ControlComponentConstants.NODE_IDS;
import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigInteger;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableBigInteger;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableList;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableString;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.GqGroupVectorDeserializer;

public record LongChoiceReturnCodesShare(String electionEventId,
										 String verificationCardSetId,
										 String verificationCardId,
										 int nodeId,
										 @JsonDeserialize(using = GqGroupVectorDeserializer.class)
										 GroupVector<GqElement, GqGroup> longChoiceReturnCodeShare) implements HashableList {

	public LongChoiceReturnCodesShare {
		checkArgument(NODE_IDS.contains(nodeId), "The node id must be part of the known node ids. [nodeId: %s]", nodeId);
		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);
		validateUUID(verificationCardId);
		checkNotNull(longChoiceReturnCodeShare);
	}

	@Override
	public List<Hashable> toHashableForm() {
		return List.of(
				HashableString.from(electionEventId),
				HashableString.from(verificationCardSetId),
				HashableString.from(verificationCardId),
				HashableBigInteger.from(BigInteger.valueOf(nodeId)),
				longChoiceReturnCodeShare);
	}

}
