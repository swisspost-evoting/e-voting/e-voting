/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.voting.confirmvote;

import static ch.post.it.evoting.evotinglibraries.domain.ControlComponentConstants.NODE_IDS;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Comparator;
import java.util.List;

import com.google.common.base.Preconditions;

public record ControlComponenthlVCCRequestPayload(List<ControlComponenthlVCCPayload> controlComponenthlVCCPayloads) {
	public ControlComponenthlVCCRequestPayload(final List<ControlComponenthlVCCPayload> controlComponenthlVCCPayloads) {
		checkNotNull(controlComponenthlVCCPayloads);
		this.controlComponenthlVCCPayloads = controlComponenthlVCCPayloads.stream().map(Preconditions::checkNotNull).toList();

		checkArgument(NODE_IDS.size() == this.controlComponenthlVCCPayloads.size(),
				"The control component hlVCCPayload list size should match the control component list size. [size: %s]", this.controlComponenthlVCCPayloads.size());
		checkArgument(NODE_IDS.containsAll(this.controlComponenthlVCCPayloads.stream().map(ControlComponenthlVCCPayload::getNodeId).toList()), "The control component hlVCCPayload list size should contains a payload by nodeId");

	}

	public List<ControlComponenthlVCCPayload> controlComponenthlVCCPayloads() {
		return controlComponenthlVCCPayloads.stream()
				.sorted(Comparator.comparingInt(ControlComponenthlVCCPayload::getNodeId))
				.toList();
	}
}
