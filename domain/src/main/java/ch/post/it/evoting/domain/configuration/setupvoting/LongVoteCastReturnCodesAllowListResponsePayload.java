/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.configuration.setupvoting;

import static ch.post.it.evoting.evotinglibraries.domain.ControlComponentConstants.NODE_IDS;
import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;

public record LongVoteCastReturnCodesAllowListResponsePayload(int nodeId,
															  String electionEventId,
															  String verificationCardSetId) {

	public LongVoteCastReturnCodesAllowListResponsePayload {
		checkArgument(NODE_IDS.contains(nodeId), "The node id must be part of the known node ids. [nodeId: %s]", nodeId);
		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);
	}
}
