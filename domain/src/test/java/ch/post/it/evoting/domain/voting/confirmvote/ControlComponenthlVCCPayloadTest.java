/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.voting.confirmvote;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base64Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.GqGroupGenerator;
import ch.post.it.evoting.evotinglibraries.domain.common.ContextIds;
import ch.post.it.evoting.evotinglibraries.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.evotinglibraries.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;

class ControlComponenthlVCCPayloadTest {

	private static final Random random = RandomFactory.createRandom();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();
	private static final Alphabet base64Alphabet = Base64Alphabet.getInstance();

	private final String electionEventId = random.genRandomString(ID_LENGTH, base16Alphabet);
	private final String verificationCardSetId = random.genRandomString(ID_LENGTH, base16Alphabet);
	private final String verificationCardId = random.genRandomString(ID_LENGTH, base16Alphabet);
	private final String hashLongVoteCastCodeShare = random.genRandomString(ID_LENGTH, base64Alphabet);

	@Test
	void serialisationDeserialisationCycleWorks() throws IOException {
		final ContextIds contextIds = new ContextIds(electionEventId, verificationCardSetId, verificationCardId);
		final int nodeId = 1;
		final int unsuccessfulConfirmationAttemptCount = 0;
		final GqGroup encryptionGroup = GroupTestData.getGqGroup();
		final GqElement gqElement = new GqGroupGenerator(encryptionGroup).genMember();
		final ConfirmationKey confirmationKey = new ConfirmationKey(contextIds, gqElement);
		final CryptoPrimitivesSignature signature = new CryptoPrimitivesSignature("randomSignatureContents".getBytes(StandardCharsets.UTF_8));
		final ControlComponenthlVCCPayload responsePayload = new ControlComponenthlVCCPayload(encryptionGroup,
				nodeId, hashLongVoteCastCodeShare, confirmationKey, unsuccessfulConfirmationAttemptCount, signature);
		final ObjectMapper objectMapper = DomainObjectMapper.getNewInstance();
		final ControlComponenthlVCCPayload responsePayloadCycled = objectMapper.readValue(
				objectMapper.writeValueAsBytes(responsePayload), ControlComponenthlVCCPayload.class);
		assertEquals(responsePayload, responsePayloadCycled);

	}

	@Nested
	@DisplayName("Check constructor validation")
	class CheckConstructor {

		private final ContextIds contextIds = new ContextIds(electionEventId, verificationCardSetId, verificationCardId);
		private final CryptoPrimitivesSignature signature = new CryptoPrimitivesSignature("signature".getBytes(StandardCharsets.UTF_8));
		private final Integer nodeId = 1;
		private final int unsuccessfulConfirmationAttemptCount = 0;

		private final GqGroup encryptionGroup = GroupTestData.getGqGroup();

		private final ConfirmationKey confirmationKey = new ConfirmationKey(contextIds, new GqGroupGenerator(encryptionGroup).genMember());

		@Test
		@DisplayName("Check null arguments")
		void nullArgs() {

			assertAll(
					() -> assertThrows(NullPointerException.class,
							() -> new ControlComponenthlVCCPayload(null, nodeId, hashLongVoteCastCodeShare, confirmationKey,
									unsuccessfulConfirmationAttemptCount,
									signature)),
					() -> assertThrows(NullPointerException.class,
							() -> new ControlComponenthlVCCPayload(encryptionGroup, nodeId, null, confirmationKey,
									unsuccessfulConfirmationAttemptCount, signature)),
					() -> assertThrows(NullPointerException.class,
							() -> new ControlComponenthlVCCPayload(encryptionGroup, nodeId, hashLongVoteCastCodeShare, null,
									unsuccessfulConfirmationAttemptCount,
									signature)),
					() -> assertThrows(NullPointerException.class,
							() -> new ControlComponenthlVCCPayload(encryptionGroup, nodeId, hashLongVoteCastCodeShare, confirmationKey,
									unsuccessfulConfirmationAttemptCount, null))
			);
		}

		@Test
		@DisplayName("Check hashLongVoteCastCodeShare in format Base64")
		void formatBase64() {
			final String validBase64Value = "Ax/6A+2w";
			final String invalidBase64Value = "$$";

			assertAll(
					() -> assertDoesNotThrow(
							() -> new ControlComponenthlVCCPayload(encryptionGroup, nodeId, validBase64Value, confirmationKey,
									unsuccessfulConfirmationAttemptCount, signature)),
					() -> assertThrows(FailedValidationException.class,
							() -> new ControlComponenthlVCCPayload(encryptionGroup, nodeId, invalidBase64Value, confirmationKey,
									unsuccessfulConfirmationAttemptCount, signature))
			);
		}

		@Test
		@DisplayName("Check nodeIds")
		void nodeIdArgs() {

			assertAll(
					() -> assertThrows(IllegalArgumentException.class,
							() -> new ControlComponenthlVCCPayload(encryptionGroup, 0, hashLongVoteCastCodeShare, confirmationKey,
									unsuccessfulConfirmationAttemptCount, signature)),
					() -> assertDoesNotThrow(
							() -> new ControlComponenthlVCCPayload(encryptionGroup, 1, hashLongVoteCastCodeShare, confirmationKey,
									unsuccessfulConfirmationAttemptCount, signature)),
					() -> assertDoesNotThrow(
							() -> new ControlComponenthlVCCPayload(encryptionGroup, 2, hashLongVoteCastCodeShare, confirmationKey,
									unsuccessfulConfirmationAttemptCount, signature)),
					() -> assertDoesNotThrow(
							() -> new ControlComponenthlVCCPayload(encryptionGroup, 3, hashLongVoteCastCodeShare, confirmationKey,
									unsuccessfulConfirmationAttemptCount, signature)),
					() -> assertDoesNotThrow(
							() -> new ControlComponenthlVCCPayload(encryptionGroup, 4, hashLongVoteCastCodeShare, confirmationKey,
									unsuccessfulConfirmationAttemptCount, signature)),
					() -> assertThrows(IllegalArgumentException.class,
							() -> new ControlComponenthlVCCPayload(encryptionGroup, 5, hashLongVoteCastCodeShare, confirmationKey,
									unsuccessfulConfirmationAttemptCount, signature))
			);
		}
	}

}
