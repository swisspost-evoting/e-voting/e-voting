/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.configuration;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.hashing.HashFactory;
import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.evotinglibraries.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.evotinglibraries.domain.signature.CryptoPrimitivesSignature;

@DisplayName("A SetupComponentVerificationCardKeystoresPayload")
class SetupComponentVerificationCardKeystoresPayloadTest {

	private static final ObjectMapper mapper = DomainObjectMapper.getNewInstance();
	private static final Random random = RandomFactory.createRandom();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();
	private static final Hash hash = HashFactory.createHash();
	private static SetupComponentVerificationCardKeystoresPayload setupComponentVerificationCardKeystoresPayload;
	private static ObjectNode rootNode;

	@BeforeAll
	static void setupAll() throws JsonProcessingException {

		// Create payload.
		final String electionEventId = random.genRandomString(ID_LENGTH, base16Alphabet);
		final String verificationCardSetId = random.genRandomString(ID_LENGTH, base16Alphabet);

		final List<VerificationCardKeystore> verificationCardKeystores = List.of(
				new VerificationCardKeystore(random.genRandomString(ID_LENGTH, base16Alphabet),
						"dmVyaWZpY2F0aW9uQ2FyZEtleXN0b3JlMQ=="),
				new VerificationCardKeystore(random.genRandomString(ID_LENGTH, base16Alphabet),
						"dmVyaWZpY2F0aW9uQ2FyZEtleXN0b3JlMg=="),
				new VerificationCardKeystore(random.genRandomString(ID_LENGTH, base16Alphabet),
						"dmVyaWZpY2F0aW9uQ2FyZEtleXN0b3JlMw==")
		);

		setupComponentVerificationCardKeystoresPayload = new SetupComponentVerificationCardKeystoresPayload(electionEventId, verificationCardSetId,
				verificationCardKeystores);

		final byte[] payloadHash = hash.recursiveHash(setupComponentVerificationCardKeystoresPayload);
		final CryptoPrimitivesSignature signature = new CryptoPrimitivesSignature(payloadHash);
		setupComponentVerificationCardKeystoresPayload.setSignature(signature);

		// Create expected Json.
		rootNode = mapper.createObjectNode();
		rootNode.set("electionEventId", mapper.readTree(mapper.writeValueAsString(electionEventId)));
		rootNode.set("verificationCardSetId", mapper.readTree(mapper.writeValueAsString(verificationCardSetId)));
		rootNode.set("verificationCardKeystores", mapper.readTree(mapper.writeValueAsString(verificationCardKeystores)));
		rootNode.set("signature", mapper.readTree(mapper.writeValueAsString(signature)));
	}

	@Test
	@DisplayName("serialized gives expected json")
	void serializePayload() throws JsonProcessingException {
		final String serializedPayload = mapper.writeValueAsString(setupComponentVerificationCardKeystoresPayload);
		assertEquals(rootNode.toString(), serializedPayload);
	}

	@Test
	@DisplayName("deserialized gives expected payload")
	void deserializePayload() throws IOException {
		final SetupComponentVerificationCardKeystoresPayload deserializedPayload = mapper.readValue(rootNode.toString(),
				SetupComponentVerificationCardKeystoresPayload.class);
		assertEquals(setupComponentVerificationCardKeystoresPayload, deserializedPayload);
	}

	@Test
	@DisplayName("serialized then deserialized gives original payload")
	void cycle() throws IOException {
		final SetupComponentVerificationCardKeystoresPayload deserializedPayload = mapper
				.readValue(mapper.writeValueAsString(setupComponentVerificationCardKeystoresPayload),
						SetupComponentVerificationCardKeystoresPayload.class);

		assertEquals(setupComponentVerificationCardKeystoresPayload, deserializedPayload);
	}
}
