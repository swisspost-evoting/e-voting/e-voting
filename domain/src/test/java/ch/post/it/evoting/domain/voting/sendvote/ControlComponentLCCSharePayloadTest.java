/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.voting.sendvote;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.security.SecureRandom;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.domain.MapperSetUp;
import ch.post.it.evoting.evotinglibraries.domain.SerializationUtils;
import ch.post.it.evoting.evotinglibraries.domain.signature.CryptoPrimitivesSignature;

@DisplayName("A ControlComponentLCCSharePayload")
class ControlComponentLCCSharePayloadTest extends MapperSetUp {

	private static final Random random = RandomFactory.createRandom();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();

	private static final String electionEventId = random.genRandomString(ID_LENGTH, base16Alphabet);
	private static final String verificationCardSetId = random.genRandomString(ID_LENGTH, base16Alphabet);
	private static final String verificationCardId = random.genRandomString(ID_LENGTH, base16Alphabet);
	private static final int NODE_ID = 1;
	private static final SecureRandom secureRandom = new SecureRandom();
	private static final byte[] randomBytes = new byte[10];

	private static ObjectNode rootNode;
	private static ControlComponentLCCSharePayload controlComponentLCCSharePayload;

	@BeforeAll
	static void setUpAll() {
		final GqGroup gqGroup = SerializationUtils.getGqGroup();
		final GroupVector<GqElement, GqGroup> longChoiceCodes = SerializationUtils.getLongChoiceCodes(2);

		// Generate random bytes for signature content and create payload signature.
		secureRandom.nextBytes(randomBytes);
		final CryptoPrimitivesSignature signature = new CryptoPrimitivesSignature(randomBytes);

		// Create payload.
		final LongChoiceReturnCodesShare payload = new LongChoiceReturnCodesShare(electionEventId, verificationCardSetId, verificationCardId, NODE_ID,
				longChoiceCodes);

		controlComponentLCCSharePayload = new ControlComponentLCCSharePayload(gqGroup, payload, signature);

		// Create expected Json.
		rootNode = mapper.createObjectNode();

		final JsonNode encryptionGroupNode = SerializationUtils.createEncryptionGroupNode(gqGroup);
		rootNode.set("encryptionGroup", encryptionGroupNode);

		final ObjectNode payloadNode = mapper.createObjectNode();
		payloadNode.put("electionEventId", electionEventId);
		payloadNode.put("verificationCardSetId", verificationCardSetId);
		payloadNode.put("verificationCardId", verificationCardId);
		payloadNode.put("nodeId", NODE_ID);

		final ArrayNode electionPublicKeyNode = SerializationUtils.createGqGroupVectorNode(longChoiceCodes);
		payloadNode.set("longChoiceReturnCodeShare", electionPublicKeyNode);

		rootNode.set("longChoiceReturnCodesShare", payloadNode);

		final JsonNode signatureNode = SerializationUtils.createSignatureNode(signature);
		rootNode.set("signature", signatureNode);
	}

	@Test
	@DisplayName("longChoiceReturnCodesShare has same GqGroup order as the encryptionGroup")
	void checkGroups() {
		assertEquals(controlComponentLCCSharePayload.getLongChoiceReturnCodesShare().longChoiceReturnCodeShare().getGroup(),
				controlComponentLCCSharePayload.getEncryptionGroup());
	}

	@Test
	@DisplayName("serialized gives expected json")
	void serializePayload() throws JsonProcessingException {
		final String serializedPayload = mapper.writeValueAsString(controlComponentLCCSharePayload);

		assertEquals(rootNode.toString(), serializedPayload);
	}

	@Test
	@DisplayName("deserialized gives expected payload")
	void deserializePayload() throws JsonProcessingException {
		final ControlComponentLCCSharePayload deserializedPayload = mapper.readValue(rootNode.toString(), ControlComponentLCCSharePayload.class);

		assertEquals(controlComponentLCCSharePayload, deserializedPayload);
	}

	@Test
	@DisplayName("serialized then deserialized gives original payload")
	void cycle() throws JsonProcessingException {
		final ControlComponentLCCSharePayload deserializedPayload = mapper
				.readValue(mapper.writeValueAsString(controlComponentLCCSharePayload), ControlComponentLCCSharePayload.class);

		assertEquals(controlComponentLCCSharePayload, deserializedPayload);
	}

}
