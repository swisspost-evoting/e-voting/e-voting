/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.configuration;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import ch.post.it.evoting.cryptoprimitives.hashing.HashFactory;
import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.evotinglibraries.domain.SerializationUtils;
import ch.post.it.evoting.evotinglibraries.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.evotinglibraries.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;

@DisplayName("An electoralBoardHashesPayload")
class ElectoralBoardHashesPayloadTest {

	private static final ObjectMapper mapper = DomainObjectMapper.getNewInstance();
	private static final ch.post.it.evoting.cryptoprimitives.hashing.Hash hash = HashFactory.createHash();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();
	private static final String electionEventId = RandomFactory.createRandom().genRandomString(ID_LENGTH, base16Alphabet);
	private static ElectoralBoardHashesPayload electoralBoardHashesPayload;
	private static List<byte[]> hashes;
	private static ObjectNode rootNode;

	@BeforeAll
	static void setupAll() throws JsonProcessingException {

		// Create payload.
		hashes = List.of(new byte[] { 1, 2 }, new byte[] { 3, 4 });
		electoralBoardHashesPayload = new ElectoralBoardHashesPayload(electionEventId, hashes);

		final byte[] payloadHash = hash.recursiveHash(electoralBoardHashesPayload);
		final CryptoPrimitivesSignature signature = new CryptoPrimitivesSignature(payloadHash);
		electoralBoardHashesPayload.setSignature(signature);

		// Create expected Json.
		rootNode = mapper.createObjectNode();
		rootNode.set("electionEventId", mapper.readTree(mapper.writeValueAsString(electionEventId)));
		rootNode.set("electoralBoardHashes", mapper.readTree(mapper.writeValueAsString(hashes)));
		rootNode.set("signature", SerializationUtils.createSignatureNode(signature));
	}

	@Test
	@DisplayName("serialized gives expected json")
	void serializePayload() throws JsonProcessingException {
		final String serializedPayload = mapper.writeValueAsString(electoralBoardHashesPayload);
		assertEquals(rootNode.toString(), serializedPayload);
	}

	@Test
	@DisplayName("deserialized gives expected payload")
	void deserializePayload() throws IOException {
		final ElectoralBoardHashesPayload deserializedPayload = mapper.readValue(rootNode.toString(), ElectoralBoardHashesPayload.class);
		assertEquals(electoralBoardHashesPayload, deserializedPayload);
		assertEquals(electionEventId, electoralBoardHashesPayload.getElectionEventId());
	}

	@Test
	@DisplayName("serialized then deserialized gives original payload")
	void cycle() throws IOException {
		final ElectoralBoardHashesPayload deserializedPayload =
				mapper.readValue(mapper.writeValueAsString(electoralBoardHashesPayload), ElectoralBoardHashesPayload.class);

		assertEquals(electoralBoardHashesPayload, deserializedPayload);
	}

	@Test
	@DisplayName("constructed with invalid fields throws an exception")
	void testInvalidElectionEventContext() {
		assertAll(
				() -> assertThrows(NullPointerException.class,
						() -> new ElectoralBoardHashesPayload(null, hashes)),
				() -> assertThrows(FailedValidationException.class,
						() -> new ElectoralBoardHashesPayload("invalidElectionEventId", hashes)),
				() -> assertThrows(NullPointerException.class,
						() -> new ElectoralBoardHashesPayload(electionEventId, null))
		);
	}
}
