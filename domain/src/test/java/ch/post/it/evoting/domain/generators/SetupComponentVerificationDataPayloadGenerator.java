/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.generators;

import static ch.post.it.evoting.cryptoprimitives.hashing.HashFactory.createHash;
import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.BASE64_ENCODED_HASH_OUTPUT_LENGTH;
import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;

import java.util.List;
import java.util.stream.Stream;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base64Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ElGamalGenerator;
import ch.post.it.evoting.evotinglibraries.domain.returncodes.SetupComponentVerificationData;
import ch.post.it.evoting.evotinglibraries.domain.returncodes.SetupComponentVerificationDataPayload;
import ch.post.it.evoting.evotinglibraries.domain.signature.CryptoPrimitivesSignature;

public class SetupComponentVerificationDataPayloadGenerator {

	private static final Hash hash = createHash();
	private static final Random random = RandomFactory.createRandom();
	private static final Base16Alphabet base16Alphabet = Base16Alphabet.getInstance();
	private static final Base64Alphabet base64Alphabet = Base64Alphabet.getInstance();

	private final GqGroup encryptionGroup;
	private final ElGamalGenerator elGamalGenerator;

	public SetupComponentVerificationDataPayloadGenerator(final GqGroup encryptionGroup) {
		this.encryptionGroup = encryptionGroup;
		this.elGamalGenerator = new ElGamalGenerator(encryptionGroup);
	}

	public SetupComponentVerificationDataPayloadGenerator() {
		this(GroupTestData.getLargeGqGroup());
	}

	public SetupComponentVerificationDataPayload generate() {
		final String electionEventId = random.genRandomString(ID_LENGTH, base16Alphabet);
		final String verificationCardSetId = random.genRandomString(ID_LENGTH, base16Alphabet);
		final int totalNumberOfVotingOptions = 3;

		return generate(electionEventId, verificationCardSetId, totalNumberOfVotingOptions);
	}

	public SetupComponentVerificationDataPayload generate(final String electionEventId, final String verificationCardSetId,
			final int totalNumberOfVotingOptions) {
		final int numberOfVotingCards = 2;

		final List<SetupComponentVerificationData> setupComponentVerificationData = Stream.generate(
						() -> generateSetupComponentVerificationData(totalNumberOfVotingOptions))
				.limit(numberOfVotingCards)
				.toList();

		final List<String> partialChoiceReturnCodesAllowList = Stream.generate(
						() -> random.genRandomString(BASE64_ENCODED_HASH_OUTPUT_LENGTH, base64Alphabet))
				.limit((long) totalNumberOfVotingOptions * numberOfVotingCards)
				.toList();

		final SetupComponentVerificationDataPayload setupComponentVerificationDataPayload = new SetupComponentVerificationDataPayload(electionEventId,
				verificationCardSetId, partialChoiceReturnCodesAllowList, 1, encryptionGroup, setupComponentVerificationData);

		final byte[] payloadHash = hash.recursiveHash(setupComponentVerificationDataPayload);
		final CryptoPrimitivesSignature signature = new CryptoPrimitivesSignature(payloadHash);
		setupComponentVerificationDataPayload.setSignature(signature);

		return setupComponentVerificationDataPayload;
	}

	private SetupComponentVerificationData generateSetupComponentVerificationData(final int totalNumberOfVotingOptions) {
		final String verificationCardId = random.genRandomString(ID_LENGTH, base16Alphabet);

		final ElGamalMultiRecipientCiphertext encryptedHashedSquaredPartialChoiceReturnCodes = elGamalGenerator.genRandomCiphertext(
				totalNumberOfVotingOptions);

		final ElGamalMultiRecipientCiphertext encryptedHashedSquaredConfirmationKey = elGamalGenerator.genRandomCiphertext(1);

		final ElGamalMultiRecipientPublicKey verificationCardPublicKey = elGamalGenerator.genRandomPublicKey(1);

		return new SetupComponentVerificationData(verificationCardId, verificationCardPublicKey, encryptedHashedSquaredPartialChoiceReturnCodes,
				encryptedHashedSquaredConfirmationKey);
	}
}
