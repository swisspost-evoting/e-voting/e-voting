/*
 * (c) Copyright 2023 Swiss Post Ltd
 */
package ch.post.it.evoting.domain.configuration.setupvoting;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.BCK_LENGTH;
import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.google.common.base.Throwables;

import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base64Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.domain.ElectionSetupUtils;

class VoterInitialCodesPayloadTest {

	private static final Random RANDOM = RandomFactory.createRandom();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();

	private String electionEventId;
	private String verificationCardSetId;
	private List<VoterInitialCodes> voterInitialCodesList;

	@BeforeEach
	void setup() {
		electionEventId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		verificationCardSetId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		voterInitialCodesList = Stream.generate(this::generateVoterInitialCodes).limit(5).toList();
	}

	@Test
	void constructWithNullArgumentsThrows() {
		assertThrows(NullPointerException.class, () -> new VoterInitialCodesPayload(null, verificationCardSetId, voterInitialCodesList));
		assertThrows(NullPointerException.class, () -> new VoterInitialCodesPayload(electionEventId, null, voterInitialCodesList));
		assertThrows(NullPointerException.class, () -> new VoterInitialCodesPayload(electionEventId, verificationCardSetId, null));
	}

	@Test
	void constructWithEmptyVoterInitialCodesListThrows() {
		final List<VoterInitialCodes> emptyList = List.of();
		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new VoterInitialCodesPayload(electionEventId, verificationCardSetId, emptyList));
		assertEquals("The voter initial codes list must not be empty.", Throwables.getRootCause(exception).getMessage());
	}

	@Test
	void constructWithVoterInitialCodesListContainingNullThrows() {
		final List<VoterInitialCodes> voterInitialCodesWithNull = new ArrayList<>(voterInitialCodesList);
		voterInitialCodesWithNull.add(null);
		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new VoterInitialCodesPayload(electionEventId, verificationCardSetId, voterInitialCodesWithNull));
		assertEquals("The voter initial codes list must not contain null elements.", Throwables.getRootCause(exception).getMessage());
	}

	@Test
	void constructWithDuplicateVoterIdentificationThrows() {
		final List<VoterInitialCodes> voterInitialCodesWithDuplicateVoterIdentification = new ArrayList<>(voterInitialCodesList);
		final VoterInitialCodes newVoterInitialCodes = generateVoterInitialCodes();
		final VoterInitialCodes voterInitialCodesDuplicateVoterIdentification = new VoterInitialCodes(
				voterInitialCodesList.get(0).voterIdentification(),
				newVoterInitialCodes.votingCardId(), newVoterInitialCodes.verificationCardId(), newVoterInitialCodes.startVotingKey(),
				newVoterInitialCodes.extendedAuthenticationFactor(), newVoterInitialCodes.ballotCastingKey());
		voterInitialCodesWithDuplicateVoterIdentification.add(voterInitialCodesDuplicateVoterIdentification);
		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new VoterInitialCodesPayload(electionEventId, verificationCardSetId, voterInitialCodesWithDuplicateVoterIdentification));
		assertEquals("The list of voter initial codes must not contain any duplicate voter identifications.",
				Throwables.getRootCause(exception).getMessage());
	}

	@Test
	void constructWithDuplicateVotingCardIdThrows() {
		final List<VoterInitialCodes> voterInitialCodesWithDuplicateVotingCardId = new ArrayList<>(voterInitialCodesList);
		final VoterInitialCodes newVoterInitialCodes = generateVoterInitialCodes();
		final VoterInitialCodes voterInitialCodesDuplicateVotingCardId = new VoterInitialCodes(newVoterInitialCodes.voterIdentification(),
				voterInitialCodesList.get(0).votingCardId(), newVoterInitialCodes.verificationCardId(), newVoterInitialCodes.startVotingKey(),
				newVoterInitialCodes.extendedAuthenticationFactor(), newVoterInitialCodes.ballotCastingKey());
		voterInitialCodesWithDuplicateVotingCardId.add(voterInitialCodesDuplicateVotingCardId);
		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new VoterInitialCodesPayload(electionEventId, verificationCardSetId, voterInitialCodesWithDuplicateVotingCardId));
		assertEquals("The list of voter initial codes must not contain any duplicate voting card ids.",
				Throwables.getRootCause(exception).getMessage());
	}

	@Test
	void constructWithDuplicateVerificationCardIdThrows() {
		final List<VoterInitialCodes> voterInitialCodesWithDuplicateVerificationCardId = new ArrayList<>(voterInitialCodesList);
		final VoterInitialCodes newVoterInitialCodes = generateVoterInitialCodes();
		final VoterInitialCodes voterInitialCodesDuplicateVerificationCardId = new VoterInitialCodes(newVoterInitialCodes.voterIdentification(),
				newVoterInitialCodes.votingCardId(), voterInitialCodesList.get(0).verificationCardId(), newVoterInitialCodes.startVotingKey(),
				newVoterInitialCodes.extendedAuthenticationFactor(), newVoterInitialCodes.ballotCastingKey());
		voterInitialCodesWithDuplicateVerificationCardId.add(voterInitialCodesDuplicateVerificationCardId);
		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new VoterInitialCodesPayload(electionEventId, verificationCardSetId, voterInitialCodesWithDuplicateVerificationCardId));
		assertEquals("The list of voter initial codes must not contain any duplicate verification card ids.",
				Throwables.getRootCause(exception).getMessage());
	}

	private VoterInitialCodes generateVoterInitialCodes() {
		final Alphabet base64Alphabet = Base64Alphabet.getInstance();
		final String voterIdentification = RANDOM.genRandomString(50, base64Alphabet);
		final String votingCardId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		final String verificationCardId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		final String startVotingKey = ElectionSetupUtils.genStartVotingKey();
		final String extendedAuthenticationFactor = String.join("", RANDOM.genUniqueDecimalStrings(4, 2));
		final String ballotCastingKey = RANDOM.genUniqueDecimalStrings(BCK_LENGTH, 1).get(0);
		return new VoterInitialCodes(voterIdentification, votingCardId, verificationCardId, startVotingKey, extendedAuthenticationFactor,
				ballotCastingKey);
	}
}
