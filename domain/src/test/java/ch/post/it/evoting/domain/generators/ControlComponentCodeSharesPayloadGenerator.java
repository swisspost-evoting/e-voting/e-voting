/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.generators;

import static ch.post.it.evoting.cryptoprimitives.hashing.HashFactory.createHash;
import static ch.post.it.evoting.evotinglibraries.domain.ControlComponentConstants.NODE_IDS;
import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;

import java.util.List;
import java.util.stream.IntStream;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ElGamalGenerator;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ZqGroupGenerator;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ExponentiationProof;
import ch.post.it.evoting.evotinglibraries.domain.returncodes.ControlComponentCodeShare;
import ch.post.it.evoting.evotinglibraries.domain.returncodes.ControlComponentCodeSharesPayload;
import ch.post.it.evoting.evotinglibraries.domain.signature.CryptoPrimitivesSignature;

public class ControlComponentCodeSharesPayloadGenerator {

	private static final Hash hash = createHash();
	private static final Random random = RandomFactory.createRandom();
	private static final Base16Alphabet base16Alphabet = Base16Alphabet.getInstance();

	final GqGroup encryptionGroup;
	final ElGamalGenerator elGamalGenerator;
	final ZqGroupGenerator zqGroupGenerator;

	public ControlComponentCodeSharesPayloadGenerator(final GqGroup encryptionGroup) {
		this.encryptionGroup = encryptionGroup;
		this.elGamalGenerator = new ElGamalGenerator(encryptionGroup);
		final ZqGroup zqGroup = ZqGroup.sameOrderAs(encryptionGroup);
		this.zqGroupGenerator = new ZqGroupGenerator(zqGroup);
	}

	public ControlComponentCodeSharesPayloadGenerator() {
		this(GroupTestData.getLargeGqGroup());
	}

	public List<ControlComponentCodeSharesPayload> generate() {
		final String electionEventId = random.genRandomString(ID_LENGTH, base16Alphabet);
		final String verificationCardSetId = random.genRandomString(ID_LENGTH, base16Alphabet);
		final int chunkId = 2;
		final int numberOfControlComponentCodeShares = 10;

		return generate(electionEventId, verificationCardSetId, chunkId, numberOfControlComponentCodeShares);
	}

	public List<ControlComponentCodeSharesPayload> generate(final String electionEventId, final String verificationCardSetId, final int chunkId,
			final int numberOfControlComponentCodeShares) {
		final List<ControlComponentCodeShare> controlComponentCodeShares = IntStream.range(0, numberOfControlComponentCodeShares)
				.mapToObj(i -> generateControlComponentCodeShare())
				.toList();

		return NODE_IDS.stream()
				.map(nodeId -> generate(electionEventId, verificationCardSetId, nodeId, chunkId, controlComponentCodeShares))
				.toList();
	}

	private ControlComponentCodeSharesPayload generate(final String electionEventId, final String verificationCardSetId, final int nodeId,
			final int chunkId, final List<ControlComponentCodeShare> controlComponentCodeShares) {
		final ControlComponentCodeSharesPayload controlComponentCodeSharesPayload = new ControlComponentCodeSharesPayload(electionEventId,
				verificationCardSetId, chunkId, encryptionGroup, controlComponentCodeShares, nodeId);

		final byte[] payloadHash = hash.recursiveHash(controlComponentCodeSharesPayload);
		final CryptoPrimitivesSignature signature = new CryptoPrimitivesSignature(payloadHash);
		controlComponentCodeSharesPayload.setSignature(signature);

		return controlComponentCodeSharesPayload;
	}

	private ControlComponentCodeShare generateControlComponentCodeShare() {
		final String verificationCardId = random.genRandomString(ID_LENGTH, base16Alphabet);

		final ElGamalMultiRecipientPublicKey voterChoiceReturnCodeGenerationPublicKey = elGamalGenerator.genRandomPublicKey(1);

		final ElGamalMultiRecipientPublicKey voterVoteCastReturnCodeGenerationPublicKey = elGamalGenerator.genRandomPublicKey(1);

		final ElGamalMultiRecipientCiphertext exponentiatedEncryptedPartialChoiceReturnCodes = elGamalGenerator.genRandomCiphertext(1);
		final ExponentiationProof encryptedPartialChoiceReturnCodeExponentiationProof = new ExponentiationProof(
				zqGroupGenerator.genRandomZqElementMember(),
				zqGroupGenerator.genRandomZqElementMember()
		);

		final ElGamalMultiRecipientCiphertext exponentiatedEncryptedConfirmationKey = elGamalGenerator.genRandomCiphertext(1);
		final ExponentiationProof encryptedConfirmationKeyExponentiationProof = new ExponentiationProof(
				zqGroupGenerator.genRandomZqElementMember(),
				zqGroupGenerator.genRandomZqElementMember()
		);

		return new ControlComponentCodeShare(verificationCardId, voterChoiceReturnCodeGenerationPublicKey, voterVoteCastReturnCodeGenerationPublicKey,
				exponentiatedEncryptedPartialChoiceReturnCodes, exponentiatedEncryptedConfirmationKey,
				encryptedPartialChoiceReturnCodeExponentiationProof, encryptedConfirmationKeyExponentiationProof);
	}
}
