/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.tally;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;

@DisplayName("A GetMixnetInitialCiphertextsRequestPayload built with")
class GetMixnetInitialCiphertextsRequestPayloadTest {

	private static final Random random = RandomFactory.createRandom();
	private static final Alphabet base64Alphabet = Base16Alphabet.getInstance();
	private static String electionEventId;
	private static String ballotBoxId;

	@BeforeEach
	void setUp() {
		electionEventId = random.genRandomString(ID_LENGTH, base64Alphabet);
		ballotBoxId = random.genRandomString(ID_LENGTH, base64Alphabet);
	}

	@Test
	@DisplayName("valid inputs does not throw.")
	void happyPath() {
		assertDoesNotThrow(() -> new GetMixnetInitialCiphertextsRequestPayload(electionEventId, ballotBoxId));
	}

	@Test
	@DisplayName("any null input throws a NullPointerException.")
	void nullInputs() {
		assertAll(
				() -> assertThrows(NullPointerException.class, () -> new GetMixnetInitialCiphertextsRequestPayload(null, ballotBoxId)),
				() -> assertThrows(NullPointerException.class, () -> new GetMixnetInitialCiphertextsRequestPayload(electionEventId, null)));
	}

	@Test
	@DisplayName("invalid election event id throws a FailedValidationException.")
	void invalidElectionEventId() {
		final String invalidElectionEventId = "invalid";
		assertThrows(FailedValidationException.class, () -> new GetMixnetInitialCiphertextsRequestPayload(invalidElectionEventId, ballotBoxId));
	}

	@Test
	@DisplayName("invalid ballot box id throws a FailedValidationException.")
	void invalidBallotBoxId() {
		final String invalidBallotBoxId = "invalid";
		assertThrows(FailedValidationException.class, () -> new GetMixnetInitialCiphertextsRequestPayload(electionEventId, invalidBallotBoxId));
	}
}
