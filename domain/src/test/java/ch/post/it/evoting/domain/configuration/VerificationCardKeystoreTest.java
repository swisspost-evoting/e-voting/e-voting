/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.configuration;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.test.tools.TestGroupSetup;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;

@DisplayName("A VerificationCardKeystore")
class VerificationCardKeystoreTest extends TestGroupSetup {
	private static final Random random = RandomFactory.createRandom();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();
	private static final String verificationCardId = random.genRandomString(ID_LENGTH, base16Alphabet);
	private static final String verificationCardKeystore = "verificationCardKeystore";

	@Test
	@DisplayName("created with a null verificationCardId, throws a NullPointerException.")
	void nullVerificationCardId() {
		assertThrows(NullPointerException.class, () -> new VerificationCardKeystore(null, verificationCardKeystore));
	}

	@Test
	@DisplayName("created with an invalid verificationCardId, throws a FailedValidationException.")
	void invalidVerificationCardId() {
		assertThrows(FailedValidationException.class, () -> new VerificationCardKeystore("verificationCardId", verificationCardKeystore));
	}

	@Test
	@DisplayName("created with a null verificationCardKeystore, throws a NullPointerException.")
	void nullVerificationCardKeystore() {
		assertThrows(NullPointerException.class, () -> new VerificationCardKeystore(verificationCardId, null));
	}

	@Test
	@DisplayName("created with a non-null values does not throw.")
	void withNonNullValuesDoesNotThrow() {
		assertDoesNotThrow(() -> new VerificationCardKeystore(verificationCardId, verificationCardKeystore));
	}
}
