/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.configuration.setupvoting;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.evotinglibraries.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;

@DisplayName("A longVoteCastReturnCodesAllowListResponsePayload")
class LongVoteCastReturnCodesAllowListResponsePayloadTest {

	private static final ObjectMapper mapper = DomainObjectMapper.getNewInstance();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();
	private static final int NODE_ID = 1;
	private static final String electionEventId = RandomFactory.createRandom().genRandomString(ID_LENGTH, base16Alphabet);
	private static final String verificationCardSetId = RandomFactory.createRandom().genRandomString(ID_LENGTH, base16Alphabet);
	private static LongVoteCastReturnCodesAllowListResponsePayload longVoteCastReturnCodesAllowListResponsePayload;
	private static ObjectNode rootNode;

	@BeforeAll
	static void setupAll() throws JsonProcessingException {

		// Create payload.
		longVoteCastReturnCodesAllowListResponsePayload = new LongVoteCastReturnCodesAllowListResponsePayload(NODE_ID, electionEventId,
				verificationCardSetId);

		// Create expected Json.
		rootNode = mapper.createObjectNode();
		rootNode.set("nodeId", mapper.readTree(mapper.writeValueAsString(NODE_ID)));
		rootNode.set("electionEventId", mapper.readTree(mapper.writeValueAsString(electionEventId)));
		rootNode.set("verificationCardSetId", mapper.readTree(mapper.writeValueAsString(verificationCardSetId)));
	}

	@Test
	@DisplayName("serialized gives expected json")
	void serializePayload() throws JsonProcessingException {
		final String serializedPayload = mapper.writeValueAsString(longVoteCastReturnCodesAllowListResponsePayload);
		assertEquals(rootNode.toString(), serializedPayload);
	}

	@Test
	@DisplayName("deserialized gives expected result")
	void deserializePayload() throws IOException {
		final LongVoteCastReturnCodesAllowListResponsePayload deserializedPayload = mapper.readValue(rootNode.toString(),
				LongVoteCastReturnCodesAllowListResponsePayload.class);
		assertEquals(longVoteCastReturnCodesAllowListResponsePayload, deserializedPayload);
		assertEquals(NODE_ID, longVoteCastReturnCodesAllowListResponsePayload.nodeId());
		assertEquals(electionEventId, longVoteCastReturnCodesAllowListResponsePayload.electionEventId());
		assertEquals(verificationCardSetId, longVoteCastReturnCodesAllowListResponsePayload.verificationCardSetId());
	}

	@Test
	@DisplayName("serialized then deserialized gives original object")
	void cycle() throws IOException {
		final LongVoteCastReturnCodesAllowListResponsePayload deserializedPayload = mapper.readValue(
				mapper.writeValueAsString(longVoteCastReturnCodesAllowListResponsePayload), LongVoteCastReturnCodesAllowListResponsePayload.class);

		assertEquals(longVoteCastReturnCodesAllowListResponsePayload, deserializedPayload);
	}

	@Test
	@DisplayName("constructed with invalid fields throws an exception")
	void testInvalidElectionEventContext() {
		assertAll(
				() -> assertThrows(NullPointerException.class,
						() -> new LongVoteCastReturnCodesAllowListResponsePayload(NODE_ID, null, verificationCardSetId)),
				() -> assertThrows(FailedValidationException.class,
						() -> new LongVoteCastReturnCodesAllowListResponsePayload(NODE_ID, "invalidElectionEventId", verificationCardSetId)),
				() -> assertThrows(NullPointerException.class,
						() -> new LongVoteCastReturnCodesAllowListResponsePayload(NODE_ID, electionEventId, null)),
				() -> assertThrows(FailedValidationException.class,
						() -> new LongVoteCastReturnCodesAllowListResponsePayload(NODE_ID, electionEventId, "invalidVerificationCardSetId")));
	}

	@Test
	@DisplayName("equals give expected result")
	void testEquality() {
		final LongVoteCastReturnCodesAllowListResponsePayload same = new LongVoteCastReturnCodesAllowListResponsePayload(NODE_ID, electionEventId,
				verificationCardSetId);
		final LongVoteCastReturnCodesAllowListResponsePayload different = new LongVoteCastReturnCodesAllowListResponsePayload(NODE_ID,
				RandomFactory.createRandom().genRandomString(ID_LENGTH, base16Alphabet), verificationCardSetId);

		assertAll(
				() -> assertEquals(longVoteCastReturnCodesAllowListResponsePayload, longVoteCastReturnCodesAllowListResponsePayload),
				() -> assertEquals(longVoteCastReturnCodesAllowListResponsePayload, same),
				() -> assertNotEquals(longVoteCastReturnCodesAllowListResponsePayload, different),
				() -> assertNotEquals(null, longVoteCastReturnCodesAllowListResponsePayload),
				() -> assertNotEquals(1L, longVoteCastReturnCodesAllowListResponsePayload),
				() -> assertEquals(longVoteCastReturnCodesAllowListResponsePayload.hashCode(), same.hashCode())
		);
	}
}
