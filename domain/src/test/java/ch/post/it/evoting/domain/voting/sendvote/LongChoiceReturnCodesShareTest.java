/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */

package ch.post.it.evoting.domain.voting.sendvote;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.test.tools.TestGroupSetup;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;

class LongChoiceReturnCodesShareTest extends TestGroupSetup {

	private static final Random random = RandomFactory.createRandom();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();

	private final String electionEventId = random.genRandomString(ID_LENGTH, base16Alphabet);
	private final String verificationCardSetId = random.genRandomString(ID_LENGTH, base16Alphabet);
	private final String verificationCardId = random.genRandomString(ID_LENGTH, base16Alphabet);
	private final int nodeId = 1;
	private final GroupVector<GqElement, GqGroup> longChoiceReturnCodeShare = gqGroupGenerator.genRandomGqElementVector(2);

	@Test
	@DisplayName("Check null arguments")
	void nullArgs() {
		assertAll(
				() -> assertThrows(NullPointerException.class,
						() -> new LongChoiceReturnCodesShare(null, verificationCardSetId, verificationCardId, nodeId,
								longChoiceReturnCodeShare)),
				() -> assertThrows(NullPointerException.class,
						() -> new LongChoiceReturnCodesShare(electionEventId, null, verificationCardId, nodeId,
								longChoiceReturnCodeShare)),
				() -> assertThrows(NullPointerException.class,
						() -> new LongChoiceReturnCodesShare(electionEventId, verificationCardSetId, null, nodeId,
								longChoiceReturnCodeShare)),
				() -> assertThrows(NullPointerException.class,
						() -> new LongChoiceReturnCodesShare(electionEventId, verificationCardSetId, verificationCardId, nodeId,
								null)),
				() -> assertDoesNotThrow(
						() -> new LongChoiceReturnCodesShare(electionEventId, verificationCardSetId, verificationCardId, nodeId,
								longChoiceReturnCodeShare))
		);
	}

	@Test
	@DisplayName("Check UUID format")
	void formatArgs() {
		final String invalidElectionEventId = "electionEventId";
		final String invalidVerificationCardSetId = "verificationCardSetId";
		final String invalidVerificationCardId = "verificationCardId";

		assertAll(
				() -> assertThrows(FailedValidationException.class,
						() -> new LongChoiceReturnCodesShare(invalidElectionEventId, verificationCardSetId, verificationCardId, nodeId,
								longChoiceReturnCodeShare)),
				() -> assertThrows(FailedValidationException.class,
						() -> new LongChoiceReturnCodesShare(electionEventId, invalidVerificationCardSetId, verificationCardId, nodeId,
								longChoiceReturnCodeShare)),
				() -> assertThrows(FailedValidationException.class,
						() -> new LongChoiceReturnCodesShare(electionEventId, verificationCardSetId, invalidVerificationCardId, nodeId,
								longChoiceReturnCodeShare))
		);
	}

	@Test
	@DisplayName("Check nodeIds in range 1 to 4")
	void nodeIsArgs() {
		assertAll(
				() -> assertThrows(IllegalArgumentException.class,
						() -> new LongChoiceReturnCodesShare(electionEventId, verificationCardSetId, verificationCardId, 0,
								longChoiceReturnCodeShare)),
				() -> assertDoesNotThrow(() -> new LongChoiceReturnCodesShare(electionEventId, verificationCardSetId, verificationCardId, 1,
						longChoiceReturnCodeShare)),
				() -> assertDoesNotThrow(() -> new LongChoiceReturnCodesShare(electionEventId, verificationCardSetId, verificationCardId, 2,
						longChoiceReturnCodeShare)),
				() -> assertDoesNotThrow(() -> new LongChoiceReturnCodesShare(electionEventId, verificationCardSetId, verificationCardId, 3,
						longChoiceReturnCodeShare)),
				() -> assertDoesNotThrow(() -> new LongChoiceReturnCodesShare(electionEventId, verificationCardSetId, verificationCardId, 4,
						longChoiceReturnCodeShare)),
				() -> assertThrows(IllegalArgumentException.class,
						() -> new LongChoiceReturnCodesShare(electionEventId, verificationCardSetId, verificationCardId, 5,
								longChoiceReturnCodeShare))
		);
	}

}
