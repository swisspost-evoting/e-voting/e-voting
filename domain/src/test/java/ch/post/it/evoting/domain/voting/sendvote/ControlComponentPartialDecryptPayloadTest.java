/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.voting.sendvote;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.hashing.HashFactory;
import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.GqGroupGenerator;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ZqGroupGenerator;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ExponentiationProof;
import ch.post.it.evoting.domain.MapperSetUp;
import ch.post.it.evoting.evotinglibraries.domain.SerializationUtils;
import ch.post.it.evoting.evotinglibraries.domain.common.ContextIds;
import ch.post.it.evoting.evotinglibraries.domain.signature.CryptoPrimitivesSignature;

@DisplayName("A ControlComponentPartialDecryptPayload")
class ControlComponentPartialDecryptPayloadTest extends MapperSetUp {

	private static final Random random = RandomFactory.createRandom();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();
	private static final String electionEventId = random.genRandomString(ID_LENGTH, base16Alphabet);
	private static final String verificationCardSetId = random.genRandomString(ID_LENGTH, base16Alphabet);
	private static final String verificationCardId = random.genRandomString(ID_LENGTH, base16Alphabet);
	private static final int nodeId = 1;
	private static final Hash hash = HashFactory.createHash();

	private static ObjectNode rootNode;
	private static ControlComponentPartialDecryptPayload controlComponentPartialDecryptPayload;

	@BeforeAll
	static void setUpAll() {
		final GqGroup gqGroup = GroupTestData.getGqGroup();
		final GqGroupGenerator gqGroupGenerator = new GqGroupGenerator(gqGroup);
		final ZqGroupGenerator zqGroupGenerator = new ZqGroupGenerator(ZqGroup.sameOrderAs(gqGroup));

		// Create payload.
		final ContextIds contextIds = new ContextIds(electionEventId, verificationCardSetId, verificationCardId);
		final GroupVector<GqElement, GqGroup> exponentiatedGammas = gqGroupGenerator.genRandomGqElementVector(2);
		final GroupVector<ExponentiationProof, ZqGroup> exponentiationProofs = Stream.generate(
						() -> new ExponentiationProof(zqGroupGenerator.genRandomZqElementMember(), zqGroupGenerator.genRandomZqElementMember()))
				.limit(2)
				.collect(GroupVector.toGroupVector());

		final PartiallyDecryptedEncryptedPCC partiallyDecryptedEncryptedPCC = new PartiallyDecryptedEncryptedPCC(contextIds, nodeId,
				exponentiatedGammas, exponentiationProofs);

		final ControlComponentPartialDecryptPayload payload = new ControlComponentPartialDecryptPayload(gqGroup, partiallyDecryptedEncryptedPCC
		);
		final byte[] payloadBytes = hash.recursiveHash(payload);
		final CryptoPrimitivesSignature signature = new CryptoPrimitivesSignature(payloadBytes);
		payload.setSignature(signature);

		controlComponentPartialDecryptPayload = payload;

		// Create expected Json.
		rootNode = mapper.createObjectNode();

		final JsonNode encryptionGroupNode = SerializationUtils.createEncryptionGroupNode(gqGroup);
		rootNode.set("encryptionGroup", encryptionGroupNode);

		final ObjectNode contextIdsNode = mapper.createObjectNode();
		contextIdsNode.put("electionEventId", electionEventId);
		contextIdsNode.put("verificationCardSetId", verificationCardSetId);
		contextIdsNode.put("verificationCardId", verificationCardId);

		final ObjectNode partiallyDecryptedEncryptedPCCNode = mapper.createObjectNode();
		final ArrayNode exponentiationProofsNode = SerializationUtils.createExponentiationProofsNode(exponentiationProofs);
		final ArrayNode exponentiatedGammasNode = SerializationUtils.createGqGroupVectorNode(exponentiatedGammas);
		partiallyDecryptedEncryptedPCCNode.set("contextIds", contextIdsNode);
		partiallyDecryptedEncryptedPCCNode.put("nodeId", nodeId);
		partiallyDecryptedEncryptedPCCNode.set("exponentiatedGammas", exponentiatedGammasNode);
		partiallyDecryptedEncryptedPCCNode.set("exponentiationProofs", exponentiationProofsNode);

		rootNode.set("partiallyDecryptedEncryptedPCC", partiallyDecryptedEncryptedPCCNode);

		final JsonNode signatureNode = SerializationUtils.createSignatureNode(signature);
		rootNode.set("signature", signatureNode);
	}

	@Test
	@DisplayName("serialized gives expected json")
	void serializePayload() throws JsonProcessingException {
		final String serializedPayload = mapper.writeValueAsString(controlComponentPartialDecryptPayload);

		assertEquals(rootNode.toString(), serializedPayload);
	}

	@Test
	@DisplayName("deserialized gives expected payload")
	void deserializePayload() throws JsonProcessingException {
		final ControlComponentPartialDecryptPayload deserializedPayload = mapper.readValue(rootNode.toString(),
				ControlComponentPartialDecryptPayload.class);

		assertEquals(controlComponentPartialDecryptPayload, deserializedPayload);
	}

	@Test
	@DisplayName("serialized then deserialized gives original payload")
	void cycle() throws JsonProcessingException {
		final ControlComponentPartialDecryptPayload deserializedPayload = mapper.readValue(
				mapper.writeValueAsString(controlComponentPartialDecryptPayload), ControlComponentPartialDecryptPayload.class);

		assertEquals(controlComponentPartialDecryptPayload, deserializedPayload);
	}

}
