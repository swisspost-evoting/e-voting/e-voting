/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */

package ch.post.it.evoting.domain.voting.confirmvote;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.GqGroupGenerator;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;

class LongVoteCastReturnCodesShareTest {

	private static final Random random = RandomFactory.createRandom();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();

	private final GqGroup gqGroup = GroupTestData.getGqGroup();
	private final GqGroupGenerator gqGroupGenerator = new GqGroupGenerator(gqGroup);
	private final String electionEventId = random.genRandomString(ID_LENGTH, base16Alphabet);
	private final String verificationCardSetId = random.genRandomString(ID_LENGTH, base16Alphabet);
	private final String verificationCardId = random.genRandomString(ID_LENGTH, base16Alphabet);
	private final int nodeId = 1;
	private final GqElement longVoteCastReturnCodeShare = gqGroupGenerator.genMember();

	@Nested
	@DisplayName("Check constructor validation")
	class CheckConstructor {
		@Test
		@DisplayName("Check null arguments")
		void nullArgs() {

			assertAll(
					() -> assertThrows(NullPointerException.class,
							() -> new LongVoteCastReturnCodesShare(null, verificationCardSetId, verificationCardId, nodeId,
									longVoteCastReturnCodeShare)),
					() -> assertThrows(NullPointerException.class,
							() -> new LongVoteCastReturnCodesShare(electionEventId, null, verificationCardId, nodeId,
									longVoteCastReturnCodeShare)),
					() -> assertThrows(NullPointerException.class,
							() -> new LongVoteCastReturnCodesShare(electionEventId, verificationCardSetId, null, nodeId,
									longVoteCastReturnCodeShare)),
					() -> assertThrows(NullPointerException.class,
							() -> new LongVoteCastReturnCodesShare(electionEventId, verificationCardSetId, verificationCardId, nodeId,
									null)),

					() -> assertDoesNotThrow(
							() -> new LongVoteCastReturnCodesShare(electionEventId, verificationCardSetId, verificationCardId, nodeId,
									longVoteCastReturnCodeShare))
			);
		}

		@Test
		@DisplayName("Check UUID format")
		void formatArgs() {

			final String invalidElectionEventId = "electionEventId";
			final String invalidVerificationCardSetId = "verificationCardSetId";
			final String invalidVerificationCardId = "verificationCardId";

			assertAll(
					() -> assertThrows(FailedValidationException.class,
							() -> new LongVoteCastReturnCodesShare(invalidElectionEventId, verificationCardSetId, verificationCardId, nodeId,
									longVoteCastReturnCodeShare)),
					() -> assertThrows(FailedValidationException.class,
							() -> new LongVoteCastReturnCodesShare(electionEventId, invalidVerificationCardSetId, verificationCardId, nodeId,
									longVoteCastReturnCodeShare)),
					() -> assertThrows(FailedValidationException.class,
							() -> new LongVoteCastReturnCodesShare(electionEventId, verificationCardSetId, invalidVerificationCardId, nodeId,
									longVoteCastReturnCodeShare))
			);
		}

		@Test
		@DisplayName("Check nodeIds in range 1 to 4")
		void nodeIsArgs() {

			assertAll(
					() -> assertThrows(IllegalArgumentException.class,
							() -> new LongVoteCastReturnCodesShare(electionEventId, verificationCardSetId, verificationCardId, 0,
									longVoteCastReturnCodeShare)),
					() -> assertDoesNotThrow(() -> new LongVoteCastReturnCodesShare(electionEventId, verificationCardSetId, verificationCardId, 1,
							longVoteCastReturnCodeShare)),
					() -> assertDoesNotThrow(() -> new LongVoteCastReturnCodesShare(electionEventId, verificationCardSetId, verificationCardId, 2,
							longVoteCastReturnCodeShare)),
					() -> assertDoesNotThrow(() -> new LongVoteCastReturnCodesShare(electionEventId, verificationCardSetId, verificationCardId, 3,
							longVoteCastReturnCodeShare)),
					() -> assertDoesNotThrow(() -> new LongVoteCastReturnCodesShare(electionEventId, verificationCardSetId, verificationCardId, 4,
							longVoteCastReturnCodeShare)),
					() -> assertThrows(IllegalArgumentException.class,
							() -> new LongVoteCastReturnCodesShare(electionEventId, verificationCardSetId, verificationCardId, 5,
									longVoteCastReturnCodeShare))
			);
		}

		@Test
		@DisplayName("Check all groups are of the same order")
		void checkGroupAreSameOrder() {
			final GqGroup differentGqGroup = GroupTestData.getDifferentGqGroup(gqGroup);
			final GqGroupGenerator gqDifferentGroupGenerator = new GqGroupGenerator(differentGqGroup);

			final GqElement longVoteCastReturnCodeShareDifferentGroup = gqDifferentGroupGenerator.genMember();

			assertThrows(IllegalArgumentException.class,
					() -> new LongVoteCastReturnCodesShare(electionEventId, verificationCardSetId, verificationCardId, 0,
							longVoteCastReturnCodeShareDifferentGroup));
		}
	}

}
