# Changelog

## Patch 1.4.4.4

Patches 1.4.4.1 - 1.4.4.3 were technical internal patches to fix the build reproducibility and are hence omitted in the change log.

* Fixed a problem in the voter portal with fetching resources when having poor network connectivity.
* Handle the case more gracefully when voters enter an incorrect URL of the voter portal.

## Release 1.4.4

Release 1.4.4 incorporates the following improvements:

* Improved the reliability of the communication from the Secure Data Manager with the voting server.
* Added a retry mechanism in the Secure Data Manager to recover from connection failures.
* Slightly improved transaction handling in the voting server for better reliability.
* Improved the fixed thread executor to better use the available CPU in the Secure Data Manager.
* Added a log message for the opening and closing dates of the ballot boxes.
* Added a no-store option and an idempotency key in requests to prevent double requests from certain browsers.
* Added the notranslate directive in the voter portal HTML.
* Removed search engine indexation from the voter portal.
* Minor text improvements in the voter portal.
* Fixed the documentation of the HTTP response headers in the voter portal.
* Improved the parametrization of the log file in the xml-signature tool.
* Various small improvements and bug fixes.
* Updated dependencies and third-party libraries.

## Release 1.4.3.2

Release 1.4.3.2 incorporates the following improvements:

* Increase the maximum supported number of write-ins from 15 to 30.
* Update e-voting-libraries dependency to 1.4.3.2.

## Release 1.4.3.1

Release 1.4.3.1 incorporates a minor finding from the public intrusion test and a small fix:

* Prevent an inconsistent state in the untrustworthy voting server's view when receiving two concurrent sendVote requests (issue reported in the 2024 public intrusion test). A similar correction was made to the setup component public keys' upload in the voting server.
* Aligned the naming convention of the direct trust keystores and fixed a small bug in the SDM UI.
* Increased the transaction time-out and made the polling of the ballot box status more robust in the SDM.

## Release 1.4.3

Release 1.4.3 includes some feedback from the Federal Chancellery's mandated experts and other experts of the community.
We want to thank the experts for their high-quality, constructive remarks:

* Thomas Edmund Haines (Australian National University), Olivier Pereira (Université catholique Louvain), Vanessa Teague (Thinking Cybersecurity)
* Aleksander Essex (Western University Canada)
* Rolf Haenni, Reto Koenig, Philipp Locher, Eric Dubuis (Bern University of Applied Sciences)

Release 1.4.3 includes the following functionalities and improvements:

* Added the GetHashElectionEventContext algorithm to the SetupTallyCCM and SetupTallyEB algorithms (feedback from Thomas Haines, Olivier Pereira, and Vanessa Teague).
* Extended the service to store the entire election event context in the control component's database.
* Improved input validation in the CreateVote, EncodeWriteIns, WriteInToInteger, and WriteInToQuadraticResidue algorithms (feedback from Rolf Haenni, Reto Koenig and Philipp Locher).
* Removed the letter U+00A0 (Non-Breaking Space) from the latin alphabet (feedback from Rolf Haenni, Reto Koenig and Philipp Locher).
* Improved the handling of context variables in the GetAuthenticationChallenge algorithm (feedback from Rolf Haenni, Reto Koenig and Philipp Locher).
* Added an additional checkbox in the legal terms of the voter portal to confirm that the voter votes electronically (feedback from Thomas Haines, Olivier Pereira, and Vanessa Teague).
* Simplified the context and input to the VerifyCCSchnorrProofs algorithm (feedback from Rolf Haenni, Reto Koenig and Philipp Locher).
* Improved the voting server's input validation, idempotency, and logging.
* Improved the Secure Data Manager's handling of frontend states.
* Aligned file names across different components.
* Improved the control components' parametrization of concurrency and database time-outs.
* Various improvements to the Secure Data Manager's frontend and connection handling.
* Fixed an inconsistency in the evoting-print's handling of cumulated candidates.
* Removed the spell check of the start voting key in the voter portal.
* Added various improvements to the direct trust tool.
* Clarified the build instructions.
* Various small improvements and bug fixes.
* Updated dependencies and third-party libraries.

## Release 1.4.2

Release 1.4.2 includes some feedback from the Federal Chancellery's mandated experts (see above) and other experts of the community.

Release 1.4.2 includes the following functionalities and improvements:

* Improved the voting server's connection and thread management.
* Strenghtened the Content-Security-Policy of the Voter Portal.
* Improved the path handling in the Secure Data Manager.
* Various improvements to the direct trust tool.
* Fixed various issues related to the integration of the xml-signature tool.
* Added a constraint to the control component's ballot box database table to allow only one update (feedback from Thomas Haines, Olivier Pereira, and Vanessa Teague).
* Added the generation of a file in the SDM with metadata for reporting purposes.
* Added a possibility to display the contents of test ballot boxes to the SDM.
* Various improvements to the SDM workflow.
* Refactored the evoting-build docker file and related scripts and moved them outside the code repository.
* Refactored the usage of synthetic test data.
* Added configurability to the voter portal's time-out values.
* Improved error handling in the SDM.
* Improved election event summary in the SDM.
* Removed empty CSS files in the voter portal.
* Various small improvements and bug fixes.
* Updated dependencies and third-party libraries.

## Release 1.4.1

Release 1.4.1 includes some feedback from the Federal Chancellery's mandated experts (see above) and other experts of the community.

Release 1.4.1 includes the following functionalities and improvements:

* Prevented the execution of the GetMixnetInitialCiphertexts algorithm before the closing of regular ballot boxes (feedback from Vanessa Teague, Olivier Pereira, and Thomas Haines).
* Provided a file-cryptor tool that facilitates the symmetric encryption and decryption for auditing purposes (feedback from Vanessa Teague, Olivier Pereira, and Thomas Haines).
* Further reduced third-party dependencies in the e-voting system (feedback from Rolf Haenni, Reto Koenig, Philipp Locher and Eric Dubuis).
* Various improvements in the Secure Data Manager Frontend.
* Improved validations in the Secure Data Manager related to the display of various information in the Secure Data Manager.
* Refactored the direct trust tool for better usability and robustness.
* Improved recoverability of errors in the voting server and control components.
* Improved the configuration of the Artemis message broker.
* Improved idempotency in the voting server.
* Added a health check endpoint to the voting server.
* Removed code in the Secure Data Manager linked to the admin board functionality.
* Fixed an out-of-memory problem when downloading large ballot boxes.
* Fixed a bug where incorrect tally files were collected.
* Fixed a small bug in the Voter Portal when quickly changing the selections.
* Updated dependencies and third-party libraries.

## Release 1.4.0

Release 1.4.0 includes some feedback from the Federal Chancellery's mandated experts (see above) and other experts of the community.

Release 1.4.0 includes the following functionalities and improvements:

* Significantly reduced the number of third-party dependencies (feedback from Rolf Haenni, Reto Koenig, Philipp Locher and Eric Dubuis).
* Removed unused exponentiation proofs in the algorithms CreateLCCShare and CreateLVCCShare (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, Eric Dubuis, Vanessa Teague, Olivier Pereira, and Thomas Haines).
* Improved the validation and separation of context and input variables in all algorithms (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, Eric Dubuis, Vanessa Teague, Olivier Pereira, and Thomas Haines).
* Facilitated dispute resolution when the control components do not agree on the list of confirmed votes by adding an additional consistency check to the algorithm MixDecOnline (feedback from Thomas Haines, Olivier Pereira, Vanessa Teague).
* Improved domain separation in voter authentication algorithms (feedback from Aleksander Essex).
* Extended the primes mapping table and implemented the new algorithms operating on it.
* Extended the semantic information in the pTable to comprise all 4 languages (feedback from Rolf Haenni, Reto Koenig, Philipp Locher and Eric Dubuis).
* Restrain the format of the encryption parameters' seed in GetElectionEventEncryptionParameters.
* Improved the ProcessPlaintexts algorithm by checking for invalid votes after decryption (feedback from Vanessa Teague, Olivier Pereira, and Thomas Haines).
* Refactored the services to better encapsulate the algorithms for increased auditability of the system.
* Aligned the configuration phase algorithms by using the GenSetupData algorithm to generate the primes mapping tables.
* Simplified the protocol by executing GenKeysCCR before GenEncLongCodeShares.
* Centralized the Schnorr proof verification methods into the e-voting-libraries.
* Moved the alphabets to crypto-primitives.
* Encode the actual voting options and write-ins within the CreateVote algorithm.
* CreateVote restricts the length of the write-ins and prevents usage of # within the write-in fields.
* Centralized hashing operations with the usage of the algorithm GetHashContext.
* Incorporated the encryption parameters generation into the Secure Data manager and removed the encryption parameters generation from the config cryptographic parameters tool.
* Renamed the remaining functionality of the cryptographic parameters tool to Direct Trust Tool.
* Switched the message broker from RabbitMQ to Artemis for better reliability and robustness.
* Improved asynchronous communication between the voting server and the control components.
* [Secure Data Manager] Complete redesign of the Secure Data Manager Frontend. Simplified the user interface and provided better feedback to the user for the individual process steps.
* [Secure Data Manager] Migrated the frontend framework from AngularJS to Angular.
* [Secure Data Manager] Improved resilience and robustness to prevent inconsistencies.
* [Secure Data Manager] Removed the necessity of installing sTunnel to provide client authentication.
* [Voting Server] Slightly improved the validations when receiving data in the configuration phase.
* [Voter Portal] Support primary / secondary elections.
* [Voter Portal] Various improvements in texts and cross-browser support.
* Minor improvements and code clean-ups.
* Updated dependencies and third-party libraries.


## Patch 1.3.3.2

Patch 1.3.3.2 is a minor patch containing the following bug fixes and voter portal improvements (patch 1.3.3.1 was an internal patch and is hence omitted in the change log).

* Reduced the concurrency of the mixing process in the control components and added a retry mechanism to mitigate against out-of-thread problems when mixing large ballot boxes.
* Fixed a bug that prevented the mixing of large ballot boxes due to a message size constraint.
* Fixed a small bug in the voting server database table definition.
* [Voter Portal] Increased the configurability of the texts to accommodate canton-specific needs.
* [Voter Portal] Fixed a bug with incomplete translations on certain browsers.

## Release 1.3.3

Release 1.3.3 is a minor maintenance patch containing the following changes:

* Increased the voter authentication time step to 300 seconds, added a forward time step, and improved the error message for desynchronized clients.
* Replaced two characters in the alphabet of the start voting key for increased usability by using the new GenRandomSVK algorithm.
* Implemented a mechanism to recover gracefully from a partially failed compute operation in the configuration phase.
* Enhanced the validation process for inaccurately addressed messages within the control components' mixing operations.
* Fixed a bug that prevented the mixing of very large ballot boxes.
* Reduced the default value for cache entries in the fixed-base exponentiation table (performance optimization) for better stability.
* Voter-Portal: Extended the configurability of the legal terms page and the help menu.
* Voter-Portal: Added the possibility to use hyperlinks in the support address.
* Voter-Portal: Display the question number next to the question and improve the display of variant ballots.
* Voter-Portal: Resolved two browser-specific issues that prevented the proper display of the Start Voting Key.
* Updated dependencies and third-party libraries.

## Release 1.3.2

Release 1.3.2 is a minor maintenance patch containing the following changes:

* Added an error message in the voter portal in case of desynchronized system time.
* Increased the minimum password size of the import / export password to 24 characters.
* Added parallelization and a retry mechanism to the compute step in the configuration phase.
* Introduced a transient CONFIRMING voting card state in the voting server.
* Fixed a bug in the xml-signature tool when handling keystores without a signing key pair.
* Strengthened the exactly-once processing in the control components.
* Minor bug fixes and improvements in the voter portal.
* Updated dependencies and third-party libraries.

## Release 1.3.1

Release 1.3.1 includes some feedback from the Federal Chancellery's mandated experts (see above) and other experts of the community.

Release 1.3.1 includes the following functionalities and improvements:

* Modified the voter authentication protocol by introducing a voting client generated nonce to allow authentications within a 30-seconds window.
* Increased parallelization of time-consuming operations.
* Integrated fixed-base exponentiations for time-consuming operations in the configuration phase (GenVerDat).
* Reduced the amount of information transferred when a voter logs in but the vote was already confirmed (reported in GitLab issue [43 / Results from the public intrusion test 2022](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/issues/43))
* Improved performance by storing individual entries of the CMTable and pCC allow list in the database instead of the table as a whole.
* Added the chunk-wise processing of large data items to avoid time-outs and out-of-memory issues.
* Introduced idempotent processing of requests in the voting server to increase robustness.
* Reduced the amount of transferred data during the export and import by filtering irrelevant data items.
* Improved the Secure Data Manager's reliability by disabling certain actions when preconditions are not met.
* Increased the minimum length of electoral board passwords to 24 characters.
* Improved method transactions in the voting server.
* Fixed a minor issue related to the zip-slip vulnerability whereby empty directories could be created by a malicious ZIP file.
* Minor bug fixes.
* Updated dependencies and third-party libraries. Removed unused dependencies.

## Release 1.3

Release 1.3 includes some feedback from the Federal Chancellery's mandated experts (see above) and other experts of the community.

Release 1.3 includes the following functionalities and improvements:

* Implemented the new voter authentication protocol (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, Eric Dubuis, and Aleksander Essex).
* Removed the legacy cryptolib and cryptolib-js libraries (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, and Eric Dubuis).
* Greatly reduced the number of JavaScript third-party libraries (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, and Eric Dubuis).
* Added the semantic information of voting options to the primes mapping table and included it in the voting client's zero-knowledge proofs (CreateVote, VerifyBallotCCR, VerifyVotingClientProofs) and in the associated data of the verification key store (GenCredDat, GetKey) (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, and Eric Dubuis).
* Integrated the pseudo-code implementation of the GetActualVotingOptions, GetEncodedVotingOptions, and GetSemanticInformation algorithms (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, and Eric Dubuis).
* Extracted common algorithms (Factorize, QuadraticResidueToWriteIn, IntegerToWriteIn, isWriteInOption, DecodeWriteIns, GetMixnetInitialCiphertexts, VerifyMixDecOffline, VerifyVotingClientProofs) to evoting-libraries.
* Aligned the Argon2id parameters to the specification (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, and Eric Dubuis).
* Improved the consistency checks upon persisting the votes and the payloads in the control components and setup component (reported in GitLab issue [46 / #YWH-PGM232-113 & #YWH-PGM232-114 & #YWH-PGM232-120](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/issues/46)).
* Aligned the voting server to SpringBoot, simplified its architecture and replaced the sanitization mechanism with proper constructor and deserialization validation. This point addresses GitLab Issue [46 / #YWH-PGM232-109](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/issues/46).
* Symmetrical encrypt the ZIP file upon export in the Secure Data Manager and decrypt upon import. This feature mitigates GitLab issues [46 / #YWH-PGM232-106 & #YWH-PGM232-126](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/issues/46)
* Refactored the generation of the election event configuration and pre-computation step in the Secure Data Manager.
* Removed legacy certificates (platform and tenant certificates) and application-level signature headers in favor of the direct-trust certificates described in the system specification.
* Updated to the evoting-config version 5.
* Various improvements in the Voter Portal to make it more accessible to screen readers addressing issues reported in [GitLab issue #45](https://gitlab.com/swisspost-evoting/e-voting/e-voting/-/issues/45).
* Updated dependencies and third-party libraries.

---

## Release 1.2.3.

Release 1.2.3 contains some minor bug fixes and updates. Release 1.2.1 and 1.2.2 were internal releases and hence do not contain a separate readme.

* Added a customizable header section to the Voter Portal.
* Prevented the dynamic loading Javascript resources in the Voter Portal via lazy-loading.
* Fixed heap size and connection management issues in the Secure Data Manager.
* Implemented additional XXE protection mechanisms including a locale dependent comparison of strings.
* Fixed minor errors and XML serialization issues in the eCH tally files.
* Increased the maximum number of supported voting options from 1200 to 3000.
* Truncated the size of the voting card ID to 22 characters to allow its integration into a Data Matrix Code (canton-specific request).
* Various improvements in the Voter Portal to make it more accessible to screen readers.
* Updated dependencies and third-party libraries.

---

## Release 1.2

Release 1.2 includes some feedback from the Federal Chancellery's mandated experts (see above)

Release 1.2 includes the following functionalities and improvements:

* Implemented a systematic validation of the identifiers (electionEventId, verificationCardSetId, ballotBoxId, verificationCardId) prior to invoking the algorithms (feedback from Vanessa Teague, Olivier Pereira, and Thomas
  Haines).
* Ensured in the setup component that the control components' code shares match the expected content and order (feedback from Vanessa Teague, Olivier Pereira, and Thomas
Haines).
* Simplified the QuadraticResidueToWriteIn algorithm (feedback from Vanessa Teague, Olivier Pereira, and Thomas Haines).
* Improved context and input validation in the SetupTallyEB algorithm (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, Eric Dubuis, Vanessa Teague, Olivier Pereira, and Thomas Haines).
* Explicitly defined the PRNG by committing the java.security properties files (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, and Eric Dubuis).
* Improved the alignment of the ExtractCRC and ExtractVCC algorithms (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, and Eric Dubuis).
* Improved the immutability of objects in the GetMixnetInitialCiphertexts algorithm (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, and Eric Dubuis).
* Removed unnecessary output objects in the MixDecOnlineOutput class (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, and Eric Dubuis).
* Aligned the implementation of the GetElectionEventParameters algorithm (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, and Eric Dubuis).
* Optimized the Argon2id parameters (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, and Eric Dubuis).
* Minor alignment and validation improvements in various algorithms (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, and Eric Dubuis).
* Implemented various web accessibility improvements in the voter portal.
* Fixed the zip-slip vulnerability (corresponds to [GitLab issue #8](https://gitlab.com/swisspost-evoting/e-voting/e-voting/-/issues/8)).
* Fixed bugs in edge cases for generating the tally files.
* Separated the election event context into a cryptographic setup component public keys object and a domain-specific election event context.
* Optimized the performance in various operations using parallelization and caching.
* Improved the handling of large election events by implementing chunked transfer-encoding.
* Removed record-breaking characters before logging an exception in the voting server.
* Added a new participant "CANTON" to the direct trust keystores for verifying the signature of files that precede the e-voting system.
* Streamlined the handling of timezones in databases.
* Added missing Javadoc (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, and Eric Dubuis).
* Updated dependencies and third-party libraries.

---

## Release 1.1

Release 1.1 includes some feedback from the Federal Chancellery's mandated experts (see above)

Release 1.1 includes the following functionalities and improvements:

* Support elections with write-in (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, and Eric Dubuis).
* Implement the generation of the eCH-0222 file in the Secure Data Manager.
* Improved the input validation of the voting client's and control component's voting phase algorithms (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, and Eric Dubuis).
* Integrated the Base16, Base32, and Base64 encoder and decoder wrapper methods (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, and Eric Dubuis).
* Reproducible build improvements.
* Updated dependencies and third-party libraries.

---

## Release 1.0

Release 1.0 incorporates feedback from the Federal Chancellery's mandated experts (see above), improves the alignment of the Swiss Post Voting
System, and contains the following changes:

* Switched the security level to EXTENDED (3072 bit modulus) (feedback from
  Aleksander Essex, Rolf Haenni, Reto Koenig, Philipp Locher, Eric Dubuis, Vanessa Teague, Olivier Pereira, and Thomas Haines).
* Added the memory-hard key derivation function Argon2id to the algorithms GenCredDat and GetKey.
* Added the Schnorr Proofs to the GenKeysCCR and SetupTallyEB algorithms and added the verification to GenVerCardSetKeys.
* Implemented an allow list when copying files from USB keys (
  fixes #YWH-PGM2323-49 [Gitlab issue #5](https://gitlab.com/swisspost-evoting/e-voting/e-voting/-/issues/5) and
  #YWH-PGM2323-72 in [GitLab Issue #37](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/issues/37))
* Improved the handling and separation of the three Secure Data Manager instances (config, tally, and online).
* Improved the sanitization of HTTP Request parameters (fixes #YWH-PGM2323-66 in [GitLab Issue #35](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/issues/35))
* Included the primes mapping table in the voting client proofs to ensure that all parties have a consistent view (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, and Eric
  Dubuis).
* Implemented the GetElectionEventEncryptionParameters algorithm.
* Implemented the DecodeVotingOptions algorithm and included it in the ProcessPlaintexts algorithm (feedback from
  Rolf Haenni, Reto Koenig, Philipp Locher, Eric Dubuis, Vanessa Teague, Olivier Pereira, and Thomas Haines).
* Implemented the generation of the evoting-decrypt and eCH-0110 file in the Secure Data Manager.
* Extended the direct trust signatures to the configuration, decryption and eCH-0110 file.
* Removed the legacy orchestrator and moved the compute functionality to the new message-broker-orchestrator.
* Enforced the domain of the actual voting options.
* Migrated the JKS keystores to the standard PKCS12 keystores.
* Updated dependencies and third-party libraries.

---

## Release 0.15

Release 0.15 incorporates feedback from the Federal Chancellery's mandated experts (see above), improves the alignment of the Swiss Post Voting
System, and contains the following changes:

* Implemented the new confirmation phase protocol, enabling the control components to determine the confirmation status of each voter. This issue is part of the solution to fix [Gitlab issue #11](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/issues/11).
* Implemented the new tally phase algorithms. Each control component verifies the shuffle and decryption proofs of the preceding control components before mixing and decrypting the votes. This issue is part of the solution to fix [Gitlab issue #11](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/issues/11).
* Implemented the Tally control component validations of the voting client proofs. This issue is part of the solution to fix [Gitlab issue #11](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/issues/11).
* Implemented a new way for digitally signing messages using a direct-trust-based approach with a predefined set of certificates and keys  (feedback from Vanessa Teague, Olivier Pereira, and Thomas
  Haines).
* Improved the alignment of implementation to specification for all algorithms. (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, and Eric
  Dubuis).
* Implemented the possibility for electoral board members to enter passwords for deriving the electoral board secret key.
* Ensured that the setup component sends the correct election public key to the control components and added the verification of Schnorr proofs in the SetupTallyEB algorithm (feedback from Vanessa Teague, Olivier Pereira, and Thomas
  Haines).
* Added consistency checks in methods and domain objects.
* Removed Spring Batch from the methods implementing the cryptographic protocol to increase auditability. (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, and Eric
  Dubuis).
* Updated the system to Java 17.
* Updated dependencies and third-party libraries.
* Ensured reproducible builds.

---

## Release 0.14

Release 0.14 incorporates feedback from the Federal Chancellery's mandated experts (see above), improves the alignment of the Swiss Post Voting
System, and contains the following changes:

* Allowed the SDM to request the control component keys generated in the algorithms GenKeysCCR and SetupTallyCCM.
* Enforce exactly-once processing in the control components (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, and Eric Dubuis).
* Improved the alignment between code and specification in various algorithms.
* [Temporary] Replaced the election public key and Choice Return Codes encryption public key with dummy keys (see known issues below).
* Integrated the KDFtoZq method (feedback from Vanessa Teague, Olivier Pereira, and Thomas
  Haines).
* Enriched the CombineEncLongCodeShares algorithm with the lVCC allow list.
* Provided a new functionality in the config cryptographic parameters tool to generate key stores and certificates for digital signatures.
* Reorganized the control component's database tables.
* Increased the size of the Start Voting Key from 20 to 24 characters (feedback from
  Rolf Haenni, Reto Koenig, Philipp Locher, Eric Dubuis, Vanessa Teague, Olivier Pereira, and Thomas Haines).
* Removed the electoral board smart card functionality (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, and Eric
  Dubuis).
* Removed the legacy cryptolib ElGamal key pair generation functionality (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, and Eric
  Dubuis).
* Removed the unused verification card set issuer, ballot box signer, and credential ID signing key pair.
* Remove SecureLogging functionality from control components since it is no longer required with the protocol improvements.
* Updated the version of H2 database, fixing the vulnerability [CVE-2022-23221](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-23221).
* Updated outdated and vulnerable dependencies.
* Various minor improvements.

---

## Release 0.13

Release 0.13 incorporates feedback from the Federal Chancellery's mandated experts (see above), improves the alignment of the Swiss Post Voting
System, and contains the following changes:

* Introduced a new robust message broker service for communication with the control components.
* Implemented synchronisation (via a database locking mechanism) during key generation operations. Introducing message validation and additional
  locking mechanisms to prevent mulitiple different responses to the same message. (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, and Eric
  Dubuis).
* Incorporated the HashAndSquare operation before exponentiating to the Confirmation Key (feedback from Vanessa Teague, Olivier Pereira, and Thomas Haines).
* Merged the control components' Return Codes and mixing services.
* Implemented the partial Choice Return Codes allow list in the GenVerDat and CreateLCCShare algorithms. This point refers
  to [Gitlab issue #7](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/issues/7).
* Implemented the DecryptPCC_j algorithm in the control components.
* Implemented the CreateVote algorithm using a TypeScript implementation of the crypto-primitives implementation (open-source).
* Aligned the following algorithms:
  * CreateVote
  * VerifyBallotCCR
  * PartialDecryptPCC (this point solves the issue #YWH-PGM2323-51 mentioned
    in [Gitlab issue #32](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/issues/32))
  * DecryptPCC
* Removed key compression (feedback from Vanessa Teague, Olivier Pereira, and Thomas Haines).
* Removed the readObject method which could lead to insecure deserializations. This point refers to issue #YWH-PGM2323-44 mentioned
  in [Gitlab issue #32](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/issues/32).
* Fixed the lack of exception control in the class SanitizerDataHttpServletRequestWrapper This point refers to issue #YWH-PGM2323-45 mentioned
  in [Gitlab issue #32](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/issues/32).
* Added the missing element to the signature of the ReturnCodeGenerationInput object (fixes
  the [Gitlab issue #4](https://gitlab.com/swisspost-evoting/e-voting/e-voting/-/issues/4))
* Removed unused cryptolib code for ElGamal encryption and zero-knowledge proofs (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, and Eric
  Dubuis).
* Merged both Secure Data Manager backends and removed the second dependency on an external Tomcat application server (plus its associated WAR
  artifacts).
* Updated outdated and vulnerable dependencies.

---

## Release 0.12

Release 0.12 incorporates feedback from the Federal Chancellery's mandated experts (see above), facilitates the development of the Swiss Post Voting
System's verifier, and contains the following changes:

* Updated references and component names to align with the latest draft version of the Ordinance on Electronic Voting (OEV).
* Aligned the control components' zero-knowledge proofs to the system specification using the crypto-primitives implementation. Previously, we
  indicated this point as a known issue. Specifically, we improved the alignment of the algorithms:
  * CreateLCCShare
  * CreateLVCCShare
  * ExtractCRC
  * ExtractVCC
* Implemented the generation of prime numbers for encoding voting options using the crypto-primitives GetSmallPrimeGroupMembers method (feedback from
  Rolf Haenni, Reto Koenig, Philipp Locher, Eric Dubuis, Vanessa Teague, Olivier Pereira, and Thomas Haines).
* Moved shared domain objects to the crypto-primitives-domain library.
* Persisted the MixnetInitialPayload in the Secure Data Manager to allow its subsequent verification in the verifier.

Moreover, release 0.12 contains the following bug fixes and improvements. These points do not impact the implementation of the cryptographic protocol.

* Fixed a minor bug by using ordinal-style query parameters in Hibernate.
* Fixed a bug affecting the high-availability environment by changing the database integration in the control components.
* Removed the requirement to have an activated administration board in the SDM for downloading the ballot boxes.
* Simplified the build by refactoring the voting server POM files.
* Moved the findGroupGenerator method to the crypto-primitives library.
* Updated outdated and vulnerable dependencies.
* Various minor improvements.