/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {Component} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SessionService} from '../session/session.service';
import {FormBuilder, FormGroup, FormsModule, ReactiveFormsModule, Validators} from '@angular/forms';
import {API_BASE_PATH} from '../app.module';
import {HttpClient} from '@angular/common/http';
import {switchMap} from 'rxjs';

@Component({
  selector: 'app-keystores-download',
  standalone: true,
  imports: [CommonModule, FormsModule, ReactiveFormsModule],
  templateUrl: './keystores-download.component.html',
  styleUrl: './keystores-download.component.css'
})
export class KeystoresDownloadComponent {

  downloadForm: FormGroup;
  private fingerprints!: { [component: string]: string };

  constructor(private fb: FormBuilder, private http: HttpClient, public session: SessionService) {

    this.downloadForm = this.fb.group({
      customSuffix: ['', [Validators.pattern(/^\w*$/)]],
    });

    this.session.getSession().pipe(
      switchMap(sessionId => this.http.get(`${API_BASE_PATH}/key-store-download/fingerprints/${sessionId}`, {responseType: "json"}))
    ).subscribe(value => {
      this.fingerprints = <{ [component: string]: string }>value;
    });
  }

  downloadKeystores() {
    this.session.getSession().pipe(
      switchMap(sessionId => this.http.get<KeystoresDownloadDto>(`${API_BASE_PATH}/key-store-download/${sessionId}`, {responseType: "json"})
      )
    ).subscribe(archive => {
      const src = `data:text/csv;base64,${archive.content}`;
      const link = document.createElement("a");
      link.href = src;
      link.download = archive.filename;
      link.click();
      link.remove();
    });
  }

  getFingerprints(): string[][] {
    if (!this.fingerprints) {
      return [['-', '']];
    }
    return Object.entries(this.fingerprints).sort();
  }
}

interface KeystoresDownloadDto {
  filename: string;
  content: string;
}
