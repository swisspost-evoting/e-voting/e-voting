/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {ComponentFixture, TestBed} from '@angular/core/testing';

import {KeystoresDownloadComponent} from './keystores-download.component';

describe('KeystoreDownloadComponent', () => {
  let component: KeystoresDownloadComponent;
  let fixture: ComponentFixture<KeystoresDownloadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [KeystoresDownloadComponent]
    })
      .compileComponents();

    fixture = TestBed.createComponent(KeystoresDownloadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
