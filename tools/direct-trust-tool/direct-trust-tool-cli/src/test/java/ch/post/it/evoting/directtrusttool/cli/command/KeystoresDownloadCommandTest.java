/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.directtrusttool.cli.command;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.NONE;

import java.nio.file.Path;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import ch.post.it.evoting.directtrusttool.backend.process.keystoresdownload.KeystoresDownloadService;
import ch.post.it.evoting.directtrusttool.cli.DirectTrustToolCliApplication;
import ch.post.it.evoting.directtrusttool.cli.FileService;

import picocli.CommandLine;

@SpringBootTest(webEnvironment = NONE, classes = DirectTrustToolCliApplication.class)
class KeystoresDownloadCommandTest {

	@MockBean
	KeystoresDownloadService keystoresDownloadService;
	@MockBean
	FileService fileService;

	@Autowired
	CommandLine.IFactory factory;

	@Autowired
	KeystoresDownloadCommand keystoresDownloadCommand;

	@AfterEach
	void tearDown() {
		Mockito.reset(keystoresDownloadService);
		Mockito.reset(fileService);
	}

	@Test
	void testDownloadKeystoresWithDefaults() {
		// given
		final byte[] expectedZip = new byte[0];
		final String expectedSessionId = "00000000000000000000000000000000";
		final Path expectedPath = Path.of("test.zip");

		given(keystoresDownloadService.downloadKeystores(expectedSessionId)).willReturn(expectedZip);

		// when
		final int exitCode = new CommandLine(keystoresDownloadCommand, factory)
				.execute("--output", "test.zip");

		// then
		assertEquals(0, exitCode);
		then(fileService).should().saveByteArrayAsZip(expectedZip, expectedPath);
		then(keystoresDownloadService).should().downloadKeystores(expectedSessionId);
	}

	@Test
	void testDownloadKeystoresWithCustomSession() {
		// given
		final byte[] expectedZip = new byte[0];
		final String expectedSessionId = "11111111111111111111111111111111";
		final Path expectedPath = Path.of("test.zip");

		given(keystoresDownloadService.downloadKeystores(expectedSessionId)).willReturn(expectedZip);

		// when
		final int exitCode = new CommandLine(keystoresDownloadCommand, factory)
				.execute(
						"--output", "test.zip",
						"--session-id", expectedSessionId
				);

		// then
		assertEquals(0, exitCode);
		then(fileService).should().saveByteArrayAsZip(expectedZip, expectedPath);
		then(keystoresDownloadService).should().downloadKeystores(expectedSessionId);
	}

}