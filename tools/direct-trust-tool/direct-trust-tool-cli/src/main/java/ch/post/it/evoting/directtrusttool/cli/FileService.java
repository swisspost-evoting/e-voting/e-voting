/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.directtrusttool.cli;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

import org.springframework.stereotype.Service;

@Service
public class FileService {
	//Keystore
	public Map<String, String> getAllFilesContentAsString(final Path directoryPath) {
		checkNotNull(directoryPath);
		checkArgument(Files.isDirectory(directoryPath), "The given path is not a directory. [path: %s]", directoryPath);

		final Map<String, String> fileContents = new HashMap<>();
		try (final Stream<Path> paths = Files.walk(directoryPath)) {
			paths.filter(Files::isRegularFile)
					.forEach(filePath -> {
						try {
							final String content = Files.readString(filePath, StandardCharsets.UTF_8);
							fileContents.put(filePath.getFileName().toString(), content);
						} catch (final IOException e) {
							throw new UncheckedIOException("Cannot read the files.", e);
						}
					});
		} catch (final IOException e) {
			throw new UncheckedIOException(e);
		}
		return fileContents;
	}

	public void saveByteArrayAsZip(final byte[] zipContent, final Path outputPath) {
		checkNotNull(zipContent);
		checkNotNull(outputPath);

		try {
			Files.createDirectories(outputPath.getParent());
			Files.write(outputPath, zipContent);
		} catch (final IOException e) {
			throw new UncheckedIOException("Error while saving zip.", e);
		}
	}
}
