/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.directtrusttool.cli;

import java.security.cert.CertificateException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import ch.post.it.evoting.directtrusttool.backend.process.ComponentStorageRepository;
import ch.post.it.evoting.directtrusttool.backend.process.FileRepository;
import ch.post.it.evoting.directtrusttool.backend.process.NameService;
import ch.post.it.evoting.directtrusttool.backend.process.PemConverterService;
import ch.post.it.evoting.directtrusttool.backend.process.keystoresdownload.KeystoresDownloadService;
import ch.post.it.evoting.directtrusttool.backend.process.keystoresgeneration.KeystoresGenerationService;
import ch.post.it.evoting.directtrusttool.backend.process.publickeyssharing.PublicKeysSharingService;
import ch.post.it.evoting.directtrusttool.backend.process.session.SessionService;

@Configuration
public class DirectTrustToolCliConfig {

	@Bean
	public FileRepository fileRepository(
			@Value("${app.directory.output}")
			final String outputDirectory) {
		return new FileRepository(outputDirectory);
	}

	@Bean
	public ComponentStorageRepository componentStorageRepository(final FileRepository fileRepository) {
		return new ComponentStorageRepository(fileRepository);
	}

	@Bean
	public SessionService sessionService(final ComponentStorageRepository componentStorageRepository) {
		return new SessionService(componentStorageRepository);
	}

	@Bean
	public PemConverterService pemConverterService() throws CertificateException {
		return new PemConverterService();
	}

	@Bean
	public NameService nameService(ComponentStorageRepository componentStorageRepository, SessionService sessionService) {
		return new NameService(componentStorageRepository, sessionService);
	}

	@Bean
	public KeystoresDownloadService keystoreDownloadService(
			final ComponentStorageRepository componentStorageRepository,
			final PemConverterService pemConverterService,
			final SessionService sessionService,
			final NameService nameService) {
		return new KeystoresDownloadService(componentStorageRepository, pemConverterService, sessionService, nameService);
	}

	@Bean
	public PublicKeysSharingService publicKeysService(
			final ComponentStorageRepository componentStorageRepository,
			final PemConverterService pemConverterService,
			final SessionService sessionService,
			final NameService nameService) {
		return new PublicKeysSharingService(componentStorageRepository, sessionService, pemConverterService, nameService);
	}

	@Bean
	public KeystoresGenerationService keystoreGenerationService(
			final ComponentStorageRepository componentStorageRepository,
			final PemConverterService pemConverterService,
			final SessionService sessionService) {
		return new KeystoresGenerationService(componentStorageRepository, sessionService, pemConverterService);
	}
}
