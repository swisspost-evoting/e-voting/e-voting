/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.directtrusttool.backend.process.publickeyssharing;

import static ch.post.it.evoting.directtrusttool.backend.process.ComponentStorageRepository.Type.PUBLIC_KEY;
import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.security.cert.Certificate;
import java.util.Arrays;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.directtrusttool.backend.process.ComponentStorageRepository;
import ch.post.it.evoting.directtrusttool.backend.process.ComponentStorageRepository.Key;
import ch.post.it.evoting.directtrusttool.backend.process.NameService;
import ch.post.it.evoting.directtrusttool.backend.process.PemConverterService;
import ch.post.it.evoting.directtrusttool.backend.process.Zipper;
import ch.post.it.evoting.directtrusttool.backend.process.session.Phase;
import ch.post.it.evoting.directtrusttool.backend.process.session.SessionService;
import ch.post.it.evoting.evotinglibraries.domain.signature.Alias;

@Service
public class PublicKeysSharingService {

	private static final Logger LOGGER = LoggerFactory.getLogger(PublicKeysSharingService.class);

	private final ComponentStorageRepository componentStorageRepository;
	private final SessionService sessionService;
	private final PemConverterService pemConverterService;
	private final NameService nameService;

	public PublicKeysSharingService(final ComponentStorageRepository componentStorageRepository, final SessionService sessionService,
			final PemConverterService pemConverterService, final NameService nameService) {
		this.componentStorageRepository = componentStorageRepository;
		this.sessionService = sessionService;
		this.pemConverterService = pemConverterService;
		this.nameService = nameService;
	}

	public byte[] downloadPublicKeys(final String sessionId) {
		validateUUID(sessionId);
		checkState(sessionService.getSessionPhase(sessionId).equals(Phase.PUBLIC_KEYS_SHARING));

		final Map<String, byte[]> filesMap = componentStorageRepository.selectedComponents(sessionId).stream()
				.flatMap(component -> {
					final Key key = new Key(sessionId, component, PUBLIC_KEY);
					final String fileName = nameService.getFileName(sessionId, component, PUBLIC_KEY);
					return componentStorageRepository.getBytes(key).map(bytes -> Map.entry(fileName, bytes)).stream();
				})
				.collect(Collectors.toUnmodifiableMap(Map.Entry::getKey, Map.Entry::getValue));

		LOGGER.info("Files created successfully. Ready to create zip and download public keys. [sessionId: {}]", sessionId);

		return Zipper.zip(filesMap);
	}

	public void importPublicKeys(final String sessionId, final Map<String, String> componentKeysAsPem) {
		validateUUID(sessionId);
		checkState(sessionService.getSessionPhase(sessionId).equals(Phase.PUBLIC_KEYS_SHARING));
		checkNotNull(componentKeysAsPem);
		checkArgument(componentKeysAsPem.size() == Alias.values().length - 1);

		// Convert the PEM map to X509 map
		final Map<Alias, Certificate> componentKeys = componentKeysAsPem.entrySet().stream()
				.collect(Collectors.toUnmodifiableMap(
						e -> nameService.getAliasFromFileName(e.getKey()),
						e -> pemConverterService.fromPem(e.getValue())));

		final Set<Alias> selectedComponents = componentStorageRepository.selectedComponents(sessionId);
		selectedComponents.forEach(component -> {
			final byte[] keystore = componentStorageRepository.getBytes(
							new Key(sessionId, component, ComponentStorageRepository.Type.KEYSTORE))
					.orElseThrow(() -> new NoSuchElementException("Missing keystore."));
			final char[] password = componentStorageRepository.getCharArray(
							new Key(sessionId, component, ComponentStorageRepository.Type.PASSWORD))
					.orElseThrow(() -> new NoSuchElementException("Missing password for keystore."));

			final byte[] updatedKeystore;
			try {
				updatedKeystore = KeystoreKeysImporter.importPublicKeys(keystore, password.clone(), component, componentKeys);
			} finally {
				Arrays.fill(password, '0');
			}

			componentStorageRepository.putBytes(new Key(sessionId, component, ComponentStorageRepository.Type.KEYSTORE),
					updatedKeystore);
		});

		Arrays.stream(Alias.values())
				.filter(component -> !selectedComponents.contains(component))
				.filter(Alias::hasPrivateKey)
				.forEach(component -> componentStorageRepository.putCharArray(new Key(sessionId, component, PUBLIC_KEY),
						pemConverterService.toPem(componentKeys.get(component)).toCharArray()));


		LOGGER.info("Imported public keys successfully. [sessionId: {}]", sessionId);

		sessionService.setPhase(sessionId, Phase.KEYSTORES_DOWNLOAD);
	}
}
