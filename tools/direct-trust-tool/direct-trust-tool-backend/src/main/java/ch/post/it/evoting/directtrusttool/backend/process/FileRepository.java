/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.directtrusttool.backend.process;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.Optional;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

@Repository
public class FileRepository {

	private final Path outputDirectory;

	public FileRepository(
			@Value("${app.directory.output}")
			final String outputDirectory) {
		this.outputDirectory = Path.of(outputDirectory);
	}

	public Optional<byte[]> readFile(final Path fileName) {
		checkNotNull(fileName);

		final Path path = outputDirectory.resolve(fileName);
		if (Files.notExists(path)) {
			return Optional.empty();
		}
		try {
			return Optional.of(Files.readAllBytes(path));
		} catch (final IOException e) {
			throw new UncheckedIOException(e);
		}
	}

	public void writeFile(final Path fileName, final byte[] content) {
		checkNotNull(fileName);
		checkNotNull(content);

		final Path path = outputDirectory.resolve(fileName);
		try {
			Files.write(path, content);
		} catch (final IOException e) {
			throw new UncheckedIOException(e);
		}
	}

	public void createDirectory(final Path directory) {
		checkNotNull(directory);

		try {
			Files.createDirectories(outputDirectory.resolve(directory));
		} catch (final IOException e) {
			throw new UncheckedIOException(e);
		}
	}

	public boolean isPresent(final Path fileName) {
		checkNotNull(fileName);
		final Path path = outputDirectory.resolve(fileName);

		return Files.exists(path);
	}

	public void removeDirectory(final Path directory) {
		final Path path = outputDirectory.resolve(directory);
		if (Files.exists(path)) {
			// Traverse the directory from the bottom up and delete all files and directories
			try (final Stream<Path> paths = Files.walk(path).sorted(Comparator.reverseOrder())) {
				paths.forEach(streamedPath -> {
					try {
						Files.delete(streamedPath);
					} catch (final IOException e) {
						throw new UncheckedIOException(e);
					}
				});
			} catch (final IOException e) {
				throw new UncheckedIOException(e);
			}
		}
	}
}
