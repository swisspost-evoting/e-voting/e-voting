/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.directtrusttool.backend.process.keystoresgeneration;

import static com.google.common.base.Preconditions.checkNotNull;

public record CertificateDefaultValueDto(String country, String state, String locality, String organisation) {

	public CertificateDefaultValueDto {
		checkNotNull(country);
		State.isValidLabel(state);
		checkNotNull(locality);
		checkNotNull(organisation);
	}
}
