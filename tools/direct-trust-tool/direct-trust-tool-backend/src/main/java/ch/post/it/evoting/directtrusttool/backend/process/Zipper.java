/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.directtrusttool.backend.process;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class Zipper {

	private Zipper() {
		// utility class
	}

	public static byte[] zip(final Map<String, byte[]> filesMap) {
		final ByteArrayOutputStream baos = new ByteArrayOutputStream();
		final ZipOutputStream zos = new ZipOutputStream(baos);

		try (baos; zos) {
			filesMap.forEach((key, value) -> writeFileEntry(zos, key, value));

		} catch (final IOException e) {
			throw new UncheckedIOException(e);
		}
		return baos.toByteArray();
	}

	private static void writeFileEntry(final ZipOutputStream zos, final String key, final byte[] value) {
		try {
			zos.putNextEntry(new ZipEntry(key));
			zos.write(value);
		} catch (final IOException e) {
			throw new UncheckedIOException(e);
		}
	}
}
