/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.directtrusttool.backend.process.keystoresdownload;

import static ch.post.it.evoting.directtrusttool.backend.RouteConstants.BASE_PATH;
import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Base64;
import java.util.Map;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.post.it.evoting.directtrusttool.backend.process.NameService;

@RestController
@RequestMapping(BASE_PATH + "/key-store-download")
public class KeystoresDownloadController {

	private final KeystoresDownloadService keystoresDownloadService;
	private final NameService nameService;

	public KeystoresDownloadController(final KeystoresDownloadService keystoresDownloadService, final NameService nameService) {
		this.keystoresDownloadService = keystoresDownloadService;
		this.nameService = nameService;
	}

	@GetMapping(value = "{sessionId}", produces = "application/json")
	public KeystoresDownloadDto downloadKeystores(
			@PathVariable
			final String sessionId) {
		validateUUID(sessionId);
		return new KeystoresDownloadDto(
				nameService.getArchiveName(sessionId),
				new String(Base64.getEncoder().encode(keystoresDownloadService.downloadKeystores(sessionId)))
		);
	}

	@GetMapping(value = "fingerprints/{sessionId}", produces = "application/json")
	public Map<String, String> getFingerprints(
			@PathVariable
			final String sessionId
	) {
		validateUUID(sessionId);
		return keystoresDownloadService.extractFingerprints(sessionId);
	}
}
