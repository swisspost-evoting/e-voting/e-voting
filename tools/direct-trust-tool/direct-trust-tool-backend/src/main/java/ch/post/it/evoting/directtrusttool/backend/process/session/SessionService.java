/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.directtrusttool.backend.process.session;

import static ch.post.it.evoting.cryptoprimitives.math.RandomFactory.createRandom;
import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;

import java.util.Optional;
import java.util.Set;

import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.directtrusttool.backend.process.ComponentStorageRepository;
import ch.post.it.evoting.evotinglibraries.domain.signature.Alias;

@Service
public class SessionService {
	private static final String SESSION_PHASE_KEY = "phase";

	private final ComponentStorageRepository componentStorageRepository;

	public SessionService(final ComponentStorageRepository componentStorageRepository) {
		this.componentStorageRepository = componentStorageRepository;
	}

	public String createNewSession() {
		return createRandom().genRandomString(ID_LENGTH, Base16Alphabet.getInstance());
	}

	public Phase getSessionPhase(final String sessionId) {
		validateUUID(sessionId);

		return Optional.ofNullable(componentStorageRepository.getGlobalStorageKey(sessionId, SESSION_PHASE_KEY))
				.map(Phase::valueOf)
				.orElse(Phase.KEYSTORES_GENERATION);
	}

	public void setPhase(final String sessionId, final Phase phase) {
		componentStorageRepository.putGlobalStorageKey(sessionId, SESSION_PHASE_KEY, phase.name());
	}

	public void deleteSession(final String sessionId) {
		componentStorageRepository.deleteStorage(sessionId);
	}

	public Set<Alias> getCurrentComponents(final String sessionId) {
		return componentStorageRepository.selectedComponents(sessionId);
	}

}
