/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.directtrusttool.backend.process.keystoresgeneration;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;

import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base64Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.signing.AuthorityInformation;
import ch.post.it.evoting.cryptoprimitives.signing.GenKeysAndCert;
import ch.post.it.evoting.cryptoprimitives.signing.KeysAndCert;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureFactory;
import ch.post.it.evoting.evotinglibraries.domain.signature.Alias;

public class KeystoreCreator {
	private static final int PASSWORD_LENGTH = 32;
	private final LocalDate validFrom;
	private final LocalDate validUntil;
	private final Random random;
	private final Alphabet base64Alphabet;
	private final AuthorityInformation authorityInformation;

	public KeystoreCreator(final LocalDate validUntil, final AuthorityInformation authorityInformation) {
		this.validUntil = checkNotNull(validUntil);
		this.authorityInformation = checkNotNull(authorityInformation);
		this.validFrom = LocalDate.now();

		checkArgument(this.validUntil.isAfter(validFrom));

		this.random = RandomFactory.createRandom();
		this.base64Alphabet = Base64Alphabet.getInstance();
	}

	public Output generateKeystore(final Alias component) {
		final char[] password = random.genRandomString(PASSWORD_LENGTH, base64Alphabet).toCharArray();
		try (final ByteArrayOutputStream keyStoreStream = new ByteArrayOutputStream()) {
			final KeyStore keystore = KeyStore.getInstance("PKCS12");
			keystore.load(null, password.clone());
			final Optional<KeysAndCert> keysAndCert = generateKeyAndCert(component);
			keysAndCert.ifPresent(k -> writeEntryToKeystore(component, k, keystore, password.clone()));
			keystore.store(keyStoreStream, password.clone());

			return new Output(
					keyStoreStream.toByteArray(),
					password.clone(),
					keysAndCert
							.map(KeysAndCert::certificate)
							.orElse(null)
			);
		} catch (final CertificateException | KeyStoreException | IOException | NoSuchAlgorithmException e) {
			throw new KeystoreCreationException(e);
		} finally {
			Arrays.fill(password, '0');
		}
	}

	private Optional<KeysAndCert> generateKeyAndCert(final Alias component) {
		if (component.hasPrivateKey()) {
			final SignatureFactory signatureFactory = SignatureFactory.getInstance();
			final GenKeysAndCert keyAndCertGenerator = signatureFactory.createGenKeysAndCert(getAuthorityInformationForAlias(component));
			return Optional.of(keyAndCertGenerator.genKeysAndCert(validFrom, validUntil));
		}
		return Optional.empty();
	}

	private AuthorityInformation getAuthorityInformationForAlias(final Alias component) {
		return AuthorityInformation.builder()
				.setCommonName(component.get())
				.setCountry(authorityInformation.getCountry())
				.setLocality(authorityInformation.getLocality())
				.setOrganisation(authorityInformation.getOrganisation())
				.setState(authorityInformation.getState())
				.build();
	}

	private static void writeEntryToKeystore(final Alias component, final KeysAndCert k, final KeyStore keystore, final char[] password) {
		try {
			keystore.setKeyEntry(component.get(), k.privateKey(), password.clone(), new Certificate[] { k.certificate() });
		} catch (final KeyStoreException e) {
			throw new KeystoreCreationException(e);
		} finally {
			Arrays.fill(password, '0');
		}
	}

	public record Output(byte[] keyStore, char[] password, Certificate publicKey) {

		@Override
		public boolean equals(final Object o) {
			if (this == o) {
				return true;
			}
			if (o == null || getClass() != o.getClass()) {
				return false;
			}
			final Output output = (Output) o;
			return Arrays.equals(keyStore, output.keyStore) && Arrays.equals(password, output.password) && Objects.equals(
					publicKey, output.publicKey);
		}

		@Override
		public int hashCode() {
			int result = Objects.hash(publicKey);
			result = 31 * result + Arrays.hashCode(keyStore);
			result = 31 * result + Arrays.hashCode(password);
			return result;
		}

		@Override
		public String toString() {
			return "Output{" +
					"keyStore=" + Arrays.toString(keyStore) +
					", password='" + "*".repeat(password.length) + "'" +
					", publicKey=" + publicKey +
					'}';
		}
	}

	public static class KeystoreCreationException extends RuntimeException {
		public KeystoreCreationException(final Throwable cause) {
			super(cause);
		}
	}
}
