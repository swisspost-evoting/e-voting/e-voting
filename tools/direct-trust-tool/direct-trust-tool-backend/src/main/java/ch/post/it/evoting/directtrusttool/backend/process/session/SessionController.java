/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.directtrusttool.backend.process.session;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.util.Set;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.base.Preconditions;

import ch.post.it.evoting.directtrusttool.backend.RouteConstants;
import ch.post.it.evoting.directtrusttool.backend.process.ComponentStorageRepository;
import ch.post.it.evoting.evotinglibraries.domain.signature.Alias;

@RestController
@RequestMapping(RouteConstants.BASE_PATH + "/session")
public class SessionController {

	private final SessionService sessionService;
	private final ComponentStorageRepository componentStorageRepository;

	public SessionController(final SessionService sessionService, final ComponentStorageRepository componentStorageRepository) {
		this.sessionService = sessionService;
		this.componentStorageRepository = componentStorageRepository;
	}

	@PostMapping(value = "")
	public String createSession() {
		return sessionService.createNewSession();
	}

	@GetMapping(value = "{sessionId}", produces = "application/json")
	public Phase sessionPhase(
			@PathVariable
			final String sessionId) {
		validateUUID(sessionId);
		return sessionService.getSessionPhase(sessionId);
	}

	@DeleteMapping(value = "{sessionId}")
	public void deleteSession(
			@PathVariable
			final String sessionId) {
		validateUUID(sessionId);
		sessionService.deleteSession(sessionId);
	}

	@GetMapping(value = "{sessionId}/selected", produces = "application/json")
	public Set<Alias> getSelectedKeystore(
			@PathVariable
			final String sessionId) {
		validateUUID(sessionId);
		return sessionService.getCurrentComponents(sessionId);
	}

	@GetMapping(value = "{sessionId}/key/{key}", produces = "application/json")
	public String getSessionKeyValue(
			@PathVariable
			final String sessionId,
			@PathVariable
			final String key) {
		validateUUID(sessionId);
		checkNotNull(key);
		return componentStorageRepository.getGlobalStorageKey(sessionId, key);
	}
}
