/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.directtrusttool.backend.process.session;

public enum Phase {
	KEYSTORES_GENERATION,
	PUBLIC_KEYS_SHARING,
	KEYSTORES_DOWNLOAD
}