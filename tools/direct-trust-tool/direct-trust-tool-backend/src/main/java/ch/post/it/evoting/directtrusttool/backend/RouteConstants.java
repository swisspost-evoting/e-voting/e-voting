/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
package ch.post.it.evoting.directtrusttool.backend;

public final class RouteConstants {
	public static final String BASE_PATH = "api/direct-trust";

	private RouteConstants() {
		// Constant only class
	}
}
