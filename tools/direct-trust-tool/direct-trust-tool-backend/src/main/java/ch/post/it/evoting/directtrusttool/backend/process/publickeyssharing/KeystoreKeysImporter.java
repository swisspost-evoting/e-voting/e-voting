/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.directtrusttool.backend.process.publickeyssharing;

import static com.google.common.base.Preconditions.checkState;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.util.Arrays;
import java.util.Map;

import ch.post.it.evoting.cryptoprimitives.utils.VerificationResult;
import ch.post.it.evoting.evotinglibraries.direct.trust.KeystoreValidator;
import ch.post.it.evoting.evotinglibraries.domain.signature.Alias;

public class KeystoreKeysImporter {

	private KeystoreKeysImporter() {
		// utility class
	}

	public static byte[] importPublicKeys(final byte[] keyStoreBytes, final char[] password, final Alias component,
			final Map<Alias, Certificate> componentKeys) {
		try {
			final KeyStore keystore = KeyStore.getInstance("PKCS12");

			try (final ByteArrayInputStream keyStoreStream = new ByteArrayInputStream(keyStoreBytes)) {
				keystore.load(keyStoreStream, password.clone());
			}

			componentKeys.entrySet().stream()
					.filter(componentAndPublicKey -> {
						try {
							return !keystore.containsAlias(componentAndPublicKey.getKey().get());
						} catch (final KeyStoreException e) {
							throw new KeystoreImportException(e);
						}
					})
					.forEach(componentAndPublicKey -> saveCertificateInKeystore(
							componentAndPublicKey.getKey(),
							componentAndPublicKey.getValue(),
							keystore));

			final Alias signingAlias = component.hasPrivateKey() ? component : null;
			final VerificationResult result = KeystoreValidator.validateKeystore(keystore, signingAlias, password.clone());
			checkState(result.isVerified(), "Keystore validation failed. [component: %s, errorMessages: %s]", component.get(),
					result.isVerified() ? "" : result.getErrorMessages());

			try (final ByteArrayOutputStream updatedKeystoreStream = new ByteArrayOutputStream()) {
				keystore.store(updatedKeystoreStream, password.clone());
				return updatedKeystoreStream.toByteArray();
			}
		} catch (final IOException | KeyStoreException | NoSuchAlgorithmException | CertificateException e) {
			throw new KeystoreImportException(e);
		} finally {
			Arrays.fill(password, '0');
		}
	}

	private static void saveCertificateInKeystore(final Alias component, final Certificate key, final KeyStore keystore) {
		try {
			keystore.setCertificateEntry(component.get(), key);
		} catch (final KeyStoreException e) {
			throw new KeystoreImportException(e);
		}
	}

	public static class KeystoreImportException extends RuntimeException {
		public KeystoreImportException(final Throwable cause) {
			super(cause);
		}
	}
}
