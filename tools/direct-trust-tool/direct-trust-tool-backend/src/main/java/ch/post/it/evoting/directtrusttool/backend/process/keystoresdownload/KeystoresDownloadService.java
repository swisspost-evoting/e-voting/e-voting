/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.directtrusttool.backend.process.keystoresdownload;

import static ch.post.it.evoting.directtrusttool.backend.process.ComponentStorageRepository.Type.KEYSTORE;
import static ch.post.it.evoting.directtrusttool.backend.process.ComponentStorageRepository.Type.PASSWORD;
import static ch.post.it.evoting.directtrusttool.backend.process.ComponentStorageRepository.Type.PUBLIC_KEY;
import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkState;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HexFormat;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.directtrusttool.backend.process.ComponentStorageRepository;
import ch.post.it.evoting.directtrusttool.backend.process.NameService;
import ch.post.it.evoting.directtrusttool.backend.process.PemConverterService;
import ch.post.it.evoting.directtrusttool.backend.process.Zipper;
import ch.post.it.evoting.directtrusttool.backend.process.session.Phase;
import ch.post.it.evoting.directtrusttool.backend.process.session.SessionService;
import ch.post.it.evoting.evotinglibraries.domain.signature.Alias;

@Service
public class KeystoresDownloadService {

	public static final String ZIP_PATH_SEPARATOR = "/";

	private static final Logger LOGGER = LoggerFactory.getLogger(KeystoresDownloadService.class);
	private final ComponentStorageRepository componentStorageRepository;
	private final PemConverterService pemConverterService;
	private final SessionService sessionService;
	private final NameService nameService;

	public KeystoresDownloadService(final ComponentStorageRepository componentStorageRepository, final PemConverterService pemConverterService,
			final SessionService sessionService, final NameService nameService) {
		this.componentStorageRepository = componentStorageRepository;
		this.pemConverterService = pemConverterService;
		this.sessionService = sessionService;
		this.nameService = nameService;
	}

	public byte[] downloadKeystores(final String sessionId) {
		validateUUID(sessionId);
		checkState(sessionService.getSessionPhase(sessionId).equals(Phase.KEYSTORES_DOWNLOAD));

		final String platform = componentStorageRepository.getGlobalStorageKey(sessionId, "platform");
		final Map<String, byte[]> filesMap = componentStorageRepository.selectedComponents(sessionId).stream()
				.flatMap(
						component -> {
							final List<Map.Entry<String, byte[]>> element = new ArrayList<>();
							element.add(createDownloableEntry(sessionId, component, KEYSTORE));
							element.add(createDownloableEntry(sessionId, component, PASSWORD));

							if (component.hasPrivateKey()) {
								element.add(createDownloableEntry(sessionId, component, PUBLIC_KEY));
							}
							return element.stream();
						})
				.collect(Collectors.toUnmodifiableMap(Map.Entry::getKey, Map.Entry::getValue));

		LOGGER.info("Files created successfully. Ready to create zip and download keystore. [sessionId: {}, platform: {}]", sessionId, platform);

		return Zipper.zip(filesMap);
	}

	/**
	 * Extracts the fingerprints for the direct trust certificates.
	 *
	 * @return a map with the alias as keys and fingerprints as values.
	 */
	public Map<String, String> extractFingerprints(final String sessionId) {
		validateUUID(sessionId);
		checkState(sessionService.getSessionPhase(sessionId).equals(Phase.KEYSTORES_DOWNLOAD));

		final Map<String, String> fingerprints = Arrays.stream(Alias.values())
				.map(alias -> {
					if (alias.hasPrivateKey()) {
						return Map.entry(alias.get(), extractFingerprintForComponent(sessionId, alias));
					} else {
						return Map.entry(alias.get(), "-");
					}
				})
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

		LOGGER.info("Fingerprints extracted successfully. [sessionId: {}]", sessionId);

		return fingerprints;
	}

	private Map.Entry<String, byte[]> createDownloableEntry(final String sessionId, final Alias component,
			final ComponentStorageRepository.Type type) {
		final String folderName = component.get() + ZIP_PATH_SEPARATOR;
		final byte[] data = componentStorageRepository.getBytes(new ComponentStorageRepository.Key(sessionId, component, type)).orElseThrow();
		return Map.entry(folderName + nameService.getFileName(sessionId, component, type), data);
	}

	private String extractFingerprintForComponent(final String sessionId, final Alias component) {
		final byte[] certificateBytes = componentStorageRepository.getBytes(
						new ComponentStorageRepository.Key(sessionId, component, PUBLIC_KEY))
				.orElseThrow(() -> new NoSuchElementException("Missing public key."));
		final Certificate certificate = pemConverterService.fromPem(new String(certificateBytes, StandardCharsets.UTF_8));

		final byte[] encodedCertificate;
		try {
			encodedCertificate = certificate.getEncoded();
		} catch (final CertificateEncodingException e) {
			throw new IllegalStateException(String.format("Failed to get encoded certificate. [alias: %s]", component.get()), e);
		}

		final MessageDigest messageDigest;
		try {
			messageDigest = MessageDigest.getInstance("SHA-256");
		} catch (final NoSuchAlgorithmException e) {
			throw new IllegalStateException("Failed to get message digest instance.", e);
		}

		final byte[] digest = messageDigest.digest(encodedCertificate);
		final String sha256Fingerprint = HexFormat.of().formatHex(digest);

		return sha256Fingerprint.toLowerCase(Locale.ENGLISH);
	}
}
