/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
package ch.post.it.evoting.directtrusttool.backend;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class DirectTrustToolBackendApplicationTests {

	@Test
	@SuppressWarnings("java:S2699")
	void contextLoads() {
		// just try to boot spring
	}

}
