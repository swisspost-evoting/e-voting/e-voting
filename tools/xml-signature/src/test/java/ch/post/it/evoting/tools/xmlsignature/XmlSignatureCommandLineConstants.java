/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.tools.xmlsignature;

public class XmlSignatureCommandLineConstants {

	// ////////////////////////////////////
	//
	// files
	//
	// ////////////////////////////////////
	public static final String CONFIGURATION_ANONYMIZED_XML = "configuration-anonymized.xml";
	public static final String EVOTING_PRINT_XML = "evoting-print.xml";

	// ////////////////////////////////////
	//
	// paths
	//
	// ////////////////////////////////////
	public static final String CONFIGURATION_ANONYMIZED_XML_PATH = "/" + CONFIGURATION_ANONYMIZED_XML;
	public static final String CONFIGURATION_ANONYMIZED_INVALID_SIGNATURE_XML_PATH = "/configuration-anonymized-invalid-signature.xml";
	public static final String EVOTING_PRINT_XML_PATH = "/" + EVOTING_PRINT_XML;
	public static final String EVOTING_PRINT_INVALID_SIGNATURE_XML_PATH = "/evoting-print-invalid-signature.xml";

	// ////////////////////////////////////
	//
	// log messages
	//
	// ////////////////////////////////////
	public static final String USAGE_ERROR_LOG_PREFIX = "Usage : java -Ddirect.trust.keystore.location=<direct-trust-keystoreFile> ";
	public static final String SUCCESS_LOG_PREFIX = "The action has finished successfully.";
	public static final String ERROR_LOG_PREFIX = "Unable to process the requested action.";
	public static final String FAILURE_LOG_PREFIX = "The action failed.";

	private XmlSignatureCommandLineConstants() {
		// Intentionally left blank.
	}
}
