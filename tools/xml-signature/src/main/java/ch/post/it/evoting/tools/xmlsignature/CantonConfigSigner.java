/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.tools.xmlsignature;

import ch.post.it.evoting.cryptoprimitives.signing.SignatureGeneration;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureVerification;
import ch.post.it.evoting.evotinglibraries.domain.common.ChannelSecurityContextData;
import ch.post.it.evoting.evotinglibraries.domain.signature.Alias;
import ch.post.it.evoting.evotinglibraries.xml.XsdConstants;
import ch.post.it.evoting.evotinglibraries.xml.hashable.HashableCantonConfigFactory;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.Configuration;

public class CantonConfigSigner extends XmlSigner<Configuration> {

	public CantonConfigSigner(final SignatureGeneration signatureGeneration,
			final SignatureVerification signatureVerification) {
		super(signatureGeneration,
				signatureVerification,
				Configuration::setSignature,
				Configuration::getSignature,
				HashableCantonConfigFactory::fromConfiguration,
				configuration -> ChannelSecurityContextData.cantonConfig(),
				Alias.CANTON,
				Configuration.class,
				CantonConfigSigner.class.getResource(XsdConstants.CANTON_CONFIG_XSD));
	}
}
