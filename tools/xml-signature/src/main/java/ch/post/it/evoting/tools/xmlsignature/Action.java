/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.tools.xmlsignature;

import java.nio.file.Path;

public enum Action {

	SIGN() {
		@Override
		public Result apply(final XmlSigner<?> xmlSigner, final Path filePath) {
			try {
				xmlSigner.sign(filePath);
				return new Result(true, null);
			} catch (final Exception e) {
				throw new IllegalStateException(String.format("An error occurred signing the file. [filePath: %s]", filePath), e);
			}
		}
	},
	VERIFY() {
		@Override
		public Result apply(final XmlSigner<?> xmlSigner, final Path filePath) {
			try {
				return new Result(xmlSigner.verify(filePath), null);
			} catch (final Exception e) {
				throw new IllegalStateException(String.format("An error occurred verifying the signature of the file. [filePath: %s]", filePath), e);
			}
		}
	};

	public abstract Result apply(final XmlSigner<?> xmlSigner, final Path filePath);
}
