/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.tools.xmlsignature;

import ch.post.it.evoting.evotinglibraries.domain.signature.Alias;

public enum SupportedFileType {

	CONFIG(CantonConfigSigner.class, Alias.CANTON),
	PRINT(SetupComponentEvotingPrintSigner.class, Alias.SDM_CONFIG);

	private final Class<? extends XmlSigner<?>> signerClass;
	private final Alias signingAlias;

	SupportedFileType(final Class<? extends XmlSigner<?>> signerClass, final Alias signingAlias) {
		this.signerClass = signerClass;
		this.signingAlias = signingAlias;
	}

	public Class<? extends XmlSigner<?>> getSignerClass() {
		return signerClass;
	}

	public Alias getSigningAlias() {
		return signingAlias;
	}
}
