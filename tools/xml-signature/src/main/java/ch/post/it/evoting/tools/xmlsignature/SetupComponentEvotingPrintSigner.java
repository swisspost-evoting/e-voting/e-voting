/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.tools.xmlsignature;

import ch.post.it.evoting.cryptoprimitives.signing.SignatureGeneration;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureVerification;
import ch.post.it.evoting.evotinglibraries.domain.common.ChannelSecurityContextData;
import ch.post.it.evoting.evotinglibraries.domain.signature.Alias;
import ch.post.it.evoting.evotinglibraries.xml.XsdConstants;
import ch.post.it.evoting.evotinglibraries.xml.hashable.HashableSetupComponentEvotingPrintFactory;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingprint.VotingCardList;

public class SetupComponentEvotingPrintSigner extends XmlSigner<VotingCardList> {
	public SetupComponentEvotingPrintSigner(final SignatureGeneration signatureGeneration,
			final SignatureVerification signatureVerification) {
		super(signatureGeneration, signatureVerification,
				VotingCardList::setSignature,
				VotingCardList::getSignature,
				HashableSetupComponentEvotingPrintFactory::fromVotingCardList,
				print -> ChannelSecurityContextData.setupComponentEvotingPrint(),
				Alias.SDM_CONFIG,
				VotingCardList.class,
				SetupComponentEvotingPrintSigner.class.getResource(XsdConstants.SETUP_COMPONENT_EVOTING_PRINT_XSD));
	}
}
